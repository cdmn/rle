options
( direct = yes,
  skip = 1
)
load data
replace
into table comp_rle.rle_input_ex_partners
fields terminated by ';'
trailing nullcols
(bsn_dln               char
, bsn_ex                char
, a_nummer_ex           char
, naam_ex               char
, naamgebruik_ex        char
, voorvoegsels_ex       char
, voornamen_ex          char
, voorletters_ex        char
, geslacht_ex           char
, geboortedatum_ex      char
, overlijdensdatum_ex   char
, landcode_ex           char
, straatnaam_ex         char
, huisnummer_ex         char
, huisletter_ex         char
, toevoeging_ex         char
, postcode_ex           char
, gemeentenummer_ex     char
, gemeentedeel_ex       char
, huwelijk_datumbegin   char
, huwelijk_datumeinde   char
, reden_einde_huwelijk  char
)
