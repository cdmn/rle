OPTIONS
( SKIP = 1, direct=true, rows=10000
)
load data
CHARACTERSET UTF8
infile 'RLE_GBA_HUWELIJKEN_TNT.dat'
replace
into table rle_gba_huwelijken_tnt
fields terminated by ';' optionally enclosed by '"'  TRAILING NULLCOLS
(id    sequence(max,1),
 BSN_PERSOON                char(30) ,
 A_NUMMER                   char(30) ,
 BSN                        char(30) ,
 VOORNAMEN                  char(210) ,
 VOORVOEGSELS_GESLACHTSNAAM char(210) ,
 GESLACHTSNAAM              char(210) ,
 GEBOORTEDATUM              char(18) ,
 DATUM_SLUITING             char(18) ,
 DATUM_ONTBINDING           char(18) ,
 REDEN_ONTBINDING           char(210),
 OPMERKINGEN                char(1010) ,
 DAT_CREATIE expression     "sysdate"
 )
