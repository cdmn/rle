-- Doel: Laden records uit ascii bestand (RLEPRS01.dat)
--       in de tabel RLE_UITVAL_ADRESSEN
--
LOAD data
INFILE 'RLEPRS01.dat'
BADFILE 'RLEPRS01.bad'
APPEND
INTO TABLE RLE_UITVAL_ADRESSEN
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
 ( persoonsnummer                char
 , straatnaam                    char
 , postcode                      char
 , huisnummer                    char
 , huisnr_toev                   char
 , woonplaats                    char
 , ind_buitenland                char
 , soort_adres                   char
 , landcode_bui                  char
 )
