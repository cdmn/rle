-- Doel: Laden records uit ascii bestand (RLEWGR01.dat)
--       in de tabel RLE_UITVAL_ADRESSEN
--
LOAD data
INFILE 'RLEWGR01.dat'
BADFILE 'RLEWGR01.bad'
INTO TABLE RLE_UITVAL_ADRESSEN
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
 ( werkgevernummer                char
 , straatnaam                     char
 , postcode                       char
 , huisnummer                     char
 , huisnr_toev                    char
 , woonplaats                     char
 , ind_buitenland                 char
 , soort_adres                    char
 , landcode_bui                   char
 )

