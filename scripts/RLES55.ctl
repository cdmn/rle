--
--
-- Datum       Auteur            Reden
-- =================================================================================
-- 06-dec-2011 Monique v Alphen  Creatie
--
LOAD DATA
REPLACE
INTO TABLE RLE_BRIEVEN_KVK
fields terminated by ';' optionally enclosed by '"'
TRAILING NULLCOLS
(werkgevernr   char)
