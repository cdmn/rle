CREATE OR REPLACE package body comp_rle.rle_scrn_sanctielijst
is
/************************************************************
Package: RLE_SCRN_SANCTIELIJST

Package voor bijwerken van de sanctielijst

Datum      Wie        Wat
---------- ---------- -----------------------------------
14-07-2010 XKV        Aanmaak
*************************************************************/
  procedure onderhouden(
    p_bron            in       varchar2
  , p_bron_ref        in       varchar2
  , p_soort           in       varchar2
  , p_voornaam        in       varchar2
  , p_achternaam      in       varchar2
  , p_volledigenaam   in       varchar2
  , p_datgeboorte     in       date
  , p_straatnaam      in       varchar2
  , p_huisnummer      in       varchar2
  , p_postcode        in       varchar2
  , p_woonplaats      in       varchar2
  , p_toegevoegd      out      varchar2
  )
  is
/* *******************************************************
Procedure: ONDERHOUDEN



Datum      Wie        Wat
---------- ---------- -----------------------------------
14-07-2010 XKV        Aanmaak
******************************************************** */
    l_zoeknaam     rle_sanctielijsten.zoeknaam%type;
  begin
    p_toegevoegd := 'N';
    -- Bepaal zoeknaam
    l_zoeknaam := nvl( p_achternaam
                     , p_volledigenaam
                     );
    -- Diacrieten verwijderen
    l_zoeknaam := convert( upper( substr( replace( replace( replace( replace( l_zoeknaam
                                                                            , ' & '
                                                                            , ' EN '
                                                                            )
                                                                   , ' &'
                                                                   , ' EN '
                                                                   )
                                                          , '& '
                                                          , ' EN '
                                                          )
                                                 , '&'
                                                 , ' EN '
                                                 )
                                        , 1
                                        , 30
                                        ))
                         , 'US7ASCII'
                         );

    insert into comp_rle.rle_sanctielijsten
                ( bron
                , bron_referentie
                , soort
                , voornaam
                , voorletter
                , achternaam
                , volledigenaam
                , zoeknaam
                , datgeboorte
                , geboortejaar
                , straatnaam
                , huisnummer
                , postcode
                , woonplaats
                )
    values      ( p_bron
                , p_bron_ref
                , p_soort
                , p_voornaam
                , substr( p_voornaam
                        , 1
                        , 1
                        )
                , p_achternaam
                , p_volledigenaam
                , l_zoeknaam
                , p_datgeboorte
                , to_number( to_char( p_datgeboorte
                                    , 'yyyy'
                                    ))
                , p_straatnaam
                , p_huisnummer
                , p_postcode
                , p_woonplaats
                );

    p_toegevoegd := 'J';
  exception
    when dup_val_on_index
    then
      -- Alleen afwijkende rijen toevoegen
      p_toegevoegd := 'N';
  end onderhouden;
end rle_scrn_sanctielijst;
/