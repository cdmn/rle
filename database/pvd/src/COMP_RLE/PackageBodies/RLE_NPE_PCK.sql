CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_NPE_PCK IS

CN_MODULE CONSTANT VARCHAR2(30) := 'RLE_NPE_PCK';

REGELNUMMER STM_LIJSTEN.REGELNUMMER%TYPE := 1;
LIJSTNUMMER STM_LIJSTEN.LIJSTNUMMER%TYPE := 1;


/* Hoofdroutine mutatie postcode tabel */
PROCEDURE MUTATIE
 (P_WEEK_CONTROLE IN varchar2
 )
 IS

/* Ophalen van gemuteerde postcodes */
CURSOR C_MPE_002
 (B_PBN_ID NUMBER
 ,B_HERSTART_VOLGNR VARCHAR2
 )
 IS
/* C_MPE_002 */
select rowid, mpe.*
from 	rle_mut_postcode mpe
where	pbn_id = b_pbn_id
and mpe.volgnr > b_herstart_volgnr
order by mpe.volgnr;

L_PBN_ID NUMBER(9);
L_AANTAL NUMBER;
L_MELDING VARCHAR2(132);
L_VERW NUMBER;
L_NIEUW NUMBER;
L_GEWIJZ NUMBER;
L_NIET_OVERG NUMBER;
L_IND_OUD_OPN VARCHAR2(1);
L_IND_NW_OPN VARCHAR2(1);
R_MPE_002 C_MPE_002%ROWTYPE;
L_HERSTART_VOLGNR NUMBER(4);
T_VORIGE_PROCEDURE VARCHAR2(80);
T_PROCEDURE VARCHAR2(80);
T_REGELNUMMER NUMBER;
L_HUIDIG_VOLGNR NUMBER(4);

/* Totaal Ophalen van gemuteerde postcodes */
CURSOR C_MPE_002_C
 (B_PBN_ID NUMBER
 )
 IS
/* C_MPE_002_C */
select count(*)
from rle_mut_postcode
where	pbn_id = b_pbn_id
;
-- RLE_NPE_PCK_MUTATIE
BEGIN
stm_util.debug('RLE_NPE_PCK.MUTATIE');
stm_batch.init_batch	( 'RLE_NPE_PCK.MUTATIE');

t_vorige_procedure	:=	stm_util.t_procedure;
stm_util.t_procedure	:=	cn_module;
stm_util.t_programma_naam	:=	cn_module;

IF stm_util.herstart
THEN
 l_herstart_volgnr	:=	stm_util.lees_herstartsleutel('l_herstart_volgnr');
 regelnummer		:=	stm_util.lees_herstarttelling('regelnummer');
 l_verw			:=	stm_util.lees_herstarttelling('l_verw');
 l_niet_overg		:=	stm_util.lees_herstarttelling('l_niet_overg');
 l_nieuw			:=	stm_util.lees_herstarttelling('l_nieuw');
 l_gewijz			:=	stm_util.lees_herstarttelling('l_gewijz');

STM_UTIL.insert_lijsten(lijstnummer,regelnummer,
        'Procedure ' || cn_module || ' opnieuw gestart op '
        ||TO_CHAR(SYSDATE,'DD-MM-YYYY HH24:MI'));
ELSE
 l_herstart_volgnr	:= 0;
 l_verw		    	:= 0;
 l_niet_overg		:= 0;
 l_nieuw			:= 0;
 l_gewijz			:= 0;

END IF;

STM_UTIL.insert_lijsten(lijstnummer,regelnummer,
        'Procedure ' || cn_module || ' gestart op '
        ||TO_CHAR(SYSDATE,'DD-MM-YYYY HH24:MI'));

--
g_ind_npe_mutatie := 'J';
--
/*
stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                   , 'VOORTGANG'
                   , 'Inlezen mutatiebestand naar '
                   ||'rle_pcod_batchrun/rle_mut_postcode'
                   );
*/
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,'Inlezen mutatiebestand naar '
                   ||'rle_pcod_batchrun/rle_mut_postcode'
                   );
--
rle_npe_pck.vul_pcbr (p_week_controle => p_week_controle);
--
/*
stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                   , 'VOORTGANG'
                   , 'Inlezen mutatiebestand klaar.'
                   );
*/
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,'Inlezen mutatiebestand klaar.');
--
l_pbn_id := rle_pbn_get_id( g_selweek, 'M');
--
OPEN c_mpe_002_c( l_pbn_id );
FETCH c_mpe_002_c INTO l_aantal;
CLOSE c_mpe_002_c;
--
-- stm_batch.log_teverwerken( l_aantal);
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,'Aantal te verwerken: '||l_aantal);
--
-- Ophalen schrijfwijze woonplaats en straatnaam
l_sw_woonpl := stm_algm_get.sysparm( NULL, 'SWPL', SYSDATE);
l_sw_straat := stm_algm_get.sysparm( NULL, 'SSTR', SYSDATE);
--
--
OPEN c_mpe_002( l_pbn_id
         		, l_herstart_volgnr);
FETCH c_mpe_002 INTO r_mpe_002;
l_huidig_volgnr := r_mpe_002.volgnr;
WHILE c_mpe_002%FOUND
--
LOOP
--
IF l_huidig_volgnr <> r_mpe_002.volgnr
THEN
stm_util.schrijf_herstartsleutel ('l_herstart_volgnr', l_herstart_volgnr);
stm_util.schrijf_herstarttelling ('regelnummer', regelnummer);
stm_util.schrijf_herstarttelling ('l_verw', l_verw);
stm_util.schrijf_herstarttelling ('l_niet_overg', l_niet_overg);
stm_util.schrijf_herstarttelling ('l_nieuw', l_nieuw);
stm_util.schrijf_herstarttelling ('l_gewijz', l_gewijz);
commit;
END IF;
l_huidig_volgnr := r_mpe_002.volgnr;
l_herstart_volgnr := l_huidig_volgnr;


  BEGIN
    IF r_mpe_002.mutsrt = 0
    THEN
    -- mut.soort 0 = verwijderen
       IF rle_npe_pck.verwijder( r_mpe_002) = 'J'
       THEN
          l_verw := l_verw + 1;
       ELSE
          l_niet_overg := l_niet_overg + 1;
       END IF;
    ELSIF r_mpe_002.mutsrt = 1
    THEN
    -- mut.soort 1 = wijziging
    -- Als oude en nieuwe postcode en/of reeks gevuld dan wijzigen.
    -- Als alleen nieuwe postcode en/of reeks gevuld dan toevoegen.
    -- Als alleen oude postcode en/of reeks gevuld dan verwijderen.
    -- Als geen van beide controleren of plaats of woonplaats
    -- wijziging betreft.
       IF NVL(SUBSTR( r_mpe_002.postcode_oud, 5, 2), '  ') = '  ' OR
          ( NVL( r_mpe_002.numscheidingvan_oud, '0') = '0' AND
            NVL( r_mpe_002.numscheidingtm_oud, '0') = '0'
          )
       THEN
          l_ind_oud_opn := 'N';
       ELSE
          l_ind_oud_opn := 'J';
       END IF;
--
       IF NVL( SUBSTR( r_mpe_002.postcode_nw, 5, 2), '  ') = '  ' OR
          ( NVL( r_mpe_002.numscheidingvan_nw, '0') = '0' AND
            NVL( r_mpe_002.numscheidingtm_nw, '0') = '0'
          )
       THEN
          l_ind_nw_opn := 'N';
       ELSE
          l_ind_nw_opn := 'J';
       END IF;
--
       IF l_ind_oud_opn = 'J'
       THEN
          IF l_ind_nw_opn = 'J'
          THEN
             IF rle_npe_pck.wijzig( r_mpe_002) = 'J'
             THEN
                l_gewijz := l_gewijz + 1;
             ELSE
                l_niet_overg := l_niet_overg + 1;
             END IF;
          ELSE
             IF rle_npe_pck.verwijder( r_mpe_002) = 'J'
             THEN
                l_verw := l_verw + 1;
             ELSE
                l_niet_overg := l_niet_overg + 1;
             END IF;
          END IF;
       ELSE
          IF l_ind_nw_opn = 'J'
          THEN
             IF rle_npe_pck.nieuw( r_mpe_002) = 'J'
             THEN
                l_nieuw := l_nieuw + 1;
             ELSE
                l_niet_overg := l_niet_overg + 1;
             END IF;
          ELSE
             IF ( ( r_mpe_002.ind_mut_woonplaats_ptt = 1) AND
                    r_mpe_002.woonplaats_ptt_oud <>
                              r_mpe_002.woonplaats_ptt_nw
                )
                OR
                ( ( r_mpe_002.ind_mut_woonplaats_nen = 1) AND
                    r_mpe_002.woonplaats_nen_oud <>
                              r_mpe_002.woonplaats_nen_nw
                )
                OR
                ( ( r_mpe_002.ind_mut_straatnaam_ptt = 1) AND
                    r_mpe_002.straatnaam_ptt_oud <>
                              r_mpe_002.straatnaam_ptt_nw
                )
                OR
                ( ( r_mpe_002.ind_mut_straatnaam_off = 1) AND
                    r_mpe_002.straatnaam_off_oud <>
                              r_mpe_002.straatnaam_off_nw
                )
                OR
                ( ( r_mpe_002.ind_mut_straatnaam_nen = 1) AND
                    r_mpe_002.straatnaam_nen_oud <>
                              r_mpe_002.straatnaam_nen_nw
                )
             THEN
                IF rle_npe_pck.wijzig( r_mpe_002) = 'J'
                THEN
                   l_gewijz := l_gewijz + 1;
                ELSE
                   l_niet_overg := l_niet_overg + 1;
                END IF;
             ELSE
                l_niet_overg := l_niet_overg + 1;
             END IF;
          END IF;
       END IF;
--
    ELSIF r_mpe_002.mutsrt=2
    THEN
    -- mut.soort 2 = toevoeging
       IF NVL( SUBSTR( r_mpe_002.postcode_nw, 5, 2), '  ') = '  ' OR
             ( NVL( r_mpe_002.numscheidingvan_nw, 0) = '0' AND
               NVL( r_mpe_002.numscheidingtm_nw, 0) = '0'
             )
       THEN
          l_niet_overg := l_niet_overg + 1;
       ELSE
          IF rle_npe_pck.nieuw( r_mpe_002) = 'J'
          THEN
             l_nieuw := l_nieuw + 1;
          ELSE
             l_niet_overg := l_niet_overg + 1;
          END IF;
       END IF;
    END IF;
--
    DELETE rle_mut_postcode
    WHERE  ROWID = r_mpe_002.rowid;
--
  EXCEPTION
  WHEN OTHERS
  THEN
     ROLLBACK;
     --stm_batch.log_fout( 'RLE_NPE_PCK.MUTATIE');
     STM_UTIL.insert_lijsten(lijstnummer,regelnummer,'Fout tijdens RLE_NPE_PCK.MUTATIE');
--
     l_melding := 'Fout IN postcode: '
               || 'Oud:'
               || r_mpe_002.postcode_oud || '/'
               || r_mpe_002.reekscod_oud || '/'
               || r_mpe_002.numscheidingvan_oud || '/'
               || r_mpe_002.numscheidingtm_oud
               || '-Nieuw:'
               || r_mpe_002.postcode_nw || '/'
               || r_mpe_002.reekscod_nw || '/'
               || r_mpe_002.numscheidingvan_nw || '/'
               || r_mpe_002.numscheidingtm_nw ;
--
/*
     stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                        , 'FOUT'
                        , l_melding);
*/
     STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
--
--   In geval van een foutsituatie binnen de cursor loop na de rollback een foutmelding opnemen
--   en STM_LIJSTEN en deze committen, openstaande cursor afsluiten en foutmelding opnieuw raisen
--   om te voorkomen dat de volgende postcode wordt verwerkt (RIK). Anders volgt altijd de
--   foutmelding: ORA-01002 fetch out of sequence!
--
     COMMIT;
     CLOSE c_mpe_002;
     RAISE;
  END;
--
  FETCH c_mpe_002 INTO r_mpe_002;
--
END LOOP;
--
CLOSE c_mpe_002;
--
l_melding := 'Aantal postcodes niet verwerkt : '
          || TO_CHAR( l_niet_overg);
/*
stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                   , 'CONTROLE'
                   , l_melding);
*/
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
l_melding := 'Aantal postcodes verwijderd    : '
          || TO_CHAR(l_verw);
/*
stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                   , 'CONTROLE'
                   , l_melding);
*/
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
l_melding := 'Aantal postcodes toegevoegd    : '
          || TO_CHAR( l_nieuw);
/*
stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                   , 'CONTROLE'
                   , l_melding);
*/
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
l_melding := 'Aantal postcodes gewijzigd     : '
          ||TO_CHAR(l_gewijz);
/*
stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                   , 'CONTROLE'
                   , l_melding);
*/
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
--
g_ind_npe_mutatie := 'N';
--
stm_batch.einde_batch( 'J');
--
COMMIT;
-- Boodschap tbv productiebeheer.
dbms_output.put_line( 'Verwerking succesvol afgerond.');
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,'Verwerking succesvol afgerond.');
--
EXCEPTION
WHEN OTHERS
THEN
   ROLLBACK;
-- stm_batch.log_fout( 'RLE_NPE_PCK.MUTATIE');
   g_ind_npe_mutatie := 'N';
   stm_batch.einde_batch( 'N');
-- Boodschap tbv productiebeheer.
   dbms_output.put_line( 'Verwerking FOUT afgelopen.');
   dbms_output.put_line(sqlerrm);
   RAISE;
END MUTATIE;
/* Vullen van RLE_PCOD_BATCHRUN EN RLE_MUT_POSTCODE */
PROCEDURE VUL_PCBR
 (P_WEEK_CONTROLE IN varchar2
 )
 IS

L_LAATSTE_WEEK VARCHAR2(10);
L_FILE_HANDLE ALG_OBJECT.HENDEL;
L_BESTANDSNAAM VARCHAR2(50) := 'PCTMUTR';
L_REGEL VARCHAR2(1000);
L_OK VARCHAR2(1);
L_PBN_ID NUMBER(9, 0);
L_BATCH_OK VARCHAR2(1) := 'N';
L_VOLGNR NUMBER := 0;

/* Ophalen laatst verwerkte maand uit postcoderunbatch */
CURSOR C_PBN_002
 IS
/* C_PBN_002 */
   select max(draaimaand)
     from rle_pcod_batchrun pbn
    where subtype = 'M'
        ;
/* Ophalen gegegvens voor zetten volgnr */
CURSOR C_MPE
 (B_PBN_ID NUMBER
 )
 IS
/* C_MPE */
    select *
    from rle_mut_postcode mpe
    where mpe.pbn_id = b_pbn_id
    order by postcode_nw
    for update
    ;
BEGIN
stm_util.debug('RLE_NPE_PCK.VULPCBR');
l_batch_ok := 'J';
--
l_file_handle:= alg_bestand.creeer( 'RLE'
                                  , 'POSTCODE'  -- subdeel van RLE
                                  , l_bestandsnaam
                                  , 'R'         -- READ
                                  );
--
-- Het bestand is te lezen, de eerste (1e) regel lezen en a.d.h.v.
-- deze regel bekijken of deze week al eens is verwerkt, of niet direkt volgt
-- op de laatst verwerkte week.
-- Zo ja, direct stoppen. (return)
-- Zo nee, dan nu een batchrun aanmaken.
--
alg_bestand.lees_regel( l_file_handle
                      , l_regel
                      );
--
STM_UTIL.insert_lijsten(lijstnummer,regelnummer,'Verwerking '||SUBSTR( l_regel, 1, 60));
-- 04-01-2007 KMG call 71963: i.p.v. maandverwerking nu weekverwerking
IF SUBSTR( l_regel, 1, 36) <> '*** MUTATIES POSTCODETABEL REEKS VAN'
THEN
   -- 1e regel van PTT-mutatiebestand fout!
   stm_dbproc.raise_error( '-20000'
                         , NULL
                         , 'RLE_NPE_PCK.VUL_PCBR'
                         );
ELSIF   SUBSTR( l_regel,49,1) = ' '
   THEN SELECT SUBSTR( l_regel, 38, 4) || '-0' ||
               SUBSTR( l_regel, 48, 1)
        INTO   g_selweek
        FROM   dual;
   ELSE SELECT SUBSTR( l_regel, 38, 4) || '-' ||
               SUBSTR( l_regel, 48, 2)
        INTO   g_selweek
        FROM   dual;
END IF;
--
-- 19-02-2004, VER, call 92421
-- 04-01-2007 KMG call 71963: i.p.v. maandverwerking nu weekverwerking
-- Controleren of aangeboden bestand van de juiste week is:
--  1) nog niet verwerkt is
--  2) aansluit op de vorige week
--
open  c_pbn_002;
fetch c_pbn_002 into l_laatste_week;
close c_pbn_002;
--
-- xcw 23-06-2011 niet alle weken worden meer aangeleverd (voorbeeld 2011-01)
-- hiervoor de mogelijkheid ingebouwd om de weekcontrole te deactiveren
IF    g_selweek <= l_laatste_week
      AND p_week_controle = 'J'
THEN  STM_UTIL.insert_lijsten( lijstnummer
                             , regelnummer
                             , 'Het aangeboden bestand (' || g_selweek || ') is al een keer verwerkt.'
                            );
      STM_UTIL.insert_lijsten( lijstnummer
                             , regelnummer
                             , 'Laatst verwerkte bestand is van '|| l_laatste_week
                            );
       RETURN;
ELSIF   (substr(g_selweek,1,4) = substr(l_laatste_week,1,4) and
         l_laatste_week = '2006-12' and
         g_selweek <> '2006-50')
         AND p_week_controle = 'J'
   THEN STM_UTIL.insert_lijsten( lijstnummer
                               , regelnummer
                               , 'Het aangeboden bestand (' || g_selweek ||
                                 ') sluit niet aan op laatst verwerkte bestand (' || l_laatste_week || ')'
                              );
       RETURN;
ELSIF   (substr(g_selweek,1,4) = substr(l_laatste_week,1,4) and
         to_number(substr(g_selweek,6,2)) > (to_number(substr(l_laatste_week,6,2)) + 1) and
         l_laatste_week <> '2006-12')
   OR   (to_number(substr(g_selweek,1,4)) = (to_number(substr(l_laatste_week,1,4)) + 1) and
         substr(g_selweek,6,2) <> '01' and
         l_laatste_week <> '2006-12')
   OR   (l_laatste_week = '2006-12' and
         g_selweek <> '2006-50')
         AND p_week_controle = 'J'
   THEN STM_UTIL.insert_lijsten( lijstnummer
                               , regelnummer
                               , 'Het aangeboden bestand (' || g_selweek ||
                                 ') sluit niet aan op laatst verwerkte bestand (' || l_laatste_week || ')'
                              );
       RETURN;
  -- einde aanpassing call 92421
  --
ELSE
   l_pbn_id := rle_pbn_seq1_next_id;
   stm_util.debug('INSERT into rle_pcod_batchrun ');
   stm_util.debug('l_pbn_id: '||l_pbn_id);
   stm_util.debug('g_selweek: '||g_selweek);
   INSERT
   INTO   rle_pcod_batchrun
          ( id
          , draaimaand         -- YYYY-WW
          , SUBTYPE
          )
   VALUES ( l_pbn_id
          , g_selweek
          , 'M'
          );
END IF;
--
<<LEES_REST_MUTATIETAPE>>
LOOP  -- lezen overige PTT-mutaties
--
  BEGIN
    stm_util.debug('bezig in loop LEES_REST_MUTATIETAPE');
    alg_bestand.lees_regel( l_file_handle
                          , l_regel
                          );
    stm_util.debug(l_regel);
--
    IF ( SUBSTR( l_regel, 1, 3) <> '***') AND
       ( SUBSTR( l_regel, 21, 6) <> '      ')
    THEN
       STM_UTIL.DEBUG('NU INSERTEN');
       INSERT
       INTO   rle_mut_postcode
              ( pbn_id
              , mutsrt
              , ind_mut_postcode_dl1
              , ind_mut_postcode_dl2
              , ind_mut_reekscod
              , ind_mut_numscheidingvan
              , ind_mut_numscheidingtm
              , ind_mut_woonplaats_ptt
              , ind_mut_woonplaats_nen
              , ind_mut_straatnaam_ptt
              , ind_mut_straatnaam_nen
              , ind_mut_straatnaam_off
              , ind_mut_gemeentenaam
              , ind_mut_codprov
              , ind_mut_codcebuco
              , postcode_oud
              , reekscod_oud
              , numscheidingvan_oud
              , numscheidingtm_oud
              , woonplaats_ptt_oud
              , woonplaats_nen_oud
              , straatnaam_ptt_oud
              , straatnaam_nen_oud
              , straatnaam_off_oud
              , gemeentenaam_oud
              , codprov_oud
              , codcebuco_oud
              , postcode_nw
              , reekscod_nw
              , numscheidingvan_nw
              , numscheidingtm_nw
              , woonplaats_ptt_nw
              , woonplaats_nen_nw
              , straatnaam_ptt_nw
              , straatnaam_nen_nw
              , straatnaam_off_nw
              , gemeentenaam_nw
              , codprov_nw
              , codcebuco_nw
              )
       VALUES ( l_pbn_id
              , RTRIM( SUBSTR( l_regel, 1, 1))              -- mutsrt
              , TO_NUMBER( SUBSTR( l_regel, 2, 1))          -- ind_mut_postcode_dl1
              , TO_NUMBER( SUBSTR( l_regel, 3, 1))          -- ind_mut_postcode_dl2
              , TO_NUMBER( SUBSTR( l_regel, 4, 1))          -- ind_mut_reekscod
              , TO_NUMBER( SUBSTR( l_regel, 5, 1))          -- ind_mut_numscheidingvan
              , TO_NUMBER( SUBSTR( l_regel, 6, 1))          -- ind_mut_numscheidingtm
              , TO_NUMBER( SUBSTR( l_regel, 7, 1))          -- ind_mut_woonplaats_ptt
              , TO_NUMBER( SUBSTR( l_regel, 8, 1))          -- ind_mut_woonplaats_nen
              , TO_NUMBER( SUBSTR( l_regel, 9, 1))          -- ind_mut_straatnaam_ptt
              , TO_NUMBER( SUBSTR( l_regel, 10, 1))         -- ind_mut_straatnaam_nen
              , TO_NUMBER( SUBSTR( l_regel, 11, 1))         -- ind_mut_straatnaam_off
              , TO_NUMBER( SUBSTR( l_regel, 14, 1))         -- ind_mut_gemeentenaam
              , TO_NUMBER( SUBSTR( l_regel, 15, 1))         -- ind_mut_codprov
              , TO_NUMBER( SUBSTR( l_regel, 16, 1))         -- ind_mut_codcebuco
              , RTRIM( SUBSTR( l_regel, 18, 6))
              , RTRIM( SUBSTR( l_regel, 24, 1))
              , TO_NUMBER( SUBSTR( l_regel, 25, 5))
              , TO_NUMBER( SUBSTR( l_regel, 30, 5))
              , RTRIM( SUBSTR( l_regel, 35, 18))
              , RTRIM( SUBSTR( l_regel, 53, 24))
              , RTRIM( SUBSTR( l_regel, 77, 17))
              , RTRIM( SUBSTR( l_regel, 94, 24))
              , RTRIM( SUBSTR( l_regel, 118, 43))
              , RTRIM( SUBSTR( l_regel, 174, 24))
              , RTRIM( SUBSTR( l_regel, 198, 1))
              , TO_NUMBER( SUBSTR( l_regel, 199, 3))
              , RTRIM( SUBSTR( l_regel, 202, 6))
              , RTRIM( SUBSTR( l_regel, 208, 1))
              , TO_NUMBER( SUBSTR( l_regel, 209, 5))
              , TO_NUMBER( SUBSTR( l_regel, 214, 5))
              , RTRIM( SUBSTR( l_regel, 219, 18))
              , RTRIM( SUBSTR( l_regel, 237, 24))
              , RTRIM( SUBSTR( l_regel, 261, 17))
              , RTRIM( SUBSTR( l_regel, 278, 24))
              , RTRIM( SUBSTR( l_regel, 302, 43))
              , RTRIM( SUBSTR( l_regel, 358, 24))
              , RTRIM( SUBSTR( l_regel, 382, 1))
              , TO_NUMBER( SUBSTR( l_regel, 383, 3))
              );
    END IF;
--
  EXCEPTION
  WHEN NO_DATA_FOUND -- End of file bereikt door Alg_bestand.
  THEN
     alg_bestand.wis( l_file_handle);
     EXIT LEES_REST_MUTATIETAPE;
  WHEN OTHERS
  THEN
     alg_bestand.wis( l_file_handle);
     l_batch_ok := 'N';
     stm_util.debug('exception OTHERS in block LEES_REST_MUTATIETAPE');
     RAISE;
  END;
--
END LOOP LEES_REST_MUTATIETAPE;
--
FOR r_mpe IN c_mpe (l_pbn_id)
LOOP

  l_volgnr := l_volgnr + 1;

  UPDATE rle_mut_postcode
  SET volgnr = l_volgnr
  WHERE CURRENT OF c_mpe;

END LOOP;

EXCEPTION

WHEN NO_DATA_FOUND
THEN
   NULL; -- End of file bereikt door Alg_bestand.
--
WHEN OTHERS
THEN
/*
   stm_batch.log_regel( 'RLE_NPE_PCK.VUL_PCBR'
                      , 'WHENOTHERS'
                      , 'Onverwachte fout');
*/
   RAISE;
END VUL_PCBR;
/* Nieuw voorkomen toevoegen in ned postcode tabel */
FUNCTION NIEUW
 (P_PCD_ROW c_mpe_002%rowtype
 )
 RETURN VARCHAR2
 IS

/* Ophalen straat en wnpl van postcode */
CURSOR C_NPE_002
 (B_POSTCODE VARCHAR2
 ,B_REEKSCOD VARCHAR2
 ,B_NUMSCHEIDINGVAN NUMBER
 ,B_NUMSCHEIDINGTM NUMBER
 ,B_IND_POSTCHAND VARCHAR2
 )
 IS
/* C_NPE_002 */
select *
from rle_ned_postcode
where  postcode          = b_postcode
  and  codreeks          = b_reekscod
  and  numscheidingvan   = b_numscheidingvan
  and  numscheidingtm    = b_numscheidingtm
  and  indpostchand      = b_ind_postchand;

R_NPE_002 C_NPE_002%ROWTYPE;
L_MELDING VARCHAR2(132);
-- RLE_NPE_PCK_NIEUW
BEGIN
OPEN c_npe_002( p_pcd_row.postcode_nw
              , p_pcd_row.reekscod_nw
              , p_pcd_row.numscheidingvan_nw
              , p_pcd_row.numscheidingtm_nw
              , 'J'
              );
FETCH c_npe_002 INTO r_npe_002;
WHILE c_npe_002%FOUND
--
LOOP
  IF ( l_sw_woonpl = 'PTT'   AND
       UPPER( r_npe_002.woonplaats)
                     <> UPPER( p_pcd_row.woonplaats_ptt_nw)
     )
     OR
     ( l_sw_woonpl <> 'PTT'  AND
       UPPER( r_npe_002.woonplaats)
                     <> UPPER( p_pcd_row.woonplaats_nen_nw)
     )
     OR
     ( l_sw_straat = 'PTT'   AND
       UPPER( r_npe_002.straatnaam)
                     <> UPPER( p_pcd_row.straatnaam_ptt_nw)
     )
     OR
     ( l_sw_straat = 'NEN'   AND
       r_npe_002.straatnaam <> p_pcd_row.straatnaam_nen_nw
     )
     OR
     ( l_sw_straat = 'OFF'   AND
       r_npe_002.straatnaam <> p_pcd_row.straatnaam_off_nw
     )
  THEN
     l_melding := 'Handmatige postcode overschreven : '
               || p_pcd_row.postcode_oud||'/'
               || p_pcd_row.reekscod_oud||'/'
               || p_pcd_row.numscheidingvan_oud||'/'
               || p_pcd_row.numscheidingtm_oud
               || ' Woonplaats: '
               || r_npe_002.woonplaats
               || ' Straat: '
               || r_npe_002.straatnaam;
     /*
     stm_batch.log_regel( 'RLE_NPE_PCK.NIEUW'
                        , 'WAARSCHUW'
                        , l_melding);
     */
   STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
  END IF;
--
  DELETE rle_ned_postcode
  WHERE  postcode        = r_npe_002.postcode
  AND    codreeks        = r_npe_002.codreeks
  AND    numscheidingvan = r_npe_002.numscheidingvan
  AND    numscheidingtm  = r_npe_002.numscheidingtm;
--
  FETCH c_npe_002 INTO r_npe_002;
--
END LOOP;
CLOSE c_npe_002;
--
OPEN c_npe_002( p_pcd_row.postcode_nw
              , p_pcd_row.reekscod_nw
              , p_pcd_row.numscheidingvan_nw
              , p_pcd_row.numscheidingtm_nw
              , 'N'
              );
FETCH c_npe_002 INTO r_npe_002;
IF c_npe_002%FOUND
THEN
   CLOSE c_npe_002;
   l_melding := 'Toe te voegen postcode bestaat al : '
             || p_pcd_row.postcode_nw || '/'
             || p_pcd_row.reekscod_nw || '/'
             || p_pcd_row.numscheidingvan_nw || '/'
             || p_pcd_row.numscheidingtm_nw;
   /*
   stm_batch.log_regel( 'RLE_NPE_PCK.NIEUW'
                      , 'WAARSCHUW'
                      , l_melding);
   */
   STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
ELSE
   CLOSE c_npe_002;
   IF l_sw_woonpl = 'PTT'
   THEN
      l_woonplaats := p_pcd_row.woonplaats_ptt_nw;
   ELSE
      l_woonplaats := p_pcd_row.woonplaats_nen_nw;
   END IF;
   IF l_sw_straat = 'PTT'
   THEN
      l_straatnaam  := INITCAP( p_pcd_row.straatnaam_ptt_nw);
   ELSIF l_sw_straat = 'NEN'
   THEN
      l_straatnaam := p_pcd_row.straatnaam_nen_nw;
   ELSE
      l_straatnaam := p_pcd_row.straatnaam_off_nw;
   END IF;
--
   INSERT
   INTO   rle_ned_postcode
          ( postcode
          , codreeks
          , numscheidingvan
          , numscheidingtm
          , codorganisatie
          , indpostchand
          , woonplaats
          , straatnaam
          , gemeentenaam
          , codprovincie
          , codcebuco)
   VALUES ( p_pcd_row.postcode_nw
          , p_pcd_row.reekscod_nw
          , p_pcd_row.numscheidingvan_nw
          , p_pcd_row.numscheidingtm_nw
          , 'A'
          , 'N'
          , l_woonplaats
          , l_straatnaam
          , p_pcd_row.gemeentenaam_nw
          , p_pcd_row.codprov_nw
          , p_pcd_row.codcebuco_nw
          );
--
   rle_m31_npe_mutatie( 'I'
                      , NULL
                      , NULL
                      , NULL
                      , NULL
                      , p_pcd_row.postcode_nw
                      , p_pcd_row.reekscod_nw
                      , p_pcd_row.numscheidingvan_nw
                      , p_pcd_row.numscheidingtm_nw
                      , l_woonplaats
                      , l_straatnaam
                      , NULL
                      , NULL);
   RETURN 'J';
END IF;
RETURN 'N';
END NIEUW;
/* Verwijderen uit ned postcode tabel */
FUNCTION VERWIJDER
 (P_PCD_ROW c_mpe_002%rowtype
 )
 RETURN VARCHAR2
 IS

/* Ophalen straat en wnpl van postcode */
CURSOR C_NPE_002
 (B_POSTCODE VARCHAR2
 ,B_REEKSCOD VARCHAR2
 ,B_NUMSCHEIDINGVAN NUMBER
 ,B_NUMSCHEIDINGTM NUMBER
 ,B_IND_POSTCHAND VARCHAR2
 )
 IS
/* C_NPE_002 */
select *
from rle_ned_postcode
where  postcode          = b_postcode
  and  codreeks          = b_reekscod
  and  numscheidingvan   = b_numscheidingvan
  and  numscheidingtm    = b_numscheidingtm
  and  indpostchand      = b_ind_postchand;

R_NPE_002 C_NPE_002%ROWTYPE;
L_MELDING VARCHAR2(132);
-- RLE_NPE_PCK_VERWIJD
BEGIN
OPEN c_npe_002( p_pcd_row.postcode_oud
              , p_pcd_row.reekscod_oud
              , p_pcd_row.numscheidingvan_oud
              , p_pcd_row.numscheidingtm_oud
              , 'N'
              );
--
FETCH c_npe_002 INTO r_npe_002;
IF c_npe_002%FOUND
THEN
   DELETE
   FROM   rle_ned_postcode
   WHERE  postcode        = r_npe_002.postcode
   AND    codreeks        = r_npe_002.codreeks
   AND    numscheidingvan = r_npe_002.numscheidingvan
   AND    numscheidingtm  = r_npe_002.numscheidingtm;
--
   CLOSE c_npe_002;
   rle_m31_npe_mutatie( 'D'
                      , p_pcd_row.postcode_oud
                      , p_pcd_row.reekscod_oud
                      , p_pcd_row.numscheidingvan_oud
                      , p_pcd_row.numscheidingtm_oud
                      , NULL
                      , NULL
                      , NULL
                      , NULL
                      , NULL
                      , NULL
                      , NULL
                      , NULL
                      );
   RETURN 'J';
END IF;
CLOSE c_npe_002;
--
IF NVL( SUBSTR( p_pcd_row.postcode_oud, 5, 2), '  ') = '  ' OR
      ( NVL( p_pcd_row.numscheidingvan_oud, 0) = '0' AND
        NVL( p_pcd_row.numscheidingtm_oud, 0) = '0'
      )
-- Records met deze vulling (alleen wijkcodes) worden door deze
-- programmatuur niet opgenomen dus hoeven ook niet verwijderd te worden.
THEN
   NULL;
ELSE
   -- melding geven
   l_melding := 'Te verwijderen postcode onbekend : '
             || p_pcd_row.postcode_oud || '/'
             || p_pcd_row.reekscod_oud || '/'
             || p_pcd_row.numscheidingvan_oud || '/'
             || p_pcd_row.numscheidingtm_oud;
   /*
   stm_batch.log_regel( 'RLE_NPE_PCK.VERWIJDR'
                      , 'WAARSCHUW'
                      , l_melding);
   */
   STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
END IF;
RETURN 'N';
END VERWIJDER;
/* Wijzigen in ned postcode tabel */
FUNCTION WIJZIG
 (P_PCD_ROW IN OUT c_mpe_002%rowtype
 )
 RETURN VARCHAR2
 IS

/* Ophalen straat en wnpl van postcode */
CURSOR C_NPE_003
 (B_POSTCODE VARCHAR2
 ,B_REEKSCOD VARCHAR2
 ,B_NUMSCHEIDINGVAN NUMBER
 ,B_NUMSCHEIDINGTM NUMBER
 )
 IS
/* 	C_PCOD03 */
    select ROWID, npe.*
    from rle_ned_postcode npe
    where  npe.postcode like b_postcode||'%'
      and  (npe.codreeks = b_reekscod
      		or b_reekscod is null)
      and  (npe.numscheidingvan   = b_numscheidingvan
      		or b_numscheidingvan is null)
      and  (npe.numscheidingtm    = b_numscheidingtm
      		or b_numscheidingtm is null)
    ;

L_WPS_OUD VARCHAR2(60);
R_NPE_003 C_NPE_003%ROWTYPE;
L_STT_OUD VARCHAR2(60);
L_MELDING VARCHAR2(132);
L_POSTCODE VARCHAR2(15);
L_CODREEKS NUMBER(1);
L_NUMSCHEIDINGVAN NUMBER(5);
L_NUMSCHEIDINGTM NUMBER(5);
L_GEMEENTENAAM VARCHAR2(40);
L_PROVINCIE VARCHAR2(1);
BEGIN
IF p_pcd_row.postcode_oud IS NOT NULL             AND
   ( p_pcd_row.ind_mut_postcode_dl1 = 1         OR
     p_pcd_row.ind_mut_postcode_dl2 = 1         OR
     p_pcd_row.ind_mut_reekscod = 1             OR
     p_pcd_row.ind_mut_numscheidingvan = 1      OR
     p_pcd_row.ind_mut_numscheidingtm  = 1      OR
     ( p_pcd_row.ind_mut_woonplaats_ptt = 1 AND
       l_sw_woonpl = 'PTT'
     )                                          OR
     ( p_pcd_row.ind_mut_woonplaats_nen = 1 AND
       l_sw_woonpl <> 'PTT'
     )                                          OR
     ( p_pcd_row.ind_mut_straatnaam_ptt = 1 AND
       l_sw_straat = 'PTT'
     )                                          OR
     ( p_pcd_row.ind_mut_straatnaam_nen = 1 AND
       l_sw_straat = 'NEN'
     )                                          OR
     ( p_pcd_row.ind_mut_straatnaam_off = 1 AND
       l_sw_straat = 'OFF'
     )                                          OR
     p_pcd_row.ind_mut_gemeentenaam = 1
   )
THEN
   IF p_pcd_row.numscheidingvan_nw = 0
   THEN
      p_pcd_row.numscheidingvan_nw := NULL;
   END IF;
   IF p_pcd_row.numscheidingtm_nw = 0
   THEN
      p_pcd_row.numscheidingtm_nw := NULL;
   END IF;
   IF p_pcd_row.numscheidingvan_oud = 0
   THEN
      p_pcd_row.numscheidingvan_oud := NULL;
   END IF;
   IF p_pcd_row.numscheidingtm_oud = 0
   THEN
      p_pcd_row.numscheidingtm_oud := NULL;
   END IF;
--
   OPEN c_npe_003( p_pcd_row.postcode_oud
                 , p_pcd_row.reekscod_oud
                 , p_pcd_row.numscheidingvan_oud
                 , p_pcd_row.numscheidingtm_oud
                 );
   FETCH c_npe_003 INTO r_npe_003;
   IF c_npe_003%NOTFOUND
   THEN
      CLOSE c_npe_003;
      l_melding := 'Te wijzigen postcode onbekend : '
                || p_pcd_row.postcode_oud || '/'
                || p_pcd_row.reekscod_oud || '/'
                || p_pcd_row.numscheidingvan_oud || '/'
                || p_pcd_row.numscheidingtm_oud;
      /*
      stm_batch.log_regel( 'RLE_NPE_PCK.WIJZIG'
                         , 'WAARSCHUW'
                         , l_melding);
      */
      STM_UTIL.insert_lijsten(lijstnummer,regelnummer,l_melding);
   ELSE
      IF l_sw_woonpl = 'PTT'
      THEN
         l_woonplaats := p_pcd_row.woonplaats_ptt_nw;
         l_wps_oud    := p_pcd_row.woonplaats_ptt_oud;
      ELSE
         l_woonplaats := p_pcd_row.woonplaats_nen_nw;
         l_wps_oud    := p_pcd_row.woonplaats_nen_oud;
      END IF;
      IF l_sw_straat = 'PTT'
      THEN
         l_straatnaam := p_pcd_row.straatnaam_ptt_nw;
         l_stt_oud    := p_pcd_row.straatnaam_ptt_oud;
      ELSIF l_sw_straat = 'NEN'
      THEN
         l_straatnaam := p_pcd_row.straatnaam_nen_nw;
         l_stt_oud    := p_pcd_row.straatnaam_nen_oud;
      ELSE
         l_straatnaam := p_pcd_row.straatnaam_off_nw;
         l_stt_oud    := p_pcd_row.straatnaam_off_oud;
      END IF;
      IF p_pcd_row.postcode_nw <> p_pcd_row.postcode_oud
      THEN
         l_postcode := p_pcd_row.postcode_nw;
      ELSE
         l_postcode := NULL;
      END IF;
      IF p_pcd_row.reekscod_nw <> p_pcd_row.reekscod_oud
      THEN
         l_codreeks := p_pcd_row.reekscod_nw;
      ELSE
         l_codreeks := NULL;
      END IF;
      IF p_pcd_row.numscheidingvan_nw <>
                            p_pcd_row.numscheidingvan_oud
      THEN
         l_numscheidingvan := p_pcd_row.numscheidingvan_nw;
      ELSE
         l_numscheidingvan := NULL;
      END IF;
      IF p_pcd_row.numscheidingtm_nw <>
                           p_pcd_row.numscheidingtm_oud
      THEN
         l_numscheidingtm := p_pcd_row.numscheidingtm_nw;
      ELSE
         l_numscheidingtm := NULL;
      END IF;
      IF p_pcd_row.gemeentenaam_nw <> p_pcd_row.gemeentenaam_oud
      THEN
         l_gemeentenaam   := p_pcd_row.gemeentenaam_nw;
      ELSE
         l_gemeentenaam   := NULL;
      END IF;
      IF p_pcd_row.codprov_nw <> p_pcd_row.codprov_oud
      THEN
         l_provincie := p_pcd_row.codprov_nw;
      ELSE
         l_provincie := NULL;
      END IF;

--
      LOOP
        IF LTRIM( RTRIM( SUBSTR( p_pcd_row.postcode_oud, 5, 2)))
                                                      IS NOT NULL
        THEN
           UPDATE rle_ned_postcode
           SET    postcode        = NVL(l_postcode, postcode)
           ,      codreeks        = NVL(l_codreeks, codreeks)
           ,      numscheidingvan = NVL(l_numscheidingvan, numscheidingvan)
           ,      numscheidingtm  = NVL(l_numscheidingtm, numscheidingtm)
           ,      woonplaats      = NVL(l_woonplaats, woonplaats)
           ,      straatnaam      = NVL(l_straatnaam, straatnaam)
           ,      gemeentenaam    = NVL(l_gemeentenaam, gemeentenaam)
           ,      codprovincie    = NVL(l_provincie, codprovincie)
           WHERE  ROWID = r_npe_003.rowid;
--
           rle_m31_npe_mutatie( 'M'
                              , p_pcd_row.postcode_oud
                              , p_pcd_row.reekscod_oud
                              , p_pcd_row.numscheidingvan_oud
                              , p_pcd_row.numscheidingtm_oud
                              , p_pcd_row.postcode_nw
                              , p_pcd_row.reekscod_nw
                              , p_pcd_row.numscheidingvan_nw
                              , p_pcd_row.numscheidingtm_nw
                              , l_woonplaats
                              , l_straatnaam
                              , l_wps_oud
                              , l_stt_oud
                              );
        ELSE
        -- Alleen straat of woonplaats wijziging
           IF l_stt_oud = r_npe_003.straatnaam  AND
              l_wps_oud = r_npe_003.woonplaats
           THEN
              UPDATE rle_ned_postcode
              SET    woonplaats = NVL(l_woonplaats, woonplaats)
              ,      straatnaam = NVL(l_straatnaam, straatnaam)
              WHERE  ROWID = r_npe_003.rowid;
--
              rle_m31_npe_mutatie( 'M'
                                 , p_pcd_row.postcode_oud
                                 , p_pcd_row.reekscod_oud
                                 , p_pcd_row.numscheidingvan_oud
                                 , p_pcd_row.numscheidingtm_oud
                                 , p_pcd_row.postcode_nw
                                 , p_pcd_row.reekscod_nw
                                 , p_pcd_row.numscheidingvan_nw
                                 , p_pcd_row.numscheidingtm_nw
                                 , l_woonplaats
                                 , l_straatnaam
                                 , l_wps_oud
                                 , l_stt_oud
                                 );
           END IF;
        END IF;
        FETCH c_npe_003 INTO r_npe_003;
        IF c_npe_003%notfound
        THEN
           EXIT;
        END IF;
      END LOOP;
      CLOSE c_npe_003;
--
      RETURN 'J';
   END IF;
END IF;
RETURN 'N';
END WIJZIG;
/* VERWERKEN PTT MUTATIE TAPE */
PROCEDURE VERWERK
 (P_SCRIPTNAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_WEEK_CONTROLE IN VARCHAR2 := 'J'
 )
 IS

T_SQLERRM VARCHAR2(255);
CN_LENGTE_SQLERRM VARCHAR2(255);
T_JOBSTAT_VOLGNUMMER NUMBER(10) := 0;

OTHERS EXCEPTION;
BEGIN
STM_UTIL.t_script_naam    := p_scriptnaam;
  STM_UTIL.t_script_id      := TO_NUMBER(p_script_id);
  STM_UTIL.t_programma_naam := cn_module;
  --stm_util.set_debug_on;
  --
  -- Ptt tape mutaties hoeven niet aan GBA te worden doorgegeven
  --
  RLE_INDICATIE.DOORGEVEN_AAN_GBA :=  'N';
  --
  stm_util.debug('RLE_NPE_PCK.VERWERK');
  rle_npe_pck.mutatie(p_week_controle => p_week_controle);
  STM_UTIL.insert_job_statussen(t_jobstat_volgnummer,'S','0');
stm_util.verwijder_herstart;
RLE_INDICATIE.DOORGEVEN_AAN_GBA :=  'J';
  COMMIT;

EXCEPTION

WHEN OTHERS THEN
  rollback;
t_sqlerrm := SUBSTR( STM_GET_BATCH_MESSAGE ( STM_APP_ERROR( SQLERRM
                                                               , cn_module
                                                               )
                                                )
                         , 1,cn_lengte_sqlerrm);
 STM_UTIL.insert_job_statussen
                  ( t_jobstat_volgnummer
                 , 'S'
                 , '1'
                 );
 STM_UTIL.insert_job_statussen
                  ( t_jobstat_volgnummer
                 , 'I'
                 , 'Foutmelding : '||t_sqlerrm
                 );
 STM_UTIL.insert_job_statussen
                  ( t_jobstat_volgnummer
                 , 'I'
                 , 'Foutmelding : '||sqlerrm
                 );

COMMIT;

END VERWERK;

END RLE_NPE_PCK;
/