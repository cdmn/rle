CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_VUL_BRON_EX IS
-- Wijzigingshistorie
-- Wanneer     Wie               Wat
-- ---------- ----------------- ---------------------------
-- 09-07-2013   Rudie Siwpersad   creatie
-------------------------------------------------------------
-- global t.b.v. logverslag en herstart
  g_lijstnummer       number := 1;
  g_regelnr              number := 0;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
    /**********************************************************************
    Naam:         WEGSCHRIJVEN STM_LIJST
    Beschrijving:
    **********************************************************************/

    cn_lijst constant pls_integer := 1;
begin
  stm_util.insert_lijsten ( cn_lijst
                          , g_regelnr
                          , p_text );
end lijst;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS
    -- cursor voor het selecteren van alle ingelezen data uit de tabel rle_input_ex
  cursor c_res ( b_bestandsnaam varchar2 )
  is
    select bsn_dln
        ,  regeling
        ,  bsn_ex
        ,  a_nummer_ex
        ,  naam_ex
        ,  nvl ( naamgebruik_ex, 'E' ) naamgebruik_ex
        ,  voorvoegsels_ex
        ,  voornamen_ex
        ,  voorletters_ex
        ,  geslacht_ex
        ,  geboortedatum_ex
        ,  overlijdensdatum_ex
        ,  huwelijk_datumbegin
        ,  huwelijk_datumeinde
        ,  reden_einde_huwelijk
        ,  bestandsnaam
        ,  landcode_ex
        ,  straatnaam_ex
        ,  huisnummer_ex
        ,  huisletter_ex
        ,  toevoeging_ex
        ,  postcode_ex
        ,  gemeentenummer_ex
        ,  gemeentedeel_ex
        ,  trunc ( dat_creatie ) datum_aanlevering
    from   rle_input_ex_partners
    where  foutmelding is null
    and    upper (bestandsnaam) = upper (b_bestandsnaam)
    order by 1;
  -- cursor voor het ophalen van deelnemergegevens
  cursor c_data ( b_bsn varchar2 )
  is
    select persoonsnummer
        ,  relatienummer
        ,  naam
        ,  voorletters
        ,  voorvoegsels
        ,  substr (geslacht,1,1) geslacht
        ,  geboortedatum
        ,  datum_overlijden
    from   rle_v_personen
    where  sofinummer = b_bsn;
  -- cursor voor het ophalen van a_nummer deelnemer
  cursor c_a_nr ( b_relatienummer number )
  is
    select rec.extern_relatie
    from   rle_relatie_externe_codes rec
    where  rec.dwe_wrddom_extsys = 'GBA'
    and    rec.rle_numrelatie = b_relatienummer;
  -- cursor voor het controleren van de echtscheiding bekend in RLE
  cursor c_ex ( b_relatienummer rle_relaties.numrelatie%type
              , b_dat_begin rle_relatie_koppelingen.dat_begin%type
              , b_dat_einde rle_relatie_koppelingen.dat_einde%type
              )
  is
    select rkn.rle_numrelatie_voor
    from   rle_relatie_koppelingen rkn
    where  rkn.rle_numrelatie = b_relatienummer
    and    rkn.dat_begin = nvl(b_dat_begin, rkn.dat_begin)
    and    rkn.dat_einde = b_dat_einde
    ;
  --
   r_res                     c_res%rowtype;
  r_data                    c_data%rowtype;
  r_a_nr                    c_a_nr%rowtype;
  r_ex                      c_ex%rowtype;
  --
  l_volgnummer              stm_job_statussen.volgnummer%type := 0;
  --
  cn_module  constant       varchar2 ( 25 ) := 'rle_vul_bron_ex';
  -- variabel voor opgehaalde data uit de tabel rle_input_ex_partners
  l_bsn_dln                 varchar2 ( 9 );
  l_bsn_chk_dubbel          varchar2 ( 9 ) := '0';
  l_bsn_gbav                varchar2 ( 9 );
  l_regeling                varchar2 ( 5 );
  l_adm_code                varchar2 ( 10 );
  l_bsn_ex                  varchar2 ( 9 );
  l_anummer_ex              varchar2 ( 30 );
  l_relatienummer_ex        number;
  l_persoonsnummer_ex       number;
  l_naam_ex                 varchar2 ( 120 );
  l_naamgebruik_ex          varchar2 ( 1 );
  l_voorvoegsels_ex         varchar2 ( 30 );
  l_voornamen_ex            varchar2 ( 50 );
  l_voorletters_ex          varchar2 ( 30 );
  l_geslacht_ex             varchar2 ( 1 );
  l_geboortedatum_ex        date;
  l_overlijdensdatum_ex     date;
  l_landcode_ex             varchar2 ( 5 );
  l_straat_ex               varchar2 ( 30 );
  l_huisnummer_ex           varchar2 ( 30 );
  l_huisletter_ex           varchar2 ( 30 );
  l_toevoeging_ex           varchar2 ( 30 );
  l_postcode_ex             varchar2 ( 30 );
  l_gemeentenummer_ex       varchar2 ( 30 );
  l_gemeentedeel_ex         varchar2 ( 30 );
  l_huw_datumbegin          date;
  l_huw_datumeinde          date;
  l_bestandsnaam            varchar2 ( 50 );
  -- variablen voor het aanvullen van de brondata
  -- van de deelnemer
  l_relatienummer           number ( 9 );
  l_persoonsnummer          varchar2 ( 30 );
  l_anummer                 varchar2 ( 30 );
  l_naam                    varchar2 ( 120 );
  l_voorvoegsels            varchar2 ( 30 );
  l_voorletters             varchar2 ( 30 );
  l_geslacht                varchar2 ( 1 );
  l_geboortedatum           date;
  l_overlijdensdatum        date;
  l_fout                    varchar2 ( 4000 );
  l_ind_naar_gbav           varchar2( 1 ) := 'N';
  --
  l_aantal_naar_bron         number := 0;
  l_aantal_input             number := 0;
  l_aantal_bekend            number := 0;
  l_aantal_naar_gbav         number := 0;
  l_niet_bekend              number := 0;
  l_database                 varchar2 ( 255 );
  --
  fh_gbav                    utl_file.file_type;
  l_gbavbatch                varchar2 (13):= 'GBAVBATCHBSN';
  l_dat_gbabatch_dln         date;
  l_ind_scheiding_al_bekend  varchar2 ( 1 ) := 'N';
  l_ind_verwerkt             varchar2(1) := 'N';
  l_aantal_bestanden_gbav    number := 1;
  l_teller_fout              number := 0;
  --
  t_gbavbatch                t_gbav_tab;
  t_fout                     t_fout_tab;

  l_aantal              number := 0;
  e_fout_bestand        exception;
begin
  stm_util.t_procedure := cn_module;
  stm_util.t_script_naam := p_script_naam;
  stm_util.t_script_id := p_script_id;
  stm_util.t_programma_naam := 'RLE_vul_bron_EX';
  --
  select case
           when regexp_substr ( global_name, '[^\.]*' ) = 'PPVD' then null
           else regexp_substr ( global_name, '[^\.]*' ) || '_'
         end
  into   l_database
  from   global_name;
  --
  stm_util.debug ( 'Logbestand wordt aangemaakt' );
  -- Schrijven logbestand
  lijst (rpad ( '***** LOGVERSLAG RLE_AANVULLEN_BRON_EX_PARTNERS*****', 70 )
            || 'DRAAIDATUM: ' || to_char ( sysdate, 'dd-mm-yyyy hh24:mi:ss' ));
  lijst ( ' ' );
  lijst ( rpad('-', 85, '-') );
  lijst ( ' ' );
  --
  execute immediate 'alter session set nls_date_format="dd-mm-yyyy"';
  --
  lijst ( 'Selectie echtscheidingen inputtabel o.b.v. inputbestand: '||p_bestandsnaam );
  lijst ( '' );
  lijst ( rpad('=',85, '=') );
  lijst ( '' );
  --
  select count(*)
  into l_aantal
  from rle_input_ex_partners
  where bestandsnaam = p_bestandsnaam;

  if l_aantal = 0
  then
    raise e_fout_bestand;
  end if;

  for r_res in c_res ( p_bestandsnaam )
  loop
    begin
      -- mogelijke open cursors sluiten
      if c_data%isopen
      then
        close c_data;
      end if;
      --
       if c_a_nr%isopen
      then
        close c_a_nr;
      end if;
      --
      --
      if c_ex%isopen
      then
        close c_ex;
      end if;

      l_aantal_input := l_aantal_input + 1;
      -- initialiseren variabelen voor aangeleverende data
      l_regeling            := null;
      l_bsn_dln             := null;
      l_bsn_ex              := null;
      l_anummer_ex          := null;
      l_relatienummer_ex    := null;
      l_persoonsnummer_ex   := null;
      l_naam_ex             := null;
      l_naamgebruik_ex      := null;
      l_voorvoegsels_ex     := null;
      l_voornamen_ex        := null;
      l_voorletters_ex      := null;
      l_geslacht_ex         := null;
      l_geboortedatum_ex    := null;
      l_overlijdensdatum_ex := null;
      l_landcode_ex         := null;
      l_straat_ex           := null;
      l_huisnummer_ex       := null;
      l_huisletter_ex       := null;
      l_toevoeging_ex       := null;
      l_postcode_ex         := null;
      l_gemeentenummer_ex   := null;
      l_gemeentedeel_ex     := null;
      l_huw_datumbegin      := null;
      l_huw_datumeinde      := null;
      l_bestandsnaam        := null;
      --
      l_regeling            := r_res.regeling;
      l_bsn_dln             := r_res.bsn_dln;
      l_bsn_ex              := r_res.bsn_ex;
      l_anummer_ex          := r_res.a_nummer_ex;
      l_naam_ex             := r_res.naam_ex;
      l_naamgebruik_ex      := r_res.naamgebruik_ex;
      l_voorvoegsels_ex     := r_res.voorvoegsels_ex;
      l_voornamen_ex        := substr (r_res.voornamen_ex, 1, 50);
      l_geslacht_ex         := r_res.geslacht_ex;
      l_geboortedatum_ex    := to_date (r_res.geboortedatum_ex, 'yyyymmdd');
      l_overlijdensdatum_ex := to_date (r_res.overlijdensdatum_ex, 'yyyymmdd');
      l_landcode_ex         := r_res.landcode_ex;
      l_straat_ex           := r_res.straatnaam_ex;
      l_huisnummer_ex       := r_res.huisnummer_ex;
      l_huisletter_ex       := r_res.huisletter_ex;
      l_toevoeging_ex       := r_res.toevoeging_ex;
      l_postcode_ex         := r_res.postcode_ex;
      l_gemeentenummer_ex   := r_res.gemeentenummer_ex;
      l_gemeentedeel_ex     := r_res.gemeentedeel_ex;
      l_huw_datumbegin      := to_date (r_res.huwelijk_datumbegin, 'yyyymmdd');
      l_huw_datumeinde      := to_date (r_res.huwelijk_datumeinde, 'yyyymmdd');
      l_bestandsnaam        := r_res.bestandsnaam;
      -- initialiseren aanvullende variabelen deelnemer
      l_relatienummer       := null;
      l_persoonsnummer      := null;
      l_anummer             := null;
      l_naam                := null;
      l_voorvoegsels        := null;
      l_voorletters         := null;
      l_geslacht            := null;
      l_geboortedatum       := null;
      l_overlijdensdatum    := null;
      l_fout                := null;

      -- bsn met spaties voor GBAVBATCHBSN
      l_bsn_gbav  := l_bsn_dln;
      if length ( l_bsn_gbav ) <= 9
      then
        l_bsn_gbav := lpad ( substr ( l_bsn_gbav, 1 ), 9,  ' ' );
      end if;
      --
      -- voorloopnullen plaatsen bij bsn voor het zoeken van de relatie
      if length ( l_bsn_dln ) <= 9
      then
        l_bsn_dln        := lpad ( substr ( l_bsn_dln, 1 ), 9,  '0' );
      end if;
      --
      open c_data ( l_bsn_dln );
      --
      fetch c_data
      into  r_data;
      --
      if c_data%found
      then
      -- aanvullen gegevens deelnemer:
      -- 1. Bepaal het relatienummer persoonsnummer van de deelnemer met het burgerservicenummer
      -- 2. Neem uit de persoonsgegevens de volgende gegevens over in bron:
      --    A-nummer,Naam, Voorletters, Voorvoegsels, Geslacht, Geboortedatum
      --    en  Overlijdensdatum
        l_relatienummer    := r_data.relatienummer;
        l_persoonsnummer   := r_data.persoonsnummer;
        l_naam             := r_data.naam;
        l_voorvoegsels     := r_data.voorvoegsels;
        l_voorletters      := r_data.voorletters;
        l_geslacht         := r_data.geslacht;
        l_geboortedatum    := r_data.geboortedatum;
        l_overlijdensdatum := r_data.datum_overlijden;
        --
        --ophalen a-nummer van gevonden relatie
        open c_a_nr ( l_relatienummer );
        fetch c_a_nr
        into   r_a_nr;
        if c_a_nr%found
        then
          l_anummer := r_a_nr.extern_relatie;
        end if;
        --
        -- Indien voorletters niet gevuld, bepaal deze met behulp van de functie COMP_GBA.BEPAAL_VOORLETTERS.
        if r_res.voorletters_ex is not null
        then
          l_voorletters_ex := r_res.voorletters_ex;
        else
          l_voorletters_ex := comp_gba.bepaal_voorletters ( p_tekst => r_res.voornamen_ex );
        end if;
        -- controle echtscheiding bekend in rle_relatie_koppeling
        if l_huw_datumeinde is not null
        then
          open c_ex ( l_relatienummer
                    , l_huw_datumbegin
                    , l_huw_datumeinde
                    )
                    ;
          fetch  c_ex into r_ex;
          if c_ex%found
          then

            l_aantal_bekend           := l_aantal_bekend +1;
            l_ind_scheiding_al_bekend := 'J';
            l_ind_verwerkt            := 'J';
            l_relatienummer_ex        := r_ex.rle_numrelatie_voor;
            l_persoonsnummer_ex       := rle_lib.get_persoonsnr_relatie (r_ex.rle_numrelatie_voor);
            l_dat_gbabatch_dln        := null;

            begin

              select pdt.adm_code
              into l_adm_code
              from pdt_v_regelingen pdt
                  ,rle_relatierollen_in_adm ria
              where ria.adm_code = pdt.adm_code
              and   ria.rle_numrelatie = l_relatienummer_ex
              and   pdt.rgg_code = l_regeling;

            exception
              when no_data_found
                then
                  select pdt.adm_code
                  into l_adm_code
                  from pdt_v_regelingen pdt
                  where pdt.rgg_code = l_regeling;

                  insert into rle_relatierollen_in_adm ( adm_code
                                                       , rol_codrol
                                                       , rle_numrelatie
                                                       )
                                          values ( l_adm_code
                                                 , 'PR'
                                                 , l_relatienummer_ex
                                                 );
            end;
          else

            l_dat_gbabatch_dln        := sysdate;
            l_ind_scheiding_al_bekend := 'N';
            l_ind_naar_gbav           := 'J';
            l_ind_verwerkt            := 'N';
            l_niet_bekend             := l_niet_bekend + 1;

          end if;

        end if;
        --
      else
        --
        l_fout           := 'Deelnemer niet gevonden in RLE';
        --
      end if;
      -- bijhouden aantal opgehaalde records
      l_aantal_naar_bron := l_aantal_naar_bron + 1;
      -- record toevoegen in rle_bron ex_partner
      insert into rle_bron_ex_partners ( regeling
                                       ,  bsn
                                       ,  relatienummer
                                       ,  persoonsnummer
                                       ,  naam
                                       ,  voorvoegsels
                                       ,  voorletters
                                       ,  geslacht
                                       ,  geboortedatum
                                       ,  overlijdensdatum
                                       ,  bsn_ex
                                       ,  a_nummer_ex
                                       ,  persoonsnummer_ex
                                       ,  relatienummer_ex
                                       ,  naam_ex
                                       ,  naamgebruik_ex
                                       ,  voorvoegsels_ex
                                       ,  voornamen_ex
                                       ,  voorletters_ex
                                       ,  geslacht_ex
                                       ,  geboortedatum_ex
                                       ,  overlijdensdatum_ex
                                       ,  landcode_ex
                                       ,  straatnaam_ex
                                       ,  huisnummer_ex
                                       ,  huisletter_ex
                                       ,  toevoeging_ex
                                       ,  postcode_ex
                                       ,  gemeentenummer_ex
                                       ,  gemeentedeel_ex
                                       ,  huwelijk_datumbegin
                                       ,  huwelijk_datumeinde
                                       ,  bestandsnaam
                                       ,  ind_verwerkt
                                       ,  datum_opgenomen_gbabatch_dln
                                       ,  ind_scheiding_al_bekend_rle
                                        )
                                 values ( l_regeling
                                       ,  l_bsn_dln
                                       ,  l_relatienummer
                                       ,  l_persoonsnummer
                                       ,  l_naam
                                       ,  l_voorvoegsels
                                       ,  l_voorletters
                                       ,  l_geslacht
                                       ,  l_geboortedatum
                                       ,  l_overlijdensdatum
                                       ,  l_bsn_ex
                                       ,  l_anummer_ex
                                       ,  l_persoonsnummer_ex
                                       ,  l_relatienummer_ex
                                       ,  l_naam_ex
                                       ,  l_naamgebruik_ex
                                       ,  l_voorvoegsels_ex
                                       ,  l_voornamen_ex
                                       ,  l_voorletters_ex
                                       ,  l_geslacht_ex
                                       ,  l_geboortedatum_ex
                                       ,  l_overlijdensdatum_ex
                                       ,  l_landcode_ex
                                       ,  l_straat_ex
                                       ,  l_huisnummer_ex
                                       ,  l_huisletter_ex
                                       ,  l_toevoeging_ex
                                       ,  l_postcode_ex
                                       ,  l_gemeentenummer_ex
                                       ,  l_gemeentedeel_ex
                                       ,  l_huw_datumbegin
                                       ,  l_huw_datumeinde
                                       ,  l_bestandsnaam
                                       ,  l_ind_verwerkt
                                       ,  l_dat_gbabatch_dln
                                       ,  l_ind_scheiding_al_bekend
                                       );

      -- Indien de echtscheiding niet gevonden wordt, moeten de huwelijkse gegevens van de deelnemer worden opgevraagd bij het GBAV.
      -- Plaats het bsn van de deelnemer in het bestand GBAVBATCHBSN

        if l_bsn_chk_dubbel <> l_bsn_gbav
        then
          if l_ind_naar_gbav = 'J'
          then
            if mod (l_aantal_naar_gbav, 25000) = 0
            then
              fh_gbav  := utl_file.fopen ( 'RLE_OUTBOX'
                                         ,  l_gbavbatch||'_'||l_aantal_bestanden_gbav
                                         ,  'W'
                                         );
              t_gbavbatch (l_aantal_bestanden_gbav).naam :=  l_gbavbatch||'_'||l_aantal_bestanden_gbav;

            end if;

            -- bsn wegschrijven naar GBAVBATCHBSN
            utl_file.put_line ( fh_gbav,l_bsn_gbav);
            -- voor het voorkomen van dat een BSN meerdere keren
            -- in het bestand wordt opgenomen wordt in een variable
            -- het weggeschreven BSN bijgehouden
            l_bsn_chk_dubbel:= l_bsn_gbav;
            -- teller bijwerken
            l_aantal_naar_gbav := l_aantal_naar_gbav + 1;
            -- filehandle(s) sluiten bij 25000
            if mod (l_aantal_naar_gbav, 25000) = 0
            then
              utl_file.fclose_all;
              t_gbavbatch (l_aantal_bestanden_gbav).aantal := l_aantal_naar_gbav;
              l_aantal_bestanden_gbav := l_aantal_bestanden_gbav + 1;
              l_aantal_naar_gbav := 0;
            end if;
            --
          end if;
         --
        end if;
        --
      exception
      when others
      then
        -- melding loggen en daarna doorgaan met de volgende relatie
        l_teller_fout := l_teller_fout + 1;
        t_fout (l_teller_fout).bsn := l_bsn_dln;
        t_fout (l_teller_fout).dateinde_huwelijk := l_huw_datumeinde;
        t_fout (l_teller_fout).fout := substr(sqlerrm,1,350);

      end;
    --
  end loop;
  -- alle filehandle(s) sluiten
  utl_file.fclose_all;
  --
  if l_aantal_naar_gbav <> 0
  then
    t_gbavbatch(l_aantal_bestanden_gbav).aantal := l_aantal_naar_gbav;
  end if;
  --

  lijst ('INPUT:');
  lijst ('Aantal door te zetten echtscheidingen in inputtabel: '||l_aantal_input);
  lijst ('');

  lijst ('OUTPUT:');
  lijst ( 'Aantal echtscheidingen opgenomen in brontabel: '||to_char(l_aantal_naar_bron));
  lijst ('');

  lijst ('UITSPLITSING OUTPUT: ');
  lijst ('Aantal opgenomen echtscheidingen die al bekend zijn in RLE: '||l_aantal_bekend);
  lijst ('Aantal opgenomen echtscheidingen die nog niet bekend zijn in RLE: '||l_niet_bekend);--to_char(l_aantal_naar_gbav));
  lijst ('');

  lijst ('De echtscheidingen die nog niet bekend zijn in RLE moeten bij het GBA worden gecontroleerd');
  lijst ('(o.b.v. BSN van de deelnemer).');
  lijst ('');

  for i in t_gbavbatch.first .. t_gbavbatch.last loop

    lijst ('Aantal unieke BSN''s opgenomen in GBAV-batch: '||t_gbavbatch(i).aantal||' in bestand: '||t_gbavbatch(i).naam||' (RLEdata/inbox.)');

  end loop;

  lijst(' ');

  if t_fout.count > 0
  then

     lijst ('De volgende echtscheidingen konden niet worden verwerkt vanwege een technische fout (aantal: '||l_teller_fout||').');

    for i in t_fout.first .. t_fout.last loop

      lijst ('Deelnemer BSN: '||t_fout(i).bsn||' _ Einddatum huwelijk: '||t_fout (i).dateinde_huwelijk||' _ Foutmelding:');
      lijst (t_fout (i).fout);

    end loop;

     lijst ('');

  end if;

  --  Voettekst van lijsten
  lijst ( '********** EINDE VERSLAG AANVULLEN BRON EX_PARNTERS  **********');
  lijst ('Eindtijd: ' || to_char ( sysdate, 'ddmmyyyy hh24:mi:ss' ));
  stm_util.debug ('Einde logverslag');
  --
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  --
  commit;
  --
exception
  when e_fout_bestand
  then
    rollback;
    stm_util.insert_job_statussen(l_volgnummer
                                ,'S'
                                ,'1'
                                 );
    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                  ' procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  'De opgegeven bestandsnaam ('||p_bestandsnaam||') is onbekend.');
  commit;
  when others
  then
    rollback;
    stm_util.t_foutmelding := sqlerrm;
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(l_volgnummer, 'S', '1');
    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                  ' procedure : ' || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null then
      stm_util.insert_job_statussen(l_volgnummer,
                                    'I',
                                    stm_util.t_foutmelding);
    end if;
    --
    if stm_util.t_tekst is not null then
      stm_util.insert_job_statussen(l_volgnummer, 'I', stm_util.t_tekst);
    end if;

end verwerk;


END RLE_VUL_BRON_EX;
/