CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_LIB IS
cn_rol_werkgever constant varchar2(3) := 'WG';
cn_rol_persoon   constant varchar2(3) := 'PR';
cn_extcod_werkgever constant varchar2(4) := 'WGR';
cn_extcod_persoon constant varchar2(4) := 'PRS';


type t_prs_rec is record
  ( persoonsnummer pls_integer
  , geboortedatum  date
  , overlijdensdatum date
  , geslacht varchar2(1)
  , gba_status varchar2(2)
  , ind_gemoedsbezwaard varchar2(1)
  );
type t_prs_tab is table of t_prs_rec index by binary_integer;

l_prs_tab t_prs_tab;


cursor c_rle (b_extern_nummer in varchar2
             ,b_rol in varchar2
             ,b_externe_code in varchar2
             )
is
select REC.RLE_NUMRELATIE
from rle_relatie_externe_codes rec
where rec.rol_codrol = b_rol
and rec.dwe_wrddom_extsys = b_externe_code
and REC.EXTERN_RELATIE = b_extern_nummer
and rec.dateinde is null;
--
cursor c_rec (b_relatienummer in pls_integer
             ,b_rol in varchar2
             ,b_externe_code in varchar2
             )
is
select REC.EXTERN_RELATIE
from rle_relatie_externe_codes rec
where rec.rol_codrol = b_rol
and rec.dwe_wrddom_extsys = b_externe_code
and REC.RLE_NUMRELATIE = b_relatienummer
and rec.dateinde is null;
--

FUNCTION GET_RLE_UIT_DATABASE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN T_PRS_REC;


FUNCTION GET_RELATIENR_WERKGEVER
 (P_WERKGEVERNUMMER IN varchar2
 )
 RETURN PLS_INTEGER
 IS

  l_relatienummer pls_integer;
begin
  --
  open c_rle(lpad(p_werkgevernummer, 6, '0')
            ,cn_rol_werkgever
            ,cn_extcod_werkgever
            );
  fetch c_rle into l_relatienummer;
  if c_rle%notfound
  then
     l_relatienummer := null;
  end if;
  close c_rle;
  --
  return l_relatienummer;

end get_relatienr_werkgever;
FUNCTION GET_RELATIENR_PERSOON
 (P_PERSOONSNUMMER IN pls_integer
 )
 RETURN PLS_INTEGER
 IS

  l_relatienummer pls_integer;
begin
  --
  open c_rle(to_char(p_persoonsnummer)  -- externe code is varchar2, dus even to_char ivm index-gebruik
            ,cn_rol_persoon
            ,cn_extcod_persoon
            );
  fetch c_rle into l_relatienummer;
  if c_rle%notfound
  then
     l_relatienummer := null;
  end if;
  close c_rle;
  --
  return l_relatienummer;
  --
end get_relatienr_persoon;
FUNCTION GET_WERKGEVERNR_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2
 IS

  l_werkgevernummer varchar2(6);
begin
  --
  open c_rec(p_relatienummer
            ,cn_rol_werkgever
            ,cn_extcod_werkgever
            );
  fetch c_rec into l_werkgevernummer;
  if c_rec%notfound
  then
     l_werkgevernummer := null;
  end if;
  close c_rec;
  --
  return l_werkgevernummer;
  --
end get_werkgevernr_relatie;
FUNCTION GET_PERSOONSNR_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN PLS_INTEGER
 IS

--
l_persoonsummer pls_integer;
begin
  --
  open c_rec(p_relatienummer
            ,cn_rol_persoon
            ,cn_extcod_persoon
            );
  fetch c_rec into l_persoonsummer;
  if c_rec%notfound
  then
     l_persoonsummer := null;
  end if;
  close c_rec;
  --
  return l_persoonsummer;
  --
end get_persoonsnr_relatie;
FUNCTION GET_RLE_UIT_DATABASE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN T_PRS_REC
 IS
  cursor c_rle (b_numrelatie in pls_integer)
  is
  select rec.extern_relatie  persoonsnummer
       , rle.datgeboorte     geboortedatum
       , rle.datoverlijden   overlijdensdatum
       , rle.dwe_wrddom_geslacht geslacht
       , asa.code_gba_status gba_status
    from rle_relatie_externe_codes rec
       , rle_afstemmingen_gba asa
       , rle_relaties rle
   where rle.numrelatie        = rec.rle_numrelatie
     and rec.dwe_wrddom_extsys = cn_extcod_persoon
     and rec.rol_codrol        = cn_rol_persoon
     and rle.numrelatie        = b_numrelatie
     and asa.rle_numrelatie    = rle.numrelatie
       ;
  --
  l_ind_bezwaard  varchar2(2);
  l_ing_dat_bzw   date;
  l_eind_dat_bzw  date;
  l_dummy_varchar varchar2(100);
  l_dummy_date date;
  --
  r_rle c_rle%rowtype;
  e_geen_data exception;
begin
  open c_rle (p_relatienummer);
  fetch c_rle into r_rle;
  if c_rle%found
  then
    l_prs_tab(p_relatienummer).persoonsnummer   := r_rle.persoonsnummer;
    l_prs_tab(p_relatienummer).geboortedatum    := r_rle.geboortedatum;
    l_prs_tab(p_relatienummer).overlijdensdatum := r_rle.overlijdensdatum;
    l_prs_tab(p_relatienummer).geslacht         := r_rle.geslacht;
    l_prs_tab(p_relatienummer).gba_status       := r_rle.gba_status;
    --
    rle_get_bezwaard(p_relatienummer
                    ,l_dummy_date
                    ,l_ind_bezwaard
                    ,l_ing_dat_bzw
                    ,l_eind_dat_bzw
                    ,l_dummy_varchar
                    ,l_dummy_date
                    );
    if l_eind_dat_bzw is null
    then
       l_prs_tab(p_relatienummer).ind_gemoedsbezwaard := l_ind_bezwaard;
    else
       l_prs_tab(p_relatienummer).ind_gemoedsbezwaard := 'N';  -- niet meer gemoedsbezwaard als einddatum gevuld
    end if;
    --
    return l_prs_tab(p_relatienummer);
  else
    raise e_geen_data;
  end if;
  close c_rle;
exception
  when e_geen_data
  then
    raise_application_error(-20000, 'Persoonsnummer, geboortedatum, overlijdensdatum, geslacht niet gevonden voor relatie '||p_relatienummer);
  when others
  then
    raise_application_error(-20000, 'Onverwachte fout opgetreden');
end;
FUNCTION GET_GEBOORTEDATUM_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN DATE
 IS
begin
  return l_prs_tab(p_relatienummer).geboortedatum;
exception
  when no_data_found
  then
    -- geen rij voor p_relatienummer in l_prs_tab, nu gegevens uit database ophalen
    l_prs_tab(p_relatienummer) := get_rle_uit_database(p_relatienummer);
    --
    return l_prs_tab(p_relatienummer).geboortedatum;
end get_geboortedatum_relatie;
FUNCTION GET_OVERLIJDENSDATUM_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN DATE
 IS
begin
  return l_prs_tab(p_relatienummer).overlijdensdatum;
exception
  when no_data_found
  then
    -- geen rij voor p_relatienummer in l_prs_tab, nu gegevens uit database ophalen
    l_prs_tab(p_relatienummer) := get_rle_uit_database(p_relatienummer);
    --
    return l_prs_tab(p_relatienummer).overlijdensdatum;
end get_overlijdensdatum_relatie;
/* Ophalen geslacht bij een relatie */
FUNCTION GET_GESLACHT_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2
 IS
begin
  return l_prs_tab(p_relatienummer).geslacht;
exception
  when no_data_found
  then
    -- geen rij voor p_relatienummer in l_prs_tab, nu gegevens uit database ophalen
    l_prs_tab(p_relatienummer) := get_rle_uit_database(p_relatienummer);
    --
    return l_prs_tab(p_relatienummer).geslacht;
end get_geslacht_relatie;
FUNCTION GET_GEMOEDSBEZWAARD
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2
 IS
begin
  return l_prs_tab(p_relatienummer).ind_gemoedsbezwaard;
exception
  when no_data_found
  then
    -- geen rij voor p_relatienummer in l_prs_tab, nu gegevens uit database ophalen
    l_prs_tab(p_relatienummer) := get_rle_uit_database(p_relatienummer);
    --
    return l_prs_tab(p_relatienummer).ind_gemoedsbezwaard;
end get_gemoedsbezwaard;
FUNCTION GET_GBA_STATUS_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2
 IS

 l_ophalen_uit_db  boolean;
 return_value varchar2(2);
begin
  return l_prs_tab(p_relatienummer).gba_status;
exception
  when no_data_found
  then
    -- geen rij voor p_relatienummer in l_prs_tab, nu gegevens uit database ophalen
    l_prs_tab(p_relatienummer) := get_rle_uit_database(p_relatienummer);
    --
    return l_prs_tab(p_relatienummer).gba_status;
end get_gba_status_relatie;
FUNCTION IS_COURANT_FNR
 (P_RLE_NUMRELATIE IN PLS_INTEGER
 ,P_ADM_CODE IN VARCHAR2
 ,P_IBAN IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
  cursor c_fnr
  (b_rle_numrelatie pls_integer
  ,b_adm_code       varchar2
  ,b_iban           varchar2
  )
  is
  select *
  from   rle_financieel_nummers  fnr
  where  fnr.rle_numrelatie    = b_rle_numrelatie
  and    fnr.adm_code          = b_adm_code
  and    fnr.iban              = b_iban
  and    fnr.dateinde is null
  ;
  r_fnr c_fnr%rowtype;
  l_found boolean;
begin
  open c_fnr( b_rle_numrelatie => p_rle_numrelatie
            , b_adm_code       => p_adm_code
            , b_iban           => p_iban);

  fetch c_fnr into r_fnr;
  l_found := c_fnr%found;
  close c_fnr;

  if l_found
  then
    return 'J';
  else
    return 'N';
  end if;

end;
PROCEDURE HAAL_CONTACTGEGEVENS
 (P_RLE_NUMRELATIE IN rle_relaties.numrelatie%type
 ,P_CONTACTGEG_TAB OUT rle_lib.t_contactgeg_tab
 )
 IS

cursor c_cnr (b_rle_numrelatie rle_relaties.numrelatie%type)
is
  select case
        when dwe_wrddom_srtcom = 'EMAIL'
         then cnr.numcommunicatie || ' Authentiek: ' ||
            case
        when cnr.authentiek = 'J'
        then 'Ja'
        else 'Nee'
        end
         else cnr.numcommunicatie
        end
        numcommunicatie
       , cnr.dwe_wrddom_srtcom
       , cnr.adm_code
  from rle_communicatie_nummers cnr
  where cnr.rol_codrol = 'PR'
  and   cnr.dwe_wrddom_srtcom in ('EMAIL', 'BTEL', 'TEL', 'MOBIEL')
  and   cnr.datingang <= sysdate
  and   nvl(cnr.dateinde, sysdate) >= sysdate
  and   cnr.rle_numrelatie = b_rle_numrelatie
  and   cnr.adm_code in ('PMT', 'PME', 'BPFK')
  order by cnr.adm_code
;
l_index      integer := 0;
begin

  for r_cnr in c_cnr (b_rle_numrelatie => p_rle_numrelatie)
  loop

    l_index := l_index + 1;

    p_contactgeg_tab(l_index).administratie := r_cnr.adm_code;
    p_contactgeg_tab(l_index).soort_contact := r_cnr.dwe_wrddom_srtcom;
    p_contactgeg_tab(l_index).nummer := r_cnr.numcommunicatie;

  end loop;

end haal_contactgegevens;
end rle_lib;
/