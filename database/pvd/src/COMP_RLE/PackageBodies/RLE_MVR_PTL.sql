CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_MVR_PTL IS
   g_lijstnr                 number := 1;
   g_regelnr                 number;
   g_volgnr                  number;

/* Procedure voor uitvoeren execute immediate statement */
PROCEDURE DO_DDL
 (P_DDL_STATEMENT IN VARCHAR2
 );
/* Procedure voor het refreshen van een materialized view */
PROCEDURE REFRESH_MV
 (P_MV_NAME IN VARCHAR2
 );
/* Procedure voor het zetten van de status unusable bij indexen */
PROCEDURE INVALIDATE_INDEXES
 (P_MV_NAME IN VARCHAR2
 );
/* Procedure voor het rebuilden van indexen */
PROCEDURE REBUILD_INDEXES
 (P_MV_NAME IN VARCHAR2
 );
/* Procedure voor het bijwerken van table statistics */
PROCEDURE GATHER_TABLE_STATISTICS
 (P_MV_NAME IN VARCHAR2
 );
/* Functie voor het bepalen van de in gebruik zijnde materialized view */
FUNCTION GET_CURRENT_MV_NAME
 (P_SYNONYM_NAME IN varchar2
 )
 RETURN VARCHAR2;
/* Verwerk procedure t.b.v. verwerking per materialized view */
PROCEDURE VERWERK_MV
 (P_SYNONYM_NAME IN varchar2
 );


/* Procedure voor uitvoeren execute immediate statement */
PROCEDURE DO_DDL
 (P_DDL_STATEMENT IN VARCHAR2
 )
 IS
   cn_module           constant varchar2(50) := 'RLE_MVR_PTL.DO_DDL';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, ' ');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Starten statement :');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, substr(p_ddl_statement, 1, 500));
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Starttijd : '||to_char(sysdate, 'DD-MM-YYYY HH24:MI:SS'));
   commit;
   --
   execute immediate (p_ddl_statement);
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Eindtijd  : '||to_char(sysdate, 'DD-MM-YYYY HH24:MI:SS'));
   --
   stm_util.t_procedure := l_vorige_procedure;
end do_ddl;
/* Procedure voor het refreshen van een materialized view */
PROCEDURE REFRESH_MV
 (P_MV_NAME IN VARCHAR2
 )
 IS
   cn_module           constant varchar2(50) := 'RLE_MVR_PTL.REFRESH_MV';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   do_ddl ( p_ddl_statement => 'begin dbms_mview.refresh ( list => ''COMP_RLE.'|| p_mv_name ||''', method => ''C'', atomic_refresh => FALSE); end;'); -- atomic_refresh = false : Truncate wordt uitgevoerd
   --
   stm_util.t_procedure := l_vorige_procedure;
end refresh_mv;
/* Procedure voor het zetten van de status unusable bij indexen */
PROCEDURE INVALIDATE_INDEXES
 (P_MV_NAME IN VARCHAR2
 )
 IS
   cursor c_ind
   is
   select index_name
   from   user_indexes
   where  table_name = p_mv_name;
   --
   cn_module           constant varchar2(50) := 'RLE_MVR_PTL.INVALIDATE_INDEXES';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   for r_ind in c_ind
   loop
      do_ddl ( p_ddl_statement => 'alter index '||r_ind.index_name||' unusable ');
   end loop;
   --
   stm_util.t_procedure := l_vorige_procedure;
end invalidate_indexes;
/* Procedure voor het rebuilden van indexen */
PROCEDURE REBUILD_INDEXES
 (P_MV_NAME IN VARCHAR2
 )
 IS
   cursor c_ind
   is
   select index_name
   from   user_indexes
   where  table_name = p_mv_name;
   --
   cn_module           constant varchar2(50) := 'RLE_MVR_PTL.INVALIDATE_INDEXES';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   for r_ind in c_ind
   loop
      do_ddl (p_ddl_statement => 'alter index '||r_ind.index_name||' rebuild nologging');
   end loop;
   --
   stm_util.t_procedure := l_vorige_procedure;
end rebuild_indexes;
/* Procedure voor het bijwerken van table statistics */
PROCEDURE GATHER_TABLE_STATISTICS
 (P_MV_NAME IN VARCHAR2
 )
 IS
   cn_module           constant varchar2(50) := 'RLE_MVR_PTL.GATHER_TABLE_STATISTICS';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   do_ddl ( p_ddl_statement => 'begin dbms_stats.gather_table_stats ( ownname =>  ''COMP_RLE'', tabname => '''|| p_mv_name ||''', force => TRUE, cascade => FALSE); end;'); -- cascade want indexen worden door refresh al bijgewerkt
   --
   stm_util.t_procedure := l_vorige_procedure;
end gather_table_statistics;
/* Functie voor het bepalen van de in gebruik zijnde materialized view */
FUNCTION GET_CURRENT_MV_NAME
 (P_SYNONYM_NAME IN varchar2
 )
 RETURN VARCHAR2
 IS
   cursor c_syn
   is
   select table_name
   from   user_synonyms
   where  synonym_name = p_synonym_name||'_SYN';
   --
   l_mv_name                    varchar2(50);
   --
   cn_module           constant varchar2(50) := 'RLE_MVR_PTL.GET_CURRENT_MV_NAME';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   open c_syn;
   fetch c_syn
    into l_mv_name;
   close c_syn;
   --
   stm_util.t_procedure := l_vorige_procedure;
   --
   return l_mv_name;
end get_current_mv_name;
/* Verwerk procedure t.b.v. verwerking per materialized view */
PROCEDURE VERWERK_MV
 (P_SYNONYM_NAME IN varchar2
 )
 IS
   l_current_mv_name            varchar2(40);
   l_mv_name_refresh            varchar2(40);
   --
   cn_module           constant varchar2(50) := 'RLE_MVR_PTL.VERWERK_MV';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   l_current_mv_name := get_current_mv_name ( p_synonym_name => p_synonym_name);
   --
   if l_current_mv_name = p_synonym_name||'_1'
   then
      l_mv_name_refresh := p_synonym_name||'_2';
   else
      l_mv_name_refresh := p_synonym_name||'_1';
   end if;
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, ' ');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Huidige materialized view : '|| l_current_mv_name);
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Refresh materialized view : '|| l_mv_name_refresh);
   --
   -- RBT 03-01-2010
   -- Het vooraf invalideren en achteraf rebuilden van indexen voorlopig niet uitvoeren
   -- Een refresh o.b.v. atomic_refresh = FALSE voert dit zelf al uit
   --
   -- invalidate_indexes ( p_mv_name => l_mv_name_refresh);
   --
   refresh_mv ( p_mv_name => l_mv_name_refresh);
   --
   -- rebuild_indexes ( p_mv_name => l_mv_name_refresh);
   --
   gather_table_statistics( p_mv_name => l_mv_name_refresh);
   --
   do_ddl ( p_ddl_statement => 'create or replace synonym '||p_synonym_name||'_SYN for '||l_mv_name_refresh);
   --
   stm_util.t_procedure := l_vorige_procedure;
end verwerk_mv;
/* Verwerk procedure t.b.v. aanroep vanuit Cronacle */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 ,P_MV_TYPE IN VARCHAR2 := 'ALLES'
 )
 IS
   cn_module           constant varchar2 ( 50 ) := 'RLE_MVR_PTL.VERWERK';
   l_vorige_procedure  varchar2(80);
begin
   l_vorige_procedure        := stm_util.t_procedure;
   stm_util.t_procedure      := cn_module;
   stm_util.t_script_naam    := p_script_naam;
   stm_util.t_script_id      := p_script_id;
   stm_util.t_programma_naam := 'RLE_MVR_PTL';
   --
   g_volgnr  := 0;
   g_lijstnr := 1;
   g_regelnr := 0;
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, '* * START REFRESHEN MATERIALIZED VIEWS '|| to_char ( sysdate, 'dd-mm-yyyy hh24:mi') || ' * *');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Parameters');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Type refresh : '|| p_mv_type);
   --
   if p_mv_type in ('ALLES', 'WGR') then
      verwerk_mv ( p_synonym_name => 'RLE_MV_ZOEK_WERKGEVER');
   end if;
   --
   if p_mv_type in ('ALLES', 'WNR') then
      verwerk_mv ( p_synonym_name => 'RLE_MV_ZOEK_WERKNEMER');
   end if;
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, ' ');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, '* * EINDE REFRESHEN MATERIALIZED VIEWS '|| to_char ( sysdate, 'dd-mm-yyyy hh24:mi') || ' * *');
   --
   stm_util.insert_job_statussen ( g_volgnr, 'S', '0');
   commit;
   --
   stm_util.t_procedure := l_vorige_procedure;
exception
when others
then
   rollback;
   --
   stm_util.insert_job_statussen(g_volgnr,'S','1' ) ;
   stm_util.insert_job_statussen(g_volgnr,'I',substr(stm_util.t_procedure,1,80)) ;
   stm_util.insert_job_statussen(g_volgnr,'I',substr(sqlerrm ,1,255) ) ;
   commit;
   raise_application_error(-20000, stm_app_error(sqlerrm, null, null)||' '||dbms_utility.format_error_backtrace );
   --
end verwerk;

END RLE_MVR_PTL;
/