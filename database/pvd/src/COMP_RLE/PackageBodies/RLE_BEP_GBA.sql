CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_BEP_GBA IS
gc_lijstnummer             number := 1;
gn_regelnummer             number := 1;
cn_programma               constant stm_util.t_programma_naam%type :=  'RLE_BEP_GBA';
gc_lijstnummer_2           number := 2;  -- fouten
gn_regelnummer_2           number := 1;  -- fouten
gc_lijstnummer_3           number := 3;  -- einde
gn_regelnummer_3           number := 1;  -- einde

/* controle op WIA producten */
FUNCTION CHK_VERZ_PRODUCT_WIA
 (P_PERSOONSNUMMER IN NUMBER
 ,P_PEILDATUM IN DATE
 )
 RETURN BOOLEAN;
/* controle op product WAO */
FUNCTION CHK_VERZK_PRODUCT_WAO
 (P_PERSOONSNUMMER IN NUMBER
 ,P_PEILDATUM IN DATE
 )
 RETURN BOOLEAN;
/* verwerk gba status */
PROCEDURE VERWERK_GBA_STATUS
 (P_PERSOONSNUMMER IN NUMBER
 ,P_NAAR_INDICATIE IN VARCHAR2
 ,P_AANTAL_VERWIJDERBERICHTEN OUT NUMBER
 );


/* verwerk personen */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN VARCHAR2
 ,P_PEILDATUM IN DATE
 ,P_MAX_BERICHTEN IN NUMBER
 ,P_RELNR_VANAF IN VARCHAR2
 )
 IS
--
-- Cursoren tbv controle van groep 1: Verzekeringsproducten WAO
-- iedereen die indicatie j hebben en voor komen in deelnemerschappen, of gebeurtenis hebben voor scheiding of  een gebeurtenis ongeacht soort gebeurtenis
-- moeten nog steeds op ja blijven(vandaar die MINUS)
-- de overgebleven rle moet gecontroleerd worden of ze in aanmerking komen voor het overige uitzonderingen groep 1 en groep 2
-- NB deel 2 is nog niet geimplementeerd
--
cursor c_rle (b_herstart_rlenr   in number
            , b_peildatum_delay  in date
            , b_relnr_vanaf      in number)     -- OSP 06032012
is
  with   dln_ind_j as (
  select rle_numrelatie
  from   rle_afstemmingen_gba  aga
  where  aga.afnemers_indicatie = 'J'
  and    aga.code_gba_status    = 'OV'
  and    aga.dat_mutatie       <= b_peildatum_delay
  minus
   (select rle_numrelatie
   from    ret_deelnemerschappen
   union all
   select tzg.rle_numrelatie
   from   ret_deelnemerschappen dnp
        , ret_ukg_gebeurtenissen ugs
        , pdt_reg_gebeurtenissen rgs
        , ret_ukg_varianten uvt
        , ret_toezeggingen tzg
   where  dnp.id = ugs.dnp_id
   and    ugs.status = 'A'
   and    ugs.rgs_id = rgs.id
   and    rgs.sgs_code in ('ABP', 'VECO')
   and    ugs.id = uvt.ugs_id
   and    uvt.status = 'DE'
   and    uvt.id = tzg.uvt_id
   union all
   select ube.rle_numrelatie
   from   ret_deelnemerschappen dnp
        , ret_ukg_gebeurtenissen ugs
        , pdt_reg_gebeurtenissen rgs
        , ret_ukg_varianten uvt
        , ret_uitkeringen ukg
        , ret_ukg_begunstigden ube
   where  dnp.id = ugs.dnp_id
   and    ugs.status = 'A'
   and    ugs.rgs_id = rgs.id
   and    ugs.id = uvt.ugs_id
   and    uvt.status = 'DE'
   and    uvt.id = ukg.uvt_id
   and    ukg.id = ube.ukg_id
   and    dnp.rle_numrelatie <> ube.rle_numrelatie)
    -- deel 2 nog niet verder geimplementeerd
  /* union all
   (select rle_numrelatie
   from   ret_deelnemerschappen  dnp
        , rle_relaties rle
   where  rle.numrelatie = dnp.rle_numrelatie
   and    rle.datoverlijden is null
   union all
   select tzg.rle_numrelatie
   from   ret_deelnemerschappen dnp
        , ret_ukg_gebeurtenissen ugs
        , pdt_reg_gebeurtenissen rgs
        , ret_ukg_varianten uvt
        , ret_toezeggingen tzg
        , rle_relaties rle
   where  rle.numrelatie = tzg.rle_numrelatie
   and    rle.datoverlijden is null
   and    dnp.id     = ugs.dnp_id
   and    ugs.status = 'A'
   and    ugs.rgs_id = rgs.id
   and    rgs.sgs_code in ('ABP', 'VECO')
   and    ugs.id     = uvt.ugs_id
   and    uvt.status = 'DE'
   and    uvt.id     = tzg.uvt_id
   union all
   select ube.rle_numrelatie
   from   ret_deelnemerschappen dnp
        , ret_ukg_gebeurtenissen ugs
        , ret_ukg_varianten uvt
        , ret_uitkeringen ukg
        , ret_ukg_begunstigden ube
        , rle_relaties rle
   where  rle.numrelatie = ube.rle_numrelatie
   and    rle.datoverlijden is null
   and    dnp.id     = ugs.dnp_id
   and    ugs.status = 'A'
   and    ugs.id     = uvt.ugs_id
   and    uvt.status = 'DE'
   and    uvt.id     = ukg.uvt_id
   and    ukg.id     = ube.ukg_id
   and    dnp.rle_numrelatie <> ube.rle_numrelatie
  minus
  select rle_numrelatie
  from   rle_afstemmingen_gba
  where  afnemers_indicatie = 'J' )*/
  )
  select prs.relatienummer
       , prs.persoonsnummer
       , prs.datgeboorte
       , prs.datum_overlijden
       , aga.afnemers_indicatie
  from   dln_ind_j
       , rle_mv_personen  prs
       , rle_afstemmingen_gba aga
  where  prs.relatienummer  = dln_ind_j.rle_numrelatie
  and    aga.rle_numrelatie = prs.relatienummer
  and    prs.relatienummer >= nvl(b_herstart_rlenr, prs.relatienummer)
  and    prs.relatienummer > b_relnr_vanaf
  order by prs.relatienummer
  ;
--
-- check op al aanwezig GBA bericht
-- 7 dagen delay ivm pipeline GBA
--
cursor c_gbt (b_persoonsnummer  in number
            , b_peildatum_gba   in date)     -- OSP 06032012
  is
    select 'X'
    from   per_gba_berichten gbt
    where  gbt.gbt_type = 'U'
    and    (gbt.datum_verwerkt is null or gbt.creatiemoment > b_peildatum_gba)
    and    gbt.berichttype        = 'Av01'
    and    gbt.prs_persoonsnummer = b_persoonsnummer
    ;
--
ln_volgnummer                 number := 1;
l_tot_ind_van_n_naar_j        number;
l_aantal_gba_av01_berichten   number;
l_aantal_verwijderberichten   number;
l_tot_fout                    number;
l_herstart_rlenr              number;
l_exception_prsnr             number;
r_rle                         c_rle%rowtype;
r_gbt                         c_gbt%rowtype;
l_peildatum                   date;
l_peildatum_delay             date;   -- peildatum min een aantal dagen delay
l_peildatum_gba               date;
l_afnemers_indicatie          varchar2(5);
l_error                       varchar2(500);
l_wia_product_aanw            boolean;
l_wao_product_aanw            boolean;
l_max_aantal_berichten        number;
l_relnr_vanaf                 number;
l_laagste_relnr               number;
l_hoogste_relnr               number;
l_eerste_record               boolean;
--
BEGIN
 stm_util.debug ('RLE_BEP_GBA start');
 --
 stm_util.t_procedure      := cn_programma;
 stm_util.t_script_naam    := p_script_naam;
 stm_util.t_script_id      := to_number (p_script_id);
 stm_util.t_programma_naam := cn_programma ;
 stm_util.t_tekst          := null ;
 l_peildatum               := nvl(p_peildatum, sysdate);
 l_peildatum_delay         := l_peildatum - 21;             -- dagen delay ivm verwerking lopende processen
 l_peildatum_gba           := l_peildatum - 7;              -- dagen delay voor gba berichten (ivm pipeline)
 l_max_aantal_berichten    := nvl(p_max_berichten, 25000);  -- default waarde
 l_max_aantal_berichten    := l_max_aantal_berichten - 1;   -- ivm groter dan teken
 l_relnr_vanaf             := nvl(p_relnr_vanaf, 1);
 --
 stm_util.debug('l_peildatum_delay: '||l_peildatum_delay);
 --
 if stm_util.herstart
 then
   stm_util.debug('Herstart');
   l_eerste_record := false;
   -- ophalen herstartsleutel
   l_herstart_rlenr := stm_util.lees_herstartsleutel( 'l_herstart_rlenr');
   stm_util.debug('t_herstart_rlenr: '|| l_herstart_rlenr);
   -- herinitialiseren tellingen
   gn_regelnummer              := stm_util.lees_herstarttelling('gn_regelnummer');
   l_aantal_gba_av01_berichten := stm_util.lees_herstarttelling('l_aantal_gba_av01_berichten');
   l_tot_ind_van_n_naar_j      := stm_util.lees_herstarttelling('l_tot_ind_van_n_naar_j');
   l_tot_fout                  := stm_util.lees_herstarttelling('l_tot_fout');
   l_laagste_relnr             := stm_util.lees_herstarttelling('l_laagste_relnr');

   /* schrijf herstart punt en tijd in logverslag  */
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ') ;
   stm_util.insert_lijsten
   ( gc_lijstnummer
   , gn_regelnummer
   , '*** Procedure '||cn_programma||' herstart op: '|| to_char(sysdate,'dd-mm-yyyy hh24:mi')|| ' ***' );
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
 else
   stm_util.debug('Geen herstart');
   -- initialiseren herstartsleutel
   l_herstart_rlenr := 0;
   l_tot_fout       := 0;
   l_eerste_record  := true;
   -- initialiseren tellingen
   l_aantal_gba_av01_berichten := 0;
   l_tot_ind_van_n_naar_j      := 0;
   -- Kopregel logverslag printen eerste verwerking
   stm_util.insert_lijsten (  gc_lijstnummer
                           ,  gn_regelnummer
                           ,  '*** Procedure '||cn_programma|| ' gestart op: '
                           || to_char ( sysdate, 'dd-mm-yyyy hh24:mi')|| ' ***');
   --
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, 'Parameters:');
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, '--------------------------------------------------');
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, 'Peildatum:                 '||l_peildatum);
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, 'Max aantal GBA berichten:  '||nvl(p_max_berichten, 25000));
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, 'Relatienummer vanaf:       '||l_relnr_vanaf);
   stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
   stm_util.insert_lijsten (gc_lijstnummer_2, gn_regelnummer_2, 'Technische foutenlijst: ');
   stm_util.insert_lijsten (gc_lijstnummer_2
                          , gn_regelnummer_2
                          ,  '------------------------------------------------------------------------');

 end if ;
 --
 -- hoofdloop op personen
 --
 for r_rle  in c_rle (b_herstart_rlenr  => l_herstart_rlenr
                    , b_peildatum_delay => l_peildatum_delay
                    , b_relnr_vanaf     => l_relnr_vanaf)
 loop
    --
    stm_util.debug('  ');
    -- bijhouden hoogste relnr
    l_hoogste_relnr := r_rle.relatienummer;  -- OSP 06032012
    -- vullen laagste relatienummer
    if l_eerste_record
    then
       l_laagste_relnr := r_rle.relatienummer;  -- OSP 06032012
       stm_util.schrijf_herstarttelling ('l_laagste_relnr', l_laagste_relnr);
       stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, 'Laagste relatienummer uit selectie: '||l_laagste_relnr);
       l_eerste_record := false;
    end if;
    --
    -- eerst check of er al een GBA bericht is
    -- voor deze persoon, anders niets doen
    --
    open  c_gbt (r_rle.persoonsnummer
               , l_peildatum_gba);
    fetch c_gbt into r_gbt;
    if c_gbt%notfound      -- geen GBA bericht, dan verwerken
    then
       -- vullen herstartsleutel
       l_herstart_rlenr := r_rle.relatienummer;
       if r_rle.afnemers_indicatie = 'J'
       then
         stm_util.debug('Indicatie JA relnr: '|| r_rle.relatienummer||' prsnr:  '||r_rle.persoonsnummer);
         -- start controles
         begin
           l_wia_product_aanw := false;
           l_wao_product_aanw := false;
           -- controleren voor groep 2 (WIA)
           l_wia_product_aanw := chk_verz_product_wia (p_persoonsnummer => r_rle.persoonsnummer
                                                     , p_peildatum      => l_peildatum );
           --
           if l_wia_product_aanw
           then
             -- WIA gevonden
             -- afnemersindicatie kan op J blijven
             stm_util.debug('WIA gevonden: persoon met relnr: '||r_rle.relatienummer||' is niet aangepast! ');
           else
             -- geen WIA gevonden, dan controleren op WAO
             l_wao_product_aanw := chk_verzk_product_wao (p_persoonsnummer => r_rle.persoonsnummer
                                                        , p_peildatum      => l_peildatum );
             -- als dln geen wao product heeft dan moet indicatie op N gezet te worden.
             if l_wao_product_aanw = false
             then
               -- update afnemers_indicatie
               verwerk_gba_status (p_persoonsnummer => r_rle.persoonsnummer
                                 , p_naar_indicatie => 'N'
                                 , p_aantal_verwijderberichten => l_aantal_verwijderberichten);
               if l_aantal_verwijderberichten >= 1
               then
                  stm_util.debug('GBA verwijderbericht gemaakt voor rlenr: ' || r_rle.relatienummer || ' prsnr: ' || r_rle.persoonsnummer );
               end if;
               --
               l_aantal_gba_av01_berichten := l_aantal_gba_av01_berichten + l_aantal_verwijderberichten;
               -- max aantal GBA berichten
               if l_aantal_gba_av01_berichten > l_max_aantal_berichten
               then
                  exit;
               end if;
               --
             else
               stm_util.debug('WAO gevonden: persoon met relnr: '||r_rle.relatienummer||' is niet aangepast! ');
             end if;
            --
           end if;
         --
         exception
         when others
         then
           l_tot_fout := l_tot_fout + 1;
           l_error := sqlerrm || ' ' || SUBSTR( ' Trace info:' || DBMS_UTILITY.format_error_backtrace, 1, 400);
           stm_util.debug('FOUT bij persoon P'|| l_exception_prsnr|| ' is de volgende fout opgetreden: '|| l_error );
           stm_util.insert_lijsten (gc_lijstnummer_2
                                  , gn_regelnummer_2
                                  , substr('FOUT bij persoon P'|| l_exception_prsnr|| ' is de volgende fout opgetreden: '
                                  || l_error, 1 ,450));
         end;
       elsif r_rle.afnemers_indicatie = 'N'
       then
         -- nog niet geimplementeerd!!
         stm_util.debug('Indicatie NEE relnr: '|| r_rle.relatienummer||' prsnr:  '||r_rle.persoonsnummer);
       end if;
       --
       -- schrijven herstart sleutel
       stm_util.schrijf_herstartsleutel ( 'l_herstart_rlenr', l_herstart_rlenr);
       -- schrijven herstart tellingen beide loops
       stm_util.schrijf_herstarttelling ('gn_regelnummer', gn_regelnummer );
       stm_util.schrijf_herstarttelling ('l_aantal_gba_av01_berichten', l_aantal_gba_av01_berichten);
       stm_util.schrijf_herstarttelling ('l_tot_ind_van_n_naar_j', l_tot_ind_van_n_naar_j);
       stm_util.schrijf_herstarttelling ('l_tot_fout', l_tot_fout);
       commit;
    else
       stm_util.debug('GBA bericht gevonden voor relnr: '|| r_rle.relatienummer||' prsnr:  '||r_rle.persoonsnummer);
    end if;  -- c_gbtnotfound
    close c_gbt;
    --
 end loop;
--
 stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, 'Hoogste relatienummer uit selectie: '||l_hoogste_relnr);
 stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
 stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
 stm_util.insert_lijsten (gc_lijstnummer
                        , gn_regelnummer
                        , 'Totalen');
 stm_util.insert_lijsten (gc_lijstnummer
                        , gn_regelnummer
                        ,  '------------------------------------------------------------------------');
 stm_util.insert_lijsten (gc_lijstnummer
                        , gn_regelnummer
                        ,  'Totaal aantal aangemaakte GBA verwijderberichten: ' ||l_aantal_gba_av01_berichten);
 /* stm_util.insert_lijsten (gc_lijstnummer
                        , gn_regelnummer
                        ,  'Totaal aantal GBA indicaties van nee naar ja: ' ||l_tot_ind_van_n_naar_j); */
 stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
 stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
 stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, 'Technische fouten: ');
 stm_util.insert_lijsten (gc_lijstnummer
                        , gn_regelnummer
                        ,  '------------------------------------------------------------------------');
 stm_util.insert_lijsten (gc_lijstnummer
                        , gn_regelnummer
                        ,  'Totaal aantal fouten: ' ||l_tot_fout);

 stm_util.insert_lijsten (gc_lijstnummer, gn_regelnummer, ' ');
 stm_util.insert_job_statussen (ln_volgnummer
                               , 'I'
                               , stm_util.t_programma_naam
                                 || ' procedure  OK: '|| stm_util.t_procedure);
 stm_util.insert_lijsten ( gc_lijstnummer_3
                         , gn_regelnummer_3
                         , '*** Procedure '
                         || cn_programma
                         || ' geeindigd op '
                         || to_char ( sysdate, 'dd-mm-yyyy hh24:mi')
                         || ' ***'
                         ) ;
 -- stm bijwerken en commit
 stm_util.insert_job_statussen( ln_volgnummer, 'S', '0');
 stm_util.verwijder_herstart ;
 commit;
-- klaar!
EXCEPTION
  --
  when others
  then
    rollback;
    --
    stm_util.insert_job_statussen (ln_volgnummer,'S','1') ;
    stm_util.insert_job_statussen (ln_volgnummer,'I',substr(stm_util.t_procedure,1,80));
    stm_util.insert_job_statussen (ln_volgnummer,'I','Het is fout gegaan bij prsnr: '||l_exception_prsnr);
    stm_util.insert_job_statussen (ln_volgnummer,'I',substr(sqlerrm ,1,255));
    stm_util.debug('FOUT: '||substr(sqlerrm ,1,255));
    --
    commit;
    --
END;
/* controle op WIA producten */
FUNCTION CHK_VERZ_PRODUCT_WIA
 (P_PERSOONSNUMMER IN NUMBER
 ,P_PEILDATUM IN DATE
 )
 RETURN BOOLEAN
 IS
--
-- cursor tbv controle van groep 2: Verzekeringsproducten WIA */
cursor c_veo( b_persoonsnummer in number
            , b_peildatum      in date)
 is
  select 'X'
  from  aos_verzekerd_perioden_ao veo
      , aos_verzekerden_ao vao
  where vao.id = veo.vao_id
  and   veo.fiatteur is not null
  and   veo.vervalmoment is null
  and   veo.veo_type = 'GAO'
  and   veo.ingangsdatum <= b_peildatum
  and   nvl(veo.einddatum, b_peildatum) >= b_peildatum
  and   vao.prs_persoonsnummer =  b_persoonsnummer
  union all
  -- of gedeelte
  select 'X'
  from  aos_polissen_ao pao
      , aos_verzekerden_ao vao
      , aos_verzekerd_perioden_ao veo
  where pao.id = vao.pao_id
  and   vao.id = veo.vao_id
  and   pao.vzp_nummer in (50)
  and   veo.fiatteur is not null -- in fo moet het nog vewerkt worden.
  and   veo.vervalmoment is null
  and   veo.veo_type = 'GAO'
  and   veo.ingangsdatum <= b_peildatum
  and   nvl(veo.einddatum, b_peildatum) >= b_peildatum
  and   vao.prs_persoonsnummer =  b_persoonsnummer
  ;
--
  l_dummy        varchar2(1);
  l_peildatum    date;
--
BEGIN
 stm_util.debug('start chk_verz_product_wia persoonsnummer: '||p_persoonsnummer);
 l_peildatum := p_peildatum;
  /* Controle groep 2 */
  --
  open c_veo( b_persoonsnummer => p_persoonsnummer
            , b_peildatum      => l_peildatum);
  fetch c_veo into l_dummy;
  --
  if c_veo%found
  then
    stm_util.debug('Voor persoonsnummer: ' || p_persoonsnummer ||' is op peildatum: '|| l_peildatum ||' WIA producten aanwezig.');
    close c_veo;
    return true;
  end if;
  --
  close c_veo;
  -- geen producten gevonden.
  stm_util.debug('geen WIA gevonden' );
  return false;
  --
END;
/* controle op product WAO */
FUNCTION CHK_VERZK_PRODUCT_WAO
 (P_PERSOONSNUMMER IN NUMBER
 ,P_PEILDATUM IN DATE
 )
 RETURN BOOLEAN
 IS
--
-- cursor tbv controle van groep 3: Verzekeringsproducten WAO
cursor c_toe( b_persoonsnummer in rle_mv_personen.persoonsnummer%type
            , b_peildatum      in date)
 is
  select 'X'
  from toekenningen_aov toe
      , aanspraken_aov asp
  where asp.aanspraaknummer = toe.asp_aanspraaknummer
  and   toe.vervaldatum is null
  and   toe.toekenningsbedrag > 0
  and   toe.ingangsdatum <= b_peildatum
  and   nvl(toe.einddatum, b_peildatum) >= b_peildatum
  and   asp.prs_persoonsnummer = b_persoonsnummer
  ;
--
  l_dummy        varchar2(1);
  l_peildatum    date;
--
BEGIN
 stm_util.debug(' start chk_verz_product_wao p_persoonsnummer: '||p_persoonsnummer);
 l_peildatum := p_peildatum;
  /* Controle  groep 3*/
  -- indien cursor resultaat opleverd, dan heeft deze relatienummer een verzekerings product(en) van WAO.
  open c_toe( b_persoonsnummer => p_persoonsnummer
            , b_peildatum      => l_peildatum);
  fetch c_toe into l_dummy;
  --
  if c_toe%found
  then
    stm_util.debug('Voor relatienummer: ' || p_persoonsnummer ||' is op peildatum: '|| l_peildatum ||' een lopende toekenning');
    close c_toe;
    -- relatie nr heeft degelijk een  verzekerde product(en) van WAO.
    return true;
  end if;
  --
  close c_toe;
  -- geen producten gevonden.
  stm_util.debug('geen WAO gevonden ');
  return false;

END;
/* verwerk gba status */
PROCEDURE VERWERK_GBA_STATUS
 (P_PERSOONSNUMMER IN NUMBER
 ,P_NAAR_INDICATIE IN VARCHAR2
 ,P_AANTAL_VERWIJDERBERICHTEN OUT NUMBER
 )
 IS
--
-- ophalen postcode en woonplaats van persoon uit GBA
cursor c_adr (b_persoonsnummer in number)
 is
  select postkode_bin            postcode
       , wpl_woonplaatskode_bin  woonplaats
       , ind_buitenland          ind_buitenland
  from   adressen
  where  prs_persoonsnummer = b_persoonsnummer
  and    code_adressoort    = 'D';
--
-- ophalen A_nummer van de persoon
--
cursor c_prs(b_persoonsnummer in number)
 is
  select  a_nummer
  from    personen
  where   persoonsnummer = b_persoonsnummer ;
--
t_gemeentenr                   number;
appl_code                      varchar2(3):='GBA';
r_prs                          c_prs%rowtype;
l_aantal_verwijderberichten    number;
--
BEGIN
  -- nu alleen nog verwijderberichten (alleen deel 1 FO rle0996)
  stm_util.debug('verwerk GBA status prsnr: '||p_persoonsnummer);
  l_aantal_verwijderberichten := 0;
  -- maken van verwijderbericht GBA
  for r_adr in c_adr (p_persoonsnummer)
  loop
     --
     -- Alleen personen in binnenland vindt afstemming met GBA plaats
     if r_adr.ind_buitenland = 'N'
     then
       -- bepaal gemeentenummer
       t_gemeentenr:= per_lib.bepaal_gemeentenummer(r_adr.ind_buitenland
                                                  , r_adr.woonplaats
                                                  , r_adr.postcode);
      --
      open  c_prs (p_persoonsnummer);
      fetch c_prs into r_prs;
      close c_prs;
      --
      --  maak verwijderbericht
      gba_lib.sch_av01(t_gemeentenr     --p_gemeentenummer in number
                     , null             --p_apn_id in number
                     , p_persoonsnummer --p_persoonsnummer in number
                     , r_prs.a_nummer   --p_a_nummer in number
                      );
      -- tellen aantan aangemaakte GBA verwijderberichten
      l_aantal_verwijderberichten := l_aantal_verwijderberichten + 1;
     end if;
  end loop;
  p_aantal_verwijderberichten := l_aantal_verwijderberichten;
  stm_util.debug('verwerk GBA p_aantal_verwijderberichten: '||p_aantal_verwijderberichten);
 --
END;

END RLE_BEP_GBA;
/