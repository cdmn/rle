CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_MVR_PRSM IS
   g_lijstnr                 number := 1;
   g_regelnr                 number;
   g_volgnr                  number;

/* Procedure voor uitvoeren execute immediate statement */
PROCEDURE DO_DDL
 (P_DDL_STATEMENT IN VARCHAR2
 )
 IS
   cn_module           constant varchar2(50) := 'RLE_MVR_PRSM.DO_DDL';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, ' ');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Starten statement :');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, substr(p_ddl_statement, 1, 500));
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Starttijd : '||to_char(sysdate, 'DD-MM-YYYY HH24:MI:SS'));
   commit;
   --
   execute immediate (p_ddl_statement);
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Eindtijd  : '||to_char(sysdate, 'DD-MM-YYYY HH24:MI:SS'));
   --
   stm_util.t_procedure := l_vorige_procedure;
end do_ddl;

/* Procedure voor het refreshen van een materialized view */
PROCEDURE REFRESH_MV
 (P_MV_NAME IN VARCHAR2
 )
 IS
   cn_module           constant varchar2(50) := 'RLE_MVR_PRSM.REFRESH_MV';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   do_ddl ( p_ddl_statement => 'begin dbms_mview.refresh ( list => ''COMP_RLE.'|| p_mv_name ||''', method => ''C'', atomic_refresh => FALSE); end;'); -- atomic_refresh = false : Truncate wordt uitgevoerd
   --
   stm_util.t_procedure := l_vorige_procedure;
end refresh_mv;

procedure gather_index_stats(p_table_name varchar2)
is

cn_module           constant varchar2(50) := 'RLE_MVR_PRSM.GATHER_INDEX_STATS';
l_vorige_procedure           varchar2(80);

cursor c_icn (b_table_name varchar2)
is
select *
from   all_ind_columns icn
where  icn.table_name = b_table_name
;


begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;

   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, ' ');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Starttijd : '||to_char(sysdate, 'DD-MM-YYYY HH24:MI:SS'));
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Starten statement : dbms_stats.gather_index_stats : ');
  for r_icn in c_icn(b_table_name => p_table_name) loop

   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, r_icn.table_owner||'.'||r_icn.index_name);

    DBMS_STATS.GATHER_INDEX_STATS (
    ownname          => r_icn.table_owner,
    indname          => r_icn.index_name,
    estimate_percent => dbms_stats.auto_sample_size,
    degree           => dbms_stats.auto_degree,
    force            => true);
  end loop;

  stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Eindtijd  : '||to_char(sysdate, 'DD-MM-YYYY HH24:MI:SS'));
  stm_util.t_procedure := l_vorige_procedure;

end;

/* Procedure voor het bijwerken van table statistics */
PROCEDURE GATHER_TABLE_STATISTICS
 (P_MV_NAME IN VARCHAR2
 )
 IS
   cn_module           constant varchar2(50) := 'RLE_MVR_PRSM.GATHER_TABLE_STATISTICS';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   do_ddl ( p_ddl_statement => 'begin dbms_stats.gather_table_stats ( ownname =>  ''COMP_RLE'', tabname => '''|| p_mv_name ||''',degree => DBMS_STATS.AUTO_DEGREE, force => TRUE, cascade => FALSE); end;'); -- cascade want indexen worden door refresh al bijgewerkt
   --
   stm_util.t_procedure := l_vorige_procedure;
end gather_table_statistics;
/* Functie voor het bepalen van de in gebruik zijnde materialized view */
FUNCTION GET_CURRENT_MV_NAME
 (P_SYNONYM_NAME IN varchar2
 )
 RETURN VARCHAR2
 IS
   cursor c_syn(b_SYNONYM_NAME varchar2)
   is
     select table_name
     from   user_synonyms
     where  synonym_name = b_synonym_name;
   --
   l_mv_name                    varchar2(50);
   --
   cn_module           constant varchar2(50) := 'RLE_MVR_PRSM.GET_CURRENT_MV_NAME';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   open c_syn(P_SYNONYM_NAME);
   fetch c_syn into l_mv_name;
   close c_syn;
   --
   stm_util.t_procedure := l_vorige_procedure;
   --
   return l_mv_name;
end get_current_mv_name;
/* Verwerk procedure t.b.v. verwerking per materialized view */
PROCEDURE VERWERK_MV
 (P_SYNONYM_NAME IN varchar2
 )
 IS
   l_current_mv_name            varchar2(40);
   l_mv_name_refresh            varchar2(40);
   --
   cn_module           constant varchar2(50) := 'RLE_MVR_PRSM.VERWERK_MV';
   l_vorige_procedure           varchar2(80);
begin
   l_vorige_procedure   := stm_util.t_procedure;
   stm_util.t_procedure := cn_module;
   --
   l_current_mv_name := get_current_mv_name ( p_synonym_name => p_synonym_name );
   --
   if l_current_mv_name = p_synonym_name ||'_1'
   then
      l_mv_name_refresh := p_synonym_name||'_2';
   else
      l_mv_name_refresh := p_synonym_name||'_1';
   end if;
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, ' ');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Huidige materialized view : '|| l_current_mv_name);
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Refresh materialized view : '|| l_mv_name_refresh);
   --
   --
   refresh_mv ( p_mv_name => l_mv_name_refresh);
   --
   gather_index_stats(p_table_name => l_mv_name_refresh);
   --
   gather_table_statistics( p_mv_name => l_mv_name_refresh);
   --
   do_ddl ( p_ddl_statement => 'create or replace synonym '||p_synonym_name ||' for '||l_mv_name_refresh );
   --
   stm_util.t_procedure := l_vorige_procedure;
end verwerk_mv;
/* Verwerk procedure t.b.v. aanroep vanuit Cronacle */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 )
 IS
   cn_module           constant varchar2 ( 50 ) := 'RLE_MVR_PRSM.VERWERK';
   l_vorige_procedure  varchar2(80);
   cl_mv               varchar2(80) := 'RLE_MV_PERSONEN';
begin
   l_vorige_procedure        := stm_util.t_procedure;
   stm_util.t_procedure      := cn_module;
   stm_util.t_script_naam    := p_script_naam;
   stm_util.t_script_id      := p_script_id;
   stm_util.t_programma_naam := 'RLE_MVR_PRSM';
   --
   g_volgnr  := 0;
   g_lijstnr := 1;
   g_regelnr := 0;
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, '* * START REFRESHEN MATERIALIZED VIEW ' || cl_mv || '_1/2 : ' || to_char ( sysdate, 'dd-mm-yyyy hh24:mi') || ' * *');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, 'Parameters');
   --
   verwerk_mv ( p_synonym_name => cl_mv );
   --
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, ' ');
   stm_util.insert_lijsten ( g_lijstnr, g_regelnr, '* * EINDE REFRESHEN MATERIALIZED VIEW ' || cl_mv || '_1/2 : ' || to_char ( sysdate, 'dd-mm-yyyy hh24:mi') || ' * *');
   --
   stm_util.insert_job_statussen ( g_volgnr, 'S', '0');
   commit;
   --
   stm_util.t_procedure := l_vorige_procedure;
exception
when others
then
   rollback;
   --
   stm_util.insert_job_statussen(g_volgnr,'S','1' ) ;
   stm_util.insert_job_statussen(g_volgnr,'I',substr(stm_util.t_procedure,1,80)) ;
   stm_util.insert_job_statussen(g_volgnr,'I',substr(sqlerrm ,1,255) ) ;
   commit;
   raise_application_error(-20000, stm_app_error(sqlerrm, null, null)||' '||dbms_utility.format_error_backtrace );
   --
end verwerk;
END RLE_MVR_PRSM;
/