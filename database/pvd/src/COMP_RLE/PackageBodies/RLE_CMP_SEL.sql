CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_CMP_SEL IS
  --
  g_cpe_id              number(10);
  g_regelnummer         number(10,0)     := 0;
  g_aantal_rle_in_campagnes pls_integer      := 0;
  g_lijstnummer         number           := 1;
  g_sql_tekst           varchar2(32000);
  g_herstart            boolean          := false;
  --

/* Check existing CDE_ID */
FUNCTION CHECK_CDE
 (P_CDE_ID IN number
 )
 RETURN BOOLEAN;
PROCEDURE SCHRIJF_NAAR_LOGVERSLAG
 (P_REGEL IN VARCHAR2
 );
PROCEDURE TOEV_CONSTANTEN_AAN_SQL
 (P_CONSTANTE IN VARCHAR2
 ,P_WAARDE IN VARCHAR2
 );
PROCEDURE TOEV_VARIABELEN_AAN_SQL
 (P_NAAM_VARIABELE IN VARCHAR2
 ,P_DATATYPE IN VARCHAR2
 ,P_WAARDE IN VARCHAR2
 );
PROCEDURE INIT_SQL_TEKST;
PROCEDURE SELECTIE_EN_OPSLAG
 (P_CDE_ID IN number
 ,P_TESTRUN IN varchar2
 );


/* Check existing CDE_ID */
FUNCTION CHECK_CDE
 (P_CDE_ID IN number
 )
 RETURN BOOLEAN
 IS
  --
  cursor c_cde(b_cde_id in number)
  is
  select cde.naam
  from   rle_campagne_definities cde
  where  cde.id = b_cde_id;
  --
  r_cde c_cde%rowtype;
  l_result boolean;
  --
begin
  --
  open c_cde(p_cde_id);
  fetch c_cde into r_cde;
  l_result := c_cde%found;
  close c_cde;
  --
  if l_result
  then
    --
    schrijf_naar_logverslag('Gebruikte definitie: '||to_char(p_cde_id)||' '||r_cde.naam);
    --
  end if;
  --
  return l_result;
  --
exception
  --
  when others
  then
    --
    if c_cde%isopen
    then
      --
      close c_cde;
      --
    end if;
    --
    raise;
    --
end check_cde;
PROCEDURE SCHRIJF_NAAR_LOGVERSLAG
 (P_REGEL IN VARCHAR2
 )
 IS
begin
  --
  stm_util.insert_lijsten( g_lijstnummer
                         , g_regelnummer
                         , p_regel
                         );
  --
END SCHRIJF_NAAR_LOGVERSLAG;
PROCEDURE TOEV_CONSTANTEN_AAN_SQL
 (P_CONSTANTE IN VARCHAR2
 ,P_WAARDE IN VARCHAR2
 )
 IS
  /******************************************************************************************************
  Naam:         TOEV_CONSTANTEN_AAN_SQL
  Beschrijving: Het doel van deze functie is het toevoegen van constanten aan het op te bouwen
                sql-statement

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  27-11-2009  PAS  Creatie
  *******************************************************************************************************/
begin
  if p_waarde is not null
  then
    g_sql_tekst := replace( g_sql_tekst, p_constante , 'q''#'|| p_waarde  ||'#''' );
  else
    g_sql_tekst := replace( g_sql_tekst, p_constante , 'null' );
  end if;
end;
PROCEDURE TOEV_VARIABELEN_AAN_SQL
 (P_NAAM_VARIABELE IN VARCHAR2
 ,P_DATATYPE IN VARCHAR2
 ,P_WAARDE IN VARCHAR2
 )
 IS
  /******************************************************************************************************
  Naam:         TOEV_VARIABELEN_AAN_SQL
  Beschrijving: Het doel van deze functie is het toevoegen van variabelen aan het op te bouwen
                sql-statement

                Voor de variabelen word gekeken wat voor datatype het is
                NUMBER              , geen actie
                DATE                , to_date om variabele plaatsen
                CHAR                , quotes eromheen plaatsen
                ALFANUMERIEK REEKS  , quotes om alle waarden plaatsen
                NUMERIEKE REEKS     , geen actie

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  27-11-2009  PAS  Creatie
  09-12-2009  PAS  Reeks opgesplitst in alfanumerieke en numeriek reeks
  *******************************************************************************************************/
  --
begin
  --
  if p_waarde is null
  then
    --
    g_sql_tekst := replace( g_sql_tekst, p_naam_variabele, 'null');
    --
  else
    --
    if    p_datatype = 'N'
    then
      g_sql_tekst := replace( g_sql_tekst, p_naam_variabele, p_waarde); -- Nummer
    elsif p_datatype = 'D'
    then
      g_sql_tekst := replace( g_sql_tekst, p_naam_variabele, 'to_date(' || '''' || p_waarde || '''' ||
                                                       ',' || '''' || 'DD-MM-YYYY' || '''' || ')')  ; -- Datum
    elsif p_datatype = 'R'
    then
      g_sql_tekst := replace( g_sql_tekst, p_naam_variabele, p_waarde ); -- Numerieke Reeks
    elsif p_datatype = 'A'
    then
      g_sql_tekst := replace( g_sql_tekst, p_naam_variabele, ''''||replace(replace(p_waarde,' ',''),',',''',''')||'''' ); -- Karakter Reeks
    else
      g_sql_tekst := replace( g_sql_tekst, p_naam_variabele,'''' || p_waarde || '''');  -- Karakter
    end if;
    --
  end if;
end;
PROCEDURE INIT_SQL_TEKST
 IS
  /******************************************************************************************************
  Naam:         INIT_SQL_TEKST
  Beschrijving: Het doel van deze functie is het initialiseren van het op te bouwen sql-statement
                sql-statement. Extra condities worden in de procedure selecteer toegevoegd.

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  27-11-2009  PAS  Creatie
  *******************************************************************************************************/
  --
begin
  --
  g_sql_tekst :=  q'# select null                 id                   #'||chr(10)||
                  q'# ,      :CPE_ID              cpe_id               #'||chr(10)||
                  q'# ,      rle.numrelatie       rle_numrelatie       #'||chr(10)||
                  q'# ,      'AGT'                status               #'||chr(10)||
                  q'# ,      'N'                  ind_uitgesloten      #'||chr(10)||
                  q'# ,      'N'                  ind_overleden        #'||chr(10)||
                  q'# ,      'N'                  ind_beeindigd        #'||chr(10)||
                  q'# ,      'N'                  ind_aangepast        #'||chr(10)||
                  q'# ,      'N'                  ind_toegevoegd       #'||chr(10)||
                  q'# ,      ( case                                    #'||chr(10)||
                  q'#            when rec.rol_codrol = 'WG'            #'||chr(10)||
                  q'#            then rec.extern_relatie               #'||chr(10)||
                  q'#            else null                             #'||chr(10)||
                  q'#          end                                     #'||chr(10)||
                  q'#        )                    werkgevernummer      #'||chr(10)||
                  q'# ,      ( case                                    #'||chr(10)||
                  q'#            when rec.rol_codrol = 'PR'            #'||chr(10)||
                  q'#            then to_number(rec.extern_relatie)    #'||chr(10)||
                  q'#            else null                             #'||chr(10)||
                  q'#          end                                     #'||chr(10)||
                  q'#        )                    persoonsnummer       #'||chr(10)||
                  q'# ,      rle.zoeknaam         zoeknaam             #'||chr(10)||
                  q'# ,      sysdate              status_datum         #'||chr(10)||
                  q'# ,      :CONST1              const1_waarde        #'||chr(10)||
                  q'# ,      :CONST2              const2_waarde        #'||chr(10)||
                  q'# ,      :CONST3              const3_waarde        #'||chr(10)||
                  q'# ,      :CONST4              const4_waarde        #'||chr(10)||
                  q'# ,      :CONST5              const5_waarde        #'||chr(10)||
                  q'# ,      :CONST6              const6_waarde        #'||chr(10)||
                  q'# ,      :CONST7              const7_waarde        #'||chr(10)||
                  q'# ,      :CONST8              const8_waarde        #'||chr(10)||
                  q'# ,      :CONST9              const9_waarde        #'||chr(10)||
                  q'# ,      :CONSTA              const10_waarde       #'||chr(10)||
                  q'# ,      null                 creatie_door         #'||chr(10)||
                  q'# ,      null                 dat_creatie          #'||chr(10)||
                  q'# ,      null                 mutatie_door         #'||chr(10)||
                  q'# ,      null                 dat_mutatie          #'||chr(10)||
                  q'# from  rle_relaties rle                           #'||chr(10)||
                  q'# ,     rle_relatie_externe_codes rec              #'||chr(10)||
                  q'# where rle.numrelatie     = rec.rle_numrelatie    #'||chr(10)||
                  q'# and   rle.datoverlijden is null                  #'||chr(10)||
                  q'# and  (                                           #'||chr(10)||
                  q'#        (     rec.rol_codrol = 'PR'               #'||chr(10)||
                  q'#          and rec.dwe_wrddom_extsys = 'PRS'       #'||chr(10)||
                  q'#        )                                         #'||chr(10)||
                  q'#      or                                          #'||chr(10)||
                  q'#        (     rec.rol_codrol = 'WG'               #'||chr(10)||
                  q'#          and rec.dwe_wrddom_extsys = 'WGR'       #'||chr(10)||
                  q'#        )                                         #'||chr(10)||
                  q'#      )                                           #'||chr(10)||
                  q'# and not exists                                   #'||chr(10)||
                  q'#    ( select 'x'                                  #'||chr(10)||
                  q'#      from   rle_relaties_in_campagnes ric        #'||chr(10)||
                  q'#      where  ric.rle_numrelatie = rle.numrelatie  #'||chr(10)||
                  q'#      and    ric.cpe_id         = :CPE_ID         #'||chr(10)||
                  q'#    )                                             #'||chr(10)
  ;
end;
PROCEDURE SELECTIE_EN_OPSLAG
 (P_CDE_ID IN number
 ,P_TESTRUN IN varchar2
 )
 IS
  /******************************************************************************************************
  Naam:         SELECTIE_EN_OPSLAG
  Beschrijving: Het doel van deze functie is het aanvullen van de basis sql tekst en hierna selectie en
                opslag van de relaties welke voldoen.


  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  24-06-2008  HSI  Nieuwbouw
  23-10-2008  JLU  Wjzigingen nav release 2
  27-11-2009  PAS  Code aangepast t.b.v. leesbaarheid en performance
  02-03-2010  XMK  check_cde niet voor testrun.
  *******************************************************************************************************/
  --
  -- Cursor voor het ophalen van de gebruikte voorwaarden.
  -- Outer-join is toegevoegd in geval er geen
  -- extra condities bestaan (zal eigenlijk nooit voorkomen)
  cursor c_cde ( b_cde_id in number)
  is
    select cde.naam            cde_naam
    ,      cde.const1_waarde   cde_const_w1
    ,      cde.const2_waarde   cde_const_w2
    ,      cde.const3_waarde   cde_const_w3
    ,      cde.const4_waarde   cde_const_w4
    ,      cde.const5_waarde   cde_const_w5
    ,      cde.const6_waarde   cde_const_w6
    ,      cde.const7_waarde   cde_const_w7
    ,      cde.const8_waarde   cde_const_w8
    ,      cde.const9_waarde   cde_const_w9
    ,      cde.const10_waarde  cde_const_w10
    ,      gvw.regelnummer
    ,      gvw.var1_waarde     gvw_var1
    ,      gvw.var2_waarde     gvw_var2
    ,      gvw.var3_waarde     gvw_var3
    ,      gvw.var4_waarde     gvw_var4
    ,      gvw.var5_waarde     gvw_var5
    ,      gvw.ind_geblokkeerd
    ,      sve.naam   sve_naam
    ,      sve.sqltext
    ,      sve.var1_naam
    ,      sve.var1_datatype
    ,      sve.var2_naam
    ,      sve.var2_datatype
    ,      sve.var3_naam
    ,      sve.var3_datatype
    ,      sve.var4_naam
    ,      sve.var4_datatype
    ,      sve.var5_naam
    ,      sve.var5_datatype
    from   rle_campagne_definities      cde
    ,      rle_gebruikte_voorwaarden gvw
    ,      rle_standaard_voorwaarden sve
    where  cde.id                  = b_cde_id
    and    gvw.cde_id          (+) = cde.id
    and    gvw.ind_geblokkeerd (+) = 'N'
    and    sve.id              (+) = gvw.sve_id
  ;
  --
  type rle_type is ref cursor;
  type ric_type is table of rle_relaties_in_campagnes%rowtype index by binary_integer;
  --
  c_rle_ref_cursor   rle_type;
  ric_tab            ric_type;
  l_status           rle_campagnes.status%type;
  --
begin
  --
  -- Check CDE
  --
  if p_testrun = 'N'
  then
    if not check_cde(p_cde_id) then
      --
      schrijf_naar_logverslag('Onbekende definitie: '||to_char(p_cde_id));
      --
      return;
      --
    end if;
  end if;
  --
  init_sql_tekst;
  --
  -- Nu gaan we het sql-statement aanvullen. We loopen daarvoor over alle gebruikte voorwaarden
  for r_cde in c_cde( b_cde_id => p_cde_id )
  loop
    --
    if c_cde%rowcount = 1
    then
      -- Constanten hoeven we maar 1 keer toe te voegen omdat deze waarden uit de groepsdefinitie komen
      -- en dus binnen de loop c_cde elke keer dezelfde waarde zullen hebben
      toev_constanten_aan_sql( p_constante => ':CONST1', p_waarde => r_cde.cde_const_w1);
      toev_constanten_aan_sql( p_constante => ':CONST2', p_waarde => r_cde.cde_const_w2);
      toev_constanten_aan_sql( p_constante => ':CONST3', p_waarde => r_cde.cde_const_w3);
      toev_constanten_aan_sql( p_constante => ':CONST4', p_waarde => r_cde.cde_const_w4);
      toev_constanten_aan_sql( p_constante => ':CONST5', p_waarde => r_cde.cde_const_w5);
      toev_constanten_aan_sql( p_constante => ':CONST6', p_waarde => r_cde.cde_const_w6);
      toev_constanten_aan_sql( p_constante => ':CONST7', p_waarde => r_cde.cde_const_w7);
      toev_constanten_aan_sql( p_constante => ':CONST8', p_waarde => r_cde.cde_const_w8);
      toev_constanten_aan_sql( p_constante => ':CONST9', p_waarde => r_cde.cde_const_w9);
      toev_constanten_aan_sql( p_constante => ':CONSTA', p_waarde => r_cde.cde_const_w10);
      --
    end if;
    --
    if r_cde.sqltext is not null
    then
      g_sql_tekst := g_sql_tekst ||' and exists ( '|| r_cde.sqltext ||' )';
      g_sql_tekst := replace(g_sql_tekst, ':NUMRELATIE' , 'rle.numrelatie');
      --
      toev_variabelen_aan_sql( p_naam_variabele => ':VAR1', p_datatype => r_cde.var1_datatype, p_waarde => r_cde.gvw_var1);
      toev_variabelen_aan_sql( p_naam_variabele => ':VAR2', p_datatype => r_cde.var2_datatype, p_waarde => r_cde.gvw_var2);
      toev_variabelen_aan_sql( p_naam_variabele => ':VAR3', p_datatype => r_cde.var3_datatype, p_waarde => r_cde.gvw_var3);
      toev_variabelen_aan_sql( p_naam_variabele => ':VAR4', p_datatype => r_cde.var4_datatype, p_waarde => r_cde.gvw_var4);
      toev_variabelen_aan_sql( p_naam_variabele => ':VAR5', p_datatype => r_cde.var5_datatype, p_waarde => r_cde.gvw_var5);
      --
    end if;
    --
  end loop;
  --
  if p_testrun = 'N'
  then
    l_status := 'AGT'; -- Aangemaakt
  else
    l_status := 'TST'; -- Test
  end if;
  --
  if g_cpe_id is null
  then
    select rle_cpe_seq.nextval
    into   g_cpe_id
    from   dual;
  end if;
  --
  -- Toevoegen cpe_id aan sql
  g_sql_tekst := replace( g_sql_tekst, ':CPE_ID' , g_cpe_id );
  --
  if not g_herstart
  then
    --
    -- Nu is sql-statement helemaal klaar en kan een groep worden aangemaakt
    -- Hierbij vind een implicitie conversie plaats voor het veld 'gebruikt_sql_statement' van varchar2 naar clob
    -- Explicitie conversie mbv functie to_clob geeft namelijk problemen bij aanroep vanuit forms
    insert into rle_campagnes
      ( id
      , cde_id
      , aanmaakmoment
      , status
      , briefcode
      , technische_procesnaam
      , werkbak
      , code_soort_adres
      , gebruikt_sql_statement
      , aanmaakmoment_brieven
      , creatie_door
      , dat_creatie
      , mutatie_door
      , dat_mutatie
      )
    values
      ( g_cpe_id
      , p_cde_id
      , sysdate
      , l_status
      , null
      , null
      , null
      , null
      , g_sql_tekst -- LET OP: bewuste impliciete conversie
      , null
      , user
      , sysdate
      , user
      , sysdate
      );
  end if;
  -- In onderstaande loop worden alle relaties per 1000 stuks opgehaald en worden
  -- bulksgewijs weggeschreven naar de tabel rle_relaties_in_groepen
  open c_rle_ref_cursor for g_sql_tekst;
  loop
    --
    ric_tab.delete;
    --
    fetch c_rle_ref_cursor bulk collect
    into  ric_tab limit 1000;
    --
    exit when ric_tab.count = 0;
    --
    forall i in ric_tab.first..ric_tab.last
    insert into rle_relaties_in_campagnes
    values ric_tab(i);
    --
    g_aantal_rle_in_campagnes := g_aantal_rle_in_campagnes + ric_tab.count;
    --
    if p_testrun = 'N'
    then
      stm_util.schrijf_herstarttelling('g_aantal_rle_in_campagnes' , g_aantal_rle_in_campagnes);
    end if;
    --
    commit; -- commit per bulk collect
    --
  end loop;
  --
  close c_rle_ref_cursor;
  --
  -- voor het geval 0 records worden geselecteerd hier een extra commit voor opslag van de campagne
  commit;
  --
end SELECTIE_EN_OPSLAG;
PROCEDURE TESTRUN
 (P_CDE_ID IN number
 ,P_CPE_ID OUT number
 )
 IS
  /******************************************************************************************************
  Naam:         TESTRUN
  Beschrijving: Het doel van deze functie is het uitvoeren van een testrun.
                De selectie wordt hierbij beperkt tot een max aantal op te halen rijen.


  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  29-07-2008  PAS  Nieuwbouw
  *******************************************************************************************************/
  --
begin
  --
  g_cpe_id := null;
  --
  selectie_en_opslag( p_cde_id           => p_cde_id
                    , p_testrun          => 'J'
                    );
  p_cpe_id := g_cpe_id; -- vullen out-parameter
  --
exception
  when others
  then
    rollback;
    stm_log.debug(p_msg => g_sql_tekst);
    stm_log.debug(p_msg => dbms_utility.format_error_backtrace);
    raise;
end TESTRUN;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN VARCHAR2
 ,P_CDE_ID IN NUMBER
 )
 IS
  /******************************************************************************************************
  Naam:         VERWERK
  Beschrijving: Het doel van deze functie is het aanmaken van een set van relaties welke aan
                voldoen aan de opgegeven selectie-criteria vastgelegd in de campagne tool
                Deze procedure wordt aangeroepen vanuit de JCS-procedure RLEPRC41.

                Ter info:
                JCS-procedure RLEPRC42 is de volgende stap. Deze batch maakt de XML-bestanden
                aan voor StreamServe.

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  24-06-2008 HSI  Nieuwbouw
  27-11-2009 PAS  Diverse aanpassingen (o.a. inbouw herstart)
  *******************************************************************************************************/
  l_volgnummer             number :=0;
  l_vorig_procedure        varchar2(80);
  --
begin
  --
  l_vorig_procedure         := stm_util.t_procedure;
  stm_util.t_procedure      := 'VERWERK';
  stm_util.t_script_naam    := p_script_naam;
  stm_util.t_script_id      := to_number(p_script_id);
  stm_util.t_programma_naam := 'RLE_CMP_SEL';
  --
  if stm_util.herstart
  then
    g_herstart := true;
    /* Ophalen herstartsleutel */
    g_cpe_id      := stm_util.lees_herstartsleutel('l_herstart_id');
    /* Herinitialiseren tellingen */
    g_regelnummer         := stm_util.lees_herstarttelling('g_regelnummer');
    g_aantal_rle_in_campagnes := stm_util.lees_herstarttelling('g_aantal_rle_in_campagnes');
    --
    schrijf_naar_logverslag('* * * * * Herstart RLE_CMP_SEL voor campagnes '||to_char(g_cpe_id)|| ' * * * * * ');
    schrijf_naar_logverslag('Datum : '|| TO_CHAR(sysdate,'dd-mm-yyyy hh24:mi'));
    schrijf_naar_logverslag('');
    --
    -- Sql tekst aanpassen zodat al geselecteerde relaties niet nogmaals worden geselecteerd;
    --
  else
    g_herstart := false;
    /* Initialiseren herstartsleutel = ID van campagnes*/
    select rle_cpe_seq.nextval
    into   g_cpe_id
    from   dual;
    --
    -- Maak de koptekst van het verwerkingsverslag aan
    schrijf_naar_logverslag('* * * * * RLE_CMP_SEL Campagne Tool ( Campagne Nr: '||to_char(g_cpe_id)||') * * * * * ');
    schrijf_naar_logverslag('Datum : '|| TO_CHAR(sysdate,'dd-mm-yyyy hh24:mi:ss'));
    schrijf_naar_logverslag('');
    --
    stm_util.schrijf_herstartsleutel('l_herstart_id'         , g_cpe_id);
    stm_util.schrijf_herstarttelling('g_regelnummer'         , g_regelnummer);
    stm_util.schrijf_herstarttelling('g_aantal_rle_in_campagnes' , g_aantal_rle_in_campagnes);
    --
  end if ;
  --
  selectie_en_opslag( p_cde_id           => p_cde_id
                    , p_testrun          => 'N'
                    );
  --
  schrijf_naar_logverslag('Aantal geselecteerde relaties: '||to_char(g_aantal_rle_in_campagnes));
  schrijf_naar_logverslag('');
  schrijf_naar_logverslag('Einde van procedure ' || stm_util.t_programma_naam ||' op ' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss') );
  --
  stm_util.insert_job_statussen(l_volgnummer,'S','0');
  stm_util.verwijder_herstart;
  commit;
  --
  stm_util.t_procedure := l_vorig_procedure;
  --
exception
  when others
  then
    rollback;
    --
    stm_util.insert_job_statussen(l_volgnummer,'S','1' ) ;
    stm_util.insert_job_statussen(l_volgnummer,'I',substr(stm_util.t_procedure,1,80)) ;
    stm_util.insert_job_statussen(l_volgnummer,'I',substr(sqlerrm ,1,255) ) ;
    commit;
    stm_log.debug(p_msg => g_sql_tekst);
    raise_application_error(-20000, stm_app_error(sqlerrm, null, null)||' '||dbms_utility.format_error_backtrace );
end VERWERK;

END RLE_CMP_SEL;
/