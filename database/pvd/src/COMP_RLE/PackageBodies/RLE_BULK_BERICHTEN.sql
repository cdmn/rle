CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_BULK_BERICHTEN IS
---------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is aanmaken van plaatsingsberichten
-- op basis van aangeleverde lijst met persoonsnummers.
--------------------------------------------------------------
 -- global t.b.v. logverslag
  g_lijstnummer         number     := 1;
  g_regelnr                  number     := 0;
  g_scriptnaam          varchar2(10);
  g_script_id               number;

PROCEDURE LIJST
 (P_TEXT IN VARCHAR2
 );
PROCEDURE AANMAKEN_BERICHT
 (P_SOORT_BERICHT IN VARCHAR2
 );


PROCEDURE LIJST
 (P_TEXT IN VARCHAR2
 )
 IS
begin
  stm_util.insert_lijsten( g_lijstnummer
                         , g_regelnr
                         , p_text
                         );
end lijst;
PROCEDURE AANMAKEN_BERICHT
 (P_SOORT_BERICHT IN VARCHAR2
 )
 IS
  -- cursor aangeboden persoonsnummers
  cursor c_prs
  is
    select rbb.persoonsnummer
        ,  rle.relatienummer
        ,  rle.sofinummer
        ,  adr.postkode_bin
        ,  adr.wpl_woonplaatskode_bin
        ,  adr.code_adressoort
        ,  adr.ind_buitenland
        ,  prs.a_nummer
        ,  prs.geboortedatum
        ,  prs.geslacht
        ,  per_lib.bepaal_gemeentenummer ( adr.ind_buitenland
                                         , adr.wpl_woonplaatskode_bin
                                         , adr.postkode_bin) gemeentenummer
    from   rle_bulk_berichten_ext rbb
    left join rle_mv_personen rle
    on     rbb.persoonsnummer = rle.persoonsnummer
    left join adressen adr
    on     rbb.persoonsnummer = adr.prs_persoonsnummer
    left join personen prs
    on     rbb.persoonsnummer = prs.persoonsnummer
    order by rbb.persoonsnummer asc ;

   -- cursor ter controle status OV en N
   cursor c_rag ( b_relatienummer number)
   is
   select rag.code_gba_status||'/'||rag.afnemers_indicatie gba_status
   from   rle_afstemmingen_gba rag
   where  rag.rle_numrelatie = b_relatienummer
    ;

  --
  r_rag                 c_rag%rowtype;
  l_status              varchar2(10);
  --
  l_database            varchar2(255);
  l_uitvallijst         utl_file.file_type;
  l_bestandsnaam        varchar2(255);
  --
  l_teller_rle_ng       number:= 0;
  l_teller_bsn_ng       number:= 0;
  l_teller_bin_ng       number:= 0;
  l_teller_afs_ng       number:= 0;
  l_teller_overig_ng    number:= 0;
  l_teller_totaal_ng    number:= 0;
  l_teller_bericht      number:= 0;
  l_bericht_aanm        varchar2 (1):= 'J';
  l_pers_vorig          number:= 0;
  --
begin
  --
  -- bestandsnaam formeren
  select case
           when regexp_substr ( global_name
                             , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;
  -- genereren bestandsnaam
  l_bestandsnaam := l_database || g_scriptnaam
                               || '_'     || 'RLE_UITVAL_BULK_BERICHTEN'
                               || '_'     || to_char(sysdate, 'yyyymmdd')
                               || '_'     || to_char(g_script_id);

  -- openen van de filehandler
  l_uitvallijst := utl_file.fopen ( 'RLE_OUTBOX'
                                , l_bestandsnaam||'.CSV'
                                , 'W'
                               );
  -- schrijf de header records
  utl_file.put_line ( l_uitvallijst
                    , 'persoonsnummer;reden_uitval'
                    );

  if p_soort_bericht = 'PLAATSINGSBERICHT'
  then
    l_status := 'OV/N';
  elsif p_soort_bericht = 'VERWIJDERBERICHT'
  then
    l_status := 'OV/J';
  end if;


  -- uitvoeren controle
  for r_prs in c_prs
  loop
    --
    if r_prs.persoonsnummer <>l_pers_vorig
    then
      begin
      --initialiseren indicator aanmaken bericht
      l_bericht_aanm:= 'J';
      -- controle relatienummer in RLE
      if r_prs.relatienummer is null
      then
        -- teller bijhouden
        l_teller_rle_ng     := l_teller_rle_ng +1 ;
        l_teller_totaal_ng  := l_teller_totaal_ng + 1;
        -- persoonsnummer op uitvallijst
        utl_file.put_line ( l_uitvallijst
                          , r_prs.persoonsnummer || ';' ||'Relatienummer niet gevonden in RLE obv persoonsnummer'
                          );
        --
        l_bericht_aanm := 'N';
      -- controle BSN in RLE
      elsif  r_prs.sofinummer is null
      then
        -- teller bijhouden
        l_teller_bsn_ng := l_teller_bsn_ng + 1;
        l_teller_totaal_ng  := l_teller_totaal_ng + 1;
        -- persoonsnummer op uitvallijst
        utl_file.put_line ( l_uitvallijst
                          , r_prs.persoonsnummer || ';' ||' BSN niet gevonden in RLE obv persoonsnummer'
                          );
        --
        l_bericht_aanm := 'N';
      -- Adres niet Nederlands
      elsif ( r_prs.ind_buitenland = 'J' or r_prs.code_adressoort <> 'D')
      then
        -- teller bijhouden
        l_teller_bin_ng := l_teller_bin_ng + 1;
        l_teller_totaal_ng  := l_teller_totaal_ng + 1;
        -- persoonsnummer op uitvallijst
        utl_file.put_line ( l_uitvallijst
                          , r_prs.persoonsnummer || ';' ||'Adrestype <> DA of heeft indicatie buitenland'
                          );
        --
        l_bericht_aanm := 'N';
       -- status en indicatie in RLE niet gelijk aan OV/N
       else
         --
         open c_rag (r_prs.relatienummer);
         fetch c_rag into r_rag;
         close c_rag;
         --

         if r_rag.gba_status <> l_status
         then
           -- teller bijhouden
           l_teller_afs_ng := l_teller_afs_ng + 1;
           l_teller_totaal_ng  := l_teller_totaal_ng + 1;
           -- persoonsnummer op uitvallijst
           utl_file.put_line ( l_uitvallijst
                              , r_prs.persoonsnummer || ';' ||'Huidige status in RLE <> '||l_status
                              );
           --
           l_bericht_aanm := 'N';
         end if;
         -- aanmaken plaatsingsbericht
         if l_bericht_aanm = 'J'
         then
           if p_soort_bericht = 'PLAATSINGSBERICHT'
           then
             gba_lib.schrijven_ap01_bericht ( p_persoonsnummer   => r_prs.persoonsnummer
                                            , p_gemeentenummer   => r_prs.gemeentenummer
                                            , p_a_nummer         => r_prs.a_nummer
                                            , p_naam             => null
                                            , p_postcode         => r_prs.postkode_bin
                                            , p_geboortedatum    => r_prs.geboortedatum
                                            , p_straat           => null
                                            , p_huisnummer       => null
                                            , p_geslacht         => null
                                            , p_sofinummer       => r_prs.sofinummer
                                            ) ;
           elsif p_soort_bericht = 'VERWIJDERBERICHT'
           then
             gba_lib.sch_av01(p_gemeentenummer => r_prs.gemeentenummer
                             ,p_apn_id         =>  NULL
                             ,p_persoonsnummer => r_prs.persoonsnummer
                             ,p_a_nummer       => r_prs.a_nummer
                             );
           end if;
           -- teller bijhouden aantal aangemaakte plaatsings- of verwijderberichten
           l_teller_bericht := l_teller_bericht + 1;
           --
         end if;
         --
      end if;
      -- uitval overig
      exception
        when others
        then
          -- bijhouden verwerkte persoonsnummer/ per persoonsnummer 1 bericht
          l_pers_vorig :=  r_prs.persoonsnummer;
          -- teller uitval overig
          l_teller_overig_ng := l_teller_overig_ng + 1;
          l_teller_totaal_ng  := l_teller_totaal_ng + 1;
          --
          utl_file.put_line ( l_uitvallijst
                            , r_prs.persoonsnummer || ';' ||'Uitval overig, foutmelding :'|| substr (sqlerrm, 1, 100)
                            );
      --
      end;
      -- bijhouden verwerkte persoonsnummer/ per persoonsnummer 1 bericht
      l_pers_vorig :=  r_prs.persoonsnummer;
      --
     end if;
     --
  end loop;
  -- filehandle(s) sluiten
  utl_file.fclose_all;
  --
  lijst ('Aantal personen verwerkt     : '|| to_char (l_teller_bericht));
  lijst ('Aantal personen niet verwerkt: '|| to_char (l_teller_totaal_ng));
  lijst ('--------------------------------------- ');
  lijst (' ');
  lijst ('Aantallen per uitvalgroep: ');
  lijst ('--------------------------------------- ');
  lijst ('Relatienummer niet gevonden in RLE                   : '||to_char (l_teller_rle_ng));
  lijst ('BSN niet gevonden                                    : '||to_char (l_teller_bsn_ng));
  lijst ('DA met buitenland indicatie                          : '|| to_char (l_teller_bin_ng));
  lijst ('Status/afn ind in RLE niet gelijk aan '||l_status||'           : '|| to_char (l_teller_afs_ng) );
  lijst ('Uitval overig                                        : '|| to_char (l_teller_overig_ng));
  lijst (' ');
  lijst ('Uitvalbestand '||l_bestandsnaam||'.csv aangemaakt op de ftp pool(RLE inbox)!' );
  lijst ( ' ' );
  lijst ('--------------------------------------------------------------------------------------------------------------------------');
  --

end aanmaken_bericht;
/* Aanmaken bulk berichten */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_SOORT_BERICHT IN VARCHAR2
 )
 IS
--
  l_volgnummer          stm_job_statussen.volgnummer%type := 0;
  e_abort_batch         exception;
  l_aantal_aangeboden   number := 0;
begin
  stm_util.t_procedure       := 'VERWERK';
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_BULK_BERICHTEN';
  --scriptnaam en id nodig bij het samenstellen van bestandsnaam
  g_scriptnaam := p_script_naam;
  g_script_id  := p_script_id;
  -- Schrijven logbestand
  lijst ( rpad ('***** LOGVERSLAG RLEPRC75: RLE_BULK_BERICHTEN  *****', 70)
               || ' STARTDATUM: ' || to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  lijst ( ' ' );
  lijst ('--------------------------------------------------------------------------------------------------------------------------');
  lijst ( ' ' );
  lijst ( 'Input bestand                    : BULK_BERICHTEN.csv' );
  lijst ( 'Parameter p_soort_bericht        : '||p_soort_bericht );
   --l_aantal_aangeleverd := SQL%ROWCOUNT;
  select count(*)
  into   l_aantal_aangeboden
  from   rle_bulk_berichten_ext
  where  persoonsnummer is not null;
  --
  lijst ('Aantal persoonsnummers aangeboden: ' || to_char(l_aantal_aangeboden));
  lijst ( ' ' );
  -- aanmaken bericht
  aanmaken_bericht (p_soort_bericht => p_soort_bericht);
  --  Voettekst van lijsten
  lijst ( '******* EINDE VERSLAG RLE_BULK_BERICHTEN - EINDDATUM: '||to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss')||'*******');
  stm_util.debug ('Einde logverslag');
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  -- opslaan
  commit;

exception
  when others
  then
    rollback;
    stm_util.t_foutmelding := substr(sqlerrm,1,500);
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);
    lijst( substr(sqlerrm,1,500) );

    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(stm_util.t_foutmelding,1,250)
                                  );
    commit;
end verwerk;

END RLE_BULK_BERICHTEN;
/