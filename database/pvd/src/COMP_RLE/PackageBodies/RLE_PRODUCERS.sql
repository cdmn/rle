CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_PRODUCERS IS

PROCEDURE RLE_PRS_EVENT_PRODUCER
 (P_NUMRELATIE ovt_ind_overeenkomsten.rle_numrelatie%TYPE
 ,P_DAT_BEGIN date := null
 )
 IS
/******************************************************************
  Naam:         RLE_PRS_EVENT_PRODUCER
  Beschrijving: Start PRS Event voor afleiden dd periodes

  Datum       Wie  Wat
  ----------  ---  ------------------------------------------------
  06-01-2009  XCW  Aangemaakt
 ******************************************************************* */
  --
  cursor c_rgg ( b_rle_numrelatie in rle_relatierollen_in_adm.rle_numrelatie%type )
  is
  select
    rgg.code rgg_code
  from
    rle_relatierollen_in_adm rie
  , pdt_regelingen rgg
  where
    rie.adm_code = rgg.adm_code and
    rie.rle_numrelatie = b_rle_numrelatie;
  --
  r_rgg               c_rgg%rowtype;
  cn_rle_numrelatie   ovt_ind_overeenkomsten.rle_numrelatie%TYPE;
  cn_aanleiding       varchar2(100);
  --
BEGIN
  --
  cn_aanleiding     := 'Gebdat-Ovldat-Mut';
  --
  if p_numrelatie is not null
  then
    --
    for r_rgg in c_rgg ( p_numrelatie )
    loop
      --
      ovt_evt_producer.prs_mutatie ( p_rgg_code           =>  r_rgg.rgg_code
                                   , p_prs_rle_numrelatie =>  p_numrelatie
                                   , p_prs_ingangsdatum   =>  p_dat_begin
                                   , p_aanleiding         =>  cn_aanleiding
                                   );
      --
    end loop;
  end if;
  --
END;
END RLE_PRODUCERS;
/