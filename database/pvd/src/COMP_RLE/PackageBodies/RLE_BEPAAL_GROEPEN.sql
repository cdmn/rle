CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_BEPAAL_GROEPEN IS

--------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is bepalen van de groepen en dit vastleggen in synchronisatie_statussen.
-- FO: RLE1003
-- Wanneer     Wie               Wat
--------------------------------------------------------
-- 03-02-2014  WHR               Creatie
-- 28-7-2014   MAL               Procedure maak_detailbestand toegevoegd ivm Herba thermometer
-----------------------------------------------------------------------------------------------


-- global t.b.v. logverslag
g_lijstnummer    number        := 1;
g_regelnr        number        := 0;
g_script_id      number;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
PROCEDURE VERWERK_GBA_IST;
PROCEDURE VERWERK_GROEPEN;
PROCEDURE RAPPORTAGE_GROEPEN;
/* Aanmaken detailbestand */
PROCEDURE MAAK_DETAILBESTAND;


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
begin
    stm_util.insert_lijsten( g_lijstnummer
                           , g_regelnr
                           , p_text
                           );
end lijst;
PROCEDURE VERWERK_GBA_IST
 IS

  -- Relatienummers GBA IST J
  cursor c_gba_ist_j
  is
    SELECT rec.rle_numrelatie
    FROM   rle_gba_ist_tmp gba
    JOIN   rle_relatie_externe_codes rec
    ON     gba.bsn = rec.extern_relatie
    AND    rec.rol_codrol = 'PR'
    AND    rec.dwe_wrddom_extsys = 'SOFI'
    MINUS
    SELECT scs.rle_numrelatie
    FROM   rle_synchronisatie_statussen scs
    WHERE  scs.gba_afnemers_indicatie = 'J';

  -- Relatienummers GBA IST N
  cursor c_gba_ist_n
  is
    SELECT scs.rle_numrelatie
    FROM   rle_synchronisatie_statussen scs
    WHERE  nvl(scs.gba_afnemers_indicatie,'-1') <> 'N'
    MINUS
    SELECT rec.rle_numrelatie
    FROM   comp_rle.rle_gba_ist_tmp gba
    JOIN   rle_relatie_externe_codes rec
    ON     gba.bsn = rec.extern_relatie
    AND    rec.rol_codrol = 'PR'
    AND    rec.dwe_wrddom_extsys = 'SOFI';

  -- GBA IST niet gevonden in RLE
  cursor c_gba_ist_niet_gevonden
  is
    SELECT gba.a_nummer
         , gba.bsn
    FROM   rle_gba_ist_tmp gba
    WHERE  NOT EXISTS
           (SELECT 1
            FROM   rle_relatie_externe_codes rec
            WHERE  gba.bsn = rec.extern_relatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'SOFI'
            );

  --
  l_aantal_aangeleverd   number := 0;
  l_aantal_verwerkt_scs  number := 0;
  l_aantal_niet_gevonden number := 0;
  l_aantal_gba_indicatie number := 0;
  --
begin
  stm_util.t_procedure       := 'VERWERK_GBA_IST';

  -- Tijdelijke GBA_IST tabel leegmaken
  execute immediate 'TRUNCATE TABLE rle_gba_ist_tmp';

  -- Haal Data over van external table naar tijdelijke tabel
  INSERT INTO rle_gba_ist_tmp
    ( a_nummer
    , bsn
    )
    (SELECT a_nummer
          , bsn
     FROM   rle_gba_ist_ext
    );
  l_aantal_aangeleverd := SQL%ROWCOUNT;
  lijst ('GBA IST aantal aangeleverd      : ' || to_char(l_aantal_aangeleverd));

  commit;

  -- GBA IST J
  for r_gba_ist_j
  in  c_gba_ist_j
  loop

    rle_update_syn_status
    ( p_numrelatie    => r_gba_ist_j.rle_numrelatie
    , p_gba_indicatie => 'J'
    );

    l_aantal_verwerkt_scs := l_aantal_verwerkt_scs+1;
  end loop;
  lijst ('GBA IST J aantal verwerkt         : ' || to_char(l_aantal_verwerkt_scs));

  commit;

  -- GBA IST N

  l_aantal_verwerkt_scs := 0;

  for r_gba_ist_n
  in  c_gba_ist_n
  loop

    rle_update_syn_status
    ( p_numrelatie    => r_gba_ist_n.rle_numrelatie
    , p_gba_indicatie => 'N'
    );

    l_aantal_verwerkt_scs := l_aantal_verwerkt_scs+1;
  end loop;
  lijst ('GBA IST N aantal verwerkt         : ' || to_char(l_aantal_verwerkt_scs));

  commit;

  SELECT COUNT(*)
  INTO   l_aantal_gba_indicatie
  FROM   rle_synchronisatie_statussen scs
  WHERE  scs.gba_afnemers_indicatie = 'J';
  lijst ('GBA IST J aantal na verwerking    : ' || to_char(l_aantal_gba_indicatie));

  -- GBA IST waarvan BSN niet gevonden in RLE
  lijst (' ');
  lijst ('GBA IST waarvan BSN niet gevonden in RLE:');
  lijst ('BSN       A-nummer');
  for r_gba_ist_niet_gevonden
  in  c_gba_ist_niet_gevonden
  loop
    lijst ( r_gba_ist_niet_gevonden.bsn || ' ' || r_gba_ist_niet_gevonden.a_nummer );
    l_aantal_niet_gevonden := l_aantal_niet_gevonden+1;
  end loop;
  lijst (' ');
  lijst ('GBA IST aantal niet gevonden    : ' || to_char(l_aantal_niet_gevonden));
  lijst (' ');

  commit;

end verwerk_gba_ist;
PROCEDURE VERWERK_GROEPEN
 IS
  -- Cursor bepaling Groep
  cursor c_scs
  is
    SELECT *
    FROM   (SELECT scs.rle_numrelatie
                  ,CASE
                     WHEN scs.rle_gewenste_indicatie = 'J' -- RLE SOLL
                          AND bsn.extern_relatie IS NULL -- BSN leeg
                          AND anr.extern_relatie IS NOT NULL -- ANR gevuld
                      THEN
                      '11'
                     WHEN scs.rle_gewenste_indicatie = 'J' -- RLE SOLL
                          AND bsn.extern_relatie IS NULL -- BSN leeg
                          AND anr.extern_relatie IS NULL -- ANR gevuld
                      THEN
                      '12'
                     WHEN scs.rle_gewenste_indicatie = 'J' -- RLE SOLL
                          AND asa.afnemers_indicatie = 'N' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'N' -- GBA IST
                      THEN
                      '05'
                     WHEN scs.rle_gewenste_indicatie = 'J' -- RLE SOLL
                          AND asa.afnemers_indicatie = 'J' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'N' -- GBA IST
                      THEN
                      '06'
                     WHEN scs.rle_gewenste_indicatie = 'J' -- RLE SOLL
                          AND asa.afnemers_indicatie = 'N' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'J' -- GBA IST
                      THEN
                      '07'
                     WHEN scs.rle_gewenste_indicatie = 'J' -- RLE SOLL
                          AND asa.afnemers_indicatie = 'J' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'J' -- GBA IST
                      THEN
                      '08'
                     WHEN (scs.rle_gewenste_indicatie = 'N' OR scs.rle_gewenste_indicatie IS NULL) -- RLE SOLL
                          AND asa.afnemers_indicatie = 'J' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'N' -- GBA IST
                      THEN
                      '09A'
                     WHEN (scs.rle_gewenste_indicatie = 'N' OR scs.rle_gewenste_indicatie IS NULL) -- RLE SOLL
                          AND asa.afnemers_indicatie = 'J' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'J' -- GBA IST
                      THEN
                      '09B'
                     WHEN (scs.rle_gewenste_indicatie = 'N' OR scs.rle_gewenste_indicatie IS NULL) -- RLE SOLL
                          AND asa.afnemers_indicatie = 'N' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'J' -- GBA IST
                      THEN
                      '10'
                     WHEN (scs.rle_gewenste_indicatie = 'N' OR scs.rle_gewenste_indicatie IS NULL) -- RLE SOLL
                          AND asa.afnemers_indicatie = 'N' -- RLE IST
                          AND scs.gba_afnemers_indicatie = 'N' -- GBA IST
                      THEN
                      '13'
                     ELSE
                      '99'
                   END AS groep_new
                  ,scs.groep
                  ,scs.datum_groep
            FROM   rle_synchronisatie_statussen scs
            LEFT   OUTER JOIN rle_afstemmingen_gba asa
            ON     scs.rle_numrelatie = asa.rle_numrelatie
            LEFT   OUTER JOIN rle_relatie_externe_codes bsn
            ON     scs.rle_numrelatie = bsn.rle_numrelatie
            AND    bsn.rol_codrol = 'PR'
            AND    bsn.dwe_wrddom_extsys = 'SOFI'
            LEFT   OUTER JOIN rle_relatie_externe_codes anr
            ON     scs.rle_numrelatie = anr.rle_numrelatie
            AND    anr.rol_codrol = 'PR'
            AND    anr.dwe_wrddom_extsys = 'GBA')
    WHERE  nvl(groep_new, '-1') <> nvl(groep, '-1');

  l_aantal_verwerkt_scs  number := 0;
begin
  stm_util.t_procedure       := 'VERWERK_GROEPEN';

  -- Bepalen groep
  for r_scs in c_scs
  loop

    rle_update_syn_status
    ( p_numrelatie => r_scs.rle_numrelatie
    , p_groep      => r_scs.groep_new
    );

    l_aantal_verwerkt_scs := l_aantal_verwerkt_scs+1;
  end loop;
  lijst ('Bepalen groep, aantal verwerkt : ' || to_char(l_aantal_verwerkt_scs));

  commit;

end verwerk_groepen;
PROCEDURE RAPPORTAGE_GROEPEN
 IS


  -- Cursor rapportage per groep
  cursor c_grp
  is
    SELECT scs.groep                            AS groep
          ,nvl(scs.rle_gewenste_indicatie, 'N') AS rle_soll
          ,nvl(asa.afnemers_indicatie    , ' ') AS rle_ist
          ,nvl(scs.gba_afnemers_indicatie, ' ') AS gba_ist
          ,COUNT(1)                             AS aantal
    FROM   rle_synchronisatie_statussen scs
    LEFT   OUTER JOIN rle_afstemmingen_gba asa
    ON     scs.rle_numrelatie = asa.rle_numrelatie
    GROUP  BY scs.groep
             ,nvl(scs.rle_gewenste_indicatie,'N')
             ,nvl(asa.afnemers_indicatie    ,' ')
             ,nvl(scs.gba_afnemers_indicatie,' ')
    ORDER  BY groep;

  -- Cursor rapportage per groep en indicatie_reden
  cursor c_grp_reden
  is
    SELECT scs.groep                            AS groep
          ,nvl(scs.rle_gewenste_indicatie, 'N') AS rle_soll
          ,nvl(asa.afnemers_indicatie    , ' ') AS rle_ist
          ,nvl(scs.gba_afnemers_indicatie, ' ') AS gba_ist
          ,CASE
             WHEN scs.reden IS NOT NULL THEN
              'J'
             ELSE
              'N'
           END                                  AS indicatie_reden
          ,COUNT(*)                             AS aantal
    FROM   rle_synchronisatie_statussen scs
    LEFT   OUTER JOIN rle_afstemmingen_gba asa
    ON     scs.rle_numrelatie = asa.rle_numrelatie
    GROUP  BY scs.groep
             ,nvl(scs.rle_gewenste_indicatie,'N')
             ,nvl(asa.afnemers_indicatie    ,' ')
             ,nvl(scs.gba_afnemers_indicatie,' ')
             ,CASE
                WHEN scs.reden IS NOT NULL THEN
                 'J'
                ELSE
                 'N'
              END
    ORDER  BY groep
             ,indicatie_reden;
begin
  stm_util.t_procedure       := 'RAPPORTAGE_GROEPEN';

  lijst (' ');
  lijst ('Rapportage per groep:');
  lijst ('GROEP RLE_SOLL RLE_IST GBA_IST     AANTAL');
  for r_grp
  in  c_grp
  loop
    lijst ( rpad(r_grp.groep,6)    ||
            rpad(r_grp.rle_soll,9) ||
            rpad(r_grp.rle_ist,8)  ||
            rpad(r_grp.gba_ist,8)  ||
            lpad(to_char(r_grp.aantal, '999G999G990', 'NLS_NUMERIC_CHARACTERS=,.'),10)
          );
  end loop;
  lijst (' ');

  commit;

  lijst (' ');
  lijst ('Rapportage per groep en indicatie_reden:');
  lijst ('GROEP RLE_SOLL RLE_IST GBA_IST INDICATIE_REDEN     AANTAL');
  for r_grp_reden
  in  c_grp_reden
  loop
    lijst ( rpad(r_grp_reden.groep,6)            ||
            rpad(r_grp_reden.rle_soll,9)         ||
            rpad(r_grp_reden.rle_ist,8)          ||
            rpad(r_grp_reden.gba_ist,8)          ||
            rpad(r_grp_reden.indicatie_reden,16) ||
            lpad(to_char(r_grp_reden.aantal, '999G999G990', 'NLS_NUMERIC_CHARACTERS=,.'),10)
          );
  end loop;
  lijst (' ');

  commit;
  --
end RAPPORTAGE_groepen;
/* Aanmaken detailbestand */
PROCEDURE MAAK_DETAILBESTAND
 IS

cursor c_rle
is
with ads as
  (select ras.rle_numrelatie
         ,ads.postcode
         ,ads.huisnummer
         ,ads.toevhuisnum
		 ,case
		  when
		  lnd.codland = 'NL'
		  then
		  'N'
		  else
		  'J'
		  end ind_buitenland
         ,lnd.naam  landnaam
         ,lnd.codland lnd_codland
      from rle_relatie_adressen ras
          ,rle_adressen ads
          ,rle_woonplaatsen wps
          ,rle_landen lnd
      where ras.ads_numadres = ads.numadres
      and   wps.woonplaatsid = ads.wps_woonplaatsid
      and   wps.lnd_codland = lnd.codland
      and   ras.dwe_wrddom_srtadr = 'DA'
      and   ras.datingang <= sysdate
      and   nvl(ras.dateinde, sysdate) >= sysdate
  )
select rec.extern_relatie persoonsnummer
      ,rss.groep
      ,ads.postcode
      ,ads.huisnummer
      ,ads.toevhuisnum
      ,ads.ind_buitenland
      ,ads.lnd_codland
	  , ads.landnaam
      ,(select wm_concat (rie.adm_code)
        from rle_relatierollen_in_adm rie
        where rie.rle_numrelatie = rss.rle_numrelatie
        and rie.rol_codrol = 'PR'
        group by rie.rle_numrelatie) as administraties
      ,bsn.extern_relatie bsn
      ,anr.extern_relatie anr
      ,rss.reden
      ,rss.datum_reden
from rle_synchronisatie_statussen rss
    ,rle_relatie_externe_codes rec
    ,rle_relatie_externe_codes bsn
    ,rle_relatie_externe_codes anr
    ,ads
where rss.groep not in ('13', '08')
and   reden is null
and   ads.rle_numrelatie(+) = rss.rle_numrelatie
and   rec.rle_numrelatie = rss.rle_numrelatie
and   bsn.rle_numrelatie(+) = rss.rle_numrelatie
and   anr.rle_numrelatie(+) = rss.rle_numrelatie
and   rec.dwe_wrddom_extsys = 'PRS'
and   bsn.dwe_wrddom_extsys (+) = 'SOFI'
and   anr.dwe_wrddom_extsys (+) = 'GBA'
;

fh_bestand       utl_file.file_type;
l_teller         integer := 0;
l_bestandsnaam   varchar2(250);
l_database       varchar2(20);
begin

  select case
           when regexp_substr ( global_name
                             , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;

  l_bestandsnaam := l_database||'RLEPRC69_RLE_BEPAAL_GROEPEN_'||to_char(sysdate, 'yyyymmdd')||'_'||g_script_id|| '.csv';

  fh_bestand := utl_file.fopen ('RLE_OUTBOX'
                               ,l_bestandsnaam
                               ,'W'
                               );

  --schrijven kopregel
  utl_file.put_line (fh_bestand
                    ,'PERSOONSNUMMER'         ||';'||
                     'GROEPSNUMMER'           ||';'||
                     'POSTCODE DA'            ||';'||
                     'HUISNUMMER DA'          ||';'||
                     'TOEVOEGING DA'          ||';'||
                     'LANDNAAM'               ||';'||
                     'ADMINISTRATIECODE'      ||';'||
                     'BSN'                    ||';'||
                     'A-NUMMER'               ||';'||
                     'INDICATIE REDEN'        ||';'||
                     'DATUM REDEN'
                    );

  for r_rle in c_rle
  loop

    utl_file.put_line (fh_bestand
                      ,r_rle.persoonsnummer     ||';'||
                       r_rle.groep              ||';'||
                       r_rle.postcode           ||';'||
                       r_rle.huisnummer         ||';'||
                       r_rle.toevhuisnum        ||';'||
                       r_rle.landnaam           ||';'||
                       r_rle.administraties     ||';'||
                       r_rle.bsn                ||';'||
                       r_rle.anr                ||';'||
                       r_rle.reden              ||';'||
                       r_rle.datum_reden
                      );
    l_teller := l_teller + 1;

  end loop;

  lijst ( '');
  lijst ('Aanmaken Detailbestand.');
  lijst (rpad ('-',100,'-'));
  lijst (l_teller||' records weggeschreven naar bestand '||l_bestandsnaam);
  lijst ('Dit bestand staat op de FTP pool (RLE data/inbox');

  lijst ('');
end maak_detailbestand;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN number
 ,P_DETAILBESTAND IN VARCHAR2
 )
 IS

  --
  l_volgnummer          stm_job_statussen.volgnummer%type := 0;
  e_abort_batch         exception;
begin
  stm_util.t_procedure       := 'VERWERK';
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_BEPAAL_GROEPEN';

  g_script_id  := p_script_id;

  -- Schrijven logbestand
  lijst ( rpad ('***** LOGVERSLAG RLE_BEPAAL_GROEPEN  *****', 70)
               || ' DRAAIDATUM: ' || to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  lijst ( ' ' );
  lijst ( '--------------------------------------------------------------------------------' );
  lijst ( ' ' );

  -- Verwerk GBA_IST in Synchronisatie statussen
  verwerk_gba_ist;

  -- Verwerk GROEPEN in Synchronisatie statussen
  verwerk_groepen;

  -- Rapportage over de GROEPEN in Synchronisatie statussen
  rapportage_groepen;

  if p_detailbestand = 'J'
  then
    maak_detailbestand;
  end if;
  --  Voettekst van lijsten
  lijst ( '********** EINDE VERSLAG BEPAAL GROEPEN  **********');
  stm_util.debug ('Einde logverslag');
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  -- opslaan
  commit;

exception
  when others
  then
    rollback;
    stm_util.t_foutmelding := substr(sqlerrm,1,500);
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);
    lijst( substr(sqlerrm,1,500) );

    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(stm_util.t_foutmelding,1,250)
                                  );
    commit;
end verwerk;
END RLE_BEPAAL_GROEPEN;
/