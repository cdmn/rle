CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_BEP_RELNR IS


  /* implementatie van rle0978 */


PROCEDURE ZOEK_NUMRELATIE
 (P_ADMINISTRATIES IN rle_bep_relnr.t_adm_lijst
 ,P_NAAM IN varchar2
 ,P_GEBOORTEDATUM IN date
 ,P_POSTCODE IN varchar2
 ,P_PEILDATUM IN date
 ,P_RLE_NUMRELATIE OUT number
 ,P_MEERDERE_RESULTATEN OUT varchar2
 )
 IS

    l_dummy pls_integer;
begin

    /* controleren of het DA adres geraadpleegd mag worden */
    if p_administraties.count > 0
    then

      for i in 1..p_administraties.count
      loop
        begin
          select 1
          into   l_dummy
          from   rle_administraties
          where  ind_afstemmen_gba_tgstn= 'J'
          and    code = p_administraties(i);
        exception
          when no_data_found
          then  -- geen administratie gevonden
            stm_dbproc.raise_error('RLE-00978');
        end;
      end loop;

      begin
        select rle.numrelatie
        into   p_rle_numrelatie
        from   rle_relaties              rle
        ,      rle_relatie_adressen      ras
        ,      rle_adressen              ads
        where  ads.postcode          = p_postcode
        and    ras.ads_numadres      = ads.numadres
        and    ras.dwe_wrddom_srtadr = 'DA'
        and    rle.numrelatie        = ras.rle_numrelatie
        and    substr(rle.zoeknaam,1,4) = substr(p_naam,1,4)
        and    rle.datgeboorte = p_geboortedatum
        and    p_peildatum between ras.datingang and nvl(ras.dateinde,p_peildatum);
      exception
        when no_data_found
        then null; -- geen relatie gevonden
        when too_many_rows
        then p_meerdere_resultaten := 'J';
      end;
    end if;
END ZOEK_NUMRELATIE;
END RLE_BEP_RELNR;
/