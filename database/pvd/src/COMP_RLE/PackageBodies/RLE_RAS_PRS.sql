CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_RAS_PRS IS

-- Sub-Program Unit Declarations
PROCEDURE TELLINGEN
 (T_LIJSTNUMMER IN OUT NUMBER
 ,T_REGELNUMMER IN OUT NUMBER
 ,T_VOORNA IN VARCHAR2
 );
PROCEDURE AANMAKEN_BUITENLANDS_ADRES
 (P_LAND_LANDKODE_BUI IN VARCHAR2
 ,P_STRAATNAAM IN VARCHAR2
 ,P_WOONPLAATS IN VARCHAR2
 ,P_NUMADRES OUT NUMBER
 );

-- Sub-Program Units
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 )
 IS
-- PL/SQL Specification
--
  cursor c_rua
   is
   select rua.persoonsnummer
        , rua.werkgevernummer
        , rua.straatnaam
        , rua.postcode
        , rua.huisnummer
        , rua.huisnr_toev
        , rua.woonplaats
        , rua.ind_buitenland
        , rua.soort_adres
        , rua.landcode_bui
     from rle_uitval_adressen rua
    where rua.persoonsnummer  is not null
      and rua.werkgevernummer is null
    order by rua.persoonsnummer;
   --
   r_rua                      c_rua%rowtype;
   --
   cn_module              constant varchar2(25) := 'RLE_RAS_PRS';
   --
   t_volgnummer           number := 0;
   t_lijstnummer          number := 1;
   t_uitvallijst          number := 2;
   t_regelnummer          number := 0;
   t_numadres             number(7);
   --
   t_sqlerrm              varchar2(255);
   t_procedure            varchar2(80);
   t_vorige_procedure     varchar2(255);
   t_locatie              varchar2(50);
   --
   t_aantal_per           number := 0;
   t_aantal_fout          number := 0;
   --
   p_o_numadres           number(7);
   p_land                 varchar2(3) := 'NL';
   --
   lv_p_systeemcomp       varchar2( 100 ) := 'PRS' ;
   lv_p_codrol            varchar2( 100 ) := 'PR';
   ld_p_datparm           date            := sysdate;
   ln_p_o_relnum          number;
   --

-- PL/SQL Block
begin
   t_vorige_procedure        := stm_util.t_procedure;
   stm_util.t_procedure      := cn_module;
   stm_util.t_script_naam    := p_script_naam;
   stm_util.t_script_id      := p_script_id;
   stm_util.t_programma_naam := 'RLE_RAS_PRS';
   t_locatie := ' begin 001' ;
   --
   -- Maak de koptekst van het verwerkingsverslag aan
   --
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , 'RLE_RAS_PRS  Herstel conversie adressen personen'
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , 'Datum : '|| to_char(sysdate,'dd-mm-yyyy hh24:mi')
                          );
   --
   tellingen(t_lijstnummer, t_regelnummer,'voor');
   --
   stm_util.insert_lijsten( t_uitvallijst
                          , t_regelnummer
                          , 'Uitvallijst conversie relaties (personen)'
                          );
   stm_util.insert_lijsten( t_uitvallijst
                          , t_regelnummer
                          , 'Datum : '|| to_char(sysdate,'dd-mm-yyyy hh24:mi')
                          );
   stm_util.insert_lijsten(t_uitvallijst
                          ,t_regelnummer
                          , rpad('persoonsnummer ',15)
                            ||rpad('error',64)
                          );
   stm_util.insert_lijsten(t_uitvallijst
                          , t_regelnummer
                          , '-------------------------------------------------------------------------------'
                          );
   t_locatie := '002';
   --
   for r_rua in c_rua
   loop
      begin
        -- 04-07-2005 W3115/P3117 DFU aanroep naar ibm vervangen door aanroep naar wrapper
        --
        COMP_STM.GET_EXT_RLE ( r_rua.persoonsnummer, lv_p_systeemcomp, lv_p_codrol, ld_p_datparm, ln_p_o_relnum );
         --
         if ln_p_o_relnum is null
         then
            raise_application_error(-20000, 'Bij persoon '||r_rua.persoonsnummer||' is geen'
                                    ||' relatienummer gevonden.'
                                   );
         end if;
         --
         t_locatie := '003';
         if r_rua.ind_buitenland = 'N'
         then
            begin
               /* fouten afvangen met duidelijkere melding */
               rle_m15_ads_verwerk( p_land
                                  , r_rua.postcode
                                  , r_rua.huisnummer
                                  , r_rua.huisnr_toev
                                  , r_rua.woonplaats
                                  , r_rua.straatnaam
                                  , p_o_numadres
                                  );
            exception
               when others
               then
                  raise_application_error(-20000, 'aanmaken adres is mislukt voor adres  met postcode '||
                                                   r_rua.postcode||' en huisnr '||r_rua.huisnummer
                                         );
            end;
            --
            t_locatie := '004';
            insert into rle_relatie_adressen
               ( provincie
               , rle_numrelatie
               , rol_codrol
               , datingang
               , dateinde
               , numkamer
               , dwe_wrddom_srtadr
               , locatie
               , ind_woonboot
               , ind_woonwagen
               , id
               , codorganisatie
               , ads_numadres
               , indinhown
               )
            values
               ( null
               , ln_p_o_relnum
               , 'PR'
               , trunc(sysdate)
               , null
               , null
               , r_rua.soort_adres
               , null
               , 'N'
               , 'N'
               , rle_ras_seq1.nextval
               , 'A'
               , p_o_numadres
               , 'N'
               );
            t_locatie := '005';
            --
         else
            if r_rua.woonplaats is null
            then
               stm_util.debug('geen woonplaats gevuld bij btlandadres');
               stm_dbproc.raise_error(' woonplaats is leeg bij dit buitenlands adres');
            end if;
            --
            t_locatie := '006';
            aanmaken_buitenlands_adres( r_rua.landcode_bui
                                      , r_rua.straatnaam
                                      , r_rua.woonplaats
                                      , t_numadres
                                      );
            --
            t_locatie := '007';
            insert into rle_relatie_adressen
               ( provincie
               , rle_numrelatie
               , rol_codrol
               , datingang
               , dateinde
               , numkamer
               , dwe_wrddom_srtadr
               , locatie
               , ind_woonboot
               , ind_woonwagen
               , id
               , codorganisatie
               , ads_numadres
               , indinhown
               )
            values
               ( null
               , ln_p_o_relnum
               , 'PR'
               , trunc(sysdate)
               , null
               , null
               , r_rua.soort_adres
               , null
               , 'N'
               , 'N'
               , rle_ras_seq1.nextval
               , 'A'
               , t_numadres
               , 'N'
               );
               --
         end if;
         --
         t_locatie := '008';
         t_aantal_per := t_aantal_per + 1;
         --
         delete
           from rle_uitval_adressen
          where persoonsnummer = r_rua.persoonsnummer
              ;
         commit;
         --
      exception
         when others
         then
            t_aantal_fout := t_aantal_fout + 1;
            --
            t_sqlerrm := substr(stm_get_batch_message(sqlerrm),1,255);
            --
            stm_util.debug(r_rua.persoonsnummer);
            stm_util.insert_lijsten( t_uitvallijst
                                   , t_regelnummer
                                   , rpad(r_rua.persoonsnummer,10)||' '||t_sqlerrm
                                   );
            if substr(sqlerrm,5,3) in ('010','015','016')
            then
               raise_application_error(-20000, stm_get_batch_message(sqlerrm));
            end if;
            --
            if substr(sqlerrm,12,3) in ('ORU','PLS')
            then
               raise_application_error(-20000, stm_get_batch_message(sqlerrm));
            end if;
            --
      end;
      --
   end loop;
   --
   tellingen(t_lijstnummer, t_regelnummer,'na');
   --
   --
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , rpad('Aantal succesvol verwerkte personen: ', 51) || lpad(t_aantal_per, 5)
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , rpad('Aantal NIET geconverteerde persoon adressen: ', 51) || lpad(t_aantal_fout, 5)
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , 'EINDE  conversie adressen (personen)'
                          );
   --
   stm_util.insert_job_statussen( t_volgnummer
                                , 'S'
                                , '0'
                                );
   commit;
   --
   stm_util.t_procedure := t_vorige_procedure;
   --
exception
   when others
   then
      t_sqlerrm   := substr(stm_get_batch_message(sqlerrm),1,255);
      t_procedure := substr(stm_util.t_procedure,1,80);
      rollback;
      stm_util.debug(t_sqlerrm);
      --
      stm_util.insert_job_statussen(t_volgnummer,'S','1' ) ;
      stm_util.insert_job_statussen(t_volgnummer,'I',t_procedure) ;
      stm_util.insert_job_statussen(t_volgnummer,'I',t_sqlerrm ) ;
      --
      commit;
      --
      if c_rua%isopen
      then
         close c_rua;
      end if;
      --
      raise_application_error(-20000, stm_get_batch_message(sqlerrm)
                              ||' locatie: '||t_locatie
                             );
      --
end verwerk;

PROCEDURE TELLINGEN
 (T_LIJSTNUMMER IN OUT NUMBER
 ,T_REGELNUMMER IN OUT NUMBER
 ,T_VOORNA IN VARCHAR2
 )
 IS
-- PL/SQL Specification
--
   t_alle_prs_uitval_adressen       number;
   t_rle_straten_voor               number;
   t_rle_woonplaatsen_voor          number;
   t_rle_adressen_voor              number;
   t_rle_pr_relatie_adressen_voor   number;
--

-- PL/SQL Block
begin
   --
   select count('1')
     into t_alle_prs_uitval_adressen
     from rle_uitval_adressen rua
    where rua.persoonsnummer is not null
        ;
   --
   select count('1')
     into t_rle_straten_voor
     from rle_straten
        ;
   --
   select count('1')
     into t_rle_woonplaatsen_voor
     from rle_woonplaatsen
        ;
   --
   select count('1')
     into t_rle_adressen_voor
     from rle_adressen;
   --
   select count('1')
     into t_rle_pr_relatie_adressen_voor
     from rle_relatie_adressen
    where rol_codrol = 'PR'
        ;
   --
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , rpad('Aantal PRS uitval adressen '||t_voorna
                            ||' conversie: ', 48)||lpad(t_alle_prs_uitval_adressen, 8)
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , rpad('Aantal RLE straten '||t_voorna
                            ||' conversie: ', 48)||lpad(t_rle_straten_voor, 8)
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , rpad('Aantal RLE woonplaatsen '
                            ||t_voorna||' conversie: ', 48)||lpad(t_rle_woonplaatsen_voor, 8)
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , rpad('Aantal RLE adressen '||t_voorna
                            ||' conversie: ', 48)||lpad(t_rle_adressen_voor, 8)
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , rpad('Aantal RLE PR-relatie_adressen '||t_voorna
                            ||' conversie: ', 48)||lpad(t_rle_pr_relatie_adressen_voor, 8)
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , ' ======================================================='
                          );
end tellingen;

PROCEDURE AANMAKEN_BUITENLANDS_ADRES
 (P_LAND_LANDKODE_BUI IN VARCHAR2
 ,P_STRAATNAAM IN VARCHAR2
 ,P_WOONPLAATS IN VARCHAR2
 ,P_NUMADRES OUT NUMBER
 )
 IS
-- PL/SQL Specification
--
   cursor c_ads( b_codland varchar2
               , b_stt_id  number
               , b_wps_id  number
               )
       is
   select ads.numadres
     from rle_adressen ads
    where ads.lnd_codland      = b_codland
      and ads.wps_woonplaatsid = b_wps_id
      and ads.stt_straatid     = b_stt_id;
   --
   r_ads           c_ads%rowtype;
   t_woonplaatsid  number(7);
   t_straatid      number(7);
   t_land          varchar2(10);
   --

-- PL/SQL Block
begin
   begin
      --
      t_land := p_land_landkode_bui;
      --
      select woonplaatsid
        into t_woonplaatsid
        from rle_woonplaatsen
       where woonplaats  = p_woonplaats
         and lnd_codland = t_land
           ;
      --
   exception
      when no_data_found
      then
         insert into rle_woonplaatsen
            ( woonplaatsid
            , lnd_codland
            , woonplaats
            , dat_creatie
            , creatie_door
            , dat_mutatie
            , mutatie_door
            )
         values
            ( null --t_woonplaatsid
            , t_land
            , p_woonplaats
            , sysdate
            , user
            , sysdate
            , user
            );
   end;
   --
   select woonplaatsid
     into t_woonplaatsid
     from rle_woonplaatsen
    where woonplaats  = p_woonplaats
      and lnd_codland = t_land
        ;
   --
   begin
      t_straatid     := rle_stt_seq1_next_id;
      insert into rle_straten
         ( straatid
         , wps_woonplaatsid
         , straatnaam
         , dat_creatie
         , creatie_door
         , dat_mutatie
         , mutatie_door
         )
      values
         ( t_straatid
         , t_woonplaatsid
         , p_straatnaam
         , sysdate
         , user
         , sysdate
         , user
         );
      --
   exception
      when dup_val_on_index
      then
        select straatid
          into t_straatid
          from rle_straten
         where straatnaam       = p_straatnaam
           and wps_woonplaatsid = (select woonplaatsid
                                     from rle_woonplaatsen
                                    where woonplaats  = p_woonplaats
                                      and lnd_codland = t_land
                                  );
   end;
   --
   open c_ads( t_land
             , t_straatid
             , t_woonplaatsid
             );
   fetch c_ads into r_ads;
   if c_ads%found
   then
      p_numadres := r_ads.numadres;
   else
      insert into rle_adressen
         ( numadres
         , lnd_codland
         , codorganisatie
         , stt_straatid
         , wps_woonplaatsid
         , huisnummer
         , postcode
         , toevhuisnum
         , dat_creatie
         , creatie_door
         , dat_mutatie
         , mutatie_door
         )
      values
         ( rle_ads_seq1_next_id
         , t_land
         , 'A'
         , t_straatid
         , t_woonplaatsid
         , null
         , null
         , null
         , sysdate
         , user
         , sysdate
         , user
         );
     --
     select rle_ads_seq1.currval
       into p_numadres
       from dual
          ;
   end if;
   close c_ads;
end;

-- PL/SQL Block
END RLE_RAS_PRS;
/