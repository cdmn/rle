CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_AANMAKEN_GBAVBATCH IS

g_gbavbestand_tab                   t_gbavbestand_tab;

PROCEDURE SCHRIJF_BESTAND_GBAV
 (P_GBAV_TAB IN t_gbav_tab
 ,P_FILE_NAME IN VARCHAR2
 ,P_DIRECTORY_NAME IN VARCHAR2 := 'RLE_OUTBOX'
 );


PROCEDURE SCHRIJF_BESTAND_GBAV
 (P_GBAV_TAB IN t_gbav_tab
 ,P_FILE_NAME IN VARCHAR2
 ,P_DIRECTORY_NAME IN VARCHAR2 := 'RLE_OUTBOX'
 )
 IS

  /*****************************************************
  Procedurenaam: Schrijf_bestand_gbav
  Beschrijving: procedure voor het aanmaken van het gbavbatch bestand
  Parameters:
  p_gbav_tab : pl/sql tabel zoals centraal gedefinieerd in RLE_AANMAKEN_GBAVBATCH package
  p_file_name: gbavbatch (zonder extensie)
  p_directory_name: RLE_OUTBOX
  *********************************************************** */
     l_vorige_procedure   varchar2( 255 );
     l_file_id       utl_file.file_type;
     l_file_name      VARCHAR2(100);
     l_teller number:=0;
     l_foutmelding varchar2(200):='';
     l_aantal_bestanden_gbav number:=1;
     --l_file_id  utl_file.file_type;
     l_line varchar2(4000);
     t_locatie  varchar2(50); ------------
begin
    --
     l_vorige_procedure        := stm_util.t_procedure;
     stm_util.t_procedure      := 'SCHRIJF_BESTAND_GBAV';

     l_foutmelding:='Begin schrijf gbav bestand';
     l_file_name:=p_file_name;


    ---utl_file.fopen(l_directory_name,l_file_name,'w');

     if p_gbav_tab.count > 0
     then
       for i in p_gbav_tab.first .. p_gbav_tab.last
       loop
          if mod (l_teller, 25000) = 0
          then
            l_foutmelding:='In gbav_tab loop';
            t_locatie := '32';
            l_file_id  := utl_file.fopen ( 'RLE_OUTBOX'
                                       ,  l_file_name||'_'||l_aantal_bestanden_gbav
                                       ,  'W'
                                       );

          end if;

          l_teller:=l_teller+1;
          l_line :=lpad(to_char(p_gbav_tab(i).intern_kenmerk), 15,' ')
               || lpad(nvl(p_gbav_tab(i).bsn,'         '), 9,' ')
               || rpad(lower(nvl(p_gbav_tab(i).voornamen, ' '))  , 60,' ')
               || rpad(lower(nvl(p_gbav_tab(i).geslachtsnaam,'             ')) , 30,' ')
               || nvl(to_char(p_gbav_tab(i).geboortedatum, 'YYYYMMDD'), '        ')
               || rpad(nvl(p_gbav_tab(i).geslachtsaanduiding,' '), 1,' ')
               || rpad(nvl(to_char(p_gbav_tab(i).huisnummer), '     '), 5,' ')
               || rpad(nvl(p_gbav_tab(i).postcode,'      '), 6,' ')
               || rpad( nvl(p_gbav_tab(i).anummer,'          '), 10,' ');



          utl_file.put_line(l_file_id, CONVERT(l_line,'UTF8','WE8MSWIN1252'));
          -- filehandle(s) sluiten bij 25000
          if mod (l_teller, 25000) = 0
          then
            utl_file.fclose_all;
            g_gbavbestand_tab (l_aantal_bestanden_gbav).aantal := l_teller;
            l_aantal_bestanden_gbav := l_aantal_bestanden_gbav + 1;
            l_teller := 0;
          end if;
         end loop; -- p_text_tab
     end if;

     if l_teller <> 0
     then
        g_gbavbestand_tab (l_aantal_bestanden_gbav).aantal := l_teller;
     end if;

      --
      l_foutmelding:='close file';
      utl_file.fclose_all;
      --
      stm_util.t_procedure := l_vorige_procedure;
      --
  exception
  when no_data_found then
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when utl_file.invalid_path then
    raise_application_error('-20000',stm_util.t_procedure||' invalid path');
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when utl_file.invalid_mode then
    raise_application_error('-20000',stm_util.t_procedure||' invalid mode');
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when utl_file.invalid_filehandle then
    raise_application_error('-20000',stm_util.t_procedure||' invalid filehandle');
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when utl_file.invalid_operation then
    raise_application_error('-20000',stm_util.t_procedure||' file could not be opened as requested');
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when utl_file.read_error then
    raise_application_error('-20000',stm_util.t_procedure||' read error');
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when utl_file.write_error then
    raise_application_error('-20000',stm_util.t_procedure||' write error');
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when utl_file.internal_error then
    raise_application_error('-20000',stm_util.t_procedure||' internal error');
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  when others then
    raise_application_error('-20000',substr(stm_util.t_procedure||':'||l_foutmelding||':'||sqlerrm,1,250));
    utl_file.fflush (l_file_id);
    utl_file.fclose(l_file_id);
  end SCHRIJF_BESTAND_GBAV;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS
/**********************************************************************
  Procedure:    RLE_AANMAKEN_GBAVBATCH.VERWERK
  Beschrijving: Verwerking van versturen GBA berichten van ex partners

  Datum       Wie  Wat
  --------------------------------------------------------------
  08-10-2013  XYO  Creatie
  03-03-2014   mal    Div aanpassingen nav herontwerp
 ********************************************************************* */

    -- PL/SQL Specification

    cn_module                constant varchar2( 255 ) := 'RLE_AANMAKEN_GBAVBATCH.VERWERK';
    t_volgnummer             number := 0;
    t_lijstnummer            number := 1;
    t_regelnummer            number := 0;
    t_locatie                varchar2( 100 );
    l_foutmelding            varchar2( 1000 );
    l_index                  number:=0;
    l_aantal_fout_verw       number:=0;
    l_aantal_te_verw         number := 0;
    l_aantal_gbav            number := 0;
    l_aantal_controle        number := 0;
    l_aantal_ex_bekend       number := 0;
    l_aantal_ex_onbekend     number := 0;
    l_fout_bsn_anders        number := 0;
    l_fout_ex_dubbel         number := 0;
    l_bsn                    rle_bron_ex_partners.bsn%type;
    l_relatienummer          rle_relaties.numrelatie%type;
    l_persoonsnummer_ex      rle_bron_ex_partners.persoonsnummer_ex%type;

    l_gbav_tab               t_gbav_tab;
    l_resultaat              varchar(50);
  --
    l_bsn_vorig              rle_bron_ex_partners.bsn_ex%type;
    l_voornaam_vorig         rle_bron_ex_partners.voornamen_ex%type;
    l_naam_vorig             rle_bron_ex_partners.naam_ex%type;
    l_geboortedatum_vorig    rle_bron_ex_partners.geboortedatum_ex%type;
    l_opnemen_batch          varchar2(10);

    l_bsn_ex 			           rle_bron_ex_partners.bsn_ex%type;
    l_geboortedatum_ex 		   rle_bron_ex_partners.geboortedatum_ex%type;
    l_voornamen_ex		       rle_bron_ex_partners.voornamen_ex%type;
    l_naam_ex                rle_bron_ex_partners.naam_ex%type;
    l_voorletters_ex         rle_bron_ex_partners.voorletters_ex%type;
    l_a_nummer_ex            rle_bron_ex_partners.a_nummer_ex%type;
    l_huwelijk_datumbegin    rle_bron_ex_partners.huwelijk_datumbegin%type;
    l_huwelijk_datumeinde    rle_bron_ex_partners.huwelijk_datumeinde%type;
    l_geslacht_ex            rle_bron_ex_partners.geslacht_ex%type;
    l_huisnummer_ex          rle_bron_ex_partners.huisnummer_ex%type;
    l_postcode_ex            rle_bron_ex_partners.postcode_ex%type;
    l_intern_kenmerk		     rle_bron_ex_partners.intern_kenmerk%type;

    t_fout                   t_fout_tab;

    l_aantal              number := 0;
    e_fout_bestand        exception;

    cursor c_par (b_bestandsnaam  rle_bron_ex_partners.bestandsnaam%type)
    is
    select    par.relatienummer
            , par.intern_kenmerk
            , par.BSN BSN
            , to_number(par.BSN_ex) BSN_ex
            , lower(convert(translate(par.VOORNAMEN_EX, 'ÅãåæÐØøñ', 'AaaadOon'), 'US7ASCII')) VOORNAMEN_EX
            , lower(convert(translate(par.NAAM_EX, 'ÅãåæÐØøñ', 'AaaadOon'), 'US7ASCII')) NAAM_EX
            , lower(convert(translate(par.VOORLETTERS_EX, 'ÅãåæÐØøñ', 'AaaadOon'), 'US7ASCII')) VOORLETTERS_EX
            , par.GEBOORTEDATUM_EX
            , par.A_NUMMER_EX
            , par.huwelijk_datumbegin
            , par.huwelijk_datumeinde
            , par.geslacht_ex
            , par.huisnummer_ex
            , par.postcode_ex
	          , to_number(par.gba_ex_bsn) gba_ex_bsn
	          , lower(convert(translate(par.gba_ex_voornamen, 'ÅãåæÐØøñ', 'AaaadOon'), 'US7ASCII')) gba_ex_voornamen
            , lower(convert(translate(par.gba_ex_naam, 'ÅãåæÐØøñ', 'AaaadOon'), 'US7ASCII')) gba_ex_naam
            , lower(convert(translate(par.gba_ex_voorletters, 'ÅãåæÐØøñ', 'AaaadOon'), 'US7ASCII')) gba_ex_voorletters
	          , par.gba_ex_datgeboorte
            , par.gba_ex_anr
            , par.gba_ex_geslacht
            , par.gba_ex_huisnr
            , par.gba_ex_postcode
     from     rle_bron_ex_partners par
     where    par.datum_verwerkt_gba_dln_geg is not null
     and      par.ind_ex_al_bekend_rle is null
     and      par.bestandsnaam = b_bestandsnaam
     and      par.ind_gba_fout_dln = 'N'
     order by 4,6,5,8
     ;
     -- r_par       c_par%rowtype;
    cursor c_prs_zoekactie1
           (b_sofinummer    rle_relatie_externe_codes.extern_relatie%type
           ,b_geboortedatum rle_relaties.datgeboorte%TYPE)
    is
    select rle.numrelatie relatienummer
    from   rle_relaties rle
    ,      rle_relatie_externe_codes rec
    where  rle.numrelatie = rec.rle_numrelatie
    and  rle.datgeboorte = b_geboortedatum
    and  rec.extern_relatie = b_sofinummer
    and  rec.dwe_wrddom_extsys = 'SOFI'
     ;

    cursor c_prs_zoekactie2
          (b_a_nummer rle_relatie_externe_codes.extern_relatie%type
          ,b_geboortedatum rle_relaties.datgeboorte%type)
    is
    select rle.numrelatie relatienummer
    from   rle_relaties rle
    ,      rle_relatie_externe_codes rec
    where  rle.numrelatie = rec.rle_numrelatie
    and  rle.datgeboorte = b_geboortedatum
    and  rec.extern_relatie = b_a_nummer
    and  rec.dwe_wrddom_extsys = 'GBA'
     ;

    cursor c_prs_zoekactie3
          (b_relatienummer rle_relaties.numrelatie%type
          ,b_dat_begin rle_relatie_koppelingen.dat_begin%type
          ,b_dat_einde rle_relatie_koppelingen.dat_einde%type )
    is
    select rkn.rle_numrelatie_voor
    from   rle_relatie_koppelingen rkn
    where  rkn.rle_numrelatie = b_relatienummer
    and    rkn.dat_begin = nvl(b_dat_begin, rkn.dat_begin)
    and    rkn.dat_einde = b_dat_einde;

     cursor c_prs_zoekactie4
          (b_naam rle_relaties.namrelatie%type
          ,b_geboortedatum rle_relaties.datgeboorte%type
          ,b_voorletter rle_bron_ex_partners.voorletters_ex%type)
    is
    select  rle.numrelatie relatienummer
    from    rle_relaties rle
    where   rle.datgeboorte = b_geboortedatum
    and   lower(rle.namrelatie) = b_naam
    and   lower(rle.voorletter) = b_voorletter
    ;

    cursor c_per
                (b_relatienummer rle_relaties.numrelatie%type)
    is
    select rle.persoonsnummer
    from rle_mv_personen rle
    where rle.relatienummer = b_relatienummer
    ;

    cursor c_bsn
          (b_relatienummer rle_relaties.numrelatie%type
           )
    is
    select  rec.extern_relatie sofinummer
    from    rle_relatie_externe_codes rec
    where   rec.rle_numrelatie = b_relatienummer
    and     rec.rol_codrol = 'SOFI'
    ;

    cursor c_tot (b_bestandsnaam in varchar2)
    is
      select   count (distinct bsn) aantal
              ,count(*) totaal
      from     rle_bron_ex_partners par
      where    par.datum_verwerkt_gba_dln_geg is not null
      and      par.ind_ex_al_bekend_rle is null
      and      par.bestandsnaam = b_bestandsnaam
      and      par.ind_gba_fout_dln = 'N'
      ;

    r_prs_zoekactie1  c_prs_zoekactie1%rowtype;
    r_prs_zoekactie2  c_prs_zoekactie2%rowtype;
    r_prs_zoekactie3  c_prs_zoekactie3%rowtype;
    r_prs_zoekactie4  c_prs_zoekactie4%rowtype;
    r_prs_zoekactie4b c_prs_zoekactie4%rowtype;
    r_bsn	            c_bsn%rowtype;
    r_tot             c_tot%rowtype;
begin

  execute immediate 'alter session set nls_date_format = ''dd-mm-yyyy hh24:mi:ss''';
  --
  stm_util.t_procedure := cn_module;
  stm_util.t_script_naam := p_script_naam;
  stm_util.t_script_id := p_script_id;
  stm_util.t_programma_naam := 'RLE_AANMAKEN_GBAVBATCH';
  t_locatie := ' 001 begin ';

  -- Maak de koptekst van het verwerkingsverslag aan
  --
  t_locatie := ' 002 ';

  t_locatie := ' 003 ';
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'RLEPRC67 RLE_AANMAKEN_GBAVBATCH - Controleren ex aanwezig in rle en aanmaken GBAVBATCH'
                         );
  t_locatie := ' 004 ';
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'Start : ' || to_char( sysdate
                                                , 'dd-mm-yyyy hh24:mi'
                                                )
                         );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , rpad('-', 95, '-')
                         );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , ''
                         );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'Selectie echtscheidingen bronbestand o.b.v. inputbestand: ' || p_bestandsnaam
                         );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , rpad('=', 95, '=')
                         );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , ''
                         );

  select count(*)
  into l_aantal
  from rle_bron_ex_partners
  where bestandsnaam = p_bestandsnaam;

  if l_aantal = 0
  then
    raise e_fout_bestand;
  end if;
  --
  open c_tot (p_bestandsnaam);
  fetch c_tot into r_tot;
  close c_tot;

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'INPUT:'
                         );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                        , 'Aantal te verwerken echtscheidingen in bronbestand: '||  r_tot.totaal
                        );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'Dit betreft '||r_tot.aantal||' unieke deelnemers.');

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , ''
                          );

  t_locatie := ' 006 ';


  t_locatie := ' 007 ';

    --controle ex bekend in RLE
  for r_par in c_par(p_bestandsnaam)
  loop
    t_locatie := '007 in loop ';
    l_bsn:=r_par.bsn;
    begin

      l_aantal_controle    := l_aantal_controle + 1;
      l_resultaat          := 'Niet gevonden';
      l_aantal_ex_onbekend := l_aantal_ex_onbekend + 1;

      if c_prs_zoekactie1%isopen
      then close c_prs_zoekactie1;
      end if;
      if c_prs_zoekactie2%isopen
      then close c_prs_zoekactie2;
      end if;
      if c_prs_zoekactie3%isopen
      then close c_prs_zoekactie3;
      end if;
      if c_prs_zoekactie4%isopen
      then close c_prs_zoekactie4;
      end if;
      if c_bsn%isopen
      then close c_bsn;
      end if;

      stm_util.debug( 'Bezig met internkenmerk: ' || r_par.intern_kenmerk );
      -- aantal gecontroleerd

      --
      -- Zoekactie 1
      t_locatie := '008';

       l_bsn_ex 	            	:= null;
       l_geboortedatum_ex 	    := null;
       l_voornamen_ex	      	:= null;
       l_naam_ex               := null;
       l_voorletters_ex        := null;
       l_a_nummer_ex           := null;
       l_huwelijk_datumbegin   := null;
       l_huwelijk_datumeinde   := null;
       l_geslacht_ex           := null;
       l_huisnummer_ex         := null;
       l_postcode_ex           := null;
       l_relatienummer         := null;
       l_intern_kenmerk        := null;

      l_bsn_ex 	              :=r_par.gba_ex_bsn;
      l_geboortedatum_ex 	  :=r_par.gba_ex_datgeboorte;
      l_voornamen_ex	      :=r_par.gba_ex_voornamen;
      l_naam_ex               :=r_par.gba_ex_naam;
      l_voorletters_ex        :=r_par.gba_ex_voorletters;
      l_a_nummer_ex           :=r_par.gba_ex_anr;
      l_huwelijk_datumbegin   :=r_par.huwelijk_datumbegin;
      l_huwelijk_datumeinde   :=r_par.huwelijk_datumeinde;
      l_geslacht_ex           :=r_par.gba_ex_geslacht;
      l_huisnummer_ex         :=r_par.gba_ex_huisnr;
      l_postcode_ex           :=r_par.gba_ex_postcode;
      l_relatienummer         :=r_par.relatienummer;
      l_intern_kenmerk        :=r_par.intern_kenmerk;

      --Zoekactie 1
      if l_bsn_ex is not null and l_geboortedatum_ex is not null
      then
        open c_prs_zoekactie1(l_bsn_ex
                             ,l_geboortedatum_ex
                             );

        fetch c_prs_zoekactie1 into r_prs_zoekactie1;
        if  c_prs_zoekactie1%found
        then
          l_relatienummer := r_prs_zoekactie1.relatienummer;
          l_resultaat := 'Gevonden';
          l_aantal_ex_onbekend := l_aantal_ex_onbekend - 1;
        end if;
      end if;
      --Zoekactie2: zoeken op A-nummer en geboortedatum
	  if l_resultaat = 'Niet gevonden' and l_a_nummer_ex is not null and l_geboortedatum_ex is not null
      then
        t_locatie := '009';
        open c_prs_zoekactie2(l_a_nummer_ex
                             ,l_geboortedatum_ex
                             );
        fetch c_prs_zoekactie2 into r_prs_zoekactie2;
        if c_prs_zoekactie2%found
        then
          l_relatienummer := r_prs_zoekactie2.relatienummer;
          l_resultaat := 'Gevonden';
          l_aantal_ex_onbekend := l_aantal_ex_onbekend - 1;
        end if;
      end if;
	  --Zoekactie 3.Zoek verder. Nu op deelnemer, begindatum (als deze gevuld is) en einddatum van scheiding
      if l_resultaat = 'Niet gevonden' and l_relatienummer is not null and l_huwelijk_datumeinde is not null
      then
        t_locatie := '010 Zoekactie3 ';
        open c_prs_zoekactie3(l_relatienummer
                             ,l_huwelijk_datumbegin
                             ,l_huwelijk_datumeinde
                             );
        fetch c_prs_zoekactie3 into r_prs_zoekactie3;
        if  c_prs_zoekactie3%found
        then
          l_relatienummer := r_prs_zoekactie3.rle_numrelatie_voor;
          l_resultaat := 'Gevonden';
          l_aantal_ex_onbekend := l_aantal_ex_onbekend - 1;
        end if;
      end if;
      --Zoekacie 4. Nu op naam, geboortedatum en voorletters. Deze velden mogen niet leeg zijn)
      if l_resultaat = 'Niet gevonden' and l_naam_ex is not null and l_geboortedatum_ex is not null and l_voorletters_ex is not null
      then
        t_locatie := '011';
        open c_prs_zoekactie4(l_naam_ex
                             ,l_geboortedatum_ex
                             ,l_voorletters_ex);
        fetch c_prs_zoekactie4 into r_prs_zoekactie4;
        t_locatie := '012';
        if c_prs_zoekactie4%found
        then
          t_locatie := '013';
        --De combinatie naam, geboortedatum en voorletters mag niet meer dan 1 keer voorkomen.
          fetch c_prs_zoekactie4
          into r_prs_zoekactie4b;
          --
          if c_prs_zoekactie4%found
          then
            t_locatie := '014 ';
            update rle_bron_ex_partners brs
            set brs.foutmelding =  'Bij zoeken op naam, geb.datum en voorletters  ex-partner dubbel'
            where   brs.intern_kenmerk = r_par.intern_kenmerk;
            l_fout_ex_dubbel := l_fout_ex_dubbel+1;
          else
            t_locatie := '015';
            --Indien gevonden moet er gecontroleerd worden of het BNS nummer gevuld is.
            open c_bsn(r_prs_zoekactie4.relatienummer);
            fetch c_bsn into r_bsn;
            if c_bsn%found
            then
              update comp_rle.rle_bron_ex_partners brs
              set     brs.foutmelding = 'Ex gevonden op naam, geb.datum en voorletters, BSN anders'
              where   brs.intern_kenmerk = l_intern_kenmerk;
              --
              l_fout_bsn_anders := l_fout_bsn_anders  +1;
              else
                t_locatie := '16';
                l_relatienummer  := r_prs_zoekactie4.relatienummer;
                l_resultaat     := 'Gevonden';
                l_aantal_ex_onbekend := l_aantal_ex_onbekend - 1;
              end if;
            end if;
         end if;
      end if;

    ---Ex-partner is bekend in RLE
    --Bepalen persoonsnummer voor update brontabel
     if l_resultaat = 'Gevonden'
     then
       t_locatie := '18';
       open c_per(l_relatienummer);
       fetch c_per into l_persoonsnummer_ex ;
       close  c_per;

       -- aantal records met bekend ex
       l_aantal_ex_bekend:= l_aantal_ex_bekend+1;
       t_locatie := '20';
       --updaten brontabel
       update rle_bron_ex_partners brs
       set brs.ind_ex_al_bekend_rle = 'J'
       ,    brs.persoonsnummer_ex =    l_persoonsnummer_ex
       ,   brs.relatienummer_ex = l_relatienummer
       where   brs.intern_kenmerk = l_intern_kenmerk;
       t_locatie := '20-b';
     else
       if l_resultaat = 'Niet gevonden'
       then
         if r_par.BSN_EX is not null
         then
           if l_bsn_vorig = l_BSN_EX
           then
             l_opnemen_batch := 'N';
           else
             l_opnemen_batch := 'J';
           end if;
         else
           if l_voornaam_vorig = l_voornamen_ex and l_naam_vorig = l_naam_ex and l_geboortedatum_vorig = l_geboortedatum_ex
           then
             l_opnemen_batch := 'N';
           else
             l_opnemen_batch := 'J';
           end if;
         end if;

         if l_opnemen_batch = 'J'
         then
           t_locatie := '20-c';
           l_index := l_index +1;
           -- vullen in plsql tabel
           l_gbav_tab(l_index).intern_kenmerk      := l_intern_kenmerk;
           l_gbav_tab(l_index).bsn                 := l_bsn_ex;
           l_gbav_tab(l_index).voornamen           := l_voornamen_ex;
           l_gbav_tab(l_index).geslachtsnaam       := l_naam_ex;
           l_gbav_tab(l_index).geboortedatum       := l_geboortedatum_ex;
           l_gbav_tab(l_index).anummer             := l_a_nummer_ex;
	       l_gbav_tab(l_index).geslachtsaanduiding := l_geslacht_ex;
           l_gbav_tab(l_index).postcode            := l_postcode_ex;
           l_gbav_tab(l_index).huisnummer          := l_huisnummer_ex;
           --
           l_aantal_gbav := l_aantal_gbav +1;
         end if;

         t_locatie := '21';
         --Ex is niet bekend in RLE
         update rle_bron_ex_partners brs
         set  brs.ind_ex_al_bekend_rle = 'N'
         ,    brs.datum_opgenomen_gbabatch_ex = sysdate
         where   brs.intern_kenmerk = l_intern_kenmerk;
       end if;
     end if;

     exception
     when others
     then
       t_locatie := '22';
       -- melding loggen en daarna doorgaan met de volgende relatie
       l_aantal_fout_verw := l_aantal_fout_verw +1;
       l_aantal_controle := l_aantal_controle - 1;

       t_fout (l_aantal_fout_verw).bsn := l_bsn;
       t_fout (l_aantal_fout_verw).dateinde_huwelijk := l_huwelijk_datumeinde;
       t_fout (l_aantal_fout_verw).fout := substr(sqlerrm,1,350);

       update rle_bron_ex_partners brs
       set brs.foutmelding = 'Fout opgetreden in RLE_AANMAKEN_GBAVBATCH.VERWERK'
       where   brs.intern_kenmerk = l_intern_kenmerk;

     end;
   end loop;

   t_locatie := '21b';
       ---wegschrijven bestand
   if l_index>0
   then
     t_locatie := '21c';
     schrijf_bestand_gbav ( p_gbav_tab        => l_gbav_tab
                          , p_file_name      => 'GBAVBATCH'
                          , p_directory_name => 'RLE_OUTBOX'
                          );
   end if;

   t_locatie := '23';


   stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'OUTPUT:'
                          );

   stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'Aantal echtscheidingen in bronbestand waarvan de ex-en niet bekend zijn in RLE: ' || l_aantal_ex_onbekend
                         );


   stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'Aantal echtscheidingen in bronbestand waarvan de ex-en al bekend zijn in RLE: '  || l_aantal_ex_bekend
                         );

   stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , ''
                           );

  for i in g_gbavbestand_tab.first .. g_gbavbestand_tab.last
  loop

    stm_util.insert_lijsten( t_lijstnummer
                           , t_regelnummer
                           , 'Aantal ex-en opgenomen in GBAVBATCH: '||g_gbavbestand_tab(i).aantal||' in bestand: '||g_gbavbestand_tab(i).naam
                         );
  end loop;

    stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , ''
                           );

  if t_fout.count > 0
  then
    stm_util.insert_lijsten( t_lijstnummer
                           , t_regelnummer
                           ,'De volgende echtscheidingen konden niet worden verwerkt vanwege een technische fout (aantal: '||l_aantal_fout_verw||').');


    for i in t_fout.first .. t_fout.last
    loop

       stm_util.insert_lijsten( t_lijstnummer
                              , t_regelnummer
                             , 'Deelnemer BSN: '||t_fout(i).bsn||' _ Einddatum huwelijk: '||t_fout (i).dateinde_huwelijk||' _ Foutmelding: '
                             );

       stm_util.insert_lijsten( t_lijstnummer
                              , t_regelnummer
                              , t_fout (i).fout);

    end loop;

    stm_util.insert_lijsten( t_lijstnummer
                           , t_regelnummer
                           , ''
                           );

  end if;


   stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , ''
                         );
   stm_util.insert_lijsten( t_lijstnummer
                        , t_regelnummer
                        , 'EINDE RLEPRC67 - Aanvragen deelnemers bij GBA '||to_char( sysdate
                                                  , 'dd-mm-yyyy hh24:mi'
                                                  )
                        );

   stm_util.insert_job_statussen( t_volgnummer
                               , 'S'
                               , '0'
                               );
--   commit;
   stm_util.module_end;
   --
   exception
   when e_fout_bestand
   then
      rollback;
      stm_util.insert_job_statussen(t_volgnummer
                                  ,'S'
                                  ,'1'
                                   );
      stm_util.insert_job_statussen(t_volgnummer,
                                    'I',
                                    stm_util.t_programma_naam ||
                                   ' procedure : ' || stm_util.t_procedure);

      stm_util.insert_job_statussen(t_volgnummer,
                                    'I',
                                    'De opgegeven bestandsnaam ('||p_bestandsnaam||') is onbekend.');
      commit;

   when others
   then
     rollback;
     stm_util.t_foutmelding := substr(l_foutmelding||' '||sqlerrm, 1, 255);
     stm_util.debug ('Fout: ' || stm_util.t_foutmelding);
     --
     stm_util.insert_job_statussen (t_volgnummer, 'S', '1');
     --
     stm_util.insert_job_statussen (ijs_volgnummer     =>  t_volgnummer
                                  ,ijs_uitvoer_soort  =>  'I'
                                  ,ijs_uitvoer_waarde =>  stm_util.t_programma_naam||' Procedure : '|| stm_util.t_procedure);
     --
     if stm_util.t_foutmelding is not null
     then
       stm_util.insert_job_statussen (ijs_volgnummer     => t_volgnummer
                                    ,ijs_uitvoer_soort  => 'I'
                                    ,ijs_uitvoer_waarde => substr ('Fout: '||stm_util.t_foutmelding, 1, 255));
    end if;
    --
    commit;
    --
    raise_application_error (-20000, stm_app_error (sqlerrm, null, null) || ' ' || dbms_utility.format_error_backtrace );

end verwerk;
end RLE_AANMAKEN_GBAVBATCH;
/