CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_RLE_LEVERINGEN IS
cn_verre_toekomst     constant date := to_date ('01-01-2999', 'dd-mm-yyyy');
cn_directory          constant varchar2(15) := 'RLE_OUTBOX';



cursor c_adr (b_rle_numrelatie     in   rle_relatie_adressen.rle_numrelatie%type
             ,b_peildatum          in   date
             ,b_vorige_peildatum   in   date)
is
   select   stt.straatnaam
           ,wps.woonplaats
           ,case
               when adr.lnd_codland = 'NL'
                  then substr (adr.postcode, 1, 4) || ' ' || substr (adr.postcode, 5)
               else adr.postcode
            end postcode
           ,adr.huisnummer
           ,adr.toevhuisnum
           ,lnd.landcode_iso3166_a2 landcode
           ,lnd.naam landnaam
   from     rle_adressen adr
           ,rle_relatie_adressen ras
           ,rle_straten stt
           ,rle_woonplaatsen wps
           ,rle_landen lnd
   where    adr.numadres = ras.ads_numadres
   and      adr.stt_straatid = stt.straatid
   and      adr.wps_woonplaatsid = wps.woonplaatsid
   and      adr.lnd_codland = lnd.codland
   and      ras.dwe_wrddom_srtadr in ('OA', 'CA')   /* Opgegeven adres (igv personen), Corr adres (igv werkgevers) */
   and      upper(stt.straatnaam) <> 'ONBEKEND'
   and      upper(wps.woonplaats) <> 'ONBEKEND'
   and      lnd.landcode_iso3166_a2 <> 'XX'
   and      ras.rle_numrelatie = b_rle_numrelatie
   and      ras.datingang <= b_peildatum
   and      nvl (ras.dateinde, cn_verre_toekomst) >= b_vorige_peildatum
   order by nvl (ras.dateinde, cn_verre_toekomst) desc;


/* bepalen parameters */
PROCEDURE BEPAAL_PARAMETERS
 (P_BSD_NAAM IN VARCHAR2
 ,P_PEILDATUM IN DATE
 ,P_VORIGE_PEILDATUM IN DATE
 ,O_VORIGE_PEILDATUM IN OUT DATE
 ,O_VORIGE_RUNDATUM IN OUT DATE
 ,O_RUNDATUM IN OUT DATE
 )
 IS
   /******************************************************************************
      NAAM:       bepaal_parameters
      DOEL:       Het bepalen/overrulen van de parameters

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
      2         25-06-2013  Monique v Alphen  Aangepast tbv deze batch
   ******************************************************************************/

l_initiele_datum   date;
begin


  o_rundatum := sysdate;

  if p_vorige_peildatum is null
  then
     begin
        select tot_datum
              ,starttijd_uitvoer
        into   o_vorige_peildatum
              ,o_vorige_rundatum
        from   rle_uitvoer_statussen
        where  (bsd_naam, runnummer) in (select   uss.bsd_naam
                                                 ,max (uss.runnummer)
                                         from     rle_uitvoer_statussen uss
                                         where    uss.bsd_naam = p_bsd_naam
                                         and      uss.tot_datum < p_peildatum
                                         group by uss.bsd_naam);
     exception
        when no_data_found
        then

          select dat_begin
          into l_initiele_datum
          from rle_bestandsabonnementen
          where bsd_naam = p_bsd_naam;

           o_vorige_peildatum := l_initiele_datum;
           o_vorige_rundatum := o_rundatum;
     end;
  else
     o_vorige_peildatum := p_vorige_peildatum;

     begin
        select starttijd_uitvoer
        into   o_vorige_rundatum
        from   rle_uitvoer_statussen
        where  (bsd_naam, runnummer) in (select   bsd_naam
                                                 ,max (runnummer)
                                         from     rle_uitvoer_statussen
                                         where    bsd_naam = p_bsd_naam
                                         and      tot_datum = p_vorige_peildatum
                                         group by bsd_naam);
     exception
        when no_data_found
        then
           o_vorige_peildatum := p_vorige_peildatum;
           o_vorige_rundatum := o_rundatum;
     end;
  end if;

exception
   when others
   then
      raise_application_error (-20000
                              ,nvl (cg$errors.geterrors
                                   , stm_app_error (sqlerrm, null, null)
                                      || ' '
                                      || dbms_utility.format_error_backtrace) );
end bepaal_parameters;
PROCEDURE OPSLAAN_UITVOER_GEGEVENS
 (P_RUNNUMMER IN NUMBER
 ,P_BSD_NAAM IN VARCHAR2
 ,P_VANAF_DATUM IN DATE
 ,P_TOT_DATUM IN DATE
 ,P_STARTTIJD_UITVOER IN DATE
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS
   /******************************************************************************
      NAAM:       opslaan_uitvoer_gegevens
      DOEL:       Het opslaan van de gegevens van de zojuist aangemaakte bestanden

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          25-02-2008  R.Podt      XRP  Aanmaak routine
      2         25-06-2013  Monique v Alphen Aangepast tbv deze batch
   ******************************************************************************/
l_initiele_datum   date;
begin

 if p_bsd_naam = 'OTIB_WGR'
 then
   l_initiele_datum := to_date('01-01-1992', 'dd-mm-yyyy');
 else
   l_initiele_datum := to_date('01-01-2013', 'dd-mm-yyyy');
 end if;

   if p_vanaf_datum = l_initiele_datum
   then
      delete      rle_uitvoer_statussen uss
      where       uss.bsd_naam = p_bsd_naam;
   else
      delete      rle_uitvoer_statussen uss
      where       uss.bsd_naam = p_bsd_naam
      and         uss.tot_datum > p_tot_datum;
   end if;

  merge into rle_uitvoer_statussen uss
     using (select p_runnummer runnummer
                  ,p_bsd_naam bsd_naam
                  ,p_vanaf_datum vanaf_datum
                  ,p_tot_datum tot_datum
                  ,p_starttijd_uitvoer starttijd_uitvoer
                  ,sysdate eindtijd_uitvoer
                  ,p_bestandsnaam bestandsnaam
                  ,cn_directory db_directory_uitvoer
            from   dual) d
     on (    uss.bsd_naam = d.bsd_naam
         and uss.vanaf_datum = d.vanaf_datum)
     when matched then
        update
           set runnummer = d.runnummer, tot_datum = d.tot_datum, starttijd_uitvoer = d.starttijd_uitvoer
              ,eindtijd_uitvoer = d.eindtijd_uitvoer, bestandsnaam = d.bestandsnaam
              ,db_directory_uitvoer = d.db_directory_uitvoer
     when not matched then
        insert (runnummer, bsd_naam, vanaf_datum, tot_datum, starttijd_uitvoer, eindtijd_uitvoer, bestandsnaam
               ,db_directory_uitvoer)
        values (d.runnummer, d.bsd_naam, d.vanaf_datum, d.tot_datum, d.starttijd_uitvoer, d.eindtijd_uitvoer
               ,d.bestandsnaam, d.db_directory_uitvoer);
   end opslaan_uitvoer_gegevens;
FUNCTION GET_BESTANDSNAAM
 (P_BSD_NAAM IN VARCHAR2
 ,P_BSD_TYPE IN VARCHAR2
 ,P_VANAF_DATUM IN DATE
 ,P_TOT_DATUM IN DATE
 )
 RETURN VARCHAR2
 IS

l_filenaam   varchar2(50);
l_rowcount   number;
begin

select case when p_bsd_type = 'WNR'
         then (select count(*) from rle_wn_bestand)
         else (select count(*) from rle_wg_bestand)
         end rowcount
         into l_rowcount
         from dual;

select replace (replace (replace (replace (formaat_naam
                                                ,'<vanaf_datum>'
                                                ,to_char (p_vanaf_datum, 'YYYYMMDD') )
                                       ,'<tot_datum>'
                                       ,to_char (p_tot_datum, 'YYYYMMDD') )
                              ,'<rowcount>'
                              ,l_rowcount)
                     ,'<jobid>'
                     ,stm_util.t_script_id) geformateerde_naam
      into l_filenaam
      from   rle_bestanden
      where  naam = p_bsd_naam;

return l_filenaam;
end get_bestandsnaam;
PROCEDURE SCHRIJF_OTIB_BESTAND
 (P_BSD_TYPE IN VARCHAR2
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS

cursor c_wnr
is

  select *
  from rle_wn_bestand

  order by nr_werkgever, nr_persoon, dat_ingang_geg;




l_xml              xmltype;

l_file                utl_file.file_type;
begin

  if p_bsd_type = 'WGR'
  then

    select xmlroot(
         xmlelement("werkgevers"
        ,xmlattributes('http://qios.nl/ORL-Schema' as "xmlns")
           ,xmlagg(xmlelement("werkgever"
                  ,xmlelement("nummer", lpad (nr_werkgever, 6, 0))
                  ,xmlelement("naam" , xmlcdata(naam))
                  ,case when dat_ingang_geg is null then null else xmlelement("ingangsdatum", to_char(dat_ingang_geg, 'yyyy-mm-dd')) end
                  ,case when dat_einde_geg is null then null else xmlelement("einddatum", to_char(dat_einde_geg, 'yyyy-mm-dd')) end
                  ,case when lidmaatschap is null then null else xmlelement("lidmaatschap", lidmaatschap) end
                  ,case when dat_ingang_lidm is null then null else xmlelement("ingangsdatumLidmaatschap", to_char(dat_ingang_lidm, 'yyyy-mm-dd')) end
                  ,case when dat_einde_lidm is null then null else xmlelement("einddatumLidmaatschap", to_char(dat_einde_lidm, 'yyyy-mm-dd')) end
                  ,case when rechtsvorm is null then null else xmlelement("rechtsvorm", rechtsvorm) end
                  ,case when fonds is null then null else xmlelement("fondscode", fonds)end
                  ,case when gvw is null then null else xmlelement("groepVanWerkzaamheden", gvw) end
                  ,case when sgw is null then null else xmlelement("subGroepVanWerkzaamheden", sgw) end
                  ,case when contactpersoon is null then null else xmlelement("terAttentieVan", contactpersoon) end
                  ,case when za_straat is null then null else xmlelement("zaakAdresStraat", xmlcdata(za_straat)) end
                  ,case when za_huisnr is null then null else xmlelement("zaakAdresHuisnummer", za_huisnr) end
                  ,case when za_huisnrtoev is null then null else xmlelement("zaakAdresToevoeging", za_huisnrtoev) end
                  ,case when za_postcode is null then null else xmlelement("zaakAdresPostcode", za_postcode) end
                  ,case when za_woonplaats is null then null else xmlelement("zaakAdresPlaats", xmlcdata(za_woonplaats)) end
                  ,case when za_landcode is null then null else xmlelement("zaakAdresLandcode", za_landcode) end
                  ,case when za_landnaam is null then null else xmlelement("zaakAdresLandnaam", za_landnaam) end
                  ,case when ca_straat is null then null else xmlelement("correspondentieAdresStraat", xmlcdata(ca_straat)) end
                  ,case when ca_huisnr is null then null else xmlelement("correspondentieAdresHuisnummer", ca_huisnr) end
                  ,case when ca_huisnrtoev is null then null else xmlelement("correspondentieAdresToevoeging", ca_huisnrtoev) end
                  ,case when ca_postcode is null then null else xmlelement("correspondentieAdresPostcode", ca_postcode)  end
                  ,case when ca_woonplaats is null then null else xmlelement("correspondentieAdresPlaats", xmlcdata(ca_woonplaats)) end
                  ,case when ca_landcode is null then null else xmlelement("correspondentieAdresLandcode", ca_landcode) end
                  ,case when ca_landnaam is null then null else xmlelement("correspondentieAdresLandnaam", ca_landnaam) end
                  ,case when tel_nr is null then null else xmlelement("telefoonnummer", tel_nr) end
                  ,case when iban is null then null else xmlelement("iban", iban) end
                  ,case when dat_einde is null then null else xmlelement("datumBeeindiging", to_char(dat_einde, 'yyyy-mm-dd')) end
                  ,case when reden_einde is null then null else xmlelement("redenBeeindiging", reden_einde) end
                  ,case when overname is null then null else xmlelement("overnameDoor", lpad(overname, 6, 0)) end
                  ,case when dienstverbanden is null then null else xmlelement("aantalDienstverbanden", dienstverbanden) end
                  ,case when loonsom is null then null else xmlelement("loonsom", round(loonsom, 2)) end
                  ,case when kvk_nummer is null then null else xmlelement("kvkNummer", kvk_nummer) end
                  ,case when cvc is null then null else xmlelement("collectiefVrijwilligContract", cvc) end
                            )
                  order by nr_werkgever, dat_ingang_geg)), version '1.0" encoding="UTF-8') adres_xml
    into l_xml
    from rle_wg_bestand
    where bestandsgroep = 'OTIB';

    dbms_xslprocessor.clob2file (l_xml.getclobval(), cn_directory, p_bestandsnaam, 871);

  else

  l_file := utl_file.fopen ( 'RLE_OUTBOX', p_bestandsnaam, 'W');

  utl_file.putf (l_file,  convert('<?xml version="1.0" encoding="UTF-8" ?>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
  utl_file.putf (l_file,  convert('<werknemers xmlns="http://qios.nl/ORL-Schema">\n', 'AL32UTF8', 'WE8MSWIN1252' ));

  for r_wnr in c_wnr
  loop

    utl_file.putf (l_file,convert('<werknemer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    utl_file.putf (l_file, convert('<nummer>'||lpad(r_wnr.nr_persoon, 8, 0)||'</nummer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    utl_file.putf (l_file, convert('<naam><![CDATA[' ||r_wnr.naam||']]></naam>\n', 'AL32UTF8', 'WE8MSWIN1252' ) ) ;

    if r_wnr.voorletters is not null
    then
      utl_file.putf (l_file, convert('<voorletters>'||r_wnr.voorletters||'</voorletters>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.voorvoegsel is not null
    then
     utl_file.putf (l_file, convert( '<voorvoegsels><![CDATA[' ||r_wnr.voorvoegsel||']]></voorvoegsels>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.geslacht is not null
    then
      utl_file.putf (l_file, convert('<geslacht>'||r_wnr.geslacht||'</geslacht>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.dat_geboorte is not null
    then
      utl_file.putf (l_file, convert('<geboortedatum>'||to_char(r_wnr.dat_geboorte, 'yyyy-mm-dd')||'</geboortedatum>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.dat_ingang_geg is not null
	then
	utl_file.putf (l_file, convert('<ingangsdatum>'||to_char(r_wnr.dat_ingang_geg, 'yyyy-mm-dd')||'</ingangsdatum>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
	if r_wnr.dat_einde_geg is not null
	then
    utl_file.putf (l_file, convert('<einddatum>'||to_char(r_wnr.dat_einde_geg, 'yyyy-mm-dd')||'</einddatum>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
	if r_wnr.nr_werkgever is not null
	then
    utl_file.putf (l_file, convert('<werkgeverNummer>'||lpad (r_wnr.nr_werkgever, 6, 0)||'</werkgeverNummer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
	if r_wnr.avg_dat_ingang is not null
	then
    utl_file.putf (l_file, convert('<aanvangsdatumDienstverband>'||to_char (r_wnr.avg_dat_ingang, 'yyyy-mm-dd')||'</aanvangsdatumDienstverband>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
    if r_wnr.avg_dat_einde is not null
    then
      utl_file.putf (l_file, convert('<einddatumDienstverband>'||to_char (r_wnr.avg_dat_einde, 'yyyy-mm-dd')||'</einddatumDienstverband>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.avg_uren is not null
    then
      utl_file.putf (l_file, convert('<omvangDienstverband>'||replace(r_wnr.avg_uren, ',', '.')||'</omvangDienstverband>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.reden_einde is not null
    then
      utl_file.putf (l_file, convert('<redenUitstroom>'||r_wnr.reden_einde||'</redenUitstroom>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.inkomen is not null
    then
      utl_file.putf (l_file, convert('<inkomen>'||replace(round(r_wnr.inkomen, 2), ',', '.')||'</inkomen>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.freq_inkomen is not null
    then
      utl_file.putf (l_file, convert('<inkomensfrequentie>'||r_wnr.freq_inkomen||'</inkomensfrequentie>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.functiecat is not null
    then
      utl_file.putf (l_file, convert('<functiecategorie>'||r_wnr.functiecat||'</functiecategorie>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.beroepcode is not null
    then
      utl_file.putf (l_file, convert('<beroepcode>'||lpad(r_wnr.beroepcode, 4, '0')||'</beroepcode>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.vvgl_partner is not null
    then
      utl_file.putf (l_file, convert('<voorvoegselsEchtgenoot>'||r_wnr.vvgl_partner||'</voorvoegselsEchtgenoot>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.naam_partner is not null
    then
      utl_file.putf (l_file, convert('<naamEchtgenoot><![CDATA[' ||r_wnr.naam_partner||']]></naamEchtgenoot>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.dat_overlijden is not null
    then
      utl_file.putf (l_file, convert('<datumOverlijden>'||to_char(r_wnr.dat_overlijden, 'yyyy-mm-dd')||'</datumOverlijden>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.straat is not null
    then
      utl_file.putf (l_file, convert('<adresStraat><![CDATA[' ||r_wnr.straat||']]></adresStraat>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.huisnr is not null
    then
      utl_file.putf (l_file, convert('<adresHuisnummer>'||r_wnr.huisnr||'</adresHuisnummer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.huisnrtoev is not null
    then
      utl_file.putf (l_file, convert('<adresToevoeging>'||r_wnr.huisnrtoev||'</adresToevoeging>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.postcode is not null
    then
      utl_file.putf (l_file, convert('<adresPostcode>'||r_wnr.postcode||'</adresPostcode>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.woonplaats is not null
    then
      utl_file.putf (l_file, convert('<adresPlaats><![CDATA['||r_wnr.woonplaats||']]></adresPlaats>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.landcode is not null
    then
      utl_file.putf (l_file, convert('<adresLandcode>'||r_wnr.landcode||'</adresLandcode>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.landnaam is not null
    then
      utl_file.putf (l_file, convert('<adresLandnaam>'||r_wnr.landnaam||'</adresLandnaam>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;

    utl_file.putf (l_file,convert('</werknemer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));

  end loop;

  utl_file.putf (l_file,convert('</werknemers>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
--
  utl_file.fclose(l_file);

  end if;

end schrijf_otib_bestand;
PROCEDURE SCHRIJF_OOM_BESTAND
 (P_BSD_TYPE IN VARCHAR2
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS
cursor c_wnr

is

  select *

  from rle_wn_bestand

  order by nr_werkgever, nr_persoon, dat_ingang_geg;




l_xml              xmltype;


l_file             utl_file.file_type;
begin

  if p_bsd_type = 'WGR'
  then

    select xmlroot(
         xmlelement("werkgevers"
        ,xmlattributes('http://qios.nl/ORL-Schema' as "xmlns")
           ,xmlagg(xmlelement("werkgever"
                  ,xmlelement("nummer", lpad (nr_werkgever, 6, 0))
                  ,xmlelement("naam" , xmlcdata(naam)  )
                  ,case when dat_ingang_geg is null then null else xmlelement("ingangsdatum", to_char(dat_ingang_geg, 'yyyy-mm-dd')) end
                  ,case when dat_einde_geg is null then null else xmlelement("einddatum", to_char(dat_einde_geg, 'yyyy-mm-dd')) end
                  ,case when lidmaatschap is null then null else xmlelement("lidmaatschap", lidmaatschap) end
                  ,case when dat_ingang_lidm is null then null else xmlelement("ingangsdatumLidmaatschap", to_char(dat_ingang_lidm, 'yyyy-mm-dd')) end
                  ,case when dat_einde_lidm is null then null else xmlelement("einddatumLidmaatschap", to_char(dat_einde_lidm, 'yyyy-mm-dd')) end
                  ,case when rechtsvorm is null then null else xmlelement("rechtsvorm", rechtsvorm) end
                  ,case when gvw is null then null else xmlelement("groepVanWerkzaamheden", gvw) end
                  ,case when sgw is null then null else xmlelement("subGroepVanWerkzaamheden", sgw) end
                  ,case when contactpersoon is null then null else xmlelement("terAttentieVan", contactpersoon) end
                  ,case when za_straat is null then null else xmlelement("zaakAdresStraat", xmlcdata(za_straat)) end
                  ,case when za_huisnr is null then null else xmlelement("zaakAdresHuisnummer", za_huisnr) end
                  ,case when za_huisnrtoev is null then null else xmlelement("zaakAdresToevoeging", za_huisnrtoev) end
                  ,case when za_postcode is null then null else xmlelement("zaakAdresPostcode", za_postcode) end
                  ,case when za_woonplaats is null then null else xmlelement("zaakAdresPlaats", xmlcdata(za_woonplaats)) end
                  ,case when za_landcode is null then null else xmlelement("zaakAdresLandcode", za_landcode) end
                  ,case when za_landnaam is null then null else xmlelement("zaakAdresLandnaam", za_landnaam) end
                  ,case when ca_straat is null then null else xmlelement("correspondentieAdresStraat", xmlcdata(ca_straat)) end
                  ,case when ca_huisnr is null then null else xmlelement("correspondentieAdresHuisnummer", ca_huisnr) end
                  ,case when ca_huisnrtoev is null then null else xmlelement("correspondentieAdresToevoeging", ca_huisnrtoev) end
                  ,case when ca_postcode is null then null else xmlelement("correspondentieAdresPostcode", ca_postcode)  end
                  ,case when ca_woonplaats is null then null else xmlelement("correspondentieAdresPlaats", xmlcdata(ca_woonplaats)) end
                  ,case when ca_landcode is null then null else xmlelement("correspondentieAdresLandcode", ca_landcode) end
                  ,case when ca_landnaam is null then null else xmlelement("correspondentieAdresLandnaam", ca_landnaam) end
                  ,case when tel_nr is null then null else xmlelement("telefoonnummer", tel_nr) end
                  ,case when iban is null then null else xmlelement("iban", iban) end
                  ,case when dat_einde is null then null else xmlelement("datumBeeindiging", to_char(dat_einde, 'yyyy-mm-dd')) end
                  ,case when reden_einde is null then null else xmlelement("redenBeeindiging", reden_einde) end
                  ,case when overname is null then null else xmlelement("overnameDoor", lpad(overname, 6,0)) end
                  ,case when dienstverbanden is null then null else xmlelement("aantalDienstverbanden", dienstverbanden) end
                  ,case when loonsom is null then null else xmlelement("loonsom", round(loonsom, 2)) end
                  ,case when kvk_nummer is null then null else xmlelement("kvkNummer", kvk_nummer) end
                  ,case when cvc is null then null else xmlelement("collectiefVrijwilligContract", cvc) end
                            )
                  order by nr_werkgever, dat_ingang_geg)), version '1.0" encoding="UTF-8')
    into l_xml
    from rle_wg_bestand
    where fonds = 'OOM';

    dbms_xslprocessor.clob2file (l_xml.getclobval(), cn_directory, p_bestandsnaam, 871);

  else

 l_file := utl_file.fopen ( 'RLE_OUTBOX', p_bestandsnaam, 'W');

  utl_file.putf (l_file,  convert('<?xml version="1.0" encoding="UTF-8" ?>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
  utl_file.putf (l_file,  convert('<werknemers xmlns="http://qios.nl/ORL-Schema">\n', 'AL32UTF8', 'WE8MSWIN1252' ));

  for r_wnr in c_wnr
  loop

    utl_file.putf (l_file,convert('<werknemer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    utl_file.putf (l_file, convert('<nummer>'||lpad(r_wnr.nr_persoon, 8, 0)||'</nummer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    utl_file.putf (l_file, convert('<naam><![CDATA[' ||r_wnr.naam||']]></naam>\n', 'AL32UTF8', 'WE8MSWIN1252' ) ) ;

    if r_wnr.voorletters is not null
    then
      utl_file.putf (l_file, convert('<voorletters>'||r_wnr.voorletters||'</voorletters>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.voorvoegsel is not null
    then
     utl_file.putf (l_file, convert( '<voorvoegsels><![CDATA[' ||r_wnr.voorvoegsel||']]></voorvoegsels>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.geslacht is not null
    then
      utl_file.putf (l_file, convert('<geslacht>'||r_wnr.geslacht||'</geslacht>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.dat_geboorte is not null
    then
      utl_file.putf (l_file, convert('<geboortedatum>'||to_char(r_wnr.dat_geboorte, 'yyyy-mm-dd')||'</geboortedatum>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.dat_ingang_geg is not null
	then
	utl_file.putf (l_file, convert('<ingangsdatum>'||to_char(r_wnr.dat_ingang_geg, 'yyyy-mm-dd')||'</ingangsdatum>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
	if r_wnr.dat_einde_geg is not null
	then
    utl_file.putf (l_file, convert('<einddatum>'||to_char(r_wnr.dat_einde_geg, 'yyyy-mm-dd')||'</einddatum>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
	if r_wnr.nr_werkgever is not null
	then
    utl_file.putf (l_file, convert('<werkgeverNummer>'||lpad (r_wnr.nr_werkgever, 6, 0)||'</werkgeverNummer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
	if r_wnr.avg_dat_ingang is not null
	then
    utl_file.putf (l_file, convert('<aanvangsdatumDienstverband>'||to_char (r_wnr.avg_dat_ingang, 'yyyy-mm-dd')||'</aanvangsdatumDienstverband>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
	end if;
    if r_wnr.avg_dat_einde is not null
    then
      utl_file.putf (l_file, convert('<einddatumDienstverband>'||to_char (r_wnr.avg_dat_einde, 'yyyy-mm-dd')||'</einddatumDienstverband>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.avg_uren is not null
    then
      utl_file.putf (l_file, convert('<omvangDienstverband>'||replace(r_wnr.avg_uren, ',', '.')||'</omvangDienstverband>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.reden_einde is not null
    then
      utl_file.putf (l_file, convert('<redenUitstroom>'||r_wnr.reden_einde||'</redenUitstroom>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.inkomen is not null
    then
      utl_file.putf (l_file, convert('<inkomen>'||replace(round(r_wnr.inkomen, 2), ',', '.')||'</inkomen>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.freq_inkomen is not null
    then
      utl_file.putf (l_file, convert('<inkomensfrequentie>'||r_wnr.freq_inkomen||'</inkomensfrequentie>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.functiecat is not null
    then
      utl_file.putf (l_file, convert('<functiecategorie>'||r_wnr.functiecat||'</functiecategorie>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.beroepcode is not null
    then
      utl_file.putf (l_file, convert('<beroepcode>'||lpad(r_wnr.beroepcode, 4, '0')||'</beroepcode>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.vvgl_partner is not null
    then
      utl_file.putf (l_file, convert('<voorvoegselsEchtgenoot>'||r_wnr.vvgl_partner||'</voorvoegselsEchtgenoot>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.naam_partner is not null
    then
      utl_file.putf (l_file, convert('<naamEchtgenoot><![CDATA[' ||r_wnr.naam_partner||']]></naamEchtgenoot>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.dat_overlijden is not null
    then
      utl_file.putf (l_file, convert('<datumOverlijden>'||to_char(r_wnr.dat_overlijden, 'yyyy-mm-dd')||'</datumOverlijden>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.straat is not null
    then
      utl_file.putf (l_file, convert('<adresStraat><![CDATA[' ||r_wnr.straat||']]></adresStraat>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.huisnr is not null
    then
      utl_file.putf (l_file, convert('<adresHuisnummer>'||r_wnr.huisnr||'</adresHuisnummer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.huisnrtoev is not null
    then
      utl_file.putf (l_file, convert('<adresToevoeging>'||r_wnr.huisnrtoev||'</adresToevoeging>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.postcode is not null
    then
      utl_file.putf (l_file, convert('<adresPostcode>'||r_wnr.postcode||'</adresPostcode>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.woonplaats is not null
    then
      utl_file.putf (l_file, convert('<adresPlaats><![CDATA['||r_wnr.woonplaats||']]></adresPlaats>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.landcode is not null
    then
      utl_file.putf (l_file, convert('<adresLandcode>'||r_wnr.landcode||'</adresLandcode>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;
    if r_wnr.landnaam is not null
    then
      utl_file.putf (l_file, convert('<adresLandnaam>'||r_wnr.landnaam||'</adresLandnaam>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
    end if;

    utl_file.putf (l_file,convert('</werknemer>\n', 'AL32UTF8', 'WE8MSWIN1252' ));

  end loop;

    utl_file.putf (l_file,convert('</werknemers>\n', 'AL32UTF8', 'WE8MSWIN1252' ));
--
    utl_file.fclose(l_file);

  end if;

end schrijf_oom_bestand;
PROCEDURE SCHRIJF_OOC_BESTAND
 (P_BESTANDSNAAM IN VARCHAR2
 ,P_BSD_TYPE IN VARCHAR2
 )
 IS

cursor c_wgr
is
  select *
  from rle_wg_bestand
  where fonds = 'OOC'
  order by nr_werkgever, dat_ingang_geg;

cursor c_wnr
is
  select *
  from rle_wn_bestand
  where bestandsgroep = 'OOC'
  order by nr_werkgever, nr_persoon, dat_ingang_geg;

fh_wg             utl_file.file_type;
fh_wn             utl_file.file_type;
begin

  if p_bsd_type = 'WGR'
  then

    fh_wg := utl_file.fopen ( cn_directory
                             , p_bestandsnaam
                            , 'W'
                                );

    utl_file.put_line     --kopregel
    (fh_wg, 'NUMMER;NAAM;INGANGSDATUM;EINDDATUM;LIDMAATSCHAP;INGANGSDATUM LIDMAATSCHAP;EINDDATUM LIDMAATSCHAP;RECHTSVORM;FONDSCODE;GROEP VAN WERKZAAMHEDEN;'||
            'SUBGROEP VAN WERKZAAMHEDEN;TER ATTENTIE VAN;ZAAKADRES STRAAT;ZAAKADRES HUISNUMMER;ZAAKADRES TOEVOEGING;ZAAKADRES POSTCODE;ZAAKADRES PLAATS;'||
            'ZAAKADRES LANDCODE;ZAAKADRES LANDNAAM;CORRESPONDENTIEADRES STRAAT;CORRESPONDENTIEADRES HUISNUMMER;CORRESPONDENTIEADRES TOEVOEGING;CORRESPONDENTIEADRES POSTCODE;'||
            'CORRESPONDENTIEADRES PLAATS;CORRESPONDENTIEADRES LANDCODE;CORRESPONDENTIEADRES LANDNAAM;TELEFOONNUMMER;IBAN;DATUM BEEINDIGING;REDEN BEEINDIGING;'||
            'OVERNAME DOOR;AANTAL DIENSTVERBANDEN;LOONSOM;KVK NUMMER;COLLECTIEF VRIJWILLIG CONTRACT');

    for r_wgr in c_wgr
    loop

      utl_file.put_line ( fh_wg
                         ,lpad(r_wgr.nr_werkgever, 6, 0)                                          ||';'||
                          r_wgr.naam                                                              ||';'||
                          r_wgr.dat_ingang_geg                                                    ||';'||
                          r_wgr.dat_einde_geg                                                     ||';'||
                          r_wgr.lidmaatschap                                                      ||';'||
                          r_wgr.dat_ingang_lidm                                                   ||';'||
                          r_wgr.dat_einde_lidm                                                    ||';'||
                          r_wgr.rechtsvorm                                                        ||';'||
                          r_wgr.fonds                                                             ||';'||
                          r_wgr.gvw                                                               ||';'||
                          r_wgr.sgw                                                               ||';'||
                          r_wgr.contactpersoon                                                    ||';'||
                          r_wgr.za_straat                                                         ||';'||
                          r_wgr.za_huisnr                                                         ||';'||
                          r_wgr.za_huisnrtoev                                                     ||';'||
                          r_wgr.za_woonplaats                                                     ||';'||
                          r_wgr.za_postcode                                                       ||';'||
                          r_wgr.za_landcode                                                       ||';'||
                          r_wgr.za_landnaam                                                       ||';'||
                          r_wgr.ca_straat                                                         ||';'||
                          r_wgr.ca_huisnr                                                         ||';'||
                          r_wgr.ca_huisnrtoev                                                     ||';'||
                          r_wgr.ca_postcode                                                       ||';'||
                          r_wgr.ca_woonplaats                                                     ||';'||
                          r_wgr.ca_landcode                                                       ||';'||
                          r_wgr.ca_landnaam                                                       ||';'||
                          r_wgr.tel_nr                                                            ||';'||
                          r_wgr.iban                                                              ||';'||
                          r_wgr.dat_einde                                                         ||';'||
                          r_wgr.reden_einde                                                       ||';'||
                          lpad(r_wgr.overname, 6,0)                                               ||';'||
                          r_wgr.dienstverbanden                                                   ||';'||
                          translate(to_char(round(r_wgr.loonsom , 2), '999999999.99'), '.', ',')  ||';'||
                          r_wgr.kvk_nummer                                                        ||';'||
                          r_wgr.cvc
                       );

    end loop;

    utl_file.fclose(fh_wg);

  else

    fh_wn := utl_file.fopen ( cn_directory
                             , p_bestandsnaam
                            , 'W'
                                );

    utl_file.put_line     --kopregel
    (fh_wn, 'NUMMER;NAAM;VOORLETTERS;VOORVOEGSELS;GESLACHT;GEBOORTEDATUM;INGANGSDATUM;EINDDATUM;WERKGEVERNUMMER;AANVANGSDATUM DIENSTVERBAND;EINDDATUM DIENSTVERBAND;'||
            'OMVANG DIENSTVERBAND;REDEN UITSTROOM;INKOMEN;INKOMENS FREQUENTIE;FUNCTIE CATEGORIE;BEROEPCODE;VOORVOEGSELS ECHTGENOOT;NAAM ECHTGENOOT;DATUM OVERLIJDEN;'||
            'ADRES STRAAT;ADRES HUISNUMMER;ADRES TOEVOEGING;ADRES POSTCODE;ADRES PLAATS;ADRES LANDCODE;ADRES LANDNAAM');

    for r_wnr in c_wnr
    loop

      utl_file.put_line ( fh_wn
                         ,lpad(r_wnr.nr_persoon, 8, 0)                                            ||';'||
                          r_wnr.naam                                                              ||';'||
                          r_wnr.voorletters                                                       ||';'||
                          r_wnr.voorvoegsel                                                       ||';'||
                          r_wnr.geslacht                                                          ||';'||
                          r_wnr.dat_geboorte                                                      ||';'||
                          r_wnr.dat_ingang_geg                                                    ||';'||
                          r_wnr.dat_einde_geg                                                     ||';'||
                          lpad(r_wnr.nr_werkgever,6,0)                                            ||';'||
                          r_wnr.avg_dat_ingang                                                    ||';'||
                          r_wnr.avg_dat_einde                                                     ||';'||
                          r_wnr.avg_uren                                                          ||';'||
                          r_wnr.reden_einde                                                       ||';'||
                          translate(to_char(round(r_wnr.inkomen , 2), '999999999.99'), '.', ',')  ||';'||
                          r_wnr.freq_inkomen                                                      ||';'||
                          r_wnr.functiecat                                                        ||';'||
                          r_wnr.beroepcode                                                        ||';'||
                          r_wnr.vvgl_partner                                                      ||';'||
                          r_wnr.naam_partner                                                      ||';'||
                          r_wnr.dat_overlijden                                                    ||';'||
                          r_wnr.straat                                                            ||';'||
                          r_wnr.huisnr                                                            ||';'||
                          r_wnr.huisnrtoev                                                        ||';'||
                          r_wnr.postcode                                                          ||';'||
                          r_wnr.woonplaats                                                        ||';'||
                          r_wnr.landcode                                                          ||';'||
                          r_wnr.landnaam
                       );

    end loop;

  utl_file.fclose(fh_wn);

  end if;

end schrijf_ooc_bestand;
PROCEDURE SCHRIJF_BESTAND
 (P_BESTANDSGROEP IN VARCHAR2
 ,P_BESTANDTYPE IN VARCHAR2
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS
begin


  if p_bestandsgroep = 'OTIB'
  then

    schrijf_otib_bestand (p_bsd_type =>         p_bestandtype
                         ,p_bestandsnaam =>     p_bestandsnaam);

  elsif p_bestandsgroep = 'OOM'
  then

      schrijf_oom_bestand (p_bsd_type =>         p_bestandtype
                           ,p_bestandsnaam =>     p_bestandsnaam);

  elsif p_bestandsgroep = 'OOC'
  then

    schrijf_ooc_bestand (p_bsd_type =>         p_bestandtype
                        ,p_bestandsnaam =>     p_bestandsnaam);

    end if;

end schrijf_bestand;
PROCEDURE GET_WN_GEGEVENS
 (P_PEILDATUM IN DATE
 ,P_RELATIENUMMER IN NUMBER
 ,P_AANTAL_AVG OUT NUMBER
 ,P_LOONSOM OUT NUMBER
 )
 IS

cursor c_avg (b_rle_numrelatie_wg   in   number
             ,b_peildatum   in   date)
is
  select nvl(sum( decode( wni.code_frequentie
                    , 'M',(  wni.bedrag_basisvaluta
                           * 12.96 )
                    , '4',(  wni.bedrag_basisvaluta
                           * 14.09 )
                    , 'W',(  wni.bedrag_basisvaluta
                           * 56.36 )
                    , 'J',(  wni.bedrag_basisvaluta
                           * 1.08 )
                    , 0
                    )), 0) loonsom
  from   avg_arbeidsverhoudingen avg
       , avg_werknemer_inkomens wni
  where  avg.rle_numrelatie_wg = b_rle_numrelatie_wg
  and    b_peildatum between avg.dat_begin and nvl( avg.dat_einde
                                                  , b_peildatum
                                                  )
  and    wni.avh_id = avg.id
  and    wni.code_inkomenscomponent in( 'SALI', 'UUR', 'PROV' )
  and    b_peildatum between wni.dat_begin and nvl( wni.dat_einde
                                                  , b_peildatum
                                                  )
  and    not exists( select 1
                    from   avg_werknemer_beroepen wbp
                    where  wbp.avg_id = avg.id
                    and    b_peildatum between wbp.dat_begin and nvl( wbp.dat_einde
                                                                    , b_peildatum
                                                                    )
                    and    wbp.code_beroep in( '1000', '1001', '0070', '007', '0007' ))
                                                                                       ;



cursor c_wnr (b_rle_numrelatie_wg   in   number
             ,b_peildatum   in   date)
is
  select count( avg.werknemer ) aantal_werknemers
  from   avg_arbeidsverhoudingen avg
  where  avg.rle_numrelatie_wg = b_rle_numrelatie_wg
  and    b_peildatum between avg.dat_begin and nvl( avg.dat_einde
                                                  , b_peildatum
                                                  )
  and    not exists( select 1
                    from   avg_werknemer_beroepen wbp
                    where  wbp.avg_id = avg.id
                    and    b_peildatum between wbp.dat_begin and nvl( wbp.dat_einde
                                                                    , b_peildatum
                                                                    )
                    and    wbp.code_beroep in( '1000', '1001', '0070', '007', '0007' ))
;

r_avg                 c_avg%rowtype;
r_wnr                 c_wnr%rowtype;
begin

  open c_avg(  p_relatienummer
             , b_peildatum =>  p_peildatum
             );

  fetch c_avg
  into  r_avg;
  close c_avg;

  p_loonsom := r_avg.loonsom;

  open c_wnr(  p_relatienummer
             , b_peildatum =>  p_peildatum
              );

  fetch c_wnr
  into  r_wnr;
  close c_wnr;

  p_aantal_avg := r_wnr.aantal_werknemers;

end get_wn_gegevens;
PROCEDURE AANMAKEN_TMP_WGR
 (P_BSD_NAAM IN VARCHAR2
 )
 IS
begin


      insert into rle_tmp_werkgever
                  (werkgevernummer
                  ,code_functiecategorie
                  ,dataanv
                  ,dateind
                  ,init_datum)
     (                    /* Werkgevers worden middels een subquery bepaald wegen performance overwegingen
         ** Het betreft hier alle werkgevers die na de initiële datum op enig moment
         ** bij OTIB waren/zijn aangesloten.
         */
      select distinct nvl (wsf.wgr_werkgnr, cvc.wgr_werkgnr) werkgevernummer
                     ,nvl (wsf.code_functiecategorie, cvc.code_functiecategorie) code_functiecategorie
                     ,greatest (nvl (wsf.dataanv, cvc.dataanv), nvl (cvc.dataanv, wsf.dataanv) ) dataanv
                     ,least (nvl (wsf.dateind, cvc.dateind), nvl (cvc.dateind, wsf.dateind) ) dateind
                     ,nvl(wsf.dat_begin, cvc.dat_begin)
      from            (select lpad (wgr.nr_werkgever, 6, 0) wgr_werkgnr
                             ,'HB' code_functiecategorie
                             ,greatest (wwg.dat_begin, bst.dat_begin ) dataanv
                             ,wwg.dat_einde dateind
                             ,bst.dat_begin
                       from   wgr_werkgevers wgr
                             ,wgr_werkzaamheden wwg
                             ,wsf wsf
                             ,rle_bestandsabonnementen bst
                       where  wsf.kodefon = bst.code_abonnee
                       and    bst.bsd_naam = p_bsd_naam
                       and    wwg.wgr_id = wgr.id
                       and    wwg.code_groep_werkzhd = wsf.kodegvw
                       and    nvl (wwg.code_subgroep_werkzhd, '@') = nvl (wsf.kodesgw, '@')
                       and    nvl (wwg.dat_einde, bst.dat_begin ) >= bst.dat_begin
                       and    nvl (wsf.dateind, bst.dat_begin ) >= bst.dat_begin
                       and    nvl (wgr.dat_einde, bst.dat_begin ) >= bst.dat_begin ) wsf
                      full outer join
                      (select distinct nvl (h.wgr_werkgnr, b.wgr_werkgnr) wgr_werkgnr
                                      , h.code_functiecategorie || b.code_functiecategorie code_functiecategorie
                                      ,greatest (nvl (h.dataanv, b.dataanv)
                                                ,nvl (b.dataanv, h.dataanv)
                                                ,nvl(h.dat_begin, b.dat_begin)) dataanv
                                      ,least (nvl (h.dateind, b.dateind), nvl (b.dateind, h.dateind) ) dateind
                                      ,nvl(h.dat_begin, b.dat_begin) dat_begin
                       from            (select cvc.wgr_werkgnr
                                              ,cvc.code_functiecategorie
                                              ,cvc.dataanv
                                              ,cvc.dateind
                                              ,bst.dat_begin
                                        from   cvc cvc
                                              ,verzekeringspakketten vzp
                                              ,rle_bestandsabonnementen bst
                                        where  cvc.code_functiecategorie = 'H'
                                        and    nvl (cvc.dateind, bst.dat_begin ) >= bst.dat_begin
                                        and    cvc.vzp_nummer = vzp.verzekeringspakketnummer
                                        and    vzp.fon_kodefon = bst.code_abonnee
                                        and    bst.bsd_naam = p_bsd_naam) h
                                       full outer join
                                       (select cvc.wgr_werkgnr
                                              ,cvc.code_functiecategorie
                                              ,cvc.dataanv
                                              ,cvc.dateind
                                              ,bst.dat_begin
                                        from   cvc cvc
                                              ,verzekeringspakketten vzp
                                              ,rle_bestandsabonnementen bst
                                        where  cvc.code_functiecategorie = 'B'
                                        and    nvl (cvc.dateind, bst.dat_begin ) >= bst.dat_begin
                                        and    cvc.vzp_nummer = vzp.verzekeringspakketnummer
                                        and    vzp.fon_kodefon = bst.code_abonnee
                                        and    bst.bsd_naam = p_bsd_naam) b
                                       on (    h.wgr_werkgnr = b.wgr_werkgnr
                                           and h.dataanv < nvl (b.dateind, h.dataanv + 1)
                                           and b.dataanv < nvl (h.dateind, b.dataanv + 1) )
                                       ) cvc
                      on (    cvc.wgr_werkgnr = wsf.wgr_werkgnr
                          and cvc.dataanv < nvl (wsf.dateind, cvc.dataanv + 1)
                          and wsf.dataanv < nvl (cvc.dateind, wsf.dataanv + 1) )
                      );
end aanmaken_tmp_wgr;
PROCEDURE INSERT_WGR
 (P_BSD_NAAM IN VARCHAR2
 ,P_PEILDATUM IN DATE
 ,P_VORIGE_PEILDATUM IN DATE
 ,P_HERSTART_WGR IN NUMBER
 )
 IS

cursor c_wgr    (b_peildatum          in   date
                ,b_vorige_peildatum   in   date
                ,b_bsd_naam           in   varchar2
                ,b_herstart_wgr       in   number)
is
  with wsf_wgr_set as                    -- xjb, 09-06-2009 voorwaartse declaratie van de relevante werkzaamheid combos
            (select wsf3.kodegvw
                   ,wsf3.kodesgw
                   ,wsf3.kodefon
                   ,wwg.wgr_id
                   ,greatest (wsf3.dataanv, wwg.dat_begin) dataanv
                   ,case
                       when least (nvl (wwg.dat_einde, cn_verre_toekomst), nvl (wsf3.dateind, cn_verre_toekomst) ) =
                                                                                                 cn_verre_toekomst
                          then to_date (null)
                       else least (nvl (wwg.dat_einde, cn_verre_toekomst), nvl (wsf3.dateind, cn_verre_toekomst) )
                    end dateind
                   ,wwg.code_reden_einde
                   ,bst.dat_begin
             from   wsf wsf3
                   ,wgr_werkzaamheden wwg
                   ,rle_bestandsabonnementen bst
             where  wsf3.kodefon = bst.code_abonnee
             and    bst.bsd_naam = b_bsd_naam
             and    wwg.code_groep_werkzhd = wsf3.kodegvw
             and    nvl (wwg.code_subgroep_werkzhd, '@') = nvl (wsf3.kodesgw, '@')
             and    wsf3.dataanv <= nvl (wwg.dat_einde, wsf3.dataanv)
             and    wwg.dat_begin <= nvl (wsf3.dateind, wwg.dat_begin)
             --
             union   --union (ipv union all) is noodzakelijk ivm cvc.code_functiecategorie
             --
             select null kodegvw
                   ,null kodesgw
                   ,vzp.fon_kodefon kodefon
                   ,wgr.id wgr_id
                   ,cvc.dataanv dataanv
                   ,nvl (cvc.dateind, vzp.einddatum) dateind
                   ,null code_reden_einde
                   ,bst.dat_begin
             from   cvc cvc
                   ,verzekeringspakketten vzp
                   ,wgr_werkgevers wgr
                   ,rle_bestandsabonnementen bst
             where  vzp.fon_kodefon = bst.code_abonnee
             and    bst.bsd_naam = b_bsd_naam
             and    vzp.verzekeringspakketnummer = cvc.vzp_nummer
             and    lpad (wgr.nr_werkgever, 6, 0) = cvc.wgr_werkgnr
            )
  select   wgr.nr_werkgever nr_werkgever
          ,rle.numrelatie rle_numrelatie
          ,nvl (rle.handelsnaam, rle.namrelatie) handelsnaam
          ,greatest (wsf2.dat_begin , wsf2.dataanv, wgr.rvm_dat_begin, ras.datingang, rle.datbegin)
                                                                                            ingangsdatum
          ,case
              when least (nvl (wsf2.dateind, cn_verre_toekomst)
                         ,nvl (ras.dateinde, cn_verre_toekomst)
                         ,nvl (rle.dateinde, cn_verre_toekomst)
                         ,nvl (wgr.rvm_dat_einde, cn_verre_toekomst)) = cn_verre_toekomst
                 then to_date (null)
              else least (nvl (wsf2.dateind, cn_verre_toekomst)
                         ,nvl (ras.dateinde, cn_verre_toekomst)
                         ,nvl (rle.dateinde, cn_verre_toekomst)
                         ,nvl (wgr.rvm_dat_einde, cn_verre_toekomst))
           end einddatum
          ,wgr.code_rechtsvorm rechtsvorm
          ,case   /* Let op, werksfeer OLC is niet afgesloten wanneer werksfeer OTIB_L is opgevoerd */
              when wsf2.kodefon = 'OLC'
                 then 'OTIB_L'
              when wsf2.kodefon = 'SKO'
                 then 'OTIB_K'
              when wsf2.kodefon = 'OFE'
                 then 'OTIB_E'
              else wsf2.kodefon
           end fonds
         ,wsf2.kodegvw gvw
         ,wsf2.kodesgw sgw
         ,stt.straatnaam straat
         ,adr.huisnummer huisnr
         ,adr.toevhuisnum toev_huisnr
         ,wps.woonplaats woonplaats
         ,case
              when adr.lnd_codland = 'NL'
                 then substr (adr.postcode, 1, 4) || ' ' || substr (adr.postcode, 5)
              else adr.postcode
          end postcode
         ,lnd.landcode_iso3166_a2 landcode
         ,lnd.naam  landnaam
         ,greatest (wsf2.dataanv, wsf2.dat_begin ) dat_ingang_lidm
         ,wsf2.dateind eind_dat_lid
         ,wsf2.code_reden_einde reden_einde
         ,ovn.ovn_overnamebedrijf  ovn_overnamebedrijf
         ,ovn.ovn_rkn_dat_begin
         ,wsf2.dat_begin
  from      wsf_wgr_set wsf2
          , (select wgr.id
                   ,wgr.nr_werkgever
                   ,wgr.dat_mutatie  wgr_dat_mutatie
                   ,rvm.dat_begin    rvm_dat_begin
                   ,rvm.dat_einde    rvm_dat_einde
                   ,rvm.dat_mutatie  rvm_dat_mutatie
                   ,rvm.code_rechtsvorm
             from   wgr_werkgevers wgr
                   ,wgr_rechtsvormen rvm
             where  wgr.id = rvm.wgr_id
             union all                                 -- xjb, 09-06-2009
             select wgr3.id                            -- simuleer de ontbrekende rvm records: anders vallen
             ,      wgr3.nr_werkgever                  -- werkzaamheid periodes ten onrechte uit de hoofd query
             ,      wgr3.dat_mutatie  wgr_dat_mutatie
             ,      wsf4.dataanv      rvm_dat_begin
             ,      wsf4.dateind      rvm_dat_einde
             ,      NULL              rvm_dat_mutatie
             ,      NULL              code_rechtsvorm
             from   wgr_werkgevers wgr3
             ,      wsf_wgr_set    wsf4
             where  wgr3.id = wsf4.wgr_id
             and not exists (select 1
                             from   wgr_rechtsvormen rvm3
                             where  rvm3.wgr_id = wgr3.id
                             and    rvm3.dat_begin <= nvl(wsf4.dateind, rvm3.dat_begin)
                            )
            ) wgr
          , (select wgr2.nr_werkgever  ovn_overnamebedrijf
             ,      rkn.rle_numrelatie         ovn_rle_numrelatie
             ,      rkn.dat_begin                  ovn_rkn_dat_begin
             ,      rkn.dat_einde                  ovn_rkn_dat_einde
             from   rle_relatie_koppelingen rkn
                   ,rle_rol_in_koppelingen rkg
                   ,rle_relaties rle2
                   ,wgr_werkgevers wgr2
             where  rkn.rkg_id = rkg.id
             and    rle2.numrelatie = rkn.rle_numrelatie_voor
             and    wgr2.nr_relatie = rle2.numrelatie
             and    rkg.code_rol_in_koppeling in ('FUST', 'OVND', 'AFSN', 'RVGR')   /* Fusie, Overname, Afslitsing of Rechtsvoorganger */
            ) ovn
          ,rle_relatie_externe_codes rec
          ,rle_relaties rle
          ,rle_relatie_adressen ras
          ,rle_adressen adr
          ,rle_straten stt
          ,rle_woonplaatsen wps
          ,rle_landen lnd
  where    wsf2.wgr_id = wgr.id
  --
  and      lpad (wgr.nr_werkgever, 6, 0) = rec.extern_relatie
  and      wgr.nr_werkgever >= b_herstart_wgr
  and      rec.dwe_wrddom_extsys = 'WGR'
  and      rec.rol_codrol = 'WG'
  --
  and      rec.rle_numrelatie = rle.numrelatie
  --
  and      rec.rle_numrelatie = ras.rle_numrelatie
  --
  and      ovn.ovn_rle_numrelatie (+) = rle.numrelatie
  --
  and      ras.dwe_wrddom_srtadr = 'SZ'
  and      ras.ads_numadres = adr.numadres
  --
  and      adr.stt_straatid = stt.straatid
  --
  and      adr.wps_woonplaatsid = wps.woonplaatsid
  and      adr.lnd_codland = lnd.codland
  --
  and      wsf2.dataanv <= nvl (rle.dateinde, wsf2.dataanv)
  and      wsf2.dataanv <= nvl (ras.dateinde, wsf2.dataanv)
  and      wsf2.dataanv <= nvl (wgr.rvm_dat_einde, wsf2.dataanv)
  --
  and      ras.datingang <= nvl (rle.dateinde, ras.datingang)
  and      ras.datingang <= nvl (wsf2.dateind, ras.datingang)
  and      ras.datingang <= nvl (wgr.rvm_dat_einde, ras.datingang)
  --
  and      rle.datbegin <= nvl (wsf2.dateind, rle.datbegin)
  and      rle.datbegin <= nvl (ras.dateinde, rle.datbegin)
  and      rle.datbegin <= nvl (wgr.rvm_dat_einde, rle.datbegin)
  --
  and      wgr.rvm_dat_begin <= nvl (wsf2.dateind, wgr.rvm_dat_begin)
  and      wgr.rvm_dat_begin <= nvl (rle.dateinde, wgr.rvm_dat_begin)
  and      wgr.rvm_dat_begin <= nvl (ras.dateinde, wgr.rvm_dat_begin)
  --
  and      nvl (wsf2.dateind, wsf2.dat_begin) >= wsf2.dat_begin
  and      nvl (rle.dateinde, wsf2.dat_begin) >= wsf2.dat_begin
  and      nvl (ras.dateinde, wsf2.dat_begin) >= wsf2.dat_begin
  and      nvl (wgr.rvm_dat_einde, wsf2.dat_begin) >= wsf2.dat_begin
  --
  and      (   /* Gegevens tussen :b_vorige_peildatum en :b_peildatum geldig (geweest) */
               (    b_peildatum >= wgr.rvm_dat_begin
                and nvl (wgr.rvm_dat_einde, b_peildatum) >= b_vorige_peildatum
                and b_peildatum >= wsf2.dataanv
                and nvl (wsf2.dateind, b_peildatum) >= b_vorige_peildatum
                and b_peildatum >= rle.datbegin
                and nvl (rle.dateinde, b_peildatum) >= b_vorige_peildatum
                and b_peildatum >= ras.datingang
                and nvl (ras.dateinde, b_peildatum) >= b_vorige_peildatum)
            or   /*Of... gegevens afgelopen maand gemuteerd */
               (   wgr.wgr_dat_mutatie >= b_vorige_peildatum   -- Werkgever gemuteerd
                or wgr.rvm_dat_mutatie >= b_vorige_peildatum   -- Rechtsvorm gemuteerd
                or rle.dat_mutatie >= b_vorige_peildatum   -- Relatie gemuteerd
                or ras.dat_mutatie >= b_vorige_peildatum   -- Statutaire Zetel gemuteerd
                or adr.dat_mutatie >= b_vorige_peildatum   -- Statutaire Zetel gemuteerd
                or exists (select 1   -- Werkzaamheid gemuteerd
                           from   wgr_werkzaamheden wwg
                           where  wwg.wgr_id = wgr.id
                           and    wwg.dat_mutatie >= b_vorige_peildatum)
                or exists (   -- Werksfeer gemuteerd
                      select 1
                      from   wsf_h wsfh
                      where  wsf2.kodefon = wsfh.kodefon
                      and    wsf2.dataanv = wsfh.dataanv
                      and    wsf2.kodegvw = wsfh.kodegvw
                      and    wsf2.kodesgw = wsfh.kodesgw
                      and    wsfh.registratiedatum >= b_vorige_peildatum)
                or exists (   -- Correspondentie adres gemuteerd
                      select 1
                      from   rle_adressen adr
                            ,rle_relatie_adressen ras
                      where  adr.numadres = ras.ads_numadres
                      and    rle.numrelatie = ras.rle_numrelatie
                      and    ras.dwe_wrddom_srtadr = 'CA'
                      and    adr.dat_mutatie >= b_vorige_peildatum
                      and    ras.dat_mutatie >= b_vorige_peildatum) ) )
  order by wgr.nr_werkgever asc;

--wgo
cursor c_wgo (b_relatienummer in number)
is
  select rec.extern_relatie
  from rle_relatie_koppelingen rkn
      ,rle_relatie_externe_codes rec
  where rkn.rle_numrelatie = b_relatienummer
    and rec.rle_numrelatie = rkn.rle_numrelatie_voor
    and rkn.rkg_id = (select rkg.id
                      from rle_rol_in_koppelingen rkg
                      where rkg.code_rol_in_koppeling = 'WB')
    and rec.rol_codrol = 'BO'
    and rec.dwe_wrddom_extsys = 'BOR'
    and rkn.dat_begin <= sysdate
    and nvl(rkn.dat_einde, sysdate) >= sysdate;

--iban
cursor c_fns (b_relatienummer in varchar2)
is
 select  fns.iban
from rle_financieel_nummers fns
where fns.rol_codrol = 'WG'
  and fns.datingang <= sysdate
  and nvl(fns.dateinde, sysdate) >= sysdate
  and coddoelrek = coalesce ('ALGRN', 'AIC')
  and fns.rle_numrelatie = b_relatienummer;

cursor c_cns (b_relatienummer in number)
is
  select numcommunicatie
  from comp_rle.rle_communicatie_nummers crn
  where dwe_wrddom_srtcom = 'TEL'
  and regexp_instr (lower(numcommunicatie), '[a-z]', 1, 1) = 0
  and nvl(crn.dateinde, sysdate) >=sysdate
  and crn.rle_numrelatie = b_relatienummer
  and not exists (select 1
                  from comp_rle.rle_communicatie_nummers crn2
                  where crn.id = crn2.id
                  and (crn2.numcommunicatie not like '%-%'
                  and crn2.numcommunicatie not like '%(%'
                  and crn2.numcommunicatie not like '%)%'
                  and crn2.numcommunicatie not like '%*%'
                  and crn2.numcommunicatie not like '%/%'
                  and crn2.numcommunicatie not like '%\%'));

l_ovn_wgr            number;
r_adr                c_adr%rowtype;
r_wgo                c_wgo%rowtype;
r_cns                c_cns%rowtype;
l_aantal_wnr         number;
l_wgo                varchar2(50);
l_contactpersoon_id  number;
l_contactpersoon     varchar(500);
l_telefoonnummer     varchar2(50);
l_melding            varchar2(40);
l_dummy              varchar2(10);
l_iban               varchar2(40);
l_kvknummer          varchar2(25);
l_ind_cvc            varchar2(10);
l_aantal_avg         number;
l_loonsom            number;
l_bestandsgroep      varchar2(10);
l_herstart_wgr       number := 0;
l_initiele_datum     date;
l_length             number;
begin


 if p_bsd_naam = 'OTIB_WGR'
 then
   l_initiele_datum := to_date('01-01-1992', 'dd-mm-yyyy');
 else
   l_initiele_datum := to_date('01-01-2013', 'dd-mm-yyyy');
 end if;

  for r_wgr in c_wgr (p_peildatum, p_vorige_peildatum, p_bsd_naam, p_herstart_wgr)
  loop

    if l_herstart_wgr <> r_wgr.nr_werkgever
    then

      stm_util.schrijf_herstartsleutel('l_herstart_wgr', r_wgr.nr_werkgever);
      commit;
      l_herstart_wgr := r_wgr.nr_werkgever;

    end if;


    if nvl(r_wgr.einddatum, r_wgr.dat_begin) + 1 >= r_wgr.ovn_rkn_dat_begin
         and r_wgr.dat_ingang_lidm < r_wgr.ovn_rkn_dat_begin
    then
      l_ovn_wgr := r_wgr.ovn_overnamebedrijf;
    else
      l_ovn_wgr := null;
    end if;

    r_adr:=null;

    --ophalen correspondentie adres
    open c_adr (r_wgr.rle_numrelatie, p_peildatum, p_vorige_peildatum);

    fetch c_adr
    into  r_adr;

    close c_adr;

    l_wgo := null;
    --ophalen werkgeversorganisatie
    open c_wgo (r_wgr.rle_numrelatie);
    fetch c_wgo into r_wgo;
    close c_wgo;

    if p_bsd_naam = 'OTIB_WGR'
       and r_wgo.extern_relatie in ('UNETO-VNI', 'NVKL', 'STEK')
    then
      l_wgo := r_wgo.extern_relatie;
    elsif p_bsd_naam = 'OOM_WGR'
      and r_wgo.extern_relatie in ('MU', 'NGO', 'NVM', 'ORTHOBANDA', 'ROMAZO', 'VSBN')
    then
      l_wgo := r_wgo.extern_relatie;
    elsif p_bsd_naam = 'OOC_WGR'
      and r_wgo.extern_relatie = 'FOCWA'
    then
      l_wgo := r_wgo.extern_relatie;
    end if;


     -- ophalen contactpersoon
    rle_get_contactpersoon( r_wgr.rle_numrelatie, 'SZ', null, l_contactpersoon_id );
    l_contactpersoon := rle_get_verzendnaam( l_contactpersoon_id);

    --ophalen telefoonnr
    l_telefoonnummer := null;

    open c_cns (r_wgr.rle_numrelatie);
    fetch c_cns into r_cns;

    if c_cns%found
    then
      l_telefoonnummer := r_cns.numcommunicatie;
    end if;
    close c_cns;

    --ophalen IBAN
    l_iban := null;
    open c_fns (r_wgr.rle_numrelatie);
    fetch c_fns into l_iban;
    close c_fns;

    --ophalen loonsom en aantal_avg
    get_wn_gegevens(p_peildatum, r_wgr.rle_numrelatie, l_aantal_avg, l_loonsom);

    --ophalen kvk
    begin
      get_extcod( 'KVK'
                , 'WG'
                , r_wgr.rle_numrelatie
                , sysdate
                , l_kvknummer);

      l_length := length(l_kvknummer);

      if l_length <> 8
      then
        l_kvknummer := null;
      end if;
    exception
      when others
      then
        l_kvknummer := null;
    end;

    if r_wgr.gvw is null
    then

      l_ind_cvc := 'true';

    else

      l_ind_cvc := 'false';

    end if;

    l_bestandsgroep :=    substr(p_bsd_naam, 0, instr(p_bsd_naam,  '_')-1)  ;

    insert into rle_wg_bestand
      (fonds
      ,nr_werkgever
      ,rle_numrelatie
      ,naam
      ,dat_ingang_geg
      ,dat_einde_geg
      ,lidmaatschap
      ,dat_ingang_lidm
      ,dat_einde_lidm
      ,rechtsvorm
      ,gvw
      ,sgw
      ,contactpersoon
      ,za_straat
      ,za_huisnr
      ,za_huisnrtoev
      ,za_postcode
      ,za_woonplaats
      ,za_landcode
      ,za_landnaam
      ,ca_straat
      ,ca_huisnr
      ,ca_huisnrtoev
      ,ca_postcode
      ,ca_woonplaats
      ,ca_landcode
      ,ca_landnaam
      ,tel_nr
      ,iban
      ,dat_einde
      ,reden_einde
      ,overname
      ,dienstverbanden
      ,loonsom
      ,kvk_nummer
      ,cvc
      ,bestandsgroep
      ) values
           (r_wgr.fonds
           ,r_wgr.nr_werkgever
           ,r_wgr.rle_numrelatie
           ,r_wgr.handelsnaam
           ,r_wgr.ingangsdatum
           ,r_wgr.einddatum
           ,l_wgo
           ,r_wgr.dat_ingang_lidm
           ,r_wgr.eind_dat_lid
           ,r_wgr.rechtsvorm
           ,r_wgr.gvw
           ,r_wgr.sgw
           ,l_contactpersoon
           ,r_wgr.straat
           ,r_wgr.huisnr
           ,r_wgr.toev_huisnr
           ,r_wgr.postcode
           ,r_wgr.woonplaats
           ,r_wgr.landcode
           ,r_wgr.landnaam
           ,r_adr.straatnaam
           ,r_adr.huisnummer
           ,r_adr.toevhuisnum
           ,r_adr.postcode
           ,r_adr.woonplaats
           ,r_adr.landcode
           ,r_adr.landnaam
           ,l_telefoonnummer
           ,l_iban
           ,r_wgr.eind_dat_lid
           ,r_wgr.reden_einde
           ,l_ovn_wgr
           ,l_aantal_avg
           ,l_loonsom
           ,l_kvknummer
           ,l_ind_cvc
           ,l_bestandsgroep);

  end loop;


end insert_wgr;
PROCEDURE INSERT_WNR
 (P_BSD_NAAM IN VARCHAR2
 ,P_PEILDATUM IN DATE
 ,P_VORIGE_PEILDATUM IN DATE
 ,P_HERSTART_WGR IN NUMBER
 )
 IS

      /******************************************************************************
         NAAM:       aanmaken_wnr_bsd
         DOEL:       Aanmaken van het OTIB werknemerbestand

         REVISIES:
         Versie     Datum       Auteur           Beschrijving
         ---------  ----------  ---------------  ------------------------------------
         1          11-02-2008  R.Podt      XRP  Aanmaak routine
         2          13-01-2009  R.Podt      XRP  Cursor c_wnr001 gewijzigd;
                                                 1) geldigheidsperiode ext.code is verwijderd
        3          22-06-2009  monique v alphen     provisie en oproepkrachten meegenomen
        4          26-02-2010  monique v alphen    sofinummer optioneel gemaakt
        5         25-06-2013   Monique v Alphen   Aangepast voor creatie nieuwe batch
      ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type                 := cn_package || '.AANMAKEN_WNR_BSD';
      l_bestandsnaam       rle_uitvoer_statussen.bestandsnaam%type;

      cursor c_wgr (b_herstart_wgr in number)
      is
        select to_number(werkgevernummer) werkgevernummer
        from rle_tmp_werkgever
        where werkgevernummer > to_char(b_herstart_wgr)
        order by 1;

      /* ........... */
      cursor c_wnr001 (
         b_peildatum          in   date
        ,b_vorige_peildatum   in   date
        ,b_nr_werkgever       in   number)
      is
        select   avg.werkgever werkgevernummer
                 ,avg.werknemer persoonsnummer
                 ,greatest (wni.dat_begin, wnu.dat_begin, wbp.dat_begin, wgr2.dataanv, wgr2.init_datum) ingangsdatum
                 ,case
                     when least (nvl (wni.dat_einde, cn_verre_toekomst)
                                ,nvl (wnu.dat_einde, cn_verre_toekomst)
                                ,nvl (wbp.dat_einde, cn_verre_toekomst)
                                ,nvl (wgr2.dateind, cn_verre_toekomst) ) = cn_verre_toekomst
                        then to_date (null)
                     else least (nvl (wni.dat_einde, cn_verre_toekomst)
                                ,nvl (wnu.dat_einde, cn_verre_toekomst)
                                ,nvl (wbp.dat_einde, cn_verre_toekomst)
                                ,nvl (wgr2.dateind, cn_verre_toekomst) )
                  end einddatum
                 ,brp.code_beroep brp_code_beroep
                 ,wni.bedrag_basisvaluta bedrag
                 ,wni.code_frequentie freq_uitbetaling
                 ,wnu.uren_vastgesteld uren
                 ,rle.datgeboorte geboortedatum
                 ,rle.dwe_wrddom_geslacht geslacht
                 ,rle.namrelatie naam
                 ,rle.voorletter voorletters
                 ,rle.datoverlijden overlijdensdatum
                 ,(select waarde from domeinwaarden vvgsl where vvgsl.domeincode = 'VVL' and vvgsl.code = rle.dwe_wrddom_vvgsl  )  voorvoegsels
                 ,rle.naam_partner naam_echtgenoot
                 ,(select waarde from domeinwaarden vvgsl where vvgsl.domeincode = 'VVL' and vvgsl.code = rle.vvgsl_partner )      voorvoegsels_echtgenoot
                 ,avg.dat_begin avg_ingangsdatum
                 ,avg.dat_einde avg_einddatum
                 ,avg.reden_einde
                 ,brp.code_functiecategorie code_functiecategorie
                 ,rle.numrelatie
         from     avg_arbeidsverhoudingen avg
                 ,avg_werknemer_inkomens wni
                 ,avg_werknemer_beroepen wbp
                 ,avg_werknemer_uren wnu
                 ,beroepen brp
                 ,rle_relaties rle
                 , (select rle_numrelatie
                          ,extern_relatie
                          ,dat_mutatie
                    from   rle_relatie_externe_codes
                    where  rol_codrol = 'PR'
                    and    dwe_wrddom_extsys = 'PRS') rec   --relatie --> persoon
                  ,rle_tmp_werkgever wgr2
         where    avg.werkgever = wgr2.werkgevernummer
         and      instr (wgr2.code_functiecategorie, brp.code_functiecategorie) > 0
         and      to_number(wgr2.werkgevernummer) = b_nr_werkgever
         --
         and      rec.extern_relatie = avg.werknemer
         --
         and      rec.rle_numrelatie = rle.numrelatie
         --
         and      avg.id = wni.avh_id
         and      wni.code_inkomenscomponent in ('SALI', 'PROV', 'UUR')
         --
         and      avg.id = wbp.avg_id
         --
         and      avg.id = wnu.avh_id
         --
         and      avg.id = wbp.avg_id
         --
         and      wbp.code_beroep = brp.code_beroep
         --
         and      wnu.dat_begin <= nvl (wni.dat_einde, cn_verre_toekomst)
         and      wnu.dat_begin <= nvl (wbp.dat_einde, cn_verre_toekomst)
         and      wnu.dat_begin <= nvl (wgr2.dateind, cn_verre_toekomst)
         --
         and      wni.dat_begin <= nvl (wbp.dat_einde, cn_verre_toekomst)
         and      wni.dat_begin <= nvl (wnu.dat_einde, cn_verre_toekomst)
         and      wni.dat_begin <= nvl (wgr2.dateind, cn_verre_toekomst)
         --
         and      wbp.dat_begin <= nvl (wni.dat_einde, cn_verre_toekomst)
         and      wbp.dat_begin <= nvl (wnu.dat_einde, cn_verre_toekomst)
         and      wbp.dat_begin <= nvl (wgr2.dateind, cn_verre_toekomst)
         --
         and      wgr2.dataanv <= nvl (wni.dat_einde, cn_verre_toekomst)
         and      wgr2.dataanv <= nvl (wbp.dat_einde, cn_verre_toekomst)
         and      wgr2.dataanv <= nvl (wnu.dat_einde, cn_verre_toekomst)
         --
         and      least (nvl (wni.dat_einde, cn_verre_toekomst)
                        ,nvl (wnu.dat_einde, cn_verre_toekomst)
                        ,nvl (wbp.dat_einde, cn_verre_toekomst)
                        ,nvl (wgr2.dateind, cn_verre_toekomst) ) >= wgr2.init_datum
         --
         and      (   -- Gegevens tussen :b_vorige_peildatum en :b_peildatum geldig (geweest)
                      (    b_peildatum >= wni.dat_begin
                       and nvl (wni.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= wnu.dat_begin
                       and nvl (wnu.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= wbp.dat_begin
                       and nvl (wbp.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= avg.dat_begin
                       and nvl (avg.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= wgr2.dataanv
                       and nvl (wgr2.dateind, b_peildatum) >= b_vorige_peildatum)
                   or   -- Of... gegevens afgelopen maand gemuteerd
                      (   avg.dat_mutatie >= b_vorige_peildatum   -- Arbeidsrelatie gemuteerd
                       or wni.dat_mutatie >= b_vorige_peildatum   -- Werknemer inkomens gemuteerd
                       or wnu.dat_mutatie >= b_vorige_peildatum   -- Werknemer uren gemuteerd
                       or wbp.dat_mutatie >= b_vorige_peildatum   -- Werknemer beroepen gemuteerd
                       or rec.dat_mutatie >= b_vorige_peildatum   -- Relatie externe code gemuteerd
                       or rle.dat_mutatie >= b_vorige_peildatum   -- Relatie gemuteerd
                       or exists (   /* Opgegeven adres gemuteerd */
                             select 1
                             from   rle_adressen adr
                                   ,rle_relatie_adressen ras
                             where  adr.numadres = ras.ads_numadres
                             and    ras.rle_numrelatie = rle.numrelatie
                             and    ras.dwe_wrddom_srtadr = 'OA'
                             and    (   adr.dat_mutatie >= b_vorige_peildatum   -- Adres gemuteerd
                                     or ras.dat_mutatie >= b_vorige_peildatum)   -- Relatie adres gemuteerd
                                                                              ) ) )
         order by avg.werkgever asc;


r_adr                c_adr%rowtype;
l_bestandsgroep      varchar2(10);
l_vorige_peildatum   date;
l_herstart_wgr       number := 0;
begin


  aanmaken_tmp_wgr (p_bsd_naam => p_bsd_naam);

  for r_wgr in c_wgr (p_herstart_wgr)
  loop
    if l_herstart_wgr <> r_wgr.werkgevernummer
    then
      stm_util.schrijf_herstartsleutel('l_herstart_wgr', r_wgr.werkgevernummer);
      commit;
      l_herstart_wgr := r_wgr.werkgevernummer;

    end if;


     /* Geldige werkgevers of werkgevers waarvan de werksfeer gewijzigd is */
     for r_wnr in c_wnr001 (p_peildatum, p_vorige_peildatum, r_wgr.werkgevernummer)
     loop

       if l_herstart_wgr <> r_wnr.werkgevernummer
       then
         stm_util.schrijf_herstartsleutel('l_herstart_wgr', l_herstart_wgr);
         commit;
         l_herstart_wgr := r_wnr.werkgevernummer;

       end if;

        r_adr := null; -- BUD 13-04-2016 SDM12153476
        open c_adr (r_wnr.numrelatie, p_peildatum, p_vorige_peildatum);

        fetch c_adr
        into  r_adr;

        close c_adr;

        l_bestandsgroep :=    substr(p_bsd_naam, 0, instr(p_bsd_naam,  '_')-1)  ;

        insert into rle_wn_bestand
          (nr_persoon
          ,rle_numrelatie
          ,voorletters
          ,voorvoegsel
          ,naam
          ,geslacht
          ,dat_geboorte
          ,dat_ingang_geg
          ,dat_einde_geg
          ,nr_werkgever
          ,avg_dat_ingang
          ,avg_dat_einde
          ,avg_uren
          ,reden_einde
          ,inkomen
          ,freq_inkomen
          ,functiecat
          ,beroepcode
          ,vvgl_partner
          ,naam_partner
          ,dat_overlijden
          ,straat
          ,huisnr
          ,huisnrtoev
          ,postcode
          ,woonplaats
          ,landcode
          ,landnaam
          ,bestandsgroep)
       values
         (r_wnr.persoonsnummer
         ,r_wnr.numrelatie
         ,r_wnr.voorletters
         ,r_wnr.voorvoegsels
         ,r_wnr.naam
         ,r_wnr.geslacht
         ,r_wnr.geboortedatum
         ,r_wnr.ingangsdatum
         ,r_wnr.einddatum
         ,r_wnr.werkgevernummer
         ,r_wnr.avg_ingangsdatum
         ,r_wnr.avg_einddatum
         ,r_wnr.uren
         ,r_wnr.reden_einde
         ,r_wnr.bedrag
         ,r_wnr.freq_uitbetaling
         ,r_wnr.code_functiecategorie
         ,r_wnr.brp_code_beroep
         ,r_wnr.voorvoegsels_echtgenoot
         ,r_wnr.naam_echtgenoot
         ,r_wnr.overlijdensdatum
         ,r_adr.straatnaam
         ,r_adr.huisnummer
         ,r_adr.toevhuisnum
         ,r_adr.postcode
         ,r_adr.woonplaats
         ,r_adr.landcode
         ,r_adr.landnaam
         ,l_bestandsgroep
         );

     end loop;
  end loop;


end insert_wnr;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN PLS_INTEGER
 ,P_BESTANDSGROEP IN VARCHAR2
 ,P_PEILDATUM IN DATE := trunc(sysdate, 'MON')
 ,P_VORIGE_PEILDATUM IN DATE := to_date(null)
 )
 IS


  cn_module   constant  stm_util.t_procedure%type                 := cn_package || '.VERWERK';
  l_volgnummer          stm_job_statussen.volgnummer%type         := 1;
  l_vorige_peildatum    date;
  l_vorige_rundatum     date;
  l_rundatum            date;
  l_bestandsnaam        rle_uitvoer_statussen.bestandsnaam%type;
  -- xtra var voor stm_lijsten
  l_programma           varchar2(100);
  l_regelnr             number(10,0) := 0; -- Regelnummer voor stm_lijsten
  l_lijstnummer         number:=1;
  --l_volgnummer        number := 0;
  l_foutomgeving        varchar2(2000);
  l_foutmelding         varchar2(2000);
  l_proc_naam           varchar2(50) := cn_module;
  l_rowcount            number := 0;

  l_bestandsgroep       varchar2(20);
  l_bestandtype         varchar2(3);
  l_bsd_naam            varchar2(20);
  l_procedure           varchar2(100);
  l_herstart_wgr        number;
BEGIN
  l_foutomgeving := '001 - ' || l_proc_naam || ' - gestart om : ' || to_char( sysdate, 'DD-MM-YYYY HH24:MI:SS');

  stm_util.debug('p_peildatum: '||p_peildatum);
  --
  l_programma := null;
  --
  stm_util.t_programma_naam := substr (cn_module, 1, instr (cn_module, '.') - 1);
  stm_util.module_start (substr (cn_module, instr (cn_module, '.') + 1) );
  stm_util.t_script_naam := p_script_naam;
  stm_util.t_script_id := to_number (p_script_id);
  stm_util.t_tekst := null;

  if stm_util.herstart
  then
    --
    -- ophalen herstart-gegevens en herstart in LIJSTEN tabel zetten
    --
    l_procedure := stm_util.lees_herstartsleutel( 'l_procedure' );
    l_vorige_peildatum := stm_util.lees_herstartsleutel ('l_vorige_peildatum');
    l_vorige_rundatum := stm_util.lees_herstartsleutel ('l_vorige_rundatum');
    l_rundatum := stm_util.lees_herstartsleutel ('l_rundatum');
    l_bestandsgroep:= stm_util.lees_herstartsleutel('l_bestandsgroep');
    l_bestandtype := stm_util.lees_herstartsleutel('l_bestandtype');
    l_herstart_wgr := stm_util.lees_herstartsleutel ('l_herstart_wgr');
    l_regelnr := stm_util.lees_herstarttelling( 'l_regelnr' );


    stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                            , il_regelnummer =>    l_regelnr
                            , il_tekst =>          'Herstart van procedure ' || stm_util.t_programma_naam || ' op '
                                                                             || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss'   )
                             );

    stm_util.insert_lijsten( il_lijstnummer =>    l_lijstnummer
                           , il_regelnummer =>    l_regelnr
                           , il_tekst =>          'Parameter p_bestandsgroep: '||l_bestandsgroep);
  else
        --
    l_regelnr := 0;
    --
    l_foutomgeving :=
     '020 - '
     || l_proc_naam
     || ' - Nieuwe start '
     || stm_util.t_programma_naam
     || ' Parameter P_BESTANDSGROEP : '
     || P_BESTANDSGROEP
     || ' Parameter P_PEILDATUM: '
     || to_char( P_PEILDATUM )
     || ' Parameter P_VORIGE_PEILDATUM: '
     || to_char( P_VORIGE_PEILDATUM );

    l_procedure := 'bepaal_parameters_wgr';
    l_bestandsgroep := p_bestandsgroep;
    l_programma := null;
    l_vorige_peildatum := null;
    l_vorige_rundatum := null;
    l_rundatum := null;
    l_bestandtype := 'WGR';
    l_herstart_wgr := 0;

    delete from rle_wg_bestand;

    delete from rle_wn_bestand;

    delete from rle_tmp_werkgever;

    stm_util.insert_lijsten( il_lijstnummer => l_lijstnummer
                           , il_regelnummer => l_regelnr
                           , il_tekst => 'Start ' || stm_util.t_programma_naam
                                     || ' op ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss')
                           );

    stm_util.insert_lijsten( il_lijstnummer => l_lijstnummer
                           , il_regelnummer => l_regelnr
                            , il_tekst => 'Parameter P_BESTANDSGROEP    : ' || P_BESTANDSGROEP
                           );

    stm_util.insert_lijsten( il_lijstnummer => l_lijstnummer
                           , il_regelnummer => l_regelnr
                           , il_tekst => 'Parameter P_PEILDATUM        : ' || to_char( P_PEILDATUM )
                           );

    stm_util.insert_lijsten( il_lijstnummer => l_lijstnummer
                           , il_regelnummer => l_regelnr
                           , il_tekst => 'Parameter P_VORIGE_PEILDATUM : ' || to_char( P_VORIGE_PEILDATUM )
                            );
  end if;
      --
  stm_util.schrijf_herstarttelling('l_regelnr',l_regelnr);
  stm_util.debug ('type: '||l_bestandtype);
  stm_util.debug ('procedure: '||l_procedure);
  stm_util.debug ('bestandsgroep: '||l_bestandsgroep);
  stm_util.debug('peildatum '||p_peildatum);

  stm_util.schrijf_herstartsleutel ('l_bestandsgroep', l_bestandsgroep);
  stm_util.schrijf_herstartsleutel ('l_bestandtype', l_bestandtype);
  stm_util.schrijf_herstartsleutel ('l_procedure', l_procedure);
  stm_util.schrijf_herstartsleutel ('l_herstart_wgr', l_herstart_wgr);


  if l_bestandtype = 'WGR'
  then

    select naam
    into   l_bsd_naam
    from   rle_bestanden
    where  bsd_groep = l_bestandsgroep
    and    bsd_type = 'WGR';

    stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                           , il_regelnummer =>     l_regelnr
                           , il_tekst =>           'Start met bestand ' || l_bsd_naam || ' op ' || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss' ));

    stm_util.schrijf_herstarttelling('l_regelnr',l_regelnr);


    if l_procedure = 'bepaal_parameters_wgr'
    then

      bepaal_parameters (p_bsd_naam =>              l_bsd_naam
                        ,p_peildatum =>             p_peildatum
                        ,p_vorige_peildatum =>      p_vorige_peildatum
                        ,o_vorige_peildatum =>      l_vorige_peildatum
                        ,o_vorige_rundatum =>       l_vorige_rundatum
                        ,o_rundatum =>              l_rundatum);

      l_procedure := 'insert_wgr';
      stm_util.schrijf_herstartsleutel ('l_procedure', l_procedure);
      stm_util.schrijf_herstartsleutel ('l_vorige_peildatum', l_vorige_peildatum);
      stm_util.schrijf_herstartsleutel ('l_vorige_rundatum', l_vorige_rundatum);
      stm_util.schrijf_herstartsleutel ('l_rundatum', l_rundatum);


      stm_util.debug ('l_procedure '||l_procedure);
      stm_util.debug ('l_vorige_peildatum '|| l_vorige_peildatum);
      stm_util.debug ('l_vorige_rundatum '|| l_vorige_rundatum);
      stm_util.debug ('l_rundatum '|| l_rundatum);
      stm_util.debug('l_bestandtype '||l_bestandtype);
      commit;

    end if;

    if l_procedure = 'insert_wgr'
    then

      insert_wgr (p_bsd_naam =>              l_bsd_naam
                 ,p_peildatum =>             p_peildatum
                 ,p_vorige_peildatum =>      l_vorige_peildatum
                 ,p_herstart_wgr =>          l_herstart_wgr
                 );

      l_procedure := 'schrijf_bestand_wgr';
      stm_util.schrijf_herstartsleutel ('l_procedure', l_procedure);
      commit;

    end if;

    if l_procedure = 'schrijf_bestand_wgr'
    then
      stm_util.debug('get_bestandsnaam');
      l_bestandsnaam := get_bestandsnaam (p_bsd_naam =>          l_bsd_naam
                                         ,p_bsd_type =>          l_bestandtype
                                         ,p_vanaf_datum =>       l_vorige_peildatum
                                         ,p_tot_datum =>         p_peildatum
                                         );
    stm_util.debug('opslaan_uitvoer_gegevens');
      schrijf_bestand (l_bestandsgroep, l_bestandtype, l_bestandsnaam);

      opslaan_uitvoer_gegevens (p_runnummer =>           p_script_id
                               ,p_bsd_naam =>            l_bsd_naam
                               ,p_vanaf_datum =>         l_vorige_peildatum
                               ,p_tot_datum =>           p_peildatum
                               ,p_starttijd_uitvoer =>   l_rundatum
                               ,p_bestandsnaam =>        l_bestandsnaam);

      l_bestandtype := 'WNR';
      stm_util.schrijf_herstartsleutel ('l_bestandtype', l_bestandtype);
      l_procedure := 'bepaal_parameters_wnr';
      stm_util.schrijf_herstartsleutel ('l_procedure', l_procedure);
      l_herstart_wgr := 0;
      stm_util.schrijf_herstartsleutel ('l_herstart_wgr', l_herstart_wgr);

      l_vorige_peildatum := null;
      l_vorige_rundatum := null;
      l_rundatum := null;

    commit;

    end if;
  end if;


  if l_bestandtype = 'WNR'
  then

    select naam
    into   l_bsd_naam
    from   rle_bestanden
    where  bsd_groep = l_bestandsgroep
    and    bsd_type = 'WNR';

    stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                           , il_regelnummer =>     l_regelnr
                           , il_tekst =>           'Start met bestand ' || l_bsd_naam || ' op ' || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss' ));

    stm_util.schrijf_herstarttelling('l_regelnr',l_regelnr);


    stm_util.debug('l_bsd_naam'||l_bsd_naam);
    stm_util.debug('l_procedure'||l_procedure);

    if l_procedure = 'bepaal_parameters_wnr'
    then

       bepaal_parameters (p_bsd_naam =>              l_bsd_naam
                         ,p_peildatum =>             p_peildatum
                         ,p_vorige_peildatum =>      p_vorige_peildatum
                         ,o_vorige_peildatum =>      l_vorige_peildatum
                         ,o_vorige_rundatum =>       l_vorige_rundatum
                         ,o_rundatum =>              l_rundatum);

      l_procedure := 'insert_wnr';

      stm_util.schrijf_herstartsleutel ('l_procedure', l_procedure);
      stm_util.schrijf_herstartsleutel ('l_vorige_peildatum', l_vorige_peildatum);
      stm_util.schrijf_herstartsleutel ('l_vorige_rundatum', l_vorige_rundatum);
      stm_util.schrijf_herstartsleutel ('l_rundatum', l_rundatum);

      stm_util.debug ('l_procedure '||l_procedure);
      stm_util.debug ('l_vorige_peildatum'|| l_vorige_peildatum);
      stm_util.debug ('l_vorige_rundatum'|| l_vorige_rundatum);
      stm_util.debug ('l_rundatum'|| l_rundatum);

      commit;

    end if;

    if l_procedure = 'insert_wnr'
    then

      insert_wnr (p_bsd_naam =>              l_bsd_naam
                 ,p_peildatum =>             p_peildatum
                 ,p_vorige_peildatum =>      l_vorige_peildatum
                 ,p_herstart_wgr =>          l_herstart_wgr);

      l_procedure := 'schrijf_bestand_wnr';
      stm_util.schrijf_herstartsleutel ('l_procedure', l_procedure);
      commit;

    end if;

    if l_procedure = 'schrijf_bestand_wnr'
    then

      l_bestandsnaam := get_bestandsnaam (p_bsd_naam =>          l_bsd_naam
                                         ,p_bsd_type =>          l_bestandtype
                                         ,p_vanaf_datum =>       l_vorige_peildatum
                                         ,p_tot_datum =>         p_peildatum
                                         );

      schrijf_bestand (l_bestandsgroep, l_bestandtype, l_bestandsnaam);

      opslaan_uitvoer_gegevens (p_runnummer =>           p_script_id
                               ,p_bsd_naam =>            l_bsd_naam
                               ,p_vanaf_datum =>         l_vorige_peildatum
                               ,p_tot_datum =>           p_peildatum
                               ,p_starttijd_uitvoer =>   l_rundatum
                               ,p_bestandsnaam =>        l_bestandsnaam);
      commit;

    end if;
  end if;


  stm_util.insert_lijsten( il_lijstnummer => l_lijstnummer
                         , il_regelnummer =>     l_regelnr
                         , il_tekst =>  'Einde ' || stm_util.t_programma_naam
                                 || ' op ' || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss')
                         );
--
  l_foutomgeving := '900 - ' || l_proc_naam || ' - Voor verwijder herstart ';
  stm_util.verwijder_herstart;
--
  stm_util.insert_job_statussen (l_volgnummer, 'S', '0');
  stm_util.module_end;
  commit;
--
  l_foutomgeving := '999 - ' || l_proc_naam || ' - geeindigd om : ' || to_char( sysdate, 'DD-MM-YYYY HH24:MI:SS');
exception
when others
  then

       raise_application_error (-20000
                               , stm_get_batch_message (sqlerrm) || chr (10) || dbms_utility.format_error_backtrace);
--    else
--      rollback;
      stm_util.t_foutmelding := sqlerrm;
      stm_util.debug ('Foutmelding  : ' || stm_util.t_foutmelding);
      stm_util.debug ('In programma : ' || stm_util.t_programma_naam);
      stm_util.debug ('In procedure : ' || stm_util.t_procedure);
      --
      stm_util.insert_job_statussen (l_volgnummer, 'S', '1');
      stm_util.insert_job_statussen (l_volgnummer
                                    ,'I'
                                    , stm_util.t_programma_naam || ' procedure : ' || stm_util.t_procedure);

      --
      if stm_util.t_foutmelding is not null
      then
         stm_util.insert_job_statussen (l_volgnummer, 'I', stm_util.t_foutmelding);
      end if;

      --
      if stm_util.t_tekst is not null
      then
         stm_util.insert_job_statussen (l_volgnummer, 'I', stm_util.t_tekst);
      end if;

      --
      commit;

end VERWERK;
END RLE_RLE_LEVERINGEN;
/