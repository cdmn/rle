CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_PERS IS

/**********************************************************************
  Naam:         RLE_PERS
  Beschrijving: Deze package is een wrapper over bestaande functionaliteit
                en bedoeld
                om persoons gegevens in de rijke view RLE_V_PERSONEN_XL te kunnen
                tonen.

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
**********************************************************************/


FUNCTION GET_PERS_OMSCHR
 (P_SOORT IN VARCHAR2
 ,P_CODE IN VARCHAR2
 ,P_IND_PORTAL IN VARCHAR2
 )
 RETURN VARCHAR2
 IS

/**********************************************************************
  Naam:         GET_PERS_OMSCHR
  Beschrijving: Haal de omschrijving op van de diverse codes

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
**********************************************************************/
l_omschrijving varchar2(4000);
l_code         varchar2(4000);
l_ind_ibm      varchar2(1) := 'N';
l_lengte_ibm   number := 100;
begin
  l_code := p_code;

  if     l_code is not null
     and p_ind_portal <> 'N'
     and p_soort = 'VVL'
  then
    -- Indien aanroep is portal en soort is voorvoegsel bepaal omschr met ibmportal
    ibmportal.rfw_get_wrd( 'VVL'
                         , l_code
                         , l_ind_ibm
                         , l_lengte_ibm
                         , l_omschrijving
                         );
  elsif l_code is not null
  then
    -- Bepaal omschrijving
    rfw_get_wrd( p_soort
               , l_code
               , l_ind_ibm
               , l_lengte_ibm
               , l_omschrijving
               );
  end if;

  return l_omschrijving;
END GET_PERS_OMSCHR;
FUNCTION GET_KANAAL
 (P_NUMRELATIE IN NUMBER
 )
 RETURN VARCHAR2
 IS

/**********************************************************************
  Naam:         GET_KANAAL
  Beschrijving: Bepaal kanaalvoorkeur voor een relatie

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
**********************************************************************/
l_kanaalvoorkeur varchar2(4000);
l_datbegin       date;
l_dateinde       date;
begin
  -- bepaal kanaal (Email, etc) voor een relatie
  rle_get_kanaal( p_numrelatie
                , sysdate
                , l_kanaalvoorkeur
                , l_datbegin
                , l_dateinde
                );
  return l_kanaalvoorkeur;
END GET_KANAAL;
FUNCTION GET_ADRES
 (P_NUMRELATIE IN NUMBER
 )
 RETURN VARCHAR2
 IS
/**********************************************************************
  Naam:         GET_ADRES
  Beschrijving: Bepaal het adres voor een relatie per regel.
                Geef het OA en DA adres in één string terug.

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
  24-08-2009  UAO  Toegevoegd kruispuntteam gegevens
**********************************************************************/
  cursor c_vol(
    b_numrelatie   rle_relaties.numrelatie%type
  )
  is
    select volledige_naam1
         , volledige_naam2
         , volledige_naam3
    from   rle_v_personen
    where  numrelatie = b_numrelatie;
  --
  r_vol                c_vol%rowtype;
  --
  cursor c_oed (b_oed_id  in org_organisatie_eenheden.id%type)
  is
  select oed.naam
       , oed.telefoonnummer
  from   org_organisatie_eenheden oed
  where  oed.id = b_oed_id;
  --
  l_postcode           varchar2( 4000 );
  l_huisnr             number;
  l_toevnum            varchar2( 4000 );
  l_woonplaats         varchar2( 4000 );
  l_straat             varchar2( 4000 );
  l_codland            varchar2( 4000 );
  l_locatie            varchar2( 4000 );
  l_ind_woonboot       varchar2( 4000 );
  l_ind_woonwagen      varchar2( 4000 );
  l_landnaam           varchar2( 4000 );
  l_begindatum         date;
  l_einddatum          date;
  l_opdrachtgever      number;
  l_produkt            varchar2( 4000 );
  l_adres_string       varchar2( 4000 );
  l_kruispuntteam_naam varchar2(100);
  l_kruispuntteam_tel  varchar2(100);
  l_branchenr          varchar2(100);
  l_org_eenheid        varchar2(100);
  l_medewerker_id      varchar2(100);
  l_werkbak_id         varchar2(100);
  --
begin

  /*Door het ophalen van de default parameters wordt het DA adres
   altijd bepaald. In de view kan dit op basis van administratie
   alsnog worden uitgesloten.*/
  --Bepaal de default opdrachtgever
  l_opdrachtgever := to_number( stm_algm_get.sysparm( 'PMA'
                                                    , 'OPDR_GEVER'
                                                    , sysdate
                                                    ));
  -- Bepaal het default produkt
  l_produkt := stm_algm_get.sysparm( 'PMA'
                                   , 'PRODUKT'
                                   , sysdate
                                   );
  -- Bepaal het OA adres
  rle_get_adrsvelden(l_opdrachtgever
                    ,l_produkt
                    ,p_numrelatie
                    ,'PR'
                    ,'OA'
                    ,sysdate
                    ,l_postcode
                    ,l_huisnr
                    ,l_toevnum
                    ,l_woonplaats
                    ,l_straat
                    ,l_codland
                    ,l_locatie
                    ,l_ind_woonboot
                    ,l_ind_woonwagen
                    ,l_landnaam
                    ,l_begindatum
                    ,l_einddatum);
   -- Bouw de adresstring voor OA adres
   l_adres_string:= 'Attr01:'||l_postcode||'Attr02:'||l_huisnr||'Attr03:'||l_toevnum||'Attr04:'||l_woonplaats||'Attr05:'||l_locatie||'Attr06:'||l_straat||
                    'Attr07:'||l_codland||'Attr08:'||l_ind_woonboot||'Attr09:'||l_ind_woonwagen||'Attr10:'||l_landnaam;
   l_postcode:=null;
   l_huisnr:=null;
   l_toevnum:=null;
   l_woonplaats:=null;
   l_straat:=null;
   l_codland:=null;
   l_locatie:=null;
   l_ind_woonboot:=null;
   l_ind_woonwagen:=null;
   l_landnaam:=null;
   l_begindatum:=null;
   l_einddatum:=null;
   -- Bepaal het DA adres
   rle_get_adrsvelden(l_opdrachtgever
                     ,l_produkt
                     ,p_numrelatie
                     ,'PR'
                     ,'DA'
                     ,sysdate
                     ,l_postcode
                     ,l_huisnr
                     ,l_toevnum
                     ,l_woonplaats
                     ,l_straat
                     ,l_codland
                     ,l_locatie
                     ,l_ind_woonboot
                     ,l_ind_woonwagen
                     ,l_landnaam
                     ,l_begindatum
                     ,l_einddatum);
   -- Voeg het DA adres aan de adresstring toe
   l_adres_string:= l_adres_string||'Attr11:'||l_postcode||'Attr12:'||l_huisnr||'Attr13:'||l_toevnum||'Attr14:'||l_woonplaats||'Attr15:'||l_locatie||'Attr16:'||l_straat||
                    'Attr17:'||l_codland||'Attr18:'||l_ind_woonboot||'Attr19:'||l_ind_woonwagen||'Attr20:'||l_landnaam;

   -- Opmaken naam
   open c_vol(p_numrelatie);
   fetch c_vol into r_vol;
   close c_vol;
   --
   l_adres_string:= l_adres_string||'Attr21:'||r_vol.volledige_naam1||'Attr22:'||r_vol.volledige_naam2||'Attr23:'||r_vol.volledige_naam3;
   --

   -- Bepaal het CA adres
   rle_get_adrsvelden(l_opdrachtgever
                     ,l_produkt
                     ,p_numrelatie
                     ,'PR'
                     ,'CA'
                     ,sysdate
                     ,l_postcode
                     ,l_huisnr
                     ,l_toevnum
                     ,l_woonplaats
                     ,l_straat
                     ,l_codland
                     ,l_locatie
                     ,l_ind_woonboot
                     ,l_ind_woonwagen
                     ,l_landnaam
                     ,l_begindatum
                     ,l_einddatum);
   -- Voeg het CA adres aan de adresstring toe
   l_adres_string:= l_adres_string||'Attr24:'||l_postcode||'Attr25:'||l_huisnr||'Attr26:'||l_toevnum||'Attr27:'||l_woonplaats||'Attr28:'||l_locatie||'Attr29:'||l_straat||
                    'Attr30:'||l_codland||'Attr31:'||l_ind_woonboot||'Attr32:'||l_ind_woonwagen||'Attr33:'||l_landnaam;

   -- Haal id organisatorische eenheid op
   org_bep_tgn_oe  ('KLT'
                  , null
                  , p_numrelatie
                  , l_branchenr
                  , l_org_eenheid
                  , l_medewerker_id
                  , l_werkbak_id);

   -- Haal kruispuntteam naam en tel op
   if l_org_eenheid is not null
   then
      open c_oed ( l_org_eenheid );
      fetch c_oed into l_kruispuntteam_naam, l_kruispuntteam_tel;
      close c_oed;
   end if;

   --
   l_adres_string:= l_adres_string||'Attr34:'||l_kruispuntteam_naam||'Attr35:'||l_kruispuntteam_tel;
   --
   return l_adres_string;
END GET_ADRES;
FUNCTION GET_GBA
 (P_SOORT IN VARCHAR2
 ,P_NUMRELATIE IN NUMBER
 )
 RETURN VARCHAR2
 IS

/**********************************************************************
  Naam:         GET_GBA
  Beschrijving: Bepaal de GBA status voor een relatie.

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
**********************************************************************/
l_code_gba_status        varchar2(1000);
l_gba_afnemers_indicatie varchar2(1000);
l_waarde                 varchar2(1000);
begin
  rle_gba_get_status( null
                    , p_numrelatie
                    , l_code_gba_status
                    , l_gba_afnemers_indicatie
                    );

  if p_soort = 'STATUS'
  then
    -- Toon GBA Status
    l_waarde := l_code_gba_status;
  elsif p_soort = 'INDICATIE'
  then
    -- Toon afnemers indicatie
    l_waarde := l_gba_afnemers_indicatie;
  end if;

  return l_waarde;
END GET_GBA;
FUNCTION GET_ADM
 (P_NUMRELATIE IN NUMBER
 )
 RETURN VARCHAR2
 IS

/**********************************************************************
  Naam:         GET_ADM
  Beschrijving: Bepaal de adminstratie voor een relatie (indien
                meerdere samenvoegen door spatie gescheiden)

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
**********************************************************************/
  cursor c_adm(
    b_numrelatie   in   number
  )
  is
    select adm.code
    from   rle_relatierollen_in_adm rel
         , rle_administraties adm
    where  adm.code = rel.adm_code
    and    rel.rol_codrol = 'PR'
    and    adm.type = 'P'
    and    rel.rle_numrelatie = b_numrelatie;

  l_pens_adm   varchar2( 4000 );
begin
  for r_adm in c_adm( p_numrelatie ) -- Loop door de bij de relatie behorende administraties
  loop
    l_pens_adm := l_pens_adm || ' ' || r_adm.code;
  end loop;

  return ltrim( l_pens_adm );  -- Retourneer gevonden adminstraties in 1 string
END GET_ADM;
FUNCTION GET_STRING_ATTR
 (P_ATTR IN VARCHAR2
 ,P_STRING IN VARCHAR2
 )
 RETURN VARCHAR2
 IS

/**********************************************************************
  Naam:         GET_STRING_ATTR
  Beschrijving: Haal de attribuutwaarde op uit de string

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
**********************************************************************/
l_next_attr  varchar2(100);
l_from       number;
l_length     number;
l_attr_value varchar2(4000);
begin
  -- Bepaal de parameters voor de substring
  l_next_attr := 'Attr'||to_char(lpad(to_number(substr(p_attr,5,2))+1,2,0));
  l_from      := instr(p_string,p_attr)+7;
  if p_attr = 'Attr35'  -- Laatste attribuut
  then
    l_length:=4000;
  else
    l_length:= instr(p_string,l_next_attr)-(instr(p_string,p_attr)+7);
  end if;
  -- Bepaal de waarde uit de string
  l_attr_value:= substr(p_string,l_from,l_length);

  return l_attr_value;
END GET_STRING_ATTR;
FUNCTION VALIDE_ADMIN
 (P_STRING IN VARCHAR2
 )
 RETURN VARCHAR2
 IS

/**********************************************************************
  Naam:         VALIDE_ADMIN
  Beschrijving: Controleer de administratie context

  Datum       Wie  Wat
--------------------------------------------------------------
  20-03-2009  XKV  Creatie
**********************************************************************/
l_valide_adm varchar2(1);
l_adm        varchar2(100);
l_from_new   number;
l_until_curr number;
l_string     varchar2(4000);
begin
  l_string:=p_string;
  if l_string is not null and
     stm_context.administratie is not null AND
     stm_context.administratie <> 'ALG'
  then
    loop  -- Loop door de administratiestring en bepaal de administraties.
          -- Zodra de administratie matched return 'match' ('J').
      l_until_curr:= instr(l_string,' ')-1;    -- lezen tot en met
      l_from_new:= instr(l_string,' ')+1;      -- lees string opnieuw vanaf
      l_adm:= substr(l_string,1,l_until_curr);

      if l_adm=stm_context.administratie
      then
        l_valide_adm:='J';
        exit;
      elsif l_until_curr > 0
      then
        l_string:=substr(l_string,l_from_new,1000); -- Verwijder de niet matchende administratie
      else
        l_adm:= substr(l_string,1,1000);  -- voor de laatste administratie in de string
        if l_adm=stm_context.administratie
        then
          l_valide_adm:='J';
        else
          l_valide_adm:='N';
        end if;
        exit;
      end if;
    end loop;
  elsif stm_context.administratie = 'ALG' or
        stm_context.administratie is null
  then
    l_valide_adm:='J';
  else
    l_valide_adm:='N';
  end if;
  return l_valide_adm;
END VALIDE_ADMIN;
END RLE_PERS;
/