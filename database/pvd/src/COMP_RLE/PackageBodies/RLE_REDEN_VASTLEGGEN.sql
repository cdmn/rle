CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_REDEN_VASTLEGGEN IS
--------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is vastellegen reden (niet afstemmen met GBA) in rle_synchronisatie_statussen
-- op basis van aangeleverde lijst persoonsnummers
--------------------------------------------------------------
-- Wijzigingshistorie
-- Wanneer     Wie               Wat
--------------------------------------------------------
-- 24-02-2014  AZM               Creatie
-------------------------------------------------------------

-- global t.b.v. logverslag
g_lijstnummer         number := 1;
g_regelnr             number := 0;

g_msg_doorlopen_personen     number := 0;
g_msg_rle_rdn_gewijzigd     number := 0;
g_msg_rle_rdn_ongewijzigd     number := 0;


--

--

--

--

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
PROCEDURE VERWERK_PERSONEN;
PROCEDURE VERWERK_REDEN;


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
begin
    stm_util.insert_lijsten( g_lijstnummer
                           , g_regelnr
                           , p_text
                           );
end lijst;
PROCEDURE VERWERK_PERSONEN
 IS

  -- RLE persoosnummer niet gevonden in RLE
  cursor c_persnr_niet_gevonden
  is
    SELECT persoonsnummer
    FROM   rle_pers_reden_tmp prs
    WHERE  prs.persoonsnummer is not null
    AND NOT EXISTS
           (SELECT 1
            FROM   rle_relatie_externe_codes rec
            WHERE  prs.persoonsnummer = rec.extern_relatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'PRS'
            )
     ;
  --
  l_aantal_aangeleverd   number := 0;
  l_aantal_niet_gevonden number := 0;
  --
begin
  stm_util.t_procedure       := 'VERWERK_PERSONEN';

  -- Tijdelijke tabel leegmaken
  execute immediate 'TRUNCATE TABLE rle_pers_reden_tmp';

  -- Haal data over van external table naar tijdelijke tabel
  INSERT INTO rle_pers_reden_tmp
    ( persoonsnummer, reden
    )
    (SELECT persoonsnummer, trim(reden)
     FROM   rle_pers_reden_ext
    );

  --l_aantal_aangeleverd := SQL%ROWCOUNT;
  SELECT count(*)
    INTO l_aantal_aangeleverd
  FROM   rle_pers_reden_tmp
  WHERE  persoonsnummer is not null;

  lijst ('Aantal redenen aangeboden     			          : ' || to_char(l_aantal_aangeleverd));

  --lijst ( to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss') );

  commit;

  -- Personen waarvan persoonsnummer niet gevonden in RLE
  lijst (' ');
  lijst ('Personen waarvan persoonsnummer niet gevonden in RLE:');
  lijst  ('       '||'persoonsnummer');
  for r_persnr_niet_gevonden
  in  c_persnr_niet_gevonden
  loop
    lijst('             '|| r_persnr_niet_gevonden.persoonsnummer );
    l_aantal_niet_gevonden := l_aantal_niet_gevonden+1;
  end loop;
  lijst (' ');
  lijst ('Aantal redenen niet verwerkt, omdat bijgeleverd persoonsnummer niet gevonden in RLE: ' || to_char(l_aantal_niet_gevonden));

  --lijst ( to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss') );

  commit;

end VERWERK_PERSONEN;
PROCEDURE VERWERK_REDEN
 IS
  -- Cursor PRS PERS_REDEN
  cursor c_prs_reden
  is
    SELECT rec.rle_numrelatie
         , gba.persoonsnummer
		 , nvl(trim(gba.reden), '-') reden
    FROM   rle_pers_reden_tmp gba
    JOIN   rle_relatie_externe_codes rec
    ON      gba.persoonsnummer = rec.extern_relatie
    AND    gba.persoonsnummer is not null
    AND    rec.rol_codrol = 'PR'
    AND    rec.dwe_wrddom_extsys = 'PRS';

  -- Aantallen verwerkte records per reden
  cursor c_redenen_verwerkt
  is
    SELECT trim(reden) reden, count(*) aantal
    FROM    rle_pers_reden_tmp prs
    WHERE prs.persoonsnummer is not null
    AND EXISTS
           (SELECT 1
            FROM   rle_relatie_externe_codes rec
            WHERE  prs.persoonsnummer = rec.extern_relatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'PRS'
            )
    group by trim(reden)
    order by 1;
begin
  stm_util.t_procedure       := 'VERWERK_REDEN';

  lijst ( ' ' );
  --
  for r_prs_reden in c_prs_reden
  loop
    g_msg_doorlopen_personen := g_msg_doorlopen_personen + 1;
    begin
      rle_update_syn_status (p_numrelatie=> r_prs_reden.rle_numrelatie
                            ,p_reden     => r_prs_reden.reden);
      g_msg_rle_rdn_gewijzigd := g_msg_rle_rdn_gewijzigd + 1;
    exception
	when others then
	  g_msg_rle_rdn_ongewijzigd := g_msg_rle_rdn_ongewijzigd + 1;
      lijst ( 'Persoonsnr waarvoor REDEN NIET vastgelegd kon worden: P' || r_prs_reden.persoonsnummer||' - met Relatienr '||r_prs_reden.rle_numrelatie);
	end;

  end loop;

  --lijst ( 'Totaal aantal doorlopen personen uit de ingelezen lijst : ' || g_msg_doorlopen_personen      );
  lijst ( 'Aantal redenen verwerkt totaal                                                      : ' || g_msg_rle_rdn_gewijzigd      );

  -- aantallen verwerkt per reden
  lijst ( '       Aantal verwerkte redenen per soort');
  for r_redenen_verwerkt
  in  c_redenen_verwerkt
  loop
  lijst ( '             '||rpad(nvl(r_redenen_verwerkt.reden, 'leeg gemaakt'),35)||': '||r_redenen_verwerkt.aantal);
  end loop;

  lijst ( 'Aantal redenen niet verwerkt vanwege technische fout (update mislukt)               : ' || g_msg_rle_rdn_ongewijzigd     );
  lijst ( ' ' );

  commit;

end VERWERK_REDEN;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN number
 )
 IS
  -- cursor t.b.v. wegschrijven van verzamelde A-nummers en BSN
  cursor c_csv_data
  is
  select rec.extern_relatie persoonsnummer
      , ( select rec.extern_relatie
          from   rle_relatie_externe_codes rec
          where  rec.rle_numrelatie = syn.rle_numrelatie
          and    rec.rol_codrol = 'PR'
          and    rec.dwe_wrddom_extsys = 'SOFI'
         ) as bsn
      , ( select rec.extern_relatie
          from   rle_relatie_externe_codes rec
          where  rec.rle_numrelatie = syn.rle_numrelatie
          and    rec.rol_codrol = 'PR'
          and    rec.dwe_wrddom_extsys = 'GBA'
         ) as anummer
      ,  syn.reden
      ,  syn.datum_reden
      ,  syn.groep
      ,  syn.datum_groep
  from  rle_synchronisatie_statussen syn
      , rle_relatie_externe_codes rec
  where rec.rle_numrelatie = syn.rle_numrelatie
  and   rec.dwe_wrddom_extsys = 'PRS'
  and   syn.reden is not null;
  --
  r_csv_data             c_csv_data%rowtype;
  l_teller              number := 0;
  l_database            varchar2(255);
  fh_stoplijst          utl_file.file_type;
  l_bestandsnaam        varchar2(255);
  --
  l_volgnummer          stm_job_statussen.volgnummer%type := 0;
  e_abort_batch         exception;
  --
begin
  stm_util.t_procedure       := 'VERWERK';
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_REDEN_VASTLEGGEN';

  -- Schrijven logbestand
  lijst ( rpad ('***** LOGVERSLAG RLEPRC71 RLE_REDEN_VASTLEGGEN  *****', 70)
               || ' STARTDATUM: ' || to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  lijst ( ' ' );
  lijst ( '--------------------------------------------------------------------------------' );
  lijst ( 'Invoerparameters: Geen' );
  lijst ( ' ' );
  lijst ( 'Input bestand: RLE_PERS_REDEN_EXT.csv' );
  lijst ( ' ' );
  lijst ( '--------------------------------------------------------------------------------' );

  -- Verwerk Niet-afgestemde personen
  verwerk_personen;

  -- Aanpassen REDEN
  verwerk_reden;

  -- Aanmaken overzicht verwerkte personen met redenen
  -- Naamgeving van het bestand: RLE_PERS_REDEN_TOTAAL.csv
  -- bestandsnaam formeren   xcw 15-08-2013 aangepast na opmerking produktiebeheer
  select case
           when regexp_substr ( global_name
                             , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;

  l_bestandsnaam := l_database || p_script_naam
                               || '_'     || 'RLE_PERS_REDEN_TOTAAL'
                               || '_'     || to_char(sysdate, 'yyyymmdd')
                               || '_'     || to_char(p_script_id);
  -- openen van de filehandler
  fh_stoplijst := utl_file.fopen ( 'RLE_OUTBOX'
                                  , l_bestandsnaam||'.CSV'
                                  , 'W'
                                  );

  -- schrijf de header records
  utl_file.put_line ( fh_stoplijst
                    , 'persoonsnummer; bsn;a_nummer;reden;datum_reden;groep;syn.datum_groep');

  -- wegschrijven data naar csvbestand
  for r_csv_data in c_csv_data
  loop
      l_teller := l_teller +1;
      utl_file.put_line ( fh_stoplijst
                        , r_csv_data.persoonsnummer       || ';' ||
                          r_csv_data.bsn                  || ';' ||
                          r_csv_data.anummer              || ';' ||
                          r_csv_data.reden                || ';' ||
                          r_csv_data.datum_reden          || ';' ||
                          r_csv_data.groep                || ';' ||
                          r_csv_data.datum_groep
                        );
  end loop;
  --
  lijst ('Aantal naar csv bestand weggeschreven : ' || to_char(l_teller));
  --
  lijst ('Bestand '||l_bestandsnaam||'.CSV'||' aangemaakt in de RLE inbox op de ftp pool!' );
  --
  -- filehandle(s) sluiten
  utl_file.fclose_all;
  --  Voettekst van lijsten
  lijst ( '******* EINDE VERSLAG RLEPRC71 RLE_REDEN_VASTLEGGEN - EINDDATUM: '||to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss')||' *******');
  stm_util.debug ('Einde logverslag');
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  -- opslaan
  commit;

exception
  when others
  then
    rollback;
    stm_util.t_foutmelding := substr(sqlerrm,1,500);
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);
    lijst( substr(sqlerrm,1,500) );

    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(stm_util.t_foutmelding,1,250)
                                  );
    commit;
end verwerk;
END RLE_REDEN_VASTLEGGEN;
/