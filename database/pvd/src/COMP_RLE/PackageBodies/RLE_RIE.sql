CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_RIE IS
/* ******************************************************************
PACKAGE: rle_rie

DOEL:    Package bevat programmatuur voor mutaties op RIE

CHANGES:
22-04-2008 XMK Creatie
05-12-2009 XKV Administratie ook via OVT
****************************************************************** */


/* Opvoeren relatiekoppeling in administratie voor RWG */
PROCEDURE KOP_RWG_CVC
 (P_WGR_WERKGNR IN VARCHAR2
 ,P_ADM_CODE IN VARCHAR2
 ,P_FUNC_CAT IN VARCHAR2
 ,P_DAT_BEGIN IN DATE
 ,P_DAT_EINDE IN DATE
 ,P_ERROR OUT VARCHAR
 )
 IS
cursor c_adm (b_adm_code rle_administraties.code%type)
is
select *
from   rle_administraties adm
where  adm.code = b_adm_code
;
--
cursor c_avg ( b_werkgever avg_arbeidsverhoudingen.werkgever%type
             , b_functiecategorie beroepen.code_functiecategorie%type
             , b_datum_begin avg_werknemer_beroepen.dat_begin%type
             , b_datum_einde avg_werknemer_beroepen.dat_einde%type)
is
select distinct avg.werknemer
from   avg_arbeidsverhoudingen  avg
,	   avg_werknemer_beroepen	brp
,      beroepen				    br2
where  avg.werkgever             = b_werkgever
and    brp.avg_id                = avg.id
and    b_datum_begin             < nvl(brp.dat_einde
                                      ,to_date('01-01-3500','dd-mm-yyyy'))
and    nvl(b_datum_einde
          ,to_date('01-01-3500','dd-mm-yyyy'))
                                 > brp.dat_begin
and    br2.code_beroep           = brp.code_beroep
and	   br2.code_functiecategorie = b_functiecategorie
;
--
l_rle_numrelatie_wgr aos_verzekerden_ao.rle_numrelatie%type;
l_rle_numrelatie_prs aos_verzekerden_ao.rle_numrelatie%type;
l_prs_persoonsnummer rle_relatie_externe_codes.extern_relatie%type;
l_prs_error          rle_relatie_externe_codes.extern_relatie%type;
r_adm                rle_administraties%rowtype;
l_ind_aangemaakt     varchar2(1);
l_errorcode          varchar2(2000);
l_error              varchar2(2000);
l_aantal_fout        number := 0;
l_fout_avg           boolean := false;
e_fout_rie           exception;
c_proc_name constant varchar2(25) := 'RLE_RIE.KOP_RWG_CVC';
--
begin
   --
   -- Stap 1 Werkgever: Ophalen administraties met WSF932 en aanroep RLE980
   --
   get_ext_rle(p_relext      => p_wgr_werkgnr
              ,p_systeemcomp => 'WGR'
              ,p_codrol      => 'WG'
              ,p_datparm     => trunc(sysdate)
              ,p_o_relnum    => l_rle_numrelatie_wgr
              );
   --
   stm_util.debug('rle_numrelatie_wgr: '||l_rle_numrelatie_wgr);
   --
   if l_rle_numrelatie_wgr is null
   then
     stm_dbproc.raise_error('RLE-10383 Rle numrelatie is niet te bepalen '||
                            ' voor wgr_werkgnr: '||p_wgr_werkgnr);
   end if;
   --
   open c_adm(p_adm_code);
   fetch c_adm into r_adm;
   if c_adm%notfound
   then
      close c_adm;
      stm_dbproc.raise_error('RLE-10381 Deze administratie bestaat niet '||
                             ' code: '||p_adm_code);
   end if;
   close c_adm;
   --
   rle_insert_rie(p_adm_code                    => r_adm.code
                 ,p_rle_numrelatie              => l_rle_numrelatie_wgr
                 ,p_rol_codrol                  => 'WG'
                 ,p_ind_aangemaakt              => l_ind_aangemaakt
                 ,p_error                       => l_errorcode
                 ,p_gba_sync_indien_al_aanwezig => 'N'
 		         );
   --
   if l_errorcode is not null
   then
      stm_dbproc.raise_error(l_errorcode);
   end if;
   --
   -- Stap 2 en 3: Aanroep RLE980 voor alle actieve arbeidsverhoudingen van de werkgever
   -- en hun verwanten
   --
   for r_avg in c_avg (p_wgr_werkgnr
                     , p_func_cat
                     , p_dat_begin
                      ,p_dat_einde)
    loop
        l_fout_avg := false;
        savepoint s_avg;
       --
       l_rle_numrelatie_prs := null;
       l_prs_persoonsnummer := r_avg.werknemer;
       --
       get_ext_rle(p_relext      => r_avg.werknemer
                  ,p_systeemcomp => 'PRS'
                  ,p_codrol      => 'PR'
                  ,p_datparm     => trunc(sysdate)
                  ,p_o_relnum    => l_rle_numrelatie_prs
                  );
       --
       stm_util.debug('rle_numrelatie_prs: '||l_rle_numrelatie_prs);
       --
       if l_rle_numrelatie_prs is null
       then
         l_prs_error := l_prs_persoonsnummer;
         l_error := 'RLE-10383 Rle numrelatie is niet te bepalen';
         l_fout_avg := true;
       --
       else
          kop_wnr_avg( p_rle_numrelatie              => l_rle_numrelatie_prs
                     , p_adm_code                    => p_adm_code
                     , p_adm_type                    => r_adm.type
                     , p_error                       => l_errorcode
                     , p_gba_sync_indien_al_aanwezig => 'N'
                     );
          --
          if l_errorcode is not null
          then
             l_prs_error := l_prs_persoonsnummer;
             l_error := l_errorcode;
             l_fout_avg := true;
          end if;
       --
       end if;
       --
       if l_fout_avg
       then
          l_aantal_fout := l_aantal_fout + 1;
          rollback to s_avg;
       end if;
    --
    end loop;
    --
    if l_error is not null
    then
       raise e_fout_rie;
    end if;
    --
exception
when e_fout_rie
then
   p_error := c_proc_name||' werkgever: '||p_wgr_werkgnr||' heeft '||l_aantal_fout||' fouten. '
                         ||' Laatste fout bij werknemer: '||l_prs_error||' fout: '||l_error;
when others
then
   p_error := c_proc_name||' werkgever: '||p_wgr_werkgnr
                         ||' werknemer: '||l_prs_persoonsnummer||' fout: '||sqlerrm;
end;
/* Opvoeren relatiekoppeling in administratie voor WGR */
PROCEDURE KOP_WGR_WZD
 (P_WGR_WERKGNR IN VARCHAR2
 ,P_KODEGVW IN VARCHAR2
 ,P_KODESGW IN VARCHAR2
 ,P_DAT_BEGIN IN DATE
 ,P_DAT_EINDE IN DATE
 ,P_ERROR OUT VARCHAR
 )
 IS
-- Eerste deel union haalt alle AVG's van werknemers op die geen DGA zijn (geweest).
-- Zij mogen worden gekoppeld op basis van datum begin en einde van AVG.  Tweede
-- deel haalt alle beroepen op van werknemers die wel DGA zijn (geweest) behalve die
-- perioden waar ze DGA zijn. Deze laatste mogen niet worden gekoppeld.
--
cursor c_avg ( b_werkgever avg_arbeidsverhoudingen.werkgever%type
              ,b_dat_begin avg_arbeidsverhoudingen.dat_begin%type
              ,b_dat_einde avg_arbeidsverhoudingen.dat_einde%type)
is
select avg.werknemer
,      avg.dat_begin
,      avg.dat_einde
from   avg_arbeidsverhoudingen  avg
where  avg.werkgever       = b_werkgever
and    b_dat_begin         < nvl(avg.dat_einde
                            ,to_date('01-01-3500','dd-mm-yyyy'))
and    nvl(b_dat_einde
          ,to_date('01-01-3500','dd-mm-yyyy'))
                           > avg.dat_begin
and not exists (select 'x'
                from  avg_werknemer_beroepen wbp_sub
                where wbp_sub.avg_id      = avg.id
                and   wbp_sub.code_beroep in ('1000', '1001', '0007', '007', '0070', '1112')
                )
union all
select avg.werknemer
,      wbp.dat_begin
,      wbp.dat_einde
from   avg_arbeidsverhoudingen  avg
,	   avg_werknemer_beroepen	wbp
where  avg.werkgever       = b_werkgever
and    wbp.avg_id          = avg.id
and    wbp.code_beroep not in ('1000', '1001', '0007', '007', '0070', '1112')
and    b_dat_begin         < nvl(wbp.dat_einde
                            ,to_date('01-01-3500','dd-mm-yyyy'))
and    nvl(b_dat_einde
          ,to_date('01-01-3500','dd-mm-yyyy'))
                           > wbp.dat_begin
and    exists (select 'x'
               from  avg_werknemer_beroepen wbp_sub
               where wbp_sub.avg_id      = avg.id
               and   wbp_sub.code_beroep in ('1000', '1001', '0007', '007', '0070', '1112')
              )
order by 1
,        2
;
l_rle_numrelatie_wgr aos_verzekerden_ao.rle_numrelatie%type;
l_rle_numrelatie_prs aos_verzekerden_ao.rle_numrelatie%type;
l_prs_persoonsnummer rle_relatie_externe_codes.extern_relatie%type;
l_prs_error          rle_relatie_externe_codes.extern_relatie%type;
l_adm_tab            wsf_adm.tab_adm;
l_ind_aangemaakt     varchar2(1);
l_errorcode          varchar2(2000);
l_error              varchar2(2000);
l_aantal_fout        number := 0;
l_fout_avg           boolean := false;
e_fout_rie           exception;
c_proc_name constant varchar2(25) := 'RLE_RIE.KOP_WGR_WZD';
--
begin
   --
   -- Stap 1 Werkgever: Ophalen administraties met WSF931 en aanroep RLE980
   --
   get_ext_rle(p_relext      => p_wgr_werkgnr
              ,p_systeemcomp => 'WGR'
              ,p_codrol      => 'WG'
              ,p_datparm     => trunc(sysdate)
              ,p_o_relnum    => l_rle_numrelatie_wgr
              );
   --
   stm_util.debug('rle_numrelatie_wgr: '||l_rle_numrelatie_wgr);
   --
   if l_rle_numrelatie_wgr is null
   then
      stm_dbproc.raise_error('RLE-10383 Rle numrelatie is niet te bepalen '||
                             ' voor wgr_werkgnr: '||p_wgr_werkgnr);
   end if;
   -- Administratie voor werkgever bepalen
   wsf_adm.get_adm_wsf(p_kodegvw     => p_kodegvw
                      ,p_kodesgw     => p_kodesgw
                      ,p_datum_begin => p_dat_begin
                      ,p_datum_einde => p_dat_einde
                      ,p_adm_table   => l_adm_tab);
   --
   for i in 0 .. l_adm_tab.count-1
   loop
      rle_insert_rie(p_adm_code                    => l_adm_tab(i).code
                    ,p_rle_numrelatie              => l_rle_numrelatie_wgr
                    ,p_rol_codrol                  => 'WG'
                    ,p_ind_aangemaakt              => l_ind_aangemaakt
                    ,p_error                       => l_errorcode
                    ,p_gba_sync_indien_al_aanwezig => 'N'
 		            );
      --
      if l_errorcode is not null
      then
         stm_dbproc.raise_error(l_errorcode);
      end if;
    --
    end loop;
   --
   -- Stap 2 en 3: Aanroep RLE980 voor alle actieve arbeidsverhoudingen van de werkgever
   -- en hun verwanten
   --
   for r_avg in c_avg (p_wgr_werkgnr
                      ,p_dat_begin
                      ,p_dat_einde)
   loop
      savepoint s_avg;
      l_fout_avg:= false;
      --
      l_prs_persoonsnummer := r_avg.werknemer;
      -- Administraties voor arbeidsverhouding bepalen
      l_adm_tab.delete;
      wsf_adm.get_adm_wsf(p_kodegvw     => p_kodegvw
                         ,p_kodesgw     => p_kodesgw
                         ,p_datum_begin => r_avg.dat_begin
                         ,p_datum_einde => r_avg.dat_einde
                         ,p_adm_table   => l_adm_tab);
      --
      l_rle_numrelatie_prs := null;
      --
      get_ext_rle(p_relext      => r_avg.werknemer
                 ,p_systeemcomp => 'PRS'
                 ,p_codrol      => 'PR'
                 ,p_datparm     => trunc(sysdate)
                 ,p_o_relnum    => l_rle_numrelatie_prs
                 );
      --
      stm_util.debug('rle_numrelatie_prs: '||l_rle_numrelatie_prs);
      --
      if l_rle_numrelatie_prs is null
      then
         l_prs_error := l_prs_persoonsnummer;
         l_error := 'RLE-10383 Rle numrelatie is niet te bepalen';
         l_fout_avg:= true;
      else
      --
         for i in 0 .. l_adm_tab.count-1
         loop
            kop_wnr_avg( p_rle_numrelatie              => l_rle_numrelatie_prs
                       , p_adm_code                    => l_adm_tab(i).code
                       , p_adm_type                    => l_adm_tab(i).type
                       , p_error                       => l_errorcode
                       , p_gba_sync_indien_al_aanwezig => 'N'
                       );
            --
            if l_errorcode is not null
            then
               l_prs_error := l_prs_persoonsnummer;
               l_error := l_errorcode;
               l_fout_avg:= true;
            end if;
          --
         end loop;
      --
      end if;
      --
      if l_fout_avg
      then
         l_aantal_fout := l_aantal_fout + 1;
         rollback to s_avg;
      end if;
   --
   end loop;
   --
   if l_error is not null
   then
      raise e_fout_rie;
   end if;
   --
exception
when e_fout_rie
then
   p_error := c_proc_name||' werkgever: '||p_wgr_werkgnr||' heeft '||l_aantal_fout||' fouten. '
                         ||' Laatste fout bij werknemer: '||l_prs_error||' fout: '||l_error;
when others
then
   p_error := c_proc_name||' werkgever: '||p_wgr_werkgnr
                         ||' werknemer: '||l_prs_persoonsnummer||' fout: '||sqlerrm;
end;
/* Op basis van werkgve en admin. worden wrknermers gekoppeld aan admin */
PROCEDURE KOP_WNR_AVG
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ADM_CODE IN VARCHAR2
 ,P_ADM_TYPE IN VARCHAR2
 ,P_ERROR OUT VARCHAR2
 ,P_GBA_SYNC_INDIEN_AL_AANWEZIG IN VARCHAR2 := 'J'
 )
 IS
cursor c_rkn (b_rle_numrelatie rle_relatie_koppelingen.rle_numrelatie%type)
is
select rkn.rle_numrelatie_voor
from   rle_relatie_koppelingen rkn
where  rkn.rle_numrelatie = b_rle_numrelatie
and    rkn.rkg_id        in (1,3,4)
;
--
l_ind_aangemaakt     varchar2(1);
l_errorcode          varchar2(2000);
l_foutmelding        varchar2(2000);
e_fout_rie           exception;
c_proc_name constant varchar2(25) := 'RLE_RIE.KOP_WNR_AVG';
--
begin
   --
   -- Stap 2 Werknemer: Aanroep RLE980 voor alle actieve arbeidsverhoudingen
   --
   rle_insert_rie(p_adm_code                    => p_adm_code
                 ,p_rle_numrelatie              => p_rle_numrelatie
                 ,p_rol_codrol                  => 'PR'
                 ,p_ind_aangemaakt              => l_ind_aangemaakt
                 ,p_error                       => l_errorcode
                 ,p_gba_sync_indien_al_aanwezig => p_gba_sync_indien_al_aanwezig
                 );
   --
   if l_errorcode is not null
   then
      raise e_fout_rie;
   end if;
   --
   -- Stap 3 begunstigde: Aanroep RLE980 voor alle verwanten
   --
   if l_ind_aangemaakt = 'J'
   and p_adm_type = 'P'
   then
      for r_rkn in c_rkn (p_rle_numrelatie)
      loop
         stm_util.debug('rle_numrelatie_begunstigde: '||r_rkn.rle_numrelatie_voor);
         --
         rle_insert_rie(p_adm_code                    => p_adm_code
                       ,p_rle_numrelatie              => r_rkn.rle_numrelatie_voor
                       ,p_rol_codrol                  => 'PR'
                       ,p_ind_aangemaakt              => l_ind_aangemaakt
                       ,p_error                       => l_errorcode
                       ,p_gba_sync_indien_al_aanwezig => p_gba_sync_indien_al_aanwezig
                       );
         --
         if l_errorcode is not null
         then
            raise e_fout_rie;
         end if;
         --
      end loop;
   end if;
exception
when e_fout_rie
then
   p_error := c_proc_name||' rle_numrelatie : '||p_rle_numrelatie||' fout: '||l_errorcode;
when others
then
   p_error := c_proc_name||' rle_numrelatie : '||p_rle_numrelatie||' fout: '||sqlerrm;
end;
/* Opvoeren relatiekoppeling in administratie voor AVG */
PROCEDURE KOP_AVG_AVG
 (P_AVG_ID IN NUMBER
 ,P_DAT_BEGIN IN DATE
 ,P_DAT_EINDE IN DATE
 ,P_CODE_BEROEP IN VARCHAR2
 ,P_ERROR OUT VARCHAR
 ,P_GBA_SYNC_INDIEN_AL_AANWEZIG IN VARCHAR2 := 'J'
 )
 IS
cursor c_avg(
    b_avg_id   avg_arbeidsverhoudingen.id%type
  )
  is
    select avg.werkgevernummer
         , avg.persoonsnummer
         , avg.rle_numrelatie_wg
    from   avg_v_arbeidsverhoudingen avg
    where  avg.avg_id = b_avg_id;

  cursor c_vwzd(
    b_nr_werkgever   wgr_v_werkzaamheden.nr_werkgever%type
  , b_dat_begin      wgr_v_werkzaamheden.dat_begin%type
  , b_dat_einde      wgr_v_werkzaamheden.dat_einde%type
  )
  is
    select vwzd.code_groep_werkzhd
         , vwzd.code_subgroep_werkzhd
    from   wgr_v_werkzaamheden vwzd
    where  vwzd.nr_werkgever = b_nr_werkgever
    and    b_dat_begin < nvl( vwzd.dat_einde
                            , to_date( '01-01-3500'
                                     , 'dd-mm-yyyy'
                                     )
                            )
    and    nvl( b_dat_einde
              , to_date( '01-01-3500'
                       , 'dd-mm-yyyy'
                       )
              ) > vwzd.dat_begin;

  cursor c_vcvc(
    b_wgr_werkgnr   cvc_v.wgr_werkgnr%type
  , b_dataanv       cvc_v.dataanv%type
  , b_dateind       cvc_v.dateind%type
  )
  is
    select vcvc.vzp_nummer
         , vcvc.code_functiecategorie
    from   cvc_v vcvc
    where  vcvc.wgr_werkgnr = b_wgr_werkgnr
    and    b_dataanv < nvl( vcvc.dateind
                          , to_date( '01-01-3500'
                                   , 'dd-mm-yyyy'
                                   )
                          )
    and    nvl( b_dateind
              , to_date( '01-01-3500'
                       , 'dd-mm-yyyy'
                       )
              ) > vcvc.dataanv;

  cursor c_ovt(
    b_numrelatie   ovt_wgr_overeenkomsten.rle_numrelatie%type
  , b_dataanv      ovt_wgr_overeenkomsten.datum_begin%type
  )
  is
    select wok.id
         , wok.rgg_code
         , rgl.adm_code
    from   ovt_wgr_overeenkomsten wok
         , pdt_regelingen rgl
    where  wok.rgg_code = rgl.code
    and    wok.soort_aansluiting = 'VR'
    and    wok.rle_numrelatie = b_numrelatie
    and    b_dataanv between wok.datum_begin and nvl( wok.datum_einde
                                                    , b_dataanv
                                                    );

  cursor c_wnb(
    b_avg_id    avg_arbeidsverhoudingen.id%type
  , b_dataanv   avg_werknemer_beroepen.dat_begin%type
  )
  is
    select wnb.code_beroep
    from   avg_werknemer_beroepen wnb
    where  wnb.avg_id = b_avg_id
    and    b_dataanv between wnb.dat_begin and nvl( wnb.dat_einde
                                                  , b_dataanv
                                                  );

  cursor c_rfc(
    b_rgg_code                pdt_reg_functiecategorien.rgg_code%type
  , b_code_functiecategorie   pdt_reg_functiecategorien.code_functiecategorie%type
  )
  is
    select rfc.dekkingswijze
    from   pdt_reg_functiecategorien rfc
    where  rfc.rgg_code = b_rgg_code
    and    rfc.code_functiecategorie = b_code_functiecategorie;

  cursor c_ind(
    b_wot_id   ovt_wgr_afs_deelnamebasis.wot_id%type
  )
  is
    select waf.indicatie_gedekt
    from   ovt_wgr_afs_deelnamebasis wad
         , ovt_wgr_afspr_functcat waf
    where  waf.wad_id = wad.id
    and    sysdate between wad.datum_begin and nvl( wad.datum_einde
                                                  , sysdate
                                                  )
    and    wad.wot_id = b_wot_id;

  l_werkgevernummer         avg_v_arbeidsverhoudingen.werkgevernummer%type;
  l_persoonsnummer          avg_v_arbeidsverhoudingen.persoonsnummer%type;
  l_avg_gevonden            boolean                                          := false;
  l_rle_numrelatie          aos_verzekerden_ao.rle_numrelatie%type;
  l_rle_numrelatie_wg       wgr_werkgevers.nr_relatie%type;
  l_adm_tab                 wsf_adm.tab_adm;
  l_kodegvw                 wgr_v_werkzaamheden.code_groep_werkzhd%type;
  l_kodesgw                 wgr_v_werkzaamheden.code_subgroep_werkzhd%type;
  l_code_functiecategorie   cvc_v.code_functiecategorie%type;
  l_errorcode               varchar2( 2000 );
  r_adm                     rle_administraties%rowtype;
  e_fout_rie                exception;
  c_proc_name      constant varchar2( 25 )                                   := 'RLE_RIE.KOP_AVG_AVG';
  r_wnb                     c_wnb%rowtype;
  r_rfc                     c_rfc%rowtype;
  r_ind                     c_ind%rowtype;
--
  l_rle_numrelatie_wgr      aos_verzekerden_ao.rle_numrelatie%type;
  l_adm                     varchar2( 1000 );
  l_bedrijfssector          varchar2( 1000 );
--
begin
  --
  open c_avg( p_avg_id );

  fetch c_avg
  into  l_werkgevernummer
      , l_persoonsnummer
      , l_rle_numrelatie_wg;

  l_avg_gevonden := c_avg%found;

  close c_avg;

  -- Bij klappen advanced queue kan de arbeidverhouding inmiddels verwijderd zijn als de
  -- queue weer wordt opgestart. In dat geval wordt de mutatie niet verwerkt in RLE.
  if l_avg_gevonden
  then
    get_ext_rle( p_relext =>          l_persoonsnummer
               , p_systeemcomp =>     'PRS'
               , p_codrol =>          'PR'
               , p_datparm =>         trunc( sysdate )
               , p_o_relnum =>        l_rle_numrelatie
               );
    --
    stm_util.debug( 'rle_numrelatie: ' || l_rle_numrelatie );

    --
    if l_rle_numrelatie is null
    then
      stm_dbproc.raise_error( 'RLE-10383 Rle numrelatie is niet te bepalen ' || ' voor persoon ' || l_persoonsnummer );
    end if;

    --
    -- Stap 1.1 Bepalen administratie op basis van werkzaamheden
    --
    if    p_code_beroep is null
       or p_code_beroep not in( '1000', '1001', '0007', '007', '0070', '1112' )
    then
      for r_vwzd in c_vwzd( l_werkgevernummer
                          , p_dat_begin
                          , p_dat_einde
                          )
      loop
        l_adm_tab.delete;
        --
        wsf_adm.get_adm_wsf( p_kodegvw =>         r_vwzd.code_groep_werkzhd
                           , p_kodesgw =>         r_vwzd.code_subgroep_werkzhd
                           , p_datum_begin =>     p_dat_begin
                           , p_datum_einde =>     p_dat_einde
                           , p_adm_table =>       l_adm_tab
                           );

        --
        -- tijdelijke oplossing voor bevinding NOS GAT_FAT_29
        --
        if l_adm_tab.count = 0
        then
          --
          get_ext_rle( p_relext =>          l_werkgevernummer
                     , p_systeemcomp =>     'WGR'
                     , p_codrol =>          'WG'
                     , p_datparm =>         trunc( sysdate )
                     , p_o_relnum =>        l_rle_numrelatie_wgr
                     );
          stm_util.debug( 'rle_numrelatie_wgr: ' || l_rle_numrelatie_wgr );

          if l_rle_numrelatie_wgr is null
          then
            stm_dbproc.raise_error( 'RLE-10383 Rle numrelatie is niet te bepalen ' || ' voor wgr_werkgnr: ' || l_werkgevernummer );
          end if;

          l_bedrijfssector := wsf_sec.get_sec( 'WGR'
                                             , trunc( sysdate )
                                             , null
                                             , l_werkgevernummer
                                             );
          --
          rle_pck_lijsten.prc_init_0985( p_numrelatie =>     l_rle_numrelatie_wgr
                                       , p_rol_codrol =>     'WG'
                                       );

          loop
            --
            rle_pck_lijsten.prc_get_0985( p_adm_code =>     l_adm );
            --
            exit when l_adm is null;

            --
            if    (     l_bedrijfssector = 'KV'
                    and l_adm = 'BPFK' )
               or (     l_bedrijfssector = 'ME'
                    and l_adm = 'PME' )
            then
              kop_wnr_avg( p_rle_numrelatie =>     l_rle_numrelatie
                         , p_adm_code =>           l_adm
                         , p_adm_type =>           'P'
                         , p_error =>              l_errorcode
                         );
            end if;
          --
          end loop;
        --
        end if;

        -- einde tijdelijke oplossing
        --
        for i in 0 .. l_adm_tab.count - 1
        loop
          l_errorcode := null;
          --
          -- Stap 2 en 3: Aanroep RLE980 voor de werknemer en verwanten
          --
          kop_wnr_avg( p_rle_numrelatie =>                  l_rle_numrelatie
                     , p_adm_code =>                        l_adm_tab( i ).code
                     , p_adm_type =>                        l_adm_tab( i ).type
                     , p_error =>                           l_errorcode
                     , p_gba_sync_indien_al_aanwezig =>     p_gba_sync_indien_al_aanwezig
                     );

          --
          if l_errorcode is not null
          then
            raise e_fout_rie;
          end if;
        --
        end loop;
      --
      end loop;
    --
    end if;

    --
    -- Stap 1.2 Bepalen administratie op basis van overeenkomst
    --
    for r_ovt in c_ovt( l_rle_numrelatie_wg
                      , p_dat_begin
                      )
    loop
      l_errorcode := null;

      if p_code_beroep is null
      then
        -- Bepaal avg beroepcode
        open c_wnb( p_avg_id
                  , p_dat_begin
                  );

        fetch c_wnb
        into  r_wnb;

        close c_wnb;
      end if;

      -- Bepaal bedrijfssector
      l_bedrijfssector := wsf_sec.get_sec( 'AVG'
                                         , trunc( sysdate )
                                         , p_avg_id
                                         );

      if l_bedrijfssector <> 'KV'
      then
        -- Bepaal functiecategorie
        reg_get_funccat( nvl( p_code_beroep
                            , r_wnb.code_beroep
                            )
                       , l_code_functiecategorie
                       );

        -- Bepaal dekkingswijze
        open c_rfc( r_ovt.rgg_code
                  , l_code_functiecategorie
                  );

        fetch c_rfc
        into  r_rfc;

        close c_rfc;

        if r_rfc.dekkingswijze <> 'S'   -- Standaard
        then
          -- Bepaal indicatie gedekt
          open c_ind( r_ovt.id );

          fetch c_ind
          into  r_ind;

          close c_ind;
        end if;
      end if;

      --
      -- Alleen koppelen wanneer functiecategorie gedekt door de overeenkomst of
      -- in geval van Koopvaardij.
      --
      if    l_bedrijfssector = 'KV'
         or r_rfc.dekkingswijze = 'S'   -- Standaard
         or (     r_rfc.dekkingswijze = 'D'
              and   -- Dispensatie
                  nvl( r_ind.indicatie_gedekt
                     , 'J'
                     ) = 'J' )
         or (     r_rfc.dekkingswijze = 'O'
              and   -- Optioneel
                  nvl( r_ind.indicatie_gedekt
                     , 'N'
                     ) = 'J' )
      then
        --
        -- Stap 2 en 3: Aanroep RLE980 voor de werknemer en verwanten
        --
        kop_wnr_avg( p_rle_numrelatie =>                  l_rle_numrelatie
                   , p_adm_code =>                        r_ovt.adm_code
                   , p_adm_type =>                        'P'
                   , p_error =>                           l_errorcode
                   , p_gba_sync_indien_al_aanwezig =>     p_gba_sync_indien_al_aanwezig
                   );

        --
        if l_errorcode is not null
        then
          raise e_fout_rie;
        end if;
      --
      end if;
    --
    end loop;

    --
    -- Stap 1.3 Bepalen administratie op basis van contracten
    --
    for r_vcvc in c_vcvc( l_werkgevernummer
                        , p_dat_begin
                        , p_dat_einde
                        )
    loop
      l_errorcode := null;
      --
      -- Alleen koppelen als functiecat gelijk is aan die van arbeidsverhouding
      --
      reg_get_funccat( p_code_beroep
                     , l_code_functiecategorie
                     );

      --
      if r_vcvc.code_functiecategorie = l_code_functiecategorie
      then
        wsf_adm.get_adm_vzp( p_vzp_nummer =>     r_vcvc.vzp_nummer
                           , p_adm_row =>        r_adm
                           );

        --
        -- Administratie alleen toevoegen als deze nog niet bestaat.
        --
        if r_adm.code is not null
        then
          --
          -- Stap 2 en 3: Aanroep RLE980 voor de werknemer en verwanten
          --
          kop_wnr_avg( p_rle_numrelatie =>                  l_rle_numrelatie
                     , p_adm_code =>                        r_adm.code
                     , p_adm_type =>                        r_adm.type
                     , p_error =>                           l_errorcode
                     , p_gba_sync_indien_al_aanwezig =>     p_gba_sync_indien_al_aanwezig
                     );

          --
          if l_errorcode is not null
          then
            raise e_fout_rie;
          end if;
        --
        end if;
      --
      end if;
    --
    end loop;
  --
  end if;
--
exception
  when e_fout_rie
  then
    p_error := c_proc_name || ' arbeidsverhouding : ' || p_avg_id || ' persoon : ' || l_persoonsnummer || ' werkgever: ' || l_werkgevernummer || ' fout: ' || l_errorcode;
  when others
  then
    p_error := c_proc_name || ' arbeidsverhouding : ' || p_avg_id || ' persoon : ' || l_persoonsnummer || ' werkgever: ' || l_werkgevernummer || ' fout: ' || sqlerrm;
end;
/* Het koppelen van verwant deelnemer op basis administratie deelnemer */
PROCEDURE KOP_RKN_RIE
 (P_RLE_NUMRELATIE_DLNMR IN NUMBER
 ,P_RLE_NUMRELATIE_VRWNT IN NUMBER
 ,P_ERROR OUT VARCHAR
 )
 IS
cursor c_rie(b_rle_numrelatie rle_relatierollen_in_adm.rle_numrelatie%type)
is
select adm.code
from   rle_relatierollen_in_adm rie
,      rle_administraties adm
where  adm.code           = rie.adm_code
and    adm.type           = 'P'
and    rie.rle_numrelatie = b_rle_numrelatie
;
l_ind_aangemaakt     varchar2(1);
l_errorcode          varchar2(2000);
e_fout_rie           exception;
c_proc_name constant varchar2(25) := 'RLE_RIE.KOP_RKN_RIE';
--
begin
   for r_rie in c_rie (p_rle_numrelatie_dlnmr)
   loop
      --
      rle_insert_rie(p_adm_code                    => r_rie.code
                    ,p_rle_numrelatie              => p_rle_numrelatie_vrwnt
                    ,p_rol_codrol                  => 'PR'
                    ,p_ind_aangemaakt              => l_ind_aangemaakt
                    ,p_error                       => l_errorcode
                    ,p_gba_sync_indien_al_aanwezig => 'N'
                    );
      --
      if l_errorcode is not null
      then
         raise e_fout_rie;
      end if;
      --
   end loop;
exception
when e_fout_rie
then
   p_error := c_proc_name||' deelnemer : '||p_rle_numrelatie_dlnmr
                         ||' verwant : '||p_rle_numrelatie_vrwnt||' fout: '||l_errorcode;
when others
then
   p_error := c_proc_name||' deelnemer : '||p_rle_numrelatie_dlnmr
                         ||' verwant : '||p_rle_numrelatie_vrwnt||' fout: '||sqlerrm;
end;
/* Koppelen administratie n.a.v. waardeoverdracht */
PROCEDURE KOP_WOD
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ADM_CODE IN VARCHAR2
 ,P_ROL_CODROL IN VARCHAR2
 ,P_ERROR OUT VARCHAR2
 )
 IS
cursor c_adm(b_adm_code rle_administraties.code%type)
is
select adm.type
from rle_administraties adm
where adm.code = b_adm_code
;
l_adm_type rle_administraties.type%type;
l_ind_aangemaakt varchar2(1);
l_error varchar2(2000);
e_fout_rie exception;
c_proc_name constant varchar2(25) := 'RLE_RIE.KOP_WOD';
--
begin
  open c_adm(p_adm_code);
  fetch c_adm into l_adm_type;
  close c_adm;
  -- Als er al een arbeidsverhouding met pensioenproducten bestaat
  -- dan mag een pensioenadministratie niet opnieuw worden aangemaakt.
  if  rle_tel_pensioen(p_rle_numrelatie => p_rle_numrelatie) <> 0
  and l_adm_type = 'P'
  and rle_chk_rie_exists(p_rle_numrelatie => p_rle_numrelatie
                        ,p_rol_codrol     => p_rol_codrol
                        ,p_adm_code       => p_adm_code) = 'J'
  then
     null;
  else
     --
     -- We maken gebruik van dezelfde procedure als registreren
     -- administratie bij een arbeidsverhouding. Functionaliteit=hetzelfde.
     --
     rle_rie.kop_wnr_avg( p_rle_numrelatie              => p_rle_numrelatie
                        , p_adm_code                    => p_adm_code
                        , p_adm_type                    => l_adm_type
                        , p_error                       => l_error
                        , p_gba_sync_indien_al_aanwezig => 'N'
                         );
     --
     if l_error is not null
     then
       raise e_fout_rie;
     end if;
    --
  end if;
  --
  exception
  when e_fout_rie
  then
     p_error := c_proc_name||' relatie: '||p_rle_numrelatie||' fout: '||l_error;
  when others
  then
     p_error := c_proc_name||' relatie: '||p_rle_numrelatie||' fout: '||sqlerrm;
end;
/* Het aanvullen van adm_codes van bron persoon naar doel persoon */
PROCEDURE AANVULLEN_ADM_CODES_PR
 (P_NUMRELATIE_BRON IN PLS_INTEGER
 ,P_NUMRELATIE_DOEL IN PLS_INTEGER
 )
 IS
    /*
        Doel:
        Het aanvullen van de administratie codes van een persoon
        met de administratie codes van een ander persoon.
        Aanleiding:
        Bij het ontdubbelen van personen is het nodig de ontbrekende
        administratie codes toe te voegen.
        Parameters:
        p_numrelatie_bron; het relatienummer van de persoon waar de
            administratie codes vandaan komen.
        p_numrelatie_doel: het relatinummer van de persoon waarbij de
            administratie codes aangevuld worden.

        wie             wanneer         wat
        --------------- --------------- ---------------
        Karel Vlieg     04-12-2012      creatie
    */

    l_rol_pr constant varchar2(2) := 'PR';
begin
    insert into rle_relatierollen_in_adm
    (adm_code
    ,rle_numrelatie
    ,rol_codrol)
    (
    select rie.adm_code
    ,      p_numrelatie_doel
    ,      l_rol_pr
    from   rle_relatierollen_in_adm rie
    where  rie.rle_numrelatie = p_numrelatie_bron
    and    rie.rol_codrol     = l_rol_pr
    minus
    select rie.adm_code
         , p_numrelatie_doel
         , l_rol_pr
    from   rle_relatierollen_in_adm rie
    where  rie.rle_numrelatie = p_numrelatie_doel
    and    rie.rol_codrol     = l_rol_pr
    );

end;

END RLE_RIE;
/