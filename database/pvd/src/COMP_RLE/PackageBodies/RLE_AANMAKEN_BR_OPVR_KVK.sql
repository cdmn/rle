CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_AANMAKEN_BR_OPVR_KVK IS

CN_MODULE CONSTANT VARCHAR2(30) := 'RLEPRC57';
CN_CODE_ROL_RELATIE CONSTANT VARCHAR2(5) := 'WG';
CN_GROEPNUMMER CONSTANT NUMBER(5) := 1;
CN_SYSTEEM CONSTANT VARCHAR2(20) := 'RLE';
CN_PROGRAMMA CONSTANT VARCHAR2(30) := 'RLE_AANMAKEN_BR_OPVR_KVK';
CN_HOOFDDOCUMENT CONSTANT VARCHAR2(20) := 'WSF001';

E_ONGELDIG_DOCUMENTSOORT EXCEPTION;
E_ONGELDIG_ADM_CODE EXCEPTION;
E_FOUTIEVE_GEGEVENS_SET EXCEPTION;
E_ONBEKEND EXCEPTION;
/*****************************************************************************
 Naam      : Aanmaken_brieven_BI
 Doel      : Het aanmaken van brieven met het verzoek inzending van diverse gegevens
             voor een juiste database.
 Parameters: p_script_naam   | wordt door JCS
             p_script_id     | bepaald
 Wijzigingshistorie:
 Versie Naam                   Datum      Wijziging
 ------ ---------------------- ---------- ------------------------------------
 1.0    J. Tsesmelis           19-09-2007 Nav aanpassingen ivm uitfaseren filemerge
                                          brieven worden voortaan dmv OPU en Streamserve aangemaakt.
                                          Briefvariablen via xml-bestand.
 1.1    Tony Yoeri             03-06-2008 PV-KC/BU/BI wordt PD-KC/CONS
 1.2    XFZ                    21-09-2009 aanroep get_ext_rle aangepast (2e param)
 1.3    mal                    05-12-2011 Div bevindingen op de batch, logverslag aangepast
                                          verplaatst van wsf.
 1.4    PLW                    04-11-2016 PE-44: Vergroten inzetbaarheid RLEPRC57 batch tbv werkingssfeer
******************************************************************************/

PROCEDURE OPU_VERZEND
 (P_DOC_TYPE IN VARCHAR2
 ,P_DOC_ID OUT NUMBER
 );


PROCEDURE OPU_VERZEND
 (P_DOC_TYPE IN VARCHAR2
 ,P_DOC_ID OUT NUMBER
 )
 IS
begin
  stm_util.debug('start opu_verzend');

  stm_doc_direct.verwerk ( p_script_naam => 'Handmatig_'||p_doc_type
                         , p_script_id => 1
                         , p_systeem => cn_systeem
                         , p_programma => cn_programma
                         , p_doc_kenmerk_opu => p_doc_type
                         , p_bron => 'BULK'
                         , p_ind_commit => 'N'
                         , p_ind_job_status => 'N'
                         , p_doc_id => p_doc_id
                         );
  --
  stm_util.debug('legen');

  stm_doc_direct_legen.verwerk ( p_script_naam => 'Handmatig_'||p_doc_type
                               , p_script_id => 1
                               , p_systeem => cn_systeem
                               , p_programma => cn_programma
                               , p_ind_commit => 'N'
                               , p_ind_job_status => 'N'
                               );
exception
   when others
   then
      raise_application_error (-20000
                              ,nvl (cg$errors.geterrors
                                   , stm_app_error (sqlerrm, null, null)
                                      || ' '
                                      || dbms_utility.format_error_backtrace) );
END opu_verzend;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_DOCUMENTSOORT IN VARCHAR2
 )
 IS
    cursor c_bkk is
      select bkk.werkgevernr from rle_brieven_kvk bkk order by werkgevernr;

    cursor c_rle(b_werkgevernr in varchar2) is
      select nr_werkgever wgnr,
             rle.namrelatie naam,
             str.straatnaam || ' ' || adr.huisnummer || ' ' ||
             adr.toevhuisnum str_hsnr,
             substr(adr.postcode, 1, 4) || '  ' ||
             substr(adr.postcode, 5, 2) || ' ' || wpl.woonplaats pc_wnpls,
             decode(wpl.lnd_codland, 'NL', '', wpl.lnd_codland) land,
             radr.dwe_wrddom_srtadr srt_adr,
             to_char(sysdate, 'dd-mm-yyyy') || '.' sys_datum
        from wgr_werkgevers       wgr,
             rle_relaties         rle,
             rle_relatie_adressen radr,
             rle_adressen         adr,
             rle_straten          str,
             rle_woonplaatsen     wpl
       where wgr.nr_werkgever = b_werkgevernr
         and wgr.nr_relatie = rle.numrelatie
         and radr.rol_codrol = 'WG'
         and rle.numrelatie = radr.rle_numrelatie
         and radr.ads_numadres = adr.numadres
         and adr.stt_straatid = str.straatid
         and str.wps_woonplaatsid = wpl.woonplaatsid
         and sysdate between radr.datingang and nvl(radr.dateinde, sysdate)
         and radr.dwe_wrddom_srtadr =
             (select min(radr2.dwe_wrddom_srtadr)
                from rle_relatie_adressen radr2
               where radr2.rle_numrelatie = radr.rle_numrelatie
                 and radr2.dateinde is null
                 and radr2.dwe_wrddom_srtadr in ('CA', 'SZ')
                 and sysdate between radr2.datingang and
                     nvl(radr2.dateinde, sysdate));

    cursor c_doc(b_documentsoort in varchar2) is
      select opu.productcode
        from comp_opu.opu_documenttypen opu
       where opu.srt = b_documentsoort
         and opu.vervaldatum is null;

    cursor c_admin(b_werkgevernr in varchar2, b_adm_code in varchar2) is
      select rle.adm_code
        from comp_rle.rle_relatierollen_in_adm rle
       where rle.rol_codrol = 'WG'
         and rle.rle_numrelatie =
             rle_lib.get_relatienr_werkgever(b_werkgevernr)
         and rle.adm_code = b_adm_code;

    --
    r_rle                 c_rle%rowtype;
    l_code                varchar2(15);
    l_wgnr                number;
    l_wgnr_fout           number;
    l_wgr_naam            varchar2(100);
    l_str_hsnr            varchar2(100);
    l_pc_wnpls            varchar2(50);
    l_land                varchar2(50);
    l_srt_adrs            varchar2(10);
    l_sysdate             varchar2(20);
    l_batch_vars          stm_batch.t_batch_vars;
    l_teller              pls_integer;
    l_relatienummer       number;
    l_index               binary_integer;
    l_locatie             varchar2(4);
    l_volgnummer          stm_job_statussen.volgnummer%type := 0;
    l_vorige_procedure    varchar2(255);
    l_huidige_procedure   varchar2(255) := 'RLE_AANMAKEN_BR_OPVR_KVK';
    l_sqlerrm             varchar2(255);
    l_lijstnummer         stm_lijsten.lijstnummer%type := 1;
    l_regelnummer         stm_lijsten.regelnummer%type := 0;
    l_aantal_aangemaakt   pls_integer := 0;
    l_aantal_geselecteerd pls_integer := 0;
    l_aantal_fout         pls_integer := 0;
    l_doc_id              number;

    --
    l_adm_code varchar2(10);
    r_admin    c_admin%rowtype;
begin
    l_vorige_procedure        := stm_util.t_procedure;
    stm_util.t_programma_naam := 'RLE_AANMAKEN_BR_OPVR_KVK';
    stm_util.t_script_naam    := upper(p_script_naam);
    stm_util.t_script_id      := to_number(p_script_id);

    stm_util.insert_lijsten(l_lijstnummer,
                            l_regelnummer,
                            'Start aanmaken brieven opvragen KVK   : ' ||
                            to_char(sysdate, 'hh24:mi'));

    stm_util.insert_lijsten(l_lijstnummer,
                            l_regelnummer,
                            '-------------------------------------------------------------------------------');

    stm_util.insert_lijsten(l_lijstnummer, l_regelnummer, ' ');

    -- 1) Controleer of de opgegeven documentsoort geldig zijn
    open c_doc(p_documentsoort);
    fetch c_doc
      into l_adm_code;
    if c_doc%found then

      for r_bkk in c_bkk loop

        l_aantal_geselecteerd := l_aantal_geselecteerd + 1;

        begin

          open c_rle(r_bkk.werkgevernr);
          fetch c_rle
            into r_rle;

          if c_rle%found then


            -- 2) Controleer of de administratie code geldig is
            open c_admin(r_bkk.werkgevernr, l_adm_code);
            fetch c_admin
              into r_admin;

            if c_admin%found then
              close c_admin;


              l_wgnr     := r_rle.wgnr;
              l_wgr_naam := r_rle.naam;
              l_str_hsnr := r_rle.str_hsnr;
              l_pc_wnpls := r_rle.pc_wnpls;
              l_land     := r_rle.land;
              l_sysdate  := r_rle.sys_datum;
              l_srt_adrs := r_rle.srt_adr;


              -- 3) Controleer of de opgehaald gegevens volledig is
              if l_wgnr is null or l_wgr_naam is null or l_str_hsnr is null or
                 l_pc_wnpls is null or l_sysdate is null or
                 l_srt_adrs is null then
                l_wgnr        := r_bkk.werkgevernr;
                l_aantal_fout := l_aantal_fout + 1;

                raise e_foutieve_gegevens_set;

              end if;

            else
              l_wgnr        := r_bkk.werkgevernr;
              l_aantal_fout := l_aantal_fout + 1;

              raise e_ongeldig_adm_code;
            end if;

          else

            l_wgnr        := r_bkk.werkgevernr;
            l_aantal_fout := l_aantal_fout + 1;

            raise e_onbekend;

          end if;

          close c_rle;

          l_teller := 1;
          l_batch_vars.delete;

          if l_wgnr is not null then
            -- wrapperaanroep bepalen RELATIENUMMER
            get_ext_rle(l_wgnr, 'WGR', 'WG', null, l_relatienummer);
          end if;

          l_batch_vars(l_teller).variabele := 'WG_NUMMER_D';
          l_batch_vars(l_teller).waarde := lpad(l_wgnr, 6, 0);
          l_batch_vars(l_teller).dty_srt := p_documentsoort;
          l_teller := l_teller + 1;

          l_batch_vars(l_teller).variabele := 'WG_SZ_NAAM_D';
          l_batch_vars(l_teller).waarde := l_wgr_naam;
          l_batch_vars(l_teller).dty_srt := p_documentsoort;
          l_teller := l_teller + 1;

          l_batch_vars(l_teller).variabele := 'STR_HNR_TV';
          l_batch_vars(l_teller).waarde := l_str_hsnr;
          l_batch_vars(l_teller).dty_srt := p_documentsoort;
          l_teller := l_teller + 1;

          l_batch_vars(l_teller).variabele := 'PC_WNPL';
          l_batch_vars(l_teller).waarde := l_pc_wnpls;
          l_batch_vars(l_teller).dty_srt := p_documentsoort;
          l_teller := l_teller + 1;

          l_batch_vars(l_teller).variabele := 'BLND_ADRS';
          l_batch_vars(l_teller).waarde := l_land;
          l_batch_vars(l_teller).dty_srt := p_documentsoort;
          l_teller := l_teller + 1;

          l_batch_vars(l_teller).variabele := 'SYSDATE_D';
          l_batch_vars(l_teller).waarde := l_sysdate;
          l_batch_vars(l_teller).dty_srt := p_documentsoort;
          l_teller := l_teller + 1;

          l_batch_vars(l_teller).variabele := 'SRT_ADRES_D';
          l_batch_vars(l_teller).waarde := l_srt_adrs;
          l_batch_vars(l_teller).dty_srt := p_documentsoort;
          l_teller := l_teller + 1;

          l_aantal_aangemaakt := l_aantal_aangemaakt + 1;

          --  --
          --  -- tonen array met variabelen. Voor DEBUG doeleinden
          --      for i in l_batch_vars.first .. l_batch_vars.last
          --      loop
          --        dbms_output.put_line ( to_char ( i ) || '.var:' || l_batch_vars ( i ).variabele || ' waarde:'
          --                               || l_batch_vars ( i ).waarde || ' dty_srt:' || l_batch_vars ( i ).dty_srt );
          --      end loop;
          --
          --     nav project uitfaseren filemerge
          --     output via OPU streamserve, aanroep via wrapper
          --
          comp_stm.ins_doc_data(p_systeem          => cn_systeem,
                                p_programma        => cn_programma,
                                p_doc_kenmerk_opu  => p_documentsoort,
                                p_relatienummer    => l_relatienummer,
                                p_code_rol_relatie => cn_code_rol_relatie,
                                p_groepnummer      => cn_groepnummer,
                                p_batch_vars       => l_batch_vars);

          opu_verzend(p_documentsoort, l_doc_id);

        exception
          when e_ongeldig_adm_code then
            if c_admin%isopen then
              close c_admin;

            end if;

            if c_rle%isopen then
              close c_rle;

            end if;

            stm_util.insert_lijsten(l_lijstnummer,
                                    l_regelnummer,
                                    'Geen administratiecode kunnen bepalen voor werkgever: ' ||
                                    l_wgnr);

          when e_foutieve_gegevens_set then

            if c_rle%isopen then
              close c_rle;

            end if;

            stm_util.insert_lijsten(l_lijstnummer,
                                    l_regelnummer,
                                    'De gegevens set voor werkgever: ' ||
                                    l_wgnr || ' is niet volledig.');
          when e_onbekend then
            if c_rle%isopen then
              close c_rle;

            end if;

            stm_util.insert_lijsten(l_lijstnummer,
                                    l_regelnummer,
                                    'Geen brief verzonden voor werkgever: ' ||
                                    l_wgnr);
        end;

        stm_util.t_programma_naam := 'RLE_AANMAKEN_BR_OPVR_KVK';
        stm_util.t_script_naam    := upper(p_script_naam);
        stm_util.t_script_id      := to_number(p_script_id);
      end loop;

      stm_util.insert_lijsten(l_lijstnummer,
                              l_regelnummer,
                              'Aantal werkgevers geselecteerd: ' ||
                              l_aantal_geselecteerd);

      stm_util.insert_lijsten(l_lijstnummer,
                              l_regelnummer,
                              'Aantal brieven verstuurd: ' ||
                              l_aantal_aangemaakt);

      stm_util.insert_lijsten(l_lijstnummer,
                              l_regelnummer,
                              'Aantal brieven niet aangemaakt: ' ||
                              l_aantal_fout);

      stm_util.insert_lijsten(l_lijstnummer, l_regelnummer, ' ');

      stm_util.insert_lijsten(l_lijstnummer,
                              l_regelnummer,
                              '-------------------------------------------------------------------------------');

      stm_util.insert_lijsten(l_lijstnummer,
                              l_regelnummer,
                              'Einde aanmaken brieven opvragen KVK   : ' ||
                              to_char(sysdate, 'hh24:mi'));
      --
      --
      stm_util.insert_job_statussen(l_volgnummer, 'S', '0');
      stm_util.verwijder_herstart;
      stm_util.t_procedure := l_vorige_procedure;
      commit;
    else
      raise e_ongeldig_documentsoort;
    end if;

    close c_doc;

  exception
    when e_ongeldig_documentsoort then
      if c_doc%isopen then
        close c_doc;

      end if;

      l_sqlerrm := 'De opgegeven documentsoort ' || p_documentsoort ||
                   ' is niet geldig';

      rollback;
      stm_util.insert_job_statussen(l_volgnummer, 'S', '1');
      stm_util.insert_job_statussen(l_volgnummer,
                                    'I',
                                    'RLE_AANMAKEN_BR_OPVR_KVK');
      stm_util.insert_job_statussen(l_volgnummer, 'I', l_sqlerrm);
      commit;

    when others then
      l_sqlerrm := substr(sqlerrm, 1, 255);
      rollback;
      stm_util.insert_job_statussen(l_volgnummer, 'S', '1');
      stm_util.insert_job_statussen(l_volgnummer,
                                    'I',
                                    'RLE_AANMAKEN_BR_OPVR_KVK');
      stm_util.insert_job_statussen(l_volgnummer, 'I', l_sqlerrm);
      commit;
  end;

END RLE_AANMAKEN_BR_OPVR_KVK;
/