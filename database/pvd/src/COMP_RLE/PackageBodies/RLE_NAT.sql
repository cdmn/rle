CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_NAT IS

FUNCTION NAT_GET_NAT
 (P_RLE_NUMRELATIE IN RLE_NATIONALITEITEN.RLE_NUMRELATIE%TYPE
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN DATE
 )
 RETURN VARCHAR2
 IS
--
cursor c_nat (b_rle_numrelatie in rle_nationaliteiten.rle_numrelatie%type , b_peildatum in date )
is
select nat.code_nationaliteit
from   rle_nationaliteiten nat
where  nat.rle_numrelatie = b_rle_numrelatie and
       nat.dat_begin <= b_peildatum and
       nvl(nat.dat_einde , b_peildatum) >= sysdate;
--
lc_procfuncnaam varchar2(100) := 'NAT_GET_NAT';
l_foutmelding   varchar2(1000);
l_nationaliteit rle_nationaliteiten.code_nationaliteit%type;
l_peildatum     date;
--
begin
--
open c_nat ( p_rle_numrelatie , p_dat_begin );
fetch c_nat into l_nationaliteit;
  if c_nat%found
  then return l_nationaliteit;
  else return null;
  end if;
close c_nat;
--
exception
when others
then
  --
  l_foutmelding := 'Onverwachte fout in '||g_package||'.'||lc_procfuncnaam||': '||substr(sqlerrm,1,200);
  return l_foutmelding;
  --
end;
FUNCTION NAT_INSERT
 (P_RLE_NUMRELATIE IN RLE_NATIONALITEITEN.RLE_NUMRELATIE%TYPE
 ,P_CODE_NATIONALITEIT IN VARCHAR2
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN DATE
 )
 RETURN VARCHAR2
 IS
--
cursor c_nat_old ( b_rle_numrelatie in rle_nationaliteiten.rle_numrelatie%type
                 , b_dat_begin in date
                 )
is
select *
from   rle_nationaliteiten nat
where  nat.rle_numrelatie = b_rle_numrelatie and
       nat.dat_begin >=
       ( select max(nat2.dat_begin)
         from rle_nationaliteiten nat2
         where nat2.rle_numrelatie = b_rle_numrelatie
       )
       ;
r_nat_old c_nat_old%rowtype;
--
l_new_id                    number;
l_new_rle_numrelatie        number;
l_new_code_nationaliteit    rle_nationaliteiten.code_nationaliteit%type;
l_new_dat_begin             date;
l_new_dat_einde             date;
l_old_id                    number;
l_old_rle_numrelatie        number;
l_old_code_nationaliteit    rle_nationaliteiten.code_nationaliteit%type;
l_old_dat_begin             date;
l_old_dat_einde             date;
--
l_rle_prs            varchar2(1);
l_ins_upd            varchar2(1);
l_ind_fout           varchar2(1)   := 'N';
l_foutmelding        varchar2(4000);
lc_procfuncnaam       varchar2(100) := 'NAT_INSERT';
--
begin
--
l_new_rle_numrelatie        := p_rle_numrelatie;
l_new_code_nationaliteit    := p_code_nationaliteit;
l_new_dat_begin             := trunc(p_dat_begin);
l_new_dat_einde             := trunc(p_dat_einde);
--
open  c_nat_old ( p_rle_numrelatie , p_dat_begin );
fetch c_nat_old into r_nat_old;
  if c_nat_old%found
  then
    l_old_id                    := r_nat_old.id;
    l_old_rle_numrelatie        := r_nat_old.rle_numrelatie;
    l_old_code_nationaliteit    := r_nat_old.code_nationaliteit;
    l_old_dat_begin             := trunc(r_nat_old.dat_begin);
    l_old_dat_einde             := trunc(r_nat_old.dat_einde);
  end if;
close c_nat_old;
--
if l_foutmelding is null
then
  if l_new_dat_begin = l_old_dat_begin
  then  l_ins_upd := 'U';
  else  l_ins_upd := 'I';
  end if;
end if;
--
if l_old_code_nationaliteit is not null and l_new_code_nationaliteit is null
then
  update rle_nationaliteiten nat
  set
    nat.dat_einde   = trunc(sysdate)
  where
    nat.rle_numrelatie = p_rle_numrelatie and
    nat.dat_begin      =
      ( select max(nat2.dat_begin)
        from   rle_nationaliteiten nat2
        where  nat2.rle_numrelatie = p_rle_numrelatie and
               nat2.dat_begin < sysdate
      );
else
  if l_ins_upd = 'I'
  then
    --
    insert into rle_nationaliteiten
    ( rle_numrelatie
    , code_nationaliteit
    , dat_begin
    , dat_einde
    )
  values
    ( p_rle_numrelatie
    , p_code_nationaliteit
    , p_dat_begin
    , p_dat_einde
    );
    --
  elsif l_ins_upd = 'U'
  then
    update rle_nationaliteiten
    set
      code_nationaliteit = p_code_nationaliteit
    , dat_begin          = p_dat_begin
    , dat_einde          = p_dat_einde
    where
      rle_numrelatie = p_rle_numrelatie and
      dat_begin      = l_old_dat_begin;
  else
    --
    if l_foutmelding is null
    then
      l_foutmelding := 'Geen insert of update uitgevoerd voor relatie '||p_rle_numrelatie;
    end if;
  --
  end if;
end if;
--
return l_foutmelding;
--
exception
when others
then
  --
  l_foutmelding := substr('Onverwachte fout in '||g_package||'.'||lc_procfuncnaam||': '||substr(sqlerrm,1,200),1,4000);
  return l_foutmelding;
 --
end;
FUNCTION NAT_DEL
 (P_NAT_ID IN RLE_NATIONALITEITEN.ID%TYPE
 )
 RETURN VARCHAR2
 IS
--
l_foutmelding varchar2(100);
lc_procfuncnaam varchar2(100) := 'NAT_DEL';
--
begin
--
delete from
  rle_nationaliteiten
where
  id = p_nat_id;
--
return l_foutmelding;
--
exception
when others
then
  --
  l_foutmelding := 'Onverwachte fout in '||g_package||'.'||lc_procfuncnaam||': '||substr(sqlerrm,1,200);
  return l_foutmelding;
  --
end;

END RLE_NAT;
/