CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_RRL IS

C_PACKAGE CONSTANT VARCHAR2(32) := 'PKG_RRL_CTX';


/* Insert van een relatierol record. */
PROCEDURE INS_RRL
 (P_RLE_NUMRELATIE IN rle_relatie_rollen.rle_numrelatie%TYPE
 ,P_ROL_CODROL IN rle_relatie_rollen.rol_codrol%TYPE
 )
 IS

C_MODULE CONSTANT VARCHAR2(32) := 'p_ins_rrl';
BEGIN
   insert into rle_relatie_rollen( rle_numrelatie
                                 , rol_codrol
                                 , codorganisatie
                                 )
   values ( p_rle_numrelatie
          , p_rol_codrol
          , 'A'
          );

   EXCEPTION
   WHEN OTHERS
   THEN
     qms$errors.unhandled_exception(c_package||'.'||c_module);
END INS_RRL;
/* update rol_coderol */
PROCEDURE UPD_RRL_ROL
 (P_RLE_NUMRELATIE IN rle_relatie_rollen.rle_numrelatie%TYPE
 ,P_ROL_CODROL IN rle_relatie_rollen.rol_codrol%TYPE
 ,P_OLD_ROL_CODROL IN rle_relatie_rollen.rol_codrol%TYPE
 )
 IS

C_MODULE CONSTANT VARCHAR2(32) := 'p_upd_rrl_rol';
BEGIN
  -- Eerst de rol aanmaken met de nieuwe code
  --
  ins_rrl(p_rle_numrelatie => p_rle_numrelatie
         , p_rol_codrol => p_rol_codrol);
  --
  -- Kopieren van de relatierollen in administraties
  --
  insert into rle_relatierollen_in_adm
  (adm_code
  , rle_numrelatie
  , rol_codrol)
  select adm_code
  , rle_numrelatie
  , p_rol_codrol
  from rle_relatierollen_in_adm
  where rle_numrelatie = p_rle_numrelatie
  and rol_codrol = p_old_rol_codrol;
  --
  -- Rolcode aanpassen van de bestaande adressen
  --
  update rle_relatie_adressen
  set rol_codrol = p_rol_codrol
  where rle_numrelatie = p_rle_numrelatie
  and rol_codrol = p_old_rol_codrol;
  --
  -- Oude relatierol in amdinistratie verwijderen
  --
  delete rle_relatierollen_in_adm rie
  where  rie.rol_codrol = p_old_rol_codrol
    and  rie.RLE_NUMRELATIE = p_rle_numrelatie;
  --
  -- Oude relatierollen verwijderen
  --
  delete rle_relatie_rollen
  where  rle_numrelatie = p_rle_numrelatie
  and    rol_codrol = p_old_rol_codrol
  ;
  EXCEPTION
   WHEN OTHERS
   THEN
     qms$errors.unhandled_exception(c_package||'.'||c_module);
END UPD_RRL_ROL;

END RLE_RRL;
/