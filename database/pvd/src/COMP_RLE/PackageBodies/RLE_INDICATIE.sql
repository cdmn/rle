CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_INDICATIE IS

/* Zet de indicatie op J voor gebruik in forms. */
PROCEDURE INDICATIE_GBA_J
 IS
begin
  doorgeven_aan_gba := 'J';
end;
/* Zet de indicatie op N voor gebruik in forms. */
PROCEDURE INDICATIE_GBA_N
 IS
begin
  doorgeven_aan_gba := 'N';
end;
/* Teruggeven GBA indicatie voro gebruik in forms */
FUNCTION INDICATIE_GBA
 RETURN VARCHAR2
 IS
begin
  return(doorgeven_aan_gba);
end;
BEGIN
   /* Standaard moeten wijzigingen aan GBA worden doorgegeven */
   doorgeven_aan_gba := 'J' ;
END;
/