CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_VZT IS

G_PACKAGE CONSTANT VARCHAR2(20) := 'RLE_VZT';


/* controle op overlap van verzetregistraties */
FUNCTION RLE_VZT_OVERLAP
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_TUSSENPERSOON IN RLE_RELATIE_VERZETTEN.TUSSENPERSOON%TYPE
 ,P_ADMINISTRATIE IN RLE_ADMINISTRATIES.CODE%TYPE
 ,P_PRODUCTGROEP IN RLE_RELATIE_VERZETTEN.PRODUCTGROEP%TYPE
 ,P_VERZET_KANAAL IN RLE_RELATIE_VERZETTEN.VERZET_KANAAL%TYPE
 )
 RETURN BOOLEAN
 IS
/******************************************************************************************************
  Naam:          RLE_VZT_OVERLAP  (  BR IER0038 RVT  )
  Beschrijving:  Functie welke controleert of er geen overlap is met bestaande verzet bij registreren van verzet
                            in verzetsadministratie  ( RLE_RELATIE_VERZETTEN)
  Toelichting :   indien overlap gevonden  functie returned     = TRUE
                  indien geen overlap gevonden functie returned = FALSE
                 ( Aangeroepen in triggers RLE_RVT_BRI + RLE_RVT_BRU )

                 voorbeeld overlap
                 TP       Adm    Productgroep   Kanaal
                 --------------------------------------------------
                 1  BIV     -    -              -
                 2  BIV     -    VERZUIM        -
                 3  BIV     -    -              Telefoon

                 als 1 bestaat dan mogen 2 en 3 niet worden toegevoegd wegens overlap
                 als 2 ( of 3 ) bestaat dan mag 1 niet worden toegevoegd
                 als 2 bestaat mag wel 3 worden toegevoegd ( !ondanks overlap! )
                 als 3 bestaat mag wel 2 worden toegevoegd ( !ondanks overlap! )

  Datum       Wie          Wat
  ---------------------------------------------------------------------------
  10-11-2008  Jan luinenburg  Creatie ( BR IER0038 RVT )
*******************************************************************************************************/
cursor c_vzt_1( b_numrelatie    in rle_relaties.numrelatie%type
              , b_tussenpersoon in rle_relatie_verzetten.tussenpersoon%type
              , b_administratie in rle_administraties.code%type ) is
  -- bestaat er al een verzet voor gehele Tussenpersoon/Administratie
  select 1
  from rle_relatie_verzetten rvt
  where rvt.rle_numrelatie = b_numrelatie
    and (
         ( b_tussenpersoon is not null and
           rvt.tussenpersoon = b_tussenpersoon ) or
         ( b_administratie is not null and
           rvt.adm_code      = b_administratie )
        )
    and rvt.productgroep  is null
    and rvt.verzet_kanaal is null
    and rvt.verval_datum is null;
--
cursor c_vzt_2( b_numrelatie    in rle_relaties.numrelatie%type
              , b_tussenpersoon in rle_relatie_verzetten.tussenpersoon%type
              , b_administratie in rle_administraties.code%type
              , b_productgroep  in rle_relatie_verzetten.productgroep%type
              , b_verzet_kanaal in rle_relatie_verzetten.verzet_kanaal%type ) is
  -- bestaat er al een verzet voor productgroep of verzetskanaal
  select 1
  from rle_relatie_verzetten rvt
  where rvt.rle_numrelatie = b_numrelatie
    and (
         ( b_tussenpersoon is not null and
           rvt.tussenpersoon = b_tussenpersoon ) or
         ( b_administratie is not null and
           rvt.adm_code      = b_administratie )
        )
    and ( rvt.productgroep is not null or
         rvt.verzet_kanaal is not null )
    and rvt.verval_datum is null;
--
lc_procfuncnaam constant varchar2(20) := 'rle_vzt_overlap';
l_dummy         pls_integer;
l_retval        boolean := false;
e_params        exception;
begin
  -- controle parameters
  if p_numrelatie is null or
    (
      ( p_tussenpersoon is null and p_administratie is null ) or
      ( p_tussenpersoon is not null and p_administratie is not null )
    )
  then
     raise e_params;
  end if;
  -- verzet aangetekend per productgroep of verzetkanaal ( of beide )
  if p_productgroep is not null or
     p_verzet_kanaal is not null
  then
     -- bestaat er al een verzet over gehele tussenpersoon of administratie
     open c_vzt_1( b_numrelatie    => p_numrelatie
                 , b_tussenpersoon => p_tussenpersoon
                 , b_administratie => p_administratie );
     fetch c_vzt_1 into l_dummy;
     l_retval := c_vzt_1%found;
     close c_vzt_1;
  end if;
  -- verzet aangetekend over gehele tussenpersoon of administratie
  if p_productgroep is null and
     p_verzet_kanaal is null
  then
    -- bestaat er al een verzet per productgroep of verzetkanaal ( of beide )
    open c_vzt_2( b_numrelatie    => p_numrelatie
                , b_tussenpersoon => p_tussenpersoon
                , b_administratie => p_administratie
                , b_productgroep  => p_productgroep
                , b_verzet_kanaal => p_verzet_kanaal);
    fetch c_vzt_2 into l_dummy;
    l_retval := c_vzt_2%found;
    close c_vzt_2;
  end if;
  --
  return l_retval;
  --
exception
when e_params
then
  qms$errors.unhandled_exception('Parameters niet juist gevuld in '||g_package||'.'||lc_procfuncnaam);
when others
then
  if c_vzt_1%isopen
  then
    close c_vzt_1;
  end if;
  --
  if c_vzt_2%isopen
  then
    close c_vzt_2;
  end if;
  qms$errors.unhandled_exception('Onverwachte fout in '||g_package||'.'||lc_procfuncnaam||' fout : '||sqlerrm);
END RLE_VZT_OVERLAP;
/* controle tussenperoon of administratie gevuld in verzetsadministratie */
FUNCTION RLE_VZT_TP_ADM
 (P_TUSSENPERSOON IN RLE_RELATIE_VERZETTEN.TUSSENPERSOON%TYPE
 ,P_ADMINISTRATIE IN RLE_ADMINISTRATIES.CODE%TYPE
 )
 RETURN BOOLEAN
 IS
/******************************************************************************************************
  Naam            :   RLE_VZT_TP_ADM
  Beschrijving:  Functie welke controleert of tussenpersoon dan wel administratie is gevuld bij
                                           registreren van verzet in verzetsadministratie  ( RLE_RELATIE_VERZETTEN)
  Toelichting :  indien juiste vulling functie returned  = TRUE
                          indien onjuiste vulling geeft returned = FALSE
                          ( Aangeroepen in triggers RLE_RVT_BRI + RLE_RVT_BRU )

  Datum          Wie                        Wat
--------------------------------------------------------------------------------------------------------------------------------
  10-11-2008  Jan luinenburg  Creatie ( BR IER0038 RVT )
*******************************************************************************************************/
lc_procfuncnaam constant varchar2(20) := 'rle_vzt_tp_adm';
l_retval boolean := true;
begin
  -- minstens 1 van de 2 moet gevuld zijn
  if p_tussenpersoon is null and
     p_administratie is null
  then
     l_retval := false;
  end if;
  -- hooguit 1 van de 2 mag gevuld zijn
  if p_tussenpersoon is not null and
     p_administratie is not null
  then
     l_retval := false;
  end if;
  --
  return l_retval;
  --
exception
when others
then
   qms$errors.unhandled_exception('Onverwachte fout in '||g_package||'.'||lc_procfuncnaam||' fout : '||sqlerrm);
END RLE_VZT_TP_ADM;
/* controle op reeds bestaan van verzetregistraties */
FUNCTION RLE_VZT_UNIEK
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_TUSSENPERSOON IN RLE_RELATIE_VERZETTEN.TUSSENPERSOON%TYPE
 ,P_ADMINISTRATIE IN RLE_ADMINISTRATIES.CODE%TYPE
 ,P_PRODUCTGROEP IN RLE_RELATIE_VERZETTEN.PRODUCTGROEP%TYPE
 ,P_VERZET_KANAAL IN RLE_RELATIE_VERZETTEN.VERZET_KANAAL%TYPE
 )
 RETURN BOOLEAN
 IS
/******************************************************************************************************
  Naam:          RLE_VZT_UNIEK ( BR IER0038 RVT )
  Beschrijving:  Functie welke controleert of er dubbele invoer is met bestaande verzet bij registreren van verzet
                            in verzetsadministratie  ( RLE_RELATIE_VERZETTEN)
  Toelichting :   indien dubbele invoer gevonden  functie returned     = TRUE
                         indien dubbele invoer gevonden functie returned = FALSE
                 ( Aangeroepen in triggers RLE_RVT_BRI + RLE_RVT_BRU )

   Datum       Wie          Wat
  -------------------------------------------------------------------------------------------------------------------------------
  11-11-2008  Jan luinenburg  Creatie  ( BR IER0038 RVT  )
*******************************************************************************************************/
cursor c_vzt( b_numrelatie    in rle_relaties.numrelatie%type
                        , b_tussenpersoon in rle_relatie_verzetten.tussenpersoon%type
                        , b_administratie in rle_administraties.code%type
                        , b_productgroep  in rle_relatie_verzetten.productgroep%type
                        , b_verzet_kanaal in rle_relatie_verzetten.verzet_kanaal%type ) is
  -- bestaat er al een zelfde verzet
  select 1
  from rle_relatie_verzetten rvt
  where rvt.rle_numrelatie = b_numrelatie
    and nvl(rvt.tussenpersoon,'XYZ') = nvl(b_tussenpersoon,'XYZ')
    and nvl(rvt.adm_code,'XYZ')       = nvl(b_administratie,'XYZ')
    and nvl(rvt.productgroep,'XYZ')   = nvl(b_productgroep,'XYZ')
    and nvl(rvt.verzet_kanaal,'XYZ')  = nvl(b_verzet_kanaal,'XYZ')
    and rvt.verval_datum is null;
--
lc_procfuncnaam constant varchar2(20) := 'rle_vzt_uniek';
l_dummy         pls_integer;
l_retval        boolean := false;
e_params        exception;
begin
  -- controle parameters
  if p_numrelatie is null or
    (
      ( p_tussenpersoon is null and p_administratie is null ) or
      ( p_tussenpersoon is not null and p_administratie is not null )
    )
  then
     raise e_params;
  end if;
  --
  -- bestaat er al een zelfde verzet
  open c_vzt( b_numrelatie         => p_numrelatie
                       , b_tussenpersoon => p_tussenpersoon
                       , b_administratie     => p_administratie
                       , b_productgroep     => p_productgroep
                       , b_verzet_kanaal    => p_verzet_kanaal);
   fetch c_vzt into l_dummy;
   l_retval := c_vzt%found;
  close c_vzt;
  --
  return l_retval;
  --
exception
when e_params
then
  qms$errors.unhandled_exception('Parameters niet juist gevuld in '||g_package||'.'||lc_procfuncnaam);
when others
then
  if c_vzt%isopen
  then
    close c_vzt;
  end if;
  --
  qms$errors.unhandled_exception('Onverwachte fout in '||g_package||'.'||lc_procfuncnaam||' fout : '||sqlerrm);
END RLE_VZT_UNIEK;

END RLE_VZT;
/