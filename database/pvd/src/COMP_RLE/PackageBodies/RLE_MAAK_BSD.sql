CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_MAAK_BSD IS
   cn_verre_toekomst             constant date                                  := to_date ('01-01-2999', 'dd-mm-yyyy');
   cn_initiele_datum             constant date                                  := to_date ('01-01-1992', 'dd-mm-yyyy');
   --
   cn_sep                        constant varchar2 (10)                                        := ';';
   cn_spatie                     constant varchar2 (1)                                         := ' ';
   cn_at                         constant varchar2 (1)                                         := '@';
   --
   cn_op_del                     constant varchar2 (3)                                         := 'DEL';
   cn_merge                      constant varchar2 (1)                                         := 'M';
   cn_delete                     constant varchar2 (1)                                         := 'D';
   cn_truncate                   constant varchar2 (1)                                         := 'T';
   --
   cn_gegeven_niet_beschikbaar   constant varchar2 (30)                                        := 'g.n.b.';
   --
   cn_fc_beambte                  constant varchar2 (2)                                         := 'B';
   cn_fc_handarbeider             constant varchar2 (2)                                         := 'H';
   cn_fc_beambte_of_handarbeider  constant varchar2 (2)                                         := 'HB';
   --
   cn_corr_adres                 constant varchar2 (10)                                        := 'CA';
   cn_stat_zetel                 constant varchar2 (10)                                        := 'SZ';
   cn_opg_adres                  constant varchar2 (10)                                        := 'OA';
   --
   cn_sal                        constant avg_werknemer_inkomens.code_inkomenscomponent%type   := 'SALI';
   --
   cn_wgr                        constant varchar2 (10)                                        := 'WGR';
   cn_wnr                        constant varchar2 (10)                                        := 'WNR';
   cn_wg_rol                     constant varchar2 (10)                                        := 'WG';
   cn_psn_nr                     constant varchar2 (10)                                        := 'PRS';
   cn_bsn_nr                     constant varchar2 (10)                                        := 'SOFI';
   cn_psn_rol                    constant varchar2 (10)                                        := 'PR';
   --
   cn_nederland                  constant varchar2 (10)                                        := 'NL';
   /* Fondsen */
   cn_otib_l                     constant varchar2 (10)                                        := 'OTIB_L';
   cn_otib_k                     constant varchar2 (10)                                        := 'OTIB_K';
   cn_otib_e                     constant varchar2 (10)                                        := 'OTIB_E';
   cn_olc                        constant varchar2 (10)                                        := 'OLC';
   cn_sko                        constant varchar2 (10)                                        := 'SKO';
   cn_ofe                        constant varchar2 (10)                                        := 'OFE';

   --
   type layout_rec is record (
      veldnr         number (2, 0)
     ,veldnaam       varchar2 (30)
     ,veldtype       varchar2 (1)
     ,veldlengte     number (4, 2)
     ,veldweergave   varchar2 (100)
   );

   type layout_tab is table of layout_rec
      index by binary_integer;

   /* private globals */
   i_veldnr                               binary_integer;
   g_string                               varchar2 (2000);
   g_tmp_file_name                        varchar2 (50);
   g_bsd_layout                           layout_tab;
   g_bsd                                  utl_file.file_type;
   g_directory                            rle_bestanden.db_directory%type;
   /* Global exceptions */
   e_invalid_output_type                  exception;

   /* Global cursor definitions */
   cursor c_bsd001(b_bestandsgroep in rle_bestanden.bsd_groep%type)
   is
      select   naam
              ,db_directory
              ,bsd_type
              ,formaat_naam
      from     rle_bestanden
      where    bsd_groep = b_bestandsgroep
      order by bsd_type
              ,naam;

   cursor c_adr001 (
      b_rle_numrelatie     in   rle_relatie_adressen.rle_numrelatie%type
     ,b_peildatum          in   date
     ,b_vorige_peildatum   in   date)
   is
      select   stt.straatnaam
              ,wps.woonplaats
              ,case
                  when adr.lnd_codland = cn_nederland
                     then substr (adr.postcode, 1, 4) || cn_spatie || substr (adr.postcode, 5)
                  else adr.postcode
               end postcode
              ,adr.huisnummer
              ,adr.toevhuisnum
              ,lnd.codland land
      from     rle_adressen adr
              ,rle_relatie_adressen ras
              ,rle_straten stt
              ,rle_woonplaatsen wps
              ,rle_landen lnd
      where    adr.numadres = ras.ads_numadres
      and      adr.stt_straatid = stt.straatid
      and      adr.wps_woonplaatsid = wps.woonplaatsid
      and      adr.lnd_codland = lnd.codland
      and      ras.dwe_wrddom_srtadr in (cn_opg_adres, cn_corr_adres)   /* Opgegeven adres (igv personen), Corr adres (igv werkgevers) */
      and      ras.rle_numrelatie = b_rle_numrelatie
      and      ras.datingang <= b_peildatum
      and      nvl (ras.dateinde, cn_verre_toekomst) >= b_vorige_peildatum
      order by nvl (ras.dateinde, cn_verre_toekomst) desc;

/* Initialisatie */
PROCEDURE INIT_WGR;
PROCEDURE INIT_WNR;
/* Toevoegen van een velddefinitie */
PROCEDURE ADD_VELD
 (P_VELDNR IN NUMBER
 ,P_VELDNAAM IN varchar2
 ,P_VELDTYPE IN varchar2
 ,P_VELDLENGTE IN number
 ,P_VELDWEERGAVE IN varchar2
 );
/* Het openen van een tijdlijk bestand */
PROCEDURE OPEN_BESTAND
 (P_FILE_NAME IN varchar2
 );
/* Het afsluiten van het aangemaakte bestand */
FUNCTION SLUIT_BESTAND
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,P_ROWCOUNT IN number
 )
 RETURN RLE_UITVOER_STATUSSEN.BESTANDSNAAM%TYPE;
/* Voeg de string toe aan een regel */
PROCEDURE PUT
 (P_STRING IN varchar2
 );
/* Voeg de string toe aan een regel en schrijf deze regel naar het output */
PROCEDURE PUT_LINE
 (P_STRING IN varchar2 := null
 );
/* schrijf de inhoud van een veld naar het output medium */
PROCEDURE PRINT
 (P_VELDNR IN number
 ,P_WAARDE IN date
 );
/* schrijf de inhoud van een veld naar het output medium */
PROCEDURE PRINT
 (P_VELDNR IN number
 ,P_WAARDE IN varchar2
 );
/* Het bepalen/overrulen van de */
PROCEDURE BEPAAL_PARAMETERS
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,O_VORIGE_PEILDATUM IN OUT date
 ,O_VORIGE_RUNDATUM IN OUT date
 ,O_RUNDATUM IN OUT date
 );
/* Het opslaan van de gegevens van de zojuist aangemaakte bestanden */
PROCEDURE OPSLAAN_UITVOER_GEGEVENS
 (P_RUNNUMMER IN rle_uitvoer_statussen.runnummer%type
 ,P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_VANAF_DATUM IN rle_uitvoer_statussen.vanaf_datum%type
 ,P_TOT_DATUM IN rle_uitvoer_statussen.tot_datum%type
 ,P_STARTTIJD_UITVOER IN rle_uitvoer_statussen.starttijd_uitvoer%type
 ,P_BESTANDSNAAM IN rle_uitvoer_statussen.bestandsnaam%type
 );
/* Controle of een werkgevers na de initiële datum op enig moment */
FUNCTION WGR_IS_OTIB
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_WGR_NR IN wgr_werkgevers.nr_werkgever%type
 ,O_WGR_NR IN OUT wgr_werkgevers.nr_werkgever%type
 )
 RETURN BOOLEAN;
FUNCTION WGR_IS_OTIB
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_RLE_NR IN rle_relaties.numrelatie%type
 ,O_WGR_NR IN OUT wgr_werkgevers.nr_werkgever%type
 )
 RETURN BOOLEAN;
/* Controle of een werkgevers na de initiële datum op enig moment */
FUNCTION WGR_IS_OTIB
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_WGR_ID IN wgr_werkgevers.id%type
 ,O_WGR_NR IN OUT wgr_werkgevers.nr_werkgever%type
 )
 RETURN BOOLEAN;
/* Vullen tijdelijke werkgever tabel */
PROCEDURE AANMAKEN_TMP_WGR
 (P_BSD_NAAM IN rle_bestanden.naam%type
 );
/* aanmaken van delete opdracht records voor het wgr bestand */
PROCEDURE AANMAKEN_WGR_DEL_RECORDS
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_VORIGE_RUN_DATUM IN date
 ,P_AANTAL_WGR_DEL OUT NUMBER
 );
/* aanmaken van delete opdracht records voor het wnr bestand */
PROCEDURE AANMAKEN_WNR_DEL_RECORDS
 (P_VORIGE_RUN_DATUM IN date
 ,P_AANTAL_WNR_DEL OUT NUMBER
 );
/* Aanmaken van het OTIB werkgeverbestand */
FUNCTION AANMAKEN_WGR_BSD
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,P_VORIGE_RUNDATUM IN date
 )
 RETURN RLE_UITVOER_STATUSSEN.BESTANDSNAAM%TYPE;
/* Aanmaken van het OTIB werknemerbestand */
FUNCTION AANMAKEN_WNR_BSD
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,P_VORIGE_RUNDATUM IN date
 )
 RETURN RLE_UITVOER_STATUSSEN.BESTANDSNAAM%TYPE;


/* Initialisatie */
PROCEDURE INIT_WGR
 IS
   /******************************************************************************
      NAAM:       init_wgr
      DOEL:       initialisatie routine

                  Let op: Deze routine zorgt alleen voor de prompts, het datatype,
                          de lengte,het format, de transformatie en de encryptie
                          van een veld. De volgorde en het aantal velden van een
                          bestand worden hierin niet geregeld.

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          11-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type   := cn_package || '.INIT';
begin
      i_veldnr := 0;
      g_string := null;
      g_bsd_layout.delete;
      g_tmp_file_name := cn_wgr;
      --
      /* Let op: Pas ook routine aanmaak_wgr_bsd aan bij aanpassing van de
      ** volgnummers van de velden!
      */
      add_veld (1, 'Wgr', 'N', 6, 'LPAD(#,6,''0'')');
      add_veld (2, 'Mutatie type', 'A', 1, null);
      add_veld (3, 'Ing.datum', 'D', null, 'DD-MM-YYYY');
      add_veld (4, 'Einddatum', 'D', null, 'DD-MM-YYYY');
      add_veld (5, 'Naam', 'A', 30, null);
      add_veld (6, 'Rechtsvorm', 'A', '10', null);
      add_veld (7, 'Ing.datum lidm', 'D', null, 'DD-MM-YYYY');
      add_veld (8, 'Einddatum lidm', 'D', null, 'DD-MM-YYYY');
      add_veld (12, 'Straat', 'A', 26, null);
      add_veld (13, 'Huisnr', 'N', 5, null);
      add_veld (14, 'Huisnr toev', 'A', 10, null);
      add_veld (15, 'Plaats', 'N', 30, 'UPPER(#)');
      add_veld (16, 'PC', 'A', 7, null);
      add_veld (17, 'Straat CA', 'A', 26, null);
      add_veld (18, 'Huisnr CA', 'N', 6, null);
      add_veld (19, 'Huisnr toev CA', 'A', 10, null);
      add_veld (20, 'Plaats CA', 'A', 30, 'UPPER(#)');
      add_veld (21, 'PC CA', 'A', '7', null);
      add_veld (9, 'Fonds', 'A', '6', null);
      add_veld (10, 'GVW', 'N', '6', null);
      add_veld (11, 'SGW', 'N', '6', null);
      add_veld (22, 'Datum einde', 'D', null, 'DD-MM-YYYY');
      add_veld (23, 'Reden einde', 'A', '4', null);
      add_veld (24, 'Overname wgr', 'N', '6', 'LPAD(#,6, ''0'')');
      add_veld (25, 'Aantal wnr', 'N', '6', null);
      open_bestand (g_tmp_file_name);
   end init_wgr;
PROCEDURE INIT_WNR
 IS
   /******************************************************************************
      NAAM:       init_wnr
      DOEL:       initialisatie routine

                  Let op: Deze routine zorgt alleen voor de prompts, het datatype,
                          de lengte,het format, de transformatie en de encryptie
                          van een veld. De volgorde en het aantal velden van een
                          bestand worden hierin niet geregeld.

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          11-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type   := cn_package || '.INIT';
begin
      i_veldnr := 0;
      g_string := null;
      g_bsd_layout.delete;
      g_tmp_file_name := cn_wnr;
      --
      /* Let op: Pas ook routine aanmaak_wgr_bsd aan bij aanpassing van de
      ** volgnummers van de velden!
      */
      add_veld (1,'Persnr', 'N', 8, 'LPAD(#,8,''0'')');
      add_veld (2,'Mutatie type','A',1,null);
      add_veld (3,'Ing.datum', 'D', null, 'DD-MM-YYYY');
      add_veld (4,'Einddatum', 'D', null, 'DD-MM-YYYY');
      add_veld (5,'Wgr', 'N', 6, 'LPAD(#,6,''0'')');
      add_veld (6,'BSN', 'N', 10, null);
      add_veld (7,'Naam', 'A', 30, null);
      add_veld (8,'Voorvoegsels', 'A', 7, null);
      add_veld (9,'Voorletters', 'A', 10, 'REPLACE(REPLACE(#,''.''),'' '')');
      add_veld (10,'Partnernaam', 'A', 50, null);
      add_veld (11,'Voorv.partner', 'A', 7, null);
      add_veld (12,'Geboortedatum', 'D', null, 'DD-MM-YYYY');
      add_veld (13,'Datum overlijden', 'D', null, 'DD-MM-YYYY');
      add_veld (14,'Geslacht', 'A', '1', null);
      add_veld (15,'Straat', 'A', 26, null);
      add_veld (16,'Huisnr', 'N', 5, null);
      add_veld (17,'Huisnr toev', 'A', 10, null);
      add_veld (18,'Plaats', 'N', 30, 'UPPER(#)');
      add_veld (19,'PC', 'A', 7, null);
      add_veld (20,'Land', 'A', 30, null);
      add_veld (21,'Datum in dienst', 'D', null, 'DD-MM-YYYY');
      add_veld (22,'Datum ontslag', 'D', null, 'DD-MM-YYYY');
      add_veld (23,'Reden ontslag', 'A', '4', null);
      add_veld (24,'Omv. DVD', 'N', 5, null);
      add_veld (25,'Inkomen', 'N', 10, null);
      add_veld (26,'Inkomensfreq.', 'A', 1, null);
      add_veld (27,'Functiecategorie', 'A', 10, null);
      add_veld (28,'Beroepscode', 'A', 10, null);

      open_bestand (g_tmp_file_name);
   end init_wnr;
/* Toevoegen van een velddefinitie */
PROCEDURE ADD_VELD
 (P_VELDNR IN NUMBER
 ,P_VELDNAAM IN varchar2
 ,P_VELDTYPE IN varchar2
 ,P_VELDLENGTE IN number
 ,P_VELDWEERGAVE IN varchar2
 )
 IS
   /******************************************************************************
      NAAM:       add_veld
      DOEL:       Het toevoegen van een velddefinitie aan de pl/sql tabel die de
                bestandslayout bevat

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type   := cn_package || '.INIT';
      l_rec_layout         layout_rec;
begin
   i_veldnr := greatest(p_veldnr,i_veldnr);
   l_rec_layout.veldnr := p_veldnr;
   l_rec_layout.veldnaam := p_veldnaam;
   l_rec_layout.veldtype := p_veldtype;
   l_rec_layout.veldlengte := p_veldlengte;
   l_rec_layout.veldweergave := p_veldweergave;
   g_bsd_layout (p_veldnr) := l_rec_layout;
end add_veld;
/* Het openen van een tijdlijk bestand */
PROCEDURE OPEN_BESTAND
 (P_FILE_NAME IN varchar2
 )
 IS
   /******************************************************************************
      NAAM:       open_bestand
      DOEL:       Het openen van een tijdlijk bestand

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
begin
      if g_output_type = cn_output_file
      then
         g_bsd := utl_file.fopen (g_directory, p_file_name, 'w');
      elsif g_output_type = cn_output_screen
      then
         dbms_output.enable (1000000);
      else
         raise e_invalid_output_type;
      end if;
   end open_bestand;
/* Het afsluiten van het aangemaakte bestand */
FUNCTION SLUIT_BESTAND
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,P_ROWCOUNT IN number
 )
 RETURN RLE_UITVOER_STATUSSEN.BESTANDSNAAM%TYPE
 IS
   /******************************************************************************
      NAAM:       sluit_bestand
      DOEL:       Het afsluiten van het aangemaakte bestand

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      l_file_name               rle_uitvoer_statussen.bestandsnaam%type;
      cn_date_format   constant varchar2 (20)                             := 'YYYYMMDD';
begin
      select replace (replace (replace (replace (formaat_naam
                                                ,'<vanaf_datum>'
                                                ,to_char (p_vorige_peildatum, cn_date_format) )
                                       ,'<tot_datum>'
                                       ,to_char (p_peildatum, cn_date_format) )
                              ,'<rowcount>'
                              ,p_rowcount)
                     ,'<jobid>'
                     ,stm_util.t_script_id) geformateerde_naam
      into   l_file_name
      from   rle_bestanden
      where  naam = p_bsd_naam;

      if g_output_type = cn_output_file
      then
         utl_file.fclose (g_bsd);
         utl_file.frename (src_location =>       g_directory
                          ,src_filename =>       g_tmp_file_name
                          ,dest_location =>      g_directory
                          ,dest_filename =>     l_file_name
                          ,overwrite =>          true);
      elsif g_output_type = cn_output_screen
      then
         put_line (to_char (p_rowcount) || ' rows selected');
      else
         raise e_invalid_output_type;
      end if;

      return l_file_name;
   end sluit_bestand;
/* Voeg de string toe aan een regel */
PROCEDURE PUT
 (P_STRING IN varchar2
 )
 IS
   /******************************************************************************
      NAAM:       put
      DOEL:       Voeg de string toe aan een regel

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          11-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type   := cn_package || '.PUT';
begin
      g_string := g_string || p_string;
   end put;
/* Voeg de string toe aan een regel en schrijf deze regel naar het output */
PROCEDURE PUT_LINE
 (P_STRING IN varchar2 := null
 )
 IS
   /******************************************************************************
      NAAM:       put_line
      DOEL:       Voeg de string toe aan een regel en schrijf deze regel naar het
                  output medium (scherm/bestand)

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          11-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type   := cn_package || '.PUT_LINE';
      l_line               varchar2 (2000);
begin
      l_line := g_string || p_string;

      if g_output_type = cn_output_screen
      then
         dbms_output.put_line (substr (l_line, 1, 255) );
      elsif g_output_type = cn_output_file
      then
         utl_file.put_line (g_bsd, l_line);
      else
         raise e_invalid_output_type;
      end if;

      g_string := null;
   end put_line;
/* schrijf de inhoud van een veld naar het output medium */
PROCEDURE PRINT
 (P_VELDNR IN number
 ,P_WAARDE IN date
 )
 IS
   /******************************************************************************
      NAAM:       print
      DOEL:       schrijf de inhoud van een veld, rekening houdend met de initialisatie
                  instellingen, naar het output medium

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          11-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type   := cn_package || '.PRINT';
      l_waarde             date;
      ls_waarde            varchar2 (255);
      l_stmt               varchar2 (255);
begin
      l_waarde := p_waarde;

      if g_bsd_layout (p_veldnr).veldweergave = '*'
      then
         ls_waarde := lpad ('*', nvl (g_bsd_layout (p_veldnr).veldlengte, 1), '*');
      elsif     g_bsd_layout (p_veldnr).veldtype = 'D'
            and g_bsd_layout (p_veldnr).veldweergave is not null
      then
         ls_waarde := to_char (l_waarde, g_bsd_layout (p_veldnr).veldweergave);
      else
         ls_waarde := l_waarde;
      end if;

      if p_veldnr = i_veldnr
      then
         put_line (l_waarde);
      else
         put (l_waarde || cn_sep);
      end if;
   end print;
/* schrijf de inhoud van een veld naar het output medium */
PROCEDURE PRINT
 (P_VELDNR IN number
 ,P_WAARDE IN varchar2
 )
 IS
   /******************************************************************************
      NAAM:       print
      DOEL:       schrijf de inhoud van een veld, rekening houdend met de initialisatie
                  instellingen, naar het output medium

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          11-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type   := cn_package || '.PRINT';
      l_waarde             varchar2 (255);
      ld_waarde            date;
      ln_waarde            number;
      ls_waarde            varchar2 (255);
      l_stmt               varchar2 (255);
begin
      l_waarde := p_waarde;

      if g_bsd_layout (p_veldnr).veldweergave = '*'
      then
         l_waarde := lpad ('*', nvl (g_bsd_layout (p_veldnr).veldlengte, 1), '*');
      elsif g_bsd_layout (p_veldnr).veldtype in ('A', 'N')
      then
         if g_bsd_layout (p_veldnr).veldweergave is not null
         then
            l_stmt := 'select ' || replace (g_bsd_layout (p_veldnr).veldweergave, '#', ':b1') || ' from dual';

            execute immediate l_stmt
            into              l_waarde
            using             l_waarde;
         end if;
      end if;

      if p_veldnr = i_veldnr
      then
         put_line (l_waarde);
      else
         put (l_waarde || cn_sep);
      end if;
   end print;
/* Het bepalen/overrulen van de */
PROCEDURE BEPAAL_PARAMETERS
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,O_VORIGE_PEILDATUM IN OUT date
 ,O_VORIGE_RUNDATUM IN OUT date
 ,O_RUNDATUM IN OUT date
 )
 IS
   /******************************************************************************
      NAAM:       bepaal_parameters
      DOEL:       Het bepalen/overrulen van de

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
begin
      select db_directory
      into   g_directory
      from   rle_bestanden
      where  naam = p_bsd_naam;

      o_rundatum := sysdate;

      if p_vorige_peildatum is null
      then
         begin
            select tot_datum
                  ,starttijd_uitvoer
            into   o_vorige_peildatum
                  ,o_vorige_rundatum
            from   rle_uitvoer_statussen
            where  (bsd_naam, runnummer) in (select   bsd_naam
                                                     ,max (runnummer)
                                             from     rle_uitvoer_statussen
                                             where    bsd_naam = p_bsd_naam
                                             and      tot_datum < p_peildatum
                                             group by bsd_naam);
         exception
            when no_data_found
            then
               o_vorige_peildatum := cn_initiele_datum;
               o_vorige_rundatum := o_rundatum;
         end;
      else
         o_vorige_peildatum := p_vorige_peildatum;

         begin
            select starttijd_uitvoer
            into   o_vorige_rundatum
            from   rle_uitvoer_statussen
            where  (bsd_naam, runnummer) in (select   bsd_naam
                                                     ,max (runnummer)
                                             from     rle_uitvoer_statussen
                                             where    bsd_naam = p_bsd_naam
                                             and      tot_datum = p_vorige_peildatum
                                             group by bsd_naam);
         exception
            when no_data_found
            then
               o_vorige_peildatum := p_vorige_peildatum;
               o_vorige_rundatum := o_rundatum;
         end;
      end if;
   end bepaal_parameters;
/* Het opslaan van de gegevens van de zojuist aangemaakte bestanden */
PROCEDURE OPSLAAN_UITVOER_GEGEVENS
 (P_RUNNUMMER IN rle_uitvoer_statussen.runnummer%type
 ,P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_VANAF_DATUM IN rle_uitvoer_statussen.vanaf_datum%type
 ,P_TOT_DATUM IN rle_uitvoer_statussen.tot_datum%type
 ,P_STARTTIJD_UITVOER IN rle_uitvoer_statussen.starttijd_uitvoer%type
 ,P_BESTANDSNAAM IN rle_uitvoer_statussen.bestandsnaam%type
 )
 IS
   /******************************************************************************
      NAAM:       opslaan_uitvoer_gegevens
      DOEL:       Het opslaan van de gegevens van de zojuist aangemaakte bestanden

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          25-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
begin
   if p_vanaf_datum = cn_initiele_datum
   then
      delete      rle_uitvoer_statussen uss
      where       uss.bsd_naam = p_bsd_naam;
   else
      delete      rle_uitvoer_statussen uss
      where       uss.bsd_naam = p_bsd_naam
      and         uss.tot_datum > p_tot_datum;
   end if;

      merge into rle_uitvoer_statussen uss
         using (select p_runnummer runnummer
                      ,p_bsd_naam bsd_naam
                      ,p_vanaf_datum vanaf_datum
                      ,p_tot_datum tot_datum
                      ,p_starttijd_uitvoer starttijd_uitvoer
                      ,sysdate eindtijd_uitvoer
                      ,p_bestandsnaam bestandsnaam
                      ,g_directory db_directory_uitvoer
                from   dual) d
         on (    uss.bsd_naam = d.bsd_naam
             and uss.vanaf_datum = d.vanaf_datum)
         when matched then
            update
               set runnummer = d.runnummer, tot_datum = d.tot_datum, starttijd_uitvoer = d.starttijd_uitvoer
                  ,eindtijd_uitvoer = d.eindtijd_uitvoer, bestandsnaam = d.bestandsnaam
                  ,db_directory_uitvoer = d.db_directory_uitvoer
         when not matched then
            insert (runnummer, bsd_naam, vanaf_datum, tot_datum, starttijd_uitvoer, eindtijd_uitvoer, bestandsnaam
                   ,db_directory_uitvoer)
            values (d.runnummer, d.bsd_naam, d.vanaf_datum, d.tot_datum, d.starttijd_uitvoer, d.eindtijd_uitvoer
                   ,d.bestandsnaam, d.db_directory_uitvoer);
   end opslaan_uitvoer_gegevens;
/* Controle of een werkgevers na de initiële datum op enig moment */
FUNCTION WGR_IS_OTIB
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_WGR_NR IN wgr_werkgevers.nr_werkgever%type
 ,O_WGR_NR IN OUT wgr_werkgevers.nr_werkgever%type
 )
 RETURN BOOLEAN
 IS
   /******************************************************************************
      NAAM:       wgr_is_otib
      DOEL:       Controle of een werkgevers na de initiële datum op enig moment
                  bij OTIB was/is aangesloten.

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cursor c_otib (
         b_wgr_nr   in   wgr_werkgevers.nr_werkgever%type)
      is
         select wgr.nr_werkgever werkgevernummer
         from   wgr_werkgevers wgr
               ,wgr_werkzaamheden wwg
               ,wsf wsf
         where  g_use_access_path_wsf = cn_true
         and    wsf.kodefon in (select code_abonnee
                                from   rle_bestandsabonnementen
                                where  bsd_naam = p_bsd_naam)
         and    wwg.wgr_id = wgr.id
         and    wwg.code_groep_werkzhd = wsf.kodegvw
         and    nvl (wwg.code_subgroep_werkzhd, cn_at) = nvl (wsf.kodesgw, cn_at)
         and    nvl (wwg.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    nvl (wsf.dateind, cn_initiele_datum) >= cn_initiele_datum
         and    nvl (wgr.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    wgr.nr_werkgever = b_wgr_nr
         union
         select wgr.nr_werkgever
         from   cvc cvc
               ,verzekeringspakketten vzp
               ,wgr_werkgevers wgr
         where  g_use_access_path_rwg = 'true'
         and    vzp.fon_kodefon in (select code_abonnee
                                    from   rle_bestandsabonnementen
                                    where  bsd_naam = p_bsd_naam)
         and    vzp.verzekeringspakketnummer = cvc.vzp_nummer
         and    lpad (wgr.nr_werkgever, 6, 0) = cvc.wgr_werkgnr
         and    nvl (cvc.dateind, cn_initiele_datum) >= cn_initiele_datum
         and    wgr.nr_werkgever = b_wgr_nr
         /* union
         select rec.extern_relatie
         from   rle_relatierollen_in_adm rie
               ,rle_relatie_rollen rrl
               ,rle_relatie_externe_codes rec
         where  g_use_access_path_rle = cn_true
         and    rie.adm_code in (select code_abonnee
                                 from   rle_bestandsabonnementen
                                 where  bsd_naam = p_bsd_naam)
         and    rie.rol_codrol = rrl.rol_codrol
         and    rrl.rle_numrelatie = rec.rle_numrelatie
         and    rec.extern_relatie = lpad (b_wgr_nr, 6, 0) */
		 ;

      l_return_value   boolean;
begin
      open c_otib (p_wgr_nr);

      fetch c_otib
      into  o_wgr_nr;

      l_return_value := c_otib%found;

      close c_otib;

      return l_return_value;
   end wgr_is_otib;
FUNCTION WGR_IS_OTIB
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_RLE_NR IN rle_relaties.numrelatie%type
 ,O_WGR_NR IN OUT wgr_werkgevers.nr_werkgever%type
 )
 RETURN BOOLEAN
 IS
   /******************************************************************************
      NAAM:       wgr_is_otib
      DOEL:       Controle of een werkgevers na de initiële datum op enig moment
                  bij OTIB was/is aangesloten.

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cursor c_otib (
         b_rle_nr   in   rle_relaties.numrelatie%type)
      is
         select wgr.nr_werkgever werkgevernummer
         from   wgr_werkgevers wgr
               ,wgr_werkzaamheden wwg
               ,wsf wsf
               ,rle_relatie_externe_codes rec
         where  g_use_access_path_wsf = cn_true
         and    wsf.kodefon in (select code_abonnee
                                from   rle_bestandsabonnementen
                                where  bsd_naam = p_bsd_naam)
         and    wwg.wgr_id = wgr.id
         and    wwg.code_groep_werkzhd = wsf.kodegvw
         and    nvl (wwg.code_subgroep_werkzhd, cn_at) = nvl (wsf.kodesgw, cn_at)
         and    nvl (wwg.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    nvl (wsf.dateind, cn_initiele_datum) >= cn_initiele_datum
         and    nvl (wgr.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    lpad (wgr.nr_werkgever, 6, 0) = rec.extern_relatie
         and    rec.dwe_wrddom_extsys = cn_wgr
         and    rec.rol_codrol = cn_wg_rol
         and    rec.rle_numrelatie = b_rle_nr
         union
         select wgr.nr_werkgever
         from   cvc cvc
               ,verzekeringspakketten vzp
               ,wgr_werkgevers wgr
               ,rle_relatie_externe_codes rec
         where  g_use_access_path_rwg = 'true'
         and    vzp.fon_kodefon in (select code_abonnee
                                    from   rle_bestandsabonnementen
                                    where  bsd_naam = p_bsd_naam)
         and    vzp.verzekeringspakketnummer = cvc.vzp_nummer
         and    lpad (wgr.nr_werkgever, 6, 0) = cvc.wgr_werkgnr
         and    nvl (cvc.dateind, cn_initiele_datum) >= cn_initiele_datum
         and    lpad (wgr.nr_werkgever, 6, 0) = rec.extern_relatie
         and    rec.dwe_wrddom_extsys = cn_wgr
         and    rec.rol_codrol = cn_wg_rol
         and    rec.rle_numrelatie = b_rle_nr
         /* union
         select wgr.nr_werkgever
         from   rle_relatierollen_in_adm rie
               ,rle_relatie_rollen rrl
               ,rle_relatie_externe_codes rec
               ,wgr_werkgevers wgr
         where  g_use_access_path_rle = cn_true
         and    rie.adm_code in (select code_abonnee
                                 from   rle_bestandsabonnementen
                                 where  bsd_naam = p_bsd_naam)
         and    rie.rol_codrol = rrl.rol_codrol
         and    rrl.rle_numrelatie = rec.rle_numrelatie
         and    rec.extern_relatie = lpad (wgr.nr_werkgever, 6, 0)
         and    rec.rle_numrelatie = b_rle_nr */
		 ;

      l_return_value   boolean;
begin
      open c_otib (p_rle_nr);

      fetch c_otib
      into  o_wgr_nr;

      l_return_value := c_otib%found;

      close c_otib;

      return l_return_value;
   end wgr_is_otib;
/* Controle of een werkgevers na de initiële datum op enig moment */
FUNCTION WGR_IS_OTIB
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_WGR_ID IN wgr_werkgevers.id%type
 ,O_WGR_NR IN OUT wgr_werkgevers.nr_werkgever%type
 )
 RETURN BOOLEAN
 IS
   /******************************************************************************
      NAAM:       wgr_is_otib
      DOEL:       Controle of een werkgevers na de initiële datum op enig moment
                  bij OTIB was/is aangesloten.

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
      cursor c_otib (
         b_wgr_id   in   wgr_werkgevers.id%type)
      is
         select wgr.nr_werkgever werkgevernummer
         from   wgr_werkgevers wgr
               ,wgr_werkzaamheden wwg
               ,wsf wsf
         where  g_use_access_path_wsf = cn_true
         and    wsf.kodefon in (select code_abonnee
                                from   rle_bestandsabonnementen
                                where  bsd_naam = p_bsd_naam)
         and    wwg.wgr_id = wgr.id
         and    wwg.code_groep_werkzhd = wsf.kodegvw
         and    nvl (wwg.code_subgroep_werkzhd, cn_at) = nvl (wsf.kodesgw, cn_at)
         and    nvl (wwg.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    nvl (wsf.dateind, cn_initiele_datum) >= cn_initiele_datum
         and    nvl (wgr.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    wgr.id = b_wgr_id
         union
         select wgr.nr_werkgever
         from   cvc cvc
               ,verzekeringspakketten vzp
               ,wgr_werkgevers wgr
         where  g_use_access_path_rwg = cn_true
         and    vzp.fon_kodefon in (select code_abonnee
                                    from   rle_bestandsabonnementen
                                    where  bsd_naam = p_bsd_naam)
         and    vzp.verzekeringspakketnummer = cvc.vzp_nummer
         and    lpad (wgr.nr_werkgever, 6, 0) = cvc.wgr_werkgnr
         and    nvl (cvc.dateind, cn_initiele_datum) >= cn_initiele_datum
         and    wgr.id = b_wgr_id
         /* union
         select wgr.nr_werkgever
         from   rle_relatierollen_in_adm rie
               ,rle_relatie_rollen rrl
               ,rle_relatie_externe_codes rec
               ,wgr_werkgevers wgr
         where  g_use_access_path_rle = cn_true
         and    rie.adm_code in (select code_abonnee
                                 from   rle_bestandsabonnementen
                                 where  bsd_naam = p_bsd_naam)
         and    rie.rol_codrol = rrl.rol_codrol
         and    rrl.rle_numrelatie = rec.rle_numrelatie
         and    rec.extern_relatie = lpad (wgr.nr_werkgever, 6, 0)
         and    wgr.id = b_wgr_id */
		 ;

      l_return_value   boolean;
begin
      open c_otib (p_wgr_id);

      fetch c_otib
      into  o_wgr_nr;

      l_return_value := c_otib%found;

      close c_otib;

      return l_return_value;
   end wgr_is_otib;
/* Vullen tijdelijke werkgever tabel */
PROCEDURE AANMAKEN_TMP_WGR
 (P_BSD_NAAM IN rle_bestanden.naam%type
 )
 IS
/******************************************************************************
      NAAM:       aanmaken_tmp_wgr
      DOEL:       Vullen tijdelijke werkgever tabel (ivm performance)

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          06-03-2008  R.Podt      XRP  Aanmaak routine
   ******************************************************************************/
begin
      delete      rle_tmp_actieve_wgr;

      insert into rle_tmp_actieve_wgr
                  (werkgevernummer
                  ,code_functiecategorie
                  ,dataanv
                  ,dateind)
         (                    /* Werkgevers worden middels een subquery bepaald wegen performance overwegingen
             ** Het betreft hier alle werkgevers die na de initiële datum op enig moment
             ** bij OTIB waren/zijn aangesloten.
             */
          select distinct nvl (wsf.wgr_werkgnr, cvc.wgr_werkgnr) werkgevernummer
                         ,nvl (wsf.code_functiecategorie, cvc.code_functiecategorie) code_functiecategorie
                         ,greatest (nvl (wsf.dataanv, cvc.dataanv), nvl (cvc.dataanv, wsf.dataanv) ) dataanv
                         ,least (nvl (wsf.dateind, cvc.dateind), nvl (cvc.dateind, wsf.dateind) ) dateind
          from            (select lpad (wgr.nr_werkgever, 6, 0) wgr_werkgnr
                                 ,cn_fc_beambte_of_handarbeider code_functiecategorie
                                 ,greatest (wwg.dat_begin, cn_initiele_datum) dataanv
                                 ,wwg.dat_einde dateind
                           from   wgr_werkgevers wgr
                                 ,wgr_werkzaamheden wwg
                                 ,wsf wsf
                                 ,rle_bestandsabonnementen bst
                           where  g_use_access_path_wsf = 'true'
                           and    wsf.kodefon = bst.code_abonnee
                           and    bst.bsd_naam = p_bsd_naam
                           and    wwg.wgr_id = wgr.id
                           and    wwg.code_groep_werkzhd = wsf.kodegvw
                           and    nvl (wwg.code_subgroep_werkzhd, cn_at) = nvl (wsf.kodesgw, cn_at)
                           and    nvl (wwg.dat_einde, cn_initiele_datum) >= cn_initiele_datum
                           and    nvl (wsf.dateind, cn_initiele_datum) >= cn_initiele_datum
                           and    nvl (wgr.dat_einde, cn_initiele_datum) >= cn_initiele_datum) wsf
                          full outer join
                          (select distinct nvl (h.wgr_werkgnr, b.wgr_werkgnr) wgr_werkgnr
                                          , h.code_functiecategorie || b.code_functiecategorie code_functiecategorie
                                          ,greatest (nvl (h.dataanv, b.dataanv)
                                                    ,nvl (b.dataanv, h.dataanv)
                                                    ,cn_initiele_datum) dataanv
                                          ,least (nvl (h.dateind, b.dateind), nvl (b.dateind, h.dateind) ) dateind
                           from            (select cvc.wgr_werkgnr
                                                  ,cvc.code_functiecategorie
                                                  ,cvc.dataanv
                                                  ,cvc.dateind
                                            from   cvc cvc
                                                  ,verzekeringspakketten vzp
                                                  ,rle_bestandsabonnementen bst
                                            where  g_use_access_path_rwg = cn_true
                                            and    cvc.code_functiecategorie = cn_fc_handarbeider
                                            and    nvl (cvc.dateind, cn_initiele_datum) >= cn_initiele_datum
                                            and    cvc.vzp_nummer = vzp.verzekeringspakketnummer
                                            and    vzp.fon_kodefon = bst.code_abonnee
                                            and    bst.bsd_naam = p_bsd_naam) h
                                           full outer join
                                           (select cvc.wgr_werkgnr
                                                  ,cvc.code_functiecategorie
                                                  ,cvc.dataanv
                                                  ,cvc.dateind
                                            from   cvc cvc
                                                  ,verzekeringspakketten vzp
                                                  ,rle_bestandsabonnementen bst
                                            where  g_use_access_path_rwg = cn_true
                                            and    cvc.code_functiecategorie = cn_fc_beambte
                                            and    nvl (cvc.dateind, cn_initiele_datum) >= cn_initiele_datum
                                            and    cvc.vzp_nummer = vzp.verzekeringspakketnummer
                                            and    vzp.fon_kodefon = bst.code_abonnee
                                            and    bst.bsd_naam = p_bsd_naam) b
                                           on (    h.wgr_werkgnr = b.wgr_werkgnr
                                               and h.dataanv < nvl (b.dateind, h.dataanv + 1)
                                               and b.dataanv < nvl (h.dateind, b.dataanv + 1) )
                                           ) cvc
                          on (    cvc.wgr_werkgnr = wsf.wgr_werkgnr
                              and cvc.dataanv < nvl (wsf.dateind, cvc.dataanv + 1)
                              and wsf.dataanv < nvl (cvc.dateind, wsf.dataanv + 1) )
                          );
   end aanmaken_tmp_wgr;
/* aanmaken van delete opdracht records voor het wgr bestand */
PROCEDURE AANMAKEN_WGR_DEL_RECORDS
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_VORIGE_RUN_DATUM IN date
 ,P_AANTAL_WGR_DEL OUT NUMBER
 )
 IS
      /******************************************************************************
         NAAM:       aanmaken_wgr_del_records
         DOEL:       aanmaken van delete opdracht records voor het wgr bestand

         REVISIES:
         Versie     Datum       Auteur           Beschrijving
         ---------  ----------  ---------------  ------------------------------------
         1          21-02-2008  R.Podt      XRP  Aanmaak routine
      ******************************************************************************/
      cursor c_wgr_del001 (
         b_vorige_run_datum   in   date)
      is
         select cn_delete  operatie
               ,old.nr_werkgever oude_wgr_nr
         from   wgr_werkgevers new
               ,wgr_werkgevers_jn old
         where  old.id = new.id   /* Primary key is het zelfde gebleven, maar UK is gewijzigd */
         and    old.nr_werkgever <> new.nr_werkgever
         and    new.dat_creatie < b_vorige_run_datum
         and    new.dat_mutatie >= b_vorige_run_datum
         and    (old.id, old.jn_date_time) in (select   old2.id
                                                       ,max (old2.jn_date_time)
                                               from     wgr_werkgevers_jn old2
                                               where    old.id = old2.id
                                               and      jn_date_time < b_vorige_run_datum
                                               group by old2.id)
         union
         select cn_delete
               ,nr_werkgever
         from   wgr_werkgevers_jn
         where  jn_date_time >= b_vorige_run_datum
         and    jn_operation = cn_op_del;

      cursor c_rvm_del001 (
         b_vorige_run_datum   in   date)
      is
         select distinct cn_delete operatie
                        ,wgr_id wgr_id
                        ,greatest (dat_begin, cn_initiele_datum) oude_ingangsdatum
         from            wgr_rechtsvormen_jn
         where           nvl (dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and             jn_date_time >= b_vorige_run_datum
         and             jn_operation = cn_op_del;

      cursor c_wzd_del001 (
         b_vorige_run_datum   in   date)
      is
         select distinct cn_delete  operatie
                        ,wgr_id wgr_id
                        ,greatest (dat_begin, cn_initiele_datum) oude_ingangsdatum
         from            wgr_werkzaamheden_jn wwg
         where           nvl (dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and             jn_date_time >= b_vorige_run_datum
         and             jn_operation = cn_op_del;

      cursor c_wsf_del001 (
         b_vorige_run_datum   in   date)
      is
         select distinct cn_delete  operatie
                        ,wwg.wgr_id
                        ,greatest (wsf.dataanv, cn_initiele_datum) oude_ingangsdatum
         from            wsf_h wsf
                        ,wgr_werkzaamheden_jn wwg
         where           wsf.kodefon in (select code_abonnee
                                         from   rle_bestandsabonnementen
                                         where  bsd_naam = p_bsd_naam)
         and             wwg.code_groep_werkzhd = wsf.kodegvw
         and             nvl (wwg.code_subgroep_werkzhd, cn_at) = nvl (wsf.kodesgw, cn_at)
         and             wsf.registratiedatum >= b_vorige_run_datum
         and             nvl (wsf.dateind, cn_initiele_datum) >= cn_initiele_datum
         and             wsf.ind_vervallen = 'J';

      cursor c_cvc_del001 (
         b_vorige_run_datum   in   date)
      is
         select distinct cn_delete  operatie
                        ,to_number (cvc.wgr_werkgnr) oude_wgr_nr
                        ,greatest (cvc.dataanv, cn_initiele_datum) oude_ingangsdatum
         from            cvc_h cvc
                        ,verzekeringspakketten vzp
         where           vzp.fon_kodefon in (select code_abonnee
                                             from   rle_bestandsabonnementen
                                             where  bsd_naam = p_bsd_naam)
         and             vzp.verzekeringspakketnummer = cvc.vzp_nummer
         and             cvc.registratiedatum >= b_vorige_run_datum
         and             nvl (cvc.dateind, cn_initiele_datum) >= cn_initiele_datum
         and             cvc.ind_vervallen = 'J';

      cursor c_rle_del001 (
         b_vorige_run_datum   in   date)
      is
         select cn_delete  operatie
               ,old.numrelatie
               ,greatest (old.datbegin, cn_initiele_datum) oude_ingangsdatum
         from   rle_relaties new
               ,rle_relaties_jn old
               ,rle_relatie_externe_codes rec
         where  old.numrelatie = new.numrelatie   /* Primary key is het zelfde gebleven, maar UK is gewijzigd */
         and    old.datbegin <> new.datbegin
         and    new.dat_creatie < b_vorige_run_datum
         and    new.dat_mutatie >= b_vorige_run_datum
         and    nvl (old.dateinde, cn_initiele_datum) >= cn_initiele_datum
         and    rec.dwe_wrddom_extsys = cn_wgr
         and    rec.rol_codrol = cn_wg_rol
         and    rec.rle_numrelatie = new.numrelatie
         and    (old.numrelatie, old.jn_date_time) in (
                                            select   old2.numrelatie
                                                    ,max (old2.jn_date_time)
                                            from     rle_relaties_jn old2
                                            where    old.numrelatie = old2.numrelatie
                                            and      jn_date_time < b_vorige_run_datum
                                            group by old2.numrelatie)
         union
         select cn_delete
               ,numrelatie
               ,greatest (datbegin, cn_initiele_datum) oude_ingangsdatum
         from   rle_relaties_jn
         where  jn_date_time >= b_vorige_run_datum
         and    jn_operation = cn_op_del
         and    nvl (dateinde, cn_initiele_datum) >= cn_initiele_datum;

          cursor c_ras_del001 (
         b_vorige_run_datum   in   date)
      is
         select cn_delete operatie
               ,old.rle_numrelatie
               ,greatest (old.datingang, cn_initiele_datum) oude_ingangsdatum
         from   rle_relatie_adressen new
               ,rle_relatie_adressen_jn old
               ,rle_relatie_externe_codes rec
         where  old.id = new.id   /* Primary key is het zelfde gebleven, maar UK is gewijzigd */
         and    old.rle_numrelatie || cn_sep || old.rol_codrol || cn_sep || old.datingang <>
                                               new.rle_numrelatie || cn_sep || new.rol_codrol || cn_sep || new.datingang
         and    new.dat_creatie < b_vorige_run_datum
         and    new.dat_mutatie >= b_vorige_run_datum
         and    nvl (old.dateinde, cn_initiele_datum) >= cn_initiele_datum
         and    rec.dwe_wrddom_extsys = cn_wgr
         and    rec.rol_codrol = cn_wg_rol
         and    rec.rle_numrelatie = new.rle_numrelatie
         and    (old.id, old.jn_date_time) in (select   old2.id
                                                       ,max (old2.jn_date_time)
                                               from     rle_relatie_adressen_jn old2
                                               where    old.id = old2.id
                                               and      jn_date_time < b_vorige_run_datum
                                               group by old2.id)
         union
         select cn_delete
               ,rle_numrelatie
               ,greatest (datingang, cn_initiele_datum) oude_ingangsdatum
         from   rle_relatie_adressen_jn
         where  jn_date_time >= b_vorige_run_datum
         and    jn_operation = cn_op_del
         and    nvl (dateinde, cn_initiele_datum) >= cn_initiele_datum;

      l_wgr_nr   wgr_werkgevers.nr_werkgever%type;
      l_aantal_wgr_del     number  := 0;
begin
      --
      for r_wgr in c_wgr_del001 (p_vorige_run_datum)
      loop
         if wgr_is_otib (p_bsd_naam => p_bsd_naam, p_wgr_nr => r_wgr.oude_wgr_nr, o_wgr_nr => l_wgr_nr)
         then
            l_aantal_wgr_del := l_aantal_wgr_del + 1;
            put_line (lpad(l_wgr_nr, 6, 0) || cn_sep || r_wgr.operatie);
         end if;
      end loop;

      for r_rvm in c_rvm_del001 (p_vorige_run_datum)
      loop
         if wgr_is_otib (p_bsd_naam => p_bsd_naam, p_wgr_id => r_rvm.wgr_id, o_wgr_nr => l_wgr_nr)
         then
            l_aantal_wgr_del := l_aantal_wgr_del + 1;
            put_line (lpad(l_wgr_nr, 6, 0) || cn_sep || r_rvm.operatie || cn_sep || r_rvm.oude_ingangsdatum);
         end if;
      end loop;

      for r_wzd in c_wzd_del001 (p_vorige_run_datum)
      loop
         if wgr_is_otib (p_bsd_naam => p_bsd_naam, p_wgr_id => r_wzd.wgr_id, o_wgr_nr => l_wgr_nr)
         then
             l_aantal_wgr_del := l_aantal_wgr_del + 1;
            put_line (lpad(l_wgr_nr, 6, 0) || cn_sep || r_wzd.operatie || cn_sep || r_wzd.oude_ingangsdatum);
         end if;
      end loop;

      for r_wsf in c_wsf_del001 (p_vorige_run_datum)
      loop
         if wgr_is_otib (p_bsd_naam => p_bsd_naam, p_wgr_id => r_wsf.wgr_id, o_wgr_nr => l_wgr_nr)
         then
             l_aantal_wgr_del := l_aantal_wgr_del + 1;
           put_line (lpad(l_wgr_nr, 6, 0) || cn_sep || r_wsf.operatie || cn_sep || r_wsf.oude_ingangsdatum);
         end if;
      end loop;

      for r_cvc in c_cvc_del001 (p_vorige_run_datum)
      loop
         if wgr_is_otib (p_bsd_naam => p_bsd_naam, p_wgr_nr => r_cvc.oude_wgr_nr, o_wgr_nr => l_wgr_nr)
         then
            l_aantal_wgr_del := l_aantal_wgr_del + 1;
            put_line (lpad(l_wgr_nr, 6, 0) || cn_sep || r_cvc.operatie || cn_sep || r_cvc.oude_ingangsdatum);
         end if;
      end loop;

      for r_rle in c_rle_del001 (p_vorige_run_datum)
      loop
         if wgr_is_otib (p_bsd_naam => p_bsd_naam, p_rle_nr => r_rle.numrelatie, o_wgr_nr => l_wgr_nr)
         then
             l_aantal_wgr_del := l_aantal_wgr_del + 1;
            put_line (lpad(l_wgr_nr, 6, 0) || cn_sep || r_rle.operatie || cn_sep || r_rle.oude_ingangsdatum);
         end if;
      end loop;

      for r_ras in c_ras_del001 (p_vorige_run_datum)
      loop
         if wgr_is_otib (p_bsd_naam => p_bsd_naam, p_rle_nr => r_ras.rle_numrelatie, o_wgr_nr => l_wgr_nr)
         then
             l_aantal_wgr_del := l_aantal_wgr_del + 1;
            put_line (lpad(l_wgr_nr, 6, 0) || cn_sep || r_ras.operatie || cn_sep || r_ras.oude_ingangsdatum);
         end if;
      end loop;
   --
   p_aantal_wgr_del := l_aantal_wgr_del;
   end aanmaken_wgr_del_records;
/* aanmaken van delete opdracht records voor het wnr bestand */
PROCEDURE AANMAKEN_WNR_DEL_RECORDS
 (P_VORIGE_RUN_DATUM IN date
 ,P_AANTAL_WNR_DEL OUT NUMBER
 )
 IS
      /******************************************************************************
          NAAM:       aanmaken_wgr_del_records
          DOEL:       aanmaken van delete opdracht records voor het wgr bestand

          REVISIES:
          Versie     Datum       Auteur           Beschrijving
          ---------  ----------  ---------------  ------------------------------------
          1          21-02-2008  R.Podt      XRP  Aanmaak routine
          2          06-03-2008  R.Podt      XRP  Routine uitgebreid met wni, wnu en wbp
         3          22-06-2009  MAL                     Records buiten idmaatschap OTIB uitgezonderd

       ******************************************************************************/
     cursor c_wni_del001 (
         b_vorige_run_datum   in   date)
      is
         select cn_delete operatie
               ,avg.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_werknemer_inkomens new
               ,avg_werknemer_inkomens_jn old
               ,avg_arbeidsverhoudingen avg
         where  old.id = new.id   /* Primary key is het zelfde gebleven, maar UK is gewijzigd */
         and    new.avh_id = avg.id
         and    old.avh_id || cn_sep || old.code_inkomenscomponent || cn_sep || old.dat_begin <>
                                           new.avh_id || cn_sep || new.code_inkomenscomponent || cn_sep || new.dat_begin
         and    new.dat_creatie < b_vorige_run_datum
         and    new.dat_mutatie >= b_vorige_run_datum
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    old.code_inkomenscomponent in ('PROV', 'SALI', 'UUR')
         and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = avg.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1) )
         and    (old.id, old.jn_date_time) in (select   old2.id
                                                       ,max (old2.jn_date_time)
                                               from     avg_werknemer_inkomens_jn old2
                                               where    old.id = old2.id
                                               and      jn_date_time < b_vorige_run_datum
                                               group by old2.id)
         union
         select cn_delete operatie
               ,avg.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_werknemer_inkomens_jn old
               ,avg_arbeidsverhoudingen_jn avg
         where  old.jn_date_time >= b_vorige_run_datum
         and    old.avh_id = avg.id
         and    old.code_inkomenscomponent in ('PROV', 'SALI', 'UUR')
         and    old.jn_operation = cn_op_del
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
        and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = avg.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1) );

      cursor c_wnu_del001 (
         b_vorige_run_datum   in   date)
      is
         select cn_delete operatie
               ,avg.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_werknemer_uren new
               ,avg_werknemer_uren_jn old
               ,avg_arbeidsverhoudingen avg
         where  old.id = new.id   /* Primary key is het zelfde gebleven, maar UK is gewijzigd */
         and    new.avh_id = avg.id
         and    old.avh_id || cn_sep || old.dat_begin <> new.avh_id || cn_sep || new.dat_begin
         and    new.dat_creatie < b_vorige_run_datum
         and    new.dat_mutatie >= b_vorige_run_datum
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = avg.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1) )
         and    (old.id, old.jn_date_time) in (select   old2.id
                                                       ,max (old2.jn_date_time)
                                               from     avg_werknemer_uren_jn old2
                                               where    old.id = old2.id
                                               and      jn_date_time < b_vorige_run_datum
                                               group by old2.id)
         union
         select cn_delete operatie
               ,avg.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_werknemer_uren_jn old
               ,avg_arbeidsverhoudingen_jn avg
         where  old.jn_date_time >= b_vorige_run_datum
         and    old.avh_id = avg.id
          and    old.jn_operation = cn_op_del
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = avg.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1) );

      cursor c_wbp_del001 (
         b_vorige_run_datum   in   date)
      is
         select cn_delete operatie
               ,avg.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_werknemer_beroepen new
               ,avg_werknemer_beroepen_jn old
               ,avg_arbeidsverhoudingen avg
          where  old.id = new.id   /* Primary key is het zelfde gebleven, maar UK is gewijzigd */
         and    new.avg_id = avg.id
         and    old.avg_id || cn_sep || old.dat_begin <> new.avg_id || cn_sep || new.dat_begin
         and    new.dat_creatie < b_vorige_run_datum
         and    new.dat_mutatie >= b_vorige_run_datum
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = avg.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1) )
         and    (old.id, old.jn_date_time) in (select   old2.id
                                                       ,max (old2.jn_date_time)
                                               from     avg_werknemer_beroepen_jn old2
                                               where    old.id = old2.id
                                               and      jn_date_time < b_vorige_run_datum
                                               group by old2.id)
         union all
         select cn_delete operatie
               ,avg.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_werknemer_beroepen_jn old
               ,avg_arbeidsverhoudingen_jn avg
              ,avg_arbeidsverhoudingen  avg2
         where  old.jn_date_time >= b_vorige_run_datum
         and    old.avg_id = avg.id
         and    avg2.id = avg.id
         and    old.jn_operation = cn_op_del
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = avg.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1) );

      cursor c_avg_del001 (
         b_vorige_run_datum   in   date)
      is
         select cn_delete operatie
               ,old.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_arbeidsverhoudingen new
               ,avg_arbeidsverhoudingen_jn old
         where  old.id = new.id   /* Primary key is het zelfde gebleven, maar UK is gewijzigd */
        and    old.werknemer || cn_sep || old.dat_begin <> new.werknemer || cn_sep || new.dat_begin
         and    new.dat_creatie < b_vorige_run_datum
         and    new.dat_mutatie >= b_vorige_run_datum
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = old.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1) )
         and    (old.id, old.jn_date_time) in (select   old2.id
                                                       ,max (old2.jn_date_time)
                                               from     avg_arbeidsverhoudingen_jn old2
                                               where    old.id = old2.id
                                               and      jn_date_time < b_vorige_run_datum
                                               group by old2.id)
         union all
         select cn_delete operatie
               ,old.werknemer oude_werknemernummer
               ,greatest (old.dat_begin, cn_initiele_datum) oude_ingangsdatum
         from   avg_arbeidsverhoudingen_jn old
               ,avg_arbeidsverhoudingen avg
         where  old.jn_date_time >= b_vorige_run_datum
         and    avg.id = old.id
         and    old.jn_operation = cn_op_del
         and    nvl (old.dat_einde, cn_initiele_datum) >= cn_initiele_datum
         and    exists (select 1
                        from   rle_tmp_actieve_wgr tmp
                        where  tmp.werkgevernummer = old.werkgever
                        and    old.dat_begin >= tmp.dataanv
                        and    old.dat_begin < nvl (tmp.dateind, sysdate + 1));

l_aantal_wnr_del   number := 0;
begin

      -- Werknemer inkomsten
      for r_wni in c_wni_del001 (p_vorige_run_datum)
      loop
         l_aantal_wnr_del := l_aantal_wnr_del + 1;
         put_line (r_wni.oude_werknemernummer || cn_sep || r_wni.operatie || cn_sep || r_wni.oude_ingangsdatum);
      end loop;

      -- Werknemer uren
      for r_wnu in c_wnu_del001 (p_vorige_run_datum)
      loop
         l_aantal_wnr_del := l_aantal_wnr_del + 1;
         put_line (r_wnu.oude_werknemernummer || cn_sep || r_wnu.operatie || cn_sep || r_wnu.oude_ingangsdatum);
      end loop;

      -- Werknemer beroepen
      for r_wbp in c_wbp_del001 (p_vorige_run_datum)
      loop
         l_aantal_wnr_del := l_aantal_wnr_del + 1;
         put_line (r_wbp.oude_werknemernummer || cn_sep || r_wbp.operatie || cn_sep || r_wbp.oude_ingangsdatum);
      end loop;

      -- Arbeidsverhoudingen
      for r_avg in c_avg_del001 (p_vorige_run_datum)
      loop
        l_aantal_wnr_del := l_aantal_wnr_del + 1;
         put_line (r_avg.oude_werknemernummer || cn_sep || r_avg.operatie || cn_sep || r_avg.oude_ingangsdatum);
      end loop;

      p_aantal_wnr_del := l_aantal_wnr_del;

   end aanmaken_wnr_del_records;
/* Aanmaken van het OTIB werkgeverbestand */
FUNCTION AANMAKEN_WGR_BSD
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,P_VORIGE_RUNDATUM IN date
 )
 RETURN RLE_UITVOER_STATUSSEN.BESTANDSNAAM%TYPE
 IS
      /**********************************************************************************
         NAAM:       aanmaken_wgr_bsd
         DOEL:       aanmaken van het werkgeverbestand tbv OTIB

         REVISIES:
         Versie     Datum       Auteur           Beschrijving
         ---------  ----------  ---------------  ------------------------------------
         1          11-02-2008  R.Podt      XRP  Aanmaak routine
         2          13-01-2009  R.Podt      XRP  Cursor c_wgr001 gewijzigd;
                                                 1) rechtsvorm optioneel
                                                 2) geldigheidsperiode ext.code is verwijderd
                                                 3) nvl om handelsnaam
         3          09-06-2009  XJB              Cursor c_wgr001 gewijzigd:
                                                 1) Rechtsvorm is nu volledig optioneel
         4         09-07-2009  MAL  cursor c_wgr001 gewijzigd:
                                                      meerdere relatiekoppelingen worden getoond en
                                                     rechtsvoorganger+afsplitsing naar toegevoegd
         5        26-02-2010  mal  Voorloopnullen veld 24 (overname bedrijf) toegevoegd
         6        26-10-2010 XKV r_adr initialiseren ivm INC-200508
      ************************************************************************************/
      cn_module   constant stm_util.t_procedure%type                 := cn_package || '.AANMAKEN_WGR_BSD';
      l_aantal_wnr         number;
      l_rowcount           number                                    := 0;
      l_bestandsnaam       rle_uitvoer_statussen.bestandsnaam%type;

      /* Cursor voor het ophalen van de werkgevergegevens */
      cursor c_wgr001 (
         b_peildatum          in   date
        ,b_vorige_peildatum   in   date)
      is
        with wsf_wgr_set as                    -- xjb, 09-06-2009 voorwaartse declaratie van de relevante werkzaamheid combos
                  (select wsf3.kodegvw
                         ,wsf3.kodesgw
                         ,wsf3.kodefon
                         ,wwg.wgr_id
                         ,greatest (wsf3.dataanv, wwg.dat_begin) dataanv
                         ,case
                             when least (nvl (wwg.dat_einde, cn_verre_toekomst), nvl (wsf3.dateind, cn_verre_toekomst) ) =
                                                                                                       cn_verre_toekomst
                                then to_date (null)
                             else least (nvl (wwg.dat_einde, cn_verre_toekomst), nvl (wsf3.dateind, cn_verre_toekomst) )
                          end dateind
                         ,wwg.code_reden_einde
                   from   wsf wsf3
                         ,wgr_werkzaamheden wwg
                         ,rle_bestandsabonnementen bst
                   where  g_use_access_path_wsf = cn_true
                   and    wsf3.kodefon = bst.code_abonnee
                   and    bst.bsd_naam = p_bsd_naam
                   and    wwg.code_groep_werkzhd = wsf3.kodegvw
                   and    nvl (wwg.code_subgroep_werkzhd, cn_at) = nvl (wsf3.kodesgw, cn_at)
                   and    wsf3.dataanv <= nvl (wwg.dat_einde, wsf3.dataanv)
                   and    wwg.dat_begin <= nvl (wsf3.dateind, wwg.dat_begin)
                   --
                   union   --union (ipv union all) is noodzakelijk ivm cvc.code_functiecategorie
                   --
                   select '-' kodegvw
                         ,'-' kodesgw
                         ,vzp.fon_kodefon kodefon
                         ,wgr.id wgr_id
                         ,cvc.dataanv dataanv
                         ,nvl (cvc.dateind, vzp.einddatum) dateind
                         ,null code_reden_einde
                   from   cvc cvc
                         ,verzekeringspakketten vzp
                         ,wgr_werkgevers wgr
                         ,rle_bestandsabonnementen bst
                   where  g_use_access_path_rwg = cn_true
                   and    vzp.fon_kodefon = bst.code_abonnee
                   and    bst.bsd_naam = p_bsd_naam
                   and    vzp.verzekeringspakketnummer = cvc.vzp_nummer
                   and    lpad (wgr.nr_werkgever, 6, 0) = cvc.wgr_werkgnr
                  )
         select   lpad (wgr.nr_werkgever, 6, 0) veld01_reg_nr
                 ,greatest (cn_initiele_datum, wsf2.dataanv, wgr.rvm_dat_begin, ras.datingang, rle.datbegin)
                                                                                                    veld20_ingangsdatum
                 ,case
                     when least (nvl (wsf2.dateind, cn_verre_toekomst)
                                ,nvl (ras.dateinde, cn_verre_toekomst)
                                ,nvl (rle.dateinde, cn_verre_toekomst)
                                ,nvl (wgr.rvm_dat_einde, cn_verre_toekomst)) = cn_verre_toekomst
                        then to_date (null)
                     else least (nvl (wsf2.dateind, cn_verre_toekomst)
                                ,nvl (ras.dateinde, cn_verre_toekomst)
                                ,nvl (rle.dateinde, cn_verre_toekomst)
                                ,nvl (wgr.rvm_dat_einde, cn_verre_toekomst))
                  end veld21_einddatum
                 ,nvl (rle.handelsnaam, rle.namrelatie) veld02_handelsnaam
                 ,stt.straatnaam veld03_straat
                 ,adr.huisnummer veld04_huisnr
                 ,adr.toevhuisnum veld05_toev_huisnr
                 ,wps.woonplaats veld06_plaats
                 ,case
                     when adr.lnd_codland = cn_nederland
                        then substr (adr.postcode, 1, 4) || cn_spatie || substr (adr.postcode, 5)
                     else adr.postcode
                  end veld07_postcode
                 ,case   /* Let op, werksfeer OLC is niet afgesloten wanneer werksfeer OTIB_L is opgevoerd */
                     when wsf2.kodefon = cn_olc
                        then cn_otib_l
                     when wsf2.kodefon = cn_sko
                        then cn_otib_k
                     when wsf2.kodefon = cn_ofe
                        then cn_otib_e
                     else wsf2.kodefon
                  end veld13_fonds
                 ,wsf2.kodegvw veld14_gvw
                 ,wsf2.kodesgw veld15_sgw
                 ,wgr.code_rechtsvorm veld16_rechtsvorm
                 ,greatest (wsf2.dataanv, cn_initiele_datum) veld17_ing_dat_lid
                 ,wsf2.dateind veld18_eind_dat_lid
                 ,cn_gegeven_niet_beschikbaar veld22_heffing_betaald
                 ,wsf2.dateind veld23_eind_dat_lid2
                 ,wsf2.code_reden_einde veld24_reden_einde
                ,lpad(ovn.ovn_overnamebedrijf, 6, 0)  ovn_overnamebedrijf
               ,ovn.ovn_rkn_dat_begin
               ,rle.numrelatie rle_numrelatie
         from      wsf_wgr_set wsf2                           -- xjb, 09-06-2009 gebruik nu de voorw. gedecl. set
                 , (select wgr.id
                          ,wgr.nr_werkgever
                          ,wgr.dat_mutatie  wgr_dat_mutatie
                          ,rvm.dat_begin    rvm_dat_begin
                          ,rvm.dat_einde    rvm_dat_einde
                          ,rvm.dat_mutatie  rvm_dat_mutatie
                          ,rvm.code_rechtsvorm
                    from   wgr_werkgevers wgr
                          ,wgr_rechtsvormen rvm
                    where  wgr.id = rvm.wgr_id
                    union all                                 -- xjb, 09-06-2009
                    select wgr3.id                            -- simuleer de ontbrekende rvm records: anders vallen
                    ,      wgr3.nr_werkgever                  -- werkzaamheid periodes ten onrechte uit de hoofd query
                    ,      wgr3.dat_mutatie  wgr_dat_mutatie
                    ,      wsf4.dataanv      rvm_dat_begin
                    ,      wsf4.dateind      rvm_dat_einde
                    ,      NULL              rvm_dat_mutatie
                    ,      NULL              code_rechtsvorm
                    from   wgr_werkgevers wgr3
                    ,      wsf_wgr_set    wsf4
                    where  wgr3.id = wsf4.wgr_id
                    and not exists (select 1
                                    from   wgr_rechtsvormen rvm3
                                    where  rvm3.wgr_id = wgr3.id
                                    and    rvm3.dat_begin <= nvl(wsf4.dateind, rvm3.dat_begin)
                                   )
                   ) wgr
                 , (select lpad(wgr2.nr_werkgever, 6, 0)  ovn_overnamebedrijf
                    ,      rkn.rle_numrelatie         ovn_rle_numrelatie
                    ,      rkn.dat_begin                  ovn_rkn_dat_begin
                    ,      rkn.dat_einde                  ovn_rkn_dat_einde
                    from   rle_relatie_koppelingen rkn
                          ,rle_rol_in_koppelingen rkg
                          ,rle_relaties rle2
                          ,wgr_werkgevers wgr2
                    where  rkn.rkg_id = rkg.id
                    and    rle2.numrelatie = rkn.rle_numrelatie_voor
                    and    wgr2.nr_relatie = rle2.numrelatie
                    and    rkg.code_rol_in_koppeling in ('FUST', 'OVND', 'AFSN', 'RVGR')   /* Fusie, Overname, Afslitsing of Rechtsvoorganger */
                   ) ovn
                 ,rle_relatie_externe_codes rec
                 ,rle_relaties rle
                 ,rle_relatie_adressen ras
                 ,rle_adressen adr
                 ,rle_straten stt
                 ,rle_woonplaatsen wps
         where    wsf2.wgr_id = wgr.id
         --
         and      lpad (wgr.nr_werkgever, 6, 0) = rec.extern_relatie
         and      rec.dwe_wrddom_extsys = cn_wgr
         and      rec.rol_codrol = cn_wg_rol
         --
         and      rec.rle_numrelatie = rle.numrelatie
         --
         and      rec.rle_numrelatie = ras.rle_numrelatie
         --
         and      ovn.ovn_rle_numrelatie (+) = rle.numrelatie
         --
         and      ras.dwe_wrddom_srtadr = cn_stat_zetel
         and      ras.ads_numadres = adr.numadres
         --
         and      adr.stt_straatid = stt.straatid
         --
         and      adr.wps_woonplaatsid = wps.woonplaatsid
         --
         and      wsf2.dataanv <= nvl (rle.dateinde, wsf2.dataanv)
         and      wsf2.dataanv <= nvl (ras.dateinde, wsf2.dataanv)
         and      wsf2.dataanv <= nvl (wgr.rvm_dat_einde, wsf2.dataanv)
         --
         and      ras.datingang <= nvl (rle.dateinde, ras.datingang)
         and      ras.datingang <= nvl (wsf2.dateind, ras.datingang)
         and      ras.datingang <= nvl (wgr.rvm_dat_einde, ras.datingang)
         --
         and      rle.datbegin <= nvl (wsf2.dateind, rle.datbegin)
         and      rle.datbegin <= nvl (ras.dateinde, rle.datbegin)
         and      rle.datbegin <= nvl (wgr.rvm_dat_einde, rle.datbegin)
         --
         and      wgr.rvm_dat_begin <= nvl (wsf2.dateind, wgr.rvm_dat_begin)
         and      wgr.rvm_dat_begin <= nvl (rle.dateinde, wgr.rvm_dat_begin)
         and      wgr.rvm_dat_begin <= nvl (ras.dateinde, wgr.rvm_dat_begin)
         --
         and      nvl (wsf2.dateind, cn_initiele_datum) >= cn_initiele_datum
         and      nvl (rle.dateinde, cn_initiele_datum) >= cn_initiele_datum
         and      nvl (ras.dateinde, cn_initiele_datum) >= cn_initiele_datum
         and      nvl (wgr.rvm_dat_einde, cn_initiele_datum) >= cn_initiele_datum
         --
         and      (   /* Gegevens tussen :b_vorige_peildatum en :b_peildatum geldig (geweest) */
                      (    b_peildatum >= wgr.rvm_dat_begin
                       and nvl (wgr.rvm_dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= wsf2.dataanv
                       and nvl (wsf2.dateind, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= rle.datbegin
                       and nvl (rle.dateinde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= ras.datingang
                       and nvl (ras.dateinde, b_peildatum) >= b_vorige_peildatum)
                   or   /*Of... gegevens afgelopen maand gemuteerd */
                      (   wgr.wgr_dat_mutatie >= b_vorige_peildatum   -- Werkgever gemuteerd
                       or wgr.rvm_dat_mutatie >= b_vorige_peildatum   -- Rechtsvorm gemuteerd
                       or rle.dat_mutatie >= b_vorige_peildatum   -- Relatie gemuteerd
                       or ras.dat_mutatie >= b_vorige_peildatum   -- Statutaire Zetel gemuteerd
                       or adr.dat_mutatie >= b_vorige_peildatum   -- Statutaire Zetel gemuteerd
                       or exists (select 1   -- Werkzaamheid gemuteerd
                                  from   wgr_werkzaamheden wwg
                                  where  wwg.wgr_id = wgr.id
                                  and    wwg.dat_mutatie >= b_vorige_peildatum)
                       or exists (   -- Werksfeer gemuteerd
                             select 1
                             from   wsf_h wsfh
                             where  wsf2.kodefon = wsfh.kodefon
                             and    wsf2.dataanv = wsfh.dataanv
                             and    wsf2.kodegvw = wsfh.kodegvw
                             and    wsf2.kodesgw = wsfh.kodesgw
                             and    wsfh.registratiedatum >= b_vorige_peildatum)
                       or exists (   -- Correspondentie adres gemuteerd
                             select 1
                             from   rle_adressen adr
                                   ,rle_relatie_adressen ras
                             where  adr.numadres = ras.ads_numadres
                             and    rle.numrelatie = ras.rle_numrelatie
                             and    ras.dwe_wrddom_srtadr = cn_corr_adres
                             and    adr.dat_mutatie >= b_vorige_peildatum
                             and    ras.dat_mutatie >= b_vorige_peildatum) ) )
         order by wgr.nr_werkgever asc
                 ,greatest (cn_initiele_datum, wsf2.dataanv, wgr.rvm_dat_begin, ras.datingang, rle.datbegin) asc;


     r_adr                c_adr001%rowtype;
     l_aantal_wgr_del     number;
     l_ovn_wgr            number;
begin
      stm_util.module_start (substr (cn_module, instr (cn_module, '.') + 1) );
      init_wgr;

      if i_veldnr > 0
      then
         for z in 1 .. i_veldnr
         loop
            if z = i_veldnr
            then
               put (g_bsd_layout (z).veldnaam);
            else
               put (g_bsd_layout (z).veldnaam || cn_sep);
            end if;
         end loop;

         put_line;
         aanmaken_wgr_del_records (p_bsd_naam => p_bsd_naam, p_vorige_run_datum => p_vorige_rundatum, p_aantal_wgr_del => l_aantal_wgr_del);

         for r_wgr in c_wgr001 (p_peildatum, p_vorige_peildatum)
         loop
            l_rowcount := l_rowcount + 1;

              if nvl(r_wgr.veld21_einddatum, to_date ('01-01-2999', 'dd-mm-yyyy')) + 1 >= r_wgr.ovn_rkn_dat_begin
                   and r_wgr.veld17_ing_dat_lid < r_wgr.ovn_rkn_dat_begin
              then
                l_ovn_wgr := r_wgr.ovn_overnamebedrijf;
              else
                l_ovn_wgr := null;
              end if;

            r_adr:=null;
            open c_adr001 (r_wgr.rle_numrelatie, p_peildatum, p_vorige_peildatum);

            fetch c_adr001
            into  r_adr;

            close c_adr001;

            select count (1)
            into   l_aantal_wnr
            from   avg_arbeidsverhoudingen avg
            where  avg.werkgever = r_wgr.veld01_reg_nr
            and    r_wgr.veld20_ingangsdatum between avg.dat_begin and nvl (avg.dat_einde, r_wgr.veld20_ingangsdatum);


            /* Let op: Pas ook routine init_wgr aan bij aanpassing van de
            ** volgnummers van de velden!
            */

            print (1, r_wgr.veld01_reg_nr);
            print (2, cn_merge);
            print (3, r_wgr.veld20_ingangsdatum);
            print (4, r_wgr.veld21_einddatum);
            print (5, r_wgr.veld02_handelsnaam);
            print (6, r_wgr.veld16_rechtsvorm);
            print (7, r_wgr.veld17_ing_dat_lid);
            print (8, r_wgr.veld18_eind_dat_lid);
            print (9, r_wgr.veld13_fonds);
            print (10, r_wgr.veld14_gvw);
            print (11, r_wgr.veld15_sgw);
            print (12, r_wgr.veld03_straat);
            print (13, r_wgr.veld04_huisnr);
            print (14, r_wgr.veld05_toev_huisnr);
            print (15, r_wgr.veld06_plaats);
            print (16, r_wgr.veld07_postcode);
            print (17, r_adr.straatnaam);
            print (18, r_adr.huisnummer);
            print (19, r_adr.toevhuisnum);
            print (20, r_adr.woonplaats);
            print (21, r_adr.postcode);
            print (22, r_wgr.veld23_eind_dat_lid2);
            print (23, r_wgr.veld24_reden_einde);
            print (24, l_ovn_wgr);
            print (25, l_aantal_wnr);

            --print (22, r_wgr.veld22_heffing_betaald);

         end loop;

         l_bestandsnaam :=
            sluit_bestand (p_bsd_naam =>              p_bsd_naam
                          ,p_peildatum =>             p_peildatum
                          ,p_vorige_peildatum =>      p_vorige_peildatum
                          ,p_rowcount =>              l_rowcount + l_aantal_wgr_del);
      end if;

      utl_file.fclose_all;
      stm_util.module_end;
      return l_bestandsnaam;
   end aanmaken_wgr_bsd;
/* Aanmaken van het OTIB werknemerbestand */
FUNCTION AANMAKEN_WNR_BSD
 (P_BSD_NAAM IN rle_bestanden.naam%type
 ,P_PEILDATUM IN date
 ,P_VORIGE_PEILDATUM IN date
 ,P_VORIGE_RUNDATUM IN date
 )
 RETURN RLE_UITVOER_STATUSSEN.BESTANDSNAAM%TYPE
 IS
      /******************************************************************************
         NAAM:       aanmaken_wnr_bsd
         DOEL:       Aanmaken van het OTIB werknemerbestand

         REVISIES:
         Versie     Datum       Auteur           Beschrijving
         ---------  ----------  ---------------  ------------------------------------
         1          11-02-2008  R.Podt      XRP  Aanmaak routine
         2          13-01-2009  R.Podt      XRP  Cursor c_wnr001 gewijzigd;
                                                 1) geldigheidsperiode ext.code is verwijderd
        3          22-06-2009  monique v alphen     provisie en oproepkrachten meegenomen
        4          26-02-2010  monique v alphen    sofinummer optioneel gemaakt
      ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type                 := cn_package || '.AANMAKEN_WNR_BSD';
      l_bestandsnaam       rle_uitvoer_statussen.bestandsnaam%type;

      /* ........... */
      cursor c_wnr001 (
         b_peildatum          in   date
        ,b_vorige_peildatum   in   date)
      is
         select   avg.werkgever werkgevernummer
                 ,avg.werknemer persoonsnummer
                 ,greatest (wni.dat_begin, wnu.dat_begin, wbp.dat_begin, wgr2.dataanv, cn_initiele_datum) ingangsdatum
                 ,case
                     when least (nvl (wni.dat_einde, cn_verre_toekomst)
                                ,nvl (wnu.dat_einde, cn_verre_toekomst)
                                ,nvl (wbp.dat_einde, cn_verre_toekomst)
                                ,nvl (wgr2.dateind, cn_verre_toekomst) ) = cn_verre_toekomst
                        then to_date (null)
                     else least (nvl (wni.dat_einde, cn_verre_toekomst)
                                ,nvl (wnu.dat_einde, cn_verre_toekomst)
                                ,nvl (wbp.dat_einde, cn_verre_toekomst)
                                ,nvl (wgr2.dateind, cn_verre_toekomst) )
                  end einddatum
                 ,brp.code_beroep brp_code_beroep
                 ,wni.bedrag_basisvaluta bedrag
                 ,wni.code_frequentie freq_uitbetaling
                 ,wnu.uren_vastgesteld uren
                 ,rle.datgeboorte geboortedatum
                 ,rle.dwe_wrddom_geslacht geslacht
                 ,rle.namrelatie naam
                 ,rle.voorletter voorletters
                 ,bsn.extern_relatie sofinummer
                 ,rle.datoverlijden overlijdensdatum
                 ,rle.dwe_wrddom_vvgsl voorvoegsels
                 ,rle.naam_partner naam_echtgenoot
                 ,rle.vvgsl_partner voorvoegsels_echtgenoot
                 ,avg.dat_begin avg_ingangsdatum
                 ,avg.dat_einde avg_einddatum
                 ,avg.reden_einde
                 ,brp.code_functiecategorie code_functiecategorie
                 ,rle.numrelatie
         from     avg_arbeidsverhoudingen avg
                 ,avg_werknemer_inkomens wni
                 ,avg_werknemer_beroepen wbp
                 ,avg_werknemer_uren wnu
                 ,beroepen brp
                 ,rle_relaties rle
                 , (select rle_numrelatie
                          ,extern_relatie
                          ,dat_mutatie
                    from   rle_relatie_externe_codes
                    where  rol_codrol = cn_psn_rol
                    and    dwe_wrddom_extsys = cn_psn_nr) rec   --relatie --> persoon
                 , (select rle_numrelatie
                          ,extern_relatie
                          ,dat_mutatie
                    from   rle_relatie_externe_codes
                    where  rol_codrol = cn_psn_rol
                    and    dwe_wrddom_extsys = cn_bsn_nr) bsn   --burgerservicenummer
                 ,rle_tmp_actieve_wgr wgr2
         where    avg.werkgever = wgr2.werkgevernummer
         and      instr (wgr2.code_functiecategorie, brp.code_functiecategorie) > 0
         --
         and      rec.extern_relatie = avg.werknemer
         --
         and      rec.rle_numrelatie = rle.numrelatie
         --
         and      rec.rle_numrelatie = bsn.rle_numrelatie (+)
         --
         and      avg.id = wni.avh_id
         and      wni.code_inkomenscomponent in (cn_sal, 'PROV', 'UUR')
         --
         and      avg.id = wbp.avg_id
         --
         and      avg.id = wnu.avh_id
         --
         and      avg.id = wbp.avg_id
         --
         and      wbp.code_beroep = brp.code_beroep
         --
         and      wnu.dat_begin <= nvl (wni.dat_einde, cn_verre_toekomst)
         and      wnu.dat_begin <= nvl (wbp.dat_einde, cn_verre_toekomst)
         and      wnu.dat_begin <= nvl (wgr2.dateind, cn_verre_toekomst)
         --
         and      wni.dat_begin <= nvl (wbp.dat_einde, cn_verre_toekomst)
         and      wni.dat_begin <= nvl (wnu.dat_einde, cn_verre_toekomst)
         and      wni.dat_begin <= nvl (wgr2.dateind, cn_verre_toekomst)
         --
         and      wbp.dat_begin <= nvl (wni.dat_einde, cn_verre_toekomst)
         and      wbp.dat_begin <= nvl (wnu.dat_einde, cn_verre_toekomst)
         and      wbp.dat_begin <= nvl (wgr2.dateind, cn_verre_toekomst)
         --
         and      wgr2.dataanv <= nvl (wni.dat_einde, cn_verre_toekomst)
         and      wgr2.dataanv <= nvl (wbp.dat_einde, cn_verre_toekomst)
         and      wgr2.dataanv <= nvl (wnu.dat_einde, cn_verre_toekomst)
         --
         and      least (nvl (wni.dat_einde, cn_verre_toekomst)
                        ,nvl (wnu.dat_einde, cn_verre_toekomst)
                        ,nvl (wbp.dat_einde, cn_verre_toekomst)
                        ,nvl (wgr2.dateind, cn_verre_toekomst) ) >= cn_initiele_datum
         --
         and      (   -- Gegevens tussen :b_vorige_peildatum en :b_peildatum geldig (geweest)
                      (    b_peildatum >= wni.dat_begin
                       and nvl (wni.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= wnu.dat_begin
                       and nvl (wnu.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= wbp.dat_begin
                       and nvl (wbp.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= avg.dat_begin
                       and nvl (avg.dat_einde, b_peildatum) >= b_vorige_peildatum
                       and b_peildatum >= wgr2.dataanv
                       and nvl (wgr2.dateind, b_peildatum) >= b_vorige_peildatum)
                   or   -- Of... gegevens afgelopen maand gemuteerd
                      (   avg.dat_mutatie >= b_vorige_peildatum   -- Arbeidsrelatie gemuteerd
                       or wni.dat_mutatie >= b_vorige_peildatum   -- Werknemer inkomens gemuteerd
                       or wnu.dat_mutatie >= b_vorige_peildatum   -- Werknemer uren gemuteerd
                       or wbp.dat_mutatie >= b_vorige_peildatum   -- Werknemer beroepen gemuteerd
                       or rec.dat_mutatie >= b_vorige_peildatum   -- Relatie externe code gemuteerd
                       or rle.dat_mutatie >= b_vorige_peildatum   -- Relatie gemuteerd
                       or bsn.dat_mutatie >= b_vorige_peildatum   -- BSN gemuteerd
                       or exists (   /* Opgegeven adres gemuteerd */
                             select 1
                             from   rle_adressen adr
                                   ,rle_relatie_adressen ras
                             where  adr.numadres = ras.ads_numadres
                             and    ras.rle_numrelatie = rle.numrelatie
                             and    ras.dwe_wrddom_srtadr = cn_opg_adres
                             and    (   adr.dat_mutatie >= b_vorige_peildatum   -- Adres gemuteerd
                                     or ras.dat_mutatie >= b_vorige_peildatum)   -- Relatie adres gemuteerd
                                                                              ) ) )
         order by avg.werkgever asc
                 ,avg.werknemer asc
                 ,greatest (wni.dat_begin, wnu.dat_begin, wbp.dat_begin, wgr2.dataanv, cn_initiele_datum) asc;


      r_adr                c_adr001%rowtype;
      l_rowcount           number                                    := 0;
      l_aantal_wnr_del     number;
begin
      stm_util.module_start (substr (cn_module, instr (cn_module, '.') + 1) );
      init_wnr;

      if i_veldnr > 0
      then
         for z in 1 .. i_veldnr
         loop
            if z = i_veldnr
            then
               put (g_bsd_layout (z).veldnaam);
            else
               put (g_bsd_layout (z).veldnaam || cn_sep);
            end if;
         end loop;

         put_line;
         aanmaken_tmp_wgr (p_bsd_naam => p_bsd_naam);
         aanmaken_wnr_del_records (p_vorige_run_datum => p_vorige_rundatum, p_aantal_wnr_del => l_aantal_wnr_del);

         /* Geldige werkgevers of werkgevers waarvan de werksfeer gewijzigd is */
         for r_wnr in c_wnr001 (p_peildatum, p_vorige_peildatum)
         loop
            open c_adr001 (r_wnr.numrelatie, p_peildatum, p_vorige_peildatum);

            fetch c_adr001
            into  r_adr;

            close c_adr001;

            l_rowcount := l_rowcount + 1;

            /* Let op: Pas ook routine init_wnr aan bij aanpassing van de
            ** volgnummers van de velden!
            */
            print (1, r_wnr.persoonsnummer);
            print (2, cn_merge);
            print (3, r_wnr.ingangsdatum);
            print (4, r_wnr.einddatum);
            print (5, r_wnr.werkgevernummer);
            print (6, r_wnr.sofinummer);
            print (7, r_wnr.naam);
            print (8, r_wnr.voorvoegsels);
            print (9, r_wnr.voorletters);
            print (10, r_wnr.naam_echtgenoot);
            print (11, r_wnr.voorvoegsels_echtgenoot);
            print (12, r_wnr.geboortedatum);
            print (13, r_wnr.overlijdensdatum);
            print (14, r_wnr.geslacht);
            print (15, r_adr.straatnaam);
            print (16, r_adr.huisnummer);
            print (17, r_adr.toevhuisnum);
            print (18, r_adr.woonplaats);
            print (19, r_adr.postcode);
            print (20, r_adr.land);
            print (21, r_wnr.avg_ingangsdatum);
            print (22, r_wnr.avg_einddatum);
            print (23, r_wnr.reden_einde);
            print (24, r_wnr.uren);
            print (25, r_wnr.bedrag);
            print (26, r_wnr.freq_uitbetaling);
            print (27, r_wnr.code_functiecategorie);
            print (28, r_wnr.brp_code_beroep);

         end loop;

         l_bestandsnaam :=
            sluit_bestand (p_bsd_naam =>              p_bsd_naam
                          ,p_peildatum =>             p_peildatum
                          ,p_vorige_peildatum =>      p_vorige_peildatum
                          ,p_rowcount =>              l_rowcount+ l_aantal_wnr_del);
      end if;

      utl_file.fclose_all;
      stm_util.module_end;
      return l_bestandsnaam;
   end aanmaken_wnr_bsd;
/* starten van de batch verwerking voor OTIB */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN pls_integer
 ,P_BESTANDSGROEP IN VARCHAR2 := 'OTIB'
 ,P_PEILDATUM IN DATE := trunc(sysdate,'MON')
 ,P_VORIGE_PEILDATUM IN DATE := to_date(null)
 )
 IS
   /******************************************************************************
      NAAM:       verwerk
      DOEL:       starten van de batch verwerking voor OTIB

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          21-02-2008  R.Podt      XRP  Aanmaak routine
      1.1        01-04-2010  XCW              Herstart + STM Lijsten toegevoegd
   ******************************************************************************/
      cn_module   constant stm_util.t_procedure%type                 := cn_package || '.VERWERK';
      l_volgnummer         stm_job_statussen.volgnummer%type         := 1;
      l_vorige_peildatum   date;
      l_vorige_rundatum    date;
      l_rundatum           date;
      l_bestandsnaam       rle_uitvoer_statussen.bestandsnaam%type;
      -- xtra var voor stm_lijsten
      l_herstartprogramma      varchar2(100);
      l_regelnr                number(10,0) := 0; -- Regelnummer voor stm_lijsten
      l_lijstnummer            number:=1;
      --l_volgnummer             number := 0;
      l_foutomgeving           varchar2(2000);
      l_foutmelding            varchar2(2000);
      l_proc_naam              varchar2(25) := cn_module;
begin
      l_foutomgeving := '001 - ' || l_proc_naam || ' - gestart om : ' || to_char( sysdate, 'DD-MM-YYYY HH24:MI:SS');
      --
      l_herstartprogramma := null;
      --
      stm_util.t_programma_naam := substr (cn_module, 1, instr (cn_module, '.') - 1);
      stm_util.module_start (substr (cn_module, instr (cn_module, '.') + 1) );
      stm_util.t_script_naam := p_script_naam;
      stm_util.t_script_id := to_number (p_script_id);
      stm_util.t_tekst := null;
      --
      if stm_util.herstart
      then
        --
        -- ophalen herstart-gegevens en herstart in LIJSTEN tabel zetten
        --
        l_herstartprogramma := stm_util.lees_herstartsleutel( 'L_PROC_NAAM' );
        l_regelnr := stm_util.lees_herstarttelling( 'L_REGELNR' );
        --
        l_foutomgeving := '010 - ' || l_proc_naam || ' - In Herstart van procedure '
                                   || stm_util.t_programma_naam || ' sleutel=' || l_herstartprogramma
                                   || ' l_regelnr=' || to_char( l_regelnr );
         stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                                , il_regelnummer =>     l_regelnr
                                , il_tekst =>           'Herstart van procedure ' || stm_util.t_programma_naam || ' op '
                                                                                  || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss'   )
                                 );
      else
        --
        l_regelnr := 1;
        --
        l_foutomgeving :=
         '020 - '
         || l_proc_naam
         || ' - Nieuwe start '
         || stm_util.t_programma_naam
         || ' Parameter P_BESTANDSGROEP : '
         || P_BESTANDSGROEP
         || ' Parameter P_PEILDATUM: '
         || to_char( P_PEILDATUM )
         || ' Parameter P_VORIGE_PEILDATUM: '
         || to_char( P_VORIGE_PEILDATUM );

        stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                             , il_regelnummer =>     l_regelnr
                             , il_tekst =>           'Start ' || stm_util.t_programma_naam || ' op ' || to_char( sysdate
                                                                                                             , 'dd-mm-yyyy hh24:mi:ss'
                                                                                                             )
                           );
        stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                           , il_regelnummer =>     l_regelnr
                           , il_tekst =>           'Parameter P_BESTANDSGROEP    : ' || P_BESTANDSGROEP
                           );
        stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                           , il_regelnummer =>     l_regelnr
                           , il_tekst =>           'Parameter P_PEILDATUM        : ' || to_char( P_PEILDATUM )
                           );
        stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                           , il_regelnummer =>     l_regelnr
                           , il_tekst =>           'Parameter P_VORIGE_PEILDATUM : ' || to_char( P_VORIGE_PEILDATUM )
                           );
      end if;
      --
      for r_bsd in c_bsd001(b_bestandsgroep => p_bestandsgroep)
      loop
        --
        stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
                  , il_regelnummer =>     l_regelnr
                  , il_tekst =>           'Start met bestand ' || r_bsd.naam || ' op ' || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss' )
                  );
        --
        bepaal_parameters (p_bsd_naam =>              r_bsd.naam
                           ,p_peildatum =>             p_peildatum
                           ,p_vorige_peildatum =>      p_vorige_peildatum
                           ,o_vorige_peildatum =>      l_vorige_peildatum
                           ,o_vorige_rundatum =>       l_vorige_rundatum
                           ,o_rundatum =>              l_rundatum);

         if r_bsd.bsd_type = cn_wgr
         then
            l_bestandsnaam :=
               aanmaken_wgr_bsd (p_bsd_naam =>              r_bsd.naam
                                ,p_peildatum =>             p_peildatum
                                ,p_vorige_peildatum =>      l_vorige_peildatum
                                ,p_vorige_rundatum =>       l_vorige_rundatum);
         elsif r_bsd.bsd_type = cn_wnr
         then
            l_bestandsnaam :=
               aanmaken_wnr_bsd (p_bsd_naam =>              r_bsd.naam
                                ,p_peildatum =>             p_peildatum
                                ,p_vorige_peildatum =>      l_vorige_peildatum
                                ,p_vorige_rundatum =>       l_vorige_rundatum);
         end if;

         opslaan_uitvoer_gegevens (p_runnummer =>              p_script_id
                                  ,p_bsd_naam =>               r_bsd.naam
                                  ,p_vanaf_datum =>            l_vorige_peildatum
                                  ,p_tot_datum =>              p_peildatum
                                  ,p_starttijd_uitvoer =>      l_rundatum
                                  ,p_bestandsnaam =>           l_bestandsnaam);
      end loop;
      --
      stm_util.insert_lijsten( il_lijstnummer =>     l_lijstnummer
              , il_regelnummer =>     l_regelnr
              , il_tekst =>  'Einde ' || stm_util.t_programma_naam
                            || ' op ' || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss')
              );
      --
      l_foutomgeving := '900 - ' || l_proc_naam || ' - Voor verwijder herstart ';
      stm_util.verwijder_herstart;
      --
      stm_util.insert_job_statussen (l_volgnummer, 'S', '0');
      stm_util.module_end;
      commit;
      --
      l_foutomgeving := '999 - ' || l_proc_naam || ' - geeindigd om : ' || to_char( sysdate, 'DD-MM-YYYY HH24:MI:SS');
   exception
      when others
      then
         if g_raise_exception_on_error
         then
            raise_application_error (-20000
                                    , stm_get_batch_message (sqlerrm) || chr (10) || dbms_utility.format_error_backtrace);
         else
            rollback;
            stm_util.t_foutmelding := sqlerrm;
            stm_util.debug ('Foutmelding  : ' || stm_util.t_foutmelding);
            stm_util.debug ('In programma : ' || stm_util.t_programma_naam);
            stm_util.debug ('In procedure : ' || stm_util.t_procedure);
            --
            stm_util.insert_job_statussen (l_volgnummer, 'S', '1');
            stm_util.insert_job_statussen (l_volgnummer
                                          ,'I'
                                          , stm_util.t_programma_naam || ' procedure : ' || stm_util.t_procedure);

            --
            if stm_util.t_foutmelding is not null
            then
               stm_util.insert_job_statussen (l_volgnummer, 'I', stm_util.t_foutmelding);
            end if;

            --
            if stm_util.t_tekst is not null
            then
               stm_util.insert_job_statussen (l_volgnummer, 'I', stm_util.t_tekst);
            end if;

            --
            commit;
         end if;
   end verwerk;

END RLE_MAAK_BSD;
/