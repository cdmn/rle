CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_CONTROLE_RKN_GBA IS

/**************************************************************************************

   DOEL:  Vanuit de import van de relatiekoppelingen van TNT / GBA de verschillen
          bekijken met de relatie koppelingen in RLE.

   FO:    RLE1020 Aanmaken verschillen verslag relatie koppelingen

   WIJZIGINGSHISTORIE:
   Datum            Wie                     Wat
   --------------   ---------------------   ------------------------------------
   10-04-2014       H. Kulker               Creatie

  ****************************************************************************************/

  cn_package constant varchar2 ( 80 ) := 'RLE_CONTROLE_RKN_GBA';
  cn_datum_tijd_formaat constant varchar2 ( 80 ) := 'dd-mm-yyyy hh24:mi';
  cn_module constant stm_util.t_procedure%type := cn_package || '.VERWERK';

  -- voor stm lijsten
  g_regelnummer pls_integer := 1;
  -- aantal relatiekoppelingen die overeenkomen
  g_aantal_rle_koppl_ov      pls_integer := 0;
  -- aantal relatiekoppelingen die afwijken
  g_aantal_rle_koppl_af      pls_integer := 0;
  -- afwijken niet anwezig in gba
  g_aantal_rle_koppl_na_gba  pls_integer := 0;
  -- afwijken niet anwezig in gba
  g_aantal_rle_koppl_na_rle  pls_integer := 0;
  -- bovenstaande telling is dus zonder het aantal rel koppl uit gba/tnt waar we de hele bsn/prs niet kennen in rle
  --
  g_aantal_bsn_ng            pls_integer := 0;
  --
  g_bsn_herstart                varchar2(20) := 0;
  cn_bsn                        constant varchar2(30) := 'BSN';
  cn_regelnummer                constant varchar2(30) := 'REGELNUMMER';
  cn_aantal_rle_koppl_ov        constant varchar2(30) := 'AANTAL_RLE_KOPPL_OV';
  cn_aantal_rle_koppl_af        constant varchar2(30) := 'AANTAL_RLE_KOPPL_AF';
  cn_aantal_rle_koppl_na_gba    constant varchar2(30) := 'AANTAL_RLE_KOPPL_NA_GBA';
  cn_aantal_rle_koppl_na_rle    constant varchar2(30) := 'AANTAL_RLE_KOPPL_NA_RLE';
  cn_aantal_bsn_ng              constant varchar2(30) := 'AANTAL_BSN_NG';

PROCEDURE LIJST
 (P_TEXT IN VARCHAR2
 );
PROCEDURE SCHRIJF_VERSCHIL_TNT
 (P_BSN_DLN IN VARCHAR2
 ,P_RELNR_DLN IN NUMBER
 );
PROCEDURE SCHRIJF_VERSCHIL_RLE
 (P_RELNR_DLN IN NUMBER
 );
PROCEDURE SCHRIJF_VERSCHIL_COMBI
 (P_RELNR_DLN IN NUMBER
 ,P_RKN_ID IN NUMBER
 ,P_TNT_ID IN NUMBER
 ,P_IND_HUW IN VARCHAR2
 ,P_IND_SCH IN VARCHAR2
 ,P_IND_PRT IN VARCHAR2
 );
PROCEDURE SCHRIJF_MATCH
 (P_RKN_ID IN NUMBER
 ,P_TNT_ID IN NUMBER
 ,P_BSN IN VARCHAR2
 );


PROCEDURE LIJST
 (P_TEXT IN VARCHAR2
 )
 IS

  -- Local variabelen.
  cn_lijst constant pls_integer := 1;
begin
  --
  stm_util.insert_lijsten ( cn_lijst
                          , g_regelnummer
                          , p_text );
  --
  stm_util.debug ( 'LIJST :' || p_text );

end lijst;
PROCEDURE SCHRIJF_VERSCHIL_TNT
 (P_BSN_DLN IN VARCHAR2
 ,P_RELNR_DLN IN NUMBER
 )
 IS

 cursor c_tnt       (b_bsn_persoon varchar2)
  is
  select tnt.id
  ,      tnt.bsn
  ,      tnt.a_nummer
  ,      tnt.datum_sluiting
  ,      tnt.datum_ontbinding
  ,      tnt.reden_ontbinding
  ,      tnt.geboortedatum
  ,      tnt.voornamen
  ,      tnt.voorvoegsels_geslachtsnaam
  ,      tnt.geslachtsnaam
  from   rle_gba_huwelijken_tnt tnt
  where  tnt.bsn_persoon = b_bsn_persoon
  --
  -- door deze not exist schrijft deze funktie zowel alle rijen weg
  -- als alle nog niet "verwerkte" rijen
  --
  and not exists (select 'x' from rle_gba_tnt_verschillen ver
                  where ver.id_tnt = tnt.id
                 )
                  ;
  --
  r_tnt c_tnt%rowtype;

  cursor c_prs (b_rle_nr number)
  is
  select rlv.persoonsnummer
   ,     rlv.datum_overlijden
   ,     rlv.datgeboorte
   ,     wm_concat(dnp.rgg_code) deelnemer_schappen
   from  rle_mv_personen rlv
   ,     ret_deelnemerschappen dnp
   where rlv.relatienummer = b_rle_nr
   and   rlv.relatienummer =  dnp.rle_numrelatie (+)
   group by rlv.persoonsnummer , rlv.datum_overlijden  , rlv.datgeboorte
   ;
  --
  r_prs c_prs%rowtype;
  --
  l_rol_dln varchar2(20);
begin
  --
  -- uit r_prs komen dus gegevens vd deelnemer
  --
  stm_util.debug ( 'controle schrijf_verschil tnt');

  open c_prs (p_relnr_dln);
  fetch c_prs into r_prs;
  close c_prs;
  --
  rle_gba_bepaal_rol
   ( pin_numrelatie => p_relnr_dln
   , pov_rol        => l_rol_dln);

  for r_tnt in c_tnt (p_bsn_dln) loop
    --
    stm_util.debug ( 'schrijf_verschil tnt met tnt_id : '||r_tnt.id);
    --
    insert into rle_gba_tnt_verschillen ver
    ( ver.bsn
    , ver.id_tnt
    , ver.ind_match
    , ver.persoonsnummer
    , ver.relatienummer
    , ver.geboortedatum
    , ver.overlijdensdatum
    , ver.deelnemerschappen
    , ver.primaire_relatierol
    --
    , ver.datum_huwelijk_gba
    , ver.datum_ontbinding_gba
    , ver.reden_ontbinding_gba
    , ver.bsn_partner_gba
    , ver.geboortedatum_partner_gba
    , ver.a_nr_partner_gba
    , ver.naam_partner_gba
     )
    values
    ( p_bsn_dln
    , r_tnt.id
    , 'N' -- Ja / Nee
    , r_prs.persoonsnummer
    , p_relnr_dln
    , to_char(r_prs.datgeboorte,'dd-mm-yyyy')
    , to_char(r_prs.datum_overlijden,'dd-mm-yyyy')
    , r_prs.deelnemer_schappen
    , l_rol_dln
    -- gba/tnt gegevens
    , replace (substr(r_tnt.datum_sluiting,7,2)||'-'||substr(r_tnt.datum_sluiting,5,2)||'-'||substr(r_tnt.datum_sluiting,1,4),'--','')
    , replace (substr(r_tnt.datum_ontbinding,7,2)||'-'||substr(r_tnt.datum_ontbinding,5,2)||'-'||substr(r_tnt.datum_ontbinding,1,4),'--','')
    , r_tnt.reden_ontbinding
    , r_tnt.bsn
    , replace (substr(r_tnt.geboortedatum   ,7,2)||'-'||substr(r_tnt.geboortedatum   ,5,2)||'-'||substr(r_tnt.geboortedatum,1,4),'--','')
    , r_tnt.a_nummer
    , bepaal_voorletters(r_tnt.voornamen)||' '||r_tnt.voorvoegsels_geslachtsnaam||' '||r_tnt.geslachtsnaam
    --
    );
    --
    g_aantal_rle_koppl_na_rle := g_aantal_rle_koppl_na_rle + 1;
    --
  end loop;
  --
end schrijf_verschil_tnt;
PROCEDURE SCHRIJF_VERSCHIL_RLE
 (P_RELNR_DLN IN NUMBER
 )
 IS


 cursor c_rkn     (b_relnr number)
  is
  select rkn.id
  ,      rkn.dat_begin
  ,      rkn.dat_einde
  ,      rlv.sofinummer bsn_part
  ,      rlv.datgeboorte gebdat_part
  ,      rlv.code_geboortedatum_fictief cgfp
  ,      rec.extern_relatie a_num_part
   ,     rlv.voorletter||' '||rlv.voorvoegsels||' '||rlv.naam naam_part
  from   rle_relatie_koppelingen rkn -- is van de dln
  ,      rle_mv_personen rlv -- is van de partn
  ,      rle_relatie_externe_codes rec
  where  rkn.rle_numrelatie = b_relnr
  and    rlv.relatienummer = rkn.rle_numrelatie_voor
  and    rkn.rle_numrelatie_voor = rec.rle_numrelatie (+)
  and    'GBA' = rec.dwe_wrddom_extsys (+)
  and    rkn.rkg_id = 1
  --
  -- door deze not exist schrijft deze funktie zowel alle rijen weg
  -- als alle nog niet "verwerkte" rijen
  --
  and not exists (select 'x' from rle_gba_tnt_verschillen ver
                  where ver.id_rkn = rkn.id )
                  ;

  --
  r_rkn c_rkn%rowtype;
  --
  cursor c_prs (b_rle_nr number)
  is
  select rlv.persoonsnummer
   ,     rlv.datum_overlijden
   ,     rlv.datgeboorte
   ,     rlv.sofinummer
   ,     wm_concat(dnp.rgg_code) deelnemer_schappen
   from  rle_mv_personen rlv
   ,     ret_deelnemerschappen dnp
   where rlv.relatienummer = b_rle_nr
   and   rlv.relatienummer =  dnp.rle_numrelatie (+)
   group by rlv.persoonsnummer
   , rlv.datum_overlijden
   , rlv.datgeboorte
   , rlv.sofinummer
   , rlv.voorletter||' '||rlv.voorvoegsels||' '||rlv.naam
   ;
  --
  r_prs c_prs%rowtype;
  --
  l_rol varchar2(20);
  --
begin
  --
  stm_util.debug ( 'controle schrijf_verschil rle');
  --
  open c_prs (p_relnr_dln);
  fetch c_prs into r_prs;
  close c_prs;
  --
  rle_gba_bepaal_rol
   ( pin_numrelatie => p_relnr_dln
   , pov_rol        => l_rol);
  --
  for r_rkn in c_rkn (p_relnr_dln ) loop
    --
    stm_util.debug ( 'schrijf_verschil rle met rkn_id : '||r_rkn.id);

    insert into rle_gba_tnt_verschillen ver
    ( ver.bsn
    , ver.id_rkn
    , ver.ind_match
    , ver.persoonsnummer
    , ver.relatienummer
    , ver.geboortedatum
    , ver.overlijdensdatum
    , ver.deelnemerschappen
    , ver.primaire_relatierol
    --
    , ver.datum_huwelijk_rle
    , ver.datum_ontbinding_rle
    , ver.bsn_partner_rle
    , ver.geboortedatum_partner_rle
    , ver.a_nr_partner_rle
    , ver.naam_partner_rle
     )
    values
    ( r_prs.sofinummer
    , r_rkn.id
    , 'N' -- Ja / Nee
    , r_prs.persoonsnummer
    , p_relnr_dln
    , to_char(r_prs.datgeboorte,'dd-mm-yyyy')
    , to_char(r_prs.datum_overlijden,'dd-mm-yyyy')
    , r_prs.deelnemer_schappen
    , l_rol
    --
    , to_char(r_rkn.dat_begin,'dd-mm-yyyy')
    , to_char(r_rkn.dat_einde,'dd-mm-yyyy')
    , r_rkn.bsn_part
    , case when r_rkn.cgfp in (1,3,5,7) then  '00' else to_char(r_rkn.gebdat_part,'dd') end ||'-'||
      case when r_rkn.cgfp in (2,3,6,7) then  '00' else to_char(r_rkn.gebdat_part,'mm') end ||'-'||
      case when r_rkn.cgfp in (4,5,6,7) then  '0000' else to_char(r_rkn.gebdat_part,'yyyy') end
    , r_rkn.a_num_part
    , r_rkn.naam_part
    );
    --
    g_aantal_rle_koppl_na_gba := g_aantal_rle_koppl_na_gba + 1;
    --
  end loop;
  --
end schrijf_verschil_rle;
PROCEDURE SCHRIJF_VERSCHIL_COMBI
 (P_RELNR_DLN IN NUMBER
 ,P_RKN_ID IN NUMBER
 ,P_TNT_ID IN NUMBER
 ,P_IND_HUW IN VARCHAR2
 ,P_IND_SCH IN VARCHAR2
 ,P_IND_PRT IN VARCHAR2
 )
 IS

 --
  cursor c_prs (b_rle_nr number)
  is
  select rlv.persoonsnummer
   ,     rlv.datum_overlijden
   ,     rlv.sofinummer
   ,     rlv.datgeboorte
   ,     wm_concat(dnp.rgg_code) deelnemer_schappen
   from  rle_mv_personen rlv
   ,     ret_deelnemerschappen dnp
   where rlv.relatienummer = b_rle_nr
   and   rlv.relatienummer =  dnp.rle_numrelatie (+)
   group by rlv.persoonsnummer
   , rlv.datum_overlijden
   , rlv.sofinummer
   , rlv.datgeboorte
   ;

  cursor c_rkn       (b_rkn_id number)
  is
  select
    rkn.id
  , rkn.dat_begin
  , rkn.dat_einde
  , rlv.sofinummer bsn_part
  , rlv.persoonsnummer prs_part
  , rlv.relatienummer relnr_part
  , rlv.datgeboorte gebdat_part
  , rlv.code_geboortedatum_fictief cgfp
  , rec.extern_relatie a_num_part
  , rlv.voorletter||' '||rlv.voorvoegsels||' '||rlv.naam  naam_part
  , rlv.datum_overlijden ovl_dat_part
  from   rle_relatie_koppelingen rkn -- is van de dln
  ,      rle_mv_personen rlv -- is van de partn
  ,      rle_relatie_externe_codes rec
  where  rkn.id = b_rkn_id
  and    rlv.relatienummer = rkn.rle_numrelatie_voor
  and    rkn.rle_numrelatie_voor = rec.rle_numrelatie (+)
  and    'GBA' = rec.dwe_wrddom_extsys (+);
  --
  cursor c_tnt       (b_tnt_id number)
  is
  select tnt.id
  ,      tnt.bsn
  ,      tnt.a_nummer
  ,      tnt.datum_sluiting
  ,      tnt.datum_ontbinding
  ,      tnt.reden_ontbinding
  ,      tnt.geboortedatum
  ,      tnt.voornamen
  ,      tnt.voorvoegsels_geslachtsnaam
  ,      tnt.geslachtsnaam
  from   rle_gba_huwelijken_tnt tnt
  where  tnt.id = b_tnt_id;

  r_prs c_prs%rowtype;
  r_rkn c_rkn%rowtype;
  r_tnt c_tnt%rowtype;
  l_rol varchar2(20);
begin
  --
  stm_util.debug ( 'begin schrijf_verschil combi : '||p_rkn_id||' met tnt : '||p_tnt_id);
  -- haal de deeln gegevens
  open c_prs (p_relnr_dln);
  fetch c_prs into r_prs;
  close c_prs;
  --
  rle_gba_bepaal_rol
   ( pin_numrelatie => p_relnr_dln
   , pov_rol        => l_rol);
  -- haal de RLE gegevens
  open c_rkn( b_rkn_id => p_rkn_id);
  fetch c_rkn into r_rkn;
  close c_rkn;
  -- haal de tnt gegegevens
  open c_tnt (b_tnt_id => p_tnt_id);
  fetch c_tnt into r_tnt;
  close c_tnt;
  --
  insert into rle_gba_tnt_verschillen ver
    ( ver.bsn
    , ver.id_rkn
    , ver.id_tnt
    , ver.ind_match
    , ver.persoonsnummer
    , ver.relatienummer
    , ver.geboortedatum
    , ver.overlijdensdatum
    , ver.deelnemerschappen
    , ver.primaire_relatierol
    -- extra partner
    , ver.persoonsnummer_partner_rle
    , ver.relatienummer_partner_rle
    , ver.overlijdensdatum_partner_rle
    --
    , ver.datum_huwelijk_rle
    , ver.datum_ontbinding_rle
    , ver.bsn_partner_rle
    , ver.geboortedatum_partner_rle
    , ver.a_nr_partner_rle
    , ver.naam_partner_rle
    --
    , ver.datum_huwelijk_gba
    , ver.datum_ontbinding_gba
    , ver.reden_ontbinding_gba
    , ver.bsn_partner_gba
    , ver.geboortedatum_partner_gba
    , ver.a_nr_partner_gba
    , ver.naam_partner_gba
    --
    , ver.ind_datum_huwelijk_afwijkend
    , ver.ind_datum_ontbinding_afwijkend
    , ver.ind_partner_afwijkend
     )
    values
    ( r_prs.sofinummer
    , p_rkn_id
    , p_tnt_id
    , 'G' -- Ja / Nee / Gedeeltelijk
    , r_prs.persoonsnummer
    , p_relnr_dln
    , to_char(r_prs.datgeboorte,'dd-mm-yyyy')
    , to_char(r_prs.datum_overlijden,'dd-mm-yyyy')
    , r_prs.deelnemer_schappen
    , l_rol
    -- extra partner
    , r_rkn.prs_part
    , r_rkn.relnr_part
    , to_char(r_rkn.ovl_dat_part,'dd-mm-yyyy')
    -- rle gegevens
    , to_char(r_rkn.dat_begin,'dd-mm-yyyy')
    , to_char(r_rkn.dat_einde,'dd-mm-yyyy')
    , r_rkn.bsn_part
    , case when r_rkn.cgfp in (1,3,5,7) then  '00' else to_char(r_rkn.gebdat_part,'dd') end ||'-'||
      case when r_rkn.cgfp in (2,3,6,7) then  '00' else to_char(r_rkn.gebdat_part,'mm') end ||'-'||
      case when r_rkn.cgfp in (4,5,6,7) then  '0000' else to_char(r_rkn.gebdat_part,'yyyy') end
    , r_rkn.a_num_part
    , r_rkn.naam_part
    -- gba/tnt gegevens
    , replace (substr(r_tnt.datum_sluiting,7,2)||'-'||substr(r_tnt.datum_sluiting,5,2)||'-'||substr(r_tnt.datum_sluiting,1,4),'--','')
    , replace (substr(r_tnt.datum_ontbinding,7,2)||'-'||substr(r_tnt.datum_ontbinding,5,2)||'-'||substr(r_tnt.datum_ontbinding,1,4),'--','')
    , r_tnt.reden_ontbinding
    , r_tnt.bsn
    , replace (substr(r_tnt.geboortedatum   ,7,2)||'-'||substr(r_tnt.geboortedatum   ,5,2)||'-'||substr(r_tnt.geboortedatum,1,4),'--','')
    , r_tnt.a_nummer
    , bepaal_voorletters(r_tnt.voornamen)||' '||r_tnt.voorvoegsels_geslachtsnaam||' '||r_tnt.geslachtsnaam
    --
    , p_ind_huw
    , p_ind_sch
    , p_ind_prt
    );
    --
    g_aantal_rle_koppl_af := g_aantal_rle_koppl_af + 1;

  --
end schrijf_verschil_combi;
PROCEDURE SCHRIJF_MATCH
 (P_RKN_ID IN NUMBER
 ,P_TNT_ID IN NUMBER
 ,P_BSN IN VARCHAR2
 )
 IS
begin
  --  dit doen we om de niet gematchte/overgebleven rijen
  --  van rle of van gba te kunnen identificeren
  --  die moeten nog verder behandeld worden nl

  stm_util.debug ( 'begin schrijf_match rle rkn : '||p_rkn_id||' met tnt : '||p_tnt_id);
  --
  insert into rle_gba_tnt_verschillen ver
    ( ver.bsn
    , ver.id_rkn
    , ver.id_tnt
    , ver.ind_match
     )
    values
    ( p_bsn
    , p_rkn_id
    , p_tnt_id
    , 'J' -- Ja / Nee / Gedeeltelijk
  );
  --
  g_aantal_rle_koppl_ov := g_aantal_rle_koppl_ov + 1;
  --
end schrijf_match;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN VARCHAR2
 )
 IS

--
  -- verwerk alle personen uit de gba / tnt stroom
  -- incl een indicatie of er voor deze persoon huwelijk gegegens staan in het gba/tnt bestand
  -- adhv dit bsn/relnr gaan we de rel koppl ophalen en die vergelijken met de rel koppl uit gba/tnt
  --
  cursor c_gpr (b_bsn varchar2)
  is
  select tnt.bsn_persoon
  ,      case when max (tnt.datum_sluiting ) is null then 0 else 1 end ind_huwlijk_aanw
  from   rle_gba_huwelijken_tnt tnt
  where  tnt.bsn_persoon > b_bsn
  group by bsn_persoon
  order by bsn_persoon asc
  ;
  r_gpr c_gpr%rowtype;
  --
  cursor c_rle (b_bsn_persoon varchar2)
  is
  select rec.rle_numrelatie
  from   rle_relatie_externe_codes rec
  where  rec.extern_relatie = b_bsn_persoon
  and    rec.dwe_wrddom_extsys = 'SOFI';
  --
  -- haal alle rel koppelingen op uit RLE vd persoon uit GBA/TNT
  --
  cursor c_rkn (b_numrelatie number)
  is
  select rkn.id rkn_id
  , rlv_bsn.geboortedatum        gebdat_bsn
  , rkn.rle_numrelatie_voor       relnr_part
  , rlv_rnv.sofinummer              bsn_part
  , rlv_rnv.geboortedatum        gebdat_part
  , rlv_rnv.datum_overlijden     ovldat_part
  , rlv_rnv.voorletter            voorl_part
  , rlv_rnv.voorvoegsels_partner  voorv_part
  , rlv_rnv.naam                   naam_part
  , rlv_rnv.code_geboortedatum_fictief  code_fict_part
  , rkn.dat_begin
  , rkn.dat_einde
  , rec.extern_relatie            a_num_part
  from   rle_relatie_koppelingen rkn
  ,      rle_mv_personen rlv_bsn -- deelnemer
  ,      rle_mv_personen rlv_rnv -- partner
  ,      rle_relatie_externe_codes rec
  where  rkn.rle_numrelatie = b_numrelatie
  and    rec.rle_numrelatie (+)= rlv_rnv.relatienummer
  and    rec.dwe_wrddom_extsys (+)= 'GBA'
  and    rkn.rle_numrelatie = rlv_bsn.relatienummer
  and    rkn.rle_numrelatie_voor = rlv_rnv.relatienummer
  and    rkn.rkg_id = 1
  -- en het gaat om de rijen die nog niet zijn weggeschreven naar verschillen
  -- en/of daar als match in staan !
  and not exists (select 'x' from rle_gba_tnt_verschillen ver
                  where ver.id_rkn = rkn.id )
  order by rkn.dat_begin
  ;
  --
  r_rkn c_rkn%rowtype;
  --
  cursor c_criteria   (b_bsn_persoon varchar2
                     , b_bsn_part    varchar2
                     , b_gebdat_part varchar2
                     , b_a_nummer    varchar2
                     , b_voorletters varchar2
                     , b_naam        varchar2
                     , b_dat_begin date
                     , b_dat_einde date)
  is
  select tnt.id
  ,      case when tnt.bsn = b_bsn_part and tnt.geboortedatum = b_gebdat_part then 1 else 0 end criteria_3_1
  ,      case when tnt.a_nummer = b_a_nummer and tnt.geboortedatum = b_gebdat_part then 1 else 0 end criteria_3_2
  ,      case when bepaal_voorletters(tnt.voornamen) = b_voorletters and tnt.geslachtsnaam = b_naam and tnt.geboortedatum = b_gebdat_part then 1 else 0 end criteria_3_3
  ,      case when nvl(tnt.datum_ontbinding,'x') = nvl(to_char(b_dat_einde,'yyyymmdd'),'x') then  1 else 0 end  criteria_2
  ,      case when tnt.datum_sluiting   = to_char(b_dat_begin,'yyyymmdd') then  1 else 0 end criteria_1
  from   rle_gba_huwelijken_tnt tnt
  where  tnt.bsn_persoon = b_bsn_persoon
  -- en het gaat om de rijen die nog niet zijn weggeschreven naar verschillen
  -- en/of daar als match in staan !
  and not exists (select 'x' from rle_gba_tnt_verschillen ver
                  where ver.id_tnt = tnt.id )
  order by tnt.datum_sluiting
  --
  -- hier zou in de where clause evt info over de loop gang (1, 2, of 3) bij kunnen
  -- zodat bij loop 3 een rij van tnt wel meerdere matches mag hebben
  -- dit hebben we nu dus (nog) niet gedaan.
  --
  -- Dus => (bij loop 3) na een 1 op 3 match wordt dit huwelijk als afgehandeld gezien
  -- terwijl hij met meerdere huwelijken een 1 op 3 match zou kunnen hebben !
  --
  ;
  --
  l_tnt_id         number;
  l_criteria_3_1   pls_integer;
  l_criteria_3_2   pls_integer;
  l_criteria_3_3   pls_integer;
  l_criteria_2     pls_integer;
  l_criteria_1     pls_integer;
  --
  l_rle_numrelatie     number;
  --
  l_descript_lengte    pls_integer := 25;
  l_volgnummer         pls_integer := 1;
  l_aantal_bsn         pls_integer := 1;
  --
  l_jaar               varchar2(4);
  l_maand              varchar2(2);
  l_dag                varchar2(2);
  --
  -- criterea om niet onnodig 3 x door de loop van rkn te gaan 3:3 , 2:3 en 1:3 match
  --
  l_stop_rkn           varchar2(1) := 'N';
  --
begin
  stm_util.module_start ( cn_module );
  stm_util.t_script_naam := p_script_naam;
  stm_util.t_script_id := to_number ( p_script_id );
  stm_util.t_programma_naam := cn_package;
  stm_util.t_tekst := null;

  -- initialiseren van logverslag
  if stm_util.herstart
  then
    --
    g_bsn_herstart                := stm_util.lees_herstartsleutel ( cn_bsn );
    g_regelnummer                 := stm_util.lees_herstarttelling ( cn_regelnummer        );
    --
    g_aantal_rle_koppl_ov         := stm_util.lees_herstarttelling ( cn_aantal_rle_koppl_ov        );
    g_aantal_rle_koppl_af         := stm_util.lees_herstarttelling ( cn_aantal_rle_koppl_af        );
    g_aantal_rle_koppl_na_gba     := stm_util.lees_herstarttelling ( cn_aantal_rle_koppl_na_gba    );
    g_aantal_rle_koppl_na_rle     := stm_util.lees_herstarttelling ( cn_aantal_rle_koppl_na_rle    );
    g_aantal_bsn_ng               := stm_util.lees_herstarttelling ( cn_aantal_bsn_ng              );
    --
    lijst ( '*** LOGVERSLAG herstart tbv VERGELIJK TNT/GBA met RLE *** ' || to_char ( sysdate ,cn_datum_tijd_formaat ) );
    lijst ( ' ' );
    lijst ( 'Parameters bij het proces / herstart ' );
    lijst ( '-----------------------------------------------------------------------------' );
    lijst ( rpad ( 'Script naam', l_descript_lengte )           || ': ' || p_script_naam );
    lijst ( rpad ( 'Script ID', l_descript_lengte )             || ': ' || p_script_id   );
    lijst ( ' ' );
    --
  else
    lijst ( '*** LOGVERSLAG tbv VERGELIJK TNT/GBA met RLE *** ' || to_char ( sysdate ,cn_datum_tijd_formaat ) );
    lijst ( ' ' );
    lijst ( 'Parameters bij het proces ' );
    lijst ( '-----------------------------------------------------------------------------' );
    lijst ( rpad ( 'Script naam', l_descript_lengte )           || ': ' || p_script_naam );
    lijst ( rpad ( 'Script ID', l_descript_lengte )             || ': ' || p_script_id   );

    lijst ( ' ' );
    --
    delete from rle_gba_tnt_verschillen;
    --
  end if;

  --
  -- van alle personen uit het gba/tnt bestand worden de RLE koppelingen vergeleken met gba/tnt
  --
  for r_gpr in c_gpr (g_bsn_herstart) loop
      --
      -- bestaat de persoon
      --
      stm_util.debug(' ');
      stm_util.debug('Start afhandelen gba/tnt persoon met bsn : '||r_gpr.bsn_persoon);
      --
      open c_rle ( b_bsn_persoon => r_gpr.bsn_persoon );
      fetch c_rle into l_rle_numrelatie;
      --
      if c_rle%found
      then
       --
       -- 3 x => voor criteria 3 op 3 , 2 op 3 en 1 op 3 match
       --
       -- inits voor het evt eerder stoppen vd loop
       --
       l_stop_rkn          := 'N';
       --
       for l_count in 1..3 loop
         --
         -- prs gevonden , loop adhv het bsn/relatienummer van gba/tnt
         -- door de RLE koppelingen vd prs en check de criteria
         --
         open c_rkn ( b_numrelatie => l_rle_numrelatie);
         fetch c_rkn into r_rkn;

         if c_rkn%found
         then
            if r_gpr.ind_huwlijk_aanw = 1
            then
            --
            -- er staan huw geg in rle en gba/tnt
            -- loop door de rel kopp uit rle en vergelijk obv de criteria met gba/tnt
            --
               while c_rkn%found loop
                 --
                 -- vergelijk de aktueele rel koppl met alle "resterende/niet weggeschreven" gba/tnt koppelelingen
                 --
                 l_jaar  := to_char(r_rkn.gebdat_part,'yyyy');
                 l_maand := to_char(r_rkn.gebdat_part,'mm');
                 l_dag   := to_char(r_rkn.gebdat_part,'dd');
                 --
                 if r_rkn.code_fict_part in (1,3,5,7) then
                    l_dag := '00';
                 end if;
                 --
                 if r_rkn.code_fict_part in (2,3,6,7) then
                    l_maand := '00';
                 end if;
                 --
                 if r_rkn.code_fict_part in (4,5,6,7) then
                    l_jaar := '0000';
                 end if;
                 --
                 open c_criteria   ( b_bsn_persoon  => r_gpr.bsn_persoon
                                   , b_bsn_part     => r_rkn.bsn_part
                                   , b_gebdat_part  => l_jaar||l_maand||l_dag
                                   , b_a_nummer     => r_rkn.a_num_part
                                   , b_voorletters  => r_rkn.voorl_part
                                   , b_naam         => r_rkn.naam_part
                                   , b_dat_begin    => r_rkn.dat_begin
                                   , b_dat_einde    => r_rkn.dat_einde
                                    );
                 fetch c_criteria into    l_tnt_id
                                         ,l_criteria_3_1
                                         ,l_criteria_3_2
                                         ,l_criteria_3_3
                                         ,l_criteria_2
                                         ,l_criteria_1
                                         ;
                 --
                 if c_criteria%found
                 then
                    -- loop door de nog aanwezig te controlen rijen van gba/tnt
                    while c_criteria%found loop

                      -- 2.0  => 3 op 3 match
                      stm_util.debug('Start controle rkn id : '||r_rkn.rkn_id||' / tnt id : '||l_tnt_id||
                      ' obv criteria 1,2,3 abc : '||l_criteria_1||l_criteria_2||l_criteria_3_1||l_criteria_3_2||l_criteria_3_3||
                      ' loop nr : '||l_count );
                      --
                      if l_count= 1 and
                      ( (l_criteria_3_1 = 1 or l_criteria_3_2 = 1 or l_criteria_3_3 = 1)
                        and l_criteria_2 = 1 and l_criteria_1 = 1 )
                        then
                          -- full match
                          schrijf_match    ( p_rkn_id => r_rkn.rkn_id
                                           , p_tnt_id => l_tnt_id
                                           , p_bsn    => r_gpr.bsn_persoon );
                          exit; -- na 1 match of gedeeltelijke match door naar de volgende rkn
                      elsif
                      -- 3.0 => 2 op 3 match
                      l_count = 2 and
                         ( (l_criteria_3_1 = 0 and l_criteria_3_2 = 0 and l_criteria_3_3 = 0
                           and l_criteria_2 = 1 and l_criteria_1 = 1)
                        or ((l_criteria_3_1 = 1 or l_criteria_3_2 = 1 or l_criteria_3_3 = 1)
                            and l_criteria_2 = 0 and l_criteria_1 = 1)
                        or ((l_criteria_3_1 = 1 or l_criteria_3_2 = 1 or l_criteria_3_3 = 1)
                            and l_criteria_2 = 1 and l_criteria_1 = 0) )
                        then
                          -- schijf gecombineerder rij naar output
                          schrijf_verschil_combi( p_relnr_dln => l_rle_numrelatie
                                              , p_rkn_id  => r_rkn.rkn_id
                                              , p_tnt_id  => l_tnt_id
                                              , p_ind_huw => case when l_criteria_1 = 1 then 1 else 0 end
                                              , p_ind_sch => case when l_criteria_2 = 1 then 1 else 0 end
                                              , p_ind_prt => case when (l_criteria_3_1 = 1 or l_criteria_3_2 = 1 or l_criteria_3_3 = 1) then 1 else 0 end
                                               );
                          exit; -- na 1 match of gedeeltelijke match door naar de volgende rkn

                      elsif
                      -- 5.0 => 1 op 3 match
                      l_count = 3 and
                         ( (l_criteria_3_1 = 0 and l_criteria_3_2 = 0 and l_criteria_3_3 = 0
                           and l_criteria_2 = 0 and l_criteria_1 = 1)
                        or ((l_criteria_3_1 = 0 and l_criteria_3_2 = 0 and l_criteria_3_3 = 0)
                            and l_criteria_2 = 1 and l_criteria_1 = 0)
                        or ((l_criteria_3_1 = 1 or l_criteria_3_2 = 1 or l_criteria_3_3 = 1)
                            and l_criteria_2 = 0 and l_criteria_1 = 0))
                        then
                        -- schijf een verschil tussen rle en gba/tnt naar output
                          schrijf_verschil_combi( p_relnr_dln => l_rle_numrelatie
                                              , p_rkn_id    => r_rkn.rkn_id
                                              , p_tnt_id    => l_tnt_id
                                              , p_ind_huw => case when l_criteria_1 = 1 then 1 else 0 end
                                              , p_ind_sch => case when l_criteria_2 = 1 then 1 else 0 end
                                              , p_ind_prt => case when (l_criteria_3_1 = 1 or l_criteria_3_2 = 1 or l_criteria_3_3 = 1) then 1 else 0 end
                                              );
                      end if;
                      --
                      fetch c_criteria into    l_tnt_id
                                         ,l_criteria_3_1
                                         ,l_criteria_3_2
                                         ,l_criteria_3_3
                                         ,l_criteria_2
                                         ,l_criteria_1  ;

                    end loop;
                 --
                 end if; -- c_criteria%found
                 --
                 close c_criteria;
                 --
                 fetch c_rkn into r_rkn;
               --
               end loop;
               --
            end if;  -- r_gpr.ind_huwlijk_aanw = 1
         else
            l_stop_rkn := 'J'; -- loop 3 x kan stoppen
         end if; -- c_rkn%notfound

         -- volgende verwantschap van de tnt/gba persoon
         close c_rkn;
         --
         if l_stop_rkn = 'J' or r_gpr.ind_huwlijk_aanw = 0
         then
            exit;
         end if;
         --
       end loop; -- 3 x => voor criteria 3 op 3 , 2 op 3 en 1 op 3 match
       --
       -- schrijf evt/aanw niet "verwerkte en/of geraakte en/of gematchte" rijen door naar verschillen
       --
       if l_stop_rkn = 'N' then
          schrijf_verschil_rle ( p_relnr_dln => l_rle_numrelatie );
       end if;
       --
       if r_gpr.ind_huwlijk_aanw = 1 then
          schrijf_verschil_tnt ( p_bsn_dln   => r_gpr.bsn_persoon
                               , p_relnr_dln => l_rle_numrelatie );
       end if;
       --
      else
       --
       -- prs (bsn) niet aanwezig in rle ,
       -- maakt dan niet uit of er huwelijkse gegevens worden aangeleverd van gba/tnt
       -- dit gegeven loggen en door met de volgende
       --
       lijst('Persoon met bsn : ' ||r_gpr.bsn_persoon||' niet gevonden in RLE' );
       --
       g_aantal_bsn_ng := g_aantal_bsn_ng + 1;
       --
      end if;
      --
      close c_rle;
      --
      l_aantal_bsn := l_aantal_bsn + 1;
      if l_aantal_bsn > 100 then
        stm_util.schrijf_herstartsleutel(cn_bsn , r_gpr.bsn_persoon);
        stm_util.schrijf_herstarttelling(cn_regelnummer, g_regelnummer);
        stm_util.schrijf_herstarttelling(cn_aantal_rle_koppl_ov     , g_aantal_rle_koppl_ov    );
        stm_util.schrijf_herstarttelling(cn_aantal_rle_koppl_af     , g_aantal_rle_koppl_af    );
        stm_util.schrijf_herstarttelling(cn_aantal_rle_koppl_na_gba , g_aantal_rle_koppl_na_gba);
        stm_util.schrijf_herstarttelling(cn_aantal_rle_koppl_na_rle , g_aantal_rle_koppl_na_rle);
        stm_util.schrijf_herstarttelling(cn_aantal_bsn_ng           , g_aantal_bsn_ng);
        --
        commit;
        l_aantal_bsn := 0;
      end if;
  end loop;

  -- telling wegschrijven naar logverslag
  lijst ('');
  lijst ('GELIJK');
  lijst ( 'Aantal relatiekoppelingen RLE die overeenkomen met het GBA           : '||g_aantal_rle_koppl_ov     );
  lijst ('');
  lijst ('AFWIJKINGEN');
  lijst ( 'Aantal relatiekoppelingen die afwijken tussen RLE en het GBA-bestand : '||g_aantal_rle_koppl_af     );
  lijst ( 'Aantal relatiekoppelingen in RLE niet gevonden in het GBA-bestand    : '||g_aantal_rle_koppl_na_gba );
  lijst ( 'Aantal relatiekoppelingen in het GBA-bestand niet gevonden in RLE    : '||g_aantal_rle_koppl_na_rle );
  lijst ( 'Aantal aangeleverde BSN nummers niet gevonden in RLE                 : '||g_aantal_bsn_ng );
  --
  lijst ('');
  lijst ( ' *******************************  EINDE LOGVERSLAG  *******************************  ' );
  stm_util.verwijder_herstart;
  stm_util.insert_job_statussen ( ijs_volgnummer => l_volgnummer
                                , ijs_uitvoer_soort => 'S'
                                , ijs_uitvoer_waarde => '0' );

  stm_util.module_end;
  commit;
  --
  exception
    --
    when others then
      rollback;
      stm_util.insert_job_statussen ( l_volgnummer, 'S', '1' );
      stm_util.insert_job_statussen ( l_volgnummer, 'I', stm_util.t_programma_naam || ' procedure : ' || stm_util.t_procedure );
      stm_util.insert_job_statussen ( l_volgnummer, 'I', stm_util.t_programma_naam || ' SQL error : ' || substr ( sqlerrm, 1, 200 ));
      --
      if stm_util.t_foutmelding is not null
      then
        stm_util.insert_job_statussen ( l_volgnummer, 'I', stm_util.t_foutmelding);
      end if;

      --
      if stm_util.t_tekst is not null
      then
        stm_util.insert_job_statussen ( l_volgnummer, 'I', stm_util.t_tekst);
      end if;

  commit;

end verwerk;
END RLE_CONTROLE_RKN_GBA;
/