CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_VERW_XML_HUW_EXEN IS
-- declaration

  g_vorige_procedure                     stm_job_statussen.programma_naam%type;
  g_volgnummer                               pls_integer := 0;

type l_bsn_rec is record
  (bsn varchar2(9));

 type t_bsn_tab  is
  table of  l_bsn_rec
  index by pls_integer;

type l_signaal_rec is record
  (bsn varchar2(9)
  ,bsn_ex varchar2(9)
 ,persoonsnr_dlnr varchar2(20)
 ,relatienr_dlnr varchar2(20)
 ,geboortedatum date
 ,geboortedatum_gbav date
 ,overlijdensdatum date
 ,overlijdensdatum_gbav date
 ,persoonsnr_exp varchar2(20)
 ,relatienr_exp varchar2(20)
  ,ingangsdatum date
  ,einddatum date
  ,melding varchar2(4000));

 type t_signaallijst_tab  is
  table of  l_signaal_rec
  index by pls_integer;

type l_gbav_rec is record
     (id  varchar2(15)
     ,bsn varchar2(9)
     ,voornamen varchar2(60)
     ,geslachtsnaam varchar2(30)
     ,geboortedatum date
     ,geslachtsaanduiding varchar2(1)
     ,huisnummer    varchar2(5)
     ,postcode      varchar2(6)
     ,anummer       varchar2(10)
     );

type t_gbav_tab  is
     table of  l_gbav_rec
     index by pls_integer;
-- definitie pl/sql verwerkingstabel

type t_bron_exp_gba_tab  is
table of rle_EXP_HUWELIJKEN_GBAV%rowtype
index by pls_integer;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
/* Inlezen GBAV XML resultaat en fouten bestand met huwelijke gegevens */
PROCEDURE DIRECT_XML_PROCESSING
 (P_GBAV_XML IN VARCHAR2
 ,P_DIR IN VARCHAR2
 ,P_BESTANDSNAAM IN VARCHAR2
 ,P_VERWERKT OUT VARCHAR2
 );
/* Inserten gbav xml resultaat huwelijkse gegevens naar tijdelijke tabel */
PROCEDURE INSERT_RESULTAAT
 (P_HUW_TAB IN t_bron_exp_gba_tab
 ,P_DATUM_VERWERKING IN DATE
 ,P_BESTANDSNAAM IN VARCHAR2
 );
/* verwerk ingelezen gbav xml resultaat en foutenbestand */
PROCEDURE VERWERK_RESULTAAT
 (P_BESTANDSNAAM IN VARCHAR2
 );
/* Foutbestand verwerken */
PROCEDURE VERWERK_RESULTAAT_FOUT
 (P_BESTANDSNAAM IN VARCHAR2
 );


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
    /**********************************************************************
    Naam:         WEGSCHRIJVEN STM_LIJST
    Beschrijving:
    **********************************************************************/

    cn_lijst constant pls_integer := 1;
begin
  stm_util.insert_lijsten ( cn_lijst
                          , g_volgnummer
                          , p_text );
end lijst;
/* Inlezen GBAV XML resultaat en fouten bestand met huwelijke gegevens */
PROCEDURE DIRECT_XML_PROCESSING
 (P_GBAV_XML IN VARCHAR2
 ,P_DIR IN VARCHAR2
 ,P_BESTANDSNAAM IN VARCHAR2
 ,P_VERWERKT OUT VARCHAR2
 )
 IS

   l_signaallijst_tab            t_signaallijst_tab;
   l_empty_signaallijst_tab      t_signaallijst_tab;
   l_huw_exp_tab                 t_bron_exp_gba_tab;
   l_empty_tab                   t_bron_exp_gba_tab;
   l_filename                    VARCHAR2 (50);
   l_categorie_01                BOOLEAN;
   l_categorie_05_55             BOOLEAN;
   l_categorie_06                BOOLEAN;
   -- deelnemer
   l_A_nummer                    VARCHAR2 (30)  := '';
   l_BSN                         VARCHAR2 (10)  := '';
   l_voornamen                   VARCHAR2 (100) := '';
   l_voorvoegsels                VARCHAR2 (50)  := '';
   l_geslachtsnaam               VARCHAR2 (100) := '';
   l_geboortedatum               DATE;
   l_overlijdensdatum            DATE;
   l_geboorteplaats              VARCHAR2 (50)  := '';
   l_geslachtsaanduideling       VARCHAR2 (50)  := '';
   l_aanduiding_naamgebruik      VARCHAR2 (50)  := '';
   -- (ex)-partner(s)
   l_A_nummer_ex                 VARCHAR2 (1000) := '';
   l_BSN_ex                      VARCHAR2 (10)   := '';
   l_voornamen_ex                VARCHAR2 (100)  := '';
   l_voorvoegsels_ex             VARCHAR2 (50)   := '';
   l_geslachtsnaam_ex            VARCHAR2 (100)  := '';
   l_geboortedatum_ex            DATE;
   l_geboorteplaats_ex           VARCHAR2 (50)   := '';
   l_geslachtsaanduideling_ex    VARCHAR2 (50)   := '';
   l_aanduiding_naamgebruik_ex   VARCHAR2 (50)   := '';
   l_melding                     VARCHAR2 (4000) := '';
   l_melding_dln                 VARCHAR2 (4000) := '';
   -- huwelijkse perioden
   l_datum_einde_huwelijk        DATE;
   l_reden_einde_huwelijk        VARCHAR2(10)    :='';
   l_datum_begin_huwelijk        DATE;
   l_geldigheidsdatum            DATE;
   l_opgeslagen                  BOOLEAN         := FALSE;
   l_opgeslagen_partner          BOOLEAN         := FALSE;
   l_antwoorden                  BOOLEAN         := FALSE;
   l_fouten                      BOOLEAN         := FALSE;
   l_index                       NUMBER          := 0;
   l_index2                      NUMBER          := 0;
   l_foutmelding                 VARCHAR2 (300)  := '00000';
   -- l_aantal_foutieve_datums      number          := 0;
   l_file_id                     UTL_FILE.file_type;
   l_string                      VARCHAR2 (4000);
   l_prev_string                 VARCHAR2 (4000) := ' ';
   l_pos_begin                   NUMBER          := 0;
   l_pos_eind                    NUMBER          := 0;
   l_prev_bsn                    varchar2(200)   := '';
   l_bestaat                     boolean;
   l_lengte                      number;
   l_blocksize                   number;
  --
   l_regelnummer                 NUMBER := 0;
   l_lijstnummer                 NUMBER := 0;
   l_regelnummer_csv             NUMBER := 0;
   l_datum_verwerking            DATE   := SYSDATE;
   l_volgnummer stm_job_statussen.volgnummer%type := 0;
begin
   --
--   g_vorige_procedure        := stm_util.t_procedure;
--   stm_util.t_procedure      := 'DIRECT_XML_PROCESSING';
   --
   --vooraf leegmaken tabel waarin het xml-bestanden (gbav-resultaat.xml of gbav-resultaat-fouten.xml) tijdelijk ingelezen wordt
   if p_gbav_xml = 'gbav-resultaat.xml'
   then
     delete from rle_exp_huwelijken_gbav where filename = p_bestandsnaam;
   else
     delete from rle_exp_xml_fouten_gbav where filename = p_bestandsnaam;
   end if;
   --
   l_prev_bsn := '-';
   --
   l_foutmelding := '00001';
   --
   l_filename := p_bestandsnaam;
   --
   l_foutmelding := 'open utl_file pdir: ' || p_dir || ' file: ' || p_gbav_xml;
   dbms_output.put_line(l_foutmelding);
   stm_util.debug(l_foutmelding);

       utl_file.fgetattr (p_dir, p_gbav_xml, l_bestaat, l_lengte, l_blocksize);

   if not l_bestaat
   then

     dbms_output.put_line('Bestand '||p_gbav_xml || ' bestaat niet (dit kan korrekt zijn, indien maar 1 bestand is aangeboden)');
     p_verwerkt := 'N';
     return;

   else

   l_file_id := UTL_FILE.fopen ('RLE_INBOX', p_gbav_xml, 'r');
   --
   IF not utl_file.is_open(l_file_id)
   THEN
     dbms_output.put_line('Bestand '||p_gbav_xml || ' bestaat niet (dit kan korrekt zijn, indien maar 1 bestand is aangeboden)');
     p_verwerkt := 'N';
     return;
   ELSIF UTL_FILE.is_open (l_file_id)
   THEN
      --dbms_output.put_line('Bestand '||p_gbav_xml || ' is geopend en wordt verwerkt');
      LOOP
         BEGIN
            l_string := '';
            UTL_FILE.get_line (l_file_id, l_string);
            l_pos_begin := INSTR (l_string, '<', 1);
            l_pos_eind := (LENGTH (l_string) + 1) - INSTR (l_string, '<', 1);
            l_string := SUBSTR (l_string, l_pos_begin, l_pos_eind);
            --
            -- bepaal het bestand
            IF l_string LIKE '%<batch:gbavFouten%'
               AND l_antwoorden = FALSE
               AND l_fouten = FALSE
            THEN
               l_fouten := TRUE;
            ELSIF     l_string LIKE '%<batch:gbavAntwoorden%'
                  AND l_antwoorden = FALSE
                  AND l_fouten = FALSE
            THEN
               l_antwoorden := TRUE;
            END IF;
            --
            -- verwerken antwoorden
            IF l_antwoorden
            THEN
               --
               stm_util.debug(l_string);
               --
               l_foutmelding := '00004';
               --
               IF l_string = '</batch:gbavAntwoorden>'
               THEN
                  EXIT;
               END IF;
               --
               -- loop lines in bestand
               --
               l_foutmelding := '00005';

               --
               stm_util.debug(l_foutmelding);
               IF l_prev_string = '<nummer>01</nummer>'         --categorie 01
                  AND l_string = '<naam>Persoon</naam>'
               THEN
                  l_foutmelding := '00005a';
                  stm_util.debug(l_foutmelding);
                  l_categorie_01 := TRUE;
                  l_categorie_05_55 := FALSE;
                  l_categorie_06 := FALSE;
               ELSIF (l_prev_string = '<nummer>05</nummer>' --categorie 05 or 55
                      OR l_prev_string = '<nummer>55</nummer>')
                     AND (l_string =
                             '<naam>Huwelijk/geregistreerd partnerschap</naam>'
                          OR l_string =
                               '<naam>Historisch Huwelijk/geregistreerd partnerschap</naam>')
               THEN
                  l_foutmelding := '00005b';
                  stm_util.debug(l_foutmelding);
                  l_categorie_01 := FALSE;
                  l_categorie_05_55 := TRUE;
                  l_categorie_06 := FALSE;

                  --
                  IF     l_geslachtsnaam_ex IS NOT NULL
                     AND NOT l_opgeslagen_partner
                     AND l_prev_string = '<nummer>05</nummer>'  --categorie 05
                     AND l_string =
                           '<naam>Huwelijk/geregistreerd partnerschap</naam>'
                  THEN
                     --
                     -- leg waarden vast in pl/sql tabel
                     l_foutmelding := 'Legvast3: ' || l_BSN;
                     stm_util.debug(l_foutmelding);
                     stm_util.debug('l_melding Legvast3: '||l_melding);
                     stm_util.debug('l_melding_dln Legvast3: '||l_melding_dln);
                     --
                     l_index                                      := l_index + 1;
                     l_huw_exp_tab (l_index).filename             := l_filename;
                     l_huw_exp_tab (l_index).A_nummer             := l_A_nummer;
                     l_huw_exp_tab (l_index).BSN                  := l_BSN;
                     l_huw_exp_tab (l_index).voornamen            := l_voornamen;
                     l_huw_exp_tab (l_index).voorvoegsels         := l_voorvoegsels;
                     l_huw_exp_tab (l_index).naam                 := l_geslachtsnaam;
                     l_huw_exp_tab (l_index).geslacht             := l_geslachtsaanduideling;
                     l_huw_exp_tab (l_index).geboortedatum        := l_geboortedatum;
                     l_huw_exp_tab (l_index).overlijdensdatum     := l_overlijdensdatum;
                     l_huw_exp_tab (l_index).A_nummer_ex          := l_A_nummer_ex;
                     l_huw_exp_tab (l_index).BSN_ex               := l_BSN_ex;
                     l_huw_exp_tab (l_index).voornamen_ex         := l_voornamen_ex;
                     l_huw_exp_tab (l_index).voorvoegsels_ex      := l_voorvoegsels_ex;
                     l_huw_exp_tab (l_index).naam_ex              := l_geslachtsnaam_ex;
                     l_huw_exp_tab (l_index).geslacht_ex          := l_geslachtsaanduideling_ex;
                     l_huw_exp_tab (l_index).geboortedatum_ex     := l_geboortedatum_ex;
                     l_huw_exp_tab (l_index).datum_einde_huwelijk := l_datum_einde_huwelijk;
                     l_huw_exp_tab (l_index).reden_einde_huwelijk := l_reden_einde_huwelijk;
                     l_huw_exp_tab (l_index).datum_begin_huwelijk := l_datum_begin_huwelijk;
                     l_huw_exp_tab (l_index).foutmelding          := case when l_melding_dln is not null
                                                                          and l_melding is not null
                                                                          then l_melding_dln||'; '
                                                                          when l_melding_dln is not null
                                                                          and l_melding is null
                                                                          then l_melding_dln
                                                                          else null
                                                                          end||l_melding;
                     --
                     -- resetten loop waarden partnerschap(pen)/huwelijk(en)
                     l_A_nummer_ex := '';
                     l_BSN_ex := '';
                     l_voornamen_ex := '';
                     l_voorvoegsels_ex := '';
                     l_geslachtsnaam_ex := '';
                     l_geboortedatum_ex := '';
                     l_geboorteplaats_ex := '';
                     l_geslachtsaanduideling_ex := '';
                     l_aanduiding_naamgebruik_ex := '';
                     l_datum_einde_huwelijk := '';
                     l_reden_einde_huwelijk := '';
                     l_datum_begin_huwelijk := '';
                     l_opgeslagen_partner := FALSE;
                     l_geldigheidsdatum := '';
                     l_melding := null;
                  --
                  ELSIF l_opgeslagen_partner
                        AND l_prev_string = '<nummer>05</nummer>' --categorie 05
                        AND l_string =
                              '<naam>Huwelijk/geregistreerd partnerschap</naam>'
                  THEN
                     l_foutmelding := '00005c';
                     stm_util.debug(l_foutmelding);
                     -- de volgende persoon moet weer worden opgeslagen
                     l_A_nummer_ex := '';
                     l_BSN_ex := '';
                     l_voornamen_ex := '';
                     l_voorvoegsels_ex := '';
                     l_geslachtsnaam_ex := '';
                     l_geboortedatum_ex := '';
                     l_geboorteplaats_ex := '';
                     l_geslachtsaanduideling_ex := '';
                     l_aanduiding_naamgebruik_ex := '';
                     l_datum_einde_huwelijk := '';
                     l_reden_einde_huwelijk := '';
                     l_datum_begin_huwelijk := '';
                     l_opgeslagen_partner := FALSE;
                     l_opgeslagen := TRUE;
                     l_geldigheidsdatum := '';
                     l_melding := null;
                  END IF;
               ELSIF l_prev_string = '<nummer>06</nummer>'      --categorie 06
                     AND l_string = '<naam>Overlijden</naam>'
               THEN
                  l_foutmelding := '00005d';
                  stm_util.debug(l_foutmelding);
                  l_categorie_01 := FALSE;
                  l_categorie_05_55 := FALSE;
                  l_categorie_06 := TRUE;
               END IF;
               --
               l_foutmelding := '00006';

               stm_util.debug(l_foutmelding);
               IF     l_categorie_01
                  AND NOT l_categorie_05_55
                  AND NOT l_categorie_06
               THEN

                  IF l_prev_string = '<naam>A-nummer</naam>'  --'110' A-nummer
                     AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006a';
                     stm_util.debug(l_foutmelding);
                     l_A_nummer :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  stm_util.debug(l_A_nummer);
                  ELSIF l_prev_string = '<naam>BSN</naam>'         --'120' BSN
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006b';
                     stm_util.debug(l_foutmelding);
                     l_BSN :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);

                     IF l_bsn <> l_prev_bsn
                     THEN
                        l_prev_bsn := l_bsn;
                     END IF;
                  ELSIF l_prev_string = '<naam>Voornamen</naam>' --'0210' voornamen
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006c';
                     stm_util.debug(l_foutmelding);
                     l_voornamen :=
                        TO_CHAR (
                           SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9));
                  ELSIF l_prev_string =
                           '<naam>Voorvoegsels geslachtsnaam</naam>' --'0230' --voorvoegsels
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006d';
                     stm_util.debug(l_foutmelding);
                     l_voorvoegsels :=
                        TO_CHAR (
                           SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9));
                  ELSIF l_prev_string = '<naam>Geslachtsnaam</naam>' -- '0240'             --geslachtsnaam
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006e';
                     stm_util.debug(l_foutmelding);
                     l_geslachtsnaam :=
                        TO_CHAR (
                           SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9));
                  ELSIF l_prev_string = '<naam>Geboortedatum</naam>' --'0310'             --geboortedatum
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006f';
                     stm_util.debug(l_foutmelding);
                     begin
                       l_geboortedatum :=
                        to_date(SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9),'YYYYMMDD');
                     exception when others
                     then
                       --
                       l_melding_dln := 'Fout: Foutieve geboortedatum dlnr: '||SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                     end;
                  ELSIF l_prev_string = '<naam>Geboorteplaats</naam>' --'0320'            --geboorteplaats
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006g';
                     stm_util.debug(l_foutmelding);
                     l_geboorteplaats :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string = '<naam>Geslachtsaanduiding</naam>' --'0410'       --geslachtsaanduiding
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006h';
                     stm_util.debug(l_foutmelding);
                     l_geslachtsaanduideling :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string = '<naam>Aanduiding naamgebruik</naam>' --'6110'    --aanduiding naamgebruik
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding := '00006i';
                     stm_util.debug(l_foutmelding);
                     l_aanduiding_naamgebruik :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  END IF;
               END IF;                                            -- deelnemer

               --
               l_foutmelding := '00007';

               stm_util.debug(l_foutmelding);
               IF     NOT l_categorie_01
                  AND l_categorie_05_55
                  AND NOT l_categorie_06
               THEN
                  --
                  -- voorlopig ervanuit gaande dat we ook de huidige partner registreren
                  IF l_prev_string = '<naam>A-nummer</naam>' --'0110'                     --A-nummer
                     AND l_string LIKE '<waarde>%'
                  THEN
                     l_A_nummer_ex :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string = '<naam>BSN</naam>' -- '0120'                       --BSN
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_BSN_ex :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string = '<naam>Voornamen</naam>' -- '0210'                 --voornamen
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_voornamen_ex :=
                        TO_CHAR (
                           SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9));
                  ELSIF l_prev_string =
                           '<naam>Voorvoegsels geslachtsnaam</naam>' --'0230'  --voorvoegsels
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_voorvoegsels_ex :=
                        TO_CHAR (
                           SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9));
                  ELSIF l_prev_string = '<naam>Geslachtsnaam</naam>' --'0240'     --geslachtsnaam
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_geslachtsnaam_ex :=
                        TO_CHAR (
                           SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9));
                  ELSIF l_prev_string = '<naam>Geboortedatum</naam>' -- '0310'    --geboortedatum
                        AND l_string LIKE '<waarde>%'
                  THEN
                     begin
                       l_geboortedatum_ex :=
                        to_date(SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9),'YYYYMMDD');
                     exception when others
                     then
                        l_melding := case when l_melding is not null
                                     then l_melding||'; '
                                     else null
                                     end||'Fout: Foutieve geboortedatum partner: '||SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);

                     end;
                  ELSIF l_prev_string = '<naam>Geboorteplaats</naam>' -- '0320'   --geboorteplaats
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_geboorteplaats_ex :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string = '<naam>Geslachtsaanduiding</naam>' --'0410'  --geslachtsaanduiding
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_geslachtsaanduideling_ex :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string = '<naam>Aanduiding naamgebruik</naam>' --'6110'   --aanduiding naamgebruik
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_aanduiding_naamgebruik_ex :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string =
                           '<naam>Datum ontbinding huwelijk/partnerschap</naam>' -- '0710'      --datum_einde_huwelijk
                        AND l_string LIKE '<waarde>%'
                  THEN
                     begin

                       l_datum_einde_huwelijk :=
                        to_date(SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9),'YYYYMMDD');
                        stm_util.debug('l_datum_einde_huwelijk; '||l_datum_einde_huwelijk);
                     exception when others
                     then
                        l_melding := case when l_melding is not null
                                     then l_melding||'; '
                                     else null
                                     end||'Fout: Foutieve einddatum huwelijk: '||SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);

                     end;
                  ELSIF l_prev_string =
                           '<naam>Reden ontbinding huwelijk/partnerschap</naam>' --'0740'      --reden_einde_huwelijk
                        AND l_string LIKE '<waarde>%'
                  THEN
                     l_reden_einde_huwelijk :=
                        SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                  ELSIF l_prev_string =
                           '<naam>Datum huwelijk/partnerschap</naam>' -- '0610' --Datum huwelijk/partnerschap
                        AND l_string LIKE '<waarde>%'
                        --AND NOT l_opgeslagen_partner
                        --AND l_geslachtsnaam_ex IS NOT NULL
                  THEN
                     begin
                       l_datum_begin_huwelijk := to_date(SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9),'YYYYMMDD');
                       if l_index > 0
                       and l_opgeslagen_partner
                        then
                          stm_util.debug('l_datum_begin_huwelijk: '||l_datum_begin_huwelijk);
                          stm_util.debug('l_huw_exp_tab (l_index).datum_begin_huwelijk: '||l_huw_exp_tab (l_index).datum_begin_huwelijk);
                          stm_util.debug('l_huw_exp_tab (l_index).datum_einde_huwelijk: '||l_huw_exp_tab (l_index).datum_einde_huwelijk);

                          if l_datum_begin_huwelijk <> l_huw_exp_tab (l_index).datum_begin_huwelijk
                          and l_huw_exp_tab (l_index).datum_einde_huwelijk is not null
                          and l_huw_exp_tab (l_index).datum_einde_huwelijk - l_huw_exp_tab (l_index).datum_begin_huwelijk > 61
                          then
                            -- l_datum_begin_huwelijk := l_huw_exp_tab (l_index).datum_begin_huwelijk; -- JPG 15-03-2011
                            --
                            l_melding := case when l_melding is not null
                                         then l_melding||'; '
                                         else null
                                         end||'Let op: 61 of meer dagen verschil: geen flitsscheiding?'; -- JPG 15-03-2011 (Fout is Let op geworden en 61 ipv 16)
                          end if;
                        end if;
                     exception when others
                     then
                       l_melding := case when l_melding is not null
                                    then l_melding||'; '
                                    else null
                                    end||'Fout: Foutieve begindatum huwelijk: '||SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                     end;
                     --
                     -- leg waarden vast in pl/sql tabel
                     --

                     IF NOT l_opgeslagen_partner
                     THEN
                       l_foutmelding := 'Legvast1: ' || l_BSN;
                       stm_util.debug(l_foutmelding);
                       stm_util.debug('l_melding Legvast1: '||l_melding);
                       stm_util.debug('l_melding_dln Legvast1: '||l_melding_dln);
                       l_index := l_index + 1;
                       l_huw_exp_tab (l_index).filename             := l_filename;
                       l_huw_exp_tab (l_index).A_nummer             := l_A_nummer;
                       l_huw_exp_tab (l_index).BSN                  := l_BSN;
                       l_huw_exp_tab (l_index).voornamen            := l_voornamen;
                       l_huw_exp_tab (l_index).voorvoegsels         := l_voorvoegsels;
                       l_huw_exp_tab (l_index).naam                 := l_geslachtsnaam;
                       l_huw_exp_tab (l_index).geslacht             := l_geslachtsaanduideling;
                       l_huw_exp_tab (l_index).geboortedatum        := l_geboortedatum;
                       l_huw_exp_tab (l_index).overlijdensdatum     := l_overlijdensdatum;
                       l_huw_exp_tab (l_index).A_nummer_ex          := l_A_nummer_ex;
                       l_huw_exp_tab (l_index).BSN_ex               := l_BSN_ex;
                       l_huw_exp_tab (l_index).voornamen_ex         := l_voornamen_ex;
                       l_huw_exp_tab (l_index).voorvoegsels_ex      := l_voorvoegsels_ex;
                       l_huw_exp_tab (l_index).naam_ex              := l_geslachtsnaam_ex;
                       l_huw_exp_tab (l_index).geslacht_ex          := l_geslachtsaanduideling_ex;
                       l_huw_exp_tab (l_index).geboortedatum_ex     := l_geboortedatum_ex;
                       l_huw_exp_tab (l_index).datum_einde_huwelijk := l_datum_einde_huwelijk;
                       l_huw_exp_tab (l_index).reden_einde_huwelijk := l_reden_einde_huwelijk;
                       l_huw_exp_tab (l_index).datum_begin_huwelijk := l_datum_begin_huwelijk;
                       l_huw_exp_tab (l_index).foutmelding          := case when l_melding_dln is not null
                                                                            and l_melding is not null
                                                                            then l_melding_dln||'; '
                                                                            when l_melding_dln is not null
                                                                            and l_melding is null
                                                                            then l_melding_dln
                                                                            else null
                                                                            end||l_melding;

                       --
                       -- resetten loop waarden partnerschap(pen)/huwelijk(en)
                       --
                       l_A_nummer_ex := '';
                       l_BSN_ex := '';
                       l_voornamen_ex := '';
                       l_voorvoegsels_ex := '';
                       l_geslachtsnaam_ex := '';
                       l_geboortedatum_ex := '';
                       l_geboorteplaats_ex := '';
                       l_geslachtsaanduideling_ex := '';
                       l_aanduiding_naamgebruik_ex := '';
                       l_datum_einde_huwelijk := '';
                       l_reden_einde_huwelijk := '';
                       l_datum_begin_huwelijk := '';
                       l_opgeslagen_partner := TRUE;
                       l_geldigheidsdatum := '';
                       l_melding := null;
                     END IF;
                  END IF;

                  -- oppakken doorgegeven mutaties gba-v
                  IF l_opgeslagen_partner
                  THEN
                     l_foutmelding := 'oppakken doorgegeven mutaties gba-v';
                     --
                     stm_util.debug(l_foutmelding);
                     stm_util.debug('l_huw_exp_tab (l_index).datum_begin_huwelijk: '||l_huw_exp_tab (l_index).datum_begin_huwelijk);
                     stm_util.debug('l_datum_begin_huwelijk: '||l_datum_begin_huwelijk);
                     stm_util.debug('l_melding oppakken doorgegeven mutaties gba-v: '||l_melding);
                     stm_util.debug('l_melding_dln oppakken doorgegeven mutaties gba-v: '||l_melding_dln);
                     --
                     IF l_huw_exp_tab (l_index).BSN = l_BSN
                        AND l_huw_exp_tab (l_index).datum_begin_huwelijk IS NOT NULL
                        AND l_datum_begin_huwelijk IS NOT NULL
                        AND l_datum_begin_huwelijk <
                              l_huw_exp_tab (l_index).datum_begin_huwelijk
                     THEN
                        --
                        -- mutatie update in pl/sql tabel
                        --
                        l_huw_exp_tab (l_index).datum_begin_huwelijk := l_datum_begin_huwelijk;
                        l_huw_exp_tab (l_index).foutmelding := case when l_melding_dln is not null
                                                                    and l_melding is not null
                                                                    then l_melding_dln||'; '
                                                                    when l_melding_dln is not null
                                                                    and l_melding is null
                                                                    then l_melding_dln
                                                                    else null
                                                                    end||l_melding;
                        --
                        -- resetten loop waarden partnerschap(pen)/huwelijk(en)
                        l_datum_begin_huwelijk := '';
                        l_opgeslagen_partner := TRUE;
                     ELSIF l_huw_exp_tab (l_index).BSN = l_BSN
                           AND l_huw_exp_tab (l_index).datum_begin_huwelijk IS NULL
                           AND l_datum_begin_huwelijk IS NOT NULL
                     THEN
                        l_huw_exp_tab (l_index).datum_begin_huwelijk := l_datum_begin_huwelijk;
                        l_huw_exp_tab (l_index).foutmelding := case when l_melding_dln is not null
                                                                    and l_melding is not null
                                                                    then l_melding_dln||'; '
                                                                    when l_melding_dln is not null
                                                                    and l_melding is null
                                                                    then l_melding_dln
                                                                    else null
                                                                    end||l_melding;
                        -- resetten loop waarden partnerschap(pen)/huwelijk(en)
                        l_datum_begin_huwelijk := '';
                        l_opgeslagen_partner := TRUE;
                     ELSIF l_huw_exp_tab (l_index).BSN = l_BSN
                     AND   l_melding IS NOT NULL
                     THEN
                        --
                        -- mutatie update in pl/sql tabel
                        --
                        l_huw_exp_tab (l_index).foutmelding := case when l_melding_dln is not null
                                                                    and l_melding is not null
                                                                    then l_melding_dln||'; '
                                                                    when l_melding_dln is not null
                                                                    and l_melding is null
                                                                    then l_melding_dln
                                                                    else null
                                                                    end||l_melding;
                        --
                        -- resetten loop waarden partnerschap(pen)/huwelijk(en)
                        l_datum_begin_huwelijk := '';
                        --l_melding := '';
                        l_opgeslagen_partner := TRUE;
                     END IF;
                  END IF;
               END IF;                                          -- (ex)partner
               --
               --
               l_foutmelding := '00008';
               --
               stm_util.debug(l_foutmelding);
               --
               IF     NOT l_categorie_01
                  AND NOT l_categorie_05_55
                  AND l_categorie_06
               THEN
                  IF l_prev_string = '<naam>Datum overlijden</naam>' -- '0610' --Datum huwelijk/partnerschap
                     AND l_string LIKE '<waarde>%'
                  THEN
                     l_foutmelding :=
                        'eventueel aanvullen overlijdensdatum deelnemer ';
                     stm_util.debug(l_foutmelding);
                     begin
                       l_overlijdensdatum :=
                        to_date(SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9),'YYYYMMDD');
                     exception when others
                     then
                       l_melding_dln := case when l_melding_dln is not null
                                        then l_melding_dln||'; '
                                        else null
                                        end||'Foutieve datum overlijdensdatum dlnr: '||SUBSTR (l_string, 9, INSTR (l_string, '</w') - 9);
                     end;
                     --

                     -- Voor alle huwelijken overlijdingsdatum vullen
                     --
                     for i in l_huw_exp_tab.first .. l_huw_exp_tab.last
                     loop
                        if l_huw_exp_tab (i).BSN = l_huw_exp_tab (l_index).BSN
                        then
                          l_huw_exp_tab (i).overlijdensdatum := l_overlijdensdatum;
                        end if;
                     end loop;
                     --
                     /*IF l_opgeslagen
                     THEN
                        l_huw_exp_tab (l_index).overlijdensdatum :=
                           l_overlijdensdatum;
                     END IF;*/
                  END IF;
               END IF;

               --
               --
               --
               l_foutmelding := '00009';

               stm_util.debug(l_foutmelding);
               --
               IF l_string = '</gbavAntwoord>'
               THEN
                  IF NOT l_opgeslagen_partner AND NOT l_opgeslagen
                  THEN
                     stm_util.debug('l_datum_begin_huwelijk: '||l_datum_begin_huwelijk);
                     --
                     -- leg waarden vast in pl/sql tabel
                     l_foutmelding := 'Legvast2: ' || l_BSN;
                     stm_util.debug(l_foutmelding);
                     stm_util.debug('l_melding Legvast2: '||l_melding);
                     stm_util.debug('l_melding_dln Legvast2: '||l_melding_dln);
                     --
                     l_index                                      := l_index + 1;
                     l_huw_exp_tab (l_index).filename             := l_filename;
                     l_huw_exp_tab (l_index).A_nummer             := l_A_nummer;
                     l_huw_exp_tab (l_index).BSN                  := l_BSN;
                     l_huw_exp_tab (l_index).voornamen            := l_voornamen;
                     l_huw_exp_tab (l_index).voorvoegsels         := l_voorvoegsels;
                     l_huw_exp_tab (l_index).naam                 := l_geslachtsnaam;
                     l_huw_exp_tab (l_index).geslacht             := l_geslachtsaanduideling;
                     l_huw_exp_tab (l_index).geboortedatum        := l_geboortedatum;
                     l_huw_exp_tab (l_index).overlijdensdatum     := l_overlijdensdatum;
                     l_huw_exp_tab (l_index).A_nummer_ex          := l_A_nummer_ex;
                     l_huw_exp_tab (l_index).BSN_ex               := l_BSN_ex;
                     l_huw_exp_tab (l_index).voornamen_ex         := l_voornamen_ex;
                     l_huw_exp_tab (l_index).voorvoegsels_ex      := l_voorvoegsels_ex;
                     l_huw_exp_tab (l_index).naam_ex              := l_geslachtsnaam_ex;
                     l_huw_exp_tab (l_index).geslacht_ex          := l_geslachtsaanduideling_ex;
                     l_huw_exp_tab (l_index).geboortedatum_ex     := l_geboortedatum_ex;
                     l_huw_exp_tab (l_index).datum_einde_huwelijk := l_datum_einde_huwelijk;
                     l_huw_exp_tab (l_index).reden_einde_huwelijk := l_reden_einde_huwelijk;
                     l_huw_exp_tab (l_index).datum_begin_huwelijk := l_datum_begin_huwelijk;
                     l_huw_exp_tab (l_index).foutmelding          := case when l_melding_dln is not null
                                                                          and l_melding is not null
                                                                          then l_melding_dln||'; '
                                                                          when l_melding_dln is not null
                                                                          and l_melding is null
                                                                          then l_melding_dln
                                                                          else null
                                                                          end||l_melding;
                  END IF;

                  --
                  -- resetten alle loop waarden partnerschap(pen)/huwelijk(en)
                  l_A_nummer := '';
                  l_BSN := '';
                  l_voornamen := '';
                  l_voorvoegsels := '';
                  l_geslachtsnaam := '';
                  l_geboortedatum := '';
                  l_overlijdensdatum := '';
                  l_geboorteplaats := '';
                  l_geslachtsaanduideling := '';
                  l_aanduiding_naamgebruik := '';
                  l_melding := null;
                  l_A_nummer_ex := '';
                  l_BSN_ex := '';
                  l_voornamen_ex := '';
                  l_voorvoegsels_ex := '';
                  l_geslachtsnaam_ex := '';
                  l_geboortedatum_ex := '';
                  l_geboorteplaats_ex := '';
                  l_geslachtsaanduideling_ex := '';
                  l_aanduiding_naamgebruik_ex := '';
                  l_datum_einde_huwelijk := '';
                  l_reden_einde_huwelijk := '';
                  l_datum_begin_huwelijk := '';
                  l_opgeslagen := FALSE;
                  l_opgeslagen_partner := FALSE;
                  l_geldigheidsdatum := '';
                  l_melding := null;
                  l_melding_dln := null;
                  --
                  -- insert records
                  --
                  l_foutmelding := '00010';
                  stm_util.debug(l_foutmelding);
                  --dbms_output.put_line('Aanroep insert_resultaat in DIRECT_XML, bsn='||l_huw_exp_tab (l_index).BSN);
                  insert_resultaat (p_huw_tab          => l_huw_exp_tab
                                   ,p_datum_verwerking => l_datum_verwerking
                                   , p_bestandsnaam => p_bestandsnaam);
                  --
                  --dbms_output.put_line('Na de insert_resultaat in DIRECT_XML, bsn='||l_huw_exp_tab (l_index).BSN);
                  -- zet de index op het juiste cijfer
                  l_index2:=l_signaallijst_tab.count;
                  --
                  -- leeg plsql tabel
                  --
                  l_huw_exp_tab := l_empty_tab;
                  p_verwerkt := 'J';
               END IF;                                                      --
            END IF;

            -- verwerking fouten
            IF l_fouten
            THEN
               --
               stm_util.debug(l_string);
               --
               l_foutmelding := '00100';
               --
               IF l_string = '</batch:gbavFouten>'
               THEN
                  EXIT;
               END IF;
               --
               l_foutmelding := '00105';
               --
               IF l_string LIKE '<foutOmschrijving>%'
               THEN
                  l_foutmelding := '00110';
                  l_melding := SUBSTR (l_string, 19, INSTR (l_string, '</f') - 19);
               ELSIF l_string LIKE '<internKenmerk>%'
               THEN
                  l_foutmelding := '00115';
                  l_bsn :=
                     TO_CHAR (
                        SUBSTR (l_string, 16, INSTR (l_string, '</i') - 16));
                     IF l_bsn <> l_prev_bsn
                     THEN
                        l_prev_bsn := l_bsn;
                     END IF;
               ELSIF l_string = '</gbavFout>' AND l_bsn IS NOT NULL
               THEN
                  l_foutmelding := 'Plaats fout om te insert in foutentabel: ' || l_BSN;
                  l_index := l_index + 1;
                  l_huw_exp_tab (l_index).filename := l_filename;
                  l_huw_exp_tab (l_index).BSN := l_BSN;
                  l_huw_exp_tab (l_index).foutmelding := l_melding;
                  l_foutmelding := '00120';
                  --
                                    -- zet de index op het juiste cijfer
                  l_index2:=l_signaallijst_tab.count;
                  --
                  --dbms_output.put_line('insert into RLE_EXP_XML_FOUTEN_GBAV in DIRECT_XML_PROCESSING voor bestand '||p_gbav_xml||' en bsn='||l_BSN);

                  insert into rle_exp_xml_fouten_gbav
                  (FILENAME
                   ,INSERTDATE
                   --  ,ID
                   ,BSN
                   ,DATUM_VERWERKT
                   ,FOUTMELDING)
                   values
                  (l_huw_exp_tab(l_index).FILENAME
                  ,sysdate
                  --,ID
                  ,l_huw_exp_tab(l_index).BSN
                  ,l_huw_exp_tab(l_index).DATUM_VERWERKT
                  ,ltrim(nvl(l_melding ,l_huw_exp_tab(l_index).FOUTMELDING),' ') );

                  p_verwerkt := 'J';
                  -- leeg plsql tabel
                  --
                  l_foutmelding := '00125';
                  l_huw_exp_tab := l_empty_tab;
                  l_bsn := '';
                  l_melding := null;
               END IF;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               EXIT;
         END;

         l_prev_string := l_string;
      END LOOP;
    END IF;
      --commit;
   END IF;
   --
   --commit;

   UTL_FILE.FCLOSE(l_file_id);
   stm_util.t_procedure := g_vorige_procedure;
   --
  --dbms_output.put_line('Einde procedure DIRECT_XML_PROCESSING voor bestand '||p_gbav_xml);
  --
   EXCEPTION
   WHEN OTHERS THEN
        UTL_FILE.FCLOSE(l_file_id);
        stm_util.t_foutmelding := substr(l_foutmelding||' '||sqlerrm, 1, 255);
        stm_util.debug ('Fout: ' || stm_util.t_foutmelding);
        --
        rollback;
        --
        stm_util.insert_job_statussen (ijs_volgnummer     =>  l_volgnummer
                                      ,ijs_uitvoer_soort  =>  'I'
                                      ,ijs_uitvoer_waarde =>  stm_util.t_programma_naam
                                                              || ' Procedure : '
                                                              || stm_util.t_procedure);
        --
        if stm_util.t_foutmelding is not null
        then
            stm_util.insert_job_statussen (ijs_volgnummer     => l_volgnummer
                                          ,ijs_uitvoer_soort  => 'I'
                                          ,ijs_uitvoer_waarde => substr ('Fout: '||stm_util.t_foutmelding, 1, 255));
        end if;
        --
        commit;
        --
        raise_application_error (-20000
                                , stm_app_error (stm_util.t_foutmelding, null, null) || ' ' || dbms_utility.format_error_backtrace
                                );
  end DIRECT_XML_PROCESSING;
/* Inserten gbav xml resultaat huwelijkse gegevens naar tijdelijke tabel */
PROCEDURE INSERT_RESULTAAT
 (P_HUW_TAB IN t_bron_exp_gba_tab
 ,P_DATUM_VERWERKING IN DATE
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS
/*-----------------------------------------------------------------------------------------------------------
   Datum             Auteur                    Wijzigingen
   ==========  =============    =====================================
   07-10-2013  AZM    Creatie
  -----------------------------------------------------------------------------------------------------------*/

  cursor c_controle(b_bsn in varchar2, b_bestandsnaam varchar2)
  is
    select distinct x.bsn
    ,               x.persoonsnummer
    ,               x.relatienummer
    ,               x.geboortedatum
    ,               x.overlijdensdatum
    , x.ind_verwerkt
    from rle_bron_ex_partners x
    where x.bsn=b_bsn
    and x.ind_verwerkt = 'N'
    and x.bestandsnaam = b_bestandsnaam
    and x.geboortedatum is not null;

  l_foutmelding          varchar2(300);
  l_voornamen_deeln      varchar2(200)  := '';
  l_voorvoegsels_deeln   varchar2(200)  := '';
  l_naam_deeln           varchar2(200)  := '';
  l_voornamen_partner    varchar2(200)  := '';
  l_voorvoegsels_partner varchar2(200)  := '';
  l_naam_partner         varchar2(200)  := '';
  l_index2               number         := 0;
  l_melding              varchar2(4000) := '';
  l_melding_bron         varchar2(4000) := '';
  l_bron_status          varchar2(15)   := '';
  l_prev_bsn             varchar2(200)  := '';
begin
  --
  g_vorige_procedure        := stm_util.t_procedure;
  stm_util.t_procedure      := 'insert_resultaat';
  --
  l_foutmelding:='Converteer de text';
  --
  --dbms_output.put_line('Begin proc insert_resultaat');

  if p_huw_tab.count > 0
  then
    for i in p_huw_tab.first .. p_huw_tab.last
    loop
      --
      l_foutmelding          := 'In loop pl/sql tabel';
      --
      l_foutmelding          := 'Converteer voornamen deelnemer';
      l_voornamen_deeln      := convert_txt(p_text=>p_huw_tab(i).voornamen);
      l_foutmelding          := 'Converteer voorvoegsels deelnemer';
      l_voorvoegsels_deeln   := convert_txt(p_text=>p_huw_tab(i).voorvoegsels);
      l_foutmelding          :='Converteer naam deelnemer';
      l_naam_deeln           := convert_txt(p_text=>p_huw_tab(i).naam);
      l_foutmelding          := 'Converteer voornamen partner';
      l_voornamen_partner    := convert_txt(p_text=>p_huw_tab(i).voornamen_ex);
      l_foutmelding          := 'Converteer voorvoegsels partner';
      l_voorvoegsels_partner := convert_txt(p_text=>p_huw_tab(i).voorvoegsels_ex);
      l_foutmelding          := 'Converteer naam partner';
      l_naam_partner         := convert_txt(p_text=>p_huw_tab(i).naam_ex);
      --
      l_foutmelding:='insert RLE_EXP_HUWELIJKEN_GBAV';
      --
      l_melding     := null;
      l_bron_status := '';
      --
      if p_huw_tab(i).foutmelding is not null
      then
        l_melding := p_huw_tab(i).foutmelding;
      end if;
      --
      for r_ctrl in c_controle(b_bsn=>p_huw_tab(i).BSN, b_bestandsnaam=> p_bestandsnaam )
      loop
        stm_util.debug('p_huw_tab(i).foutmelding: '||p_huw_tab(i).foutmelding);

        if to_char(r_ctrl.geboortedatum,'YYYYMMDD') <> to_char(p_huw_tab(i).GEBOORTEDATUM ,'YYYYMMDD')
        then
          --
          l_melding := case when l_melding is not null
                       then l_melding||'; '
                       else null
                       end||'Fout: Geboortedatum GBA '||to_char(p_huw_tab(i).GEBOORTEDATUM ,'YYYYMMDD')||' ongelijk aan bronbestand';
        end if;

        if to_char(nvl(r_ctrl.overlijdensdatum,sysdate),'YYYYMMDD') <> to_char(nvl(p_huw_tab(i).OVERLIJDENSDATUM,sysdate),'YYYYMMDD')
        then
          --
          l_melding := case when l_melding is not null
                       then l_melding||'; '
                       else null
                       end||'Fout: Overlijdensdatum GBA '||to_char(p_huw_tab(i).OVERLIJDENSDATUM ,'YYYYMMDD')||' ongelijk aan bronbestand';
          -- l_melding := 'Fout: Geboorte- en/of overlijdensdatum GBA ongelijk aan bronbestand';
        end if;
      end loop;

      insert into rle_exp_huwelijken_gbav
      (FILENAME,INSERTDATE
      ,A_NUMMER
      ,BSN
      ,VOORNAMEN
      ,VOORVOEGSELS
      ,NAAM,GESLACHT
      ,GEBOORTEDATUM
      ,OVERLIJDENSDATUM
      ,A_NUMMER_EX
      ,BSN_EX
      ,VOORNAMEN_EX
      ,VOORVOEGSELS_EX
      ,NAAM_EX
      ,GESLACHT_EX
      ,GEBOORTEDATUM_EX
      ,DATUM_BEGIN_HUWELIJK
      ,DATUM_EINDE_HUWELIJK
      ,REDEN_EINDE_HUWELIJK
      ,DATUM_VERWERKT
      ,FOUTMELDING)
       values
       (p_huw_tab(i).FILENAME
       ,P_DATUM_VERWERKING
       ,p_huw_tab(i).A_NUMMER
       ,p_huw_tab(i).BSN
       ,l_voornamen_deeln
       ,l_voorvoegsels_deeln
       ,l_naam_deeln,p_huw_tab(i).GESLACHT
       ,p_huw_tab(i).GEBOORTEDATUM
       ,p_huw_tab(i).OVERLIJDENSDATUM
       ,p_huw_tab(i).A_NUMMER_EX
       ,p_huw_tab(i).BSN_EX
       ,l_voornamen_partner
       ,l_voorvoegsels_partner
       ,l_naam_partner
       ,p_huw_tab(i).GESLACHT_EX
       ,p_huw_tab(i).GEBOORTEDATUM_EX
       ,p_huw_tab(i).DATUM_BEGIN_HUWELIJK
       ,p_huw_tab(i).DATUM_EINDE_HUWELIJK
       ,p_huw_tab(i).REDEN_EINDE_HUWELIJK
       ,p_huw_tab(i).DATUM_VERWERKT
       ,ltrim(nvl(l_melding ,p_huw_tab(i).FOUTMELDING),' '));

    end loop;
  end if;
  --commit;
  --dbms_output.put_line('Einde procedure insert_resultaat voor bron = '||p_bestandsnaam);
EXCEPTION
  WHEN OTHERS THEN
    stm_util.t_foutmelding := substr(l_foutmelding||' '||sqlerrm, 1, 255);
    stm_util.debug ('Fout: ' || stm_util.t_foutmelding);
    dbms_output.put_line('Exception in procedure insert_resultaat, fout: '|| stm_util.t_foutmelding);
    --
    rollback;
    --
    stm_util.insert_job_statussen (ijs_volgnummer     =>  g_volgnummer
                                  ,ijs_uitvoer_soort  =>  'I'
                                  ,ijs_uitvoer_waarde =>  stm_util.t_programma_naam
                                                          || ' Procedure : '
                                                          || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null
    then
      stm_util.insert_job_statussen (ijs_volgnummer     => g_volgnummer
                                    ,ijs_uitvoer_soort  => 'I'
                                    ,ijs_uitvoer_waarde => substr ('Fout: '||stm_util.t_foutmelding, 1, 255));
    end if;
    --
    commit;
    --
    raise_application_error (-20000
                            , stm_app_error (sqlerrm, null, null) || ' ' || dbms_utility.format_error_backtrace
                            );
end insert_resultaat;
/* verwerk ingelezen gbav xml resultaat en foutenbestand */
PROCEDURE VERWERK_RESULTAAT
 (P_BESTANDSNAAM IN VARCHAR2
 )
 IS

-- cursor voor het ophalen van de deelnemers (uit de brontabel) waarvoor bij het GBA de Huwelijkse-gegevens zijn aangevraagd
  cursor c_bron_aangevraagd (b_bestandsnaam in varchar2)
  is
   select   bron.*
     from   rle_bron_ex_partners bron
     where  bron.datum_opgenomen_gbabatch_dln is not null
     and    bron.datum_verwerkt_gba_dln_geg is null
     and    upper(bestandsnaam)      = upper(b_bestandsnaam)
     order by bsn
     for update;


-- cursor voor het controleren of de ontvangen datumeinde huwelijk van het GBA overeenkomt met de brontabel-ex-partners bij ons
   cursor c_chk_ddeinde_gba (b_bsn in varchar2, b_bestandsnaam in varchar2, b_ddeinde_huw in date)
   is
     select *
     from   rle_exp_huwelijken_gbav
     where  bsn                  = b_bsn
     and    filename             = b_bestandsnaam
     and    datum_einde_huwelijk = b_ddeinde_huw;
   r_chk_ddeinde_gba c_chk_ddeinde_gba%rowtype;

-- cursor voor controleren of de combinatie deelnemer en ex-partner in het gba resultaatbestand voorkomt met een fout
   cursor c_chk_antw_gba_fout(b_bsn in varchar2, b_bestandsnaam in varchar2, b_bsn_ex varchar2, b_a_nummer_ex varchar2, b_geboortedatum_ex date)
   is
     select *
     from   rle_exp_huwelijken_gbav
     where  bsn      = b_bsn
     and    filename = b_bestandsnaam
     and    foutmelding is not null
     and    (nvl(bsn_ex,'x') = nvl(b_bsn_ex,'y')
             or nvl(a_nummer_ex,'x') = nvl(b_a_nummer_ex,'y')
             or nvl(geboortedatum_ex,to_date('01-01-3000', 'dd-mm-yyyy'))=nvl(b_geboortedatum_ex,to_date('01-01-4000', 'dd-mm-yyyy')));
   r_chk_antw_gba_fout c_chk_antw_gba_fout%rowtype;

   cursor c_onb (b_bsn in varchar2)
   is
     select 1
     from rle_exp_huwelijken_gbav
     where bsn = b_bsn;

   r_onb    c_onb%rowtype;
   --
   l_foutmelding          varchar2(300) :='';
   l_foutmelding2         varchar2(300) :='';
   l_bsn_ontvangen        varchar2(10)  :='';
   l_bsn_gematcht         varchar2(10)  :='';
   l_ex_anr               varchar2(10)  := '';
   l_ex_bsn               varchar2(10)  := '';
   l_ex_voorletters       varchar2(30)  := '';
   l_ex_voornamen         varchar2(100) := '';
   l_ex_voorvoegsel       varchar2(30)  := '';
   l_ex_naam              varchar2(120) := '';
   l_ex_geboorte          date          := '';
   l_ex_geslacht          date          := '';
   l_huwelijk_begin       date          := '';
   l_ind_gba_fout_dln     varchar2(1)   := 'N';
   l_dat_verwerkt_gba     date;
   l_bsn_dln              varchar2(10)  := '000000000';

   --
begin
    --
  g_vorige_procedure        := stm_util.t_procedure;
  stm_util.t_procedure      := 'verwerk_resultaat';
  --
  l_foutmelding:='verwerk_resultaat';
  --
  dbms_output.put_line('Begin proc verwerk_resultaat');
  --

  for r_bron_aangevraagd in c_bron_aangevraagd(b_bestandsnaam => p_bestandsnaam)
  loop

    l_bsn_ontvangen    := 'J';
    l_bsn_gematcht     := 'J';
    l_ex_anr           := '';
    l_ex_bsn           := '';
    l_ex_voorletters   := '';
    l_ex_voornamen     := '';
    l_ex_voorvoegsel   := '';
    l_ex_naam          := '';
    l_ex_geboorte      := '';
    l_ex_geslacht      := '';
    l_ind_gba_fout_dln := 'N';

     -- controleer of einddatum huwelijk overeenkomt met gba
    open c_chk_ddeinde_gba
    ( b_bsn          => r_bron_aangevraagd.bsn
    , b_bestandsnaam => p_bestandsnaam
    , b_ddeinde_huw  => r_bron_aangevraagd.huwelijk_datumeinde);
    --
    fetch c_chk_ddeinde_gba into r_chk_ddeinde_gba;

    --Indien datumeinde overeenkomt neem gegevens ex over in bron en zet de datum_verwerkt_gba_dln_geg = 'J' en de indicatie GBA fout dln = 'N'
    if c_chk_ddeinde_gba%found -- match huwelijk einddatum tussen bron en gba
    then
      l_ind_gba_fout_dln         := 'N';
      l_dat_verwerkt_gba         := sysdate;
      l_ex_anr                   := r_chk_ddeinde_gba.A_nummer_ex;
      l_ex_bsn                   := r_chk_ddeinde_gba.BSN_ex;
      l_ex_voorletters           := COMP_GBA.BEPAAL_VOORLETTERS(r_chk_ddeinde_gba.voornamen_ex);
      l_ex_voornamen             := r_chk_ddeinde_gba.voornamen_ex;
      l_ex_voorvoegsel           := r_chk_ddeinde_gba.voorvoegsels_ex;
      l_ex_naam                  := r_chk_ddeinde_gba.naam_ex;
      l_foutmelding              := r_chk_ddeinde_gba.foutmelding;
      l_ex_geboorte              := r_chk_ddeinde_gba.geboortedatum_ex;
      l_ex_geslacht              := r_chk_ddeinde_gba.geslacht_ex;
      l_huwelijk_begin           := r_chk_ddeinde_gba.datum_begin_huwelijk;

      close c_chk_ddeinde_gba;


    else
      --
      close c_chk_ddeinde_gba;

      open c_chk_antw_gba_fout(b_bsn              => r_bron_aangevraagd.bsn --flitsscheiding + datums
                             , b_bestandsnaam     => p_bestandsnaam
                             , b_bsn_ex           => r_bron_aangevraagd.bsn_ex
                             , b_a_nummer_ex      => r_bron_aangevraagd.a_nummer_ex
                             , b_geboortedatum_ex => r_bron_aangevraagd.geboortedatum_ex );
      fetch c_chk_antw_gba_fout into r_chk_antw_gba_fout;
        --Indien bsn voorkomt, dan zit er een fout in het antwoord in de gbav xml resultaat bestand, ook dan de gba-gegevens overnemen in de brontabel
      if c_chk_antw_gba_fout%found
      then

        l_foutmelding        := r_chk_antw_gba_fout.foutmelding;
        l_ex_anr             := r_chk_antw_gba_fout.A_nummer_ex;
        l_ex_bsn             := r_chk_antw_gba_fout.BSN_ex;
        l_ex_voorletters     := COMP_GBA.BEPAAL_VOORLETTERS(r_chk_antw_gba_fout.voornamen_ex);
        l_ex_voornamen       := r_chk_antw_gba_fout.voornamen_ex;
        l_ex_voorvoegsel     := r_chk_antw_gba_fout.voorvoegsels_ex;
        l_ex_naam            := r_chk_antw_gba_fout.naam_ex;
        l_ex_geboorte        := r_chk_antw_gba_fout.geboortedatum_ex;
        l_ex_geslacht        := r_chk_antw_gba_fout.geslacht_ex;
        l_ind_gba_fout_dln   := 'N';
        l_dat_verwerkt_gba   := sysdate;

        close  c_chk_antw_gba_fout;


      else
        close c_chk_antw_gba_fout;

        if l_bsn_dln = r_bron_aangevraagd.bsn
         then

           open c_onb (r_bron_aangevraagd.bsn);
           fetch c_onb into r_onb;

           if c_onb%found
           then
             l_foutmelding      := 'Echtscheiding is niet bekend bij het GBA.';
             l_ind_gba_fout_dln := 'J';
             l_dat_verwerkt_gba := sysdate;
           end if;
           close c_onb;
        else

          l_bsn_gematcht := 'N'; --bsn niet gevonden in resultaatbestand

        end if;
      end if;

    end if;

    if l_bsn_ontvangen = 'J' and l_bsn_gematcht = 'J'
    then
      update RLE_BRON_EX_PARTNERS
      set foutmelding                = l_foutmelding
        , gba_ex_anr                 = l_ex_anr
        , gba_ex_bsn                 = l_ex_bsn
        , gba_ex_voornamen           = l_ex_voornamen
        , gba_ex_voorletters         = l_ex_voorletters
        , gba_ex_voorvoegsel         = l_ex_voorvoegsel
        , gba_ex_naam                = l_ex_naam
        , gba_ex_datgeboorte         = l_ex_geboorte
        , gba_ex_geslacht            = l_ex_geslacht
        , huwelijk_datumbegin        = l_huwelijk_begin --22-1-2014 mal ivm flitsscheiding
        , datum_verwerkt_gba_dln_geg = l_dat_verwerkt_gba
        , ind_gba_fout_dln           = l_ind_gba_fout_dln
      where current of c_bron_aangevraagd;
    end if;

    l_bsn_dln := r_bron_aangevraagd.bsn;

  end loop;

  --commit;
  dbms_output.put_line('Einde procedure verwerk_resultaat');
  --
EXCEPTION
  WHEN OTHERS THEN
    stm_util.t_foutmelding := substr(l_foutmelding||' '||sqlerrm, 1, 255);
    stm_util.debug ('Fout: ' || stm_util.t_foutmelding);
    dbms_output.put_line('Exception in procedure verwerk_resultaat, fout: '|| stm_util.t_foutmelding);
    --
    rollback;
    --
    stm_util.insert_job_statussen (ijs_volgnummer     =>  g_volgnummer
                                  ,ijs_uitvoer_soort  =>  'I'
                                  ,ijs_uitvoer_waarde =>  stm_util.t_programma_naam
                                                          || ' Procedure : '
                                                          || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null
    then
      stm_util.insert_job_statussen (ijs_volgnummer     => g_volgnummer
                                    ,ijs_uitvoer_soort  => 'I'
                                    ,ijs_uitvoer_waarde => substr ('Fout: '||stm_util.t_foutmelding, 1, 255));
    end if;
    --
    commit;
    --
    raise_application_error (-20000
                            , stm_app_error (sqlerrm, null, null) || ' ' || dbms_utility.format_error_backtrace
                            );
end verwerk_resultaat;
/* Foutbestand verwerken */
PROCEDURE VERWERK_RESULTAAT_FOUT
 (P_BESTANDSNAAM IN VARCHAR2
 )
 IS

cursor c_bron_aangevraagd(b_bestandsnaam in varchar2)
  is
   select   bron.*
     from   rle_bron_ex_partners bron
     where  bron.datum_opgenomen_gbabatch_dln is not null
     and    bron.datum_verwerkt_gba_dln_geg is null
     and    upper(bestandsnaam)      = upper(b_bestandsnaam)
     order by bsn
     for update;

-- cursor voor het controleren of de deelnemer (waarvoor huw-geg aangevraagd is bij het GBA) voorkomt in het gbav resultaat foutenbestand
   cursor c_chk_fouten_gba(b_bsn in varchar2, b_bestandsnaam in varchar2)
   is
     select *
     from   rle_exp_xml_fouten_gbav
     where  bsn      = b_bsn
     and    filename = b_bestandsnaam;

r_chk_fouten_gba       c_chk_fouten_gba%rowtype;
l_ind_gba_fout_dln     varchar2(1)   := 'N';
l_dat_verwerkt_gba     date;
l_foutmelding          varchar2(300) := '';
begin

  g_vorige_procedure        := stm_util.t_procedure;
  stm_util.t_procedure      := 'verwerk_resultaat_fouten';
  --
  l_foutmelding:='verwerk_resultaat_fouten';
  --
  dbms_output.put_line('Begin proc verwerk_resultaat_fouten');
  --

  for r_bron_aangevraagd in c_bron_aangevraagd(b_bestandsnaam => p_bestandsnaam)
  loop

    open c_chk_fouten_gba(b_bsn=>r_bron_aangevraagd.bsn, b_bestandsnaam=> p_bestandsnaam);
    fetch c_chk_fouten_gba into r_chk_fouten_gba;
    -- Zoeken in het foutenbestand
    if c_chk_fouten_gba%found
    then

      l_ind_gba_fout_dln         := 'J';
      l_dat_verwerkt_gba         := sysdate;
      l_foutmelding              := r_chk_fouten_gba.foutmelding;

      update rle_bron_ex_partners
      set datum_verwerkt_gba_dln_geg = l_dat_verwerkt_gba
        , ind_gba_fout_dln           = l_ind_gba_fout_dln
        , foutmelding                = l_foutmelding
      where current of c_bron_aangevraagd;
    end if;
    close c_chk_fouten_gba;

  end loop;

  --commit;
  dbms_output.put_line('Einde procedure verwerk_resultaat');
  --
EXCEPTION
  WHEN OTHERS THEN
    stm_util.t_foutmelding := substr(l_foutmelding||' '||sqlerrm, 1, 255);
    stm_util.debug ('Fout: ' || stm_util.t_foutmelding);
    dbms_output.put_line('Exception in procedure verwerk_resultaat_fout, fout: '|| stm_util.t_foutmelding);
    --
    rollback;
    --
    stm_util.insert_job_statussen (ijs_volgnummer     =>  g_volgnummer
                                  ,ijs_uitvoer_soort  =>  'I'
                                  ,ijs_uitvoer_waarde =>  stm_util.t_programma_naam
                                                          || ' Procedure : '
                                                          || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null
    then
      stm_util.insert_job_statussen (ijs_volgnummer     => g_volgnummer
                                    ,ijs_uitvoer_soort  => 'I'
                                    ,ijs_uitvoer_waarde => substr ('Fout: '||stm_util.t_foutmelding, 1, 255));
    end if;
    --
    commit;
    --
    raise_application_error (-20000
                            , stm_app_error (sqlerrm, null, null) || ' ' || dbms_utility.format_error_backtrace
                            );
end verwerk_resultaat_fout;
/* Converteer de text */
FUNCTION CONVERT_TXT
 (P_TEXT IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
l_foutmelding varchar2(300):='';
l_string varchar2(300):='';
begin
    --
    g_vorige_procedure        := stm_util.t_procedure;
    stm_util.t_procedure      := 'CONVERT_TXT';
    --
    l_foutmelding:='Converteer de text';
    --
    l_string:=p_text;
    if LENGTH (REPLACE (TRANSLATE(l_string,'Ã©«Ä‡Å¼¨¶¤ž§¯£ÄŸ', ' ' ), ' ', '' ) )< LENGTH (REPLACE (l_string, ' ', '' ) )
    then
      begin
        l_string:=CONVERT(l_string,'WE8MSWIN1252','UTF8');
      exception when others then
        null;
      end;
    end if;
    return(l_string);
    --
    stm_util.t_procedure := g_vorige_procedure;
    --
    exception when others
    then

        stm_util.t_foutmelding := substr(l_foutmelding||' '||sqlerrm, 1, 255);
        stm_util.debug ('Fout: ' || stm_util.t_foutmelding);
        dbms_output.put_line('Exception in procedure CONVERT_TXT voor bron, fout: '|| stm_util.t_foutmelding);
        --
        rollback;
        --
        stm_util.insert_job_statussen (ijs_volgnummer     =>  g_volgnummer
                                      ,ijs_uitvoer_soort  =>  'I'
                                      ,ijs_uitvoer_waarde =>  stm_util.t_programma_naam
                                                              || ' Functie : '
                                                              || stm_util.t_procedure);
        --
        if stm_util.t_foutmelding is not null
        then
            stm_util.insert_job_statussen (ijs_volgnummer     => g_volgnummer
                                          ,ijs_uitvoer_soort  => 'I'
                                          ,ijs_uitvoer_waarde => substr ('Fout: '||stm_util.t_foutmelding, 1, 255));
        end if;
        --
        commit;
        --
        raise_application_error (-20000
                                , stm_app_error (sqlerrm, null, null) || ' ' || dbms_utility.format_error_backtrace
                                );

  end convert_txt;
/* Verwerken huwelijkse gegevens ex-partners */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS

  l_foutmelding      varchar2(300);
  l_bestand          varchar2(30);
  l_volgnummer       stm_job_statussen.volgnummer%type := 0;
  l_aantal           number := 0;
  e_fout_bestand     exception;
  cn_package         constant varchar2(30) := 'RLE_VERW_XML_HUW_EXEN';
  cn_script_naam     constant varchar2(30) := 'Verwerk gbav xml huw-geg';
  cn_module          constant varchar2(10) := 'VERWERK';
  --
  cursor c_aantallen (b_bestandsnaam in varchar2)
  is
   select count (distinct bsn) deelnemers
     ,    count (1) recs
   from rle_bron_ex_partners bron
   where datum_verwerkt_gba_dln_geg is not null
     and ind_gba_fout_dln = 'N'
     and upper(bestandsnaam) = upper(b_bestandsnaam)
     and exists (select 1
                 from rle_exp_huwelijken_gbav exp
                 where exp.bsn = bron.bsn
                   and exp.datum_einde_huwelijk = bron.huwelijk_datumeinde);
  r_aantallen                   c_aantallen%rowtype;

  cursor c_count (b_bestandsnaam in varchar2)
  is
    select count (distinct bsn) deelnemers
     ,    count (1) recs
    from rle_bron_ex_partners bron
    where datum_verwerkt_gba_dln_geg is not null
      and ind_gba_fout_dln = 'J'
      and foutmelding = 'Echtscheiding is niet bekend bij het GBA.'
      and upper(bestandsnaam) = upper(b_bestandsnaam)
     and exists (select 1
                 from rle_exp_huwelijken_gbav exp
                 where exp.bsn = bron.bsn);
  r_count                   c_count%rowtype;

  cursor c_fout (b_bestandsnaam in varchar2)
  is
    select count (distinct bsn) deelnemers
      ,    count (1) recs
    from rle_bron_ex_partners bron
    where datum_verwerkt_gba_dln_geg is not null
      and ind_gba_fout_dln = 'J'
      and upper(bestandsnaam) = upper(b_bestandsnaam)
      and exists (select 1
                 from rle_exp_xml_fouten_gbav exp
                 where exp.bsn = bron.bsn);

  r_fout                   c_fout%rowtype;
  --input: aantal in bron wacht op verwerking
  cursor c_bron(b_bestandsnaam in varchar2)
  is
   select  count(distinct bsn) aantal_uniek
          ,count(*) aantal
     from   rle_bron_ex_partners bron
     where  bron.datum_opgenomen_gbabatch_dln is not null
     and    bron.datum_verwerkt_gba_dln_geg is null
     and    upper(bestandsnaam)      = upper(b_bestandsnaam)
   ;
   r_bron        c_bron%rowtype;
  -- telling aantal in bron aangevraagd en verwerkt
  cursor c_bron2 (b_filename in varchar2)
  is
    select count (distinct bsn) bsn
          ,count(*) aantal
    from rle_bron_ex_partners
    where upper(bestandsnaam) = upper(b_filename)
      and datum_opgenomen_gbabatch_dln is not null
      and datum_verwerkt_gba_dln_geg is not null;

    r_bron2    c_bron2%rowtype;

  -- telling aantal in bron aangevraagd en niet teruggekregen
  cursor c_bron3 (b_bestandsnaam in varchar2)
  is
    select count (distinct bsn) bsn
          ,count(*) aantal
    from rle_bron_ex_partners
    where upper(bestandsnaam) = upper(b_bestandsnaam)
      and datum_opgenomen_gbabatch_dln is not null
      and datum_verwerkt_gba_dln_geg is null;

     r_bron3       c_bron3%rowtype;
--
 l_verwerkt     varchar2(1);
begin

  dbms_output.enable (buffer_size => null);
  --stm_util.set_debug_on;
  stm_util.module_start     (cn_package);
  stm_util.t_script_naam    := upper(p_script_naam);
  stm_util.t_script_id      := p_script_id;
  stm_util.t_programma_naam := cn_package;
  stm_util.t_procedure      := cn_module;
  --
  stm_util.t_script_naam    := p_script_naam ;
  stm_util.t_script_id      := p_script_id;

  lijst (' ');
  lijst ('Verwerken GBA-V XML huwelijkse gegevens');
  lijst ( rpad('Start', 35)||': '||TO_CHAR (SYSDATE, 'DD-MM-YYYY HH24:MI:SS'));
  lijst ( ' ' );
  lijst ( rpad('-', 95, '-') );
  lijst ( ' ' );
  lijst ( 'Selectie echtscheidingen bronbestand o.b.v. Inputbestand: '||p_bestandsnaam);
  lijst ( '' );
  lijst ( rpad('=',95, '=') );
  lijst ( '' );

  select count(*)
  into l_aantal
  from rle_bron_ex_partners
  where bestandsnaam = p_bestandsnaam;

  if l_aantal = 0
  then
    raise e_fout_bestand;
  end if;
  --
  open c_bron (p_bestandsnaam);
  fetch c_bron into r_bron;
  close c_bron;

  lijst ('INPUT: ');
  lijst ('Aantal echtscheidingen in bronbestand waarvan de verwerking wacht op huwelijkse gegevens van GBA: '||r_bron.aantal||'.');
  lijst ('Dit betreft '||r_bron.aantal_uniek||' unieke deelnemers.');
  lijst ('');

  l_foutmelding:='Insert XML bestand';
  direct_xml_processing ( p_gbav_xml => 'gbav-resultaat.xml'
                        , p_dir      => 'RLE_INBOX'
                        , p_bestandsnaam => p_bestandsnaam
                        , p_verwerkt => l_verwerkt) ;

  if l_verwerkt = 'J'
  then

    verwerk_resultaat(p_bestandsnaam => p_bestandsnaam);

  else

    lijst ('Het bestand gbav-resultaat.xml is niet gevonden of kan niet verwerkt worden.');
    lijst ('');

  end if;

  direct_xml_processing ( p_gbav_xml => 'gbav-resultaat-fouten.xml'
                        , p_dir      => 'RLE_INBOX'
                        , p_bestandsnaam => p_bestandsnaam
                        , p_verwerkt => l_verwerkt) ;
  --
  if l_verwerkt = 'J'
  then

    verwerk_resultaat_fout (p_bestandsnaam => p_bestandsnaam);

  else

    lijst ('Het bestand gbav-resultaat-fouten.xml is niet gevonden of kan niet verwerkt worden.');
    lijst ('');

  end if;

    open c_bron2 (p_bestandsnaam);
    fetch c_bron2 into r_bron2;
    close c_bron2;

    lijst ('OUTPUT: ');
    lijst ('Aantal echtscheidingen in bronbestand waarvan de GBA-gegevens zijn ontvangen en verwerkt: '||r_bron2.aantal||'.');
    lijst ('Dit betreft '||r_bron2.bsn||' unieke deelnemers.');
    lijst ('');

    open c_bron3 (p_bestandsnaam);
    fetch c_bron3 into r_bron3;
    close c_bron3;

    lijst ('Aantal echtscheidingen in bronbestand waarvan de GBA-gegevens niet zijn gevonden in de GBA-bestanden: '||r_bron3.aantal||'.');
    lijst ('Dit betreft '||r_bron3.bsn||' unieke deelnemers.');
    lijst ('');

    open c_aantallen (p_bestandsnaam);
    fetch c_aantallen into r_aantallen;
    close c_aantallen;

    lijst ('UITSPLITSING NAAR VERWERKT GBA-BESTAND:');
    lijst ('Aantal echtscheidingen die gevonden zijn in het GBA bestand zonder fouten (gbav-resultaat.xml): '||r_aantallen.recs);
    lijst ('Voor deze echtscheidingen kunnen de persoonsgegevens van de ex-en bij GBA worden opgevraagd (indien ');
    lijst ('blijkt dat de ex-en nog niet bekend zijn in RLE.');
    lijst ('Dit betreft '||r_aantallen.deelnemers||' unieke deelnemers.');
    lijst ('');

    open c_count (p_bestandsnaam);
    fetch c_count into r_count;
    close c_count;

    lijst ('Aantal echtscheidingen die NIET gevonden zijn in het GBA-bestand zonder fouten (gbav-resultaat.xml) terwijl ');
    lijst ('de huwelijkse gegevens van de deelnemer daar wel in zitten: '||r_count.recs);
    lijst ('Voor deze echtscheidingen kunnen workflow cases worden aangemaakt.');
    lijst ('Dit betreft '||r_count.deelnemers||' unieke deelnemers.');
    lijst ('');

    open c_fout (p_bestandsnaam);
    fetch c_fout into r_fout;
    close c_fout;

    lijst ('Aantal echtscheidingen die gevonden zijn in het GBA bestand met fouten (gbav-resultaat-fouten.xml): '||r_fout.recs);
    lijst ('Voor deze echtscheidingen kunnen workflow cases worden aangemaakt.');
    lijst ('Dit betreft '||r_fout.deelnemers||' unieke deelnemers.');
    lijst ('');
    lijst ( rpad('Einde', 35)||': '||TO_CHAR (SYSDATE, 'DD-MM-YYYY HH24:MI:SS'));
  -- Job is met succes afgerond
  stm_util.insert_job_statussen (l_volgnummer, 'S', '0');
  stm_util.insert_job_statussen (l_volgnummer, 'I', stm_util.t_programma_naam||' is succesvol beeindigd.');
  --
  commit;
  stm_util.module_end;

  exception
  when e_fout_bestand
  then
    rollback;
    stm_util.insert_job_statussen(l_volgnummer
                                ,'S'
                                ,'1'
                                 );
    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                  ' procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  'De opgegeven bestandsnaam ('||p_bestandsnaam||') is onbekend.');
  commit;
  when others
  then
    stm_util.t_foutmelding := substr(l_foutmelding||' '||sqlerrm, 1, 255);
    stm_util.debug ('Fout: ' || stm_util.t_foutmelding);
    --
    rollback;
    --
    stm_util.insert_job_statussen (l_volgnummer, 'S', '1');
    --
    stm_util.insert_job_statussen (ijs_volgnummer     =>  l_volgnummer
                                  ,ijs_uitvoer_soort  =>  'I'
                                  ,ijs_uitvoer_waarde =>  stm_util.t_programma_naam||' Procedure : '|| stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null
    then
      stm_util.insert_job_statussen (ijs_volgnummer     => l_volgnummer
                                    ,ijs_uitvoer_soort  => 'I'
                                    ,ijs_uitvoer_waarde => substr ('Fout: '||stm_util.t_foutmelding, 1, 255));
    end if;
    --
    commit;
    --
    raise_application_error (-20000, stm_app_error (sqlerrm, null, null) || ' ' || dbms_utility.format_error_backtrace );
  end verwerk;

END RLE_VERW_XML_HUW_EXEN;
/