CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_VERW_KANAALVOORKEUR IS

/**************************************************************************************
   NAAM:  RLE_VERW_KANAALVOORKEUR
   DOEL:  Verwerken van kanaalvoorkeuren die via een csv file worden aangeleverd.

   NB.    rle_ext_kanaalvoorkeuren.csv (RLE_INBOX) wordt zichtbaar gemaakt via external table: RLE_EXT_KANAALVOORKEUREN

   WIJZIGINGSHISTORIE:
   Datum           Wie                       Wat
   ----------      -----------------------   ------------------------------------
   19-08-2016      Martin Vlaardingerbroek   Creatie

******************************************************************************************/

   type              aantal_tabtype
   is table of       pls_integer
   index by          varchar2(30)
   ;
   --
   cn_package                    constant    varchar2(30) := 'RLE_VERW_KANAALVOORKEUR';
   cn_herstart_id                constant    varchar2(30) := 'HERSTART_ID';
   cn_regelnummer_lijst          constant    varchar2(30) := 'REGELNUMMER_LIJST';
   cn_datum_tijd_formaat         constant    varchar2(30) := 'dd-mm-yyyy hh24:mi';

   --verschillende AANTAL labels voor de herstarttellingen
   cn_te_verwerken               constant    varchar2(30) := 'TE_VERWERKEN';
   cn_verwerkt                   constant    varchar2(30) := 'VERWERKT';
   cn_niet_verwerkt              constant    varchar2(30) := 'NIET_VERWERKT';

   -- Globale variabelen
   g_aantal_tab                     aantal_tabtype;   -- plsql tabel om aantalen vast te houden
   g_aantal_omschrijving            varchar2(30);     -- index variabelen voor G_AANTAL_TAB
   g_herstart_id                    pls_integer := 0;
   g_regelnummer_lijst              pls_integer := 1;
   --

/* Schrijf een regel weg naar de lijsten tabel */
PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
/* Voer een controle uit op de parameters */
PROCEDURE CHECK_PARAMETERS;
/* Initialiseer PLSQL-tabellen waarin de aantallen worden bijgehouden */
PROCEDURE INIT_AANTAL_TAB;
/* Initiele acties die bij een herstart uitgevoerd moeten worden */
PROCEDURE HERSTART_INIT;
/* Initiele acties uit te voeren bij een reguliere start */
PROCEDURE REGULIERE_START_INIT;
/* Schrijf alle gevonden totaal-aantallen weg naar het logverslag */
PROCEDURE TOTAAL_AANTALLEN_NAAR_LOG;
/* Voer alle nodige acties uit bij een COMMIT */
PROCEDURE PROC_COMMIT
 (P_HERSTART_ID IN varchar2
 );


/* Schrijf een regel weg naar de lijsten tabel */
PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
      --lijstnummer moet wel juist ingevuld worden
      cn_lijst          constant   number := 1;
begin
      stm_util.insert_lijsten(cn_lijst
                             ,g_regelnummer_lijst
                             ,p_text
                             );
      stm_util.debug ('LIJST :'|| p_text );
end lijst;
/* Voer een controle uit op de parameters */
PROCEDURE CHECK_PARAMETERS
 IS

  cursor c_ekv
  is
  select count(*)          aantal
  from   rle_ext_kanaalvoorkeuren
  ;
  r_ekv  c_ekv%rowtype;

  cn_module          constant    stm_util.t_procedure%type := cn_package||'.CHECK_PARAMETERS';
begin

  -----------------------------------------------------------
  -- Checks rondom de csv file
  -----------------------------------------------------------
  begin
     open  c_ekv;
     fetch c_ekv into r_ekv;
     close c_ekv;

  exception
   when others
   then
      if sqlcode = -29913
      then
        raise_application_error(-20000, stm_app_error('rle_ext_kanaalvoorkeuren.csv niet aanwezig in de RLE_INBOX (unix) directory.', cn_module ));
      end if;

  end;

  if nvl(r_ekv.aantal, 0) <= 0
  then
     raise_application_error(-20000, stm_app_error('Geen regels gevonden in rle_ext_kanaalvoorkeuren.csv', cn_module ));
  end if;

end check_parameters;
/* Initialiseer PLSQL-tabellen waarin de aantallen worden bijgehouden */
PROCEDURE INIT_AANTAL_TAB
 IS
begin
      g_aantal_tab(cn_te_verwerken)   := 0;
      g_aantal_tab(cn_verwerkt)       := 0;
      g_aantal_tab(cn_niet_verwerkt)  := 0;

end init_aantal_tab;
/* Initiele acties die bij een herstart uitgevoerd moeten worden */
PROCEDURE HERSTART_INIT
 IS
begin
      stm_util.debug('Een herstart');
      g_regelnummer_lijst := stm_util.lees_herstarttelling(cn_regelnummer_lijst);
      g_herstart_id := stm_util.lees_herstartsleutel(cn_herstart_id);
      -- lees de weggeschreven aantallen terug
      g_aantal_omschrijving := g_aantal_tab.first;
      while g_aantal_omschrijving is not null
      loop
         g_aantal_tab(g_aantal_omschrijving) := stm_util.lees_herstarttelling(g_aantal_omschrijving);
         g_aantal_omschrijving := g_aantal_tab.next(g_aantal_omschrijving);
      end loop;

      lijst('*** HERSTART  *** '||to_char(sysdate ,cn_datum_tijd_formaat));
      lijst('Vanaf ID :'||to_char(g_herstart_id));
      lijst(' ');
end herstart_init;
/* Initiele acties uit te voeren bij een reguliere start */
PROCEDURE REGULIERE_START_INIT
 IS
begin
      stm_util.debug ('Geen herstart maar initieele start');
      g_regelnummer_lijst := 1;
      g_herstart_id := 0;
         -- Zet de parameters op het logverslag
      lijst('*** VERWERKEN DOOR DEELNEMERS OPGEGEVEN KANAAL VOORKEUREN *** '||to_char(sysdate ,cn_datum_tijd_formaat));
      lijst('-----------------------------------------------------------------------------------------------');
      lijst(' ');
      --
end reguliere_start_init;
/* Schrijf alle gevonden totaal-aantallen weg naar het logverslag */
PROCEDURE TOTAAL_AANTALLEN_NAAR_LOG
 IS
      l_descript_lengte    pls_integer := 70;
      l_param_lengte       pls_integer := 15;
      l_scheiding          varchar2(10) := ' : ';
begin
    lijst(' ');
    lijst(' ');
    lijst('Tellingen ');
    lijst('-----------------------------------------------------------------------------------------------');

    lijst(lpad('Aantal te verwerken rijen in csv bestand' ,l_descript_lengte)
            ||l_scheiding||lpad(g_aantal_tab(cn_te_verwerken) ,l_param_lengte)
           );
    lijst(lpad('Aantal verwerkte rijen' ,l_descript_lengte)
            ||l_scheiding||lpad(g_aantal_tab(cn_verwerkt) ,l_param_lengte)
           );
    lijst(lpad('Aantal niet verwerkte rijen' ,l_descript_lengte)
            ||l_scheiding||lpad(g_aantal_tab(cn_niet_verwerkt) ,l_param_lengte)
           );
    lijst(' ');
end totaal_aantallen_naar_log;
/* Voer alle nodige acties uit bij een COMMIT */
PROCEDURE PROC_COMMIT
 (P_HERSTART_ID IN varchar2
 )
 IS
  cn_aantal_rec_commit       constant    pls_integer := 10000;
  --
  l_aantal_omschrijving      varchar2(60);
begin
  -- alleen als het juiste commit_totaal is bereikt, dan een commit actie uitvoeren
  -- PAS 13-02-2014 Gebruik maken van 'cn_pensioenbericht_ini' ipv 'cn_totaal_deelnemers' daar deze laatste voor PME niet wordt opgehoogd.
  if mod(g_aantal_tab(cn_te_verwerken) ,cn_aantal_rec_commit) = 0
  then
    -- leg het huidige HERSTART ID vast, en alle tellingen
    stm_util.schrijf_herstartsleutel(cn_herstart_id, p_herstart_id);
    stm_util.schrijf_herstarttelling(cn_regelnummer_lijst ,g_regelnummer_lijst);
    -- loop door de PLSQL-tabel met de totaal aantallen
    l_aantal_omschrijving := g_aantal_tab.first;
    while l_aantal_omschrijving is not null
    loop
      stm_util.schrijf_herstarttelling(l_aantal_omschrijving ,g_aantal_tab(l_aantal_omschrijving));
      l_aantal_omschrijving := g_aantal_tab.next(l_aantal_omschrijving);
    end loop;
    --
    commit;
    --
  end if;
end proc_commit;
/* Hoofdprocedure: Verwerken kanaal voorkeuren vanuit csv file */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_INITIELE_REGISTRATIE IN VARCHAR2 := 'J'
 )
 IS
  cn_module  constant    stm_util.t_procedure%type := cn_package||'.VERWERK';
  --
  cursor c_ekv (b_sort_sleutel in number)
  is
  select *
  from   (select case when ekv.relatienummer is null then ekv.persoonsnummer else ekv.relatienummer end sort_sleutel
          ,      ekv.*
          from   rle_ext_kanaalvoorkeuren ekv
         )
  where  sort_sleutel > to_char(b_sort_sleutel)
  order  by sort_sleutel nulls first
  ;

  cursor c_rel(b_relatienummer  in rle_mv_personen.relatienummer%type
              ,b_persoonsnummer in rle_mv_personen.persoonsnummer%type
              )
  is
  select rle.relatienummer
  ,      rle.geboortedatum
  ,      rle.datum_overlijden
  from   rle_mv_personen       rle
  where  ( (b_relatienummer is not null and
            rle.numrelatie      = b_relatienummer )
       or  (b_relatienummer is null and
            rle.persoonsnummer  = b_persoonsnummer )
         )
  ;
  r_rel  c_rel%rowtype;
  --
  cursor c_ccn(b_regeling     in rle_communicatie_categorieen.adm_code%type
              ,b_oms_comm_cat in rle_communicatie_categorieen.omschrijving%type
              )
  is
  select ccn.code         categorie
  ,      ccn.id           ccn_id
  from   rle_communicatie_categorieen ccn
  where  sysdate between ccn.datum_begin and nvl(ccn.datum_einde, sysdate)
  and    upper(ccn.adm_code) = upper(b_regeling)
  and    lower(ccn.omschrijving) = lower(b_oms_comm_cat)
  ;
  r_ccn  c_ccn%rowtype;

  cursor c_cck(b_ccn_id in rle_communicatie_cat_kanalen.ccn_id%type
              ,b_srt_com in rle_communicatie_cat_kanalen.dwe_wrddom_srtcom%type
              )
  is
  select cck.dwe_wrddom_srtcom  srt_com
  from   rle_communicatie_cat_kanalen cck
  where  cck.ccn_id = b_ccn_id
  and    upper(cck.dwe_wrddom_srtcom) = upper(b_srt_com)
  and    sysdate between cck.datum_begin and nvl(cck.datum_einde, sysdate)
  ;
  r_cck  c_cck%rowtype;

  cursor c_com( b_rle_numrelatie in rle_mv_personen.relatienummer%type
              , b_communicatie_categorie in rle_communicatie_categorieen.code%type
              )
  is
  select rkv.rle_numrelatie
  ,      rkv.dat_begin begindatum_kanaalvoorkeur
  ,      rkv.dat_einde einddatum_kanaalvoorkeur
  ,      rkv.authentiek
  ,      rkv.cck_id
  ,      ccn.id ccn_id
  ,      cck.dwe_wrddom_srtcom soort_cat_kanaal
  ,      ccn.code code_cat
  from   rle_kanaalvoorkeuren rkv
  ,      rle_communicatie_cat_kanalen cck
  ,      rle_communicatie_categorieen ccn
  where  rkv.cck_id = cck.id
  and    cck.ccn_id = ccn.id
  and    rkv.rle_numrelatie = b_rle_numrelatie
  and    ccn.code  = b_communicatie_categorie
  ;
  r_com  c_com%rowtype;

  --
  l_relatienummer   number;
  l_persoonsnummer  number;

  l_volgnummer               pls_integer := 1; -- volgnummer voor JOB_STATUSSEN
  l_vastleggen               boolean; -- boolean die bijhoudt of de gegevens uiteindelijk vastgelegd moeten worden

  --
begin
  execute immediate('alter session set optimizer_index_cost_adj = 100');
  --
  stm_util.module_start(cn_module);
  stm_util.t_script_naam    := p_script_naam;
  stm_util.t_script_id      := to_number (p_script_id);
  stm_util.t_programma_naam := cn_package;
  stm_util.t_tekst          := null;
  --
  -- controleer de parameters
  check_parameters;
  --
  init_aantal_tab;
  --
  if stm_util.herstart
  then
    herstart_init;
  else
    reguliere_start_init;
  end if;

  -- parameters afdrukken
  lijst('Parameters: ');
  lijst('----------- ');
  lijst('P_INITIELE_REGISTRATIE: '||p_initiele_registratie);
  lijst(' ');
  lijst(' ');

  --
  -- Hoofd cursor
  for r_ekv in c_ekv(b_sort_sleutel => g_herstart_id)
  loop

    g_aantal_tab(cn_te_verwerken) := g_aantal_tab(cn_te_verwerken) + 1;
    l_vastleggen := true;

    l_persoonsnummer := case when length(trim(translate(trim(r_ekv.persoonsnummer), '0123456789',' '))) is null then r_ekv.persoonsnummer else null end;
    l_relatienummer  := case when length(trim(translate(trim(r_ekv.relatienummer), '0123456789',' ')))  is null then r_ekv.relatienummer  else null end;

    if l_vastleggen
    then
       -- controleer of relatienummer en/of persoonsnummer is gevuld
       if l_persoonsnummer is null and l_relatienummer is null
       then
          l_vastleggen := false;
          g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
          lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
                'Persoon en Relatienummer zijn leeg of ongeldig');
       end if;
    end if;

    if l_vastleggen
    then
      -- controleer bestaan van de relatie
      r_rel := null;
      open c_rel(b_relatienummer => l_relatienummer
                ,b_persoonsnummer => l_persoonsnummer
                );
      fetch c_rel into r_rel;
      close c_rel;

      if r_rel.relatienummer is null
      then
        l_vastleggen := false;
        g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
        lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
              'Geen geldige relatie');
      end if;

      -- controleer of de relatie is overleden
      if r_rel.datum_overlijden is not null
      then
        l_vastleggen := false;
        g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
        lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
              'Relatie is overleden ('||to_char(r_rel.datum_overlijden, 'dd-mm-yyyy')||')');
      end if;


    end if;

    -- controleer de regeling
    if l_vastleggen
    then
       if r_ekv.regeling is null or not pdt_bestaat_regeling(r_ekv.regeling)
       then
          l_vastleggen := false;
          g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
          lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
                'Geen geldige regeling');
       end if;
    end if;

    -- controleer de communicatie categorie bij de opgegeven regeling
    if l_vastleggen
    then
       r_ccn := null;
       open c_ccn(b_regeling     => r_ekv.regeling
                 ,b_oms_comm_cat => r_ekv.communicatie_categorie
                 );
       fetch c_ccn into r_ccn;
       close c_ccn;

       if r_ccn.categorie is null
       then
          l_vastleggen := false;
          g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
          lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
                'Geen geldige communicatie categorie');
       end if;
    end if;

    -- controleer de kanaalvoorkeur (srt_com)
    if l_vastleggen
    then
       r_cck := null;
       open c_cck(b_ccn_id  => r_ccn.ccn_id
                 ,b_srt_com => r_ekv.kanaal_voorkeur
                 );
       fetch c_cck into r_cck;
       close c_cck;

       if r_cck.srt_com is null
       then
          l_vastleggen := false;
          g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
          lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
                'Geen geldige kanaalvoorkeur bij opgegeven communicatie categorie en regeling');
       end if;
    end if;

    -- indien P_INITIELE_REGISTRATIE = J, dan controleren of de opgegeven communicatie categorie al voorkomt bij de relatie
    --        alleen als de opgegeven communicatie categorie NIET bestaat bij de deelnemer wordt deze opgevoerd
    if l_vastleggen and
       nvl(p_initiele_registratie, 'J') = 'J'
    then
       r_com := null;
       open  c_com( b_rle_numrelatie         => r_rel.relatienummer
                  , b_communicatie_categorie => r_ccn.categorie
                  );
       fetch c_com into r_com;
       close c_com;

       if r_com.begindatum_kanaalvoorkeur is not null  -- categorie bestaat al
       then
          l_vastleggen := false;
          g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
          lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
                'Communicatie categorie bestaat reeds');
       end if;

    end if;

    --
    -- Als alle test geslaagd zijn dan mag het record vastgelegd worden via RLE0997 Bijwerken kanaalvoorkeuren
    --
    if l_vastleggen
    then
       stm_context.set_parameter( p_parameter => 'administratie'
                                , p_value     => upper(r_ekv.regeling)
                                );

       begin
          rle_kvr_verwerk(p_numrelatie => r_rel.relatienummer
                         ,p_dat_begin  => sysdate
                         ,p_dat_einde  => null
                         ,p_categorie  => r_ccn.categorie
                         ,p_srt_com    => r_cck.srt_com
                         ,p_authentiek => 'J'
                         );
          exception
            when others
            then
               l_vastleggen := false;
               g_aantal_tab(cn_niet_verwerkt) := g_aantal_tab(cn_niet_verwerkt) + 1;
               lijst('P'||r_ekv.persoonsnummer||';'||'R'||r_ekv.relatienummer||';'||r_ekv.regeling||';'||r_ekv.communicatie_categorie||';'||r_ekv.kanaal_voorkeur||';'||
                     'Onverwachte fout: '||sqlerrm);
       end;
    end if;

    if l_vastleggen
    then
       g_aantal_tab(cn_verwerkt) := g_aantal_tab(cn_verwerkt) + 1;
    end if;

    -- Check of er een COMMIT uitgevoerd moet worden
    proc_commit(r_ekv.sort_sleutel);

  end loop;
  --
  -- Leg de gevonden aantallen vast in het logverslag
  totaal_aantallen_naar_log;
  --
  lijst (' ');

  -- We zijn goed geeindigd
  lijst(' ***   E I N D E    V E R S L A G   ***  ');
  stm_util.verwijder_herstart;
  stm_util.insert_job_statussen(l_volgnummer,'S','0');

  commit;

  stm_context.clear_parameter( p_parameter => 'administratie');

  stm_util.module_end;

  exception
      when others
      then
        stm_util.t_foutmelding := sqlerrm;
        rollback;
        stm_util.debug ('Foutmelding  : ' || stm_util.t_foutmelding);
        stm_util.debug ('In programma : ' || stm_util.t_programma_naam);
        stm_util.debug ('In procedure : ' || stm_util.t_procedure);
        --
        stm_util.insert_job_statussen (l_volgnummer
                                      ,'S'
                                      ,'1'
                                      );
        stm_util.insert_job_statussen (l_volgnummer
                                      ,'I'
                                      ,    stm_util.t_programma_naam
                                        || ' procedure : '
                                        || stm_util.t_procedure
                                      );
        --
        if stm_util.t_foutmelding is not null
        then
          stm_util.insert_job_statussen (l_volgnummer
                                        ,'I'
                                        ,stm_util.t_foutmelding
                                        );
        end if;
        --
        if stm_util.t_tekst is not null
        then
          stm_util.insert_job_statussen (l_volgnummer
                                        ,'I'
                                        ,stm_util.t_tekst
                                        );
        end if;
        --
        commit;

END VERWERK;
END RLE_VERW_KANAALVOORKEUR;
/