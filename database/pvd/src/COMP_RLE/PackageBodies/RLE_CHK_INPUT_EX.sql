CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_CHK_INPUT_EX IS
--------------------------------------------------------------
  -- Wijzigingshistorie
  -- Wanneer     Wie               Wat
  -- ---------- ----------------- ---------------------------
  -- 07-08-2013   Rudie Siwpersad   creatie
  -- 20-2-2014     Monique v Alphen  Div wijzigingen nav herontwerp
-- 10-04-2014    Monique v Alphen   opbouwperioden checken en param echtscheiding vanaf toegevoegd

-------------------------------------------------------------
  -- global t.b.v. logverslag
   g_lijstnummer                      number        := 1;
   g_regelnr                          number        := 0;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
 begin
  stm_util.insert_lijsten( g_lijstnummer
                         , g_regelnr
                         , p_text
                         );
end lijst;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 ,P_REGELING IN VARCHAR2
 ,P_SCHEIDING_VROUW_VANAF IN DATE
 ,P_SCHEIDING_MAN_VANAF   IN DATE
 )
 IS
  -- cursor voor het selecteren van alle ingelezen data uit de tabel rle_input_ex
  cursor c_rec
  is
    select bsn_dln
         , regeling
         , bsn_ex
         , a_nummer_ex
         , naam_ex
         , naamgebruik_ex
         , voorvoegsels_ex
         , voornamen_ex
         , voorletters_ex
         , geslacht_ex
         , geboortedatum_ex
         , landcode_ex
         , huwelijk_datumbegin
         , huwelijk_datumeinde
         , reden_einde_huwelijk
    from   rle_input_ex_partners
    order by rowid
    for update
    ;
  -- cursor t.b.v. wegschrijven van uitval
  cursor c_uitval_data
  is
    select bsn_dln
         , regeling
         , bsn_ex
         , a_nummer_ex
         , naam_ex
         , naamgebruik_ex
         , voornamen_ex
         , voorvoegsels_ex
         , voorletters_ex
         , geslacht_ex
         , geboortedatum_ex
         , overlijdensdatum_ex
         , landcode_ex
         , straatnaam_ex
         , huisnummer_ex
         , huisletter_ex
         , toevoeging_ex
         , postcode_ex
         , gemeentenummer_ex
         , gemeentedeel_ex
         , huwelijk_datumbegin
         , huwelijk_datumeinde
         , reden_einde_huwelijk
         , foutmelding
         , bestandsnaam
    from   rle_input_ex_partners
    where  foutmelding is not null
      and foutmelding not like 'technische fout%'
    ;

  cursor c_ies
  is
      select bsn_dln
           ,rownum + 1 regelnr
           ,substr(foutmelding, 18, 350) foutmelding
    from   rle_input_ex_partners
    where  foutmelding like 'technische fout%'
;
  -- cursor t.b.v. controle deelneemer bekend in RLE
  cursor c_rle (b_bsn varchar2)
  is
   select rle.sofinummer
         ,rle.relatienummer
         ,rle.persoonsnummer
         ,case rle.geslacht
             when 'Man' then 'M'
             when 'Onbekend' then 'O'
             when 'Vrouw' then 'V'
             end as geslacht
    from rle_v_personen rle
    where rle.sofinummer = b_bsn
    ;
    cursor c_dnp (b_relatienummer number
                 ,b_regeling varchar2)
    is
      select 1
      from ret_v_deelnemerschappen
      where rle_numrelatie = b_relatienummer
        and rgg_code = b_regeling;

cursor c_aoe ( b_datum_einde date
             , b_beeindigde_opbouw varchar2 := 'N'
             , b_actieve_opbouw varchar2 := 'N'
             )
is
with a as ( select aoe.*
            ,      lag(reden_vervallen, 1, null) OVER (ORDER BY aoe.einddatum_opbouwperiode) AS prev_reden_vervallen
            from   ret_tmp_alle_opbouwperioden aoe
          )
select *
from   a
where  ( b_actieve_opbouw = 'N'
       and b_beeindigde_opbouw = 'N'
       and b_datum_einde >= a.begindatum_opbouwperiode
       and ( b_datum_einde <= a.einddatum_opbouwperiode
           or a.einddatum_opbouwperiode is null
           )
       )
or     ( b_actieve_opbouw = 'N'
       and b_beeindigde_opbouw = 'J'
       and b_datum_einde >= a.begindatum_opbouwperiode
       and b_datum_einde <= a.einddatum_opbouwperiode
       )
or     ( b_actieve_opbouw = 'J'
       and (  (b_datum_einde >= a.einddatum_actieve_opbouw)
           or ( b_datum_einde <= a.begindatum_actieve_opbouw
              and ( b_datum_einde <= a.einddatum_actieve_opbouw
                  or a.einddatum_actieve_opbouw is null
                  )
              )
           )
       and a.prev_reden_vervallen in ('UITGWRD','AVP','EMI')
       )
;
cursor c_conv( b_relatienummer ret_deelnemerschappen.rle_numrelatie%type
             , b_rgg_code ret_deelnemerschappen.rgg_code%type
             )
is
select gpr.*
from   ret_deelnemerschappen dnp
,      ret_vastg_pens vpr
,      ret_geconv_pens gpr
where  dnp.rle_numrelatie = b_relatienummer
and    dnp.id = vpr.dnp_id
and    vpr.peil_dat = to_date ('31-12-1993','dd-mm-yyyy')
and    vpr.id = gpr.vpr_id
and    gpr.pens_recht_srt = 'RECHTEN VOOR 1975'
and    dnp.rgg_code = b_rgg_code
;
  -- variablen t.b.v. programmatuur
  r_rec                  c_rec%rowtype;
  r_uitval_data          c_uitval_data%rowtype;
  r_rle                  c_rle%rowtype;
  r_dnp                  c_dnp%rowtype;
  r_aoe                  c_aoe%rowtype;
  r_conv                 c_conv%rowtype;
  --
  l_volgnummer           stm_job_statussen.volgnummer%type := 0;
  --
  l_aantal_gecontroleerd number := 0;
  l_teller               number := 0;
  l_database             varchar2(255);
  fh_stoplijst           utl_file.file_type;
  l_bestandsnaam         varchar2(255);
  --
  cn_module              constant varchar2(25) := 'rle_syn_bestand';
  --
  l_bsn_dln              varchar2 (9);
  l_geslacht_dln         varchar2 (10);
  l_regeling             varchar2 (5);
  l_afn_ind              varchar2 (1);
  l_bsn_ex               varchar2 (9);
  l_anummer_ex           varchar2 (30);
  l_naam_ex              varchar2 (120);
  l_naamgebruik_ex       varchar2 (1);
  l_voornamen_ex         varchar2 (100);
  l_voorvoegsels_ex      varchar2 (30);
  l_voorletters_ex       varchar2 (30);
  l_geslacht_ex          varchar2 (1);
  l_geboortedatum_ex     varchar2 (10);
  l_landcode_ex          varchar2 (5);
  l_huw_datumbegin       date;
  l_huw_datumeinde       varchar2(20);
  l_reden_einde_huw      varchar2(1);
  l_foutmelding          varchar2 (4000);
  l_check_rgg_code       boolean:= true;
  l_check_land           varchar2(1);
  l_fout                 number := 0;
  l_gevonden             boolean;
  --
  l_datumbegin           varchar2 (10);
  l_datumeinde           varchar2 (10);
  --
  l_relatienummer        number;
  l_persoonsnummer       number;
  l_afstemmen            varchar2(1);
  l_rol                  varchar2(5);

  l_sqlerrm              varchar2(350);
begin
  stm_util.t_procedure       := cn_module;
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_CHK_INPUT_EX';

  select case
           when regexp_substr ( global_name
                             , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;

  stm_util.debug ( 'Logbestand wordt aangemaakt' );

  -- Schrijven logbestand
  lijst ( rpad ('***** LOGVERSLAG RLE_CHK_INPUT_EX*****', 70)
               || ' DRAAIDATUM: ' || to_char ( sysdate, 'ddmmyyyy hh24:mi:ss' ));

  lijst ( ' ' );

  lijst ( '----------------------------------------------------------------------------------------------------------' );

  lijst ( ' ' );
  --
  execute immediate 'alter session set nls_date_format="dd-mm-yyyy"';
  --
  lijst ( '                 Het verwerkte bestand is: '||p_bestandsnaam );
  lijst ( '                 Regeling is '||p_regeling );
  lijst ( '                 Parameter p_scheiding_vrouw_vanaf is: '||nvl(to_char(p_scheiding_vrouw_vanaf, 'dd-mm-yyyy'), 'leeg'));
  lijst ( '                 Parameter p_scheiding_man_vanaf is: '||nvl(to_char(p_scheiding_man_vanaf, 'dd-mm-yyyy'), 'leeg'));
  lijst ( '==========================================================================================================' );
  --
  for r_rec in c_rec
  loop
    begin
      -- bestandsnaam wegschrijven naar tabel
      update rle_input_ex_partners
      set bestandsnaam = p_bestandsnaam
      where current of c_rec;
      --regeling wegschijven
      update rle_input_ex_partners
      set regeling = p_regeling
      where current of c_rec;
      -- bijhouden aantal gecontroleerde records
      l_aantal_gecontroleerd := l_aantal_gecontroleerd + 1;
      -- initialiseren variabelen voor controle
      l_regeling          := null;
      l_bsn_dln           := null;
      l_geslacht_dln      := null;
      l_afn_ind           := null;
      l_bsn_ex            := null;
      l_anummer_ex        := null;
      l_naam_ex           := null;
      l_naamgebruik_ex    := null;
      l_voorvoegsels_ex   := null;
      l_voornamen_ex      := null;
      l_voorletters_ex    := null;
      l_geslacht_ex       := null;
      l_geboortedatum_ex  := null;
      l_landcode_ex       := null;
      l_huw_datumbegin    := null;
      l_huw_datumeinde    := null;
      l_reden_einde_huw   := null;
      l_foutmelding       := null;
      --
      l_datumbegin        := null;
      l_datumeinde        := null;
      --
      l_regeling          := p_regeling;
      l_bsn_dln           := r_rec.bsn_dln;
      l_bsn_ex            := r_rec.bsn_ex;
      l_anummer_ex        := r_rec.a_nummer_ex;
      l_naam_ex           := r_rec.naam_ex;
      l_naamgebruik_ex    := r_rec.naamgebruik_ex;
      l_voornamen_ex      := r_rec.voornamen_ex;
      l_voorvoegsels_ex   := r_rec.voorvoegsels_ex;
      l_voorletters_ex    := r_rec.voorletters_ex;
      l_geslacht_ex       := r_rec.geslacht_ex;
      l_landcode_ex       := r_rec.landcode_ex;
      l_reden_einde_huw   := r_rec.reden_einde_huwelijk;
      l_geboortedatum_ex  := r_rec.geboortedatum_ex;
      l_huw_datumeinde    := r_rec.huwelijk_datumeinde;

      -- controleren ingelezen data op:
      -- 1 en 2.  BSN deelnemer is verplicht en is bekend in RLE.
      --
      if l_bsn_dln is null
      then
        l_foutmelding := l_foutmelding ||'BSN deelnemer is niet gevuld';
      else
        -- voorloopnul plaatsen
        if length (l_bsn_dln) <= 9
        then
          l_bsn_dln := lpad (substr ( l_bsn_dln,1 ), 9,0);
        end if;
        --
        open c_rle (l_bsn_dln);
        fetch c_rle into r_rle;
        if c_rle%notfound
        then
          l_foutmelding := l_foutmelding ||' deelnemer niet bekend in RLE';
        else
        --
          l_relatienummer := r_rle.relatienummer;
          l_persoonsnummer := to_number(r_rle.persoonsnummer);
          l_geslacht_dln := r_rle.geslacht;

        end if;

        close c_rle;

      end if;
      -- 3. Regeling is verplicht.
      --
      if l_regeling is null
      then
        l_foutmelding := l_foutmelding ||' Regeling is niet gevuld';
      else
      --  Regeling moet voorkomen in PDT.
        l_check_rgg_code := comp_pdt.pdt_bestaat_regeling ( p_reg_code => l_regeling );
        if l_check_rgg_code = false
        then
          l_foutmelding := l_foutmelding ||' regeling is niet bekend in PDT';
        end if;
      end if;
      -- 4. Einddatum huwelijkse periode is verplicht.
      --
      if r_rec.huwelijk_datumeinde is null
      then
        l_foutmelding := l_foutmelding ||' Eindatum huwelijk dient gevuld te zijn';
      else
      -- De einddatum huwelijkse periode moet een geldige datum
        begin
        l_datumeinde:= to_date ( r_rec.huwelijk_datumeinde , 'yyyymmdd' );
        exception
          when others
          then
            l_foutmelding := l_foutmelding ||' einddatum huwelijkse periode is geen geldige datum';
        end;
      end if;
      -- 5. Indien begindatum huwelijkse periode gevuld dan dient deze een geldige datum te zijn.
      --
      if r_rec.huwelijk_datumbegin is not null
      then
        begin
          l_huw_datumbegin:= to_date ( r_rec.huwelijk_datumbegin , 'yyyymmdd' );
        exception
          when others
          then
            l_foutmelding := l_foutmelding ||' begindatum huwelijkse periode is geen geldige datum';
        end;
      end if;
      -- 6. Indien naamgebruik ex-partner gevuld, dan dient dit een 'E' te zijn
      --
      if l_naamgebruik_ex is not null and l_naamgebruik_ex <>'E'
      then
        l_foutmelding := l_foutmelding ||' naamgebruik dient gevuld te zijn met een E';
      end if;
      -- 7. Indien geslacht gevuld dan moet dit M,V of O zijn.
      --
      if l_geslacht_ex is not null and upper (l_geslacht_ex) not in ('M', 'V', 'O')
      then
        l_foutmelding := l_foutmelding ||' geslacht dient een M,V of O te zijn';
      end if;
      -- 8. Indien landcode gevuld is, dan dient deze te bestaan in RLE_LANDEN
      --
      if l_landcode_ex is not null
      then
        l_check_land := RLE_LND_CHK ( p_lnd_cod => l_landcode_ex);
        if l_check_land = 'N'
        then
          l_foutmelding := l_foutmelding ||' landcode ex-partner onbekend in RLE';
        end if;
      end if;
      -- 9.  BSN en A-nummer niet zijn gevuld dan moet naam gevuld zijn.
      --
      if ( l_bsn_ex is null and l_anummer_ex is null and l_naam_ex is null )
      then
        l_foutmelding := l_foutmelding ||' BSN en A-nummer ex-partner niet gevuld, naam ex-parnter verplicht';
      end if;
        --10. administratie met GBA vergunning
       l_afstemmen := rle_chk_gba_afst (l_relatienummer, l_persoonsnummer);

      if l_afstemmen = 'N'
      then
        l_foutmelding := l_foutmelding||' persoon mag niet afgestemd worden met het GBA';
      end if;
      --11. persoon heeft rol WN en/of PR
      rle_gba_bepaal_rol( l_relatienummer
                         ,l_rol
                        );
      if l_rol not in ( 'WN', 'PR' )
      then

        l_foutmelding := l_foutmelding ||' persoon heeft alleen de rol begunstigde';

      end if;

      --12. scheiding vanaf vrouw datum parameter
      if p_scheiding_vrouw_vanaf is not null
        and l_geslacht_ex = 'V'
        or (l_geslacht_ex is null
          and l_geslacht_dln = 'M')
      then

        if l_datumeinde < p_scheiding_vrouw_vanaf
        then

          l_foutmelding := l_foutmelding||' datum einde scheiding ligt voor de parameter p_scheiding_vrouw_vanaf';

        end if;
      end if;

      --13. scheiding vanaf man datum parameter
      if p_scheiding_man_vanaf is not null
        and l_geslacht_ex = 'M'
        or (l_geslacht_ex is null
          and l_geslacht_dln = 'V')
      then
        if l_datumeinde < p_scheiding_man_vanaf
        then

          l_foutmelding := l_foutmelding||' datum einde scheiding ligt voor de parameter p_scheiding_man_vanaf';

        end if;
      end if;
      --14. persoon heeft deelnemerschap
      open c_dnp (l_relatienummer, p_regeling);
      fetch c_dnp into r_dnp;

      if c_dnp%notfound
      then
        l_foutmelding := l_foutmelding||' persoon heeft geen deelnemerschap';
        close c_dnp;
      else
        close c_dnp;

        ret_bepaal_alle_opbouwperioden ( p_relatienummer => l_relatienummer
                                       , p_regeling      => p_regeling
                                       );

        -- ligt datum einde in een opbouwperiode?

        open c_aoe (b_datum_einde => l_datumeinde);
        fetch c_aoe into r_aoe;
        l_gevonden := c_aoe%found;
        close c_aoe;
        --
        if l_gevonden
        then
          stm_util.debug('reden_vervallen = '|| r_aoe.reden_vervallen || ' prev_reden_vervallen = '|| r_aoe.prev_reden_vervallen );
          -- Als vorige reden vervallen in ('UITGWRD','AVP','EMI') zoeken naar actieve periode
          if r_aoe.prev_reden_vervallen in ('UITGWRD','AVP','EMI')
          then
            open c_aoe ( b_datum_einde => l_datumeinde
                       , b_actieve_opbouw => 'J'
                       );
            fetch c_aoe into r_aoe;
            l_gevonden := c_aoe%found;
            close c_aoe;
            --
            if l_gevonden
            then
              stm_util.debug('Deelname gevonden op basis van actieve opbouwperioden');
            end if;
          else
            stm_util.debug('Deelname gevonden op basis van opbouwperioden');
          end if;
          --
        end if;
        -- als er nog geen deelname is gevonden
        -- zoek naar opgebouwde rechten voor 1975
        if not(l_gevonden)
        then
          stm_util.debug('Zoeken naar opgebouwde rechten voor 1975');
          open c_conv( b_relatienummer => l_relatienummer
                     , b_rgg_code      => p_regeling
                     );
          fetch c_conv into r_conv;
          l_gevonden := c_conv%found;
          close c_conv;
          if not l_gevonden
          then
            l_foutmelding := l_foutmelding||' er zijn geen opbouwperiodes gevonden op het moment van de echtscheiding';

          end if;
        end if;
   --
      end if;

      -- foutmelding wegschrijven naar tabel rle_input_ex_partners.
      --
      if l_foutmelding is not null
      then
        update rle_input_ex_partners
        set foutmelding = l_foutmelding
        where current of c_rec
        ;
      end if;
    --
    exception
      when others
      then
        -- melding loggen en daarna doorgaan met de volgende relatie
      l_fout := l_fout + 1;
      l_sqlerrm := substr (sqlerrm, 1, 300);

      update rle_input_ex_partners
      set foutmelding = 'technische fout: '||l_sqlerrm
      where bsn_dln = l_bsn_dln
        and huwelijk_datumeinde = l_huw_datumeinde;

    end;

  end loop;
  --
  lijst ('INPUT: ');
  lijst ( 'Aantal echtscheidingen in inputbestand: '||to_char(l_aantal_gecontroleerd));
  lijst ('  ');

    -- bestandsnaam formeren
  l_bestandsnaam := l_database ||'RLEPRC60_UITVAL_INPUT_EX_PARTNERS_'|| to_char(sysdate, 'yyyymmdd')||'_'||p_script_id|| '.CSV';

  -- openen van de filehandler
  fh_stoplijst := utl_file.fopen ( 'RLE_OUTBOX'
                                  , l_bestandsnaam
                                  , 'W'
                                  );

  -- schrijf de header records --alles overnemen
  utl_file.put_line ( fh_stoplijst
                    , 'REGELING'
               ||';'||'BSN_DLN'
               ||';'||'BSN_EX'
               ||';'||'A_NUMMER_EX'
               ||';'||'NAAM_EX'
               ||';'||'NAAMGEBRUIK_EX'
               ||';'||'VOORNAMEN_EX'
               ||';'||'VOORVOEGSELS_EX'
               ||';'||'VOORLETTERS_EX'
               ||';'||'GESLACHT_EX'
               ||';'||'GEBOORTEDATUM_EX'
               ||';'||'OVERLIJDENSDATUM_EX'
               ||';'||'LANDCODE_EX'
               ||';'||'STRAATNAAM_EX'
               ||';'||'HUISNUMMER_EX'
               ||';'||'HUISLETTER_EX'
               ||';'||'TOEVOEGING_EX'
               ||';'||'POSTCODE_EX'
               ||';'||'GEMEENTENUMMER_EX'
               ||';'||'GEMEENTEDEEL_EX'
               ||';'||'HUWELIJK_DATUMBEGIN'
               ||';'||'HUWELIJK_DATUMEINDE'
               ||';'||'REDEN_EINDE_HUWELIJK'
               ||';'||'FOUTMELDING'
               ||';'||'BESTANDSNAAM'
                    );
  -- wegschrijven data naar uitvalbestand
  for r_uitval_data in c_uitval_data
  loop
  --
    utl_file.put_line ( fh_stoplijst
                        , r_uitval_data.regeling
               ||';'||r_uitval_data.bsn_dln
               ||';'||r_uitval_data.bsn_ex
               ||';'||r_uitval_data.a_nummer_ex
               ||';'||r_uitval_data.naam_ex
               ||';'||r_uitval_data.naamgebruik_ex
               ||';'||r_uitval_data.voornamen_ex
               ||';'||r_uitval_data.voorvoegsels_ex
               ||';'||r_uitval_data.voorletters_ex
               ||';'||r_uitval_data.geslacht_ex
               ||';'||r_uitval_data.geboortedatum_ex
               ||';'||r_uitval_data.overlijdensdatum_ex
               ||';'||r_uitval_data.landcode_ex
               ||';'||r_uitval_data.straatnaam_ex
               ||';'||r_uitval_data.huisnummer_ex
               ||';'||r_uitval_data.huisletter_ex
               ||';'||r_uitval_data.toevoeging_ex
               ||';'||r_uitval_data.postcode_ex
               ||';'||r_uitval_data.gemeentenummer_ex
               ||';'||r_uitval_data.gemeentedeel_ex
               ||';'||r_uitval_data.huwelijk_datumbegin
               ||';'||r_uitval_data.huwelijk_datumeinde
               ||';'||r_uitval_data.reden_einde_huwelijk
               ||';'||r_uitval_data.foutmelding
               ||';'||r_uitval_data.bestandsnaam
                    );

    l_teller := l_teller +1;

  end loop;

  lijst ('OUTPUT: ');
  lijst ('Aantal echtscheidingen ingelezen in RLE: '||to_char(l_aantal_gecontroleerd));
  lijst (' ' );

  lijst ('UITSPLITSING OUTPUT:');
  lijst ('Aantal echtscheidingen die voldoen aan de controles: '||to_char(l_aantal_gecontroleerd - l_teller));
  lijst ('Aantal echtscheidingen die niet voldoen aan de controles: '||to_char(l_teller));
  lijst(' ');
  lijst('Echtscheidingen die voldoen aan de controles kunnen worden doorgezet naar het bronbestand in RLE.');
  lijst('Echtscheidingen die niet voldoen aan de controles zijn met hun foutmelding(en) opgenomen in het uitvalbestand: '||
         l_bestandsnaam|| ' (RLEdata/inbox).');

  lijst(' ');

  if l_fout > 0
  then
    lijst('De volgende regels uit het inputbestand konden niet worden verwerkt vanwege een technische fout '||
          '(aantal: '||l_fout||')');

    for r_ies in c_ies
    loop

      lijst('Regel: '||r_ies.regelnr||' - BSN: '||r_ies.bsn_dln||' - Foutmelding: '||r_ies.foutmelding);

    end loop;
  end if;


  -- filehandle(s) sluiten
  utl_file.fclose_all;
  --  Voettekst van lijsten
  lijst ( '********** EINDE VERSLAG CONTROLE INPUT REGISTRATIE EX_PARTNERS  **********');
  lijst ('Eindtijd: ' || to_char ( sysdate, 'ddmmyyyy hh24:mi:ss' ));
  stm_util.debug ('Einde logverslag');
  --
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  --
  commit;
  --
exception
  when others
  then
    stm_util.t_foutmelding := sqlerrm;
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(l_volgnummer, 'S', '1');
    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                  ' procedure : ' || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null then
      stm_util.insert_job_statussen(l_volgnummer,
                                    'I',
                                    stm_util.t_foutmelding);
    end if;
    --
    if stm_util.t_tekst is not null then
      stm_util.insert_job_statussen(l_volgnummer, 'I', stm_util.t_tekst);
    end if;
    --
    rollback;
    --
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(sqlerrm,1,250)
                                  );


end verwerk;

END RLE_CHK_INPUT_EX;
/