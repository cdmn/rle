CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_GET_CODA_DEBITEUR_DATA IS

-- 23092015 DRZ: CreditManagement(fase2/stroom A): Refacorslag, code iets meer modulair gemaakt
-- 23022016 DRZ: PE-316, Naam2 moet altijd leeg blijven. Voor werkgevers hoeft de handelsnaam niet meer opgehaald te worden.
-- 23022016 DRZ: PE-316, Naam1 wordt voor Personen gevuld met "voorletter tussenvoegsel achternaam", voor werkgevers met de statutaire naam.


-- PRIVATE FUNCTIONS/PROCEDURES

-- Functie om de adressen toe te voegen aan het r_debdata record
-- parameters:
--    p_debdata                : record welke met adresgegevens aangevuld wordt voor de relatie in p_debdata.relatienummer
--    p_priolijst_adrestype    : lijst van adrestypes (kommagescheiden) die gebruikt wordt voor prioritering en filter van de op te halen adresgegevens
--    p_priolijst_postadrestype: lijst van adrestypes (kommagescheiden) die gebruikt wordt voor prioritering en filter van de op te halen postadresgegevens
procedure ophalen_adressen
( p_debdata                 in out nocopy r_debdata
, p_priolijst_adrestype     in            varchar2
, p_priolijst_postadrestype in            varchar2
)
is
  l_numadres     rle_relatie_adressen.ads_numadres%type;
  l_country_A3   rle_landen.codland%type; -- landcode, alfanumeriek, 3 karakters

  -- Retourneert adressen van de relatie
  -- De parameter b_adressoort_prio fungeert als filter welke type adressen gezocht moeten
  -- worden en de volgorde waarin ze geretourneerd moeten worden
  cursor c_adres ( b_relatienummer in number, b_adressoort_prio in varchar2 )
  is
    select  rad.ads_numadres
    from    rle_relatie_adressen rad
    where   rad.rle_numrelatie    = b_relatienummer
    and     instr(','||b_adressoort_prio||',', ','||rad.dwe_wrddom_srtadr||',') > 0  -- filter alleen de adressen van het type in de prio lijst
    and     (datingang  is null or datingang < sysdate)
    and     (dateinde   is null or dateinde  > sysdate)
    order by instr(','||b_adressoort_prio||',', ','||rad.dwe_wrddom_srtadr||',') -- retourneer de lijst o.b.v. prioriteit als gegeven in de prio lijst
  ;

  cursor c_landcode_iso3166 ( b_landcode_A3 in rle_landen.codland%type)
  is
    select landcode_iso3166_a2
      from rle_landen
      where codland = b_landcode_a3
  ;
begin
  -- -----------------
  -- ophalen adres
  -- -----------------
  open  c_adres(p_debdata.relatienummer, p_priolijst_adrestype);
  fetch c_adres into l_numadres;
  close c_adres;
  if l_numadres is not null
  then
    -- ophalen adres velden voor Statutair adres
    comp_rle.RLE_GET_ADRES ( P_NUMADRES   => l_numadres
                           , P_POSTCODE   => p_debdata.zipcode
                           , P_STRAAT     => p_debdata.street1
                           , P_HUISNR     => p_debdata.housenmbr
                           , P_TOEVNUM    => p_debdata.housenmbradd
                           , P_WOONPLAATS => p_debdata.city
                           , P_LAND       => l_country_A3 -- landcode voor het record wordt geacht 2 karakters te bevatten, deze moet nog exta geconverteerd worden.
                           );
    if l_country_A3 is not null
    then
      open  c_landcode_iso3166 (l_country_A3);
      fetch c_landcode_iso3166 into p_debdata.country;
      close c_landcode_iso3166;
    end if;

    -- debug data
    -- dbms_output.put_line('straat found :<'              || p_debdata.street1 || '>') ;
    -- dbms_output.put_line('huisnummer found :<'          || p_debdata.housenmbr || '>') ;
    -- dbms_output.put_line('huisnr toevoeging found :<'   || p_debdata.housenmbradd || '>') ;
    -- dbms_output.put_line('postcode found :<'            || p_debdata.zipcode || '>') ;
    -- dbms_output.put_line('woonplaats found :<'          || p_debdata.city || '>') ;
    -- dbms_output.put_line('land found :<'                || p_debdata.country || '>') ;

  end if; -- adres gevonden

  -- -----------------
  -- ophalen postadres
  -- -----------------
  open  c_adres(p_debdata.relatienummer, p_priolijst_postadrestype);
  fetch c_adres into l_numadres;
  close c_adres;
  -- Note: let erop dat wanneer er geen adres gevonden wordt, l_numadres nog steeds gevuld is met het voorgaand gevonden adres in het blok hierboven.
  --       Dit gedrag is gewensd door de business: indien het postadres niet gevonden wordt, moet dit gevuld worden met de gegevens van het adres
  if l_numadres is not null
  then
    -- ophalen adres velden voor het postadres
    comp_rle.RLE_GET_ADRES ( P_NUMADRES   => l_numadres
                           , P_POSTCODE   => p_debdata.postalzipcode
                           , P_STRAAT     => p_debdata.postalstraat1
                           , P_HUISNR     => p_debdata.postalhousenmbr
                           , P_TOEVNUM    => p_debdata.postalhousenmbradd
                           , P_WOONPLAATS => p_debdata.postalcity
                           , P_LAND       => l_country_A3 -- landcode wordt voor het postadres niet gebruikt
                           );

    -- debug data
    --  dbms_output.put_line('straat found :<'              || p_debdata.postalstraat1 || '>') ;
    --  dbms_output.put_line('huisnummer found :<'          || p_debdata.postalhousenmbr || '>') ;
    --  dbms_output.put_line('huisnr toevoeging found :<'   || p_debdata.postalhousenmbradd || '>') ;
    --  dbms_output.put_line('postcode found :<'            || p_debdata.postalzipcode || '>') ;
    --  dbms_output.put_line('woonplaats found :<'          || p_debdata.postalcity || '>') ;
  end if; -- postadres gevonden

end ophalen_adressen;

-- PUBLIC FUNCTIONS/PROCUDURES

 procedure get_debiteur_data_voor_cmt
 ( p_elmcode    in  varchar2
 , p_type_admie in  varchar2 default null
 , p_debdata    out r_debdata
 )
 is
  c_type_rle_WERKGEVER constant varchar2(2) := 'WG';
  c_type_rle_PERSOON   constant varchar2(2) := 'PR';

-- Variabele Declaraties
    l_begin_letter             varchar2 (1);
    l_type_relatie             varchar2 (2);
    l_relatienummer            rle_relaties.numrelatie%type;
    l_zoekcode                 varchar2 (22);
    l_rle_m11_opgemaakte_naam  varchar2(250); -- outputgegeven van procedure rle_m11: deze haalt de opgemaakte persoonsnaam op ( voorvoegsel, tussenvoegsel, achternaam)
    l_rle_m11_melding          varchar2(250); -- outputgegeven van procedure rle_m11: deze haalt de opgemaakte persoonsnaam op ( voorvoegsel, tussenvoegsel, achternaam)
    l_adres_prio               varchar2(50); -- lijst van adres typen in volgorde van prioriteit (het eerst beschikbare adres hiervan wordt gerouterneerd uit c_adres)
    l_postadres_prio           varchar2(50); -- lijst van adres typen in volgorde van prioriteit (het eerst beschikbare adres hiervan wordt gerouterneerd uit c_adres)

-- type declaratie
    type r_bankdata is record ( accountnmbr     varchar2(50)
                              , swiftcode       varchar2(50)
                              , ibancode        varchar2(50)
                              ) ;

    l_bankdata r_bankdata ;
-- cursor declaraties


    -- retourneert de rollen van de relatie, geordend op prioriteit (Werkgever heeft prio 1)
    cursor c_rle_rol (b_relatienummer in number)
    is
     select rrl.rol_codrol
     from   rle_relatie_rollen rrl
     where  rrl.rle_numrelatie = b_relatienummer
     and    rrl.rol_codrol in (c_type_rle_WERKGEVER,c_type_rle_PERSOON)
     order by case rrl.rol_codrol when c_type_rle_WERKGEVER then 1 else 2 end -- werkgever rol heeft de hoogste prioriteit om te gebruiken
    ;

    cursor c_email ( b_relatienummer in number)
       is
         select NUMCOMMUNICATIE
           from RLE_COMMUNICATIE_NUMMERS com
           where com.rle_numrelatie    = b_relatienummer
           and   com.dwe_wrddom_srtcom = 'EMAIL'
           and  (com.datingang is null or com.datingang < sysdate)
           and  (com.dateinde  is null or com.dateinde  > sysdate);

    cursor c_fax ( b_relatienummer in number)
       is
         select NUMCOMMUNICATIE
           from RLE_COMMUNICATIE_NUMMERS com
           where com.rle_numrelatie    = b_relatienummer
           and   com.dwe_wrddom_srtcom = 'FAX'
           and  (com.datingang is null or com.datingang < sysdate)
           and  (com.dateinde  is null or com.dateinde  > sysdate);

    cursor c_tel ( b_relatienummer in number)
       is
         select NUMCOMMUNICATIE
           from RLE_COMMUNICATIE_NUMMERS com
           where com.rle_numrelatie    = b_relatienummer
           and   com.dwe_wrddom_srtcom = 'TEL'
           and  (com.datingang is null or com.datingang < sysdate)
           and  (com.dateinde  is null or com.dateinde  > sysdate);

    cursor c_bankdata (b_relatienummer in number )
      is
        select NUMREKENING, BIC, IBAN
          from RLE_FINANCIEEL_NUMMERS fin
          where fin.rle_numrelatie = b_relatienummer
          --and   fin.codwijzebetaling = 'SB'
          and  (fin.datingang is null or fin.datingang < sysdate)
          and  (fin.dateinde  is null or fin.dateinde  > sysdate)
          order by decode(fin.codwijzebetaling,'SB', 1, 2)
                 , datingang asc ; -- default bank is de bank met SB en als die er niet is dan neem de OUDSTE bank

    cursor c_statutairenaam (b_relatienummer in number)
    is
      select substr(statutaire_naam,1,50) as statutaire_naam
        from WGR_RECHTSVORMEN rvm
           , WGR_WERKGEVERS   wgr
      where rvm.wgr_id       = wgr.id
      and   wgr.nr_relatie = b_relatienummer
      and   wgr.dat_einde is null
      and   rvm.dat_einde is null ;


----  functie om Relatie NUMMERS zoals b.v. "PSTORNO" te skippen ---------------------------

    FUNCTION is_number( p_str IN VARCHAR2 ) RETURN varchar2 is
    l_num NUMBER;
    begin
      l_num := to_number( p_str );
      RETURN 'Y';
      EXCEPTION
        WHEN others
          then
          --  dbms_output.put_line('ERROR: Invalid P, R or W number: ' || p_str ) ;
            RETURN 'N';
    end is_number;


-----------------------------  START OF PROCEDURE _____________________________
begin
      -- PARAMETER CHECK: [p_type_admie] : Het type adminisratie is PENSIOEN, VERZEKERING of leeg
      if  p_type_admie is not null
      and p_type_admie not in (gc_type_admie_PENSIOEN, gc_type_admie_VERZEKERING)
      then
        dbms_output.put_line(gc_package||': Error, parameter [p_type_admie] : Indien gevuld mag het type administratie alleen de volgende waarden bevatten: '||gc_type_admie_PENSIOEN||','||gc_type_admie_VERZEKERING);
        -- stop het proces
        return;
      end if;

      -- PARAMETER CHECK: [p_elmcode] : code moet beginnen met een P, W of R en daarop volgend een nummer
      if not regexp_like(trim(p_elmcode),'^[PWR][0-9]+$')
      then
        dbms_output.put_line(gc_package||': Error, parameter [p_elmcode] : Relatiecode moet beginnen met een P, W of R en daarop volgend een nummer: ' || p_elmcode ) ;
        -- stop het proces
        return;
      end if;



      l_begin_letter  := substr(p_elmcode, 1, 1) ;  -- kan zijn P, W, R (of zelfs D of C)
      l_zoekcode      := substr(p_elmcode, 2) ;     -- dit moet nu een nummer eventueel met voorloopnullen

      if  l_begin_letter = 'P'
      then
        -- PERSOON
        l_relatienummer := RLE_LIB.GET_RELATIENR_PERSOON(l_zoekcode) ;
        l_type_relatie := c_type_rle_PERSOON;
      elsif l_begin_letter = 'W'
      then
        -- WERKGEVER
        l_relatienummer := RLE_LIB.GET_RELATIENR_WERKGEVER(l_zoekcode) ;
        l_type_relatie := c_type_rle_WERKGEVER;
      elsif l_begin_letter = 'R'
      then
        -- RELATIE
        l_relatienummer := to_number(l_zoekcode);
        -- Rol onbekend, ophalen uit RLE. Werkgeversrol (WG) heeft voorrang op Persoonsrol (PR)
        open c_rle_rol(l_relatienummer);
        fetch c_rle_rol into l_type_relatie;
        close c_rle_rol;
      else
        -- dit kan niet voorkomen i.v.m. parameter check
        return;
      end if;

      -- PARAMETER POST-CHECK: Het type administratie mag alleen leeg zijn wanneer de relatie (p_elmcode) een werkgever betreft in RLE.
      if  p_type_admie is null
      and l_type_relatie != c_type_rle_WERKGEVER
      then
        dbms_output.put_line(gc_package||': Error, parameter [p_type_admie] : De parameter mag alleen leeg zijn wanneer de relatie een werkgever betreft. Gevonden rol van relatie: "'||l_type_relatie||'"');
        -- stop het proces
        return;
      end if;

      -- Relatie gevonden, leg dit vast in het record
      p_debdata.relatienummer := l_relatienummer ;
      -- dbms_output.put_line('voor u gevonden: relatienummer <' || l_relatienummer || '>') ;

      -- ophalen van Email hangt ook niet af van soort relatie
      open  c_email ( l_relatienummer ) ;
      fetch c_email into p_debdata.email ;
      close c_email ;
      -- dbms_output.put_line('voor u gevonden: Email <' || p_debdata.email || '>') ;

      -- ophalen van Fax hangt ook niet af van soort relatie
      open  c_fax ( l_relatienummer ) ;
      fetch c_fax into p_debdata.fax ;
      close c_fax ;
      -- dbms_output.put_line('voor u gevonden: Fax <' || p_debdata.fax || '>') ;

      -- ophalen van Telnr hangt ook niet af van soort relatie
      open  c_tel ( l_relatienummer ) ;
      fetch c_tel into p_debdata.phone ;
      close c_tel ;
      -- dbms_output.put_line('voor u gevonden: Tel <' || p_debdata.phone || '>') ;

      -- ophalen bank gegevens hangt ook niet af van de soort relatie
      open  c_bankdata ( l_relatienummer ) ;
      fetch c_bankdata into l_bankdata ;
      close c_bankdata ;
      p_debdata.accountnmbr := l_bankdata.accountnmbr ;
      p_debdata.swiftcode   := l_bankdata.swiftcode   ;
      p_debdata.ibancode    := l_bankdata.ibancode    ;
      -- dbms_output.put_line('voor u gevonden: BankRekening <' || p_debdata.accountnmbr || '>') ;
      -- dbms_output.put_line('voor u gevonden: BIC <'          || p_debdata.swiftcode   || '>') ;
      -- dbms_output.put_line('voor u gevonden: IBAN <'         || p_debdata.ibancode    || '>') ;

      -- WERKGEVER: ophalen NAMEN en ADRESSEN
      if l_type_relatie = c_type_rle_WERKGEVER -- het is een werkgever, dan willen we ZS en CA adres, statutaire naam en handelsnaam
      then
        -- ophalen statutaire naam
        open  c_statutairenaam (l_relatienummer) ;
        fetch c_statutairenaam into p_debdata.name1_a ;
        close c_statutairenaam ;
        -- dbms_output.put_line('Statutaire naam gevonden was:' || p_debdata.name1_a );

        -- ophalen adres (Statutaire Zetel) en postadres adres (Correspondentie adres)
        ophalen_adressen
        ( p_debdata                 => p_debdata
        , p_priolijst_adrestype     => 'SZ'
        , p_priolijst_postadrestype => 'CA'
        );

        -- Ophalen kvk nr van de werkgever (doorgeven as-is)  -- 23072015 DRZ: CreditManagement(fase2/stroom A): toevoeging kvknr
        p_debdata.kvknummer := RLE_M03_REC_EXTCOD ( P_SYSTEEMCOMP => 'KVK'
                                                  , P_RELNUM => l_relatienummer
                                                  , P_CODROL => c_type_rle_WERKGEVER  -- werkgever
                                                  , P_DATPARM => null -- defaults naar sysdate
                                                  );
      end if; -- WERKGEVER

      -- PERSOON: ophalen NAMEN en ADRESSEN
      if l_type_relatie = c_type_rle_PERSOON -- het is een persoon, dan geldt er een beslisboom voor het adres op basis van type adminstratie
      then
        -- Op basis van de administratie waaronder de debiteur valt wordt bepaald of dit VERZEKERINGEN of PENSIOENEN betreft. In deze context worden de volgende gegevens opgehaald uit RLE:
        --
        -- ------------------------------------
        -- Type administratie: VERZEKERINGEN
        --   Adres: Het eerste beschikbare adres op basis van prioriteit:
        --          1. Feitelijk adres       (FA)
        --          2. Opgegeven adres       (OA)
        --          3. Correspondentie adres (CA)
        --   Postadres: Het eerste beschikbare adres op basis van prioriteit:
        --          1. Opgegeven adres       (OA)
        --          2. Correspondentie adres (CA)
        --          3. Feitelijk adres       (FA)
        -- ------------------------------------
        -- Type administratie: PENSIOENEN
        --   Adres: Domicilie adres     (DA)
        --   Postadres: Opgegeven adres (OA) (indien dit niet bekent is in RLE, dan worden de gegevens van het Domicilie adres gebruikt)
        -- ------------------------------------
        if p_type_admie = gc_type_admie_VERZEKERING
        then
          l_adres_prio     := 'FA,OA,CA';
          l_postadres_prio := 'OA,CA,FA';
        elsif p_type_admie = gc_type_admie_PENSIOEN
        then
          l_adres_prio     := 'DA';
          l_postadres_prio := 'OA';
        else
          -- zou nooit voor mogen komen vanwege de parameter controles aan het begin van de procedure
          l_adres_prio     := null;
          l_postadres_prio := null;
        end if;

        -- ophalen persoon namen
        RLE_M11_RLE_NAW( 50                        -- input: max lengte van de output opgemaakte naam (werkt niet? vandaar extra code op toekenning van p_debdata.name1_a)
                       , l_relatienummer           -- input: relatienummer
                       , l_rle_m11_opgemaakte_naam -- output
                       , l_rle_m11_melding         -- output
                       , '2'                       -- input: optie '2': ---> Voorletters + " " + voorvoegsel + " " + naam
                       );
        p_debdata.name1_a := substr( trim( coalesce( l_rle_m11_opgemaakte_naam -- Naam max 50 lang, leading en trailing spaties verwijderen
                                                   , l_rle_m11_melding         -- Prioriteit van waarde toekenning (indien waarde van een variabele NULL is)
                                                   )
                                          )
                                   ,1,50);
        -- Wanneer de naam na de verwijdering van spaties echt leeg is, geef dan NAAM ONBEKEND mee
        p_debdata.name1_a := coalesce(p_debdata.name1_a, 'NAAM ONBEKEND');
        -- dbms_output.put_line('persoons naam gevonden was:' || p_debdata.name1_a );

        -- ophalen adres en postadres o.b.v. prioriteit en filter lijsten
        ophalen_adressen
        ( p_debdata                 => p_debdata
        , p_priolijst_adrestype     => l_adres_prio
        , p_priolijst_postadrestype => l_postadres_prio
        );

      end if; -- PERSOON

      -- 23022016 DRZ: PE-316, Naam2 moet altijd leeg blijven.
      p_debdata.name1_b := null;
    end get_debiteur_data_voor_cmt;

end rle_get_coda_debiteur_data;
/