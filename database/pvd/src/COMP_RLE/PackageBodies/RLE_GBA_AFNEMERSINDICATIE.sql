CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_GBA_AFNEMERSINDICATIE IS
--------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is aanpassen rle_afstemmingen_gba
-- code_gba_status -> OV / AF
-- afnemersindicatie -> N
-- op basis van aangeleverde lijst persoonsnummers.
--------------------------------------------------------------
-- Wijzigingshistorie
-- Wanneer     Wie               Wat
--------------------------------------------------------
-- 21-02-2014  WHR               Creatie
-- 06-05-2014  AZM               Procedure zet_gba_indicatie gewijzigd nav systeemtestbevinding
-- 04-06-2014  XSW               Indien CODE STATUS GBA is ongelijk AF (of AA, IA of TB én ouder dan 1 maand)
--                               dan de CODE STATUS GBA wijzigen naar OV
------------------------------------------------------------

-- global t.b.v. logverslag
g_lijstnummer         number := 1;
g_regelnr             number := 0;

g_msg_gba_apn_gewijzigd    number := 0;
g_msg_gba_apn_ongewijzigd  number := 0;
g_msg_gba_prs_gewijzigd    number := 0;
g_msg_gba_prs_ongewijzigd  number := 0;
g_msg_rle_asa_toegevoegd   number := 0;
g_msg_rle_asa_gewijzigd    number := 0;
g_msg_rle_asa_ongewijzigd  number := 0;
g_msg_gba_prs_niet_gevonden number := 0;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
PROCEDURE VERWERK_GBA_INDIC;
PROCEDURE ZET_GBA_STATUS
 (P_PERSOONSNUMMER personen.persoonsnummer%type
 ,P_CODE_GBA_STATUS rle_afstemmingen_gba.code_gba_status%type
 );
PROCEDURE ZET_GBA_INDICATIE
 (P_PERSOONSNUMMER personen.persoonsnummer%type
 );
PROCEDURE ZET_RLE_INDICATIE_STATUS
 (P_RLE_NUMRELATIE IN rle_afstemmingen_gba.rle_numrelatie%type
 ,P_PERSOONSNUMMER IN personen.persoonsnummer%type
 ,P_CODE_GBA_STATUS OUT rle_afstemmingen_gba.code_gba_status%type
 );
PROCEDURE VERWERK_GBA_AFNEMERSINDICATIE;


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
begin
    stm_util.insert_lijsten( g_lijstnummer
                           , g_regelnr
                           , p_text
                           );
end lijst;
PROCEDURE VERWERK_GBA_INDIC
 IS


  -- GBA INDIC persoonsnummer niet gevonden in RLE
  cursor c_gba_indic_niet_gevonden
  is
    SELECT persoonsnummer
    FROM   rle_gba_indic_tmp gba
    WHERE  gba.persoonsnummer is not null
    and NOT EXISTS
           (SELECT 1
            FROM   rle_relatie_externe_codes rec
            WHERE  gba.persoonsnummer = rec.extern_relatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'PRS'
            );
  --
  l_aantal_aangeleverd   number := 0;
  l_aantal_niet_gevonden number := 0;
  --
begin
  stm_util.t_procedure       := 'VERWERK_GBA_INDIC';

  -- Tijdelijke GBA_INDIC tabel leegmaken
  execute immediate 'TRUNCATE TABLE rle_gba_indic_tmp';

  -- Haal data over van external table naar tijdelijke tabel
  INSERT INTO rle_gba_indic_tmp
    ( persoonsnummer
    )
    (SELECT persoonsnummer
     FROM   rle_gba_indic_ext
    );

  --l_aantal_aangeleverd := SQL%ROWCOUNT;
  SELECT count(*)
    INTO l_aantal_aangeleverd
  FROM   rle_gba_indic_tmp
  WHERE  persoonsnummer is not null;

  lijst ('GBA INDIC aantal aangeleverd      : ' || to_char(l_aantal_aangeleverd));

  commit;

  -- GBA INDIC waarvan persoonsnummer niet gevonden in RLE
  lijst (' ');
  lijst ('GBA INDIC waarvan persoonsnummer niet gevonden in RLE:');
  lijst ('persoonsnummer');
  for r_gba_indic_niet_gevonden
  in  c_gba_indic_niet_gevonden
  loop
    lijst ( r_gba_indic_niet_gevonden.persoonsnummer );
    l_aantal_niet_gevonden := l_aantal_niet_gevonden+1;
  end loop;
  lijst (' ');
  lijst ('GBA INDIC aantal niet gevonden    : ' || to_char(l_aantal_niet_gevonden));
  lijst (' ');

  commit;

end verwerk_gba_indic;
PROCEDURE ZET_GBA_STATUS
 (P_PERSOONSNUMMER personen.persoonsnummer%type
 ,P_CODE_GBA_STATUS rle_afstemmingen_gba.code_gba_status%type
 )
 IS


  cursor c_apn (b_persoonsnummer  per_aanmeldingen.rle_persoonsnummer%type )
  is
    select apn.id                 as apn_id
    ,      apn.status             as status
    from   per_aanmeldingen       apn
    where  apn.rle_persoonsnummer = b_persoonsnummer;
begin
  stm_util.t_procedure       := 'ZET_GBA_STATUS';

  for r_apn
  in  c_apn( b_persoonsnummer => p_persoonsnummer )
  loop
  --
    if r_apn.status <> p_code_gba_status
    then
      pergbt02.set_status_apn
        ( p_apn_id => r_apn.apn_id
        , p_status => p_code_gba_status
        );
      lijst( p_persoonsnummer || ' Status aangepast in GBA-C per_aanmeldingen (' || r_apn.apn_id || ') ' ||p_code_gba_status );
      g_msg_gba_apn_gewijzigd := g_msg_gba_apn_gewijzigd+1;
    else
      lijst( p_persoonsnummer || ' Status NIET aangepast in GBA-C per_aanmeldingen (' || r_apn.apn_id || ') ' || p_code_gba_status);
      g_msg_gba_apn_ongewijzigd := g_msg_gba_apn_ongewijzigd+1;
    end if;

  end loop;
  --
  commit;
exception
  when others
  then
    lijst( p_persoonsnummer || 'Exception ZET_GBA_STATUS');
    lijst( substr(sqlerrm,1,500) );

    commit;

end ZET_GBA_STATUS;
PROCEDURE ZET_GBA_INDICATIE
 (P_PERSOONSNUMMER personen.persoonsnummer%type
 )
 IS


  cursor c_prs (b_persoonsnummer  personen.persoonsnummer%type )
  is
    select afnemersindicatie
    from   personen          prs
    where  prs.persoonsnummer = b_persoonsnummer;
  --
  r_prs               c_prs%rowtype;
  l_doorgeven_aan_rle varchar2(1);
  l_maak_uitbericht   boolean;
begin
  stm_util.t_procedure       := 'ZET_GBA_INDICATIE';

  open  c_prs( b_persoonsnummer => p_persoonsnummer );
  fetch c_prs
  into  r_prs;
  --
  if c_prs%notfound
  then
    g_msg_gba_prs_niet_gevonden := g_msg_gba_prs_niet_gevonden+1;
    lijst( p_persoonsnummer ||  ' Persoon niet gevonden in GBA-C');
  elsif r_prs.afnemersindicatie <> 'N'
  then
    -- bewaar huidige indicaties
    l_doorgeven_aan_rle := gba_indicatie.doorgeven_aan_rle;
    l_maak_uitbericht   := rle_verwerk_syn_gba.maak_uitbericht;

    -- Zet de indicaties op niet doorgeven
    gba_indicatie.doorgeven_aan_rle        := 'N' ;
    rle_verwerk_syn_gba.g_maak_uit_bericht := false;

    pergbt02.set_afn_ind
      ( p_persoonsnummer    => p_persoonsnummer
      , p_afnemersindicatie => 'N'
      );
    lijst( p_persoonsnummer || ' Indicatie aangepast in GBA-C personen');
    g_msg_gba_prs_gewijzigd := g_msg_gba_prs_gewijzigd+1;

    -- Terugzetten indicaties
    gba_indicatie.doorgeven_aan_rle        := l_doorgeven_aan_rle;
    rle_verwerk_syn_gba.g_maak_uit_bericht := l_maak_uitbericht;
  else
    lijst( p_persoonsnummer || ' Persoon heeft reeds afnemersindicatie N');
    g_msg_gba_prs_ongewijzigd := g_msg_gba_prs_ongewijzigd+1;
  end if;
  --
  if c_prs%isopen then
     close c_prs;
  end if;
  --
  commit;
exception
  when others
  then
    lijst( p_persoonsnummer || 'Exception ZET_GBA_INDICATIE');
    lijst( substr(sqlerrm,1,500) );

    -- Terugzetten indicaties
    gba_indicatie.doorgeven_aan_rle        := l_doorgeven_aan_rle;
    rle_verwerk_syn_gba.g_maak_uit_bericht := l_maak_uitbericht;

    if c_prs%isopen then
       close c_prs;
    end if;

    commit;

end ZET_GBA_INDICATIE;
PROCEDURE ZET_RLE_INDICATIE_STATUS
 (P_RLE_NUMRELATIE IN rle_afstemmingen_gba.rle_numrelatie%type
 ,P_PERSOONSNUMMER IN personen.persoonsnummer%type
 ,P_CODE_GBA_STATUS OUT rle_afstemmingen_gba.code_gba_status%type
 )
 IS


  cursor c_asa( b_numrelatie rle_afstemmingen_gba.rle_numrelatie%type )
  is
    SELECT asa.code_gba_status
          ,asa.afnemers_indicatie
          ,asa.dat_creatie
    FROM   rle_afstemmingen_gba asa
    WHERE  asa.rle_numrelatie = b_numrelatie
    ;
  --
  r_asa  c_asa%rowtype;
  l_afnemers_indicatie rle_afstemmingen_gba.afnemers_indicatie%type;
begin
  stm_util.t_procedure := 'ZET_RLE_INDICATIE_STATUS';
  l_afnemers_indicatie := 'N';
  p_code_gba_status    := 'OV';
  --
  open  c_asa( b_numrelatie => p_rle_numrelatie );
  fetch c_asa
  into  r_asa;
  --
  if c_asa%notfound
  then
      rle_gba_wijzig_gba( pin_numrelatie            => p_rle_numrelatie
                        , piv_gba_status            => p_code_gba_status
                        , piv_gba_afnemers_ind      => l_afnemers_indicatie
                        , pin_gba_aanmeldingsnummer => null
                        );
    lijst( p_persoonsnummer || ' rle_afstemmingen_gba toegevoegd ' || l_afnemers_indicatie || '-' || p_code_gba_status);
    g_msg_rle_asa_toegevoegd := g_msg_rle_asa_toegevoegd+1;
  end if;
  --
  if c_asa%found
  then
    --
    if r_asa.code_gba_status = 'AF'
    then
      p_code_gba_status := r_asa.code_gba_status;
    --
      if (r_asa.code_gba_status   <> p_code_gba_status
      or  r_asa.afnemers_indicatie <> l_afnemers_indicatie)
      then
        rle_gba_wijzig_gba( pin_numrelatie            => p_rle_numrelatie
                          , piv_gba_status            => p_code_gba_status
                          , piv_gba_afnemers_ind      => l_afnemers_indicatie
                          , pin_gba_aanmeldingsnummer => null
                          );
        lijst( p_persoonsnummer || ' rle_afstemmingen_gba aangepast van: ' ||
               r_asa.afnemers_indicatie || '-' || r_asa.code_gba_status || ' naar: ' ||
               l_afnemers_indicatie     || '-' || p_code_gba_status
             );
        g_msg_rle_asa_gewijzigd := g_msg_rle_asa_gewijzigd+1;
      end if;
    elsif (r_asa.code_gba_status    <> p_code_gba_status
      or  r_asa.afnemers_indicatie  <> l_afnemers_indicatie)
      and r_asa.dat_creatie < add_months(trunc(sysdate),-1)-- aanpassing xsw
      then
        rle_gba_wijzig_gba( pin_numrelatie            => p_rle_numrelatie
                          , piv_gba_status            => p_code_gba_status
                          , piv_gba_afnemers_ind      => l_afnemers_indicatie
                          , pin_gba_aanmeldingsnummer => null
                          );
        lijst( p_persoonsnummer || ' rle_afstemmingen_gba aangepast van: ' ||
             r_asa.afnemers_indicatie || '-' || r_asa.code_gba_status || ' naar: ' ||
             l_afnemers_indicatie     || '-' || p_code_gba_status
           );
      g_msg_rle_asa_gewijzigd := g_msg_rle_asa_gewijzigd+1;
      --
    else
    p_code_gba_status := r_asa.code_gba_status;
    lijst( p_persoonsnummer || ' rle_afstemmingen_gba NIET aangepast' || r_asa.afnemers_indicatie || '-' || r_asa.code_gba_status);
    g_msg_rle_asa_ongewijzigd := g_msg_rle_asa_ongewijzigd+1;
    --
    end if;
    --
  end if;
  --
  if c_asa%isopen then
     close c_asa;
  end if;
  --
  commit;
exception
  when others
  then
    rollback;
    lijst( p_persoonsnummer || 'Exception ZET_RLE_INDICATIE_STATUS');
    lijst( substr(sqlerrm,1,500) );

    commit;
end ZET_RLE_INDICATIE_STATUS;
PROCEDURE VERWERK_GBA_AFNEMERSINDICATIE
 IS


  -- Cursor GBA INDIC
  cursor c_gba_indic
  is
    SELECT rec.rle_numrelatie
         , gba.persoonsnummer
    FROM   rle_gba_indic_tmp gba
    JOIN   rle_relatie_externe_codes rec
    ON     gba.persoonsnummer = rec.extern_relatie
    AND    rec.rol_codrol = 'PR'
    AND    rec.dwe_wrddom_extsys = 'PRS';
  l_code_gba_status    rle_afstemmingen_gba.code_gba_status%type;
begin
  stm_util.t_procedure       := 'VERWERK_GBA_AFNEMERSINDICATIE';

  for r_gba_indic in c_gba_indic
  loop

     l_code_gba_status := null;

     zet_rle_indicatie_status
       ( p_persoonsnummer  => r_gba_indic.persoonsnummer
       , p_rle_numrelatie  => r_gba_indic.rle_numrelatie
       , p_code_gba_status => l_code_gba_status
       );

     if l_code_gba_status is not null
     then
       zet_gba_status
         ( p_persoonsnummer  => r_gba_indic.persoonsnummer
         , p_code_gba_status => l_code_gba_status
         );
     end if;

     zet_gba_indicatie
       ( p_persoonsnummer => r_gba_indic.persoonsnummer
       );

  end loop;

  lijst ( ' ' );
  lijst ( 'Aantal RLE afstemmingen GBA gewijzigd          : ' || g_msg_rle_asa_gewijzigd      );
  lijst ( 'Aantal RLE afstemmingen GBA ongewijzigd        : ' || g_msg_rle_asa_ongewijzigd    );
  lijst ( 'Aantal RLE afstemmingen GBA toegevoegd         : ' || g_msg_rle_asa_toegevoegd     );

  lijst ( ' ' );
  lijst ( 'Aantal records in per_aanmeldingen gewijzigd   : ' || g_msg_gba_apn_gewijzigd      );
  lijst ( 'Aantal records in per_aanmeldingen ongewijzigd : ' || g_msg_gba_apn_ongewijzigd    );

  lijst ( ' ' );
  lijst ( 'Aantal GBA-C personen gewijzigd                : ' || g_msg_gba_prs_gewijzigd      );
  lijst ( 'Aantal GBA-C personen ongewijzigd              : ' || g_msg_gba_prs_ongewijzigd    );
  lijst ( 'Aantal niet in GBA-C personen gevonden         : ' || g_msg_gba_prs_niet_gevonden  );
  lijst ( ' ' );

  commit;

end verwerk_gba_afnemersindicatie;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN number
 )
 IS

  --
  l_volgnummer          stm_job_statussen.volgnummer%type := 0;
  e_abort_batch         exception;
begin
  stm_util.t_procedure       := 'VERWERK';
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_GBA_AFNEMERSINDICATIE';

  -- Schrijven logbestand
  lijst ( rpad ('***** LOGVERSLAG RLEPRC70: RLE_GBA_AFNEMERSINDICATIE  *****', 70)
               || ' STARTDATUM: ' || to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  lijst ( ' ' );
  lijst ( '--------------------------------------------------------------------------------' );
  lijst ( ' ' );
  lijst ( 'Input bestand: RLE_GBA_INDIC_EXT.csv' );
  lijst ( ' ' );
  lijst ( '--------------------------------------------------------------------------------' );

  -- Verwerk GBA_INDIC
  verwerk_gba_indic;

  -- Aanpassen code_gba_status(OV/AF) afnemers_indicatie(N)
  verwerk_gba_afnemersindicatie;

  --  Voettekst van lijsten
  lijst ( '******* EINDE VERSLAG RLE_GBA_AFNEMERSINDICATIE - EINDDATUM: '||to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss')||'*******');
  stm_util.debug ('Einde logverslag');
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  -- opslaan
  commit;

exception
  when others
  then
    rollback;
    stm_util.t_foutmelding := substr(sqlerrm,1,500);
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);
    lijst( substr(sqlerrm,1,500) );

    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(stm_util.t_foutmelding,1,250)
                                  );
    commit;
end verwerk;
END RLE_GBA_AFNEMERSINDICATIE;
/