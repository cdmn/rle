CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_GBA_THERMOMETER IS


  g_lijstnummer         number := 1;
  g_regelnr             number := 0;
  g_script_id            number;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
PROCEDURE TOTALEN;
PROCEDURE MAAK_DETAILBESTAND;


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
begin
    stm_util.insert_lijsten( g_lijstnummer
                           , g_regelnr
                           , p_text
                           );
end lijst;
PROCEDURE TOTALEN
 IS


cursor c_tot
is
  select max(case when reden = 'J' then aantal end) aantal_ja
        ,max(case when reden = 'N' then aantal end) aantal_nee
        ,rubrieknummer
  from ( select count(*) aantal
        ,rubrieknummer
        ,reden
         from(select case when rtr.rubrieknummer = '010110'
                       then '010110 A-nummer'
                     when rtr.rubrieknummer = '010120'
                       then '010120 BSN'
                     when rtr.rubrieknummer = '010230'
                       then '010230 Voorvoegsel'
                     when rtr.rubrieknummer = '010240'
                       then '010240 Geslachtsnaam'
                     when rtr.rubrieknummer = '010310'
                       then '010310 Geboortedatum'
                     when rtr.rubrieknummer = '010410'
                       then '010410 Geslachtsaanduiding'
                     when rtr.rubrieknummer = '081120'
                       then '081120 Huisnummer'
                     when rtr.rubrieknummer = '081130'
                       then '081130 Huisletter'
                     when rtr.rubrieknummer = '081140'
                       then '081140 Huisnummertoevoeging'
                     when rtr.rubrieknummer = '081160'
                       then '081160 Postcode'
                     when rtr.rubrieknummer = '081310'
                       then '081310 Land adres buitenland'
                     when rtr.rubrieknummer = '081320'
                       then '081320 Datum vertrek uit Nederland'
                     end rubrieknummer
                    ,case when rss.reden is not null
                       then 'J'
                     else 'N'
                     end reden
              from rle_thermometer rtr
                  ,rle_synchronisatie_statussen rss
              where rss.rle_numrelatie = rtr.relatienummer
             )
         group by rubrieknummer, reden
       )
  group by rubrieknummer
  order by rubrieknummer
;

cursor c_tel
is
  select count (distinct relatienummer) aantal_prs
        ,reden
  from( select rtr.relatienummer
              ,case when rss.reden is not null
                      then 'J'
                    else 'N'
                    end reden
        from rle_thermometer rtr
            ,rle_synchronisatie_statussen rss
        where rtr.relatienummer = rss.rle_numrelatie
      )
  group by reden;

l_teller          integer := 0;
l_teller_prs      integer := 0;
l_teller_rec      integer := 0;
l_teller_met      integer := 0;
l_teller_zonder   integer := 0;
l_totaal          integer := 0;
begin

  select count(distinct bsn)
  into l_teller
  from comp_rle.rle_syn_van_gba;

  select count (*)
  into l_teller_rec
  from comp_rle.rle_syn_van_gba;

  lijst ('TOTALEN: ');
  lijst (' ');
  lijst ('Aantal personen in het verschillenverslag              :'||l_teller);
  lijst ('Aantal aangeboden records                              :'||l_teller_rec);

  for r_tel in c_tel
  loop

    if r_tel.reden = 'J'
    then
      l_teller_met := r_tel.aantal_prs;
    else
      l_teller_zonder := r_tel.aantal_prs;
    end if;

    l_teller_prs := l_teller_met + l_teller_zonder;

  end loop;

  lijst ('Aantal personen met verschil in rubriek                :'||l_teller_prs);
  lijst ('Aantal personen met verschil in rubriek, met kenmerk   :'||l_teller_met);
  lijst ('Aantal personen met verschil in rubriek zonder kenmerk :'||l_teller_zonder);
  lijst (' ');
  lijst (rpad('-', 100, '-'));

  lijst (' ');
  lijst ('PER RUBRIEK');
  lijst ('Let op! Personen kunnen in meerdere rubrieken voorkomen.');
  lijst (' ');

  lijst (rpad('Rubriek', 46, ' ')||rpad('Totaal', 30, ' ')||rpad ('Met kenmerk', 30, ' ')||'Zonder kenmerk');

  for r_tot in c_tot
  loop

    l_totaal := nvl(r_tot.aantal_ja, 0) + nvl(r_tot.aantal_nee, 0);
    lijst (rpad (nvl(r_tot.rubrieknummer, 0), 46, ' ')||rpad(l_totaal, 30, ' ')||rpad (nvl(r_tot.aantal_ja, 0), 30, ' ')||nvl(r_tot.aantal_nee, 0));

  end loop;

end totalen;
PROCEDURE MAAK_DETAILBESTAND
 IS

cursor c_rtr
is
with rtr as
  (select wm_concat (rtr.rubrieknummer) rubriek
         ,rtr.persoonsnummer
         ,rtr.relatienummer
   from rle_thermometer rtr
   group by rtr.persoonsnummer
           ,rtr.relatienummer
  )
 select rtr.persoonsnummer
       ,rec.extern_relatie anr
       ,rss.reden
       ,(select wm_concat (rie.adm_code) administraties
         from rle_relatierollen_in_adm rie
         where rie.rle_numrelatie = rss.rle_numrelatie) administraties
       ,prs.sofinummer
       ,prs.naam
       ,prs.geboortedatum
       ,prs.geslacht
       ,prs.datum_overlijden
       ,rtr.rubriek
  from rtr
      ,rle_synchronisatie_statussen rss
      ,rle_relatie_externe_codes rec
      ,rle_v_personen prs
  where rtr.relatienummer = rss.rle_numrelatie
  and   rec.rle_numrelatie (+) = rss.rle_numrelatie
  and   prs.relatienummer = rss.rle_numrelatie
  and   rec.dwe_wrddom_extsys (+) = 'GBA'
;

fh_detail         utl_file.file_type;
l_bestandsnaam    varchar2(200);
l_database        varchar2(15);
begin

  select case
           when regexp_substr ( global_name
                             , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;

  l_bestandsnaam := l_database||'RLEPRC76_DETAILBESTAND_'||to_char(sysdate, 'yyyymmdd')||'_'||g_script_id||'.csv';

  fh_detail := utl_file.fopen ('RLE_OUTBOX'
                               ,l_bestandsnaam
                               ,'W'
                               );

  utl_file.put_line (fh_detail
                   ,'PERSOONSNUMMER'     ||';'||
                    'A-NUMMER'           ||';'||
                    'BSN'                ||';'||
                    'GESLACHTSNAAM'      ||';'||
                    'GEBOORTEDATUM'      ||';'||
                    'GESLACHT'           ||';'||
                    'OVERLIJDENSDATUM'   ||';'||
                    'RUBRIEKNUMMER GBA'  ||';'||
                    'REDEN'              ||';'||
                    'ADMINISTRATIES'
                     );

  for r_rtr in c_rtr
  loop

    utl_file.put_line (fh_detail
                      ,r_rtr.persoonsnummer   ||';'||
                       r_rtr.anr              ||';'||
                       r_rtr.sofinummer       ||';'||
                       r_rtr.naam             ||';'||
                       r_rtr.geboortedatum    ||';'||
                       r_rtr.geslacht         ||';'||
                       r_rtr.datum_overlijden ||';'||
                       r_rtr.rubriek          ||';'||
                       r_rtr.reden            ||';'||
                       r_rtr.administraties
                      );

  end loop;

lijst (' ');
lijst ('Het detailbestand met de naam: '||l_bestandsnaam||' is klaargezet op de FTP pool (RLE data/inbox).');
lijst (' ');
end maak_detailbestand;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 )
 IS


cursor c_rle
is
  select prs.extern_relatie persoonsnummer
        ,prs.rle_numrelatie relatienummer
        ,rsg.rubrieknummer
        ,rsg.bsn
  from comp_rle.rle_syn_van_gba rsg
      ,rle_relatie_externe_codes prs
      ,rle_relatie_externe_codes bsn
  where rsg.rubrieknummer in ('010110','010120','010230','010240','010310','010410',
                              '081120','081130','081140','081160','081310','081320')
  and   rsg.bsn = bsn.extern_relatie
  and   bsn.dwe_wrddom_extsys = 'SOFI'
  and   prs.dwe_wrddom_extsys = 'PRS'
  and   bsn.rle_numrelatie = prs.rle_numrelatie
;

l_volgnummer      stm_job_statussen.volgnummer%type := 0;
begin

  stm_util.t_procedure       := 'VERWERK';
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_GBA_THERMOMETER';

  g_script_id := p_script_id;

  lijst (rpad ('***** LOGVERSLAG RLEPRC76: RLE_GBA_THERMOMETER  *****', 70)
               || ' STARTDATUM: ' || to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  lijst (' ');
  lijst (rpad('-', 100, '-'));
  lijst (' ');
  lijst ('Invoerbestand: afwijkingen.csv');
  lijst (' ');
  lijst (rpad('-', 100, '-'));
  lijst (' ');

  delete from rle_thermometer;

  for r_rle in c_rle
  loop

    insert into rle_thermometer (bsn
                               , persoonsnummer
                               , relatienummer
                               , rubrieknummer)
    values ( r_rle.bsn
           , r_rle.persoonsnummer
           , r_rle.relatienummer
           , r_rle.rubrieknummer
           );
  end loop;

  totalen;
  maak_detailbestand;

  lijst (' ');
  lijst ('***** EINDE LOGVERSLAG Thermometerfunctie *****');
  lijst ('Einddatum: '||to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));

  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );

exception
  when others
  then
    rollback;
    stm_util.t_foutmelding := substr(sqlerrm,1,500);
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(stm_util.t_foutmelding,1,250)
                                  );
    commit;
end verwerk;
end RLE_GBA_THERMOMETER;
/