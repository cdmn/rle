CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_LIST_WGR IS
-- Sub-Program Unit Declarations
CURSOR C_ADRES
 (B_POSTCODE IN VARCHAR2
 ,B_HUISNUMMER IN NUMBER
 ,B_TOEVHUISNR IN VARCHAR2
 ,b_naam varchar2
 ,b_handelsnaam varchar2
 )
 IS
 SELECT ads.numadres
 ,      ras.rle_numrelatie
 ,      rle.numrelatie
 ,      rec.extern_relatie
 FROM rle_relatie_externe_codes rec
 ,    rle_relaties              rle
 ,    rle_relatie_adressen      ras
 ,    rle_adressen              ads
 WHERE ads.postcode          = b_postcode
 AND   ads.huisnummer        = nvl(b_huisnummer,ads.huisnummer)
 AND   NVL(toevhuisnum,'##') = NVL(b_toevhuisnr, NVL(toevhuisnum,'##'))
 and   ras.ads_numadres      = ads.numadres
 and   ras.dwe_wrddom_srtadr = 'SZ'
 and   rle.numrelatie        = ras.rle_numrelatie
 and   rle.zoeknaam like upper(b_naam||'%')
 AND   NVL(rle.handelsnaam,'#') LIKE (NVL(b_handelsnaam,NVL(rle.handelsnaam,'#'))||'%')
 and   rec.rle_numrelatie    = ras.rle_numrelatie
 AND   rec.rol_codrol = 'WG'
 AND   rec.dwe_wrddom_extsys = 'WGR'
 ;
L_NUMADRES     NUMBER(7);
-- PL/SQL Block
-- Sub-Program Units
PROCEDURE INIT_LIST_WGR
 (P_NAAM_WERKGEVER IN VARCHAR2
 ,P_HANDELSNAAM IN VARCHAR2
 ,P_POSTCODE IN VARCHAR2
 ,P_HUISNUMMER IN NUMBER
 ,P_TOEVHUISNR IN VARCHAR2
 )
 IS
-- PL/SQL Specification
L_NAAM         VARCHAR2(10) := SUBSTR(P_NAAM_WERKGEVER,1,10);
L_HANDELSNAAM  VARCHAR2(10) := SUBSTR(P_HANDELSNAAM,1,10);
-- PL/SQL Block
BEGIN
IF c_adres%isopen
THEN close c_adres;
END IF;
OPEN C_ADRES
   (P_POSTCODE
   ,P_HUISNUMMER
   ,P_TOEVHUISNR
   ,l_naam
   ,l_handelsnaam);
EXCEPTION
WHEN others THEN
  IF C_ADRES%ISOPEN
  THEN CLOSE C_ADRES;
  END IF;
  RAISE;
END INIT_LIST_WGR;
PROCEDURE GET_NEXT_WGR
 (P_IND_AANWEZIG OUT VARCHAR2
 ,P_RELATIENUMMER OUT NUMBER
 ,P_WERKGEVERSNUMMER OUT VARCHAR2
 )
 IS
r_adres c_adres%rowtype;
BEGIN
 fetch c_adres into r_adres;
 if c_adres%notfound
 then
    close c_adres;
    P_IND_AANWEZIG      := 'N';
    P_RELATIENUMMER     := NULL;
    P_WERKGEVERSNUMMER  := NULL;
    stm_util.debug('aanwezig '|| P_IND_AANWEZIG);
 ELSE
    P_IND_AANWEZIG      := 'J';
    P_RELATIENUMMER     := r_adres.NUMRELATIE;
    P_WERKGEVERSNUMMER  := r_adres.EXTERN_RELATIE;
    stm_util.debug('aanwezig '|| P_IND_AANWEZIG);
    stm_util.debug('relnr '|| P_RELATIENUMMER);
    stm_util.debug('wgrnr '|| P_WERKGEVERSNUMMER);
 END IF;
END GET_NEXT_WGR;
END RLE_LIST_WGR;
/