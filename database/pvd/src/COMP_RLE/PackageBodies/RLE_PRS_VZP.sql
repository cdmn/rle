CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_PRS_VZP IS


  gc_collectief  constant varchar2(30) := 'Collectief';
  gc_individueel constant varchar2(30) := 'Individueel';
  gc_reg_ond_std constant varchar2(30) := 'Standaard';
  gc_reg_ond_opt constant varchar2(30) := 'Optioneel';
  gc_reg_ond_dis constant varchar2(30) := 'Dispensabel';
  gc_aansl_vp    constant varchar2(30) := 'Verplicht';
  gc_aansl_vw    constant varchar2(30) := 'Vrijwillig';
  gc_aansl_vs    constant varchar2(30) := 'Vrijstelling';

/********************************************************************************************************
Created BY: A. Budiawan
DATE:       07-10-2009
Purpose:    Deze procedure bepaalt voor welke regeling(onderdelen) een persoon is aangesloten op een
            bepaalde peildatum. Het gaat hierbij alleen om de zgn. bedrijfstakeigen regelingen of om
            verzekeringen.
Input Parameters: p_persoonsnummer  persoonsnummer (optioneel)
                  p_relatienummer  relatienummer persoon (optioneel)
                  p_peildatum  default systeemdatum
Output: t_prs_typ
Usage:
Remarks: * Wanneer de context (administratiecode) is gezet, dan wordt de uitvoer beperkt tot alle regelingen die
           behoren tot de opgegeven administratie.
         * Relatienummer persoon en/of persoonsnummer kan worden opgegeven. Minimaal één van beide is
           verplicht. Als persoonsnummer gevuld is, dan is dit leidend.
         * Als er geen peildatum is opgegeven, dan wordt systeemdatum genomen.
         * Bij het bepalen van soort aansluiting is het volgende van toepassing:
           - bij individuele opties is het altijd 'vrijwillig'
           - bij collectieve regelingen is de soort aansluiting altijd 'verplicht'
Revision History
Label DATE           Name                   Description
1.0   07-10-2009     A. Budiawan            Initial Creation
1.1   08-02-2010     Karel Vlieg            Betekenisloos uniek nummer toegevoegd tbv ADF
**********************************************************************************************************/


FUNCTION GET_RLE_PRS_VZP
 (P_PERSOONSNUMMER IN varchar2
 ,P_RELATIENUMMER IN number
 ,P_PEILDATUM IN date := sysdate
 )
 RETURN T_PRS_TYP pipelined
 IS

  cursor c_rgg( b_persoonsnr in varchar2, b_peildatum in date )
  is
    select rgg.*
    from   ( -- AOV
             select to_number(null) id
             ,      to_number(null) relatienummer -- dummy to_number vanwege error PLS-00382
             ,      to_char( pav.prs_persoonsnummer ) persoonsnummer
             ,      adm.type adm_type
             ,      fon.adm_code adm_code
             ,      fon.display_kodefon rgg_code
             ,      null rgg_ond
             ,      vzp.omschrijving omschrijving
             ,      pav.ingangsdatum datum_begin
             ,      pav.einddatum datum_eind
             ,      gc_collectief col_ind
             ,      null srt_reg_ond
             ,      gc_aansl_vw srt_aansluiting
             from   polissen_aov pav
             ,      verzekeringspakketten vzp
             ,      fon fon
             ,      rle_administraties adm
             where  pav.vzp_nummer = vzp.verzekeringspakketnummer
             and    vzp.fon_kodefon = fon.kodefon
             and    fon.adm_code = adm.code
             and    pav.ind_vervallen = 'N'
             and    (    pav.einddatum is null
                      or pav.einddatum = ( select max( pav2.einddatum ) keep ( dense_rank last order by pav2.einddatum )
                                           from   polissen_aov pav2
                                           ,      verzekeringspakketten vzp2
                                           ,      fon fon2
                                           where  pav2.vzp_nummer = vzp2.verzekeringspakketnummer
                                           and    vzp2.fon_kodefon = fon2.kodefon
                                           and    pav2.prs_persoonsnummer = pav.prs_persoonsnummer
                                           and    pav2.ind_vervallen = 'N'
                                           and    (    stm_context.administratie is null
                                                    or upper( stm_context.administratie ) = 'ALG'
                                                    or upper( fon2.adm_code ) = upper( stm_context.administratie )
                                                  )
                                         )
                    )
             and    (    stm_context.administratie is null
                      or upper( stm_context.administratie ) = 'ALG'
                      or upper( fon.adm_code ) = upper( stm_context.administratie )
                    )
             and    to_char( pav.prs_persoonsnummer ) = b_persoonsnr
             union
             -- bedrijfssparen
             select to_number(null) id
             ,      to_number(null) relatienummer
             ,      to_char( sme.sde_prs_persoonsnummer ) persoonsnummer
             ,      'V' adm_type
             ,      'BSP' adm_code
             ,      'BSP' rgg_code
             ,      null rgg_ond
             ,      'Bedrijfssparen' omschrijving
             ,      min( sme.begindatum ) datum_begin
             ,      max( sme.einddatum ) keep ( dense_rank last order by sme.einddatum ) datum_eind
             ,      gc_collectief col_ind
             ,      null srt_reg_ond
             ,      gc_aansl_vw srt_aansluiting
             from   bsp_product_deelnames sme
             where  sme.bevestigingsdatum is not null
             and    to_char( sme.sde_prs_persoonsnummer ) = b_persoonsnr
             group by sme.sde_prs_persoonsnummer
             union
             -- individuele vrijstellingen
             select to_number(null) id
             ,      to_number(null) relatienummer
             ,      to_char( vrs.prs_persoonsnummer ) persoonsnummer
             ,      adm.type adm_type
             ,      fon.adm_code adm_code
             ,      fon.display_kodefon rgg_code
             ,      null rgg_ond
             ,      fon.naam omschrijving
             ,      vrs.aanvangsdatum datum_begin
             ,      vrs.einddatum datum_eind
             ,      gc_individueel col_ind
             ,      null srt_reg_ond
             ,      gc_aansl_vs srt_aansluiting
             from   individuele_vrijstellingen vrs
             ,      fon fon
             ,      rle_administraties adm
             where  adm.code = fon.adm_code
             and    fon.kodefon = vrs.fon_code_fonds
             and    adm.type <> 'P' -- Pensioen
             and    (    stm_context.administratie is null
                      or upper( stm_context.administratie ) = 'ALG'
                      or upper( fon.adm_code ) = upper( stm_context.administratie )
                    )
             and    to_char( vrs.prs_persoonsnummer ) = b_persoonsnr
             union
             -- Werkgever collective regelingen
             select distinct to_number(null) id
             ,               to_number(null) relatienummer
             ,               avg.werknemer persoonsnummer
             ,               wgr0902.adm_type
             ,               wgr0902.adm_code
             ,               wgr0902.rgg_code
             ,               wgr0902.rgg_ond
             ,               wgr0902.omschrijving
             ,               wgr0902.datum_begin
             ,               wgr0902.datum_eind
             ,               wgr0902.col_ind
             ,               wgr0902.srt_reg_ond
             ,               case upper( wgr0902.srt_aansluiting ) when upper( gc_aansl_vw ) then gc_aansl_vp
                                                                                             else wgr0902.srt_aansluiting
                             end srt_aansluiting
             from   avg_arbeidsverhoudingen avg
             ,      table( comp_wgr.wgr_reg_vzn.get_reg_wgr_vzn( null, avg.werkgever ) ) wgr0902
             where  avg.werkgever = wgr0902.werkgevernummer
             and    wgr0902.rgg_code not in ('AOV', 'BSP')
             and    (     upper( wgr0902.col_ind ) = upper( gc_collectief ) -- collectief
                      and (    wgr0902.functiecategorie = '*'
                            or exists ( select 1
                                        from   avg_werknemer_beroepen wbp
                                        ,      beroepen brp
                                        where  brp.code_beroep = wbp.code_beroep
                                        and    wbp.avg_id = avg.id
                                        and    brp.code_functiecategorie = wgr0902.functiecategorie
                                      )
                          )
                    )
             and    avg.dat_begin <= trunc( b_peildatum )
             and    (    avg.dat_einde is null
                      or avg.dat_einde >= trunc( b_peildatum )
                    )
             and not exists ( select 1
                              from   individuele_vrijstellingen vrs
                              ,      fon fon
                              ,      rle_administraties adm
                              where  adm.code = fon.adm_code
                              and    fon.kodefon = vrs.fon_code_fonds
                              and    adm.type <> 'P' -- Pensioen
                              and    (    stm_context.administratie is null
                                       or upper( stm_context.administratie ) = 'ALG'
                                       or upper( fon.adm_code ) = upper( stm_context.administratie )
                                     )
                              and    vrs.prs_persoonsnummer = avg.werknemer
                              and    fon.kodefon = wgr0902.rgg_code
                            )
             and    (    stm_context.administratie is null
                      or upper( stm_context.administratie ) = 'ALG'
                      or upper( wgr0902.adm_code ) = upper( stm_context.administratie )
                    )
             and    avg.werknemer = b_persoonsnr
           ) rgg
    --
    order by rgg.rgg_code
           , decode( rgg.srt_aansluiting, gc_aansl_vp, 1
                                        , gc_aansl_vw, 2
                                        , gc_aansl_vs, 3
                   )
  ;
  --
  l_persoonsnummer rle_relatie_externe_codes.extern_relatie%type := p_persoonsnummer;
  l_relatienummer rle_relaties.numrelatie%type := p_relatienummer;
  --
  e_param_verplicht exception;
  --
begin
  -- check eerst of er tenminste één van onderstaande parameters gevuld is.
  if (     l_persoonsnummer is null
       and l_relatienummer is null
     )
  then
    raise e_param_verplicht;
  end if;

  -- haal het persoonsnummer uit de database
  if l_persoonsnummer is null then
    l_persoonsnummer := rle_lib.get_persoonsnr_relatie( p_relatienummer => l_relatienummer );
  elsif l_relatienummer is null then
    l_relatienummer := rle_lib.get_relatienr_persoon( p_persoonsnummer => l_persoonsnummer );
  end if;

  for r_rgg in c_rgg( b_persoonsnr => l_persoonsnummer, b_peildatum => p_peildatum ) loop
    --
    select rle_prs_vzp_seq1.nextval into r_rgg.id from dual;
    r_rgg.relatienummer := l_relatienummer;
    --
    pipe row( r_rgg );
    --
  end loop;
  --
exception
  when e_param_verplicht then
    -- foutmelding
    raise_application_error( -20000, 'Minimal één van de volgende parameters is verplicht: ' ||
                                     'P_PERSOONSNUMMER, P_RELATIENUMMER.'
                           );
  when others then
    -- foutmelding
    raise_application_error( -20000, stm_app_error ( sqlerrm, null, null )
                                  || ' ' || dbms_utility.format_error_backtrace
                           );
end;
end rle_prs_vzp;
/