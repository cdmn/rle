CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_PCK_LIJSTEN IS

LGN_I093_NUMRELATIE_AKR NUMBER;
LGD_I093_PEILDATUM DATE;
LG_CODE_SRTADR RLE_RELATIE_ADRESSEN.DWE_WRDDOM_SRTADR%TYPE;
LG_CODE_EXTSYS RLE_RELATIE_EXTERNE_CODES.DWE_WRDDOM_EXTSYS%TYPE;
LG_CODE_ROL_IN_ADMIN RLE_ROLLEN.CODROL%TYPE;
LG_PEILDATUM DATE;

CURSOR C_RLE_033
 (B_MUTATIE_VANAF IN DATE
 ,B_MUTATIE_TM IN DATE
 ,B_NUMRELATIE IN NUMBER
 ,B_CODDOELREK IN VARCHAR2
 )
 IS
SELECT fnr.datingang
,      fnr.dat_mutatie
,      fnr.mutatie_door
,      fnr.numrekening
FROM   rle_financieel_nummers fnr
WHERE  fnr.rle_numrelatie = b_numrelatie
AND    fnr.coddoelrek = b_coddoelrek
AND    fnr.rol_codrol = 'WG'
AND    fnr.dat_mutatie BETWEEN b_mutatie_vanaf and b_mutatie_tm
ORDER BY datingang desc
;

r_rle_033 c_rle_033%rowtype;
/* Ophalen administraties bij relatie. */
CURSOR C_RIE_001
 (B_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,B_ROL_CODROL IN RLE_RELATIEROLLEN_IN_ADM.ROL_CODROL%TYPE
 )
 IS
/* C_RIE_001 */
select adm_code
from   rle_relatierollen_in_adm rie
where  rie.rle_numrelatie = b_numrelatie
and    rie.rol_codrol     = nvl( b_rol_codrol, rie.rol_codrol )
;
CURSOR C_RLE_031
 (B_MUTATIE_VANAF IN DATE
 ,B_MUTATIE_TM IN DATE
 ,B_WERKGEVER IN VARCHAR2
 ,B_HERSTART_WERKGEVER IN VARCHAR2
 )
 IS
SELECT   DISTINCT(rec.extern_relatie) extern_relatie
,        rec.rle_numrelatie
FROM     rle_financieel_nummers fnr
,        rle_relatie_externe_codes rec
,        rle_relaties rle
WHERE    rle.numrelatie = rec.rle_numrelatie
AND      rle.numrelatie = fnr.rle_numrelatie
AND      rec.extern_relatie = NVL(b_werkgever, rec.extern_relatie)
AND      rec.dwe_wrddom_extsys = 'WGR'
AND      fnr.rol_codrol = 'WG'
AND      fnr.dat_mutatie BETWEEN b_mutatie_vanaf and b_mutatie_tm
AND      rec.extern_relatie  > b_herstart_werkgever
ORDER BY rec.extern_relatie
;
CURSOR C_RLE_032
 (B_MUTATIE_VANAF IN DATE
 ,B_MUTATIE_TM IN DATE
 ,B_RELATIE IN NUMBER
 ,B_WERKNEMER IN VARCHAR2
 ,B_HERSTART_RELATIE IN NUMBER
 )
 IS
SELECT rle.numrelatie
,      rec.extern_relatie
,      rle.namrelatie
,      rle.dwe_wrddom_geslacht
,      rle.dwe_wrddom_vvgsl
,      rle.voorletter
,      rle.voornaam
,      rle.datgeboorte
,      rle.code_gebdat_fictief
,      rle.indcontrole
,      rle.datcontrole
,      rle.dwe_wrddom_titel --????? code naamgebruik
,      rle.dwe_wrddom_gewatitel
,      rle.datoverlijden
,      rle.regdat_overlbevest
,      rle.datemigratie
,      rle.datbegin begindatum_rle
,      rle.dateinde einddatum_rle
,      rle.dat_creatie creatiedatum_rle
,      rle.creatie_door creatie_door_rle
,      rle.dat_mutatie mutatiedatum_rle
,      rle.mutatie_door mutatie_door_rle
,      rec.datingang begindatum_werknemer
,      rec.dateinde eindddatum_werknemer
,      rec.dat_creatie creatiedatum_werknemer
,      rec.creatie_door creatie_door_werknemer
,      rec.dat_mutatie mutatiedatum_werknemer
,      rec.mutatie_door mutatie_door_werknemer
,      asa.code_gba_status
,      asa.afnemers_indicatie
,      asa.dat_creatie creatiedatum_gba
,      asa.creatie_door creatie_door_gba
,      asa.dat_mutatie mutatiedatum_gba
,      asa.mutatie_door mutatie_door_gba
FROM     rle_afstemmingen_gba asa
,        rle_relatie_externe_codes rec
,        rle_relaties rle
WHERE    rle.numrelatie = rec.rle_numrelatie
AND      rle.numrelatie = asa.rle_numrelatie
AND      asa.code_gba_status = 'OV'
AND      rec.dwe_wrddom_extsys = 'PRS'
AND      (
              TO_CHAR(rle.dat_mutatie,'YYYYMMDDHH24MI') BETWEEN
              TO_CHAR(b_mutatie_vanaf,'YYYYMMDDHH24MI') AND
              TO_CHAR(b_mutatie_tm,'YYYYMMDDHH24MI')
          OR
             TO_CHAR(rec.dat_mutatie,'YYYYMMDDHH24MI') BETWEEN
             TO_CHAR(b_mutatie_vanaf,'YYYYMMDDHH24MI') AND
             TO_CHAR(b_mutatie_tm,'YYYYMMDDHH24MI')
          OR
              TO_CHAR(asa.dat_mutatie,'YYYYMMDDHH24MI') BETWEEN
              TO_CHAR(b_mutatie_vanaf,'YYYYMMDDHH24MI') AND
              TO_CHAR(b_mutatie_tm,'YYYYMMDDHH24MI')
          )
AND     rec.extern_relatie = NVL( b_werknemer, rec.extern_relatie)
AND     rle.numrelatie = NVL( b_relatie, rle.numrelatie)
AND     rle.numrelatie > b_herstart_relatie
ORDER BY rle.numrelatie
,        rec.extern_relatie
;
r_rle_032 c_rle_032%rowtype;
CURSOR C_RKN_010
 (CPN_NUMRELATIE IN number
 ,CPD_PEILDATUM IN date
 )
 IS
SELECT rkn.rle_numrelatie
,      rkn.rle_numrelatie_voor        rkn_rle_numrelatie_voor
,      rec.extern_relatie             rec_werkgeversnummer
,      rkn.dat_begin                  rkn_dat_begin
,      rkn.dat_einde                  rkn_dat_einde
FROM   rle_relatie_koppelingen        rkn
,      rle_rol_in_koppelingen         rkg
--,      rle_relaties                   rle
,      rle_relatie_externe_codes      rec
WHERE  -- ophalen alle werkgevers bij een adm. kantoor
	 rkn.rle_numrelatie             = cpn_numrelatie
AND    rkn.rkg_id                     = rkg.id
AND    rkg.code_rol_in_koppeling      = 'AV'
--AND    rle.numrelatie                 = rkn.rle_numrelatie_voor
--AND    rec.rle_numrelatie             = rle.numrelatie
AND rec.rle_numrelatie = rkn.rle_numrelatie_voor
AND    rec.dwe_wrddom_extsys          = 'WGR'
AND    (NVL(cpd_peildatum, rkn.dat_begin) BETWEEN
            rkn.dat_begin
        AND NVL( rkn.dat_einde, cpd_peildatum )
 OR     cpd_peildatum IS NULL)
;
/* Ophalen numadres via postcode, huisnummer en toevoeging */
CURSOR C_ADS_019
 (B_POSTCODE IN varchar2
 ,B_HUISNUMMER IN number
 ,B_TOEVNUM IN varchar2
 )
 IS
SELECT  numadres
FROM     rle_adressen
WHERE  postcode        = b_postcode
AND       huisnummer  = b_huisnummer
AND       NVL(toevhuisnum, '@')  = NVL(b_toevnum, '@')
;
CURSOR C_RAS_033
 (B_NUMADRES IN number
 )
 IS
select ras.rle_numrelatie
from   rle_relatie_adressen ras
,      rle_relatie_rollen rrl
where  ras.ads_numadres = b_numadres
and    rrl.rle_numrelatie = ras.rle_numrelatie
and    rrl.rol_codrol = 'AK'
;
/* CODA:  rollen bij een relatie */
CURSOR C_RRL_004
 (B_NUMRELATIE NUMBER
 ,B_ROL VARCHAR2
 )
 IS
/* C_RRL_004 */
select	rrl.rol_codrol
from	rle_relatie_rollen    rrl
where	rrl.rle_numrelatie = b_numrelatie
and   rrl.rol_codrol = b_rol;
/* Ophalen alle woonplaatsen */
CURSOR C_WPS_010
 (B_CODLAND IN VARCHAR2
 )
 IS
/* C_WPS_010 */
SELECT wps.woonplaats
,      lnd_codland
from   rle_woonplaatsen wps
where  (b_codland is null
       or b_codland = lnd_codland)
;
/* OPhalen  alle geldige landen */
CURSOR C_LND_004
 IS
/* C_LND_004 */
select	lnd.codland
,                 lnd.naam
from	rle_landen lnd
where	(lnd.dat_einde >=  sysdate
or                lnd.dat_einde is null);
/* Ophalen rollen via relatierollen */
CURSOR C_ROL_004
 (B_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 )
 IS
/* C_ROL_004 */
SELECT	 rol.CODROL
,     	 rol.OMSROL
FROM	 rle_rollen rol
,   	 rle_relatie_rollen rrl
WHERE	 rrl.rol_codrol    	 = rol.codrol
and  	 rrl.rle_numrelatie	 = b_numrelatie;
/* Ophalen relatiekoppelingen */
CURSOR C_RKN_001
 (CPN_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 )
 IS
/* C_RKN_001

   In RLE_RELATIE_KOPPELINGEN is NUM_RELATIE
   het onderwerp van de koppeling; NUM_RELATIE_VOOR
   is het leidend voorwerp.

   Dus:
   NUM_RELATIE is <koppeling> van NUM_RELATIE_VOOR
   Bijvoorbeeld
   NUM_RELATIE is KIND VAN NUM_RELATIE_VOOR
*/
SELECT rkn.rle_numrelatie_voor    rkn_rle_numrelatie_voor
,      rkg.code_rol_in_koppeling  rkg_code_rol_in_koppeling
,      rkg.omschrijving           rkg_omschrijving
,      rkn.dat_begin              rkn_dat_begin
,      rkn.dat_einde              rkn_dat_einde
,      rle.datbegin               rle_dat_begin
,      rle.dateinde               rle_dat_einde
,      rkn.mutatie_door
,      rkn.dat_mutatie
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
,      rle_relaties             rle
WHERE  rkn.rle_numrelatie = cpn_numrelatie
AND    rkn.rkg_id         = rkg.id
AND    rle.numrelatie     = rkn.rle_numrelatie_voor
;
/* Ophalen verwantschappen (relatiekoppelingen met type 'G', 'H' of 'S' ) */
CURSOR C_RKN_002
 (CPN_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 )
 IS
/* C_RKN_002

   In RLE_RELATIE_KOPPELINGEN is NUM_RELATIE
   het onderwerp van de koppeling; NUM_RELATIE_VOOR
   is het leidend voorwerp.

   Dus:
   NUM_RELATIE is <koppeling> van NUM_RELATIE_VOOR
   Bijvoorbeeld
   NUM_RELATIE is KIND VAN NUM_RELATIE_VOOR
*/
SELECT rkn.rle_numrelatie_voor    rkn_rle_numrelatie_voor
,      rkg.code_rol_in_koppeling  rkg_code_rol_in_koppeling
,      rkg.omschrijving           rkg_omschrijving
,      rkn.dat_begin              rkn_dat_begin
,      rkn.dat_einde              rkn_dat_einde
,      rle.datgeboorte            rle_dat_geboorte
,      rle.datoverlijden          rle_dat_overlijden
,      rle.dwe_wrddom_geslacht    rle_geslacht
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
,      rle_relaties             rle
WHERE  rkn.rle_numrelatie = cpn_numrelatie
AND    rkn.rkg_id         = rkg.id
AND    rle.numrelatie     = rkn.rle_numrelatie_voor
AND    rkg.code_rol_in_koppeling IN ( 'S', 'H', 'G', 'O', 'K' )
;
/* Ophalen contactpersonen van een relatie op een bepaald adres */
CURSOR C_RLE_023
 (CPN_RLE_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,CPN_RAS_ID IN RLE_RELATIE_ADRESSEN.ID%TYPE
 ,CPV_POSTCODE IN RLE_ADRESSEN.POSTCODE%TYPE
 ,CPN_HUISNUMMER IN RLE_ADRESSEN.HUISNUMMER%TYPE
 ,CPV_TOEVOEGING IN VARCHAR2
 ,CPV_SOORT_ADRES IN VARCHAR2
 ,B_SOORT_CONTACTPERSOON IN VARCHAR2
 )
 IS
SELECT cpn.rle_numrelatie      -- numrelatie van de contactpersoon
,      rle.dwe_wrddom_geslacht
,      cpn.code_functie
,      cpn.code_soort_contactpersoon  -- OSP 13-10-2003
FROM   rle_contactpersonen      cpn
,      rle_relatie_adressen     ras
,      rle_adressen             ads
,      rle_relatie_koppelingen  rkg
,      rle_relaties             rle
WHERE  rle.numrelatie          = cpn.rle_numrelatie
AND    rkg.rle_numrelatie_voor = cpn_rle_numrelatie -- numrelatie vd werkgever
AND    rkg.rle_numrelatie      = cpn.rle_numrelatie -- koppeling contactpersoon
AND    cpn.ras_id              = NVL(cpn_ras_id,cpn.ras_id)
AND    ras.id                  = cpn.ras_id         -- rel.adres vd contactpersoon
AND    ras.ads_numadres        = ads.numadres
AND    nvl(cpn.code_soort_contactpersoon, '@') = nvl(b_soort_contactpersoon, nvl(cpn.code_soort_contactpersoon, '@')) -- OSP 07-10-2003
AND    ras.dwe_wrddom_srtadr   = NVL(cpv_soort_adres, ras.dwe_wrddom_srtadr)
AND    NVL(ads.postcode,'@')   = NVL(cpv_postcode,NVL(ads.postcode,'@'))
AND    NVL(ads.huisnummer,-1) = NVL(cpn_huisnummer,NVL(ads.huisnummer,-1))
AND    NVL(ads.toevhuisnum,'@')= NVL(cpv_toevoeging,NVL(ads.toevhuisnum,'@'))
AND    rkg.dat_einde           IS NULL              -- einddat contactpersoon leeg
ORDER BY rkg.dat_begin DESC;                        -- meest recente contactpersoon eerst
/* Ophalen externe relaties */
CURSOR C_RLE_024
 (CPN_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,CPV_COD_ROL_IN_ADMIN IN RLE_ROLLEN.CODROL%TYPE
 ,CPD_PEILDATUM IN DATE
 )
 IS
/* C_RLE_024: Ophalen externe relaties. */
SELECT rle.numrelatie
,      rle.datbegin
,      rle.dateinde
FROM   rle_relaties               rle
,      rle_relatie_koppelingen    rkn
,      rle_rol_in_koppelingen     rkg
WHERE  rkn.rle_numrelatie_voor             =  cpn_numrelatie
AND    rle.numrelatie                      =  rkn.rle_numrelatie
AND    rkn.rkg_id                          =  rkg.id
AND    rkg.rol_codrol_beperkt_tot          =  cpv_cod_rol_in_admin
AND    rkg.rol_codrol_met                  =  'WG'
AND    rkn.dat_begin                       <= cpd_peildatum
AND    NVL( rkn.dat_einde, cpd_peildatum ) >= cpd_peildatum
;
/* Ophalen relatiekoppelingen */
CURSOR C_RKN_007
 (CPN_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 )
 IS
/* C_RKN_007

   In RLE_RELATIE_KOPPELINGEN is NUM_RELATIE
   het onderwerp van de koppeling; NUM_RELATIE_VOOR
   is het leidend voorwerp.

   Dus:
   NUM_RELATIE is <koppeling> van NUM_RELATIE_VOOR
   Bijvoorbeeld
   NUM_RELATIE is KIND VAN NUM_RELATIE_VOOR
*/
SELECT rkn.rle_numrelatie_voor    rkn_rle_numrelatie_voor
,      rkg.code_rol_in_koppeling  rkg_code_rol_in_koppeling
,      rkg.omschrijving           rkg_omschrijving
,      rkn.dat_begin              rkn_dat_begin
,      rkn.dat_einde              rkn_dat_einde
,      rle.datbegin               rle_dat_begin
,      rle.dateinde               rle_dat_einde
,      rkn.mutatie_door
,      rkn.dat_mutatie
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
,      rle_relaties             rle
WHERE  rkn.rle_numrelatie = cpn_numrelatie
AND    rkn.rkg_id         = rkg.id
AND    rle.numrelatie     = rkn.rle_numrelatie_voor
-- Alleen de koppelingen die mogelijk zijn tussen werkgevers onderling
AND    rkg.rol_codrol_beperkt_tot = 'WG'
AND    rkg.rol_codrol_met               = 'WG'
;


/* Start lijst voor I-117 */
PROCEDURE PRC_INIT_I117
 (P_MUTATIE_VANAF IN DATE
 ,P_MUTATIE_TM IN DATE
 ,P_WERKGEVER IN VARCHAR2
 ,P_HERSTART_WERKGEVER IN VARCHAR2
 )
 IS
BEGIN
if c_rle_031%isopen
then
  close c_rle_031;
end if;

open c_rle_031
     ( p_mutatie_vanaf
     , p_mutatie_tm
     , p_werkgever
     , p_herstart_werkgever
     );

exception
when others then
  if c_rle_031%isopen
  then
     close c_rle_031;
  end if;
  raise;
END PRC_INIT_I117;
/* Ophalen gemuteerde banknummers */
PROCEDURE PRC_GET_I117
 (P_MUTATIE_VANAF IN DATE
 ,P_MUTATIE_TM IN DATE
 ,P_NUMREKENING_AG OUT VARCHAR2
 ,P_NUMREKENING_AI OUT VARCHAR2
 ,P_MUTATIEDATUM OUT DATE
 ,P_MUTATIE_DOOR OUT VARCHAR2
 ,P_BEGINDATUM OUT DATE
 ,P_WERKGEVER OUT VARCHAR2
 )
 IS

L_MUTATIE_DOOR_AG VARCHAR2(30);
L_DAT_MUTATIE_AG DATE;
L_DAT_BEGIN_AG DATE;
L_NUMREKENING_AG VARCHAR2(34);
L_MUTATIE_DOOR_AI VARCHAR2(30);
L_DAT_MUTATIE_AI DATE;
L_DAT_BEGIN_AI DATE;
L_NUMREKENING_AI VARCHAR2(34);
BEGIN
DECLARE
--
r_rle_031        c_rle_031%rowtype;
r_rle_033        c_rle_033%rowtype;
--
BEGIN
  IF c_rle_031%isopen
  THEN
    fetch c_rle_031 into r_rle_031;
    IF c_rle_031%found
    THEN
       -- Bepaal het acceptgiro-rekeningnummer
       open c_rle_033( p_mutatie_vanaf
                     , p_mutatie_tm
                     , r_rle_031.rle_numrelatie
                     , 'ALGRN');
       fetch c_rle_033 into r_rle_033;
       IF c_rle_033%found
       THEN
          l_numrekening_ag  := r_rle_033.numrekening;
          l_dat_mutatie_ag  := r_rle_033.dat_mutatie;
          l_mutatie_door_ag := r_rle_033.mutatie_door;
          l_dat_begin_ag   := r_rle_033.datingang;
       ELSE
          l_numrekening_ag  := NULL;
          l_dat_mutatie_ag  := NULL;
          l_mutatie_door_ag := NULL;
          l_dat_begin_ag   := NULL;

       END IF;
       CLOSE c_rle_033;

       -- Bepaal het incasso-rekeningnummer
       open c_rle_033( p_mutatie_vanaf
                     , p_mutatie_tm
                     , r_rle_031.rle_numrelatie
                     , 'AIC');
       fetch c_rle_033 into r_rle_033;
       IF c_rle_033%found
       THEN
          l_numrekening_ai  := r_rle_033.numrekening;
          l_dat_mutatie_ai  := r_rle_033.dat_mutatie;
          l_mutatie_door_ai := r_rle_033.mutatie_door;
          l_dat_begin_ai   := r_rle_033.datingang;
       ELSE
          l_numrekening_ai  := NULL;
          l_dat_mutatie_ai  := NULL;
          l_mutatie_door_ai := NULL;
          l_dat_begin_ai   := NULL;
       END IF;
       CLOSE c_rle_033;

       -- Bepaal de hoogste mutatiedatum van AG en AI en geef deze mee als output
       IF l_dat_mutatie_ag is not null
       THEN
          IF l_dat_mutatie_ai is not null
          THEN
             IF l_dat_mutatie_ag >= l_dat_mutatie_ai
             THEN
                p_mutatiedatum   := l_dat_mutatie_ag;
                p_mutatie_door   := l_mutatie_door_ag;
                p_begindatum     := l_dat_begin_ag;
                p_werkgever      := r_rle_031.extern_relatie;
                p_numrekening_ag := l_numrekening_ag;
                p_numrekening_ai := l_numrekening_ai;
             ELSE
                p_mutatiedatum   := l_dat_mutatie_ai;
                p_mutatie_door   := l_mutatie_door_ai;
                p_begindatum     := l_dat_begin_ai;
                p_werkgever      := r_rle_031.extern_relatie;
                p_numrekening_ag := l_numrekening_ag;
                p_numrekening_ai := l_numrekening_ai;
             END IF;
          ELSE -- dat_mutatie_ai is null
             p_mutatiedatum   := l_dat_mutatie_ag;
             p_mutatie_door   := l_mutatie_door_ag;
             p_begindatum     := l_dat_begin_ag;
             p_werkgever      := r_rle_031.extern_relatie;
             p_numrekening_ag := l_numrekening_ag;
             p_numrekening_ai := NULL;
         END IF;
       ELSE -- l_dat_mutatie_ag is null
          IF l_dat_mutatie_ai is not null
          THEN
             p_mutatiedatum   := l_dat_mutatie_ai;
             p_mutatie_door   := l_mutatie_door_ai;
             p_begindatum     := l_dat_begin_ai;
             p_werkgever      := r_rle_031.extern_relatie;
             p_numrekening_ag := NULL;
             p_numrekening_ai := l_numrekening_ai;
          ELSE -- zowel dat_ag en dat_ai is null
             p_mutatiedatum   := NULL;
             p_mutatie_door   := NULL;
             p_begindatum     := NULL;
             p_werkgever      := NULL;
             p_numrekening_ag := NULL;
             p_numrekening_ai := NULL;
          END IF;
       END IF;
    ELSE
       close c_rle_031;
       p_numrekening_ag := null;
       p_numrekening_ai := null;
       p_mutatiedatum   := null;
       p_mutatie_door   := null;
       p_begindatum     := null;
       p_werkgever      := null;
    END IF;
  ELSE
     p_numrekening_ag := null;
     p_numrekening_ai := null;
     p_mutatiedatum   := null;
     p_mutatie_door   := null;
     p_begindatum     := null;
     p_werkgever      := null;
  END IF;
--
EXCEPTION
  when others
  then
    if c_rle_031%isopen
    then
      close c_rle_031;
    end if;
    if c_rle_033%isopen
    then
      close c_rle_033;
    end if;
    raise;
END;
END PRC_GET_I117;
/* Ophalen gemuteerde personen */
PROCEDURE PRC_GET_I118
 (P_RELATIE OUT NUMBER
 ,P_WERKNEMER OUT VARCHAR2
 ,P_MUTATIEDATUM_RLE OUT DATE
 ,P_MUTATIE_DOOR_RLE OUT VARCHAR2
 ,P_GEBOORTEDATUM OUT DATE
 ,P_CODE_GEBDAT_FICTIEF OUT VARCHAR2
 ,P_OVERLIJDENSDATUM OUT DATE
 )
 IS
BEGIN
DECLARE
--
r_rle_032        c_rle_032%rowtype;
--
BEGIN
  IF c_rle_032%isopen
  THEN
    fetch c_rle_032 into r_rle_032;
    IF c_rle_032%found
    THEN
       p_relatie := r_rle_032.numrelatie;
       p_werknemer := r_rle_032.extern_relatie;
       p_mutatiedatum_rle := r_rle_032.mutatiedatum_rle;
       p_mutatie_door_rle := r_rle_032.mutatie_door_rle;
       p_geboortedatum := r_rle_032.datgeboorte;
       p_code_gebdat_fictief := r_rle_032.code_gebdat_fictief;
       p_overlijdensdatum := r_rle_032.datoverlijden;
    ELSE
       close c_rle_032;
       p_relatie := null;
       p_werknemer := null;
       p_mutatiedatum_rle := null;
       p_mutatie_door_rle := null;
       p_geboortedatum := null;
       p_code_gebdat_fictief := null;
       p_overlijdensdatum := null;
    END IF;
  ELSE
       p_relatie := null;
       p_werknemer := null;
       p_mutatiedatum_rle := null;
       p_mutatie_door_rle := null;
       p_geboortedatum := null;
       p_code_gebdat_fictief := null;
       p_overlijdensdatum := null;
  END IF;
--
EXCEPTION
  when others
  then
    if c_rle_032%isopen
    then
      close c_rle_032;
    end if;
    raise;
END;
END PRC_GET_I118;
/* Start lijst voor I-118 */
PROCEDURE PRC_INIT_I118
 (P_MUTATIE_VANAF IN DATE
 ,P_MUTATIE_TM IN DATE
 ,P_RELATIE IN NUMBER
 ,P_WERKNEMER IN VARCHAR2
 ,P_HERSTART_RELATIE IN NUMBER
 )
 IS
BEGIN
if c_rle_032%isopen
then
  close c_rle_032;
end if;

open c_rle_032
     ( p_mutatie_vanaf
     , p_mutatie_tm
     , p_relatie
     , p_werknemer
     , p_herstart_relatie
     );

exception
when others then
  if c_rle_032%isopen
  then
     close c_rle_032;
  end if;
  raise;
END PRC_INIT_I118;
/* Ophalen lijst i_103 (ophalen adminkantoren bij adres) */
PROCEDURE PRC_GET_I103
 (P_NUMRELATIE OUT number
 ,P_NAAM OUT varchar2
 ,P_EXTERN_NR OUT varchar2
 ,P_TELNR OUT varchar2
 )
 IS

L_NUMRELATIE NUMBER;
R_RAS_033 C_RAS_033%ROWTYPE;
L_DUMMY VARCHAR2(100);

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_i103 */
BEGIN
p_naam         := NULL;
p_extern_nr    := NULL;
p_telnr        := NULL;
p_numrelatie   := NULL;
   IF c_ras_033%ISOPEN
   THEN
          FETCH c_ras_033 INTO r_ras_033;
          IF c_ras_033%FOUND
          THEN
               p_numrelatie := r_ras_033.rle_numrelatie;
                /* ophalen nummer van het adminstratiekantoor */
		    p_extern_nr :=  RLE_M03_REC_EXTCOD('AKR'
						,  p_numrelatie
						, 'AK'
						,   SYSDATE)
						;

                /* ophalen telefoonnummer */
                RLE_M30_COMNUM(p_numrelatie
		   		, NULL
				, 'TEL'
				, SYSDATE
				, p_telnr
				, l_dummy
				, l_dummy)
				;
		    /* ophalen naam */
		    p_naam :=  RLE_GET_NAM (p_numrelatie);
           ELSE
             	/* Geen nieuwe rij gevonden */
             	CLOSE c_ras_033 ;
          END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_ras_033%ISOPEN

 THEN

         CLOSE c_ras_033 ;

 END IF ;

p_numrelatie := null;

p_extern_nr   := null;

p_naam           := null;

p_telnr             := null;

 RAISE ;
END PRC_GET_I103;
/* Start lijst voor I_103 (ophalen adminkantoren per adres) */
PROCEDURE PRC_INIT_I103
 (P_POSTCODE IN varchar2
 ,P_HUISNUMMER IN number
 ,P_TOEVNUM IN varchar2
 )
 IS

L_NUMADRES NUMBER;
/* rle_pck_lijsten.prc_init_i103 */

   /* ophalen relatieadres */
BEGIN
OPEN  c_ads_019 (p_postcode, p_huisnummer, p_toevnum);
   FETCH c_ads_019 INTO l_numadres;
   CLOSE c_ads_019;

   IF l_numadres IS NOT NULL
   THEN
      IF c_ras_033%ISOPEN
      THEN
         CLOSE c_ras_033;
      END IF ;

      OPEN c_ras_033(l_numadres) ;
   END IF;
END PRC_INIT_I103;
/* Start lijst voor I-107 (Ophalen woonplaatsen) */
PROCEDURE PRC_INIT_I107
 (P_CODLAND IN VARCHAR2 := null
 )
 IS
/* rle_pck_lijsten.prc_init_i107 */
BEGIN
IF c_wps_010%ISOPEN
   THEN
      CLOSE c_wps_010 ;
   END IF ;

   OPEN c_wps_010(p_codland);
END PRC_INIT_I107;
/* Ophalen woonplaatsen */
PROCEDURE PRC_GET_I107
 (P_WOONPLAATS OUT varchar2
 ,P_CODE_LAND OUT varchar2
 )
 IS

R_WPS_010 C_WPS_010%ROWTYPE;

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_i107 */
BEGIN
IF c_wps_010%ISOPEN
   THEN
          FETCH c_wps_010 INTO r_wps_010;
          IF c_wps_010%FOUND
          THEN
            	p_woonplaats 	:= r_wps_010.woonplaats ;
                  p_code_land 	:= r_wps_010.lnd_codland;
          ELSE
             	/* Geen nieuwe rij gevonden */
             	CLOSE c_wps_010 ;
          END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_wps_010%ISOPEN

 THEN

         CLOSE c_wps_010 ;

 END IF ;

 p_woonplaats := null;

 p_code_land := null;

 RAISE ;
END PRC_GET_I107;
/* Ophalen landen */
PROCEDURE PRC_GET_I108
 (P_CODELAND OUT VARCHAR2
 ,P_NAAM OUT VARCHAR2
 )
 IS

R_LND_004 C_LND_004%ROWTYPE;

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_i108 */
BEGIN
IF c_lnd_004%ISOPEN
   THEN
          FETCH c_lnd_004 INTO r_lnd_004;
          IF c_lnd_004%FOUND
          THEN
            	p_codeland	:= r_lnd_004.codland ;
             	p_naam		:= r_lnd_004.naam ;
          ELSE
             	/* Geen nieuwe rij gevonden */
             	CLOSE c_lnd_004 ;
          END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_lnd_004%ISOPEN

 THEN

         CLOSE c_lnd_004 ;

 END IF ;

 p_codeland	   := NULL ;

 p_naam		   := NULL ;

 RAISE ;
END PRC_GET_I108;
/* Start lijst voor I-108 (Ophalen landen) */
PROCEDURE PRC_INIT_I108
 IS
/* rle_pck_lijsten.prc_init_i108 */
BEGIN
IF c_lnd_004%ISOPEN
   THEN
      CLOSE c_lnd_004 ;
   END IF ;

   OPEN c_lnd_004 ;
END PRC_INIT_I108;
/* Ophalen rij voor I_101 (Ophalen rollen van relatie) */
PROCEDURE PRC_GET_I101
 (P_CODROL OUT RLE_ROLLEN.CODROL%TYPE
 ,P_OMSROL OUT RLE_ROLLEN.OMSROL%TYPE
 )
 IS

R_ROL_004 C_ROL_004%ROWTYPE;

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_i101 */
BEGIN
p_codrol	:= NULL ;
   p_omsrol	:= NULL ;

   IF c_rol_004%ISOPEN
   THEN
          FETCH c_rol_004 INTO r_rol_004;
          IF c_rol_004%FOUND
          THEN
            	p_codrol	:= r_rol_004.codrol ;
             	p_omsrol	:= r_rol_004.omsrol ;
          ELSE
             	/* Geen nieuwe rij gevonden */
             	CLOSE c_rol_004 ;
          END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_rol_004%ISOPEN

 THEN

         CLOSE c_rol_004 ;

 END IF ;

 p_codrol	   := NULL ;

 p_omsrol   := NULL ;

 RAISE ;
END PRC_GET_I101;
/* Start lijst voor I_101 (Ophalen rollen van relatie) */
PROCEDURE PRC_INIT_I101
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 )
 IS
/* rle_pck_lijsten.prc_init_i101 */
BEGIN
IF c_rol_004%ISOPEN
   THEN
      CLOSE c_rol_004 ;
   END IF ;

   OPEN c_rol_004( p_numrelatie) ;
END PRC_INIT_I101;
/* Start lijst voor RLE0915 (Ophalen externe relatie) */
PROCEDURE PRC_INIT_EXT_RELATIE
 (P_NUMRELATIE IN RLE_KANAALVOORKEUREN.RLE_NUMRELATIE%TYPE
 ,P_COD_ROL_IN_ADMINISTRATIE IN RLE_ROLLEN.CODROL%TYPE
 ,P_COD_EXTERN_SYSTEEM IN RLE_RELATIE_EXTERNE_CODES.DWE_WRDDOM_EXTSYS%TYPE
 ,P_COD_SOORT_ADRES IN RLE_RELATIE_ADRESSEN.DWE_WRDDOM_SRTADR%TYPE
 ,P_PEILDATUM IN DATE
 )
 IS
/* rle_pck_lijsten.prc_init_ext_relatie */
BEGIN
IF c_rle_024%ISOPEN
   THEN
      CLOSE c_rle_024 ;
   END IF ;

   lg_code_extsys       := p_cod_extern_systeem       ;
   lg_code_srtadr       := p_cod_soort_adres          ;
   lg_code_rol_in_admin := p_cod_rol_in_administratie ;
   lg_peildatum         := p_peildatum                ;

   OPEN c_rle_024( p_numrelatie
                 , p_cod_rol_in_administratie
                 , p_peildatum
                 ) ;
END PRC_INIT_EXT_RELATIE;
/* Ophalen rij voor I_050 (Ophalen contactpersonen) */
PROCEDURE PRC_GET_EXT_RELATIE
 (P_EXTERN_NUMMER OUT VARCHAR2
 ,P_OPGEMAAKTE_NAAM OUT VARCHAR2
 ,P_DAT_BEGIN OUT DATE
 ,P_DAT_EINDE OUT DATE
 ,P_CODE_SRTADR OUT VARCHAR2
 ,P_ADRES_REGEL_1 OUT VARCHAR2
 ,P_ADRES_REGEL_2 OUT VARCHAR2
 ,P_ADRES_REGEL_3 OUT VARCHAR2
 ,P_ADRES_REGEL_4 OUT VARCHAR2
 ,P_ADRES_REGEL_5 OUT VARCHAR2
 ,P_ADRES_REGEL_6 OUT VARCHAR2
 )
 IS

L_NUMRELATIE NUMBER;
L_MELDING VARCHAR2(100);
L_LENGTE_IBM NUMBER := 40;
L_IND_IBM VARCHAR2(1) := 'N';
R_RLE_024 C_RLE_024%ROWTYPE;

OTHERS EXCEPTION;
APPL_CODE           VARCHAR2(3):= 'RLE';
t_syspar_prod       varchar2(10);
t_syspar_opdr       number(10);
/* rle_pck_lijsten.prc_get_ext_relatie */
BEGIN
p_extern_nummer   := NULL ;
   p_dat_begin        := NULL ;
   p_dat_einde        := NULL ;
   p_opgemaakte_naam := NULL ;
   p_adres_regel_1   := NULL ;
   p_adres_regel_2   := NULL ;
   p_adres_regel_3   := NULL ;
   p_adres_regel_4   := NULL ;
   p_adres_regel_5   := NULL ;
   p_adres_regel_6   := NULL ;
   /*t.b.v. RRGG 2 extra parameters toegevoegd bij het aanroep van rle_m10_adres
   18-06-2003 SKL*/
   t_syspar_prod := stm_algm_get.sysparm(APPL_CODE
        	           , 'PRODUKT'
                       , sysdate);
   t_syspar_opdr := to_number(stm_algm_get.sysparm(APPL_CODE
        	           , 'OPDR_GEVER'
                       , sysdate));

   IF c_rle_024%ISOPEN
   THEN
      FETCH c_rle_024 INTO r_rle_024 ;

      IF c_rle_024%FOUND
      THEN
         p_dat_begin := r_rle_024.datbegin ;
         p_dat_einde := r_rle_024.dateinde ;

         /* Ophalen opgemaakte naam */
         rle_m11_rle_naw( 255
                        , r_rle_024.numrelatie
                        , p_opgemaakte_naam
                        , l_melding
                        , '1'
                        ) ;

         /* Ophalen opgemaakt adres */
         rle_m10_adres( t_syspar_opdr
                      , t_syspar_prod
                      , r_rle_024.numrelatie
                      , lg_code_rol_in_admin
                      , lg_code_srtadr
                      , lg_peildatum
                      , 'J'
                      , 6
                      , 100
                      , NULL
                      , l_melding
                      , p_adres_regel_1
                      , p_adres_regel_2
                      , p_adres_regel_3
                      , p_adres_regel_4
                      , p_adres_regel_5
                      , p_adres_regel_6
                      ) ;

         p_code_srtadr := lg_code_srtadr ;

         /* Ophalen extern nummer */
         p_extern_nummer := rle_m03_rec_extcod( lg_code_extsys
                                              , r_rle_024.numrelatie
                                              , lg_code_rol_in_admin
                                              , lg_peildatum
                                              ) ;

      ELSE
         /* Geen nieuwe rij gevonden */
         CLOSE c_rle_024 ;
      END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_rle_024%ISOPEN

   THEN

      CLOSE c_rle_024 ;

   END IF ;



   p_extern_nummer   := NULL ;

   p_dat_begin        := NULL ;

   p_dat_einde        := NULL ;

   p_opgemaakte_naam := NULL ;

   p_adres_regel_1   := NULL ;

   p_adres_regel_2   := NULL ;

   p_adres_regel_3   := NULL ;

   p_adres_regel_4   := NULL ;

   p_adres_regel_5   := NULL ;

   p_adres_regel_6   := NULL ;



   RAISE ;
END PRC_GET_EXT_RELATIE;
/* Start lijst voor I_050 (Ophalen contactpersonen) */
PROCEDURE PRC_INIT_I050
 (P_NUMRELATIE IN RLE_KANAALVOORKEUREN.RLE_NUMRELATIE%TYPE
 ,P_RAS_ID IN RLE_RELATIE_ADRESSEN.ID%TYPE
 ,P_POSTCODE IN RLE_ADRESSEN.POSTCODE%TYPE
 ,P_HUISNUMMER IN RLE_ADRESSEN.HUISNUMMER%TYPE
 ,P_TOEVOEGING IN VARCHAR2
 ,P_SOORT_ADRES IN VARCHAR2
 ,P_SOORT_CONTACTPERSOON IN VARCHAR2
 )
 IS
/* rle_pck_lijsten.prc_init_i050 */
BEGIN
IF c_rle_023%ISOPEN
   THEN
      CLOSE c_rle_023 ;
   END IF ;

   OPEN c_rle_023 (p_numrelatie
                 , p_ras_id
                 , p_postcode
                 , p_huisnummer
                 , p_toevoeging
                 , p_soort_adres
                 , p_soort_contactpersoon );
END PRC_INIT_I050;
/* Ophalen rij voor I_050 (Ophalen contactpersonen) */
PROCEDURE PRC_GET_I050
 (P_NUMRELATIE OUT RLE_RELATIES.NUMRELATIE%TYPE
 ,P_OMS_FUNCTIE OUT VARCHAR2
 ,P_RLE_OPGEMAAKTE_NAAM OUT VARCHAR2
 ,P_TELEFOONNUMMER OUT VARCHAR2
 ,P_FAXNUMMER OUT VARCHAR2
 ,P_EMAILADRES OUT VARCHAR2
 ,P_COD_FUNCTIE OUT VARCHAR2
 ,P_COD_GESLACHT OUT VARCHAR2
 ,P_OMS_GESLACHT OUT VARCHAR2
 ,P_MOBIELNUMMER OUT VARCHAR2
 ,P_SOORT_CONTACTPERSOON OUT VARCHAR2
 )
 IS

L_DUMMY VARCHAR2(100);
L_MELDING VARCHAR2(100);
L_LENGTE_IBM NUMBER := 40;
L_IND_IBM VARCHAR2(1) := 'N';
R_RLE_023 C_RLE_023%ROWTYPE;
/* rle_pck_lijsten.prc_get_i050 */
BEGIN
p_numrelatie           := NULL ;
   p_oms_functie          := NULL ;
   p_rle_opgemaakte_naam  := NULL ;
   p_telefoonnummer       := NULL ;
   p_faxnummer            := NULL ;
   p_emailadres           := NULL ;
   p_cod_functie          := NULL ;
   p_cod_geslacht         := NULL ;
   p_oms_geslacht         := NULL ;
   p_mobielnummer         := NULL ;
   p_soort_contactpersoon := NULL ;

   IF c_rle_023%ISOPEN
   THEN
      FETCH c_rle_023 INTO r_rle_023;

      IF c_rle_023%FOUND
      THEN
         p_numrelatie           := r_rle_023.rle_numrelatie      ;
         p_cod_functie          := r_rle_023.code_functie        ;
         p_cod_geslacht         := r_rle_023.dwe_wrddom_geslacht ;
         p_soort_contactpersoon := r_rle_023.code_soort_contactpersoon;

         /* omschrijving functie ophalen */
         IBM.IBM( 'RFW_GET_WRD'
                , 'RLE'
                , 'FTE'
                , p_cod_functie
                , l_ind_ibm
                , l_lengte_ibm
                , p_oms_functie
                ) ;

         /* omschrijving geslacht ophalen */
         IBM.IBM( 'RFW_GET_WRD'
                , 'RLE'
                , 'GES'
                , p_cod_geslacht
                , l_ind_ibm
                , l_lengte_ibm
                , p_oms_geslacht
                ) ;

         /* opgemaakte naam ophalen */
         rle_m11_rle_naw( 255
                        , r_rle_023.rle_numrelatie
                        , p_rle_opgemaakte_naam
                        , l_melding
                        , '1'
                        ) ;

         /* telefoonnummer ophalen */
         rle_m30_comnum( r_rle_023.rle_numrelatie
                       , 'CP'     -- OSP 14-10-2003
                       , 'TEL'
                       , SYSDATE
                       , p_telefoonnummer
                       , l_dummy
                       , l_dummy
                       ) ;

         /* faxnummer ophalen */
         rle_m30_comnum( r_rle_023.rle_numrelatie
                       , 'CP'     -- OSP 14-10-2003
                       , 'FAX'
                       , SYSDATE
                       , p_faxnummer
                       , l_dummy
                       , l_dummy
                       ) ;

         /* emailadres ophalen */
         rle_m30_comnum( r_rle_023.rle_numrelatie
                       , 'CP'     -- OSP 14-10-2003
                       , 'EMAIL'
                       , SYSDATE
                       , p_emailadres
                       , l_dummy
                       , l_dummy
                       ) ;

         /* mobielnummer ophalen */
         rle_m30_comnum( r_rle_023.rle_numrelatie
                       , 'CP'     -- OSP 14-10-2003
                       , 'MOBIEL'
                       , SYSDATE
                       , p_mobielnummer  -- OSP 06-10-2003
                       , l_dummy
                       , l_dummy
                       ) ;
      ELSE
         /* Geen nieuwe rij gevonden */
         CLOSE c_rle_023 ;
      END IF ;
   END IF ;

EXCEPTION
   WHEN OTHERS THEN
      IF c_rle_023%ISOPEN
      THEN
         CLOSE c_rle_023 ;
      END IF ;

      p_numrelatie           := NULL ;
      p_oms_functie          := NULL ;
      p_rle_opgemaakte_naam  := NULL ;
      p_telefoonnummer       := NULL ;
      p_faxnummer            := NULL ;
      p_emailadres           := NULL ;
      p_cod_functie          := NULL ;
      p_cod_geslacht         := NULL ;
      p_oms_geslacht         := NULL ;
      p_mobielnummer         := NULL ;
      p_soort_contactpersoon := NULL ;

      RAISE ;
END PRC_GET_I050;
/* Start lijst voor I_051 (Ophalen verwantschappen) */
PROCEDURE PRC_INIT_I051
 (P_NUMRELATIE IN RLE_KANAALVOORKEUREN.RLE_NUMRELATIE%TYPE
 )
 IS
/* rle_pck_lijsten.prc_init_i054 */
BEGIN
IF c_rkn_002%ISOPEN
   THEN
      CLOSE c_rkn_002 ;
   END IF ;

   OPEN c_rkn_002( p_numrelatie ) ;
END PRC_INIT_I051;
/* Ophalen rij voor I_054 (Ophalen relatiekoppelingen) */
PROCEDURE PRC_GET_I051
 (P_NUMRELATIE OUT RLE_RELATIES.NUMRELATIE%TYPE
 ,P_COD_RELATIE_KOPPELING OUT VARCHAR2
 ,P_OMS_RELATIE_KOPPELING OUT VARCHAR2
 ,P_RKN_DATBEGIN OUT DATE
 ,P_RKN_DATEINDE OUT DATE
 ,P_RLE_DATGEBOORTE OUT DATE
 ,P_RLE_DATOVERLIJDEN OUT DATE
 ,P_RLE_OPGEMAAKTE_NAAM OUT VARCHAR2
 ,P_REC_PERSOONSNUMMER OUT VARCHAR2
 ,P_RLE_GESLACHT OUT VARCHAR2
 ,P_OMS_GESLACHT OUT VARCHAR2
 )
 IS

L_MELDING VARCHAR2(100);
L_LENGTE_IBM NUMBER := 40;
L_IND_IBM VARCHAR2(1) := 'N';
R_RKN_002 C_RKN_002%ROWTYPE;
L_OMSCHRIJVING VARCHAR2(30);
L_IND VARCHAR2(2) := 'N';
L_LENGTE NUMBER(4) := 40;

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_i051 */
BEGIN
p_numrelatie            := NULL ;
   p_cod_relatie_koppeling := NULL ;
   p_oms_relatie_koppeling := NULL ;
   p_rkn_datbegin          := NULL ;
   p_rkn_dateinde          := NULL ;
   p_rle_datgeboorte       := NULL ;
   p_rle_datoverlijden     := NULL ;
   p_rle_opgemaakte_naam   := NULL ;
   p_rec_persoonsnummer	   := NULL ;
   p_rle_geslacht          := NULL ;
   p_oms_geslacht 	   := NULL ;
   IF c_rkn_002%ISOPEN
   THEN
      FETCH c_rkn_002 INTO r_rkn_002 ;

      IF c_rkn_002%FOUND
      THEN
         p_numrelatie            := r_rkn_002.rkn_rle_numrelatie_voor ;
         p_cod_relatie_koppeling := r_rkn_002.rkg_code_rol_in_koppeling ;
         p_oms_relatie_koppeling := r_rkn_002.rkg_omschrijving ;
         p_rkn_datbegin          := r_rkn_002.rkn_dat_begin ;
         p_rkn_dateinde          := r_rkn_002.rkn_dat_einde ;
         p_rle_datgeboorte       := r_rkn_002.rle_dat_geboorte ;
         p_rle_datoverlijden     := r_rkn_002.rle_dat_overlijden ;
         p_rle_geslacht          := r_rkn_002.rle_geslacht;

         rle_m11_rle_naw( 255
                        , r_rkn_002.rkn_rle_numrelatie_voor
                        , p_rle_opgemaakte_naam
                        , l_melding
                        , '1'
                        ) ;
         /* persoonsnummer ophalen */
         p_rec_persoonsnummer := rle_m03_rec_extcod('PRS'
				   				   , r_rkn_002.rkn_rle_numrelatie_voor
				   				   , 'PR'
								   , SYSDATE
                           			         );
         /* omschrijving geslacht ophalen */
         IBM.IBM('RFW_GET_WRD'
	          ,'RLE'
                ,'GES'
                ,r_rkn_002.rle_geslacht
                ,l_ind
                ,l_lengte
                ,l_omschrijving)
                ;
         p_oms_geslacht := l_omschrijving;
      ELSE
         /* Geen nieuwe rij gevonden */
         CLOSE c_rkn_002 ;
      END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_rkn_001%ISOPEN

   THEN

      CLOSE c_rkn_001 ;

   END IF ;



   p_numrelatie            := NULL ;

   p_cod_relatie_koppeling := NULL ;

   p_oms_relatie_koppeling := NULL ;

   p_rkn_datbegin          := NULL ;

   p_rkn_dateinde          := NULL ;

   p_rle_datgeboorte       := NULL ;

   p_rle_datoverlijden     := NULL ;

   p_rle_opgemaakte_naam   := NULL ;

   p_rec_persoonsnummer := NULL;

   p_rle_geslacht := NULL;

   p_oms_geslacht := NULL;

   RAISE ;
END PRC_GET_I051;
/* Start lijst voor I_054 (Ophalen relatiekoppelingen) */
PROCEDURE PRC_INIT_I054
 (P_NUMRELATIE IN RLE_KANAALVOORKEUREN.RLE_NUMRELATIE%TYPE
 )
 IS
/* rle_pck_lijsten.prc_init_i054 */
BEGIN
IF c_rkn_001%ISOPEN
   THEN
      CLOSE c_rkn_001 ;
   END IF ;

   OPEN c_rkn_001( p_numrelatie ) ;
END PRC_INIT_I054;
/* Ophalen rij voor I_054 (Ophalen relatiekoppelingen) */
PROCEDURE PRC_GET_I054
 (P_NUMRELATIE OUT RLE_RELATIES.NUMRELATIE%TYPE
 ,P_COD_RELATIE_KOPPELING OUT VARCHAR2
 ,P_OMS_RELATIE_KOPPELING OUT VARCHAR2
 ,P_RKN_DATBEGIN OUT DATE
 ,P_RKN_DATEINDE OUT DATE
 ,P_RLE_DATBEGIN OUT DATE
 ,P_RLE_DATEINDE OUT DATE
 ,P_RLE_OPGEMAAKTE_NAAM OUT VARCHAR2
 ,P_MUTATIE_DOOR OUT VARCHAR2
 ,P_DAT_MUTATIE OUT DATE
 )
 IS

L_MELDING VARCHAR2(100);
L_LENGTE_IBM NUMBER := 40;
L_IND_IBM VARCHAR2(1) := 'N';
R_RKN_001 C_RKN_001%ROWTYPE;

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_i054 */
BEGIN
p_numrelatie            := NULL ;
   p_cod_relatie_koppeling := NULL ;
   p_oms_relatie_koppeling := NULL ;
   p_rkn_datbegin          := NULL ;
   p_rkn_dateinde          := NULL ;
   p_rle_datbegin          := NULL ;
   p_rle_dateinde          := NULL ;
   p_rle_opgemaakte_naam   := NULL ;
   p_mutatie_door      := null;
   p_dat_mutatie         := null;

   IF c_rkn_001%ISOPEN
   THEN
      FETCH c_rkn_001 INTO r_rkn_001;

      IF c_rkn_001%FOUND
      THEN
         p_numrelatie            := r_rkn_001.rkn_rle_numrelatie_voor ;
         p_cod_relatie_koppeling := r_rkn_001.rkg_code_rol_in_koppeling ;
         p_oms_relatie_koppeling := r_rkn_001.rkg_omschrijving ;
         p_rkn_datbegin          := r_rkn_001.rkn_dat_begin ;
         p_rkn_dateinde          := r_rkn_001.rkn_dat_einde ;
         p_rle_datbegin          := r_rkn_001.rle_dat_begin ;
         p_rle_dateinde          := r_rkn_001.rle_dat_einde ;
         p_mutatie_door         := r_rkn_001.mutatie_door;
         p_dat_mutatie            := r_rkn_001.dat_mutatie;

         rle_m11_rle_naw( 255
                        , r_rkn_001.rkn_rle_numrelatie_voor
                        , p_rle_opgemaakte_naam
                        , l_melding
                        , '1'
                        ) ;
      ELSE
         /* Geen nieuwe rij gevonden */
         CLOSE c_rkn_001 ;
      END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_rkn_001%ISOPEN

   THEN

      CLOSE c_rkn_001 ;

   END IF ;



   p_numrelatie            := NULL ;

   p_cod_relatie_koppeling := NULL ;

   p_oms_relatie_koppeling := NULL ;

   p_rkn_datbegin          := NULL ;

   p_rkn_dateinde          := NULL ;

   p_rle_datbegin          := NULL ;

   p_rle_dateinde          := NULL ;

   p_rle_opgemaakte_naam   := NULL ;



   RAISE ;
END PRC_GET_I054;
/* Start lijst voor I_096 (Ophalen relatiekoppelingen) */
PROCEDURE PRC_INIT_I096
 (P_NUMRELATIE IN RLE_KANAALVOORKEUREN.RLE_NUMRELATIE%TYPE
 )
 IS
/* rle_pck_lijsten.prc_init_i096 */
BEGIN
IF c_rkn_007%ISOPEN
   THEN
      CLOSE c_rkn_007 ;
   END IF ;

   OPEN c_rkn_007( p_numrelatie ) ;
END PRC_INIT_I096;
/* Ophalen rij voor I_096 (Ophalen relatiekoppelingen) */
PROCEDURE PRC_GET_I096
 (P_NUMRELATIE OUT RLE_RELATIES.NUMRELATIE%TYPE
 ,P_COD_RELATIE_KOPPELING OUT VARCHAR2
 ,P_OMS_RELATIE_KOPPELING OUT VARCHAR2
 ,P_RKN_DATBEGIN OUT DATE
 ,P_RKN_DATEINDE OUT DATE
 ,P_RLE_DATBEGIN OUT DATE
 ,P_RLE_DATEINDE OUT DATE
 ,P_RLE_OPGEMAAKTE_NAAM OUT VARCHAR2
 ,P_MUTATIE_DOOR OUT VARCHAR2
 ,P_DAT_MUTATIE OUT DATE
 )
 IS

L_MELDING VARCHAR2(100);
L_LENGTE_IBM NUMBER := 40;
L_IND_IBM VARCHAR2(1) := 'N';
R_RKN_007 C_RKN_007%ROWTYPE;

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_i096 */
BEGIN
p_numrelatie            := NULL ;
   p_cod_relatie_koppeling := NULL ;
   p_oms_relatie_koppeling := NULL ;
   p_rkn_datbegin          := NULL ;
   p_rkn_dateinde          := NULL ;
   p_rle_datbegin          := NULL ;
   p_rle_dateinde          := NULL ;
   p_rle_opgemaakte_naam   := NULL ;
   p_mutatie_door       := NULL;
   p_dat_mutatie         := NULL;

   IF c_rkn_007%ISOPEN
   THEN
      FETCH c_rkn_007 INTO r_rkn_007;

      IF c_rkn_007%FOUND
      THEN
         p_numrelatie            := r_rkn_007.rkn_rle_numrelatie_voor ;
         p_cod_relatie_koppeling := r_rkn_007.rkg_code_rol_in_koppeling ;
         p_oms_relatie_koppeling := r_rkn_007.rkg_omschrijving ;
         p_rkn_datbegin          := r_rkn_007.rkn_dat_begin ;
         p_rkn_dateinde          := r_rkn_007.rkn_dat_einde ;
         p_rle_datbegin          := r_rkn_007.rle_dat_begin ;
         p_rle_dateinde          := r_rkn_007.rle_dat_einde ;
         p_mutatie_door       := r_rkn_007.mutatie_door ;
         p_dat_mutatie         := r_rkn_007.dat_mutatie ;

         rle_m11_rle_naw( 255
                        , r_rkn_007.rkn_rle_numrelatie_voor
                        , p_rle_opgemaakte_naam
                        , l_melding
                        , '1'
                        ) ;
      ELSE
         /* Geen nieuwe rij gevonden */
         CLOSE c_rkn_007 ;
      END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_rkn_001%ISOPEN

   THEN

      CLOSE c_rkn_001 ;

   END IF ;



   p_numrelatie            := NULL ;

   p_cod_relatie_koppeling := NULL ;

   p_oms_relatie_koppeling := NULL ;

   p_rkn_datbegin          := NULL ;

   p_rkn_dateinde          := NULL ;

   p_rle_datbegin          := NULL ;

   p_rle_dateinde          := NULL ;

   p_rle_opgemaakte_naam   := NULL ;



   RAISE ;
END PRC_GET_I096;
/* Start lijst voor I_093 (Ophalen werkgevers bij een admin. kantoor) */
PROCEDURE PRC_INIT_I093
 (P_NUMRELATIE IN RLE_RELATIE_KOPPELINGEN.RLE_NUMRELATIE%TYPE
 ,P_PEILDATUM IN DATE
 )
 IS
/* rle_pck_lijsten.prc_init_i093 */
BEGIN
IF c_rkn_010%ISOPEN
   THEN
      CLOSE c_rkn_010 ;
   END IF ;

   OPEN c_rkn_010( p_numrelatie
                 , p_peildatum
                 ) ;

   lgn_i093_numrelatie_akr := p_numrelatie ;
   lgd_i093_peildatum      := p_peildatum  ;


EXCEPTION
   WHEN OTHERS THEN
      IF c_rkn_010%ISOPEN
      THEN
         CLOSE c_rkn_010 ;
      END IF ;

      RAISE ;
END PRC_INIT_I093;
/* Ophalen rij voor I_093 (Ophalen werkgevers bij een admin. kantoor) */
PROCEDURE PRC_GET_I093
 (P_NUMRELATIE OUT RLE_RELATIES.NUMRELATIE%TYPE
 ,P_WERKGEVERSNUMMER OUT VARCHAR2
 ,P_RLE_OPGEMAAKTE_NAAM OUT VARCHAR2
 ,P_RKN_DATBEGIN OUT DATE
 ,P_RKN_DATEINDE OUT DATE
 ,P_NAAM_CONTACTPERSOON OUT VARCHAR2
 ,P_TELEFOONNUMMER OUT VARCHAR2
 )
 IS

CURSOR C_RKN_011
 (CPN_NUMRELATIE_WGR IN number
 ,CPN_NUMRELATIE_AKR IN NUMBER
 ,CPD_PEILDATUM IN date
 )
 IS
SELECT rkn_we.rle_numrelatie_voor  cpn_numrelatie
,      rle.namrelatie              cpn_naam
FROM   rle_relaties             rle
,      rle_relatie_koppelingen  rkn_we
,      rle_rol_in_koppelingen   rkg_we
,      rle_relatie_koppelingen  rkn_kc
,      rle_rol_in_koppelingen   rkg_kc
WHERE  rle.numrelatie               = rkn_we.rle_numrelatie_voor
AND    rkn_we.rle_numrelatie        = cpn_numrelatie_wgr
AND    rkn_we.rkg_id                = rkg_we.id
AND    rkg_we.code_rol_in_koppeling = 'WE'
AND    rkn_we.rle_numrelatie_voor   = rkn_kc.rle_numrelatie_voor
AND    rkn_kc.rle_numrelatie        = cpn_numrelatie_akr
AND    rkn_kc.rkg_id                = rkg_kc.id
AND    rkg_kc.code_rol_in_koppeling = 'KC'
;

R_RKN_011 C_RKN_011%ROWTYPE;
R_RKN_010 C_RKN_010%ROWTYPE;
L_MELDING VARCHAR2(100);
L_SRTCOM VARCHAR2(30);
/* rle_pck_lijsten.prc_get_i093 */
BEGIN
p_numrelatie            := NULL ;
   p_werkgeversnummer      := NULL ;
   p_rle_opgemaakte_naam   := NULL ;
   p_rkn_datbegin          := NULL ;
   p_rkn_dateinde          := NULL ;
   p_naam_contactpersoon   := NULL ;
   p_telefoonnummer        := NULL ;

   IF c_rkn_010%ISOPEN
   THEN
      FETCH c_rkn_010 INTO r_rkn_010;

      IF c_rkn_010%FOUND
      THEN
         p_numrelatie            := r_rkn_010.rkn_rle_numrelatie_voor ;
         p_werkgeversnummer      := r_rkn_010.rec_werkgeversnummer ;
         p_rkn_datbegin          := r_rkn_010.rkn_dat_begin ;
         p_rkn_dateinde          := r_rkn_010.rkn_dat_einde ;

         -- ophalen opgemaakte naam werkgever
         rle_m11_rle_naw( 255
                        , r_rkn_010.rkn_rle_numrelatie_voor
                        , p_rle_opgemaakte_naam
                        , l_melding
                        , '1'
                        ) ;

         OPEN  c_rkn_011( p_numrelatie
                        , lgn_i093_numrelatie_akr
                        , lgd_i093_peildatum
                        ) ;
         FETCH c_rkn_011 INTO r_rkn_011;
         CLOSE c_rkn_011;

         p_naam_contactpersoon   := r_rkn_011.cpn_naam ;

         -- ophalen telefoonnummer contactpersoon (indien aanwezig)
         IF r_rkn_011.cpn_numrelatie IS NOT NULL
         THEN
           rle_m30_comnum( r_rkn_011.cpn_numrelatie
                         , 'CP'
                         , 'TEL'
                         , lgd_i093_peildatum
                         , p_telefoonnummer
                         , l_srtcom
                         , l_melding
                         ) ;
         END IF;
      ELSE
         /* Geen nieuwe rij gevonden */
         CLOSE c_rkn_010 ;
      END IF ;
   END IF ;

EXCEPTION
   WHEN OTHERS THEN
      IF c_rkn_010%ISOPEN
      THEN
        CLOSE c_rkn_010 ;
      END IF ;

      p_numrelatie            := NULL ;
      p_werkgeversnummer      := NULL ;
      p_rle_opgemaakte_naam   := NULL ;
      p_rkn_datbegin          := NULL ;
      p_rkn_dateinde          := NULL ;
      p_naam_contactpersoon   := NULL ;
      p_telefoonnummer        := NULL ;

      RAISE ;
END PRC_GET_I093;
/* Start lijst voor 0985 (Ophalen administraties van relatie) */
PROCEDURE PRC_INIT_0985
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_ROL_CODROL IN RLE_RELATIEROLLEN_IN_ADM.ROL_CODROL%TYPE
 )
 IS
/* rle_pck_lijsten.prc_init_0985 */
BEGIN
IF c_rie_001%ISOPEN
   THEN
      CLOSE c_rie_001 ;
   END IF ;

   OPEN c_rie_001( b_numrelatie => p_numrelatie, b_rol_codrol => p_rol_codrol ) ;
END PRC_INIT_0985;
/* Ophalen rij voor RLE0985 (Ophalen administratie). */
PROCEDURE PRC_GET_0985
 (P_ADM_CODE OUT RLE_RELATIEROLLEN_IN_ADM.ADM_CODE%TYPE
 )
 IS

R_RIE_001 C_RIE_001%ROWTYPE;

OTHERS EXCEPTION;
/* rle_pck_lijsten.prc_get_0985 */
BEGIN
p_adm_code	:= NULL ;

   IF c_rie_001%ISOPEN
   THEN
          FETCH c_rie_001 INTO r_rie_001;
          IF c_rie_001%FOUND
          THEN
            	p_adm_code	:= r_rie_001.adm_code ;
          ELSE
             	/* Geen nieuwe rij gevonden */
             	CLOSE c_rie_001 ;
          END IF ;
   END IF ;

EXCEPTION
 WHEN OTHERS THEN
   IF c_rie_001%ISOPEN

 THEN

         CLOSE c_rie_001 ;

 END IF ;

 p_adm_code   := NULL ;

 RAISE ;
END PRC_GET_0985;

END RLE_PCK_LIJSTEN;
/