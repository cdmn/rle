CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_WGR IS
  --
  -- Deze package levert de gegevens voor de kolommen van de rle_v_werkgevers_xl view.
  -- De gewenste gegevens moeten veelal uit relatief complexe procedures worden betrokken.
  -- De benodigde procedures worden in deze package uitgevoerd, waarna de geretourneerde gegevens
  -- individueel via functies worden opgeleverd aan de view.
  --
  -- Aangezien de aangeroepen procedures meestal gegevens leveren voor meerdere kolommen in de view
  -- is het performance-technisch 'zonde' om zo'n procedure voor elk van de kolommen door de
  -- bijbehorende functie aan te roepen.
  --
  -- In deze package wordt daarom het resultaat van elke aanroep van een gegevens opleverende procedure
  -- in een collection geplaatst bij het indexnummer gelijk aan het identificerende nummer (relatienummer
  -- of werkgevernummer).
  -- Bij de gegevens in de collection wordt ook middels een tijdstempel vastgelegd wanneer deze zijn opgehaald.
  -- Die collection fungeert daarmee als een cache.
  --
  -- Bij een volgende aanroep van dezelfde procedure zal eerst gecontroleerd worden of de gegevens voor
  -- de betreffende werkgever in deze sessie al eerder opgehaald zijn.
  -- Zo ja, dan wordt gecontroleerd hoe lang geleden dat is gebeurt.
  -- Is dat minder lang geleden dan het aantal seconden (nauwkeurigheid tot 0.01 sec) dat vastgelegd is
  -- in de constante cn_cachetijd dan worden de eerder opgehaalde gegevens hergebruikt.
  --
  -- Bij initiele creatie van deze package is de cachetijd gezet op 1 seconde.
  -- Dit geeft de view ruim de tijd om binnen 1 werkgever (= 1 record in de view) gegevens te hergebruiken,
  -- en minimaliseert het risico van het gebruik van 'oude' werkgever-gegevens.

  ------------------------------------------------------------------------------- SUBTYPES
  subtype maxchar   is varchar2(32767);

  ------------------------------------------------------------------------------- CONSTANTEN
  cn_cachetijd      constant  number(9,2) := 1;   -- tijd in sec die gecachte gegevens geldig blijven

  cn_ads_statutair  constant  rle_relatie_adressen.dwe_wrddom_srtadr%type := 'SZ';
  cn_ads_feitelijk  constant  rle_relatie_adressen.dwe_wrddom_srtadr%type := 'FA';
  cn_ads_inspectie  constant  rle_relatie_adressen.dwe_wrddom_srtadr%type := 'IA';
  cn_ads_bijz_nota  constant  rle_relatie_adressen.dwe_wrddom_srtadr%type := 'BA';
  cn_ads_correspon  constant  rle_relatie_adressen.dwe_wrddom_srtadr%type := 'CA';

  ------------------------------------------------------------------------------- RECORD TYPES
  -- voor FINANCIELE NUMMERS
  type rt_fin_nummer is record (timestamp       integer
                               ,numrekening_ai  maxchar
                               ,numrekening_ag  maxchar
                               ,bic_ai             maxchar
                               ,iban_ai           maxchar
                               ,bet_wijze_ai   maxchar
                               ,bic_ag            maxchar
                              ,iban_ag           maxchar
                               ,bet_wijze_ag  maxchar
                               );

  -- voor ADRESSEN
  type rt_adres      is record (timestamp             integer
                               ,postcode              maxchar
                               ,huisnr                number
                               ,toevnum               maxchar
                               ,woonplaats            maxchar
                               ,straat                maxchar
                               ,codland               maxchar
                               ,locatie               maxchar
                               ,ind_woonboot          maxchar
                               ,ind_woonwagen         maxchar
                               ,landnaam              maxchar
                               ,contactpers           number
                               ,opmaaknaam            maxchar
                               );

  -- voor LOONADMINISTRATIE KANTOOR
  type rt_la_kantoor is record (timestamp                integer
                               ,la_relatienr             number
                               ,la_naam                  maxchar
                               ,la_straat                maxchar
                               ,la_huisnr                number
                               ,la_toevnum               maxchar
                               ,la_postcode              maxchar
                               ,la_woonplaats            maxchar
                               ,la_land                  maxchar
                               ,la_telnr_contactpersoon  maxchar
                               ,la_naam_contactpersoon   maxchar
                               );

  -- voor KANAALVOORKEUR
  -- feitelijk maar 1 gegeven, dus cachen is hier niet strikt noodzakelijk, maar wordt toch
  -- analoog aan andere gegevens-groepen gedaan, zodat als in later stadium nog
  -- meer gegevens met betrekking tot kanaalvoorkeur gewenst zijn, deze eenvooudig toe
  -- te voegen zijn, en dan niet het cachen alsnog ingevoerd moet worden
  type rt_kanaal     is record (timestamp        integer
                               ,kanaalvoorkeur   maxchar
                               );

  -- voor COMMUNICATIENUMMERS
  type rt_comm_nr    is record (timestamp    integer
                               ,tel_nummer   maxchar
                               ,fax_nummer   maxchar
                               ,email_adres  maxchar
                               ,adm_code     maxchar
                               ,tel_nummer_wgr   maxchar
                               ,email_adres_wgr  maxchar
                               );

  -- voor ACCOUNTMANAGER
  type rt_actmgr     is record (timestamp              integer
                               ,actmgr_voorletters     maxchar
                               ,actmgr_voorvoegsels    maxchar
                               ,actmgr_naam            maxchar
                               ,actmgr_telefoonnummer  maxchar
                               );

  -- voor KLANTTEAM
    type rt_klntteam is record (timestamp              integer
                               ,klntteam_naam          maxchar
                               ,klntteam_tel           maxchar
                               );

  ------------------------------------------------------------------------------- COLLECTION TYPES
  type ttab_fin_nummer is table of rt_fin_nummer index by pls_integer;
  type ttab_ads        is table of rt_adres      index by pls_integer;
  type ttab_la_kantoor is table of rt_la_kantoor index by pls_integer;
  type ttab_kanaal     is table of rt_kanaal     index by pls_integer;
  type ttab_comm_nr    is table of rt_comm_nr    index by pls_integer;
  type ttab_actmgr     is table of rt_actmgr     index by pls_integer;
  type ttab_klntteam   is table of rt_klntteam   index by pls_integer;

  ------------------------------------------------------------------------------- COLLECTIONS
  tab_fin_nummer    ttab_fin_nummer;
  tab_ads_sz        ttab_ads       ;
  tab_ads_fa        ttab_ads       ;
  tab_ads_ia        ttab_ads       ;
  tab_ads_ba        ttab_ads       ;
  tab_ads_ca        ttab_ads       ;
  tab_la_kantoor    ttab_la_kantoor;
  tab_kanaal        ttab_kanaal    ;
  tab_comm_nr       ttab_comm_nr   ;
  tab_comm_nr_sz    ttab_comm_nr   ;
  tab_comm_nr_fa    ttab_comm_nr   ;
  tab_comm_nr_ia    ttab_comm_nr   ;
  tab_comm_nr_ca    ttab_comm_nr   ;
  tab_comm_nr_ba    ttab_comm_nr   ;
  tab_actmgr        ttab_actmgr    ;
  tab_klntteam      ttab_klntteam   ;

/* Controle of gegevens in cache nog geldig zijn */
FUNCTION CACHE_VERLOPEN
 (P_CACHE_START IN NUMBER
 )
 RETURN BOOLEAN;
/* Haal financiele nummers op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_FIN_NUMMERS
 (P_RLE_NUMRELATIE IN relnum_type
 );
/* Haal adres op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_ADRES
 (P_RLE_NUMRELATIE IN relnum_type
 ,P_SOORT_ADRES IN VARCHAR2
 );
/* Haal loonadmin kantoor op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_LOONADMIN_KANTOOR
 (P_RLE_NUMRELATIE IN relnum_type
 );
/* Haal kanaalvoorkeur op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_KANAALVOORKEUR
 (P_RLE_NUMRELATIE IN relnum_type
 );
/* Haal communicatie nr's op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_COMM_NR
 (P_RLE_NUMRELATIE IN relnum_type
 ,P_SOORT_ADRES IN VARCHAR2 := null
 );
/* Haal accountmanager op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_ACTMGR
 (P_WERKGEVERNR IN wgrnum_type
 );
PROCEDURE GET_KLNTTEAM
 (P_WERKGEVERNR IN wgrnum_type
 );


/* Controle of gegevens in cache nog geldig zijn */
FUNCTION CACHE_VERLOPEN
 (P_CACHE_START IN NUMBER
 )
 RETURN BOOLEAN
 IS
begin
  -- N.B. dbms_utility.get_time geeft tijd in honderste van een sec, vandaar (cachetijd*100)
  return ((dbms_utility.get_time - p_cache_start) > cn_cachetijd * 100);
end;
/* Haal financiele nummers op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_FIN_NUMMERS
 (P_RLE_NUMRELATIE IN relnum_type
 )
 IS

R_FIN_NUMMER RT_FIN_NUMMER;
begin
  if not tab_fin_nummer.exists (p_rle_numrelatie)
  or cache_verlopen (tab_fin_nummer(p_rle_numrelatie).timestamp)
  then
    -- gegevens zijn in deze sessie nog niet eerder opgehaald, of dit is langer geleden gedaan
    -- dan de maximale geldigheid van de cache, dus haal de gegevens (opnieuw) op.
    r_fin_nummer := null;

    rle_get_fnr_ai_sepa
       (p_numrelatie     => p_rle_numrelatie            -- in
       ,p_peildatum      => sysdate                     -- in
       ,p_numrekening_ai => r_fin_nummer.numrekening_ai --    out
       ,p_numrekening_ag => r_fin_nummer.numrekening_ag --    out
       ,p_bic_ai         => r_fin_nummer.bic_ai --    out
       ,p_iban_ai        => r_fin_nummer.iban_ai --    out
       ,p_bet_wijze_ai   => r_fin_nummer.bet_wijze_ai --    out
       ,p_bic_ag         => r_fin_nummer.bic_ag --    out
       ,p_iban_ag        => r_fin_nummer.iban_ag --    out
       ,p_bet_wijze_ag    => r_fin_nummer.bet_wijze_ag --    out
       );
    r_fin_nummer.timestamp := dbms_utility.get_time;

    tab_fin_nummer(p_rle_numrelatie) := r_fin_nummer;
  end if;
end;
/* Haal adres op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_ADRES
 (P_RLE_NUMRELATIE IN relnum_type
 ,P_SOORT_ADRES IN VARCHAR2
 )
 IS

L_OPHALEN BOOLEAN;
R_ADRES RT_ADRES;
L_DUMMY_CHR_1 MAXCHAR;
L_DUMMY_DATE_1 DATE;
L_DUMMY_DATE_2 DATE;
begin
  l_ophalen  := case p_soort_adres
                  when cn_ads_statutair then (   not tab_ads_sz.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_ads_sz(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_feitelijk then (   not tab_ads_fa.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_ads_fa(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_inspectie then (   not tab_ads_ia.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_ads_ia(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_bijz_nota then (   not tab_ads_ba.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_ads_ba(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_correspon then (   not tab_ads_ca.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_ads_ca(p_rle_numrelatie).timestamp)
                                             )
                end;

  if l_ophalen
  then
    r_adres  := null;
    rle_get_adrsvelden
       (p_opdrachtgever => null                  -- in
       ,p_produkt       => null                  -- in
       ,p_numrelatie    => p_rle_numrelatie      -- in
       ,p_codrol        => 'WG'                  -- in
       ,p_srtadres      => p_soort_adres         -- in
       ,p_datump        => sysdate               -- in
       ,p_postcode      => r_adres.postcode      --    out
       ,p_huisnr        => r_adres.huisnr        --    out
       ,p_toevnum       => r_adres.toevnum       --    out
       ,p_woonplaats    => r_adres.woonplaats    --    out
       ,p_straat        => r_adres.straat        --    out
       ,p_codland       => r_adres.codland       --    out
       ,p_locatie       => r_adres.locatie       --    out
       ,p_ind_woonboot  => r_adres.ind_woonboot  --    out
       ,p_ind_woonwagen => r_adres.ind_woonwagen --    out
       ,p_landnaam      => r_adres.landnaam      --    out
       ,p_begindatum    => l_dummy_date_1        --    out
       ,p_einddatum     => l_dummy_date_2        --    out
       );

    rle_get_contactpersoon
       (p_numrelatie             => p_rle_numrelatie           -- in
       ,p_srt_adres              => p_soort_adres              -- in
       ,p_srt_contactpers        => null                       -- in
       ,p_numrelatie_contactpers => r_adres.contactpers        --    out
       );

    if r_adres.contactpers is not null
    then
      rle_m11_rle_naw
         (p_aantpos      => null                 -- in
         ,p_relnum       => r_adres.contactpers  -- in
         ,p_o_opmaaknaam => r_adres.opmaaknaam   -- in out
         ,p_o_melding    => l_dummy_chr_1        -- in out
         ,p_cod_opmaak   => 9                    -- in
         );
    end if;

    r_adres.timestamp := dbms_utility.get_time;

    case p_soort_adres
      when cn_ads_statutair then tab_ads_sz(p_rle_numrelatie) := r_adres;
      when cn_ads_feitelijk then tab_ads_fa(p_rle_numrelatie) := r_adres;
      when cn_ads_inspectie then tab_ads_ia(p_rle_numrelatie) := r_adres;
      when cn_ads_bijz_nota then tab_ads_ba(p_rle_numrelatie) := r_adres;
      when cn_ads_correspon then tab_ads_ca(p_rle_numrelatie) := r_adres;
    end case;
  end if;
end;
/* Haal loonadmin kantoor op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_LOONADMIN_KANTOOR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 IS

L_RKN_FOUND BOOLEAN;
R_LA_KANTOOR RT_LA_KANTOOR;
  --
  -- ophalen extern nummer voor het administratiekantoor van de werkgever
  --
  cursor c_rkn (b_werkgever  rle_v_relatiekoppelingen.numrelatie_voor%type)
  is
  select rec.extern_relatie
  from   (select numrelatie
          ,      dat_begin
          ,      max(dat_begin) over (partition by numrelatie_voor,code_rol_in_koppeling)  meest_recent
          from   rle_v_relatiekoppelingen
          where  numrelatie_voor       = b_werkgever
            and  code_rol_in_koppeling = 'AV'
            and  sysdate                   between dat_begin
                                           and     nvl(dat_einde,sysdate+1)
         )        rkn
  ,      (select extern_relatie
          ,      rle_numrelatie
          ,      datingang
          ,      max(datingang) over (partition by rle_numrelatie,rol_codrol)  meest_recent
          from   rle_relatie_externe_codes
          where  rol_codrol      = 'AK'
            and  sysdate         between datingang
                                 and     nvl(dateinde,sysdate+1)
         )        rec
  where  rkn.numrelatie  = rec.rle_numrelatie
    and  rkn.dat_begin   = rkn.meest_recent
    and  rec.datingang   = rec.meest_recent
  ;

  r_rkn   c_rkn%rowtype;
begin
  if not tab_la_kantoor.exists (p_rle_numrelatie)
  or cache_verlopen (tab_la_kantoor(p_rle_numrelatie).timestamp)
  then
    -- gegevens zijn in deze sessie nog niet eerder opgehaald, of dit is langer geleden gedaan
    -- dan de maximale geldigheid van de cache, dus haal de gegevens (opnieuw) op.
    open  c_rkn (p_rle_numrelatie);
    fetch c_rkn
    into  r_rkn;
    l_rkn_found  := c_rkn%found;
    close c_rkn;

    r_la_kantoor := null;

    if l_rkn_found
    then
      rle_admin_gegevens
         (p_extern_nr        => r_rkn.extern_relatie                 -- in
         ,p_numrelatie       => r_la_kantoor.la_relatienr            --    out
         ,p_namrelatie       => r_la_kantoor.la_naam                 --    out
         ,p_straat           => r_la_kantoor.la_straat               --    out
         ,p_huisnr           => r_la_kantoor.la_huisnr               --    out
         ,p_toevnum          => r_la_kantoor.la_toevnum              --    out
         ,p_postcode         => r_la_kantoor.la_postcode             --    out
         ,p_woonplaats       => r_la_kantoor.la_woonplaats           --    out
         ,p_land             => r_la_kantoor.la_land                 --    out
         ,p_numcommunicatie  => r_la_kantoor.la_telnr_contactpersoon --    out
         ,p_naam_contactpers => r_la_kantoor.la_naam_contactpersoon  --    out
         );
    end if;

    r_la_kantoor.timestamp := dbms_utility.get_time;

    tab_la_kantoor (p_rle_numrelatie) := r_la_kantoor;
  end if;
end;
/* Haal kanaalvoorkeur op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_KANAALVOORKEUR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 IS

R_KANAAL RT_KANAAL;
L_KVR_FOUND BOOLEAN;
/*
Wijzigingshistorie
==============
24-11-2011   XCW            Alleen categorie algemeen ophalen
01-12-2011   XCW            Soort kanaal voorkeur voortaan via communicatie kanalen
*/
 --
  cursor c_kvr (b_numrelatie in rle_kanaalvoorkeuren.rle_numrelatie%type
               )
  is
  select cck.dwe_wrddom_srtcom code_soort_kanaalvoorkeur  --kvr.code_soort_kanaalvoorkeur
  from   rle_kanaalvoorkeuren  kvr
  ,      rle_communicatie_cat_kanalen cck
  ,      rle_communicatie_categorieen ccn
  where  kvr.rle_numrelatie = b_numrelatie
    and  sysdate            between kvr.dat_begin
                            and     nvl(kvr.dat_einde,sysdate+1)
  and    kvr.cck_id = cck.id
  and    cck.ccn_id = ccn.id
  and    ccn.adm_code = 'ALG'
  and    ccn.code = 'ALG'
  ;

  r_kvr        c_kvr%rowtype;
begin
  if not tab_kanaal.exists (p_rle_numrelatie)
  or cache_verlopen (tab_kanaal(p_rle_numrelatie).timestamp)
  then
    -- gegevens zijn in deze sessie nog niet eerder opgehaald, of dit is langer geleden gedaan
    -- dan de maximale geldigheid van de cache, dus haal de gegevens (opnieuw) op.
    open  c_kvr(p_rle_numrelatie);
    fetch c_kvr
    into  r_kvr;
    l_kvr_found  := c_kvr%found;
    close c_kvr;

    r_kanaal := null;

    if l_kvr_found
    then
      r_kanaal.kanaalvoorkeur := r_kvr.code_soort_kanaalvoorkeur;
    end if;

    r_kanaal.timestamp := dbms_utility.get_time;

    tab_kanaal(p_rle_numrelatie) := r_kanaal;
  end if;
end;
/* Haal communicatie nr's op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_COMM_NR
 (P_RLE_NUMRELATIE IN relnum_type
 ,P_SOORT_ADRES IN VARCHAR2 := null
 )
 IS

R_COMM_NR RT_COMM_NR;
  --
  cursor c_ras(b_rle_numrelatie rle_relatie_adressen.rle_numrelatie%type
              ,b_dwe_wrddom_srtadr rle_relatie_adressen.dwe_wrddom_srtadr%type
              )
  is
  select cpn.rle_numrelatie
  from   rle_relatie_adressen ras
  ,      rle_contactpersonen cpn
  where    ras.rle_numrelatie = b_rle_numrelatie
    and    ras.dwe_wrddom_srtadr = b_dwe_wrddom_srtadr
    and    ras.id = cpn.ras_id
    and    cpn.dat_creatie = (select max (cpn_max.dat_creatie)
                              from   rle_contactpersonen cpn_max
                              where  cpn_max.ras_id = cpn.ras_id)
    and    ras.rol_codrol = 'WG'
    and    ras.adm_code = 'ALG'
    and    ras.datingang <= sysdate
    and    nvl(ras.dateinde, sysdate) >= trunc( sysdate )
  ;
  cursor c_cnr (b_rle_numrelatie  rle_communicatie_nummers.rle_numrelatie%type
               ,b_rol_codrol rle_communicatie_nummers.rol_codrol%type
               )
  is
  select numcommunicatie     nummer
  ,      dwe_wrddom_srtcom   soort
  ,      adm_code
  from   (select numcommunicatie
          ,      dwe_wrddom_srtcom
          ,      datingang
          ,      adm_code
          ,      max(datingang) over (partition by rle_numrelatie,dwe_wrddom_srtcom,adm_code) meest_recent
          from   rle_communicatie_nummers
          where  rle_numrelatie      = b_rle_numrelatie
            and  rol_codrol = b_rol_codrol
            and  dwe_wrddom_srtcom   in ('TEL'
                                        ,'FAX'
                                        ,'EMAIL'
                                        )
            and  sysdate             between datingang
                                     and     nvl(dateinde,sysdate+1)
         )
  where  datingang   = meest_recent
  ;

 cursor c_ras_wg (b_rle_numrelatie  rle_communicatie_nummers.rle_numrelatie%type)
  is
   select cnr.numcommunicatie tel_nummer_wgr,
          cnr2.numcommunicatie email_adres_wgr
   from   rle_relatie_adressen ras,
          rle_communicatie_nummers cnr,
          rle_communicatie_nummers cnr2
   where  cnr.rle_numrelatie(+) = ras.rle_numrelatie
   and    cnr.rol_codrol(+) = 'WG'
   and    cnr.dwe_wrddom_srtcom(+) = 'TEL'
   and    cnr.adm_code (+)= stm_context.administratie
   and    ras.id = cnr.ras_id(+)
   and    sysdate between cnr.datingang(+) and nvl ( cnr.dateinde(+), sysdate + 1 )
   and    cnr2.rle_numrelatie(+) = ras.rle_numrelatie
   and    cnr2.rol_codrol(+) = 'WG'
   and    cnr2.dwe_wrddom_srtcom(+) = 'EMAIL'
   and    cnr2.adm_code (+) = stm_context.administratie
   and    ras.id = cnr2.ras_id(+)
   and    sysdate between cnr2.datingang(+) and nvl ( cnr2.dateinde(+), sysdate + 1 )
   and    ras.rle_numrelatie = b_rle_numrelatie
   and    ras.rol_codrol = 'WG'
   and    ras.dwe_wrddom_srtadr = 'SZ'
   and    sysdate between ras.datingang and nvl ( ras.dateinde, sysdate + 1 );

  l_ophalen        boolean;
  l_rle_numrelatie rle_relaties.numrelatie%type;
  l_rol_codrol     rle_communicatie_nummers.rol_codrol%type;
  type ttab_cnr is table of c_cnr%rowtype  index by pls_integer;
  tab_cnr          ttab_cnr;
  r_ras_wg  c_ras_wg%rowtype;
begin
  l_ophalen  := case p_soort_adres
                  when cn_ads_statutair then (   not tab_comm_nr_sz.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_comm_nr_sz(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_feitelijk then (   not tab_comm_nr_fa.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_comm_nr_fa(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_inspectie then (   not tab_comm_nr_ia.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_comm_nr_ia(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_bijz_nota then (   not tab_comm_nr_ba.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_comm_nr_ba(p_rle_numrelatie).timestamp)
                                             )
                  when cn_ads_correspon then (   not tab_comm_nr_ca.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_comm_nr_ca(p_rle_numrelatie).timestamp)
                                             )
                  else                       (   not tab_comm_nr.exists (p_rle_numrelatie)
                                              or cache_verlopen (tab_comm_nr(p_rle_numrelatie).timestamp)
                                             )
                end;

  if l_ophalen
  then
    -- gegevens zijn in deze sessie nog niet eerder opgehaald, of dit is langer geleden gedaan
    -- dan de maximale geldigheid van de cache, dus haal de gegevens (opnieuw) op.
    if p_soort_adres is not null
    then
      open c_ras (p_rle_numrelatie
                 ,p_soort_adres);
      fetch c_ras into l_rle_numrelatie;
      close c_ras;
      l_rol_codrol := 'CP';
    else
      l_rle_numrelatie := p_rle_numrelatie;
      l_rol_codrol := 'WG';
    end if;
    open  c_cnr (l_rle_numrelatie
                ,l_rol_codrol);
    fetch c_cnr
    bulk collect
    into  tab_cnr;
    close c_cnr;

    r_comm_nr := null;

    if tab_cnr.count > 0
    then
      -- Eerst voor de context administratie
      for i_cnr in tab_cnr.first .. tab_cnr.last
      loop
        if tab_cnr(i_cnr).adm_code = stm_context.administratie
        then
          if tab_cnr(i_cnr).soort = 'TEL'  and r_comm_nr.tel_nummer is null
          then
             r_comm_nr.tel_nummer  := tab_cnr(i_cnr).nummer;
          elsif tab_cnr(i_cnr).soort = 'FAX'  and r_comm_nr.fax_nummer is null
          then
            r_comm_nr.fax_nummer  := tab_cnr(i_cnr).nummer;
          elsif tab_cnr(i_cnr).soort =  'EMAIL' and r_comm_nr.email_adres is null
          then
             r_comm_nr.email_adres := tab_cnr(i_cnr).nummer;
          end if;
        end if;
      end loop;
      -- Indien niet gevonden voor ALG administratie
      for i_cnr in tab_cnr.first .. tab_cnr.last
      loop

        if tab_cnr(i_cnr).adm_code = 'ALG'
        then
          if tab_cnr(i_cnr).soort = 'TEL'  and r_comm_nr.tel_nummer is null
          then
             r_comm_nr.tel_nummer  := tab_cnr(i_cnr).nummer;
          elsif tab_cnr(i_cnr).soort = 'FAX'  and r_comm_nr.fax_nummer is null
          then
            r_comm_nr.fax_nummer  := tab_cnr(i_cnr).nummer;
          elsif tab_cnr(i_cnr).soort =  'EMAIL' and r_comm_nr.email_adres is null
          then
             r_comm_nr.email_adres := tab_cnr(i_cnr).nummer;
          end if;
        end if;
      end loop;
    end if;
    -- Bepaal tel/email voor SZ adres werkgever

    if p_soort_adres = cn_ads_statutair
    then
      open c_ras_wg (p_rle_numrelatie);
      fetch c_ras_wg into r_ras_wg;
      close c_ras_wg;

      r_comm_nr.tel_nummer_wgr := r_ras_wg.tel_nummer_wgr;
      r_comm_nr.email_adres_wgr:= r_ras_wg.email_adres_wgr;
    end if;

    r_comm_nr.timestamp := dbms_utility.get_time;

    if l_rle_numrelatie is not null
    then
      case p_soort_adres
        when cn_ads_statutair then tab_comm_nr_sz(p_rle_numrelatie) := r_comm_nr;
        when cn_ads_feitelijk then tab_comm_nr_fa(p_rle_numrelatie) := r_comm_nr;
        when cn_ads_inspectie then tab_comm_nr_ia(p_rle_numrelatie) := r_comm_nr;
        when cn_ads_bijz_nota then tab_comm_nr_ba(p_rle_numrelatie) := r_comm_nr;
        when cn_ads_correspon then tab_comm_nr_ca(p_rle_numrelatie) := r_comm_nr;
        else                       tab_comm_nr (p_rle_numrelatie) := r_comm_nr;
        end case;
    elsif l_rle_numrelatie is null and -- geen contactpersoon
          p_soort_adres = cn_ads_statutair
    then
      tab_comm_nr_sz(p_rle_numrelatie) := r_comm_nr;
    end if;
  end if;
end;
/* Haal accountmanager op als geen geldige gecachete gegevens bestaan */
PROCEDURE GET_ACTMGR
 (P_WERKGEVERNR IN wgrnum_type
 )
 IS

R_ACTMGR RT_ACTMGR;
begin
  if not tab_actmgr.exists (p_werkgevernr)
  or cache_verlopen (tab_actmgr(p_werkgevernr).timestamp)
  then
    -- gegevens zijn in deze sessie nog niet eerder opgehaald, of dit is langer geleden gedaan
    -- dan de maximale geldigheid van de cache, dus haal de gegevens (opnieuw) op.
    r_actmgr := null;

    -- indien de bij de werkgever opgehaalde medewerker niet bekend is wordt door de
    -- onderstaande procedure een exception geraised.
    -- in dat geval geen account manager gegevens bekend, dus velden leeg laten
    begin
      org_get_geg_acc_man
         (p_deelnemernr    => null
         ,p_werkgevernr    => p_werkgevernr
         ,p_naam           => r_actmgr.actmgr_naam
         ,p_telefoonnummer => r_actmgr.actmgr_telefoonnummer
         ,p_voorletters    => r_actmgr.actmgr_voorletters
         ,p_voorvoegsels   => r_actmgr.actmgr_voorvoegsels
         );
    exception
      when others
      then
        r_actmgr := null;
    end;

    r_actmgr.timestamp := dbms_utility.get_time;
    tab_actmgr (p_werkgevernr) := r_actmgr;
  end if;
end;
FUNCTION KVK_NR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS

L_RETURN MAXCHAR;
begin
  l_return := rle_m03_rec_extcod
                 (p_systeemcomp => 'KVK'
                 ,p_relnum      => p_rle_numrelatie
                 ,p_codrol      => null
                 ,p_datparm     => sysdate
                 );
  return (l_return);
end;
FUNCTION PENS_ADMIN
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS

L_RETURN MAXCHAR;
begin
  l_return := rle_m03_rec_extcod
                 (p_systeemcomp => 'PENS'
                 ,p_relnum      => p_rle_numrelatie
                 ,p_codrol      => null
                 ,p_datparm     => sysdate
                 );
  return (l_return);
end;
FUNCTION BOVAG_NR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS

L_RETURN MAXCHAR;
begin
  l_return := rle_m03_rec_extcod
                 (p_systeemcomp => 'BOVAG'
                 ,p_relnum      => p_rle_numrelatie
                 ,p_codrol      => null
                 ,p_datparm     => sysdate
                 );
  return (l_return);
end;
FUNCTION INCASSO_NR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).numrekening_ai);
end;
FUNCTION ALGEMEEN_NR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).numrekening_ag);
end;
FUNCTION SZ_POSTCODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).postcode);
end;
FUNCTION SZ_HUISNUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).huisnr);
end;
FUNCTION SZ_HUISNUMMER_TOEVOEGING
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).toevnum);
end;
FUNCTION SZ_WOONPLAATS
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).woonplaats);
end;
FUNCTION SZ_STRAAT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).straat);
end;
FUNCTION SZ_LAND_CODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).codland);
end;
FUNCTION SZ_INDICATIEWOONBOOT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).ind_woonboot);
end;
FUNCTION SZ_INDICATIEWOONWAGEN
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).ind_woonwagen);
end;
FUNCTION SZ_LOCATIE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).locatie);
end;
FUNCTION SZ_LANDNAAM
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).landnaam);
end;
FUNCTION SZ_RELATIENR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).contactpers);
end;
FUNCTION SZ_NAAM_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_statutair  -- in
     );

  return (tab_ads_sz(p_rle_numrelatie).opmaaknaam);
end;
FUNCTION SZ_TELNR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => cn_ads_statutair -- in
     );

  return (tab_comm_nr_sz (p_rle_numrelatie).tel_nummer);
end;
FUNCTION SZ_EMAIL_ADRES
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin

  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => cn_ads_statutair  -- in
     );


  return (tab_comm_nr_sz (p_rle_numrelatie).email_adres_wgr);
end;
FUNCTION SZ_TEL_NUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => cn_ads_statutair  -- in
     );

  return (tab_comm_nr_sz (p_rle_numrelatie).tel_nummer_wgr);
end;
FUNCTION FA_POSTCODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).postcode);
end;
FUNCTION FA_HUISNUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).huisnr);
end;
FUNCTION FA_HUISNUMMER_TOEVOEGING
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).toevnum);
end;
FUNCTION FA_WOONPLAATS
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).woonplaats);
end;
FUNCTION FA_STRAAT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).straat);
end;
FUNCTION FA_LAND_CODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).codland);
end;
FUNCTION FA_INDICATIEWOONBOOT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).ind_woonboot);
end;
FUNCTION FA_INDICATIEWOONWAGEN
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).ind_woonwagen);
end;
FUNCTION FA_LOCATIE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).locatie);
end;
FUNCTION FA_LANDNAAM
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).landnaam);
end;
FUNCTION FA_RELATIENR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).contactpers);
end;
FUNCTION FA_NAAM_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_feitelijk  -- in
     );

  return (tab_ads_fa(p_rle_numrelatie).opmaaknaam);
end;
FUNCTION FA_TELNR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => cn_ads_feitelijk -- in
     );

  return (tab_comm_nr_fa (p_rle_numrelatie).tel_nummer);
end;
FUNCTION IA_POSTCODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).postcode);
end;
FUNCTION IA_HUISNUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).huisnr);
end;
FUNCTION IA_HUISNUMMER_TOEVOEGING
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).toevnum);
end;
FUNCTION IA_WOONPLAATS
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).woonplaats);
end;
FUNCTION IA_STRAAT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).straat);
end;
FUNCTION IA_LAND_CODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).codland);
end;
FUNCTION IA_INDICATIEWOONBOOT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).ind_woonboot);
end;
FUNCTION IA_INDICATIEWOONWAGEN
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).ind_woonwagen);
end;
FUNCTION IA_LOCATIE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).locatie);
end;
FUNCTION IA_LANDNAAM
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).landnaam);
end;
FUNCTION IA_RELATIENR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).contactpers);
end;
FUNCTION IA_NAAM_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_inspectie  -- in
     );

  return (tab_ads_ia(p_rle_numrelatie).opmaaknaam);
end;
FUNCTION IA_TELNR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => cn_ads_inspectie -- in
     );

  return (tab_comm_nr_ia (p_rle_numrelatie).tel_nummer);
end;
FUNCTION CA_POSTCODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).postcode);
end;
FUNCTION CA_HUISNUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).huisnr);
end;
FUNCTION CA_HUISNUMMER_TOEVOEGING
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).toevnum);
end;
FUNCTION CA_WOONPLAATS
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).woonplaats);
end;
FUNCTION CA_STRAAT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).straat);
end;
FUNCTION CA_LAND_CODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).codland);
end;
FUNCTION CA_INDICATIEWOONBOOT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).ind_woonboot);
end;
FUNCTION CA_INDICATIEWOONWAGEN
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).ind_woonwagen);
end;
FUNCTION CA_LOCATIE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).locatie);
end;
FUNCTION CA_LANDNAAM
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).landnaam);
end;
FUNCTION CA_RELATIENR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).contactpers);
end;
FUNCTION CA_NAAM_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_correspon  -- in
     );

  return (tab_ads_ca(p_rle_numrelatie).opmaaknaam);
end;
FUNCTION CA_TELNR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => cn_ads_correspon -- in
     );

  return (tab_comm_nr_ca (p_rle_numrelatie).tel_nummer);
end;
FUNCTION BA_POSTCODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).postcode);
end;
FUNCTION BA_HUISNUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).huisnr);
end;
FUNCTION BA_HUISNUMMER_TOEVOEGING
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).toevnum);
end;
FUNCTION BA_WOONPLAATS
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).woonplaats);
end;
FUNCTION BA_STRAAT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).straat);
end;
FUNCTION BA_LAND_CODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).codland);
end;
FUNCTION BA_INDICATIEWOONBOOT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).ind_woonboot);
end;
FUNCTION BA_INDICATIEWOONWAGEN
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).ind_woonwagen);
end;
FUNCTION BA_LOCATIE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).locatie);
end;
FUNCTION BA_LANDNAAM
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).landnaam);
end;
FUNCTION BA_RELATIENR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).contactpers);
end;
FUNCTION BA_NAAM_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_adres
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres     => cn_ads_bijz_nota  -- in
     );

  return (tab_ads_ba(p_rle_numrelatie).opmaaknaam);
end;
FUNCTION BA_TELNR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 ,P_SOORT_ADRES IN VARCHAR2 := 'null'
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => cn_ads_bijz_nota -- in
     );

  return (tab_comm_nr_ba (p_rle_numrelatie).tel_nummer);
end;
FUNCTION LA_RELATIENR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_relatienr);
end;
FUNCTION LA_NAAM
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_naam);
end;
FUNCTION LA_STRAAT
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_straat);
end;
FUNCTION LA_HUISNR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN NUMBER
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_huisnr);
end;
FUNCTION LA_TOEVNUM
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_toevnum);
end;
FUNCTION LA_POSTCODE
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_postcode);
end;
FUNCTION LA_WOONPLAATS
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_woonplaats);
end;
FUNCTION LA_LAND
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_land);
end;
FUNCTION LA_TELNR_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_telnr_contactpersoon);
end;
FUNCTION LA_NAAM_CONTACTPERSOON
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_loonadmin_kantoor
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_la_kantoor (p_rle_numrelatie).la_naam_contactpersoon);
end;
FUNCTION KANAALVOORKEUR
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_kanaalvoorkeur
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_kanaal (p_rle_numrelatie).kanaalvoorkeur);
end;
FUNCTION TEL_NUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     ,p_soort_adres => null --in
     );

  return (tab_comm_nr (p_rle_numrelatie).tel_nummer);
end;
FUNCTION FAX_NUMMER
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_comm_nr (p_rle_numrelatie).fax_nummer);
end;
FUNCTION EMAIL_ADRES
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_comm_nr
     (p_rle_numrelatie  => p_rle_numrelatie  -- in
     );

  return (tab_comm_nr (p_rle_numrelatie).email_adres);
end;
FUNCTION ACTMGR_NAAM
 (P_WERKGEVERNR IN wgrnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_actmgr
     (p_werkgevernr  => p_werkgevernr  -- in
     );

  return (tab_actmgr (p_werkgevernr).actmgr_naam);
end;
FUNCTION ACTMGR_TELEFOONNUMMER
 (P_WERKGEVERNR IN wgrnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_actmgr
     (p_werkgevernr  => p_werkgevernr  -- in
     );

  return (tab_actmgr (p_werkgevernr).actmgr_telefoonnummer);
end;
FUNCTION ACTMGR_VOORLETTERS
 (P_WERKGEVERNR IN wgrnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_actmgr
     (p_werkgevernr  => p_werkgevernr  -- in
     );

  return (tab_actmgr (p_werkgevernr).actmgr_voorletters);
end;
FUNCTION ACTMGR_VOORVOEGSELS
 (P_WERKGEVERNR IN wgrnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_actmgr
     (p_werkgevernr  => p_werkgevernr  -- in
     );

  return (tab_actmgr (p_werkgevernr).actmgr_voorvoegsels);
end;
FUNCTION KLNTTEAM_NAAM
 (P_WERKGEVERNR IN wgrnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_klntteam
    (p_werkgevernr  => p_werkgevernr  -- in
     );

  return (tab_klntteam (p_werkgevernr).klntteam_naam);

end;
FUNCTION KLNTTEAM_TEL
 (P_WERKGEVERNR IN wgrnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_klntteam
     (p_werkgevernr  => p_werkgevernr  -- in
     );

  return (tab_klntteam (p_werkgevernr).klntteam_tel);

end;
PROCEDURE GET_KLNTTEAM
 (P_WERKGEVERNR IN wgrnum_type
 )
 IS

R_KLNTTEAM RT_KLNTTEAM;
begin
  if not tab_klntteam.exists (p_werkgevernr)
  or cache_verlopen (tab_klntteam(p_werkgevernr).timestamp)
  then
    -- gegevens zijn in deze sessie nog niet eerder opgehaald, of dit is langer geleden gedaan
    -- dan de maximale geldigheid van de cache, dus haal de gegevens (opnieuw) op.
    r_klntteam := null;

    -- indien de bij de werkgever opgehaalde medewerker niet bekend is wordt door de
    -- onderstaande procedure een exception geraised.
    -- in dat geval geen account manager gegevens bekend, dus velden leeg laten
    begin
      org_get_geg_oed ( p_user_id        => null
                      , p_werkgevernr    => p_werkgevernr
                      , p_deelnemernr    => null
                      , p_type_org_eenh  => 'KLT'
                      , p_omschrijving   => r_klntteam.klntteam_naam
                      , p_telefoonnummer => r_klntteam.klntteam_tel
                      );
    --
    exception
      when others
      then
        r_klntteam := null;
    end;

    r_klntteam.timestamp := dbms_utility.get_time;
    tab_klntteam (p_werkgevernr) := r_klntteam;
  end if;
end;
FUNCTION BET_WIJZE_AG
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).bet_wijze_ag);
end;
FUNCTION BET_WIJZE_AI
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).bet_wijze_ai);
end;
FUNCTION BIC_AG
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).bic_ag);
end;
FUNCTION BIC_AI
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).bic_ai);
end;
FUNCTION IBAN_AG
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).iban_ag);
end;
FUNCTION IBAN_AI
 (P_RLE_NUMRELATIE IN relnum_type
 )
 RETURN VARCHAR2
 IS
begin
  get_fin_nummers (p_rle_numrelatie  => p_rle_numrelatie);

  return (tab_fin_nummer(p_rle_numrelatie).iban_ai);
end;

END RLE_WGR;
/