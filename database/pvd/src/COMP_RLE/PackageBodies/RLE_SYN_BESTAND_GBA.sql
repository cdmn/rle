CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_SYN_BESTAND_GBA IS
 /****************************************************************

  Packagenaam: RLE_SYN_BESTAND_GBA
  Beschrijving
    Aanmaken synchronisatie bestand RLE => GBA.
    Het bestand wordt ter controle aangeboden bij GBA.
 FO RLE1000

  Versie Datum      Wie        Wat
  ------ --------   ---------- --------------------------------
  1.0    16-08-2013 XKV        Creatie
  1.1    31-03-2013 XSW/WHR    Aanpassingen HERBA deel 2
  1.2    03-09-2014  MAL       Cursor c_sga aangepast op parameters
                                                   p_groep en p_reden
*****************************************************************/  --
  -- Globals
  --
  g_volgnummer         pls_integer := 0;
  g_script_id          number;
  g_veld_sofinummer    varchar2(300);
  g_bestand            utl_file.file_type;
  g_partner_gegevens   boolean:= false;
  g_overige_gegevens   boolean:= false;
  g_adres_gegevens     boolean:= false;
  type rec_rle_syn_gba is table of varchar2(300) index by binary_integer;
  tab_rle_syn_gba      rec_rle_syn_gba;
  sort_tab_rle_syn_gba rec_rle_syn_gba;

  --
  -- Constanten
  --
  cn_package     constant varchar2(30) := 'RLE_SYN_BESTAND_GBA';
  cn_module      constant stm_util.t_procedure%type := cn_package ||'.VERWERK';

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
PROCEDURE MAAK_DETAIL_REGEL
 (P_NR IN PLS_INTEGER
 ,P_WAARDE IN VARCHAR2
 ,P_LINE IN OUT VARCHAR2
 );
PROCEDURE MAAK_HEADER
 (P_FOUTMELDING OUT VARCHAR2
 ,P_SUCCES OUT BOOLEAN
 );
FUNCTION BEPAAL_SOFINUMMER
 (P_SOFINUMMER_REC IN RLE_SYN_NAAR_GBA%ROWTYPE
 )
 RETURN VARCHAR2;


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS


    /**********************************************************************
    Naam:         WEGSCHRIJVEN STM_LIJST
    Beschrijving:
    **********************************************************************/

    cn_lijst constant pls_integer := 1;
begin
  stm_util.insert_lijsten(cn_lijst, g_volgnummer, p_text);
  stm_util.debug('LIJST :' || p_text);
end lijst;
PROCEDURE MAAK_DETAIL_REGEL
 (P_NR IN PLS_INTEGER
 ,P_WAARDE IN VARCHAR2
 ,P_LINE IN OUT VARCHAR2
 )
 IS
  /**********************************************************************
    Naam:         MAAK_DETAIL_REGEL
    Beschrijving: Bouw de detailregel op
  **********************************************************************/
begin
  if p_nr = sort_tab_rle_syn_gba.last
  then
    p_line := p_line||p_waarde;
  else
    p_line := p_line||p_waarde||';';
  end if;
end maak_detail_regel;
PROCEDURE MAAK_HEADER
 (P_FOUTMELDING OUT VARCHAR2
 ,P_SUCCES OUT BOOLEAN
 )
 IS
  /**********************************************************************
    Naam:         MAAK_HEADER
    Beschrijving: Bepaal de rubrieknummers uit het parameter bestand.
                  Stop deze in volgorde van rubrieknummer GBA oplopend in
                  de plsql tabel (is de basis voor de verdere vewerking).
                  Maak vervolgens het bestand met de headerregel aan.
  **********************************************************************/
  cursor c_pos
  is
   select *
   from   rle_syn_naar_gba
   where  rownum = 1;

  r_pos                    c_pos%rowtype;
  i                        pls_integer;
  l_teller                 number:=1;
  l_header_line            varchar2(4000);
  l_rubriek_sofi           boolean:=false;
  l_rubriek_overig         boolean:=false;
  l_rubriek_postcode       boolean:=false;
  l_rubriek_huisnr         boolean:=false;
  l_rubriek_huisletter     boolean:=false;
  l_rubriek_huisnrtoev     boolean:=false;
  l_rubriek_anr_partner    boolean:=false;
  l_rubriek_sofinr_partner boolean:=false;
begin
  sort_tab_rle_syn_gba.delete;
  tab_rle_syn_gba.delete;

  stm_util.debug('**** Bepaal de aan te leveren rubrieknummers uit de parameter tabel ****');
  open c_pos;
  fetch c_pos into r_pos;
  close c_pos;

  stm_util.debug('**** Vul de array in de juiste volgorde met alle mogelijke rubrieknummers ****');
  tab_rle_syn_gba(1)  := '01.01.10';
  tab_rle_syn_gba(2)  := '01.01.20';
  tab_rle_syn_gba(3)  := '01.02.10';
  tab_rle_syn_gba(4)  := '01.02.20';
  tab_rle_syn_gba(5)  := '01.02.30';
  tab_rle_syn_gba(6)  := '01.02.40';
  tab_rle_syn_gba(7)  := '01.03.10';
  tab_rle_syn_gba(8)  := '01.04.10';
  tab_rle_syn_gba(9)  := '01.61.10';
  tab_rle_syn_gba(10) := '04.05.10';
  tab_rle_syn_gba(11) := '05.01.10';
  tab_rle_syn_gba(12) := '05.01.20';
  tab_rle_syn_gba(13) := '05.02.10';
  tab_rle_syn_gba(14) := '05.02.20';
  tab_rle_syn_gba(15) := '05.02.30';
  tab_rle_syn_gba(16) := '05.02.40';
  tab_rle_syn_gba(17) := '05.03.10';
  tab_rle_syn_gba(18) := '05.06.10';
  tab_rle_syn_gba(19) := '05.07.10';
  tab_rle_syn_gba(20) := '05.07.40';
  tab_rle_syn_gba(21) := '05.83.10';
  tab_rle_syn_gba(22) := '05.83.20';
  tab_rle_syn_gba(23) := '05.83.30';
  tab_rle_syn_gba(24) := '06.08.10';
  tab_rle_syn_gba(25) := '07.70.10';
  tab_rle_syn_gba(26) := '08.09.10';
  tab_rle_syn_gba(27) := '08.10.10';
  tab_rle_syn_gba(28) := '08.10.20';
  tab_rle_syn_gba(29) := '08.10.30';
  tab_rle_syn_gba(30) := '08.11.10';
  tab_rle_syn_gba(31) := '08.11.15';
  tab_rle_syn_gba(32) := '08.11.20';
  tab_rle_syn_gba(33) := '08.11.30';
  tab_rle_syn_gba(34) := '08.11.40';
  tab_rle_syn_gba(35) := '08.11.50';
  tab_rle_syn_gba(36) := '08.11.60';
  tab_rle_syn_gba(37) := '08.11.70';
  tab_rle_syn_gba(38) := '08.11.80';
  tab_rle_syn_gba(39) := '08.11.90';
  tab_rle_syn_gba(40) := '08.13.10';
  tab_rle_syn_gba(41) := '08.13.20';

  stm_util.debug('**** Loop over de initieele array (tab_rle_syn_gba) en vul de sort array (sort_tab_rle_syn_gba) zodra de rubriek ook gevonden is in het parameter bestand ****');
  for i in nvl ( tab_rle_syn_gba.first, 0) .. nvl ( tab_rle_syn_gba.last, -1) loop
    if tab_rle_syn_gba(i) in (r_pos.veld1,r_pos.veld2,r_pos.veld3,r_pos.veld4,r_pos.veld5,r_pos.veld6,r_pos.veld7,r_pos.veld8,r_pos.veld9,r_pos.veld10,
                              r_pos.veld11,r_pos.veld12,r_pos.veld13,r_pos.veld14,r_pos.veld15,r_pos.veld16,r_pos.veld17,r_pos.veld18,r_pos.veld19,r_pos.veld20,
                              r_pos.veld21,r_pos.veld22,r_pos.veld23,r_pos.veld24,r_pos.veld25,r_pos.veld26,r_pos.veld27,r_pos.veld28,r_pos.veld29,r_pos.veld30,
                              r_pos.veld31,r_pos.veld32,r_pos.veld33,r_pos.veld34,r_pos.veld35,r_pos.veld36,r_pos.veld37,r_pos.veld38,r_pos.veld39,r_pos.veld40,
                              r_pos.veld41,r_pos.veld42,r_pos.veld43,r_pos.veld44,r_pos.veld45,r_pos.veld46,r_pos.veld47,r_pos.veld48,r_pos.veld49,r_pos.veld50,
                              '01.01.10','01.01.20','01.03.10','01.04.10')
    then
      l_teller:= l_teller + 1;
      -- Op deze manier wordt de juiste volgorde verkregen voor de gewenste velden
      sort_tab_rle_syn_gba(l_teller):= tab_rle_syn_gba(i);

      -- Bepaal of partner gegevens moeten worden opgehaald
      if tab_rle_syn_gba(i) in ('05.01.10','05.01.20','05.02.10','05.02.30','05.02.40','05.03.10')
      then
        stm_util.debug('**** Partner gegevens bepalen ****');
        g_partner_gegevens:= true;
      end if;

      -- Bepaal of overige persoonsgegevens moeten worden opgehaald
      if tab_rle_syn_gba(i) in ('01.04.10','01.61.10')
      then
        stm_util.debug('**** Overige gegevens bepalen ****');
        g_overige_gegevens:= true;
      end if;

      -- Bepaal of adresgegevens moeten worden opgehaald
      if tab_rle_syn_gba(i) in ('08.11.10','08.11.20','08.11.30','08.11.40','08.11.60','08.11.70','08.13.10','08.13.20')
      then
        stm_util.debug('**** Adres gegevens bepalen ****');
        g_adres_gegevens:= true;
      end if;

    end if;
  end loop;

  stm_util.debug('**** Vul de header regel ****');

  for i in nvl ( sort_tab_rle_syn_gba.first, 0) .. nvl ( sort_tab_rle_syn_gba.last, -1) loop
   if sort_tab_rle_syn_gba(i) <> '01.01.20'
   then
     l_rubriek_overig:= true;
   end if;

   if sort_tab_rle_syn_gba(i)    = '01.01.20'
   then
     l_rubriek_sofi:= true;
   elsif sort_tab_rle_syn_gba(i) = '08.11.60'
   then
     l_rubriek_postcode:= true;
   elsif sort_tab_rle_syn_gba(i) = '08.11.20'
   then
     l_rubriek_huisnr:= true;
   elsif sort_tab_rle_syn_gba(i) = '08.11.30'
   then
     l_rubriek_huisletter:= true;
   elsif sort_tab_rle_syn_gba(i) = '08.11.40'
   then
     l_rubriek_huisnrtoev:= true;
   elsif sort_tab_rle_syn_gba(i) = '05.01.10'
   then
     l_rubriek_anr_partner:= true;
   elsif sort_tab_rle_syn_gba(i) = '05.01.20'
   then
     l_rubriek_sofinr_partner:= true;
   end if;

   if i = sort_tab_rle_syn_gba.last
   then
     l_header_line:= l_header_line||sort_tab_rle_syn_gba(i);
   else
     l_header_line:= l_header_line||sort_tab_rle_syn_gba(i)||';';
   end if;
  end loop;

  stm_util.debug('**** Controleer de verplichte rubrieken en maak het bestand aan ****');
  if not l_rubriek_sofi or
     not l_rubriek_overig
  then
    p_succes:=false;
    p_foutmelding:= 'Rubriek sofinummer plus een overige rubriek minimaal vereist!!';
  elsif g_adres_gegevens and
       ( not l_rubriek_postcode or
         not l_rubriek_huisnr or
         not l_rubriek_huisletter or
         not l_rubriek_huisnrtoev)
  then
    p_succes:=false;
    p_foutmelding:= 'Postcode, huisnummer, huisletter, en toevoeging huisnummer zijn verplichte adres rubrieken!!';
  elsif g_partner_gegevens and
        ( not l_rubriek_anr_partner and
          not l_rubriek_sofinr_partner)
  then
    p_succes:=false;
    p_foutmelding:= 'A nummer partner of sofinummer partner is een verplichte partner rubriek!!';
  else
    stm_util.debug('**** Maak het bestand aan ****');
    g_bestand:=utl_file.fopen ( 'RLE_OUTBOX','RLEPRC62_RLE_SYN_GBA_'||g_script_id||'.csv', 'W' );
    utl_file.put_line ( g_bestand,replace(l_header_line,'.',''));
    p_succes:=true;
  end if;

end maak_header;
FUNCTION BEPAAL_SOFINUMMER
 (P_SOFINUMMER_REC IN RLE_SYN_NAAR_GBA%ROWTYPE
 )
 RETURN VARCHAR2
 IS
  /**********************************************************************
  Naam:         BEPAAL SOFINUMMER
  Beschrijving: Bepaal voor de eerste rij de plaats van het sofinummer
                in de parameter tabel. Vervolgens het sofinummer bepalen.
  **********************************************************************/
  cursor c_pos
  is
   select *
   from   rle_syn_naar_gba
   where  rownum = 1;

  r_pos        c_pos%rowtype;

  l_sofinummer varchar2(300);
begin
  --
  -- Positie sofinummer in de parameter tabel bepalen (alleen voor de eerste rij)
  --

  if g_veld_sofinummer is null
  then
    open c_pos;
    fetch c_pos into r_pos;
    close c_pos;

    case '01.01.20' -- sofinummer
     when r_pos.veld1  then g_veld_sofinummer := 'veld1';
     when r_pos.veld2  then g_veld_sofinummer := 'veld2';
     when r_pos.veld3  then g_veld_sofinummer := 'veld3';
     when r_pos.veld4  then g_veld_sofinummer := 'veld4';
     when r_pos.veld5  then g_veld_sofinummer := 'veld5';
     when r_pos.veld6  then g_veld_sofinummer := 'veld6';
     when r_pos.veld7  then g_veld_sofinummer := 'veld7';
     when r_pos.veld8  then g_veld_sofinummer := 'veld8';
     when r_pos.veld9  then g_veld_sofinummer := 'veld9';
     when r_pos.veld10 then g_veld_sofinummer := 'veld10';
     when r_pos.veld11 then g_veld_sofinummer := 'veld11';
     when r_pos.veld12 then g_veld_sofinummer := 'veld12';
     when r_pos.veld13 then g_veld_sofinummer := 'veld13';
     when r_pos.veld14 then g_veld_sofinummer := 'veld14';
     when r_pos.veld15 then g_veld_sofinummer := 'veld15';
     when r_pos.veld16 then g_veld_sofinummer := 'veld16';
     when r_pos.veld17 then g_veld_sofinummer := 'veld17';
     when r_pos.veld18 then g_veld_sofinummer := 'veld18';
     when r_pos.veld19 then g_veld_sofinummer := 'veld19';
     when r_pos.veld20 then g_veld_sofinummer := 'veld20';
     when r_pos.veld21 then g_veld_sofinummer := 'veld21';
     when r_pos.veld22 then g_veld_sofinummer := 'veld22';
     when r_pos.veld23 then g_veld_sofinummer := 'veld23';
     when r_pos.veld24 then g_veld_sofinummer := 'veld24';
     when r_pos.veld25 then g_veld_sofinummer := 'veld25';
     when r_pos.veld26 then g_veld_sofinummer := 'veld26';
     when r_pos.veld27 then g_veld_sofinummer := 'veld27';
     when r_pos.veld28 then g_veld_sofinummer := 'veld28';
     when r_pos.veld29 then g_veld_sofinummer := 'veld29';
     when r_pos.veld30 then g_veld_sofinummer := 'veld30';
     when r_pos.veld31 then g_veld_sofinummer := 'veld31';
     when r_pos.veld32 then g_veld_sofinummer := 'veld32';
     when r_pos.veld33 then g_veld_sofinummer := 'veld33';
     when r_pos.veld34 then g_veld_sofinummer := 'veld34';
     when r_pos.veld35 then g_veld_sofinummer := 'veld35';
     when r_pos.veld36 then g_veld_sofinummer := 'veld36';
     when r_pos.veld37 then g_veld_sofinummer := 'veld37';
     when r_pos.veld38 then g_veld_sofinummer := 'veld38';
     when r_pos.veld39 then g_veld_sofinummer := 'veld39';
     when r_pos.veld40 then g_veld_sofinummer := 'veld40';
     when r_pos.veld41 then g_veld_sofinummer := 'veld41';
     when r_pos.veld42 then g_veld_sofinummer := 'veld42';
     when r_pos.veld43 then g_veld_sofinummer := 'veld43';
     when r_pos.veld44 then g_veld_sofinummer := 'veld44';
     when r_pos.veld45 then g_veld_sofinummer := 'veld45';
     when r_pos.veld46 then g_veld_sofinummer := 'veld46';
     when r_pos.veld47 then g_veld_sofinummer := 'veld47';
     when r_pos.veld48 then g_veld_sofinummer := 'veld48';
     when r_pos.veld49 then g_veld_sofinummer := 'veld49';
     when r_pos.veld50 then g_veld_sofinummer := 'veld50';
    end case;


  end if;
  --
  -- Waarde sofinummer in record parameter bepalen
  --
  case g_veld_sofinummer -- positie sofinummer
    when 'veld1'  then l_sofinummer := p_sofinummer_rec.veld1;
    when 'veld2'  then l_sofinummer := p_sofinummer_rec.veld2;
    when 'veld3'  then l_sofinummer := p_sofinummer_rec.veld3;
    when 'veld4'  then l_sofinummer := p_sofinummer_rec.veld4;
    when 'veld5'  then l_sofinummer := p_sofinummer_rec.veld5;
    when 'veld6'  then l_sofinummer := p_sofinummer_rec.veld6;
    when 'veld7'  then l_sofinummer := p_sofinummer_rec.veld7;
    when 'veld8'  then l_sofinummer := p_sofinummer_rec.veld8;
    when 'veld9'  then l_sofinummer := p_sofinummer_rec.veld9;
    when 'veld10' then l_sofinummer := p_sofinummer_rec.veld10;
    when 'veld11' then l_sofinummer := p_sofinummer_rec.veld11;
    when 'veld12' then l_sofinummer := p_sofinummer_rec.veld12;
    when 'veld13' then l_sofinummer := p_sofinummer_rec.veld13;
    when 'veld14' then l_sofinummer := p_sofinummer_rec.veld14;
    when 'veld15' then l_sofinummer := p_sofinummer_rec.veld15;
    when 'veld16' then l_sofinummer := p_sofinummer_rec.veld16;
    when 'veld17' then l_sofinummer := p_sofinummer_rec.veld17;
    when 'veld18' then l_sofinummer := p_sofinummer_rec.veld18;
    when 'veld19' then l_sofinummer := p_sofinummer_rec.veld19;
    when 'veld20' then l_sofinummer := p_sofinummer_rec.veld20;
    when 'veld21' then l_sofinummer := p_sofinummer_rec.veld21;
    when 'veld22' then l_sofinummer := p_sofinummer_rec.veld22;
    when 'veld23' then l_sofinummer := p_sofinummer_rec.veld23;
    when 'veld24' then l_sofinummer := p_sofinummer_rec.veld24;
    when 'veld25' then l_sofinummer := p_sofinummer_rec.veld25;
    when 'veld26' then l_sofinummer := p_sofinummer_rec.veld26;
    when 'veld27' then l_sofinummer := p_sofinummer_rec.veld27;
    when 'veld28' then l_sofinummer := p_sofinummer_rec.veld28;
    when 'veld29' then l_sofinummer := p_sofinummer_rec.veld29;
    when 'veld30' then l_sofinummer := p_sofinummer_rec.veld30;
    when 'veld31' then l_sofinummer := p_sofinummer_rec.veld31;
    when 'veld32' then l_sofinummer := p_sofinummer_rec.veld32;
    when 'veld33' then l_sofinummer := p_sofinummer_rec.veld33;
    when 'veld34' then l_sofinummer := p_sofinummer_rec.veld34;
    when 'veld35' then l_sofinummer := p_sofinummer_rec.veld35;
    when 'veld36' then l_sofinummer := p_sofinummer_rec.veld36;
    when 'veld37' then l_sofinummer := p_sofinummer_rec.veld37;
    when 'veld38' then l_sofinummer := p_sofinummer_rec.veld38;
    when 'veld39' then l_sofinummer := p_sofinummer_rec.veld39;
    when 'veld40' then l_sofinummer := p_sofinummer_rec.veld40;
    when 'veld41' then l_sofinummer := p_sofinummer_rec.veld41;
    when 'veld42' then l_sofinummer := p_sofinummer_rec.veld42;
    when 'veld43' then l_sofinummer := p_sofinummer_rec.veld43;
    when 'veld44' then l_sofinummer := p_sofinummer_rec.veld44;
    when 'veld45' then l_sofinummer := p_sofinummer_rec.veld45;
    when 'veld46' then l_sofinummer := p_sofinummer_rec.veld46;
    when 'veld47' then l_sofinummer := p_sofinummer_rec.veld47;
    when 'veld48' then l_sofinummer := p_sofinummer_rec.veld48;
    when 'veld49' then l_sofinummer := p_sofinummer_rec.veld49;
    when 'veld50' then l_sofinummer := p_sofinummer_rec.veld50;
  end case;

  stm_util.debug('**** bepaal_sofinummer =' ||l_sofinummer||' ****');
  return l_sofinummer;
end bepaal_sofinummer;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_ALLE_PERSONEN IN VARCHAR2 := 'N'
 ,P_GROEP IN VARCHAR2
 ,P_REDEN IN VARCHAR2
 )
 IS
 /**********************************************************************
    Naam:         VERWERK
    Beschrijving: Maak Synchronisatiebestand GBA

                  In hoofdlijnen werkt het programma als volgt:

                  1) Vul array 1 in de juiste oplopende volgorde met alle
                     mogelijke rubrieknummers (standaard set).
                  2) Loop over de aangeleverde parameter tabel rle_syn_naar_gba
                    (input voor de specifieke run) en schrijf de overeenkomende
                     rubrieknummers op volgorde weg in array 2.
                  3) Loop over de parameter tabel voor de te verwerken
                     sofinummers en loop per sofinummer over array 2
                     (benodigde rubrieknummes) en zoek de bijbehorende gegevens
                     in RLE.
                  4) Schrijf de header en detailregels weg naar het bestand.
    **********************************************************************/
  -- parameter tabel of alle personen
    cursor c_sga (b_alle_personen varchar2
               ,b_groep         varchar2
               ,b_reden         varchar2
               )
  is
   select sga.veld1,sga.veld2,sga.veld3,sga.veld4,sga.veld5,sga.veld6,sga.veld7,sga.veld8,sga.veld9,sga.veld10,
          sga.veld11,sga.veld12,sga.veld13,sga.veld14,sga.veld15,sga.veld16,sga.veld17,sga.veld18,sga.veld19,sga.veld20,
          sga.veld21,sga.veld22,sga.veld23,sga.veld24,sga.veld25,sga.veld26,sga.veld27,sga.veld28,sga.veld29,sga.veld30,
          sga.veld31,sga.veld32,sga.veld33,sga.veld34,sga.veld35,sga.veld36,sga.veld37,sga.veld38,sga.veld39,sga.veld40,
          sga.veld41,sga.veld42,sga.veld43,sga.veld44,sga.veld45,sga.veld46,sga.veld47,sga.veld48,sga.veld49,sga.veld50
   from   (select a.*,rownum nr_row
           from   rle_syn_naar_gba a) sga
   where  sga.nr_row > 1
   and    b_alle_personen = 'N'
   and    b_groep is null
   union all
   select per.extern_relatie veld1,per.extern_relatie veld2,per.extern_relatie veld3,per.extern_relatie veld4,per.extern_relatie veld5,
          per.extern_relatie veld6,per.extern_relatie veld7,per.extern_relatie veld8,per.extern_relatie veld9,per.extern_relatie veld10,
          per.extern_relatie veld11,per.extern_relatie veld12,per.extern_relatie veld13,per.extern_relatie veld14,per.extern_relatie veld15,
          per.extern_relatie veld16,per.extern_relatie veld17,per.extern_relatie veld18,per.extern_relatie veld19,per.extern_relatie veld20,
          per.extern_relatie veld21,per.extern_relatie veld22,per.extern_relatie veld23,per.extern_relatie veld24,per.extern_relatie veld25,
          per.extern_relatie veld26,per.extern_relatie veld27,per.extern_relatie veld28,per.extern_relatie veld29,per.extern_relatie veld30,
          per.extern_relatie veld31,per.extern_relatie veld32,per.extern_relatie veld33,per.extern_relatie veld34,per.extern_relatie veld35,
          per.extern_relatie veld36,per.extern_relatie veld37,per.extern_relatie veld38,per.extern_relatie veld39,per.extern_relatie veld40,
          per.extern_relatie veld41,per.extern_relatie veld42,per.extern_relatie veld43,per.extern_relatie veld44,per.extern_relatie veld45,
          per.extern_relatie veld46,per.extern_relatie veld47,per.extern_relatie veld48,per.extern_relatie veld49,per.extern_relatie veld50
   from   rle_relatie_externe_codes per,
          rle_afstemmingen_gba asa
   where  per.rle_numrelatie = asa.rle_numrelatie
   and    per.rol_codrol = 'PR'
   and    per.dwe_wrddom_extsys = 'SOFI'
   and    asa.afnemers_indicatie = 'J'
   and    b_alle_personen = 'J'
   union all
   select per.extern_relatie veld1,per.extern_relatie veld2,per.extern_relatie veld3,per.extern_relatie veld4,per.extern_relatie veld5,
          per.extern_relatie veld6,per.extern_relatie veld7,per.extern_relatie veld8,per.extern_relatie veld9,per.extern_relatie veld10,
          per.extern_relatie veld11,per.extern_relatie veld12,per.extern_relatie veld13,per.extern_relatie veld14,per.extern_relatie veld15,
          per.extern_relatie veld16,per.extern_relatie veld17,per.extern_relatie veld18,per.extern_relatie veld19,per.extern_relatie veld20,
          per.extern_relatie veld21,per.extern_relatie veld22,per.extern_relatie veld23,per.extern_relatie veld24,per.extern_relatie veld25,
          per.extern_relatie veld26,per.extern_relatie veld27,per.extern_relatie veld28,per.extern_relatie veld29,per.extern_relatie veld30,
          per.extern_relatie veld31,per.extern_relatie veld32,per.extern_relatie veld33,per.extern_relatie veld34,per.extern_relatie veld35,
          per.extern_relatie veld36,per.extern_relatie veld37,per.extern_relatie veld38,per.extern_relatie veld39,per.extern_relatie veld40,
          per.extern_relatie veld41,per.extern_relatie veld42,per.extern_relatie veld43,per.extern_relatie veld44,per.extern_relatie veld45,
          per.extern_relatie veld46,per.extern_relatie veld47,per.extern_relatie veld48,per.extern_relatie veld49,per.extern_relatie veld50
   from   rle_relatie_externe_codes per,
          rle_afstemmingen_gba asa,
          rle_synchronisatie_statussen rss
   where  per.rle_numrelatie = asa.rle_numrelatie
   and    rss.rle_numrelatie = per.rle_numrelatie
   --and    rss.rle_numrelatie in (2155, 1537)
   and    per.rol_codrol = 'PR'
   and    per.dwe_wrddom_extsys = 'SOFI'
   and    b_alle_personen = 'N'
   and    b_groep is not null
   and    rss.groep in (select substr(c, instr(c,',',1,level)+1,
                                   instr(c, ',', 1, level+1)- instr (c, ',', 1, level)- 1)
                        from (select ','||b_groep||',' c from dual) t2
                        connect by level < length(c) - length(replace(c,','))
                       )
   and    (b_reden = 'J'
             or (b_reden = 'N'
                          and rss.reden is null)
          )
   ;


  -- Overige persoonsgegevens
  cursor c_rel(b_relatienummer rle_relatie_externe_codes.rle_numrelatie%type)
  is
   SELECT dwe_wrddom_geslacht         AS geslacht
         ,code_aanduiding_naamgebruik AS code_naamgebruik
   FROM   rle_relaties
   WHERE  numrelatie = b_relatienummer;

  -- persoonsgegevens partner
  cursor c_par (b_relatienummer rle_relatie_externe_codes.rle_numrelatie%type)
  is
   SELECT voornaam
         ,voorvoegsels
         ,namrelatie AS naamrelatie
         ,to_char(datgeboorte, 'yyyymmdd') AS datgeboorte
         ,code_geboortedatum_fictief AS code_fictief
         ,relatienummer
   FROM   rle_v_personen
   WHERE  relatienummer = b_relatienummer;

  -- persoonsgegevens
  cursor c_per (b_sofinummer rle_relatie_externe_codes.extern_relatie%type)
  is
   SELECT per.voornaam
         ,per.voorvoegsels
         ,per.namrelatie AS naamrelatie
         ,to_char(per.datgeboorte, 'yyyymmdd') AS datgeboorte
         ,to_char(per.datum_overlijden, 'yyyymmdd') AS datum_overlijden
         ,per.relatienummer
         ,per.code_geboortedatum_fictief AS code_fictief
         ,asa.afnemers_indicatie
   FROM   rle_v_personen       per
         ,rle_afstemmingen_gba asa
   WHERE  per.relatienummer = asa.rle_numrelatie(+)
   AND    per.sofinummer = b_sofinummer;

  -- Relatienummer partner
  cursor c_rkg (b_numrelatie rle_relatie_koppelingen.rle_numrelatie%type)
  is
   SELECT to_char(a.dat_begin, 'yyyymmdd') AS dat_begin
         ,a.rle_numrelatie_voor AS relnr_partner
         ,to_char(a.dat_einde, 'yyyymmdd') AS dat_einde
   FROM   rle_relatie_koppelingen a
   WHERE  a.rle_numrelatie = b_numrelatie
   AND    a.rkg_id = 1 -- Huwelijk (GBA geeft alleen huwelijk/geregistreerd partnerschap door en geen samenleving)
   AND    a.dat_begin = (SELECT MAX(b.dat_begin)
                         FROM   rle_relatie_koppelingen b
                         WHERE  b.rkg_id = 1
                         AND    b.rle_numrelatie = b_numrelatie);

  -- Sofi/A nummer
  cursor c_ext (b_numrelatie rle_relatie_externe_codes.rle_numrelatie%type,
                b_dwe_wrddom_extsys rle_relatie_externe_codes.dwe_wrddom_extsys%type)
  is
   SELECT extern_relatie
   FROM   rle_relatie_externe_codes
   WHERE  dwe_wrddom_extsys = b_dwe_wrddom_extsys
   AND    rle_numrelatie = b_numrelatie;

  -- Nationaliteit
  cursor c_nat (b_numrelatie rle_relatie_externe_codes.rle_numrelatie%type)
  is
   SELECT code_nationaliteit AS nationaliteit
   FROM   rle_nationaliteiten
   WHERE  rle_numrelatie = b_numrelatie;

  r_per           c_per%rowtype;
  r_par           c_par%rowtype;
  r_rkg           c_rkg%rowtype;
  r_rel           c_rel%rowtype;
  l_sofinummer    varchar2(300);
  l_orig_sofinum  varchar2(300);
  l_test_sofi     number;
  l_postcode      varchar2( 4000 );
  l_huisnr        number;
  l_toevnum       varchar2( 4000 );
  l_tvn_waarde    varchar2( 4000 );
  l_woonplaats    varchar2( 4000 );
  l_straat        varchar2( 4000 );
  l_codland       varchar2( 4000 );
  l_locatie       varchar2( 4000 );
  l_ind_woonboot  varchar2( 4000 );
  l_ind_woonwagen varchar2( 4000 );
  l_landnaam      varchar2( 4000 );
  l_begindatum    date;
  l_einddatum     date;
  l_opdrachtgever number;
  l_produkt       varchar2( 4000 );
  i               pls_integer;
  l_detail_line   varchar2(4000);
  l_waarde        varchar2(4000);
  l_verwerkt      number:=0;
  l_niet_verwerkt number:=0;
  l_succes        boolean;
  l_foutmelding   varchar2(300);
  l_dag           varchar2 (2);
  l_maand         varchar2 (2);
  l_jaar          varchar2 (4);
  l_groep       varchar2(50);

begin
  dbms_output.enable(null);
  stm_util.module_start(cn_module);
  stm_util.t_script_naam    := P_SCRIPT_NAAM;
  stm_util.t_script_id      := P_SCRIPT_ID;
  stm_util.t_programma_naam := cn_package;
  g_script_id := p_script_id;

  stm_util.debug('**** Bepaal aan te leveren rubrieknummers ****');
  maak_header(p_succes      => l_succes
             ,p_foutmelding => l_foutmelding);
   --
  lijst ( rpad ('***** LOGVERSLAG Aanmaken synchronisatie bestand RLE => GBA *****', 70)
               || ' STARTDATUM: ' || to_char (sysdate,'dd-mm-yyyy hh24:mi:ss'));

  lijst ( ' ' );
  lijst ( '------------------------------------------------------------------------------------------------------' );
  lijst ( ' ' );
  --
  lijst ('Invoerparameter:');
  lijst ('=======================================');
  lijst ('P_ALLE_PERSONEN: '|| p_alle_personen);
  lijst ('P_GROEP        : '|| p_groep);
  lijst ('P_REDEN        : '|| p_reden);
  lijst ('=======================================');
  lijst('');
  lijst ('Invoerbestand: rle_syn_gba.csv ');
  lijst ('=======================================');
  lijst('');
  lijst('**** UITVAL VERSLAG ****');
  lijst('');
  --
  if not l_succes
  then
    lijst('');
    lijst(l_foutmelding);
    lijst('');
  else
    lijst('Sofinummer                                Reden uitval');
    lijst('----------                  ----------------------------------------');

    stm_util.debug('**** Loop door de aangeleverde parameter tabel ****');

    l_groep := replace(p_groep, ' ', '');

  for r_sga in c_sga (b_alle_personen => p_alle_personen
                     ,b_groep         => l_groep
                     ,b_reden         => p_reden
                     )
  loop
      l_postcode      := null;
      l_huisnr        := null;
      l_toevnum       := null;
      l_woonplaats    := null;
      l_straat        := null;
      l_codland       := null;
      l_locatie       := null;
      l_ind_woonboot  := null;
      l_ind_woonwagen := null;
      l_landnaam      := null;
      l_begindatum    := null;
      l_einddatum     := null;
      l_opdrachtgever := null;
      l_produkt       := null;
      l_detail_line   := null;
      r_per           := null;
      r_par           := null;
      r_rkg           := null;
      r_rel           := null;

      stm_util.debug('**** Bepaal Sofinummer ****');
      l_sofinummer    := lpad(bepaal_sofinummer(r_sga),9,0);

      stm_util.debug('**** Check Sofinummer ('||l_sofinummer||') numeriek ****');
      begin
        l_test_sofi:= l_sofinummer;
      exception
      when others
      then
        l_orig_sofinum:= l_sofinummer;
        l_sofinummer:= 1; -- valt dan uit
      end;

      stm_util.debug('**** Bepaal persoonsgegevens voor sofinummer '||l_sofinummer||' ****');
      open c_per(l_sofinummer);
      fetch c_per into r_per;
      close c_per;

      --stm_util.debug('**** Alleen verwerken indien relatie('||r_per.relatienummer||' gevonden****';
      if r_per.relatienummer is not null
      then
        stm_util.debug('**** Bepaal relatienummerpartner voor relatienummer '||r_per.relatienummer||' ****');
        open c_rkg(r_per.relatienummer);
        fetch c_rkg into r_rkg;
        close c_rkg;

        if g_partner_gegevens
        then
          stm_util.debug('**** Bepaal persoonsgegevens partner voor relatienummerpartner '||r_rkg.relnr_partner||' ****');
          open c_par(r_rkg.relnr_partner);
          fetch c_par into r_par;
          close c_par;
        end if;

        if g_overige_gegevens
        then
          stm_util.debug('**** Bepaal overige persoonsgegevens voor relatienummer '||r_per.relatienummer||' ****');
          open c_rel(r_per.relatienummer);
          fetch c_rel into r_rel;
          close c_rel;
        end if;

        if g_adres_gegevens
        then
          stm_util.debug('**** Bepaal adresgegevens voor relatienummer '||r_per.relatienummer||' ****');

          --Bepaal de default opdrachtgever
          l_opdrachtgever := to_number( stm_algm_get.sysparm( 'PMA'
                                                            , 'OPDR_GEVER'
                                                            , sysdate));
          -- Bepaal het default produkt
          l_produkt := stm_algm_get.sysparm( 'PMA'
                                           , 'PRODUKT'
                                           , sysdate
                                           );
          rle_get_adrsvelden(l_opdrachtgever
                            ,l_produkt
                            ,r_per.relatienummer
                            ,'PR'
                            ,'DA'
                            ,sysdate
                            ,l_postcode
                            ,l_huisnr
                            ,l_toevnum
                            ,l_woonplaats
                            ,l_straat
                            ,l_codland
                            ,l_locatie
                            ,l_ind_woonboot
                            ,l_ind_woonwagen
                            ,l_landnaam
                            ,l_begindatum
                            ,l_einddatum);
        end if;

        stm_util.debug('**** Loop door de opgegeven rubrieknummers en haal de bijbehorende data uit RLE ****');
        for i in nvl ( sort_tab_rle_syn_gba.first, 0) .. nvl ( sort_tab_rle_syn_gba.last, -1) loop

         l_waarde:=null;

         if sort_tab_rle_syn_gba(i) = '01.01.10'   -- A nummer
         then
           open c_ext (r_per.relatienummer,'GBA');
           fetch c_ext into l_waarde;
           close c_ext;
         elsif sort_tab_rle_syn_gba(i) = '01.01.20' -- BSN
         then
           l_waarde:= l_sofinummer;
         elsif sort_tab_rle_syn_gba(i) = '01.02.10' -- Voornamen
         then
           l_waarde:= r_per.voornaam;
         elsif sort_tab_rle_syn_gba(i) = '01.02.30' -- Voorvoegsel
         then
           l_waarde:= r_per.voorvoegsels;
         elsif sort_tab_rle_syn_gba(i) = '01.02.40' -- Geslachtsnaam
         then
           l_waarde:= r_per.Naamrelatie;
         elsif sort_tab_rle_syn_gba(i) = '01.03.10' -- Geboortedatum
         then
           if r_per.code_fictief = 1
           then
             l_waarde := substr (r_per.datgeboorte,1,6)||'00' ;
           elsif r_per.code_fictief = 2
           then
             l_jaar   := substr (r_per.datgeboorte,1,4);
             l_dag    := substr (r_per.datgeboorte,7,2);
             l_waarde := l_jaar||'00'||l_dag;
           elsif r_per.code_fictief = 3
           then
             l_jaar   := substr (r_per.datgeboorte,1,4);
             l_waarde := l_jaar||'0000';
           elsif r_per.code_fictief = 4
           then
             l_waarde := '0000'||substr (r_per.datgeboorte,5,4);
           elsif r_per.code_fictief = 5
           then
             l_maand  := substr (r_per.datgeboorte,5,2);
             l_waarde := '0000'||l_maand||'00';
           elsif r_per.code_fictief = 6
           then
             l_waarde := '000000'|| substr (r_per.datgeboorte,7,2);
           elsif r_per.code_fictief = 7
           then
             l_waarde := '00000000';
           else
             l_waarde:= r_per.datgeboorte;
           end if;
           --
         elsif sort_tab_rle_syn_gba(i) = '01.04.10' -- Geslachtsaanduiding
         then
           l_waarde:= r_rel.geslacht;
         elsif sort_tab_rle_syn_gba(i) = '01.61.10' -- Aanduiding naamgebruik
         then
           l_waarde:= r_rel.code_naamgebruik;
         elsif sort_tab_rle_syn_gba(i) = '04.05.10' -- Nationaliteit
         then
           open c_nat(r_per.relatienummer);
           fetch c_nat into l_waarde;
           close c_nat;
         elsif sort_tab_rle_syn_gba(i) = '05.01.10' -- A nummer partner
         then
           open c_ext (r_rkg.relnr_partner,'GBA');
           fetch c_ext into l_waarde;
           close c_ext;
         elsif sort_tab_rle_syn_gba(i) = '05.01.20' -- BSN partner
         then
           l_waarde:=null; -- Voor de verwerking van de partner gegevens (RLEPRC64) hebben we dit sofinummer nodig. Daarom verschil forceren zodat deze wordt teruggekoppeld door GBA.

         elsif sort_tab_rle_syn_gba(i) = '05.02.10' -- Voornamen partner
         then
           l_waarde:= r_par.voornaam;

         elsif sort_tab_rle_syn_gba(i) = '05.02.30' -- Voorvoegsel partner
         then
           l_waarde:=null; -- Voor de verwerking van de relatie gegevens (RLEPRC64) hebben we voorvoegsel partner nodig. Daarom verschil forceren zodat deze wordt teruggekoppeld door GBA.

         elsif sort_tab_rle_syn_gba(i) = '05.02.40' -- Geslachtsnaam partner
         then
           l_waarde:=null; -- Voor de verwerking van de relatie gegevens (RLEPRC64) hebben we geslachtsnaam partner nodig. Daarom verschil forceren zodat deze wordt teruggekoppeld door GBA.

         elsif sort_tab_rle_syn_gba(i) = '05.03.10' -- Geboortedatum partner
         then
           if r_par.code_fictief = 1
           then
             l_waarde := substr (r_par.datgeboorte,1,6)||'00' ;
           elsif r_par.code_fictief = 2
           then
             l_jaar   := substr (r_par.datgeboorte,1,4);
             l_dag    := substr (r_par.datgeboorte,7,2);
             l_waarde := l_jaar||'00'||l_dag;
           elsif r_par.code_fictief = 3
           then
             l_jaar   := substr (r_par.datgeboorte,1,4);
             l_waarde := l_jaar||'0000';
           elsif r_par.code_fictief = 4
           then
             l_waarde := '0000'||substr (r_par.datgeboorte,5,4);
           elsif r_par.code_fictief = 5
           then
             l_maand  := substr (r_par.datgeboorte,5,2);
             l_waarde := '0000'||l_maand||'00';
           elsif r_par.code_fictief = 6
           then
             l_waarde := '000000'|| substr (r_par.datgeboorte,7,2);
           elsif r_par.code_fictief = 7
           then
             l_waarde := '00000000';
           else
             l_waarde:= r_par.datgeboorte;
           end if;
           --
         elsif sort_tab_rle_syn_gba(i) = '05.06.10' -- Datum sluiting
         then
           l_waarde:= r_rkg.dat_begin;
         elsif sort_tab_rle_syn_gba(i) = '05.07.10' -- Datum ontbinding
         then
           l_waarde:= null; -- Voor de verwerking van de partner gegevens, bepaling exen (RLEPRC64)
         elsif sort_tab_rle_syn_gba(i) = '06.08.10' -- Datum overlijden
         then
           l_waarde:= r_per.datum_overlijden;
         elsif sort_tab_rle_syn_gba(i) = '08.11.10' -- Straatnaam
         then
           l_waarde:= l_straat;
         elsif sort_tab_rle_syn_gba(i) = '08.11.20' -- Huisnummer
         then
           l_waarde:= l_huisnr;
        -- aanpassing xsw 28-7-2014 Haal de waarde van het veld toevnum op.
        -- De waarde voor de spatie wordt toegevoegd aan rubriek 08.11.30
        -- de waarde na de spatie wordt toegevoegd aan rubriek 08.11.40.
        -- De spatie wordt niet meegenomen. Als er geen spatie aanwezig is, plaats dan de inhoud van het veld onder de huisnummertoevoeging (08.11.40).
         elsif sort_tab_rle_syn_gba(i) = '08.11.30' -- Huisletter
         then
           l_tvn_waarde := ltrim(l_toevnum);
           --
           if length(l_tvn_waarde) = 1 and not regexp_like(l_tvn_waarde, '[0-9]')
           then
             l_waarde := l_tvn_waarde;
           elsif length(l_tvn_waarde) > 1 and instr(l_tvn_waarde,' ')> 0
           then
             l_waarde := substr(l_tvn_waarde,1,1);
           else
             l_waarde := null;
          end if;
         elsif sort_tab_rle_syn_gba(i) = '08.11.40' -- Huisnummertoevoeging
         then
           l_tvn_waarde := ltrim(l_toevnum);
           --
           if length(l_tvn_waarde) = 1 and regexp_like(l_tvn_waarde, '[0-9]')
           then
             l_waarde := l_tvn_waarde;
           elsif length (l_tvn_waarde) > 1 and instr(l_tvn_waarde,' ')> 0
           then
             l_waarde := substr(l_tvn_waarde,3,5);
           else
             if length(l_tvn_waarde)> 1 and instr(l_tvn_waarde,' ') = 0
             then
               l_waarde:= l_tvn_waarde;
             end if;
          end if;
         --
         elsif sort_tab_rle_syn_gba(i) = '08.11.60' -- Postcode
         then
           l_waarde:= l_postcode;
         elsif sort_tab_rle_syn_gba(i) = '08.11.70' -- Woonplaatsnaam
         then
           l_waarde:= initcap(l_woonplaats);
         elsif sort_tab_rle_syn_gba(i) = '08.13.10' -- Land waarnaar vertrokken
         then
           if l_codland = 'NL'
           then
             l_waarde:= null;
           else
             syn_transformatie.land_code ( p_land_code_new => l_codland
                                         , p_land_code_old => l_waarde
                                         );
           end if;
         elsif sort_tab_rle_syn_gba(i) = '08.13.20' -- Datum vetrek uit Nederland
         then
           l_waarde:= case l_codland when 'NL' then null else to_char(l_begindatum,'yyyymmdd')end;
         end if;

         stm_util.debug('**** Voeg rubrieknummer: '||sort_tab_rle_syn_gba(i)||' met rubriekwaarde: '||l_waarde ||' toe aan de detailregel ****');
         maak_detail_regel (p_nr     => i,
                            p_waarde => l_waarde,
                            p_line   => l_detail_line);

        end loop;
        stm_util.debug('**** Voeg detailregel toe aan het bestand ****');
        utl_file.put_line ( g_bestand,convert(l_detail_line, 'UTF8', 'WE8MSWIN1252'));

        l_verwerkt:= l_verwerkt + 1;
      else
        l_niet_verwerkt:= l_niet_verwerkt + 1;

        if r_per.relatienummer is null
        then
          stm_util.debug('**** Uitval: Voor sofinummer '||l_sofinummer||' Persoon niet gevonden ****');
          lijst(l_sofinummer||'                    Persoon niet gevonden');
        end if;

      end if;
    end loop;

    utl_file.fclose(g_bestand);

    -- Schrijven logbestand
    lijst('');
    lijst('Aantal aangeboden: ' ||to_char (l_verwerkt + l_niet_verwerkt));
    lijst('Verwerkt         : '||to_char (l_verwerkt));
    lijst('Niet verwerkt    : '||to_char (l_niet_verwerkt));
    lijst('');
    --
  end if;

  lijst ('Bestand '||'RLEPRC62_RLE_SYN_GBA_'||g_script_id||'.csv aangemaakt in de RLE inbox op de ftp pool!' );
  --  Voettekst van lijsten
  lijst ( '********** EINDE VERSLAG Aanmaken synchronisatie bestand RLE => GBA **********'|| ' EINDDATUM: ' || to_char (sysdate,'dd-mm-yyyy hh24:mi:ss'));
  stm_util.debug ('Einde logverslag');
  --

  stm_util.insert_job_statussen(g_volgnummer, 'S', '0');
exception
  when others then
    stm_util.t_foutmelding := sqlerrm;
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(g_volgnummer, 'S', '1');
    stm_util.insert_job_statussen(g_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                  ' procedure : ' || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null then
      stm_util.insert_job_statussen(g_volgnummer,
                                    'I',
                                    stm_util.t_foutmelding);
    end if;
    --
    if stm_util.t_tekst is not null then
      stm_util.insert_job_statussen(g_volgnummer, 'I', stm_util.t_tekst);
    end if;
end;

END RLE_SYN_BESTAND_GBA;
/