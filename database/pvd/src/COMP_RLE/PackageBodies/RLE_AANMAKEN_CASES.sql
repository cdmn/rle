CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_AANMAKEN_CASES IS

  /**************************************************************************************
   NAAM:  RLE_AANMAKEN_CASES
   DOEL:  Deze functie maakt o.b.v. een CSV-bestand met persoonsnummers voor elk persoonsnummer een case
   aan in de werkbak "SPECIAAL" van de basisadministratie.
   Cases worden aangemaakt onder vermelding van een in het CSV-bestand opgegeven opmerking. Functie RLE1021


   WIJZIGINGSHISTORIE:

   Datum             Wie                        Wat
   ----------        -------------              ------------------------------------
   16-04-2014        P. van Deursen             Creatie
   10-06-2014        P. van Deursen             In verwerk l_stack legen voor end loop
   25-03-2015         Monique v Alphen       chg 405614: Aanpassingen ivm niet kunnen archiveren
 ********************************************************************************************************/

 cn_package  constant varchar2( 30 ) := 'RLE_AANMAKEN_CASES';


 -- voor stm_lijsten
 g_regelnummer_lijst           pls_integer    := 1;

PROCEDURE LIJST
 (P_TEXT IN VARCHAR2
 );


PROCEDURE LIJST
 (P_TEXT IN VARCHAR2
 )
 IS
cn_lijst   constant pls_integer := 1;
begin

  stm_util.insert_lijsten( cn_lijst
                         , g_regelnummer_lijst
                         , p_text
                          );

  stm_util.debug( 'LIJST :' || p_text );

end lijst;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 )
 IS

cursor c_rbc (b_herstart_persnr number)
is
  select rbc.persoonsnummer
       , rbc.documentsoort
       , rbc.opmerking
  from rle_bestand_cases rbc
  where rbc.persoonsnummer > b_herstart_persnr
  order by rbc.persoonsnummer asc
;

cursor c_rie (b_relatienr rle_relaties.numrelatie%type
             ,b_adm_code  rle_relatierollen_in_adm.adm_code%type
              )
is
  select adm_code
  from rle_relatierollen_in_adm rie
  where rie.rle_numrelatie = b_relatienr
  and   rie.adm_code = b_adm_code
;

cursor c_oda (b_documentsrt  opu_documenttypen.srt%type)
is
  select *
  from opu_doc_per_administratie oda
  where oda.dty_srt = b_documentsrt
;

  r_rbc                   c_rbc%rowtype;
  r_rie                   c_rie%rowtype;
  r_oda                   c_oda%rowtype;

  l_volgnummer            pls_integer := 1; -- volgnummer voor job_statussen
  l_herstart_persnr       number := 0;

  l_relatienr             rle_relaties.numrelatie%type;
  l_persnr_vorig          number;
  l_adm_vorig             rle_administraties.code%type;
  l_adm                   rle_administraties.code%type;
  l_documentsoort         opu_doc_per_administratie.dty_srt%type;
  l_doorgaan              boolean;

  -- Aanmaken klantcontact
  l_kct_id                kct_klantcontacten.id%type;
  l_kct_status            number;
  l_kct_melding           varchar2(4000);
  l_kct_inhoud            varchar2(4000);

  -- Aanmaken case
  l_stack                 varchar2(2000) := null;
  l_result                varchar2(2000) := null;
  l_melding               varchar2(2000) := null;

  l_aantal_cases          pls_integer:= 0;
  l_aantal_loop_commit    pls_integer:= 0;
   l_aantal                pls_integer:= 0;
begin

  stm_util.module_start     ( cn_package||'.verwerk' );
  stm_util.t_script_naam    := p_script_naam;
  stm_util.t_script_id      := p_script_id;
  stm_util.t_programma_naam := cn_package;

  if stm_util.herstart
  then

    l_herstart_persnr   := stm_util.lees_herstartsleutel ('persoonsnummer' );
    l_aantal_cases      := stm_util.lees_herstarttelling ( 'Aantal aangemaakte cases' );
    l_aantal            := stm_util.lees_herstarttelling ( 'l_aantal');
    g_regelnummer_lijst := stm_util.lees_herstarttelling ( 'Regelnummer' );

    lijst( '*** Herstart *** ' ||'       '|| to_char( sysdate));

  else

    -- Start logverslag
    lijst( '*** Start aanmaken cases op basis van csv bestand *** ' ||'       '|| to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));
    lijst( ' ' );
    lijst( ' ' );
    lijst( 'Het verwerkte bestand is: rle_bestand_cases.csv  ' );
    lijst( '=========================================================================== ' );
    lijst( ' ' );
    lijst( 'De volgende regels konden niet worden verwerkt: ' );
    lijst( ' ' );

  end if;

  for r_rbc in c_rbc (l_herstart_persnr)
  loop

    l_aantal_loop_commit :=  l_aantal_loop_commit + 1;
    l_documentsoort := null;
    l_adm := null;
    l_doorgaan := true;
    l_aantal := l_aantal + 1;

    stm_util.debug('docsrt:'||r_rbc.documentsoort);

    if r_rbc.persoonsnummer is null
    then

      lijst( 'Persoonsnummer is niet gevuld. Case kan niet worden verwerkt.' );
      l_doorgaan := false;

    elsif r_rbc.persoonsnummer is not null
    then

      l_relatienr := comp_ret.ret_rle_prs_converter (p_relatienummer   => null
                                                    ,p_persoonsnummer => r_rbc.persoonsnummer
                                                    );

      stm_util.debug ('l_relatienr: '||l_relatienr);

      if l_relatienr is null
      then

        lijst( 'Persoonsnummer: '||r_rbc.persoonsnummer||' bestaat niet. Hiervoor is geen case aangemaakt.' );
        l_doorgaan := false;

      end if;
    end if;

    if r_rbc.documentsoort is null
      and l_doorgaan
    then

      lijst( 'Persoonsnummer: '||r_rbc.persoonsnummer||' documentsoort is niet gevuld.' );
      l_doorgaan := false;

    elsif r_rbc.documentsoort is not null
      and l_doorgaan
    then

      if r_rbc.documentsoort like 'GBA003%'
      then

        lijst( 'Persoonsnummer: '||r_rbc.persoonsnummer||' documentsoort '||r_rbc.documentsoort||' mag niet worden gebruikt.' );
        l_doorgaan := false;

      else

        open c_oda (b_documentsrt => r_rbc.documentsoort);
        fetch c_oda into r_oda;

        if c_oda%notfound
        then

          lijst( 'Persoonsnummer: '||r_rbc.persoonsnummer||' documentsoort '||r_rbc.documentsoort||' is niet valide.' );
          l_doorgaan := false;

        else

          l_documentsoort := r_oda.dty_srt;
          l_adm           := r_oda.adm_code;

        end if;
        close c_oda;
      end if;
    end if;

    if l_adm is null
      and l_doorgaan
    then

      lijst( 'Persoonsnummer: '||r_rbc.persoonsnummer||' er kan geen administratie worden bepaald bij deze documentsoort.' );
      l_doorgaan := false;

          -- administratie bepalen
    elsif  l_adm is not null
    then

      open c_rie (b_relatienr => l_relatienr
                , b_adm_code  => l_adm
                  );
      fetch c_rie into r_rie;

      if c_rie%notfound
      then

        lijst( 'Persoonsnummer: '||r_rbc.persoonsnummer||' Persoon komt niet voor bij deze administratie.' );
        l_adm := null;
        l_doorgaan := false;

      end if;

      close c_rie;
    end if;

    if r_rbc.persoonsnummer = l_persnr_vorig
      and l_adm_vorig is not null
      and l_adm_vorig = l_adm
      and l_doorgaan
    then

      lijst( 'Persoonsnummer: '||r_rbc.persoonsnummer||' en administratie '||l_adm||' komt dubbel voor. Case met opmerking: '||r_rbc.opmerking||', is niet verwerkt.' );
      l_doorgaan := false;

    end if;

    if l_doorgaan
    then

        -- wegschrijven gegevens uit bestand in klantcontact
      l_kct_inhoud := l_kct_inhoud || chr(10) ||
              'Case aangemaakt vanuit csv-bestand' || chr(10) ||
              '----------------------'  || chr(10) ||
              'Deelnemer          :  '  || r_rbc.persoonsnummer || chr(10) ||
              'Opmerking          :  '  || r_rbc.opmerking      || chr(10);



      -- aanmaken Klantcontact

      kctkct02.KCT_INS_KCT (p_kct_type_relatie         => 'P'
                           ,p_kct_nr_relatie           => l_relatienr
                           ,p_kct_id_medewerker        => 'RLEPRC74'
                           ,p_kct_datum_tijd           => sysdate
                           ,p_kct_indikatie_ink_uitg   =>'I'
                           ,p_kct_aanleiding           => 'Ovrg'
                           ,p_kct_naam_contact         => null
                           ,p_kct_medium               => null
                           ,p_kct_label                => l_adm
                           ,p_kct_oorsprong            => 'DMS'
                           ,p_kct_id_oorsprong         => null
                           ,p_kct_vervaldatum          => null
                           ,p_kct_reden_vervallen      => null
                           ,p_owp_externe_code         => 'ONB'
                           ,p_owp_type_betrokkene      =>  'P'
                           ,p_owp_relatienr_betrokkene => l_relatienr
                           ,p_owp_resultaat            => 'rle04'
                           ,p_owp_inhoud               => l_kct_inhoud
                           ,p_kct_id                   => l_kct_id        -- out
                           ,p_kct_status               => l_kct_status    -- out
                           ,p_kct_melding              => l_kct_melding   -- out
                           );

      if l_kct_id is not null
      then

        stm_add_word (l_stack, 'KCT_ID');
        stm_add_word (l_stack, l_kct_id);
        stm_add_word (l_stack, 'KCT_VOLGNUMMER');
        stm_add_word (l_stack, 1);
        stm_add_word (l_stack, 'ADM');
        stm_add_word (l_stack, l_adm);
        stm_add_word (l_stack, 'DOCUMENTSOORT');
        stm_add_word (l_stack, l_documentsoort );
        -- aanmaken case

        comp_pma.pma_maakcase (p_bron                => 'DMS'
                              ,p_logische_procesnaam => 'Reg/mut persoon - spec. gev.'
                              ,p_ebr_id              => null
                              ,p_document_id         => null
                              ,p_volgnr              => null
                              ,p_werkgevernr         => null
                              ,p_persoonsnr          => to_char(r_rbc.persoonsnummer)
                              ,p_event_id            => null
                              ,p_parameterreeks      => l_stack   -- in/out
                              ,p_result              => l_result  -- out
			              					,p_ind_commit          => 'N'
                              );


        if l_result = 'Verwerkt'
        then

          l_aantal_cases := l_aantal_cases + 1;

        else

          l_melding := 'Het aanmaken van een case voor persoonsnummer '||r_rbc.persoonsnummer||CHR(10)||
                      ' is fout gegaan.'||CHR(10)||'Melding :'||l_result;
                      lijst ('***  '|| l_melding );


        end if; -- result = verwerkt

      end if; -- kct_id is gevuld


      l_persnr_vorig := r_rbc.persoonsnummer;
      l_adm_vorig    := l_adm;

      end if;

    if l_aantal_loop_commit > 100
    then

      l_aantal_loop_commit := 0;

       -- Gegevens voor herstart wegschrijven
      stm_util.schrijf_herstartsleutel ( p_sleutel_naam => 'persoonsnummer'
                                       , p_sleutel_waarde => r_rbc.persoonsnummer
                                          );

      stm_util.schrijf_herstarttelling ( 'Aantal aangemaakte cases' , l_aantal_cases );

      stm_util.schrijf_herstarttelling ( 'l_aantal' , l_aantal);

      stm_util.schrijf_herstarttelling ( 'Regelnummer',  g_regelnummer_lijst);

      commit;

    end if; -- aantal_loop_commit > 100


    -- legen van kct_inhoud en stack

    l_kct_inhoud := null;
    l_stack := null;

  end loop;


  -- Afronden logverslag

  lijst( ' ' );
  lijst( 'Aantal records aangeboden: '||l_aantal );
  lijst( 'Aantal cases aangemaakt: '||l_aantal_cases );
  lijst( ' ' );
  lijst( ' ' );
  lijst ( ' *** Einde aanmaken cases op basis van csv bestand ***'||'       '|| to_char (sysdate, 'dd-mm-yyyy hh24:mi:ss'));





   -- Als verwerking goed is gegaan herstartgegevens verwijderen en stm_job_statussen vullen
   -- stm_util.verwijder_herstart;
  stm_util.verwijder_herstart;
  stm_util.insert_job_statussen ( l_volgnummer
                                , 'S'
                                , '0'
                                 );

 -- commit;

  stm_util.module_end;

  exception
    when others
    then
      ROLLBACK;
      stm_util.insert_job_statussen ( l_volgnummer, 'S', '1' );
      stm_util.insert_job_statussen ( l_volgnummer, 'I', stm_util.t_programma_naam || ' procedure : ' || stm_util.t_procedure );
      stm_util.insert_job_statussen ( l_volgnummer, 'I', stm_util.t_programma_naam || ' SQL error : ' || substr ( sqlerrm, 1, 200 ));

  commit;

end verwerk;
END RLE_AANMAKEN_CASES;
/