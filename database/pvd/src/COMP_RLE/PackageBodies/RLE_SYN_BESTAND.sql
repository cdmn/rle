CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_SYN_BESTAND IS
--------------------------------------------------------------
  -- Doel:een bestand aanmaken voor T&T met daarin de personen waarvan
  -- wij vinden dat deze een afnemers indicatie JA moeten en mogen hebben.
  -- Het bestand voor de populatievergelijking bestaat uit een header met
  -- daarin de rubrieknummers  010110 (voor het A-nummer) en  Rubrieknummer 010120 (voor het BSN).
  -- De kolommmen zijn gescheiden door ; (puntkomma). De volgorde is A-nummer, BSN oplopend.
--------------------------------------------------------------
  -- Wijzigingshistorie
  -- Wanneer    Wie Wat
--------------------------------------------------------------
  -- 09-07-2013 Rudie Siwpersad   creatie
  -- 29-10-2013 XCW Diverse aanpassingen nav Requirements versie 5
  -- 27-12-2013 WHR aanpassing ivm ORU-10027: buffer overflow bij gebruik "p" procedure
  -- 30-12-2013 WHR aanpassing volgorde cursoren ivm performance
  -- 06-02-2014 WHR cursor c_dnp aanpassing selectie op ret_aanspraken
  --                performance verbetering doordat meer personen in de cursor gevonden worden
  --                hierdoor minder vaak doorrekenen totale aanspraken
  -- 18-02-2014 WHR cursor c_tzg toegevoegd selectie op ret_v_geldige_toezeggingen
  --                performance verbetering doordat meer personen in deze cursor gevonden worden
  --                hierdoor minder vaak doorrekenen totale aanspraken
  -- 13-03-2014 WHR HERBA deel 2
-------------------------------------------------------------

-- global t.b.v. logverslag
g_lijstnummer        number        := 1;
g_regelnr            number        := 0;
g_msg_error          number        := 0;
g_loglijst           utl_file.file_type;
g_code_gewenste_ind  rle_synchronisatie_statussen.rle_code_gewenste_ind%type;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 ,P_OUTPUT_TYPE IN varchar2 := 'M'
 );
FUNCTION CHECK_GEBEURTENIS
 (P_RGG IN varchar2
 ,P_SGS IN varchar2
 ,P_PEIL IN date
 )
 RETURN BOOLEAN;
PROCEDURE GET_AANSPRAKEN
 (P_RGG IN varchar2
 ,P_REL IN ret_deelnemerschappen.rle_numrelatie%type
 ,P_SOORT_GEBEURTENIS IN varchar2
 ,P_REKENDATUM IN date
 ,P_PEILDATUM IN date
 ,P_OUT_OP OUT number
 ,P_OUT_NP OUT number
 ,P_OUT_VP OUT number
 ,P_OUT_WZP OUT number
 ,P_OUT_PRP OUT number
 ,P_OUT_ANW OUT number
 ,P_OUT_PSP OUT number
 );
PROCEDURE P_AFHANDELEN_REL
 (P_RGG_CODE IN ovt_dd_periodes.rgg_code%type
 ,P_RELATIE IN ovt_dd_periodes.rle_numrelatie%type
 ,P_AANSPRAKEN OUT number
 );
PROCEDURE BEPAAL_AFNEMERSINDICATIE
 (P_NUMRELATIE IN rle_relaties.numrelatie%type
 ,P_PERSOONSNUMMER IN rle_relatie_externe_codes.extern_relatie%type
 ,P_DATOVERLIJDEN IN date
 ,P_MAANDEN_OVERLEDEN IN number
 ,P_DATUM_BEEINDIGD_NA_JO IN date
 );


PROCEDURE LIJST
 (P_TEXT IN varchar2
 ,P_OUTPUT_TYPE IN varchar2 := 'M'
 )
 IS
begin
  if p_output_type = 'F' -- (F) File
  then
    utl_file.put_line ( g_loglijst
                       ,p_text
                       );
  else -- M (Mail)
    stm_util.insert_lijsten( g_lijstnummer
                           , g_regelnr
                           , p_text
                           );
  end if;
end lijst;
FUNCTION CHECK_GEBEURTENIS
 (P_RGG IN varchar2
 ,P_SGS IN varchar2
 ,P_PEIL IN date
 )
 RETURN BOOLEAN
 IS

    l_dummy_var         pls_integer;
begin
    l_dummy_var      :=
      pdt_rgs.geef_id ( p_rgg_code       => p_rgg
                      , p_sgs_code       => p_sgs
                      , p_peildatum      => p_peil );

    if l_dummy_var is not null
    then
      return true;
    else
      return false;
    end if;
  exception
    when others
    then
      return false;
  end check_gebeurtenis;
PROCEDURE GET_AANSPRAKEN
 (P_RGG IN varchar2
 ,P_REL IN ret_deelnemerschappen.rle_numrelatie%type
 ,P_SOORT_GEBEURTENIS IN varchar2
 ,P_REKENDATUM IN date
 ,P_PEILDATUM IN date
 ,P_OUT_OP OUT number
 ,P_OUT_NP OUT number
 ,P_OUT_VP OUT number
 ,P_OUT_WZP OUT number
 ,P_OUT_PRP OUT number
 ,P_OUT_ANW OUT number
 ,P_OUT_PSP OUT number
 )
 IS

    l_registratiedatum  date := null;
    l_reden_vaststelling varchar2 ( 1000 ) := null;
    l_gebruikte_peildatum date;                                                                                   -- out
    l_gebruikte_registratiedatum date;                                                                            -- out
    l_gebruikte_reden_vaststelling varchar2 ( 1000 );                                                             -- out
    l_datum_10_jaren_eis date;                                                                                    -- out
    l_ind_mi_mtb        varchar2 ( 1000 );                                                                        -- out
    l_datum_5_jaren_eis date;                                                                                     -- out
    l_foutmelding       varchar2 ( 1000 );                                                                        -- out
    l_tab_aak           t_aanspraak_type := null;
    l_bedrag            number;
  --
begin
    stm_util.debug ( '  Get aanspraken' );
    p_out_op         := 0;
    p_out_np         := 0;
    p_out_vp         := 0;
    p_out_wzp        := 0;
    p_out_prp        := 0;
    p_out_anw        := 0;
    p_out_psp        := 0;
    ret_aak.verzamelen_aanspraken ( p_soort_gebeurtenis            => p_soort_gebeurtenis
                                  , p_regeling                     => p_rgg
                                  , p_relatienummer                => p_rel
                                  , p_rekendatum                   => p_rekendatum
                                  , p_peildatum                    => p_peildatum
                                  , p_registratiedatum             => l_registratiedatum
                                  , p_reden_vaststelling           => l_reden_vaststelling
                                  , p_gebruikte_peildatum          => l_gebruikte_peildatum
                                  , p_gebruikte_registratiedatum   => l_gebruikte_registratiedatum
                                  , p_gebruikte_reden_vaststelling => l_gebruikte_reden_vaststelling
                                  , p_datum_10_jaren_eis           => l_datum_10_jaren_eis   -- optioneel
                                  , p_ind_mi_mtb                   => l_ind_mi_mtb           -- optioneel
                                  , p_datum_5_jaren_eis            => l_datum_5_jaren_eis    -- optioneel
                                  , p_foutmelding                  => l_foutmelding          -- optioneel
                                  , p_tab_aak                      => l_tab_aak
                                  );
    stm_util.debug ( 'l_gebruikte_peildatum          = ' || to_char ( l_gebruikte_peildatum, 'dd-mon-yyyy' ) );
    stm_util.debug ( 'l_gebruikte_registratiedatum   = ' || to_char ( l_gebruikte_registratiedatum, 'dd-mon-yyyy' ) );
    stm_util.debug ( 'l_gebruikte_reden_vaststelling = ' || l_gebruikte_reden_vaststelling );
    stm_util.debug ( 'l_datum_10_jaren_eis           = ' || to_char ( l_datum_10_jaren_eis, 'dd-mon-yyyy' ) );
    stm_util.debug ( 'l_ind_mi_mtb                   = ' || l_ind_mi_mtb );
    stm_util.debug ( 'l_datum_5_jaren_eis            = ' || to_char ( l_datum_5_jaren_eis, 'dd-mon-yyyy' ) );
    stm_util.debug ( 'l_foutmelding                  = ' || l_foutmelding );
    stm_util.debug ( ' ***' );
    stm_util.debug ( 'Aantal producten               = ' || to_char ( l_tab_aak.count ) );

    for i in 1 .. l_tab_aak.count
    loop
      stm_util.debug ( '*************' );
      stm_util.debug ( 'i                : ' || to_char ( i ) );
      stm_util.debug ( 'PRODUCTGROEP     : ' || l_tab_aak ( i ).productgroep );
      stm_util.debug ( 'PRODUCT          : ' || l_tab_aak ( i ).product );
      stm_util.debug ( 'SOORT_BEDRAG     : ' || l_tab_aak ( i ).soort_bedrag );
      stm_util.debug ( 'BEDRAG           : ' || to_char ( l_tab_aak ( i ).bedrag ) );
      stm_util.debug ( 'IND_HERSCHIKBAAR : ' || l_tab_aak ( i ).ind_herschikbaar );
      stm_util.debug ( 'HERKOMST         : ' || l_tab_aak ( i ).herkomst );
      stm_util.debug ( 'DATUM_BEGIN      : ' || to_char ( l_tab_aak ( i ).datum_begin, 'dd-mon-yyyy' ) );
      stm_util.debug ( 'DATUM_EINDE      : ' || to_char ( l_tab_aak ( i ).datum_einde, 'dd-mon-yyyy' ) );
      stm_util.debug ( 'BEGUNSTIGDE      : ' || to_char ( l_tab_aak ( i ).begunstigde ) );
      l_bedrag := l_bedrag + l_tab_aak ( i ).bedrag;
      --
      case l_tab_aak ( i ).productgroep
        when 'OP'
        then
          p_out_op         := p_out_op + l_tab_aak ( i ).bedrag;
        when 'NP'
        then
          p_out_np         := p_out_np + l_tab_aak ( i ).bedrag;
        when 'VP'
        then
          p_out_vp         := p_out_vp + l_tab_aak ( i ).bedrag;
        when 'WZP'
        then
          p_out_wzp        := p_out_wzp + l_tab_aak ( i ).bedrag;
        when 'PRP'
        then
          p_out_prp        := p_out_prp + l_tab_aak ( i ).bedrag;
        when 'ANW'
        then
          p_out_anw        := p_out_anw + l_tab_aak ( i ).bedrag;
        when 'PSP'
        then
          p_out_psp        := p_out_psp + l_tab_aak ( i ).bedrag;
        else
          stm_util.debug ( ' PRODUCT GROEP NIET AFGEHANDELD : ' || l_tab_aak ( i ).productgroep );
      end case;
    --
    end loop;
  --
  end get_aanspraken;
PROCEDURE P_AFHANDELEN_REL
 (P_RGG_CODE IN ovt_dd_periodes.rgg_code%type
 ,P_RELATIE IN ovt_dd_periodes.rle_numrelatie%type
 ,P_AANSPRAKEN OUT number
 )
 IS

    --
    cursor c_rel ( b_rel ovt_dd_periodes.rle_numrelatie%type )
    is
      select r.persoonsnummer
           , r.datum_overlijden
           , r.geboortedatum
           , trunc ( add_months ( r.geboortedatum, 65 * 12 ), 'MM' ) as pensioendatum
           , trunc ( add_months ( r.geboortedatum, 60 * 12 ), 'MM' ) as vp_datum
      from   rle_mv_personen r
      where  r.relatienummer = b_rel;
    r_rel               c_rel%rowtype;
    --
    cursor c_peildatum ( b_rgg                varchar2
                       , b_rle                ret_deelnemerschappen.rle_numrelatie%type
                       , b_pensioendatum      date )
    is
      select peil.peildatum
      from   ret_deelnemerschappen dnp
           , ret_aanspraken peil
      where  dnp.rgg_code = b_rgg
      and    dnp.rle_numrelatie = b_rle
      and    dnp.id = peil.dnp_id
      and    peil.peildatum < to_date ('31-12', 'DD-MM')  --( '20111231', 'yyyymmdd' )
      and    peil.peildatum < b_pensioendatum
      order by peil.peildatum desc nulls last
             , peil.registratiemoment desc;
    --
    l_stand_op          number := 0;
    l_stand_np          number := 0;
    l_stand_vp          number := 0;
    l_stand_wzp         number := 0;
    l_stand_prp         number := 0;
    l_stand_anw         number := 0;
    l_stand_psp         number := 0;
    l_gebeurtenis       varchar2 ( 500 ) := null;
    l_berekeningsdatum  date;
    l_peildatum         date;
    --
    l_melding           varchar2 ( 1000 ) := null;
begin
    stm_util.debug ( 'p_afhandelen_rel' );
    stm_util.debug ( 'p_relatie         : ' || to_char ( p_relatie ) );
    stm_util.debug ( 'p_rgg_code        : ' || p_rgg_code );
    --
    open  c_rel ( b_rel => p_relatie );
    fetch c_rel
    into  r_rel;
    close c_rel;

    --
    stm_util.debug ( 'persoonsnummer    : ' || r_rel.persoonsnummer );
    stm_util.debug ( 'pensioendatum     : ' || to_char ( r_rel.pensioendatum, 'dd-mon-yyyy' ) );
    stm_util.debug ( 'datum_overlijden  : ' || to_char ( r_rel.datum_overlijden, 'dd-mon-yyyy' ) );
    --
    open c_peildatum ( b_rgg           => p_rgg_code
                     , b_rle           => p_relatie
                     , b_pensioendatum => r_rel.pensioendatum
                     );
    fetch c_peildatum
    into  l_peildatum;

    if c_peildatum%notfound
    then
      l_peildatum  := to_date ( '1231', 'mmdd' );
    end if;
    close c_peildatum;
    stm_util.debug ( 'l_peildatum       : ' || to_char ( l_peildatum, 'dd-mon-yyyy' ) );
    --
    ret_bepaal_alle_opbouwperioden
      ( p_relatienummer => p_relatie
      , p_regeling      => p_rgg_code
      );
    --
    ret_gebeurtenis.bepaal_code_en_datum
      ( p_regeling              => p_rgg_code
      , p_relatienummer         => p_relatie
      , p_gekozen_peildatum     => l_peildatum
      , p_out_soort_gebeurtenis => l_gebeurtenis
      , p_out_datum_gebeurtenis => l_berekeningsdatum
      , p_out_melding           => l_melding
      );
    --
    stm_util.debug ( 'l_gebeurtenis      : ' || l_gebeurtenis );
    stm_util.debug ( 'l_berekeningsdatum : ' || to_char ( l_berekeningsdatum, 'dd-mon-yyyy' ) );
    stm_util.debug ( 'l_melding          : ' || l_melding );
    --
    -- igv. OVLDN mag de peildatum niet groter zijn als de overlijdensdatum.
    if l_gebeurtenis = 'OVLDN'
       and r_rel.datum_overlijden is not null
       and l_peildatum > r_rel.datum_overlijden
    then
      stm_util.debug ( 'Peildatum moet kleiner zijn als de datum van overlijden. ' );
      open  c_peildatum ( b_rgg           => p_rgg_code
                        , b_rle           => p_relatie
                        , b_pensioendatum => r_rel.datum_overlijden
                        );
      fetch c_peildatum
      into  l_peildatum;
      --
      if c_peildatum%notfound
      then
        l_peildatum      := r_rel.datum_overlijden;
      end if;
      close c_peildatum;
      stm_util.debug ( 'Peildatum is geworden : ' || to_char ( l_peildatum, 'dd-mon-yyyy' ) );
    end if;
    --
    if l_peildatum > l_berekeningsdatum
    then
      stm_util.debug ( 'Peildatum is groter als berekeningsdatum. ' );
      stm_util.debug ( 'l_peildatum was = ' || to_char ( l_peildatum, 'dd-mon-yyyy' ) ||
                       ' en wordt nu : '    || to_char ( l_berekeningsdatum - 1, 'dd-mon-yyyy' ) );
      l_peildatum := l_berekeningsdatum - 1;
    end if;
    --
    if l_gebeurtenis is null
       or not check_gebeurtenis
              ( p_rgg  => p_rgg_code
              , p_sgs  => l_gebeurtenis
              , p_peil => l_peildatum - 1 )         -- bv. VOP in 2006
    then
      stm_util.debug ( 'gebeurtenis wordt OP gemaakt.' );
      l_gebeurtenis    := 'OP';
      stm_util.debug ( 'l_berekeningsdatum was ' || to_char ( l_berekeningsdatum, 'dd-mon-yyyy' ) ||
                       ' en wordt : ' || to_char ( r_rel.pensioendatum, 'dd-mon-yyyy' ) );
      l_berekeningsdatum := r_rel.pensioendatum;
    end if;
    --
    if l_gebeurtenis = 'VP'
       and l_berekeningsdatum is null
    then
      stm_util.debug ( 'gebeurtenis is VP en er is geen berekeningsdatum, deze wordt de vroegpensioendatum.' );
      l_berekeningsdatum := r_rel.vp_datum;
      stm_util.debug ( 'l_berekeningsdatum : ' || to_char ( l_berekeningsdatum, 'dd-mon-yyyy' ) );
    end if;
    --
    stm_util.debug ( 'Aanspraken totaal' );
    g_code_gewenste_ind := 'TOT';

    get_aanspraken ( p_rgg               => p_rgg_code
                   , p_rel               => p_relatie
                   , p_soort_gebeurtenis => l_gebeurtenis
                   , p_rekendatum        => l_berekeningsdatum
                   , p_peildatum         => l_peildatum
                   , p_out_op            => l_stand_op
                   , p_out_np            => l_stand_np
                   , p_out_vp            => l_stand_vp
                   , p_out_wzp           => l_stand_wzp
                   , p_out_prp           => l_stand_prp
                   , p_out_anw           => l_stand_anw
                   , p_out_psp           => l_stand_psp );

    p_aanspraken := l_stand_op +l_stand_np + l_stand_vp + l_stand_wzp + l_stand_prp + l_stand_anw + l_stand_psp;

    stm_util.debug (p_rgg_code || ';' || r_rel.persoonsnummer || ';' ||
                       to_char ( l_stand_op,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_op,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_np,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_np,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_vp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_vp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_wzp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_wzp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_prp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_prp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_anw,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_anw,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_psp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( l_stand_psp,'999999999D9999999','NLS_NUMERIC_CHARACTERS=,.' ) || ';' ||
                       to_char ( r_rel.pensioendatum,'DD-MM-YYYY') || ';' ||
                       l_gebeurtenis || ';' ||
                       to_char ( r_rel.datum_overlijden,'DD-MM-YYYY') || ';' ||
                       to_char ( l_berekeningsdatum,'DD-MM-YYYY') || ';' ||
                       to_char ( l_peildatum,'DD-MM-YYYY') );

  exception
  when others
  then
      stm_util.debug ( '-- Error --' );
      stm_util.debug ( dbms_utility.format_error_backtrace );
      stm_util.debug ( sqlerrm );
      stm_util.debug ( '-- Error --' );

  end p_afhandelen_rel;
PROCEDURE BEPAAL_AFNEMERSINDICATIE
 (P_NUMRELATIE IN rle_relaties.numrelatie%type
 ,P_PERSOONSNUMMER IN rle_relatie_externe_codes.extern_relatie%type
 ,P_DATOVERLIJDEN IN date
 ,P_MAANDEN_OVERLEDEN IN number
 ,P_DATUM_BEEINDIGD_NA_JO IN date
 )
 IS


  -- Afstemmingen GBA
  cursor c_asa ( b_rle_numrelatie number )
  is
    SELECT asa.rle_numrelatie
    FROM   rle_afstemmingen_gba asa
    WHERE  asa.rle_numrelatie  = b_rle_numrelatie
    AND    asa.code_gba_status = 'AF';

  -- cursor iedereen die voorkomen in deelnemerschappen met pesioenaanspraak
  cursor c_dnp ( b_rle_numrelatie       number
               , b_datum_beeindig_na_jo date )
  is
    SELECT dnp.rle_numrelatie
    FROM   ret_deelnemerschappen dnp
    WHERE  dnp.rle_numrelatie = b_rle_numrelatie
    AND    EXISTS
           (SELECT 1
            FROM   ret_aanspraken ask
            WHERE  ask.dnp_id = dnp.id
            AND    ask.rgg_code = dnp.rgg_code
            AND    ask.peildatum >= b_datum_beeindig_na_jo
            AND    ( ask.bedrag                  > 0 OR
                     ask.bedrag_voorg_dlna       > 0 OR
                     ask.bedrag_overschot        > 0 OR
                     ask.bedrag_inkomende_waarde > 0
                   )
           )
    ;

  cursor c_tzg ( b_rle_numrelatie number )
  is
    SELECT tzg.rle_numrelatie
    FROM   ret_deelnemerschappen  dnp
          ,ret_ukg_gebeurtenissen ugs
          ,pdt_reg_gebeurtenissen rgs
          ,ret_ukg_varianten      uvt
          ,ret_toezeggingen       tzg
    WHERE  dnp.id = ugs.dnp_id
    AND    ugs.status = 'A'
    AND    ugs.rgs_id = rgs.id
    AND    rgs.sgs_code IN ('ABP', 'VECO')
    AND    ugs.id = uvt.ugs_id
    AND    uvt.status = 'DE'
    AND    uvt.id = tzg.uvt_id
    AND    tzg.rle_numrelatie = b_rle_numrelatie
    ;

  cursor c_ube ( b_rle_numrelatie       number
               , b_datum_beeindig_na_jo date )
  is
    SELECT ube.rle_numrelatie
    FROM   ret_deelnemerschappen  dnp
          ,ret_ukg_gebeurtenissen ugs
          ,pdt_reg_gebeurtenissen rgs
          ,ret_ukg_varianten      uvt
          ,ret_uitkeringen        ukg
          ,ret_ukg_begunstigden   ube
    WHERE  dnp.id = ugs.dnp_id
    AND    ugs.status = 'A'
    AND    ugs.rgs_id = rgs.id
    AND    ugs.id = uvt.ugs_id
    AND    uvt.status = 'DE'
    AND    uvt.id = ukg.uvt_id
    AND    ukg.id = ube.ukg_id
    AND    ( ube.datum_einde IS NULL OR
             ube.datum_einde >= b_datum_beeindig_na_jo
           )
    AND    ube.rle_numrelatie = b_rle_numrelatie
    ;

  -- Geldige toezeggingen
  cursor c_vtzg( b_rle_numrelatie number )
  is
    SELECT vtzg.rle_numrelatie
    FROM   ret_v_geldige_toezeggingen vtzg
    WHERE  vtzg.rle_numrelatie = b_rle_numrelatie
    AND    vtzg.bedrag > 0
    ;

  -- cursor tbv actieve dienstverbanden voor jongeren die nog niet opbouwen */
  cursor c_ddp( b_rle_numrelatie number )
  is
    SELECT ddp.rle_numrelatie
    FROM   ovt_dd_periodes ddp
    WHERE  ddp.rle_numrelatie = b_rle_numrelatie
    AND    ddp.sds_code = 'AV'
    AND    SYSDATE BETWEEN ddp.datum_begin AND nvl(ddp.datum_einde, SYSDATE + 1)
    AND    ddp.datum_vervallen IS NULL
  ;

  -- cursor tbv controle van groep 2: Verzekeringsproducten WIA */
  cursor c_veo( b_persoonsnummer in number
              , b_peildatum      in date )
  is
    SELECT vao.prs_persoonsnummer
    FROM   aos_verzekerd_perioden_ao veo
          ,aos_verzekerden_ao        vao
    WHERE  vao.id = veo.vao_id
    AND    veo.fiatteur IS NOT NULL
    AND    veo.vervalmoment IS NULL
    AND    veo.veo_type = 'GAO'
    AND    veo.ingangsdatum <= b_peildatum
    AND    nvl(veo.einddatum, b_peildatum) >= b_peildatum
    AND    vao.prs_persoonsnummer = b_persoonsnummer
    UNION ALL
    SELECT vao.prs_persoonsnummer
    FROM   aos_polissen_ao           pao
          ,aos_verzekerden_ao        vao
          ,aos_verzekerd_perioden_ao veo
    WHERE  pao.id = vao.pao_id
    AND    vao.id = veo.vao_id
    AND    pao.vzp_nummer IN (50)
    AND    veo.fiatteur IS NOT NULL
    AND    veo.vervalmoment IS NULL
    AND    veo.veo_type = 'GAO'
    AND    veo.ingangsdatum <= b_peildatum
    AND    nvl(veo.einddatum, b_peildatum) >= b_peildatum
    AND    vao.prs_persoonsnummer = b_persoonsnummer
    ;

  -- cursor tbv controle van groep 3: Verzekeringsproducten WAO
  cursor c_toe( b_persoonsnummer in number
              , b_peildatum      in date )
  is
    SELECT asp.prs_persoonsnummer
    FROM   toekenningen_aov toe
          ,aanspraken_aov   asp
    WHERE  asp.aanspraaknummer = toe.asp_aanspraaknummer
    AND    toe.vervaldatum IS NULL
    AND    toe.toekenningsbedrag > 0
    AND    toe.ingangsdatum <= b_peildatum
    AND    nvl(toe.einddatum, b_peildatum) >= b_peildatum
    AND    asp.prs_persoonsnummer = b_persoonsnummer
    ;

  l_gewenste_indicatie  rle_synchronisatie_statussen.rle_gewenste_indicatie%type;
  l_aanspraken          number;
  --
  r_dnp                 c_dnp%rowtype;
  r_tzg                 c_tzg%rowtype;
  r_ube                 c_ube%rowtype;
  r_vtzg                c_vtzg%rowtype;
  r_ddp                 c_ddp%rowtype;
  r_veo                 c_veo%rowtype;
  r_toe                 c_toe%rowtype;
  r_asa                 c_asa%rowtype;
  --
begin
  --
  if  p_datoverlijden < add_months(trunc(SYSDATE, 'mm'), -p_maanden_overleden)
  then
    l_gewenste_indicatie := 'N';
    g_code_gewenste_ind  := 'OVL';
  end if;
  --
  if  l_gewenste_indicatie is null
  then
    open  c_asa ( b_rle_numrelatie => p_numrelatie );
    fetch c_asa
    into  r_asa;

    if c_asa%found
    then
      l_gewenste_indicatie := 'N';
      g_code_gewenste_ind  := 'ASA';
    end if;

    close c_asa;
  end if;
  --
  if l_gewenste_indicatie is null
  then
    open  c_dnp ( b_rle_numrelatie       => p_numrelatie
                , b_datum_beeindig_na_jo => p_datum_beeindigd_na_jo );
    fetch c_dnp
    into  r_dnp;

    if c_dnp%found
    then
      l_gewenste_indicatie := 'J';
      g_code_gewenste_ind  := 'DNP';
    end if;

    close c_dnp;
  end if;
  --
  if  l_gewenste_indicatie is null
  then
    open  c_tzg ( b_rle_numrelatie => p_numrelatie );
    fetch c_tzg
    into  r_tzg;

    if c_tzg%found
    then
      l_gewenste_indicatie := 'J';
      g_code_gewenste_ind  := 'TZG';
    end if;

    close c_tzg;
  end if;
  --
  if  l_gewenste_indicatie is null
  then
    open  c_ube ( b_rle_numrelatie       => p_numrelatie
                , b_datum_beeindig_na_jo => p_datum_beeindigd_na_jo );
    fetch c_ube
    into  r_ube;

    if c_ube%found
    then
      l_gewenste_indicatie := 'J';
      g_code_gewenste_ind  := 'UBE';
    end if;

    close c_ube;
  end if;
  --
  if  l_gewenste_indicatie is null
  and p_persoonsnummer is not null
  then
    open  c_veo ( b_persoonsnummer => p_persoonsnummer
                , b_peildatum      => trunc(sysdate)
                );
    fetch c_veo
    into  r_veo;

    if c_veo%found
    then
      l_gewenste_indicatie := 'J';
      g_code_gewenste_ind  := 'VEO';
    end if;

    close c_veo;
  end if;
  --
  if  l_gewenste_indicatie is null
  and p_persoonsnummer is not null
  then
    open  c_toe ( b_persoonsnummer => p_persoonsnummer
                , b_peildatum      => trunc(sysdate) );
    fetch c_toe
    into  r_toe;

    if c_toe%found
    then
      l_gewenste_indicatie := 'J';
      g_code_gewenste_ind  := 'TOE';
    end if;

    close c_toe;
  end if;
  --
  if  l_gewenste_indicatie is null
  then
    open  c_ddp ( b_rle_numrelatie => p_numrelatie );
    fetch c_ddp
    into  r_ddp;

    if c_ddp%found
    then
      l_gewenste_indicatie := 'J';
      g_code_gewenste_ind  := 'DDP';
    end if;

    close c_ddp;
  end if;
  --
  if  l_gewenste_indicatie is null
  then
    open  c_vtzg ( b_rle_numrelatie => p_numrelatie );
    fetch c_vtzg
    into  r_vtzg;

    if c_vtzg%found
    then
      l_gewenste_indicatie := 'J';
      g_code_gewenste_ind  := 'VTZG';
    end if;

    close c_vtzg;
  end if;
  --
  if l_gewenste_indicatie is null
  then
    g_code_gewenste_ind := 'ASP';

    <<rle2>>
    for r_rle2 in (select rgg_code, rle_numrelatie
                   from   ret_deelnemerschappen
                   where  rle_numrelatie =  p_numrelatie)
    loop
      g_code_gewenste_ind := 'AFH';

      p_afhandelen_rel ( p_rgg_code   => r_rle2.rgg_code
                       , p_relatie    => r_rle2.rle_numrelatie
                       , p_aanspraken => l_aanspraken);

      if l_aanspraken > 0
      then
        l_gewenste_indicatie := 'J';
        -- 30-12-2013 WHR exit wanneer indicatie = J
        exit rle2;
      end if;

    end loop;
  end if;

  -- Indien geen enkel criterium voldoet dan N
  if l_gewenste_indicatie is null then
     l_gewenste_indicatie := 'N';
  end if;

  -- Bijwerken RLE SYNCHRONISATIE STATUS
  rle_update_syn_status
    ( p_numrelatie    => p_numrelatie
    , p_rle_indicatie => l_gewenste_indicatie
    , p_rle_code_ind  => g_code_gewenste_ind
    );

exception
  when others
  then
      -- melding loggen en daarna doorgaan met de volgende relatie
      lijst  (to_char( p_numrelatie)||'; '||substr(sqlerrm,1,350),'F');
      g_msg_error := g_msg_error + 1;

      --sluiten openstaande cursors
      if c_dnp%isopen

      then
        close c_dnp;
      end if;
      --
      if c_tzg%isopen
      then
        close c_tzg;
      end if;
      --
      if c_ube%isopen
      then
        close c_ube;
      end if;
      --
      if c_vtzg%isopen
      then
        close c_vtzg;
      end if;
      --
      if c_ddp%isopen
      then
        close c_ddp;
      end if;
      --
      if c_veo%isopen
      then
        close c_veo;
      end if;
      --
      if c_toe%isopen
      then
        close c_toe;
      end if;
      --
      if c_asa%isopen
      then
        close c_asa;
      end if;
      --
END;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_MAANDEN_OVERLEDEN IN number := 2
 ,P_BEEINDIGD_NA_JO IN number := to_number(to_char(SYSDATE, 'yyyy'))
 )
 IS

-- cursor voor de select van alle relaties
  cursor c_rle ( b_herstart_rle number )
  is
    SELECT rle.numrelatie
          ,(SELECT rec.extern_relatie
            FROM   rle_relatie_externe_codes rec
            WHERE  rec.rle_numrelatie = rle.numrelatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'PRS') AS persoonsnummer
          ,rle.datoverlijden AS datoverlijden
    FROM   rle_relaties rle
    JOIN   rle_relatie_rollen rrl
    ON     rle.numrelatie = rrl.rle_numrelatie
    AND    rrl.rol_codrol = 'PR'
    WHERE  rle.numrelatie > b_herstart_rle
    ORDER  BY rle.numrelatie
    ;

  -- cursor t.b.v. wegschrijven van verzamelde A-nummers en BSN
  cursor c_csv_data
  is
    SELECT (SELECT rec.extern_relatie
            FROM   rle_relatie_externe_codes rec
            WHERE  rec.rle_numrelatie = scs.rle_numrelatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'GBA') AS anummer
          ,(SELECT rec.extern_relatie
            FROM   rle_relatie_externe_codes rec
            WHERE  rec.rle_numrelatie = scs.rle_numrelatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'SOFI') AS bsn
          ,(SELECT rec.extern_relatie
            FROM   rle_relatie_externe_codes rec
            WHERE  rec.rle_numrelatie = scs.rle_numrelatie
            AND    rec.rol_codrol = 'PR'
            AND    rec.dwe_wrddom_extsys = 'PRS') AS persoonsnummer
          ,scs.rle_code_gewenste_ind              AS rle_code_gewenste_ind
    FROM   rle_synchronisatie_statussen scs
    WHERE  scs.rle_gewenste_indicatie = 'J'
    ORDER  BY bsn ASC
    ;

  cursor c_scs
  is
    SELECT CASE rle_code_gewenste_ind
             WHEN 'OVL' THEN
              1
             WHEN 'ASA' THEN
              2
             WHEN 'DNP' THEN
              3
             WHEN 'TZG' THEN
              4
             WHEN 'UBE' THEN
              5
             WHEN 'VTZG' THEN
              6
             WHEN 'VEO' THEN
              7
             WHEN 'TOE' THEN
              8
             WHEN 'DDP' THEN
              9
             WHEN 'ASP' THEN
              10
             WHEN 'AFH' THEN
              11
             WHEN 'TOT' THEN
              12
           END AS nr
          ,rle_code_gewenste_ind
          ,rle_gewenste_indicatie
          ,COUNT(*) AS aantal
          ,SUM(sec) AS sum_sec
    FROM   (SELECT (rle_datum_indicatie - (lag(rle_datum_indicatie) over(ORDER BY rle_numrelatie))) * 60 * 60 * 24 AS sec
                  ,rle_gewenste_indicatie
                  ,rle_code_gewenste_ind
            FROM   rle_synchronisatie_statussen
            where    trunc (rle_datum_indicatie) = trunc (sysdate))
    GROUP  BY rle_gewenste_indicatie
             ,rle_code_gewenste_ind
    ORDER  BY nr
             ,rle_gewenste_indicatie;
  --
  r_rle                   c_rle%rowtype;
  r_csv_data              c_csv_data%rowtype;
  --
  l_herstart_rle          number :=0;
  l_scheiding             varchar2 (10) := ' : ';
  l_descript_lengte       pls_integer   := 70;
  l_volgnummer            stm_job_statussen.volgnummer%type := 0;
  --
  l_teller                number := 0;
  l_database              varchar2(255);
  fh_stoplijst            utl_file.file_type;
  l_bestandsnaam          varchar2(255);
  --
  cn_module               constant varchar2(25) := 'rle_syn_bestand';
  --
  l_datum_beeindigd_na_jo date;
  --
begin
  stm_util.t_procedure       := cn_module;
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_SYN_BESTAND';

  -- bestandsnaam formeren   xcw 15-08-2013 aangepast na opmerking produktiebeheer
  select case
           when regexp_substr ( global_name
                             , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;

  l_bestandsnaam := l_database || p_script_naam
                               || '_'     || 'RLE_SYN_BESTAND'
                               || '_'     || to_char(sysdate, 'yyyymmdd')
                               || '_'     || to_char(p_script_id);

  -- openen van de filehandler
  g_loglijst := utl_file.fopen ( 'RLE_OUTBOX'
                                  , l_bestandsnaam||'_LOG'||'.CSV'
                                  , 'W'
                                  );

  stm_util.debug ('Controle herstart');
  if stm_util.herstart
  then
    stm_util.debug ('Betreft herstart; ophalen herstartsleutel');
    -- Ophalen herstartsleutel
    l_herstart_rle      := to_number (stm_util.lees_herstartsleutel ( 'l_herstart_rle'));
    g_msg_error         := to_number (stm_util.lees_herstarttelling ( 'g_msg_error'));
    --
    stm_util.debug       ('Herstart_rle:'||l_herstart_rle );
    stm_util.debug       ('Herstart_errors:'||g_msg_error );
    --
    g_regelnr := to_number (stm_util.lees_herstarttelling('g_regelnr'));

    -- Schrijf herstart punt en tijd in logverslag
    lijst ( 'Procedure opnieuw gestart op: ' ||
            to_char(sysdate,'dd-mm-yyyy hh24:mi')
          );
    lijst ( rpad ( 'Herstart vanaf relatienummer ', l_descript_lengte ) ||
            l_scheiding ||
            to_char (l_herstart_rle)
           );

  end if;

  stm_util.debug ('Logbestand wordt aangemaakt');

  -- Schrijven logbestand
  lijst ( rpad ('***** LOGVERSLAG BEPALEN GEWENSTE GBA-AFNEMERSINDICATIE  *****', 70)
               || ' STARTDATUM: ' || to_char (sysdate,'dd-mm-yyyy hh24:mi:ss'));

  lijst ( ' ' );
  lijst ( '--------------------------------------------------------------------------------' );
  lijst ( ' ' );
  lijst ('Relatienummer '||';'||'Melding','F');

  -- bepaal datum l_beeindigd_na_jo
  l_datum_beeindigd_na_jo := to_date('01-01-' || P_BEEINDIGD_NA_JO, 'dd-mm-yyyy');

  -- bepaal populatie voor het wegschrijven naar csv bestand.
  for r_rle in c_rle ( b_herstart_rle => l_herstart_rle )
  loop
    bepaal_afnemersindicatie
      ( p_numrelatie            => r_rle.numrelatie
      , p_persoonsnummer        => r_rle.persoonsnummer
      , p_datoverlijden         => r_rle.datoverlijden
      , p_maanden_overleden     => p_maanden_overleden
      , p_datum_beeindigd_na_jo => l_datum_beeindigd_na_jo
      );

    -- Herstart sleutel wegschrijven
    stm_util.schrijf_herstarttelling ( 'g_regelnr'
                                     ,  g_regelnr
                                     );
    stm_util.schrijf_herstarttelling ( 'g_msg_error'
                                     , g_msg_error
                                     );
    stm_util.schrijf_herstartsleutel ( 'l_herstart_rle'
                                     , to_char (r_rle.numrelatie)
                                     );
    --
    commit;
    --
  end loop;

  -- openen van de filehandler
  fh_stoplijst := utl_file.fopen ( 'RLE_OUTBOX'
                                  , l_bestandsnaam||'.CSV'
                                  , 'W'
                                  );

  -- schrijf de header records
  utl_file.put_line ( fh_stoplijst
                    , 'a_nummer;bsn;persoonsnummer;code_gewenste_indicatie'
                    );

  -- wegschrijven data naar csvbestand
  for r_csv_data in c_csv_data
  loop
      l_teller := l_teller +1;
      utl_file.put_line ( fh_stoplijst
                        , r_csv_data.anummer              || ';' ||
                          r_csv_data.bsn                  || ';' ||
                          r_csv_data.persoonsnummer       || ';' ||
                          r_csv_data.rle_code_gewenste_ind
                        );
  end loop;

  -- filehandle(s) sluiten
  utl_file.fclose_all;
  --
  lijst ('Invoerparameters:');
  lijst ('=======================================');
  lijst ('P_MAANDEN_OVERLEDEN: '|| to_char (p_maanden_overleden));
  lijst ('P_BEEINDIGD_NA_JO  : '|| to_char (p_beeindigd_na_jo));
  lijst ('=======================================');
 -- slot melding weergeven
  lijst ('Invoerbestand                         : GEEN');
  lijst ('Aantal naar csv bestand weggeschreven : ' || to_char(l_teller));
  lijst ('Aantal opgetreden errors              : ' || to_char(g_msg_error));
  lijst ( ' ' );
  lijst ('Rapportage per code afnemersindicatie:');
  lijst ('NR CODE_INDICATIE GEWENSTE_INDICATIE     AANTAL   SUM_SEC');
  --
  for r_scs
  in  c_scs
  loop
    lijst ( rpad(r_scs.nr,3)    ||
            rpad(r_scs.rle_code_gewenste_ind,15) ||
            rpad(r_scs.rle_gewenste_indicatie,19)  ||
            lpad(to_char(r_scs.aantal, '999G999G990', 'NLS_NUMERIC_CHARACTERS=,.'),10) ||
            lpad(to_char(r_scs.sum_sec, '999G999G990', 'NLS_NUMERIC_CHARACTERS=,.'),10)
          );
  end loop;
  --
  lijst (' ');
  lijst ('Uitvoerbestand bevat: personen met GEWENSTE_INDICATIE gelijk aan J!');
  lijst (' ');
  --
  lijst ('Bestand '||l_bestandsnaam||'.CSV'||' aangemaakt in de RLE inbox op de ftp pool!' );
  --
  --  Voettekst van lijsten
  lijst ( '********** EINDE VERSLAG BEPALEN GEWENSTE GBA-AFNEMERSINDICATIE  **********'|| ' EINDDATUM: ' || to_char (sysdate,'dd-mm-yyyy hh24:mi:ss'));
  stm_util.debug ('Einde logverslag');
  --
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  -- Verwijderen herstart
  stm_util.verwijder_herstart ;
  --
  commit;
  --
exception
  when others
  then
    rollback;
    stm_util.t_foutmelding := substr(sqlerrm,1,500);
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(stm_util.t_foutmelding,1,250)
                                  );
    commit;
end verwerk;
END RLE_SYN_BESTAND;
/