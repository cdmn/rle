CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_OFMW_API IS

/* Registreert een adres of werkt deze bij */
PROCEDURE REGISTREER_ADRES
 (P_RELATIENUMMER IN NUMBER
 ,P_STRAATNAAM IN VARCHAR2
 ,P_HUISNUMMER IN NUMBER
 ,P_WOONPLAATS IN VARCHAR2
 ,P_HUISNUMMER_TOEVOEGING IN VARCHAR2
 ,P_POSTCODE IN VARCHAR2
 ,P_LANDCODE IN VARCHAR2
 ,P_LOCATIE IN VARCHAR2 := null
 ,P_IND_WOONBOOT IN VARCHAR2 := 'N'
 ,P_IND_WOONWAGEN IN VARCHAR2 := 'N'
 )
 IS
/* *****************************************************************************************************
    Procedurenaam: registreer_adres
    Beschrijving:  registreert een adres of werkt het bij.
                   Aangeroepen door AdresService.registreerAdres (MN 3.0 Business Service)

    Datum       Wie  Wat
    ----------  ---  --------------------------------------------------------------
    26-02-2015  OKR  Creatie
    04-05-2015  XJB  Ingangsdatum relatie adres wordt nu altijd systeemdatum, dus niet langer uiterlijk
                     de datum overlijden, n.a.l.v. Jira 409 (bug)
    ***************************************************************************************************** */

    cursor c_ras
    (
      b_relatienummer in varchar2
     ,b_soort_adres   in varchar2
     ,b_rol           in varchar2
     ,b_datum_begin   in date
    ) is
      select ras.id
            ,ras.ads_numadres
            ,ras.datingang
      from   rle_relatie_adressen ras
      where  ras.rle_numrelatie = b_relatienummer
      and    ras.rol_codrol = b_rol
      and    ras.dwe_wrddom_srtadr = b_soort_adres
      and    b_datum_begin between trunc(ras.datingang) and nvl(ras.dateinde, b_datum_begin);

    cursor c_rle(b_numrelatie in number) is
      select rle.datoverlijden from rle_relaties rle where rle.numrelatie = b_numrelatie;

    l_numadres_nieuw   number;
    r_ras_huidig       c_ras%rowtype;
    l_ras_huidig_found boolean := false;
    r_rle              c_rle%rowtype;
    l_soort_adres      varchar2(2) := 'DA'; -- Voor nu alleen DA-adres ondersteund
    l_rol              varchar2(2) := 'PR'; -- Voor nu alleen rol 'PR' ondersteund
    l_datum_begin      date ;
    l_datum_einde      date := null; -- Voor nu alleen nieuw adres zonder einddatuum ondersteund
    l_oude_indicatie   varchar2(1);
begin

    /* Garanderen dat doorgave op "J" staat. */
    l_oude_indicatie := rle_indicatie.doorgeven_aan_gba ;
    rle_indicatie.doorgeven_aan_gba := 'J';

    -- 1 Begindatum ligt op systeemdatum
    l_datum_begin := trunc(sysdate);   -- xjb, 04-05-2015

    -- 2 Bepaal het nummer van het adres
    l_numadres_nieuw := rle_verwerk_ads(p_landcode
                                       ,p_postcode
                                       ,p_huisnummer
                                       ,p_huisnummer_toevoeging
                                       ,p_straatnaam
                                       ,p_woonplaats);

    -- 3 Bepaal of de persoon al een lopend adres heeft
    open c_ras(p_relatienummer, l_soort_adres, l_rol, l_datum_begin);
    fetch c_ras
      into r_ras_huidig;
    l_ras_huidig_found := c_ras%found;
    close c_ras;

    -- 4 Maak het nieuwe adres aan. Echter, doe dit niet als het nieuwe adres
    --   gelijk is aan het huidige adres
    if not (l_ras_huidig_found and r_ras_huidig.ads_numadres = l_numadres_nieuw)
    then

      -- Als het nieuwe adres dezelfde begindatum heeft als het huidige adres
      -- pas dan het huidge adres aan
      if r_ras_huidig.datingang = l_datum_begin
      then

        update rle_relatie_adressen ras
        set    ras.ads_numadres  = l_numadres_nieuw
              ,ras.dateinde      = l_datum_einde
              ,ras.provincie     = null
              ,ras.locatie       = p_locatie
              ,ras.ind_woonboot  = nvl(p_ind_woonboot,ras.ind_woonboot)
              ,ras.ind_woonwagen = nvl(p_ind_woonwagen,ras.ind_woonwagen)
        where  ras.id = r_ras_huidig.id;

      else
        -- Het nieuwe adres heeft een andere begindatum, dus voeren we het op en
        -- beeindigen we het eventuele huidige adres

        -- Voer het nieuwe adres op
        insert into rle_relatie_adressen
          (id
          ,rle_numrelatie
          ,rol_codrol
          ,dwe_wrddom_srtadr
          ,datingang
          ,dateinde
          ,ads_numadres
          ,provincie
          ,locatie
          ,ind_woonboot
          ,ind_woonwagen)
        values
          (rle_ras_seq1.nextval
          ,p_relatienummer
          ,'PR'
          ,l_soort_adres
          ,l_datum_begin
          ,l_datum_einde
          ,l_numadres_nieuw
          ,null
          ,p_locatie
          ,nvl(p_ind_woonboot, 'N')
          ,nvl(p_ind_woonwagen, 'N')
		  );

        -- Beeindig het eventuele huidige adres. Dit moet NA opvoer nieuw adres, ivm
        -- de implementatie van check BRENT011
        if l_ras_huidig_found
        then

          update rle_relatie_adressen ras
          set ras.dateinde = trunc(l_datum_begin - 1)
          where ras.id = r_ras_huidig.id;

        end if;

      end if;

    end if;

   /* Doorgave terugzetten */
   rle_indicatie.doorgeven_aan_gba := l_oude_indicatie ;

exception
  when others
  then
    /* Doorgave terugzetten */
    rle_indicatie.doorgeven_aan_gba := l_oude_indicatie ;
    /* en re-raise */
    raise;
end registreer_adres;

END RLE_OFMW_API;
/