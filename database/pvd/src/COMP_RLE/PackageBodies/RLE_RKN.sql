CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_RKN IS


  /******************************************************************************
    PACKAGE:    RLE_RKN
    DOEL:       Package Relatie Koppeling Functionaliteiten

    Wie    Wanneer         Wat
    -----  -------------   --------------------------------
    XCW    25-10-2012      Creatie

  ******************************************************************************/

  g_event_scheiding_partner boolean := true;


FUNCTION GET_SCHEIDING_PARTNER_ENABLED
 RETURN BOOLEAN
 IS
begin
    return g_event_scheiding_partner;
  end get_scheiding_partner_enabled;
PROCEDURE SET_SCHEIDING_PARTNER_ENABLED
 (P_SCHEIDING_PARTNER IN boolean
 )
 IS
begin
    g_event_scheiding_partner := p_scheiding_partner;
  end set_scheiding_partner_enabled;
/* Verwijderen van relatie koppeling(en). */
PROCEDURE VERWIJDER_RKN
 (P_RELATIENUMMER IN NUMBER
 ,P_RELATIENUMMER_VOOR IN NUMBER
 ,P_ROL_IN_KOPPELING IN VARCHAR2
 ,P_DAT_BEGIN IN DATE
 )
 IS
/************************************************************************
    Functienaam: verwijder_rkn
    Beschrijving
      Verwijder relatie koppeling(en)
      Relatienummer is in ieder geval verplicht, maar let op dan worden alle relatiekoppelingen
      voor deze relatie verwijderd.
      Typisch scenario is day je relatienummer, relatienummer_voor, soort koppeling en begindatum ALLEMAAL opgeeft.
      Bijvoorbeeld een huwelijk verwijderen tussen R123 en R456 dat is ingegaan op 12 maart 1992:
        123, 456, 'H', '12-03-1992'

    Datum      Wie        Wat
    --------   ---------- ------------------------------------------------
    17-10-2014 XJB        Creatie

  ************************************************************************* */

e_relatienummer_verplicht  exception;

cn_module            constant varchar2 (50) := 'RLE_NR.VERWIJDER_RKN';
begin

  if p_relatienummer is null
  then
    raise e_relatienummer_verplicht;
  end if;

  delete rle_relatie_koppelingen rkn
  where  rkn.rle_numrelatie      = p_relatienummer
  and    rkn.rle_numrelatie_voor = nvl(p_relatienummer_voor, rkn.rle_numrelatie_voor)
  and    rkn.dat_begin           = nvl(p_dat_begin, rkn.dat_begin)
  and    rkn.rkg_id              = nvl ( ( select rkg.id
                                           from   rle_rol_in_koppelingen rkg
                                           where  rkg.code_rol_in_koppeling = p_rol_in_koppeling
                                          )
                                       , rkn.rkg_id
                                       );

exception
  when e_relatienummer_verplicht
  then
    raise_application_error
      (-20001, cn_module || ' - Opgeven van p_relatienummer is verplicht.');
  when others
  then
    raise_application_error
      (-20000, stm_app_error (sqlerrm, cn_module, null)|| ' ' ||
               dbms_utility.format_error_backtrace );
end verwijder_rkn;
end rle_rkn;
/