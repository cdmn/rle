CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_WGR_000001 IS
-- Program Data
LG_NUMRELATIE NUMBER;
-- Sub-Program Units
PROCEDURE PRC_CHK_WGR_000001
 (PIN_NUMRELATIE IN NUMBER
 )
 IS
BEGIN
IF pin_numrelatie = lg_numrelatie
THEN
   stm_dbproc.raise_error( 'RLE-10335' ) ;
END IF ;
END PRC_CHK_WGR_000001;
-- PL/SQL Block
BEGIN
   /* Package initialization */
   SELECT rec.rle_numrelatie
   INTO   lg_numrelatie
   FROM   rle_relatie_externe_codes  rec
   WHERE  rec.dwe_wrddom_extsys = 'WGR'
   AND    rec.extern_relatie    = '000001'
   ;
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      lg_numrelatie := NULL ;
END ;
/