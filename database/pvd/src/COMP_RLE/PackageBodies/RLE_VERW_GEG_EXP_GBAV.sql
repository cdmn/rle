CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_VERW_GEG_EXP_GBAV IS

cn_lengte_sqlerrm   number := 255;


PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS

/**********************************************************************
  Procedure:    RLE_VERW_GEG_EXP_GBAV.VERWERK
  Beschrijving: Verwerken ontvangen aanvullende GBA-V gegevens ex-partners

  De drie invoerbestanden gbav_een.txt, gbav_meer.txt en gbav_fout.txt worden hier
  als EXTERNAl tables ingelezen, genaamd RLE_GBAV_EEN, RLE_GBAV_MEER en RLE_GBAV_FOUT

  Datum       Wie  Wat
  --------------------------------------------------------------
  25-10-2013  AZM  Creatie
  04-03-2014  MAL Div aanpassingen nav herontwerp
  **********************************************************************/
-- PL/SQL Specification


  cursor c_gbav_een
  is
    select gban.*
    from   rle_gbav_een gban;
  --
  r_gbav_een c_gbav_een%rowtype;
  --
  cursor c_gbav_meer
  is
    select gbar.*
    from   rle_gbav_meer gbar;
  --
  r_gbav_meer c_gbav_meer%rowtype;
  --
  cursor c_gbav_fout
  is
    select gbaf.*
    from   rle_gbav_fout gbaf;
  --
  r_gbav_fout c_gbav_fout%rowtype;
  --
  cursor c_bron_aantal(b_bestandsnaam in varchar2)
  is
    select count(*) aantal
    from  rle_bron_ex_partners bron
    where datum_opgenomen_gbabatch_ex is not null
    and datum_verwerkt_gba_ex_geg is null
    and bron.bestandsnaam= b_bestandsnaam;
  --
  r_bron_aantal  c_bron_aantal%rowtype;
  --
  cursor c_bron(b_intern_kenmerk in number, b_bestandsnaam in varchar2)
  is
    select bron.*
    from  rle_bron_ex_partners bron
    where datum_opgenomen_gbabatch_ex is not null
    and datum_verwerkt_gba_ex_geg is null
    and bron.intern_kenmerk= b_intern_kenmerk
    and bron.bestandsnaam= b_bestandsnaam
    for update;
  --
  r_bron  c_bron%rowtype;
  --
  cn_module   constant         varchar2( 255 ) := 'VERWERK';
  --
  t_volgnummer                 number    := 0;
  t_lijstnummer                number    := 1;
  t_uitvallijst                number    := 2;
  t_regelnummer                number    := 0;
  --
  t_sqlerrm                    varchar2( 255 );
  t_procedure                  varchar2( 80 );
  t_vorige_procedure           varchar2( 255 );
  t_locatie                    varchar2( 50 );
  --
  l_foutmelding                varchar2(4000);
  l_melding_gbav_fout          varchar2(1000);
  l_sysdate                    date := sysdate;
  l_geboortedatum              date;
  l_overlijdensdatum           date;
  l_dat_verwerkt               date;
  l_waarschuwing               varchar2(4000);
  l_ind_gba_fout_ex            varchar2(10) := '';
  l_gba_aanvullende_fout       varchar2(10) := '';
  l_bestaat                    boolean;
  l_lengte                     number;
  l_blocksize                  number;
  l_bsn                        varchar2(10);
  l_dateinde_huw               date;

  l_aant_bron_vooraf           number := 0;
  l_aant_gbav_in_bron          number := 0;
  l_aant_nog_te_verwerken      number := 0;
  l_fout_verwerking            number := 0;
  l_aantal                     number := 0;

  t_fout                       t_fout_tab;
  e_fout_bestand               exception;
BEGIN

  t_vorige_procedure                := stm_util.t_procedure;
  stm_util.t_procedure              := cn_module;
  stm_util.t_script_naam            := p_script_naam;
  stm_util.t_script_id							:= p_script_id;
  stm_util.t_programma_naam		      := 'RLE_VERW_GEG_EXP_GBAV';
  t_locatie                         := ' 001 begin ';
  --
  -- Maak de koptekst van het verwerkingsverslag aan
  --
  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'RLEPRC65: '||'Verwerken ontvangen GBAV aanvullende gegevens EX-Partners'
                          );
  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'Start: '||TO_CHAR (SYSDATE, 'DD-MM-YYYY HH24:MI:SS'));

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , ' ');

  stm_util.insert_lijsten( t_lijstnummer
                           , t_regelnummer
                           , rpad('-', 95, '-')
                           );

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'Selectie echtscheidingen bronbestand o.b.v. inputbestand: '||p_bestandsnaam
                         );

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , ' ');

  stm_util.insert_lijsten( t_lijstnummer
                           , t_regelnummer
                           , rpad('=', 95, '=')
                           );

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , ' ');

  select count(*)
  into l_aantal
  from rle_bron_ex_partners
  where bestandsnaam = p_bestandsnaam;

  if l_aantal = 0
  then
    raise e_fout_bestand;
  end if;
  --
  t_locatie := ' 002 voor gbav_een loop ';
  --
  -- bepalen aantallen te verwerken vooraf, voor de logging achteraf
  open c_bron_aantal(b_bestandsnaam => p_bestandsnaam);
  fetch c_bron_aantal into r_bron_aantal;
  l_aant_bron_vooraf := r_bron_aantal.aantal;
  close c_bron_aantal;

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'INPUT:');

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'Aantal echtscheidingen in bronbestand waarvan de verwerking wacht op GBA-gegevens van de ex: '||l_aant_bron_vooraf);

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , ' ');

   utl_file.fgetattr ('RLE_INBOX', 'gbav_een', l_bestaat, l_lengte, l_blocksize);

   if l_bestaat
   then
   -- Aantal in bestand doorlopen
  --
    for r_gbav_een in c_gbav_een
    loop
      begin
      l_foutmelding    := null;
      l_waarschuwing   := null;
      --
      begin
        l_geboortedatum := to_date(r_gbav_een.geboortedatum,'YYYYMMDD');
      exception
        when others
        then
          l_waarschuwing := 'Foutieve geboortedatum';
      end;
      --
      begin
        l_overlijdensdatum := to_date(r_gbav_een.overlijdensdatum,'YYYYMMDD');
      exception
        when others
        then
          l_waarschuwing := 'Foutieve geboortedatum';
      end;
      stm_util.debug('l_waarschuwing: '||l_waarschuwing);
      --
      open c_bron(b_intern_kenmerk => r_gbav_een.intern_kenmerk, b_bestandsnaam => p_bestandsnaam);
      fetch c_bron into r_bron;
      --
      if c_bron%FOUND
      then
        l_bsn := r_bron.bsn;
        l_dateinde_huw := r_bron.huwelijk_datumeinde;
        --
        -- record gevonden bijwerken bron
        update rle_bron_ex_partners
          set datum_verwerkt_gba_ex_geg  = sysdate
            , gba_ex_anr                 = r_gbav_een.anummer
            , gba_ex_bsn                 = r_gbav_een.bsn
            , gba_ex_voornamen           = r_gbav_een.voornamen
            , gba_ex_voorletters         = comp_gba.bepaal_voorletters(r_gbav_een.voornamen)
            , gba_ex_voorvoegsel         = r_gbav_een.voorvoegsel
            , gba_ex_naam                = r_gbav_een.naam
            , gba_ex_naamgebruik         = r_gbav_een.naamgebruik
            , gba_ex_datgeboorte         = l_geboortedatum
            , gba_ex_datoverlijden       = l_overlijdensdatum
            , gba_ex_geslacht            = r_gbav_een.geslacht
            , gba_ex_landcode            = r_gbav_een.landcode
            , gba_ex_straat              = r_gbav_een.straatnaam
            , gba_ex_huisnr              = r_gbav_een.huisnummer
            , gba_ex_huisletter          = r_gbav_een.huisletter
            , gba_ex_huisnrtoev          = r_gbav_een.toevoeging
            , gba_ex_postcode            = r_gbav_een.postcode
            , gba_ex_gem_nr              = r_gbav_een.gemeentenummer
            , gba_ex_gemeentedeel        = r_gbav_een.gemeentedeel
            , foutmelding                = l_waarschuwing
            , ind_gba_fout_ex            = 'N'
          where current of c_bron;
        l_aant_gbav_in_bron :=  l_aant_gbav_in_bron + 1;
      end if;
      close c_bron;

    exception
    when others
      then
        if c_bron%ISOPEN
        then
          close c_bron;
        end if;

        l_fout_verwerking := l_fout_verwerking + 1;

        t_fout (l_fout_verwerking).bsn := l_bsn;
        t_fout (l_fout_verwerking).dateinde_huwelijk := l_dateinde_huw;
        t_fout (l_fout_verwerking).fout := substr(sqlerrm,1,350);
    end;
    end loop;
  else
    dbms_output.put_line ('Bestand gbav_een niet gevonden.');
  end if;
  --

  t_locatie := ' 002 voor gbav_meer loop ';

  utl_file.fgetattr ('RLE_INBOX', 'gbav_meer', l_bestaat, l_lengte, l_blocksize);

  if l_bestaat
  then

    for r_gbav_meer in c_gbav_meer
    loop
      begin
      l_foutmelding    := null;
      l_waarschuwing   := null;
      --
      open c_bron(b_intern_kenmerk => r_gbav_meer.intern_kenmerk, b_bestandsnaam => p_bestandsnaam);
      fetch c_bron into r_bron;
     --
      if c_bron%FOUND
      then

        l_bsn := r_bron.bsn;
        l_dateinde_huw := r_bron.huwelijk_datumeinde;

        update rle_bron_ex_partners
          set ind_gba_fout_ex = 'J'
            , foutmelding = 'Geen unieke partnergegevens gevonden.'
            , datum_verwerkt_gba_ex_geg = sysdate
            where current of c_bron;

        l_aant_gbav_in_bron :=  l_aant_gbav_in_bron + 1;
      end if;
      close c_bron;

      exception
      when others
      then

        if c_bron%ISOPEN
        then
          close c_bron;
        end if;

        l_fout_verwerking := l_fout_verwerking + 1;

        t_fout (l_fout_verwerking).bsn := l_bsn;
        t_fout (l_fout_verwerking).dateinde_huwelijk := l_dateinde_huw;
        t_fout (l_fout_verwerking).fout := substr(sqlerrm,1,350);
      end;
    end loop;
  else
    dbms_output.put_line ('Bestand gbav_meer niet gevonden.');
  end if;

  --
  t_locatie := ' 003 voor gbav_fout loop ';

  utl_file.fgetattr ('RLE_INBOX', 'gbav_fout', l_bestaat, l_lengte, l_blocksize);

  if l_bestaat
  then

    for r_gbav_fout in c_gbav_fout
    loop
      begin
      l_foutmelding    := null;
      --
      open c_bron(b_intern_kenmerk => r_gbav_fout.intern_kenmerk, b_bestandsnaam => p_bestandsnaam);
      fetch c_bron into r_bron;
     --
      if c_bron%FOUND
      then

        l_bsn := r_bron.bsn;
        l_dateinde_huw := r_bron.huwelijk_datumeinde;

        if  r_gbav_fout.resultaat like '%G%Geen gegevens gevonden%'
        then

          l_ind_gba_fout_ex := 'N';
          l_dat_verwerkt    := sysdate;

        else

          l_ind_gba_fout_ex := 'J';
          l_dat_verwerkt := sysdate;

        end if;
        --
        l_foutmelding := r_gbav_fout.resultaat;

        update rle_bron_ex_partners
          set ind_gba_fout_ex            = l_ind_gba_fout_ex
            , datum_verwerkt_gba_ex_geg  = l_dat_verwerkt
            , foutmelding                = l_foutmelding
          where current of c_bron;

        l_aant_gbav_in_bron :=  l_aant_gbav_in_bron + 1;

      end if;
      close c_bron;

      exception
      when others
      then
        if c_bron%ISOPEN
        then
          close c_bron;
        end if;

         l_fout_verwerking := l_fout_verwerking + 1;

         t_fout (l_fout_verwerking).bsn := l_bsn;
         t_fout (l_fout_verwerking).dateinde_huwelijk := l_dateinde_huw;
         t_fout (l_fout_verwerking).fout := substr(sqlerrm,1,350);
      end;
    end loop;
  else
    dbms_output.put_line ('Bestand gbav_fout niet gevonden.');
  end if;
  --


  open c_bron_aantal(b_bestandsnaam => p_bestandsnaam);
  fetch c_bron_aantal into r_bron_aantal;
  l_aant_nog_te_verwerken := r_bron_aantal.aantal;
  close c_bron_aantal;

  t_locatie := ' 028 wegschrijven aantallen ';
  stm_util.debug('t_locatie: '||t_locatie);
  --
  -- Aantallen wegschrijven voor controle verslag
  --
  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'OUTPUT:');

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'Aantal echtscheidingen in bronbestand waarvan de GBA-gegevens zijn ontvangen en verwerkt: '||l_aant_gbav_in_bron);

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'Aantal echtscheidingen in bronbestand waarvan de GBA-gegevens niet zijn gevonden in de GBA-bestanden: '||l_aant_nog_te_verwerken);
  --
  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , '');

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'Voor de echtscheidingen waarvan de GBA-gegevens zijn ontvangen en verwerkt kunnen workflowcases worden aangemaakt.');

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , '');

  if t_fout.count > 0
  then
    stm_util.insert_lijsten( t_lijstnummer
                           , t_regelnummer
                           , 'De volgende echtscheidingen konden niet worden verwerkt vanwege een technische fout (aantal: '||l_fout_verwerking||').');

    for i in t_fout.first .. t_fout.last
    loop

       stm_util.insert_lijsten( t_lijstnummer
                              , t_regelnummer
                             , 'Deelnemer BSN: '||t_fout(i).bsn||' _ Einddatum huwelijk: '||t_fout (i).dateinde_huwelijk||' _ Foutmelding: '
                             );

       stm_util.insert_lijsten( t_lijstnummer
                              , t_regelnummer
                              , t_fout (i).fout);

    end loop;

    stm_util.insert_lijsten( t_lijstnummer
                           , t_regelnummer
                           , ''
                           );

  end if;

  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , 'EINDE VERSLAG VERWERKEN AANVULLENDE GEGEVENS EX-PATNERS'
                         );

  stm_util.insert_lijsten ( t_lijstnummer
                          , t_regelnummer
                          , 'Eindtijd: '||TO_CHAR (SYSDATE, 'DD-MM-YYYY HH24:MI:SS'));

  --
  stm_util.insert_job_statussen( t_volgnummer
                               , 'S'
                               , '0'
                               );
 -- commit;
  --
  stm_util.t_procedure := t_vorige_procedure;
--
  exception
   when e_fout_bestand
   then
      rollback;
      stm_util.insert_job_statussen(t_volgnummer
                                  ,'S'
                                  ,'1'
                                   );
      stm_util.insert_job_statussen(t_volgnummer,
                                    'I',
                                    stm_util.t_programma_naam ||
                                   ' procedure : ' || stm_util.t_procedure);

      stm_util.insert_job_statussen(t_volgnummer,
                                    'I',
                                    'De opgegeven bestandsnaam ('||p_bestandsnaam||') is onbekend.');
      commit;

   when others
   then
    --
    if c_gbav_een%ISOPEN then close c_gbav_een; end if;
    if c_gbav_meer%ISOPEN then close c_gbav_meer; end if;
    if c_gbav_fout%ISOPEN then close c_gbav_fout; end if;
    if c_bron%ISOPEN then close c_bron; end if;

    t_sqlerrm := substr( stm_get_batch_message( sqlerrm ), 1, 255);
    t_procedure := substr( stm_util.t_procedure          , 1, 80 );
    --
    rollback;
    --
    stm_util.debug( t_sqlerrm );
    --
    stm_util.insert_job_statussen( t_volgnummer, 'S', '1');
    stm_util.insert_job_statussen( t_volgnummer, 'I', t_procedure);
    stm_util.insert_job_statussen( t_volgnummer, 'I', t_sqlerrm  );
    --
    commit;
    --
    raise_application_error( -20000
                           , stm_get_batch_message( sqlerrm ) ||  t_locatie  || ' ' || dbms_utility.format_error_backtrace
                           );
--
END VERWERK;
end RLE_VERW_GEG_EXP_GBAV;
/