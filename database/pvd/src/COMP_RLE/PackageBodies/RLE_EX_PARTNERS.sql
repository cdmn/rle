CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_EX_PARTNERS IS


/****************************************************************

  Packagenaam: RLE_EX_PARTNERS
  Beschrijving:
    Ondersteunende package tbv de verwerking van de door T&T
    geleverde GBA-V batchbestanden voor registratie van ex-partners

Datum      Wie        Wat
   --------   ---------- --------------------------------
   04-10-2013 MAL        Creatie
   11-03-2014 MAL        Diverse aanpassingen nav herontwerp
   03-01-2016 VER        MNGBA-1142, wijziging doorgifte voorvoegsels aan RLE_RLE_TOEV
                         Domeincheck op voorvoegsels gebeurt aldaar
*****************************************************************/  --

g_maak_uit_bericht           boolean:= true;


/* voorkomen dat een GBA uitbericht wordt gestuurd */
FUNCTION MAAK_UITBERICHT
 RETURN BOOLEAN
 IS
begin
  return g_maak_uit_bericht;
end;
PROCEDURE BESTAAT_RKN
 (P_RLE_NUMRELATIE_DLN IN number
 ,P_RLE_NUMRELATIE_EX IN number
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN date
 ,P_RKN_AANWEZIG OUT VARCHAR2
 )
 IS


cursor c_rkn (b_rle_numrelatie_dln in number
             ,b_rle_numrelatie_ex in number
             ,b_dat_begin in date
             ,b_dat_einde in date)
is
  select 1
  from rle_relatie_koppelingen rkn
  where rkn.rkg_id = 1
    and rkn.rle_numrelatie = b_rle_numrelatie_dln
    and rkn.rle_numrelatie_voor = b_rle_numrelatie_ex
    and rkn.dat_begin = b_dat_begin
    and rkn.dat_einde = b_dat_einde;

r_rkn            c_rkn%rowtype;
l_rkn_aanwezig   varchar2(1) := 'N';
begin

  open c_rkn (p_rle_numrelatie_dln
             ,p_rle_numrelatie_ex
             ,p_dat_begin
             ,p_dat_einde);
  fetch c_rkn into r_rkn;

  if c_rkn%found
  then
     l_rkn_aanwezig := 'J';

  else

    l_rkn_aanwezig := 'N';

  end if;

  close c_rkn;

  p_rkn_aanwezig := l_rkn_aanwezig;

end bestaat_rkn;
PROCEDURE BESTAAT_EX
 (P_NAAM IN varchar2
 ,P_VOORLETTERS IN varchar2
 ,P_GEBOORTEDATUM IN date
 ,P_BSN IN varchar2
 ,P_ANR IN VARCHAR2
 ,P_RLE_NUMRELATIE_EX OUT number
 ,P_PERSOONSNUMMER_EX OUT varchar2
 ,P_FOUTMELDING OUT varchar2
 )
 IS

cursor c_bsn_ex (b_bsn in varchar2
                ,b_datgeboorte in date)
is
  select rle.numrelatie
  from rle_relatie_externe_codes rec
      ,rle_relaties rle
  where rec.rol_codrol = 'PR'
    and rec.dwe_wrddom_extsys = 'SOFI'
    and rec.rle_numrelatie = rle.numrelatie
    and (extern_relatie = b_bsn
         and b_bsn is not null)
    and (rle.datgeboorte = b_datgeboorte
       and b_datgeboorte is not null);

cursor c_anr (b_anr in varchar2
             ,b_datgeboorte in date)
is
  select rle.numrelatie
  from rle_relatie_externe_codes rec
      ,rle_relaties rle
  where rle.numrelatie = rec.rle_numrelatie
    and rec.rol_codrol = 'PR'
    and rec.dwe_wrddom_extsys = 'GBA'
    and (rec.extern_relatie = b_anr
        and b_anr is not null)
    and (rle.datgeboorte = b_datgeboorte
        and b_datgeboorte is not null);

cursor c_bsn (b_rle_numrelatie in number)
is
  select extern_relatie
  from rle_relatie_externe_codes rec
  where rec.rol_codrol = 'PR'
    and rec.dwe_wrddom_extsys = 'SOFI'
    and rle_numrelatie = b_rle_numrelatie;

cursor c_rle (b_naam in varchar2
             ,b_datgeboorte in date
             ,b_voorletters in varchar2)
is
  select min(numrelatie) numrelatie
        ,count(*) aantal
  from rle_relaties rle
  where (rle.datgeboorte = b_datgeboorte
        and b_datgeboorte is not null)
    and (rle.namrelatie = b_naam
        and b_naam is not null)
    and (rle.voorletter = b_voorletters
        and b_voorletters is not null)
    group by datgeboorte, voorletter, namrelatie;

r_bsn             c_bsn%rowtype;
r_rle             c_rle%rowtype;
r_bsn_ex          c_bsn_ex%rowtype;
r_anr             c_anr%rowtype;

l_aantal                number := 0;
l_relatienummer_ex      number;
l_persoonsnummer_ex     varchar2(15);
begin

    open c_bsn_ex (p_bsn, p_geboortedatum);
    fetch c_bsn_ex into r_bsn_ex;

    if c_bsn_ex%found
    then
      l_relatienummer_ex := r_bsn_ex.numrelatie;
    else
      l_relatienummer_ex := null;
    end if;
    close c_bsn_ex;

    if l_relatienummer_ex is null
    then
      open c_anr (p_anr, p_geboortedatum);
      fetch c_anr into r_anr;

      if c_anr%found
      then
        l_relatienummer_ex := r_anr.numrelatie;
      else
        l_relatienummer_ex := null;
      end if;
      close c_anr;
    end if;

    if l_relatienummer_ex is null
    then

      open c_rle (p_naam, p_geboortedatum, p_voorletters);
      fetch c_rle into r_rle;

      if c_rle%notfound
      then

        l_relatienummer_ex := null;

      else

        l_relatienummer_ex:= r_rle.numrelatie;
        l_aantal := r_rle.aantal;

      end if;
      close c_rle;

      if l_relatienummer_ex is not null
      then

        if l_aantal > 1
        then

          p_foutmelding :=  'Bij het zoeken op naam, geb.datum en voorletters ex-partner dubbel';
          l_relatienummer_ex := null;

        end if;
      end if;

      if l_relatienummer_ex is not null
      then

      open c_bsn (l_relatienummer_ex);
      fetch c_bsn into r_bsn;

        if c_bsn%found
        then

          p_foutmelding :='Ex gevonden op naam, geb.datum en voorletters, BSN anders';
          l_relatienummer_ex := null;

        end if;
        close c_bsn;
      end if;
    end if;

  p_rle_numrelatie_ex := l_relatienummer_ex;
  p_persoonsnummer_ex := rle_lib.get_persoonsnr_relatie (l_relatienummer_ex);

end bestaat_ex;
PROCEDURE BIJWERKEN_KOPPELING
 (P_RLE_NUMRELATIE IN number
 ,P_RLE_NUMRELATIE_VOOR IN number
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN date
 ,P_KENMERK IN number
 ,P_FOUTMELDING OUT varchar2
 )
 IS

cursor c_rkn  (b_rle_numrelatie_dln in number
              ,b_rle_numrelatie_ex in number)
is
  select 1
  from rle_relatie_koppelingen rkn
  where rkn.rle_numrelatie = b_rle_numrelatie_dln
    and rkn.rle_numrelatie_voor = b_rle_numrelatie_ex
    and rkg_id = 1;

cursor c_rkn2 (b_rle_numrelatie_dln in number
              ,b_rle_numrelatie_ex in number)
is
  select rkn.dat_begin
        ,rkn.dat_einde
        ,rkn.id
  from rle_relatie_koppelingen rkn
  where rkn.rkg_id = 1
    and rkn.rle_numrelatie = b_rle_numrelatie_dln
    and rkn.rle_numrelatie_voor = b_rle_numrelatie_ex;

r_rkn                c_rkn%rowtype;
r_rkn2               c_rkn2%rowtype;
t_sqlerrm            varchar2(300);
l_start              number;
l_errorcode          varchar2(100);
l_errormessage       varchar2(400);
l_dat_begin          date;
begin

  if p_dat_begin is null
    or p_dat_begin = p_dat_einde
  then
    l_dat_begin := p_dat_einde - 1;
  else
    l_dat_begin := p_dat_begin;
  end if;

  rle_rkn.set_scheiding_partner_enabled (false);

  open c_rkn (p_rle_numrelatie
             ,p_rle_numrelatie_voor);
  fetch c_rkn into r_rkn;

  if c_rkn%notfound
  then

    insert into rle_relatie_koppelingen (id
                                        ,rkg_id
                                        ,rle_numrelatie
                                        ,rle_numrelatie_voor
                                        ,dat_begin
                                        ,dat_einde)
    values (rle_rkn_seq1.nextval
           ,1 --huwelijk
           ,p_rle_numrelatie
           ,p_rle_numrelatie_voor
           ,l_dat_begin
           ,p_dat_einde
           );

    update rle_bron_ex_partners
    set ind_rel_koppeling_aangemaakt = 'J'
       ,ind_rel_koppeling_gewijzigd = 'N'
    where intern_kenmerk = p_kenmerk;

  close c_rkn;
  else
    close c_rkn;

    open c_rkn2 (p_rle_numrelatie
                ,p_rle_numrelatie_voor);
    fetch c_rkn2 into r_rkn2;


    if r_rkn2.dat_begin <> l_dat_begin
    then

      delete from rle_relatie_koppelingen
      where id = r_rkn2.id;

      insert into rle_relatie_koppelingen (id
                                          ,rkg_id
                                          ,rle_numrelatie
                                          ,rle_numrelatie_voor
                                          ,dat_begin
                                          ,dat_einde)
                 values (rle_rkn_seq1.nextval
                        ,1
                        ,p_rle_numrelatie
                        ,p_rle_numrelatie_voor
                        ,l_dat_begin
                        ,r_rkn2.dat_einde);

      update rle_bron_ex_partners
      set ind_rel_koppeling_gewijzigd = 'J'
         ,ind_rel_koppeling_aangemaakt = 'N'
      where intern_kenmerk = p_kenmerk;

    end if;

    if r_rkn2.dat_einde is null
    then

      update rle_relatie_koppelingen
      set dat_einde = p_dat_einde
      where dat_begin = l_dat_begin
        and rle_numrelatie = p_rle_numrelatie
        and rle_numrelatie_voor = p_rle_numrelatie_voor;

      update rle_bron_ex_partners
      set ind_rel_koppeling_gewijzigd = 'J'
         ,ind_rel_koppeling_aangemaakt = 'N'
      where intern_kenmerk = p_kenmerk;

    end if;

    if r_rkn2.dat_einde <> p_dat_einde
    then

      update rle_relatie_koppelingen
      set dat_einde = p_dat_einde
      where dat_begin = l_dat_begin
        and rle_numrelatie = p_rle_numrelatie
        and rle_numrelatie_voor = p_rle_numrelatie_voor;

      update rle_bron_ex_partners
      set ind_rel_koppeling_gewijzigd = 'J'
         ,ind_rel_koppeling_aangemaakt = 'N'
      where intern_kenmerk = p_kenmerk;
    end if;
  end if;

  rle_rkn.set_scheiding_partner_enabled (true);

exception
  when others
  then
    t_sqlerrm := substr(sqlerrm, 1, 300);

   if instr(t_sqlerrm,'PER-') > 0
   then
      l_start := instr(t_sqlerrm,'PER-');
   elsif instr(t_sqlerrm,'RLE-') > 0
   then
      l_start := instr(t_sqlerrm,'RLE-');
   end if;
   --
   if l_start > 0
   then
      l_errorcode := substr(t_sqlerrm,l_start,9);
      l_errormessage := stm_get_batch_message(l_errorcode);
      t_sqlerrm := substr( l_errorcode||' '||l_errormessage||' '||t_sqlerrm,1,200);
   else
      p_foutmelding := substr(t_sqlerrm,1,200);
   end if;

   p_foutmelding := t_sqlerrm;

   rle_rkn.set_scheiding_partner_enabled (true);

end bijwerken_koppeling;
PROCEDURE MAAK_BERICHT
 (P_BESTANDSNAAM IN varchar2
 ,P_RELATIENUMMER IN number
 )
 IS

cursor c_dln (b_bestandsnaam in varchar2
             ,b_relatienummer in number
             )
is
  select persoonsnummer
        ,relatienummer
        ,bsn
        ,regeling
  from rle_bron_ex_partners
  where upper(bestandsnaam) = b_bestandsnaam
    and relatienummer = b_relatienummer
  group by persoonsnummer, relatienummer, bsn, regeling
  order by relatienummer;

cursor c_exp (b_relatienummer in number
             ,b_bestandsnaam in varchar2 )
is
  select persoonsnummer_ex
        ,lpad(bsn_ex, 9, 0) bsn_ex
        ,a_nummer_ex
        ,geboortedatum_ex
        ,overlijdensdatum_ex
        ,geslacht_ex
        ,naam_ex
        ,voorletters_ex
        ,voorvoegsels_ex
        ,naamgebruik_ex
        ,gemeentenummer_ex
        ,gemeentedeel_ex
        ,straatnaam_ex
        ,huisnummer_ex
        ,huisletter_ex
        ,toevoeging_ex
        ,postcode_ex
        ,landcode_ex
        ,huwelijk_datumbegin
        ,huwelijk_datumeinde
        ,intern_kenmerk
        ,foutmelding
  from rle_bron_ex_partners
  where relatienummer = b_relatienummer
    and upper(bestandsnaam) = upper(b_bestandsnaam)
    and (ind_gba_fout_dln = 'J'
         or ind_gba_fout_ex = 'J')
    union
  select persoonsnummer_ex
        ,lpad(gba_ex_bsn, 9, 0) bsn_ex
        ,gba_ex_anr
        ,gba_ex_datgeboorte
        ,gba_ex_datoverlijden
        ,gba_ex_geslacht
        ,gba_ex_naam
        ,gba_ex_voorletters
        ,gba_ex_voorvoegsel
        ,gba_ex_naamgebruik
        ,gba_ex_gem_nr
        ,gba_ex_gemeentedeel
        ,gba_ex_straat
        ,gba_ex_huisnr
        ,gba_ex_huisletter
        ,gba_ex_huisnrtoev
        ,gba_ex_postcode
        ,(select landnaam_hfdl from landen where landkode = gba_ex_landcode ) landkode_ex
        ,huwelijk_datumbegin
        ,huwelijk_datumeinde
        ,intern_kenmerk
        ,foutmelding
  from comp_rle.rle_bron_ex_partners
  where relatienummer = b_relatienummer
    and upper(bestandsnaam) = b_bestandsnaam
    and ind_gba_fout_dln = 'N'
    and nvl(ind_gba_fout_ex, 'N') = 'N'
  order by huwelijk_datumbegin;

cursor c_gem (b_gemeentenummer in number)
is
  select gemeentenaam
  from gemeentes
  where gemeentenummer = b_gemeentenummer;

  doc                  xmldom.domdocument;
  main_node            xmldom.domnode;
  root_node            xmldom.domnode;
  deelnemer_node       xmldom.domnode;
  ex_node              xmldom.domnode;
  item_node            xmldom.domnode;
  root_elmt            xmldom.domelement;
  item_elmt            xmldom.domelement;
  item_text            xmldom.domtext;

    l_xml                xmltype;
  l_bericht            xmltype;
  l_valmelding         varchar2(2000);
  l_persoonsnummer_ex  varchar2(15);
  r_dln_rec            c_dln%rowtype;
  l_gemeente           varchar2(100);

procedure add_node
( p_doc         in xmldom.DOMDocument
, p_parent_node in xmldom.DOMNode
, p_node_name   in VARCHAR2
, p_node_data   in VARCHAR2
)  IS
--
 item_elmt   xmldom.DOMElement;
 item_node   xmldom.DOMNode;
 item_text   xmldom.DOMText;
begin
 item_elmt := xmldom.createElement ( p_doc         , p_node_name);
 item_node := xmldom.appendChild   ( p_parent_node , xmldom.makeNode(item_elmt));
 item_text := xmldom.createTextNode( p_doc         , p_node_data);
 item_node := xmldom.appendChild   ( item_node     , xmldom.makeNode(item_text));

end add_node;
begin

  open c_dln (p_bestandsnaam, p_relatienummer);
  fetch c_dln into r_dln_rec;
  close c_dln;

    doc := xmldom.newDOMDocument;
    main_node := xmldom.makeNode(doc);
    root_elmt := xmldom.createElement(doc, 'RegistrerenMuterenSpeciaal');
    xmldom.setAttribute(root_elmt, 'xmlns' , '');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    item_elmt := xmldom.createElement(doc, 'Deelnemer');
    deelnemer_node := xmldom.appendChild(root_node, xmldom.makeNode(item_elmt));

    add_node (doc, deelnemer_node, 'Persoonsnummer', r_dln_rec.persoonsnummer);
    add_node (doc, deelnemer_node, 'Relatienummer', r_dln_rec.relatienummer);
    add_node (doc, deelnemer_node, 'Bsn', r_dln_rec.bsn);

    for r_exp_rec in c_exp (r_dln_rec.relatienummer, p_bestandsnaam)
    loop

        l_gemeente := null;

        open c_gem (r_exp_rec.gemeentenummer_ex);
        fetch c_gem into l_gemeente;
        close c_gem;

      item_elmt := xmldom.createElement(doc, 'ExPartner');
      ex_node := xmldom.appendChild(root_node, xmldom.makeNode(item_elmt));

      add_node (doc, ex_node, 'Persoonsnummer', r_exp_rec.persoonsnummer_ex);
      add_node (doc, ex_node, 'Bsn', r_exp_rec.bsn_ex);
      add_node (doc, ex_node, 'ANummer', r_exp_rec.a_nummer_ex);
      add_node (doc, ex_node, 'Geboortedatum', to_char(r_exp_rec.geboortedatum_ex, 'dd-mm-yyyy'));
      add_node (doc, ex_node, 'Overlijdensdatum', to_char(r_exp_rec.overlijdensdatum_ex, 'dd-mm-yyyy'));
      add_node (doc, ex_node, 'Geslacht', r_exp_rec.geslacht_ex);
      add_node (doc, ex_node, 'Naam', r_exp_rec.naam_ex);
      add_node (doc, ex_node, 'Voorletters', r_exp_rec.voorletters_ex);
      add_node (doc, ex_node, 'Voorvoegsels', r_exp_rec.voorvoegsels_ex);
      add_node (doc, ex_node, 'Naamgebruik', r_exp_rec.naamgebruik_ex);
      add_node (doc, ex_node, 'Gemeente', l_gemeente);
      add_node (doc, ex_node, 'Gemeentedeel', r_exp_rec.gemeentedeel_ex);
      add_node (doc, ex_node, 'Straat', r_exp_rec.straatnaam_ex);
      add_node (doc, ex_node, 'Huisnummer', r_exp_rec.huisnummer_ex);
      add_node (doc, ex_node, 'Huisletter', r_exp_rec.huisletter_ex);
      add_node (doc, ex_node, 'Toevoeging', r_exp_rec.toevoeging_ex);
      add_node (doc, ex_node, 'Postcode', r_exp_rec.postcode_ex);
      add_node (doc, ex_node, 'Landcode', r_exp_rec.landcode_ex);
      add_node (doc, ex_node, 'HuwelijkBegin', to_char(r_exp_rec.huwelijk_datumbegin, 'dd-mm-yyyy'));
      add_node (doc, ex_node, 'HuwelijkEinde', to_char(r_exp_rec.huwelijk_datumeinde, 'dd-mm-yyyy'));
      add_node (doc, ex_node, 'Kenmerk', r_exp_rec.intern_kenmerk);
      add_node (doc, ex_node, 'Foutmelding', r_exp_rec.foutmelding);

      update rle_bron_ex_partners
      set ind_workflow_case_aangemaakt = 'J'
      where intern_kenmerk = r_exp_rec.intern_kenmerk;

    end loop;

    l_xml := dbms_xmldom.getxmltype(doc);

    l_bericht := berichten_portaal.maak_bericht (p_payload       => l_xml
                                                ,p_berichttype   => 'REGISTREREN_SPECIALE_GEVALLEN'
                                                ,p_berichtid     => 'GBA_EXP'||r_dln_rec.relatienummer
                                                ,p_bronsysteem   => 'GBA'
                                                ,p_administratie => r_dln_rec.regeling);

     berichtenportaal.verstuur (p_bericht             => l_bericht.getclobval
                               ,p_validatie_meldingen => l_valmelding) ;

    xmldom.freeDocument (doc);

exception
when others then
  raise_application_error(-20000, stm_app_error(sqlerrm, null, null)||' '||dbms_utility.format_error_backtrace );
  rollback;

end maak_bericht;
PROCEDURE VERWERK_BERICHT
 (P_BERICHT_ID IN varchar2
 ,P_IND_SUCCES OUT varchar2
 ,P_FOUTMELDING OUT varchar2
 )
 IS

cursor c_apn (b_persoonsnummer in varchar2)
is
  select apn.id
  from per_aanmeldingen apn
  where apn.rle_persoonsnummer = b_persoonsnummer;

cursor c_per (b_persoonsnummer in varchar2)
is
  select *
  from personen
  where persoonsnummer = b_persoonsnummer;

l_berichtregel       varchar2(8000);
l_key                varchar2(200);
l_dummy              varchar2(50);
l_value              varchar2(4000);
l_intern_kenmerk     number;
l_teller             number := 0;
l_naam_ex            varchar2(120);
l_persoonsnummer     varchar2(15);
l_relatienummer      number;
l_geslacht           varchar2(1);
l_datgeboorte        date;
l_land               varchar2(50);
l_woonplaats         varchar2(150);
l_straat             varchar2(150);
l_persoonsnummer_ex  varchar2(10);
l_relatienummer_ex   number;
l_bsn_ex             varchar2(10);
l_ind_succes         varchar2(5);
l_error              varchar2(2000);
l_ras_id             number;
r_apn                c_apn%rowtype;
r_per                c_per%rowtype;
t_sqlerrm            varchar2(2000);
l_start              number;
l_errorcode          varchar2(100);
l_errormessage       varchar2(400);
l_verwerkt           varchar2(5);
l_foutmelding        varchar2(2000);
l_status             varchar2(5);
l_indicatie          varchar2(10);

t_ex_partner         ex_partner;

e_foutmelding          exception;
begin

  savepoint start_toevoegen;

  get_wrd_tbn (p_bericht_id
              ,l_berichtregel
              );

  while l_berichtregel is not null
  loop

    l_key := comp_stm.stm_get_word(l_berichtregel);
    l_dummy := comp_stm.stm_get_word(l_berichtregel);
    l_value := comp_stm.stm_get_word(l_berichtregel);

    stm_util.debug ('l_key = ' || l_key);
    stm_util.debug('l_value '||l_value);

    if l_key = 'PERSOONSNUMMER'
    then
       l_persoonsnummer := l_value;
    end if;

    if l_key = 'BEGINDATUM_HUWELIJK'
    then
       l_teller := l_teller + 1;
       t_ex_partner(l_teller).begindatum_huwelijk := to_date(l_value, 'dd-mm-yyyy');
    end if;

    if l_key = 'EINDDATUM_HUWELIJK'
    then

       t_ex_partner(l_teller).einddatum_huwelijk := to_date(l_value, 'dd-mm-yyyy');
    end if;

    if l_key = 'KENMERK'
    then

       t_ex_partner(l_teller).kenmerk := l_value;
    end if;

    if l_key = 'PERSOONSNUMMER_EX'
    then
       t_ex_partner(l_teller).persoonsnummer := l_value;
    end if;

    if l_key = 'BSN'
    then t_ex_partner(l_teller).bsn := l_value;
    end if;

    if l_key = 'A_NUMMER_EX'
    then t_ex_partner(l_teller).a_nummer := l_value;
    end if;

    if l_key = 'GEBOORTEDATUM_EX'
    then t_ex_partner(l_teller).geboortedatum := to_date(l_value, 'dd-mm-yyyy');
    end if;

    if l_key = 'OVERLIJDENSDATUM_EX'
    then t_ex_partner(l_teller).overlijdensdatum := to_date(l_value, 'dd-mm-yyyy');
    end if;

    if l_key = 'GESLACHT_EX'
    then t_ex_partner(l_teller).geslacht := l_value;
    end if;

    if l_key = 'NAAM_EX'
    then
    t_ex_partner(l_teller).naam := l_value;
    end if;

    if l_key = 'VOORLETTERS_EX'
    then
    t_ex_partner(l_teller).voorletters := l_value;
    end if;

    if l_key = 'VOORVOEGSELS_EX'
    then
    t_ex_partner(l_teller).voorvoegsels := l_value;
    end if;

    if l_key = 'NAAMGEBRUIK_EX'
    then
    t_ex_partner(l_teller).naamgebruik := l_value;
    end if;

    if l_key = 'STRAAT_EX'
    then
    t_ex_partner(l_teller).straat := l_value;
    end if;

    if l_key = 'HUISNUMMER_EX'
    then
    t_ex_partner(l_teller).huisnummer := l_value;
    end if;


    if l_key = 'TOEVOEGING_EX'
    then
    t_ex_partner(l_teller).huisnummer_toev := l_value;
    end if;

    if l_key = 'POSTCODE_EX'
    then
    t_ex_partner(l_teller).postcode := l_value;
    end if;

    if l_key = 'WOONPLAATS_EX'
    then
    t_ex_partner(l_teller).woonplaats := l_value;
    end if;

    if l_key = 'LAND_EX'
    then
    t_ex_partner(l_teller).land := l_value;
    end if;
  end loop;

  l_relatienummer := rle_lib.get_relatienr_persoon (l_persoonsnummer);

  stm_util.debug ('relatienummer deelnemer: '||l_relatienummer);

  if t_ex_partner.count > 0
  then
    for i in t_ex_partner.first..t_ex_partner.last
    loop
      stm_util.debug('t_ex_partner(i).begindatum_huwelijk ' ||t_ex_partner(i).begindatum_huwelijk);
      stm_util.debug('t_ex_partner(i).einddatum_huwelijk ' ||t_ex_partner(i).einddatum_huwelijk);
      stm_util.debug('t_ex_partner(i).kenmerk '||t_ex_partner(i).kenmerk);
      stm_util.debug('t_ex_partner(i).persoonsnummer ' ||t_ex_partner(i).persoonsnummer);
      stm_util.debug('t_ex_partner(i).bsn '||t_ex_partner(i).bsn);
      stm_util.debug('t_ex_partner(i).a_nummer '||t_ex_partner(i).a_nummer);
      stm_util.debug('t_ex_partner(i).geboortedatum ' ||t_ex_partner(i).geboortedatum);
      stm_util.debug('t_ex_partner(i).overlijdensdatum '||t_ex_partner(i).overlijdensdatum);
      stm_util.debug('t_ex_partner(i).geslacht '||t_ex_partner(i).geslacht);
      stm_util.debug('t_ex_partner(i).naam '||t_ex_partner(i).naam);
      stm_util.debug('t_ex_partner(i).voorletters '||t_ex_partner(i).voorletters);
      stm_util.debug('t_ex_partner(i).voorvoegsels '||t_ex_partner(i).voorvoegsels);
      stm_util.debug('t_ex_partner(i).naamgebruik '||t_ex_partner(i).naamgebruik);
      stm_util.debug('t_ex_partner(i).straat '||t_ex_partner(i).straat);
      stm_util.debug('t_ex_partner(i).huisnummer '||t_ex_partner(i).huisnummer);
      stm_util.debug('t_ex_partner(i).huisnummer_toev '||t_ex_partner(i).huisnummer_toev);
      stm_util.debug('t_ex_partner(i).postcode '||t_ex_partner(i).postcode);
      stm_util.debug('t_ex_partner(i).woonplaats '||t_ex_partner(i).woonplaats);
      stm_util.debug('t_ex_partner(i).land '||t_ex_partner(i).land);

      l_intern_kenmerk := t_ex_partner(i).kenmerk;

      g_maak_uit_bericht:= false;

      l_relatienummer_ex := null;

      if t_ex_partner(i).persoonsnummer is null
      then
      --controleren of de ex-partners inmiddels wel bekend is in rle
        bestaat_ex (p_naam => t_ex_partner(i).naam
                   ,p_voorletters => t_ex_partner(i).voorletters
                   ,p_geboortedatum => t_ex_partner(i).geboortedatum
                   ,p_bsn => t_ex_partner(i).bsn
                   ,p_anr => t_ex_partner(i).a_nummer
                   ,p_rle_numrelatie_ex => l_relatienummer_ex
                   ,p_persoonsnummer_ex => l_persoonsnummer_ex
                   ,p_foutmelding => l_error);

        if l_error is not null
        then
          l_foutmelding := l_error;
          raise e_foutmelding;
        end if;

        if l_relatienummer_ex is not null
        then

          update rle_bron_ex_partners
          set ind_ex_al_bekend_rle = 'J'
             ,ind_ex_aangemaakt_rle = 'J'
             ,relatienummer_ex = l_relatienummer_ex
             ,persoonsnummer_ex = l_persoonsnummer_ex
          where intern_kenmerk = l_intern_kenmerk;

          bijwerken_koppeling (p_rle_numrelatie => l_relatienummer
                              ,p_rle_numrelatie_voor => l_relatienummer_ex
                              ,p_dat_begin => t_ex_partner(i).begindatum_huwelijk
                              ,p_dat_einde => t_ex_partner(i).einddatum_huwelijk
                              ,p_kenmerk => l_intern_kenmerk
                              ,p_foutmelding => l_error
                              );

          if l_error is not null
          then
            l_foutmelding := 'Er is een fout opgetreden bij het bijwerken van de relatiekoppeling.';
            raise e_foutmelding;
          end if;

        else
        --ex-partner niet gevonden dus toevoegen
          stm_util.debug ('Start toevoegen ex');

          select case when dwe_wrddom_geslacht = 'M' then 'V'  else 'M' end
                ,to_date( '01-07-'|| to_char( datgeboorte, 'YYYY' ) ,'dd_mm-yyyy')
          into l_geslacht, l_datgeboorte
          from rle_relaties rle
          where numrelatie = l_relatienummer;

          rle_rle_toev (p_numrelatie => l_relatienummer_ex
                       ,p_namrelatie => nvl(t_ex_partner(i).naam, 'Onbekende ex')
                       ,p_rol_codrol => 'BG'
                       ,p_indinstelling => 'N'
                       ,p_wrddom_geslacht => nvl(t_ex_partner(i).geslacht, l_geslacht)
                       ,p_wrddom_vvgsl => null
                       ,p_voorletter => nvl(substr(t_ex_partner(i).voorletters, 1, 6), 'X')
                       ,p_voornaam => null
                       ,p_handelsnaam => null
                       ,p_datbegin => null
                       ,p_dateinde => null
                       ,p_datoprichting => null
                       ,p_datgeboorte => nvl(t_ex_partner(i).geboortedatum, l_datgeboorte)
                       ,p_code_gebdat_fictief => null
                       ,p_datoverlijden => t_ex_partner(i).overlijdensdatum
                       ,p_datregoverlijdenbevestiging => case when t_ex_partner(i).overlijdensdatum is null then null else sysdate end
                       ,p_code_aanduiding_naamgebruik => 'E'
                       ,p_naam_partner => null
                       ,p_vvgsl_partner => null
                       ,p_datemigratie => null
                       ,p_wrddom_gewatitel => null
                       ,p_ind_succes => l_ind_succes
                       ,p_melding => l_error
                       ,p_externe_code => l_persoonsnummer_ex
                       );

           stm_util.debug ('Relatienummer ex: '||l_relatienummer_ex);
           stm_util.debug ('Persoonsnummer ex: '||l_persoonsnummer_ex);

           if l_ind_succes = 'N'
           then
            l_foutmelding := 'Er is een fout opgetreden bij het opvoeren van de ex-partner.';
             raise e_foutmelding;
           end if;

           update rle_bron_ex_partners
           set relatienummer_ex = l_relatienummer_ex
              ,persoonsnummer_ex = l_persoonsnummer_ex
           where intern_kenmerk = l_intern_kenmerk;

           if t_ex_partner(i).a_nummer is not null
           then

             begin
             -- A-Nummer opvoeren
               rle_m16_rec_verwerk(p_coderelatie => t_ex_partner(i).a_nummer
                                  ,p_codsysteemcomp => 'GBA'
                                  ,p_codrol => 'PR'
                                  ,p_numrelatie => l_relatienummer_ex
                                  ,p_datingang =>sysdate
                                  ,p_dateinde => null
                                 );
             exception
               when dup_val_on_index
               then
                 l_error := 'Het A-nummer is al gekoppeld aan een ander persoon.';
                 raise e_foutmelding;
             end;
           end if;

           if t_ex_partner(i).bsn is not null
           then

             begin
         --BSN opvoeren
               rle_m16_rec_verwerk(p_coderelatie => t_ex_partner(i).bsn
                                  ,p_codsysteemcomp => 'SOFI'
                                  ,p_codrol => 'PR'
                                  ,p_numrelatie => l_relatienummer_ex
                                  ,p_datingang => sysdate
                                  ,p_dateinde => null
                                  );
               exception
                 when dup_val_on_index
                 then
                   l_error := 'Het Burgerservicenummer is al gekoppeld aan een ander persoon.';
                   raise e_foutmelding;
               end;

           end if;

         --Administratie koppelen
           rle_rie.kop_rkn_rie(p_rle_numrelatie_dlnmr => l_relatienummer
                              ,p_rle_numrelatie_vrwnt => l_relatienummer_ex
                              ,p_error                => l_error
                              );

           if l_error is not null
           then
            l_foutmelding := 'Er is een fout opgetreden bij het koppelen van de administratie aan de ex-partner.';
             raise e_foutmelding;
           end if;


           rle_get_wps_stt (p_pcode => t_ex_partner(i).postcode
                           ,p_huisnr  => t_ex_partner(i).huisnummer
                           ,p_woonplaats => l_woonplaats
                           ,p_straat => l_straat
                           );

           if l_woonplaats is not null and l_straat is not null
           then
             l_land := 'NL';
           else

             begin

               select codland
               into l_land
               from rle_landen
               where naam = nvl(t_ex_partner(i).land, 'ONBEKEND')
                 and nvl(dat_einde, sysdate) >= sysdate;

             exception
             when dup_val_on_index
             then
               l_error := 'GBA-landcode is niet bekend in de landentabel van de GBA-component.';
               raise e_foutmelding;
             end;

           end if;

           l_ras_id := rle_verwerk_ras (pin_ras_id => null
                                       ,pin_ras_rle_numrelatie => l_relatienummer_ex
                                       ,piv_ras_rol_codrol => 'PR'
                                       ,piv_ras_dwe_wrddom_srtadr => 'DA'
                                       ,pid_ras_datingang => nvl(t_ex_partner(i).geboortedatum, l_datgeboorte)
                                       ,pid_ras_dateinde => null
                                       ,pin_ras_ads_numadres => null
                                       ,piv_ras_provincie => null
                                       ,piv_ras_locatie => null
                                       ,piv_ras_ind_woonboot => 'N'
                                       ,piv_ras_ind_woonwagen => 'N'
                                       ,piv_ads_codland => l_land
                                       ,piv_ads_postcode => nvl(t_ex_partner(i).postcode, 'ONB')
                                       ,pin_ads_huisnummer => t_ex_partner(i).huisnummer
                                       ,piv_ads_toevhuisnum => t_ex_partner(i).huisnummer_toev
                                       ,piv_ads_straatnaam => nvl(t_ex_partner(i).straat, 'Onbekend')
                                       ,piv_ads_woonplaats => nvl(t_ex_partner(i).woonplaats, 'ONB')
                                     );

           rle_gba_wijzig_gba (pin_numrelatie => l_relatienummer_ex
                              ,piv_gba_status => 'OV'
                              ,piv_gba_afnemers_ind => 'N'
                              ,pin_gba_aanmeldingsnummer => null
                              );

           bijwerken_koppeling (p_rle_numrelatie => l_relatienummer
                               ,p_rle_numrelatie_voor => l_relatienummer_ex
                               ,p_dat_begin => t_ex_partner(i).begindatum_huwelijk
                               ,p_dat_einde => t_ex_partner(i).einddatum_huwelijk
                               ,p_kenmerk => l_intern_kenmerk
                               ,p_foutmelding => l_error
                               );

           if l_error is not null
           then
             l_foutmelding := 'Er is een fout opgetreden bij het bijwerken van de relatiekoppeling.';
             raise e_foutmelding;
           end if;

           open c_apn(l_persoonsnummer_ex);
           fetch c_apn into r_apn;
           close c_apn;

           stm_util.debug ('apn_id ex: '||r_apn.id);

           gba_indicatie.doorgeven_aan_rle := 'N' ;

           per_apn_overzetten (r_apn.id);

           gba_indicatie.doorgeven_aan_rle := 'J' ;

           if t_ex_partner(i).overlijdensdatum is not null
           then

             stm_util.debug ('Overlijden: '||t_ex_partner(i).overlijdensdatum);

             open c_per (l_persoonsnummer_ex);
             fetch c_per into r_per;
             close c_per;

              gba_prs_muteren (l_persoonsnummer_ex
                              ,r_per.sofinummer
                              ,r_per.a_nummer
                              ,r_per.naam
                              ,r_per.voorletters
                              ,r_per.voorvoegsels
                              ,r_per.geboortedatum
                              ,nvl(t_ex_partner(i).geslacht, l_geslacht)
                              ,t_ex_partner(i).overlijdensdatum
                              ,sysdate
                              ,null
                              ,null
                              ,null
                              ,l_verwerkt
                              ,l_error
                              ,l_status
                              ,l_indicatie);

              if l_error  is not null
              then
                l_foutmelding := 'Er is een fout opgetreden bij het bijwerken van de GBA-tabellen.';
                raise e_foutmelding;
              end if;

           end if;

           update rle_bron_ex_partners
           set ind_ex_aangemaakt_rle = 'J'
           where intern_kenmerk = l_intern_kenmerk;

        end if;
      elsif t_ex_partner(i).persoonsnummer is not null
      then

        l_relatienummer_ex := rle_lib.get_relatienr_persoon (t_ex_partner(i).persoonsnummer);

        bijwerken_koppeling (p_rle_numrelatie => l_relatienummer
                            ,p_rle_numrelatie_voor => l_relatienummer_ex
                            ,p_dat_begin => t_ex_partner(i).begindatum_huwelijk
                            ,p_dat_einde => t_ex_partner(i).einddatum_huwelijk
                            ,p_kenmerk => l_intern_kenmerk
                            ,p_foutmelding => l_error
                            );

        if l_error is not null
        then
          l_foutmelding := 'Er is een fout opgetreden bij het bijwerken van de relatiekoppeling.';
          raise e_foutmelding;
        end if;
      end if;

      update rle_bron_ex_partners
      set ind_verwerkt = 'J'
      where intern_kenmerk = l_intern_kenmerk;

    end loop;
  end if;

exception
when e_foutmelding
then
  rollback to start_toevoegen;
  p_ind_succes := 'N';
  p_foutmelding := l_foutmelding;
  stm_util.debug ('Error: '||l_error);

  update rle_bron_ex_partners
  set foutmelding = l_error
     ,ind_ex_aangemaakt_rle = 'N'
     ,ind_rel_koppeling_aangemaakt = 'N'
     ,ind_rel_koppeling_gewijzigd = 'N'
     ,ind_opgenomen_in_werkbak = 'J'
  where intern_kenmerk = l_intern_kenmerk;

when others
then
  rollback to start_toevoegen;
  p_ind_succes := 'N';

  t_sqlerrm := substr(sqlerrm, 1, 2000);
  stm_util.debug('others '||t_sqlerrm);

  p_foutmelding := 'Er is een onverwachte fout opgetreden bij het opvoeren van de ex-partner.';

  update rle_bron_ex_partners
  set foutmelding = l_error
     ,ind_ex_aangemaakt_rle = 'N'
     ,ind_rel_koppeling_aangemaakt = 'N'
     ,ind_rel_koppeling_gewijzigd = 'N'
     ,ind_opgenomen_in_werkbak = 'J'
  where intern_kenmerk = l_intern_kenmerk;

end verwerk_bericht;
PROCEDURE UPDATE_INDICATIE_BRONTABEL
 (P_NR_EBR IN VARCHAR2
 ,P_IND_WERKBAK IN VARCHAR2
 ,P_IND_VERWERKT IN VARCHAR2
 ,P_IND_FOUT_VERWERKING IN VARCHAR2
 )
 IS

cursor c_bron (b_intern_kenmerk in varchar2)
is
  select bestandsnaam
  from rle_bron_ex_partners
  where intern_kenmerk = b_intern_kenmerk
;

l_berichtregel    varchar2(8000);
l_key             varchar2(100);
l_dummy           varchar2(5);
l_value           varchar2(2000);
l_intern_kenmerk  varchar2(10);
l_persoonsnummer  varchar2(15);
l_bestandsnaam    varchar2(50);
begin


  get_wrd_tbn (p_ebr_id => p_nr_ebr
              ,p_return_value => l_berichtregel
              );

  while l_berichtregel is not null
  loop

    l_key := comp_stm.stm_get_word(l_berichtregel);
    l_dummy := comp_stm.stm_get_word(l_berichtregel);
    l_value := comp_stm.stm_get_word(l_berichtregel);

    if l_key = 'KENMERK'
    then
      l_intern_kenmerk := l_value;
    elsif l_key = 'PERSOONSNUMMER'
    then
      l_persoonsnummer := l_value;
    end if;


    open c_bron (b_intern_kenmerk => l_intern_kenmerk);
    fetch c_bron into l_bestandsnaam;
    close c_bron;

    if p_ind_verwerkt = 'J'
    then

      update rle_bron_ex_partners
      set ind_verwerkt = 'J'
      where intern_kenmerk = l_intern_kenmerk;

    elsif p_ind_werkbak = 'J'
        and p_ind_fout_verwerking = 'N'
    then

      update rle_bron_ex_partners
      set ind_opgenomen_in_werkbak = 'J'
      where persoonsnummer = l_persoonsnummer
      and   bestandsnaam = l_bestandsnaam;

    elsif p_ind_werkbak = 'N'
        and p_ind_verwerkt = 'N'
        and p_ind_fout_verwerking = 'N'
    then

      update rle_bron_ex_partners
      set ind_opgenomen_in_werkbak = 'N'
      where intern_kenmerk = l_intern_kenmerk;

    elsif p_ind_fout_verwerking = 'J'
    then

      update rle_bron_ex_partners
      set ind_opgenomen_in_werkbak = 'J'
         ,ind_ex_aangemaakt_rle = 'N'
         ,ind_rel_koppeling_aangemaakt = 'N'
         ,ind_rel_koppeling_gewijzigd = 'N'
      where persoonsnummer = l_persoonsnummer
      and   bestandsnaam = l_bestandsnaam;
    end if;

    l_intern_kenmerk := null;

    end loop;

end update_indicatie_brontabel;
end rle_ex_partners;
/