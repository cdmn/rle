CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_BRIEF IS
--------------------------------------------------------------------------------
-- Datum      Naam       Reden
-- ---------- ---------- -------------------------------------------------------
-- 22-07-2010 A. Aten    Creatie
-- 25-01-2011 A. Aten    INC 211310: Extra SDS_CODES voor fictieve perioden
-- 02-03-2011 M. Bouwens Cursoren perioden aangepast
--                       en leeg ipv Nee bij toekennen op
-- 15-03-2011 M. Bouwens Extra SDS code ES, aansluitende periode op 1 regel
--                       en geen periodes na 31-12-1994 en afkappen op 31-12-1994
-- 22-03-2011 M. Bouwens Niet gebruik maken van begin_deelname en eind_deelname uit view
--                         maar zelf berekenen (vanwege sds_code ES)
-- 26-11-2015 R.A. Pargas RLE_RLE_GET_AANHEF(fo in rle I_072) en RLE_GET_VERZENDNAAM(fo _rle09211) toegevoegd in deze package, om parameter tbv engels toe te voegen.
--                        doordat deze twee losse procedure/functions zijn, heb je de mogelijkheid niet om overlode toe te passen.
--                        Besloten om deze twee procedure in deze package toe te voegen.
--                        door nieuwe parameter toe te voegen, moet +/- 40 andere objecten aangepast worden.
--                        dit is niet gewenst, omdat 90% van deze objecten de nieuwe parameter niet gebruiken.
--                        voor de toekomst geeft deze nieuwe procedure binnen het package meer flexibiliteit om uit te breiden en afhankelijkheden te ontwijken.
--                        overigens komen deze 40 andere objecten in het toekomst te vervallen ivm MN30.
--------------------------------------------------------------------------------

FUNCTION MAAK_VAR
 (P_NUMMER IN NUMBER
 )
 RETURN VARCHAR2;
FUNCTION MAAK_NUM
 (P_NUMMER IN NUMBER
 )
 RETURN VARCHAR2;
FUNCTION GET_DOC_TYPE
 (P_DOC_TYPE IN VARCHAR2
 ,P_ADM IN VARCHAR2
 )
 RETURN VARCHAR2;


/* Aanmaken van brief RLE028 */
PROCEDURE MAAK_RLE028
 (P_NUMRELATIE IN NUMBER
 ,P_ADM IN VARCHAR2
 )
 IS
  --
    cursor c_avg(
      cp_numrelatie   in   rle_relaties.numrelatie%type
    , cp_rgg_code     in   varchar2
    )
    is
      select   datum_begin
             , least( datum_einde
                    , to_date( '31-12-1994'
                             , 'DD-MM-YYYY'
                             )
                    ) datum_einde
             , round( deeltijdfactor * 100 ) deeltijdfactor
      from     ( select distinct case
                                   when start_periode = 'J'
                                     then datum_begin
                                   else lag( datum_begin ) over( partition by rle_numrelatie, sds_code order by datum_begin )
                                 end datum_begin
                               , case
                                   when einde_periode = 'J'
                                     then datum_einde
                                   else lead( datum_einde ) over( partition by rle_numrelatie, sds_code order by datum_begin )
                                 end datum_einde
                               , deeltijdfactor
                from            ( select rle_numrelatie
                                       , sds_code
                                       , datum_begin
                                       , datum_einde
                                       , case
                                           when datum_begin - 1 = vorige_datum_einde
                                             then 'N'
                                           else 'J'
                                         end start_periode
                                       , case
                                           when datum_einde + 1 = volgende_datum_begin
                                             then 'N'
                                           else 'J'
                                         end einde_periode
                                       , deeltijdfactor
                                 from   ( select rle_numrelatie
                                               , sds_code
                                               , datum_begin
                                               , datum_einde
                                               , lag( ovt.datum_einde ) over( partition by ovt.sds_code, ovt.deeltijdfactor order by ovt.sds_code
                                                , ovt.deeltijdfactor
                                                , ovt.datum_begin ) vorige_datum_einde
                                               , lead( ovt.datum_begin ) over( partition by ovt.sds_code, ovt.deeltijdfactor order by ovt.sds_code
                                                , ovt.deeltijdfactor
                                                , ovt.datum_begin ) volgende_datum_begin
                                               , deeltijdfactor
                                         from   ovt_v_deelnames ovt
                                         where  ovt.rle_numrelatie = cp_numrelatie
                                         and    ovt.sds_code in( 'AV', 'ES' )
                                         and    ovt.rgg_code = cp_rgg_code
                                         and    ovt.datum_einde < to_date( '01-01-1995'
                                                                         , 'dd-mm-yyyy'
                                                                         )))
                where           start_periode = 'J'
                or              einde_periode = 'J' )
      order by datum_begin desc
             , datum_einde;

    --
    cursor c_fic(
      cp_numrelatie   in   rle_relaties.numrelatie%type
    , cp_rgg_code     in   varchar2
    )
    is
      select   datum_begin
             , least( datum_einde
                    , to_date( '31-12-1994'
                             , 'DD-MM-YYYY'
                             )
                    ) datum_einde
             , round( deeltijdfactor * 100 ) deeltijdfactor
      from     ( select distinct case
                                   when start_periode = 'J'
                                     then datum_begin
                                   else lag( datum_begin ) over( partition by rle_numrelatie, sds_code order by datum_begin )
                                 end datum_begin
                               , case
                                   when einde_periode = 'J'
                                     then datum_einde
                                   else lead( datum_einde ) over( partition by rle_numrelatie, sds_code order by datum_begin )
                                 end datum_einde
                               , deeltijdfactor
                from            ( select rle_numrelatie
                                       , sds_code
                                       , datum_begin
                                       , datum_einde
                                       , case
                                           when datum_begin - 1 = vorige_datum_einde
                                             then 'N'
                                           else 'J'
                                         end start_periode
                                       , case
                                           when datum_einde + 1 = volgende_datum_begin
                                             then 'N'
                                           else 'J'
                                         end einde_periode
                                       , deeltijdfactor
                                 from   ( select rle_numrelatie
                                               , sds_code
                                               , datum_begin
                                               , datum_einde
                                               , lag( ovt.datum_einde ) over( partition by ovt.sds_code, ovt.deeltijdfactor order by ovt.sds_code
                                                , ovt.deeltijdfactor
                                                , ovt.datum_begin ) vorige_datum_einde
                                               , lead( ovt.datum_begin ) over( partition by ovt.sds_code, ovt.deeltijdfactor order by ovt.sds_code
                                                , ovt.deeltijdfactor
                                                , ovt.datum_begin ) volgende_datum_begin
                                               , deeltijdfactor
                                         from   ovt_v_deelnames ovt
                                         where  ovt.rle_numrelatie = cp_numrelatie
                                         and    ovt.sds_code in( 'PV', 'FVP', 'VV', 'BOF', 'RD', 'BR' )   -- uitgebreid ivm inc 211310
                                         and    ovt.rgg_code = cp_rgg_code
                                         and    ovt.datum_einde < to_date( '01-01-1995'
                                                                         , 'dd-mm-yyyy'
                                                                         )))
                where           start_periode = 'J'
                or              einde_periode = 'J' )
      order by datum_begin desc
             , datum_einde;
  --
  /* 2e poging
  cursor c_avg( cp_numrelatie	in rle_relaties.numrelatie%type
              , cp_rgg_code 	in varchar2
              )
  is
    select distinct
              ovt.ingangsdatum_deelname  datum_begin
    ,         ovt.einddatum_deelname     datum_einde
    ,         round( ovt.deeltijdfactor * 100 )   deeltijdfactor
    from      ovt_v_deelnames     ovt
    where     ovt.rle_numrelatie = cp_numrelatie
    and       ovt.sds_code in ( 'AV', 'ES' )
    and       ovt.rgg_code = cp_rgg_code
    and       ovt.datum_einde < to_date( '01-01-1995', 'dd-mm-yyyy' )
    order by  ovt.ingangsdatum_deelname desc
            , ovt.einddatum_deelname ;

  --
  cursor c_fic( cp_numrelatie	in rle_relaties.numrelatie%type
              , cp_rgg_code 	in varchar2
              )
  is
    select distinct
              ovt.ingangsdatum_deelname datum_begin
    ,         ovt.einddatum_deelname    datum_einde
    ,         round( ovt.deeltijdfactor * 100 )   deeltijdfactor
    from      ovt_v_deelnames     ovt
    where     ovt.rle_numrelatie = cp_numrelatie
    and       ovt.sds_code in ( 'PV', 'FVP', 'VV', 'BOF', 'RD', 'BR' )
    and       ovt.rgg_code = cp_rgg_code
    and       ovt.datum_einde < to_date( '01-01-1995', 'dd-mm-yyyy' )
    order by  ovt.ingangsdatum_deelname desc
            , ovt.einddatum_deelname ;
  */
  --
  /* was als volgt
  cursor c_avg( cp_numrelatie	in rle_relaties.numrelatie%type
              , cp_rgg_code 	in varchar2
              )
  is
    select      datum_begin
    ,           datum_einde
    ,           round( deeltijdfactor * 100 )   deeltijdfactor
    from        (
                  select    ovt.datum_begin
                  ,         last_value( ovt.datum_einde ) over ( partition by ovt.deeltijdfactor, ovt.ingangsdatum_deelname
                                                                            , ovt.einddatum_deelname order by ovt.datum_begin
                                                                             range between current row and unbounded following ) datum_einde
                  ,         ovt.deeltijdfactor
                  ,         case
                              when  ovt.datum_begin - 1 = lag ( ovt.datum_einde ) over ( partition by ovt.deeltijdfactor, ovt.ingangsdatum_deelname
                                                                                                    , ovt.einddatum_deelname  order by ovt.datum_begin )
                              then  'N'
                              else  'J'
                            end gewijzigd
                  from      ovt_v_deelnames     ovt
                  where     ovt.rle_numrelatie = cp_numrelatie
                  and       ovt.sds_code in ( 'AV' )
                  and       ovt.rgg_code = cp_rgg_code
                  and       ovt.datum_einde < to_date( '01-01-1995', 'dd-mm-yyyy' )
                ) x
    where       x.gewijzigd = 'J'
    order by    x.datum_begin desc
    ,           x.datum_einde;
  --
  cursor c_fic( cp_numrelatie	in rle_relaties.numrelatie%type
              , cp_rgg_code 	in varchar2
              )
  is
    select      datum_begin
    ,           datum_einde
    ,           round( deeltijdfactor * 100 )   deeltijdfactor
    from        (
                  select    ovt.datum_begin
                  ,         last_value( ovt.datum_einde ) over ( partition by ovt.deeltijdfactor, ovt.ingangsdatum_deelname
                                                                            , ovt.einddatum_deelname order by ovt.datum_begin
                                                                   range between current row and unbounded following) datum_einde
                  ,         ovt.deeltijdfactor
                  ,         case
                              when  ovt.datum_begin - 1 = lag ( ovt.datum_einde ) over ( partition by ovt.deeltijdfactor , ovt.ingangsdatum_deelname
                                                                                                    , ovt.einddatum_deelname order by datum_begin )
                              then  'N'
                              else  'J'
                            end gewijzigd
                  from      ovt_v_deelnames     ovt
                  where     ovt.rle_numrelatie = cp_numrelatie
                  and       ovt.sds_code in ( 'PV', 'FVP', 'VV', 'BOF', 'RD', 'BR' ) -- uitgebreid ivm inc 211310
                  and       ovt.rgg_code = cp_rgg_code
                  and       ovt.datum_einde < to_date( '01-01-1995', 'dd-mm-yyyy' )
                ) x
    where       x.gewijzigd = 'J'
    order by    x.datum_begin desc
    ,           x.datum_einde;
  */
  --
  l_main_doc            opu_direct.main_doc;
  l_groep               opu_direct.groep;
  --
  l_doc_id              number;
  l_ind_openstaand      varchar2( 1 );
  --
  c_tab                 constant number := 25;
  --
  l_idx                 number;
  l_groepnummer         number;
  --
  l_var_teller          number;
  l_num_teller          number;
  l_max_regels          number;
  --
  l_doc_type            varchar2( 100 );
  --
  l_ingangsdatum_op     date;
  l_indicatie_np        number;
  l_indicatie_aow_anw   number;
  --
begin
  --
  stm_context.set_administratie_context( p_administratiecode => p_adm );
  --
  l_doc_type := get_doc_type( 'RLE028', p_adm );
  --
  l_groepnummer := 1;
  l_idx := 1;
  --
  begin
    select  max( datum )
    into    l_ingangsdatum_op
    from    ret_v_ugs_personen
    where   sgs_code in ( 'OP' )
    and     status = 'Aktueel'
    and     rle_numrelatie = p_numrelatie
    and     rgg_code = p_adm;
  exception
    when others
    then
      l_ingangsdatum_op := null;
  end;
  l_main_doc( l_idx ).groepnaam := l_doc_type;
  l_main_doc( l_idx ).groepnummer := l_groepnummer;
  l_main_doc( l_idx ).naam := 'INGANGSDATUM_OP';
  l_main_doc( l_idx ).waarde_char := case
                                       when l_ingangsdatum_op is null
                                       then
                                         ''   -- 'Nee'
                                       else
                                         'Ja, ingangsdatum: ' || to_char( l_ingangsdatum_op, 'dd-mm-yyyy' )
                                     end;
  --
  stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
  --
  l_idx := l_idx + 1;
  --
  select  count( 1 )
  into    l_indicatie_np
  from    ret_v_ugs_personen
  where   sgs_code in ( 'NP' )
  and     status = 'Aktueel'
  and     rle_numrelatie = p_numrelatie
  and     rgg_code = p_adm;
  --
  l_main_doc( l_idx ).groepnaam := l_doc_type;
  l_main_doc( l_idx ).groepnummer := l_groepnummer;
  l_main_doc( l_idx ).naam := 'INDICATIE_NP';
  l_main_doc( l_idx ).waarde_char := case
                                       when l_indicatie_np = 0
                                       then
                                         'Nee'
                                       else
                                         'Ja'
                                     end;
  --
  stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
  --
  l_idx := l_idx + 1;
  --
  select    count( 1 )
  into      l_indicatie_aow_anw
  from      ovt_v_deelnemingen
  where     rle_numrelatie = p_numrelatie
  and       sds_code = 'AV'
  and       datum_begin >= to_date( '01-01-1971', 'dd-mm-yyyy' )
  and       rgg_code = p_adm;
  --
  l_main_doc( l_idx ).groepnaam := l_doc_type;
  l_main_doc( l_idx ).groepnummer := l_groepnummer;
  l_main_doc( l_idx ).naam := 'INDICATIE_AOW_ANW';
  l_main_doc( l_idx ).waarde_char := case
                                       when l_indicatie_aow_anw = 0
                                       then
                                         'Nee'
                                       else
                                         'Ja'
                                     end;                                   -- Nog verder in te vullen
  --
  stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
  --
  l_idx := l_idx + 1;
  --
  l_main_doc( l_idx ).groepnaam := l_doc_type;
  l_main_doc( l_idx ).groepnummer := l_groepnummer;
  l_main_doc( l_idx ).naam := 'REGELING';
  l_main_doc( l_idx ).waarde_char := p_adm;
  --
  stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
  --
  l_idx := l_idx + 1;
  --
  l_var_teller := 1;
  l_num_teller := 1;
  l_max_regels := 10;
  --
  <<avg_perioden>>
  for r_avg in c_avg( p_numrelatie, p_adm ) loop
    --
    stm_util.debug( 'l_num_teller: ' || l_num_teller );
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := to_char( r_avg.datum_begin, 'dd-mm-yyyy' );
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := to_char( r_avg.datum_einde, 'dd-mm-yyyy' );
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_num( l_num_teller );
    l_main_doc( l_idx ).waarde_char := r_avg.deeltijdfactor;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_num_teller := l_num_teller + 1;
    --
    -- Check of we het maximum aantal gedefinieerde regels hebben behaald
    exit avg_perioden when l_num_teller > l_max_regels;
    --
  end loop avg_perioden;
  --
  --if l_num_teller <= l_max_regels then
  -- Voeg de overblijvende gedefinieerde variabelen aan
  for i in l_num_teller .. l_max_regels loop
    stm_util.debug( 'l_num_teller: ' || l_num_teller );
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := null;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := null;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_num( l_num_teller );
    l_main_doc( l_idx ).waarde_char := null;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_num_teller := l_num_teller + 1;
    --
  end loop;
  --
  -- Fictieve diensgelegenheid
  l_max_regels := 15;
  --
  <<fic_perioden>>
  for r_fic in c_fic( p_numrelatie, p_adm ) loop
    --
    stm_util.debug( 'l_num_teller: ' || l_num_teller );
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := to_char( r_fic.datum_begin, 'dd-mm-yyyy' );
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := to_char( r_fic.datum_einde, 'dd-mm-yyyy' );
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_num( l_num_teller );
    l_main_doc( l_idx ).waarde_char := r_fic.deeltijdfactor;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_num_teller := l_num_teller + 1;
    --
    -- Check of we het maximum aantal gedefinieerde regels hebben behaald
    exit fic_perioden when l_num_teller > l_max_regels;
    --
  end loop fic_perioden;
  --
  -- Voeg de overblijvende gedefinieerde variabelen aan
  for i in l_num_teller .. l_max_regels loop
    stm_util.debug( 'l_num_teller: ' || l_num_teller );
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := null;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_var( l_var_teller );
    l_main_doc( l_idx ).waarde_char := null;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_var_teller := l_var_teller + 1;
    --
    l_main_doc( l_idx ).groepnaam := l_doc_type;
    l_main_doc( l_idx ).groepnummer := l_groepnummer;
    l_main_doc( l_idx ).naam := maak_num( l_num_teller );
    l_main_doc( l_idx ).waarde_char := null;
    --
    stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
    --
    l_idx := l_idx + 1;
    --
    l_num_teller := l_num_teller + 1;
    --
  end loop;
  --
  l_main_doc( l_idx ).groepnaam := l_doc_type;
  l_main_doc( l_idx ).groepnummer := l_groepnummer;
  l_main_doc( l_idx ).naam := 'INGANGSDATUM_KORT_OP';
  l_main_doc( l_idx ).waarde_char := case
                                       when l_ingangsdatum_op is null
                                       then
                                         'Nee'
                                       else
                                         'Ja, ingangsdatum: ' || to_char( l_ingangsdatum_op, 'dd-mm-yyyy' )
                                     end;
  --
  stm_util.debug( rpad( l_main_doc( l_idx ).naam, c_tab ) || ' : ' || l_main_doc( l_idx ).waarde_char );
  --
  opu_direct.opu_data_vullen( p_dty_srt =>            l_doc_type
                            , p_dty_numversie =>      null
                            , p_overwat =>            p_numrelatie
                            , p_srt_overwat =>        'PR'
                            , p_codsysteem =>         'RLE'
                            , p_overwie =>            p_numrelatie
                            , p_code_rol =>           'PR'
                            , p_indaanmaken =>        'N'
                            , p_datum =>              sysdate
                            , p_bron =>               null
                            , p_ind_commit =>         'N'
                            , p_doc_id =>             l_doc_id          -- out
                            , p_ind_openstaand =>     l_ind_openstaand  -- out
                            , p_main_doc =>           l_main_doc
                            , p_groep =>              l_groep
                            );
  stm_util.debug( l_doc_id );
  --
end;
FUNCTION MAAK_VAR
 (P_NUMMER IN NUMBER
 )
 RETURN VARCHAR2
 IS
begin
  --
  return 'VAR'  || trim( to_char( p_nummer, '099' ) ) || '_D';
  --
end;
FUNCTION MAAK_NUM
 (P_NUMMER IN NUMBER
 )
 RETURN VARCHAR2
 IS
begin
  --
  return 'NUM'  || trim( to_char( p_nummer  ) ) || '_D';
  --
end;
FUNCTION GET_DOC_TYPE
 (P_DOC_TYPE IN VARCHAR2
 ,P_ADM IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
begin
  --
  return
    case
      when p_adm in ( 'ALG', 'PMT' )
      then
        p_doc_type
      when p_adm = 'PME'
      then
        p_doc_type || '-1'
      when p_adm = 'KVD'
      then
        p_doc_type || '-2'
    end;
  --
end;
/* Get aanhef incl eng output. copy van rle_rle_get_aanhef. */
PROCEDURE RLE_RLE_GET_AANHEF
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_AANHEF OUT VARCHAR2
 ,P_AANSP_TITEL OUT VARCHAR2
 ,P_AANSP_TITEL_KLEIN OUT VARCHAR2
 ,P_AANHEF_ENG OUT VARCHAR2
 ,P_AANSP_TITEL_ENG OUT VARCHAR2
 )
 IS
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
-- is een copy van procedure RLE_RLE_GET_AANHEF.
-- fo in rle I_072
/* C_RLE_001 */
--select	*
select namrelatie
,      indinstelling
,      dwe_wrddom_vvgsl
,      dwe_wrddom_geslacht
,      dwe_wrddom_gewatitel
,      naam_partner
,      vvgsl_partner
,      code_aanduiding_naamgebruik
,      datoverlijden
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Program Data
l_opmaaknaam VARCHAR2(500);
l_aanhef varchar2(100);
L_PARTNER_FNAAM VARCHAR2(500);
L_PARTNER_VOORVOEGSEL VARCHAR2(40);
L_EIGEN_VOORVOEGSEL VARCHAR2(40);
R_RLE_001 C_RLE_001%ROWTYPE;
L_IND_IBM VARCHAR2(1) := 'N';
L_EIGEN_NAAM VARCHAR2(184);
L_LENGTE_IBM NUMBER := 40;
L_EIGEN_FNAAM VARCHAR2(500);
L_PARTNER_NAAM VARCHAR2(184);
l_aansp_titel  varchar2(100);
-- YRP 25-11-2015 WCP 2015/2016
l_aanhef_eng       varchar2(100);
l_aansp_titel_eng  varchar2(100);

-- Sub-Program Unit Declarations
/* Gebruikt in RLE_M11_GET_NAW om de voorletters van een naam op te maken */
-- PL/SQL Block
begin
  stm_util.debug('Begin RLE_BRIEF.RLE_RLE_GET_AANHEF');
  l_opmaaknaam := NULL ;
  -- Ophalen eigen naam
  OPEN c_rle_001( P_NUMRELATIE ) ;
  FETCH c_rle_001 INTO r_rle_001 ;
  IF c_rle_001%NOTFOUND
  THEN
      CLOSE c_rle_001 ;
      RETURN;
  END IF ;
  close c_rle_001;
-- eerst de aanspreekwijze bepalen

   l_aanhef := 'heer, mevrouw ' ;
   l_aansp_titel := l_aanhef;

   l_aanhef_eng      :=  'Mr, Madam';
   l_aansp_titel_eng := l_aanhef_eng;

  IF r_rle_001.indinstelling =  'N'
  THEN
    l_aanhef := NULL ;
    IF r_rle_001.dwe_wrddom_gewatitel IS NOT NULL -- is er een code voor gewenste aanspreektitel
    THEN
    stm_util.debug('begin RFE_RFW_GET_WRD ');
            RFE_RFW_GET_WRD
                ( 'GWT'
               , r_rle_001.dwe_wrddom_gewatitel
               , l_ind_ibm
               , l_lengte_ibm
               , l_aanhef
              ) ;
       l_aansp_titel := l_aanhef;

       if stm_util.t_debug
       then
         stm_util.debug('l_aanhef: ' || l_aanhef);
         stm_util.debug('l_aansp_titel: ' || l_aansp_titel);
       end if;
       if upper(l_aansp_titel) = upper('De heer')
       then
        l_aanhef_eng      :=  'Mr';
        l_aansp_titel_eng := l_aanhef_eng;
       elsif upper(l_aansp_titel) = upper('Mevrouw')
       then
         l_aanhef_eng      :=  'Madam';
         l_aansp_titel_eng := l_aanhef_eng;
       end if;

      if stm_util.t_debug
      then
        stm_util.debug('l_aanhef_eng: ' || l_aanhef_eng);
        stm_util.debug('l_aansp_titel_eng: ' || l_aansp_titel_eng);
        stm_util.debug('Einde RFE_RFW_GET_WRD ');
      end if;
    ELSIF r_rle_001.dwe_wrddom_geslacht = 'M'
    THEN
      l_aanhef := 'heer' ;
      l_aansp_titel := 'De heer';

      l_aanhef_eng      :=  'Mr';
      l_aansp_titel_eng := l_aanhef_eng;

    ELSIF r_rle_001.dwe_wrddom_geslacht = 'V'
    THEN
      l_aanhef := 'mevrouw' ;
      l_aansp_titel := 'Mevrouw';

      l_aanhef_eng      :=  'Madam';
      l_aansp_titel_eng := l_aanhef_eng;

    END IF ;
--  dan de opgemaakte naam bepalen tbv AANHEF
      -- Zoek voorvoegsel
    IF r_rle_001.dwe_wrddom_vvgsl IS NOT NULL
    THEN
        RFE_RFW_GET_WRD
                ( 'VVL'
                , r_rle_001.dwe_wrddom_vvgsl
                , l_ind_ibm
                , l_lengte_ibm
                , l_eigen_voorvoegsel
                ) ;
         --
         -- RTJ 26-11-2003 Call 91115 Eerste letter voorvoegsel moet hoofdletter worden
         --
         l_eigen_voorvoegsel := UPPER(substr(l_eigen_voorvoegsel,1,1))||substr(l_eigen_voorvoegsel,2);
         -- EINDE RTJ
    END IF ;
    IF r_rle_001.code_aanduiding_naamgebruik in ('P','V','N')
    THEN
       IF r_rle_001.vvgsl_partner IS NOT NULL
       THEN
            RFE_RFW_GET_WRD
                   ( 'VVL'
                   , r_rle_001.vvgsl_partner
                   , l_ind_ibm
                   , l_lengte_ibm
                   , l_partner_voorvoegsel
                   ) ;
         --
         -- RTJ 02-12-2003 nav callnr 91690
         --
         IF l_partner_voorvoegsel IS NULL
         THEN
            l_partner_voorvoegsel := r_rle_001.vvgsl_partner;
         END IF;
         -- EINDE RTJ
         --
         -- RTJ 26-11-2003 Call 91115 Eerste letter voorvoegsel moet hoofdletter worden
         --
         l_partner_voorvoegsel := UPPER(substr(l_partner_voorvoegsel,1,1))||substr(l_partner_voorvoegsel,2);
         -- EINDE RTJ
       END IF ;
    END IF ;
    l_eigen_fnaam := ltrim(rtrim(rtrim(l_eigen_voorvoegsel)||' '||r_rle_001.namrelatie));
    l_partner_fnaam := ltrim(rtrim(rtrim(l_partner_voorvoegsel)||' '||r_rle_001.naam_partner));
    IF r_rle_001.code_aanduiding_naamgebruik in ( 'P', 'V')
    THEN
       l_opmaaknaam := l_partner_fnaam;
    ELSIF NVL(r_rle_001.code_aanduiding_naamgebruik, 'E') in ('E', 'N')  -- JPG 21-10-2009
    THEN
       l_opmaaknaam := l_eigen_fnaam;
    END IF;
  END IF;
  stm_util.debug('l_opmaaknaam :' || l_opmaaknaam);
  stm_util.debug('l_aanhef_eng1: ' || l_aanhef_eng);
 /* 02-10-2006 NKU aanhef aanpassen igv overlijden */
   IF r_rle_001.datoverlijden is not null
   THEN
     p_aanhef := 'heer, mevrouw ' ;
     p_aanhef_eng := 'Mr, Madam ' ;
   ELSE
     p_aanhef := l_aanhef || ' ' || l_opmaaknaam;
     p_aanhef_eng := l_aanhef_eng || ' ' || l_opmaaknaam;
   END IF;
   p_aansp_titel := l_aansp_titel;
   p_aansp_titel_klein := lower(l_aansp_titel);
   p_aansp_titel_eng := l_aansp_titel_eng;


  stm_util.debug('p_aanhef     : ' || p_aanhef );
  stm_util.debug('p_aansp_titel: ' || p_aansp_titel );
  stm_util.debug('p_aanhef_eng : ' || p_aanhef_eng);
  stm_util.debug('p_aansp_titel_eng: ' || p_aansp_titel_eng);

  stm_util.debug('EINDE RLE_BRIEF.RLE_RLE_GET_AANHEF');
  --
END RLE_RLE_GET_AANHEF;
/* is een copy van function RLE_GET_VERZENDNAAM, deze omgezet naar proced */
PROCEDURE RLE_GET_VERZENDNAAM
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%type
 ,P_VERZENDNAAM OUT varchar2
 ,P_VERZENDNAAM_ENG OUT varchar2
 )
 IS
-- 26-11-2015 toegevoegd ivm nieuwe parameters engels.
-- is een copy van function RLE_GET_VERZENDNAAM, deze omgezet naar procedure! ivm meerdere out parameters en meer flexibiliteit.
-- fo rle09211
-- H de Bruin 21-11-2002 dienst ondersteunde aanspreekvorm van vrouwen niet goed.
-- tevens de output aangepast aan de wensen van Communicatie */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
--select	*
select namrelatie
,      indinstelling
,      dwe_wrddom_vvgsl
,      dwe_wrddom_geslacht
,      dwe_wrddom_gewatitel
,      naam_partner
,      handelsnaam
,      vvgsl_partner
,      code_aanduiding_naamgebruik
,      voorletter
,      datoverlijden
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Program Data
l_opmaaknaam VARCHAR2(500);
l_opmaaknaam_eng VARCHAR2(500);
l_aanhef varchar2(100);
l_aanhef_eng varchar2(100);
L_EIGEN_VOORLETTERS VARCHAR2(20);
l_partner_voorletters VARCHAR2(20);
L_PARTNER_FNAAM VARCHAR2(500);
L_PARTNER_VOORVOEGSEL VARCHAR2(40);
L_EIGEN_VOORVOEGSEL VARCHAR2(40);
R_RLE_001 C_RLE_001%ROWTYPE;
L_IND_IBM VARCHAR2(1) := 'N';
L_EIGEN_NAAM VARCHAR2(184);
L_LENGTE_IBM NUMBER := 40;
L_EIGEN_FNAAM VARCHAR2(500);
L_PARTNER_NAAM VARCHAR2(184);

FUNCTION FNC_BEPAAL_VOORLETTERS
 (PIV_VOORLETTERS IN VARCHAR2
 )
 RETURN VARCHAR2;
-- Sub-Program Units
/* Gebruikt in RLE_M11_GET_NAW om de voorletters van een naam op te maken */
FUNCTION FNC_BEPAAL_VOORLETTERS
 (PIV_VOORLETTERS IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
BEGIN
IF INSTR( piv_voorletters, '.' ) = 0  AND
         INSTR( piv_voorletters, ' ' ) = 0
      THEN
         -- Geen puntjes en spaties. Dus zelf puntjes toevoegen tussen alle letters.
         RETURN( LTRIM( SUBSTR( piv_voorletters,  1, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  2, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  3, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  4, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  5, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  6, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  7, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  8, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  9, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters, 10, 1 ) || '.', '.' )
               ) ;
      ELSIF INSTR( piv_voorletters, '.' ) =  0  AND
            INSTR( piv_voorletters, ' ' ) != 0
      THEN
         -- Geen puntjes, wel spaties. Dus spaties vervangen door puntjes.
         RETURN( REPLACE( piv_voorletters, ' ', '.' ) ) ;
      ELSE
         -- Puntjes en spaties aanwezig. Gewoon zo laten.
         RETURN( piv_voorletters ) ;
      END IF ;
      RETURN( NULL ) ;
END FNC_BEPAAL_VOORLETTERS;
BEGIN
  l_opmaaknaam := NULL ;
  l_opmaaknaam_eng := NULL ;

  l_aanhef := null;
  -- Ophalen eigen naam
  OPEN c_rle_001( P_NUMRELATIE ) ;
  FETCH c_rle_001 INTO r_rle_001 ;
  IF c_rle_001%FOUND
  THEN
-- eerst de aanspreekwijze bepalen
  IF r_rle_001.indinstelling =  'N'
  THEN
    l_aanhef := NULL ;
    IF r_rle_001.dwe_wrddom_gewatitel IS NOT NULL -- is er een code voor gewenste aanspreektitel
    THEN
       RFE_RFW_GET_WRD
               ( 'GWT'
               , r_rle_001.dwe_wrddom_gewatitel
               , l_ind_ibm
               , l_lengte_ibm
               , l_aanhef
              ) ;

       if stm_util.t_debug
       then
         stm_util.debug('l_aanhef: ' || l_aanhef);
       end if;
       if upper(l_aanhef) = upper('De heer')
       then
        l_aanhef_eng      :=  'Mr';
       elsif upper(l_aanhef) = upper('Mevrouw')
       then
         l_aanhef_eng      :=  'Madam';
       end if;

      if stm_util.t_debug
      then
        stm_util.debug('l_aanhef_eng: ' || l_aanhef);
        stm_util.debug('Einde RFE_RFW_GET_WRD ');
      end if;
    ELSIF r_rle_001.dwe_wrddom_geslacht = 'M'
    THEN
      l_aanhef := 'De heer' ;
      l_aanhef_eng := 'Mr';
    ELSIF r_rle_001.dwe_wrddom_geslacht = 'V'
    THEN
      l_aanhef := 'Mevrouw' ;
      l_aanhef_eng := 'Madam';
    END IF ;
-- bepalen juiste verzendnaam
-- formatteer voorletters (puntgescheiden haafdletters)
    l_eigen_voorletters := fnc_bepaal_voorletters( r_rle_001.voorletter ) ;
-- Zoek voorvoegsel
    IF r_rle_001.dwe_wrddom_vvgsl IS NOT NULL
    THEN
        RFE_RFW_GET_WRD
                ('VVL'
                , r_rle_001.dwe_wrddom_vvgsl
                , l_ind_ibm
                , l_lengte_ibm
                , l_eigen_voorvoegsel
                ) ;
    END IF ;
    IF r_rle_001.code_aanduiding_naamgebruik in ('P','V','N')
    THEN
         l_partner_voorletters := NULL ;
         IF r_rle_001.vvgsl_partner IS NOT NULL
         THEN
           RFE_RFW_GET_WRD
                   ( 'VVL'
                   , r_rle_001.vvgsl_partner
                   , l_ind_ibm
                   , l_lengte_ibm
                   , l_partner_voorvoegsel
                   ) ;
            --
            -- RTJ 02-12-2003 nav callnr 91690
            --
            IF l_partner_voorvoegsel IS NULL
            THEN
               l_partner_voorvoegsel := r_rle_001.vvgsl_partner;
            END IF;
            -- EINDE RTJ
         END IF ;
    END IF ;
    l_eigen_fnaam := ltrim(rtrim(rtrim(l_eigen_voorvoegsel)||' '||r_rle_001.namrelatie));
    l_partner_fnaam := ltrim(rtrim(rtrim(l_partner_voorvoegsel)||' '||r_rle_001.naam_partner));
    IF r_rle_001.code_aanduiding_naamgebruik = 'P'
    THEN
         l_opmaaknaam := l_partner_fnaam;
         l_opmaaknaam_eng:= l_partner_fnaam;
    ELSIF r_rle_001.code_aanduiding_naamgebruik = 'E'
    THEN
        l_opmaaknaam := l_eigen_fnaam;
        l_opmaaknaam_eng := l_eigen_fnaam;
    ELSIF r_rle_001.code_aanduiding_naamgebruik = 'V'
    THEN
         l_opmaaknaam := l_partner_fnaam||'-'||l_eigen_fnaam;
         l_opmaaknaam_eng := l_partner_fnaam||'-'||l_eigen_fnaam;
    ELSIF r_rle_001.code_aanduiding_naamgebruik = 'N'
    THEN
         l_opmaaknaam := l_eigen_fnaam||'-'||l_partner_fnaam;
         l_opmaaknaam_eng :=  l_eigen_fnaam||'-'||l_partner_fnaam;
    END IF;

    l_opmaaknaam := l_aanhef||' '||rtrim(l_eigen_voorletters)||' '||l_opmaaknaam;
    l_opmaaknaam_eng := l_aanhef_eng||' '||rtrim(l_eigen_voorletters)||' '||l_opmaaknaam_eng;

  ELSE
    -- voor bedrijven eerst handelsnaam, anders gewone naam
    l_opmaaknaam := nvl (r_rle_001.handelsnaam, r_rle_001.namrelatie);
  END IF;   -- r_rle_001.indinstelling
  END IF;
  /* 02-10-2006 NKU */
  IF r_rle_001.datoverlijden is not null
  THEN
       l_opmaaknaam := 'De erven van '||lower(substr(l_opmaaknaam, 1, 1))||substr(l_opmaaknaam, 2);
  END IF;
  close c_rle_001;
  --return
  P_verzendnaam     := l_opmaaknaam;
  p_verzendnaam_eng := l_opmaaknaam_eng;

END RLE_GET_VERZENDNAAM;

END RLE_BRIEF;
/