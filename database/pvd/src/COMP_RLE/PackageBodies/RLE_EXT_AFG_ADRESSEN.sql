CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_EXT_AFG_ADRESSEN IS

G_PACKAGE_NAME CONSTANT VARCHAR2(50) := 'rle_ext_afg_adressen';
CN_RES_GEWIJZIGD CONSTANT VARCHAR2(2) := 'G';
CN_RES_BEOORDELEN CONSTANT VARCHAR2(2) := 'B';
CN_RES_CORRECT CONSTANT VARCHAR2(2) := 'OC';
CN_RES_NIET_TOEGESTAAN CONSTANT VARCHAR2(2) := 'OM';
CN_RES_ONGEWIJZIGD_ONBEKEND CONSTANT VARCHAR2(2) := 'OO';

/* Geef indicatie verwerkt een waarde */
PROCEDURE RLE_ZET_IND_VERWERKT
 (P_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 ,P_IND_VERWERKT IN RLE_EXT_AFGESTEMDE_ADRESSEN.IND_VERWERKT%TYPE
 );


/* Ophalen afgestemd adres */
FUNCTION RLE_HAALOP_AFGESTEMD_ADRES
 (P_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 )
 RETURN afgestemd_adres_rec
 IS

CURSOR C_EAS_001
 (B_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 )
 IS
select eas.id
     ,      eas.eat_id
     ,      eat.rol_codrol
     ,      eat.soort_adres
     ,      eas.mn_numrelatie
     ,      eas.mn_persoonsnummer
     ,      eas.mn_voorletters
     ,      eas.mn_tussenvoegsel
     ,      eas.mn_achternaam
     ,      eas.mn_straat
     ,      eas.mn_huisnr
     ,      eas.mn_huisnr_toevoeging
     ,      eas.mn_postcode
     ,      eas.mn_woonplaats
     ,      eas.uitspraakcode
     ,      eas.overeenkomst_in_perc
     ,      eas.postcode
     ,      eas.woonplaats
     ,      eas.voorletters
     ,      eas.voorvoegsels
     ,      eas.achternaam
     ,      eas.achtervoegsels
     ,      eas.straatnaam
     ,      eas.huisnr
     ,      eas.huisnr_toevoeging
     ,      eas.verhuiscode
     ,      eas.verhuisdatum
     ,      eas.datum_nazenden_tm
     ,      eas.ind_verwerkt
     ,      eas.resultaat
     from   rle_ext_afgestemde_adressen eas
     ,      rle_ext_adr_subsets         eat
     where  eas.eat_id = eat.id
     and    eas.id = b_id;
/************************************************************
Procedurenaam: rle_haalop_afgestemd_adres

Beschrijving

Functie die het adres teruggeeft dat is afgestemd met een
derde partij. Op basis van een id wordt het adres als
record teruggegeven.

Datum        Wie              Wat
----------   --------------   -------------------------
20-02-2009   E. Schillemans   Creatie
*************************************************************/
r_eas_001                 c_eas_001%rowtype;
    r_afgestemd_adres         afgestemd_adres_rec;
    e_afg_adres_niet_gevonden exception;
begin
    open c_eas_001 ( p_id );

    fetch c_eas_001
    into  r_eas_001;

    if c_eas_001%found
    then
      r_afgestemd_adres := r_eas_001;

      close c_eas_001;
    else
      close c_eas_001;

      raise e_afg_adres_niet_gevonden;
    end if;

    return r_afgestemd_adres;
  exception
    when e_afg_adres_niet_gevonden
    then
      raise_application_error
                            ( -20000
                            , stm_app_error ( sqlerrm
                                            ,  g_package_name
                                            , 'rle_haalop_afgestemd_adres: afgestemd adres niet gevonden voor id '
                                              || p_id
                                            )
                              || ' ' || dbms_utility.format_error_backtrace
                            );
    when others
    then
      raise_application_error ( -20000
                              , stm_app_error ( sqlerrm
                                              ,  g_package_name
                                              , 'rle_haalop_afgestemd_adres'
                                              ) || ' ' || dbms_utility.format_error_backtrace
                              );
  end rle_haalop_afgestemd_adres;
/* Bepalen resultaat verwerking */
FUNCTION RLE_BEPAAL_RESULTAAT_VERW
 (P_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 )
 RETURN VARCHAR2
 IS

CURSOR C_AVL_001
 (B_UITSPRAAKCODE IN RLE_EXT_ADR_VERWERKINGSREGELS.UITSPRAAKCODE%TYPE
 ,B_VERHUISCODE RLE_EXT_ADR_VERWERKINGSREGELS.VERHUISCODE%TYPE
 ,B_SCORE RLE_EXT_ADR_VERWERKINGSREGELS.SCORE_VANAF%TYPE
 )
 IS
select avl.resultaat
      ,      avl.ind_chk_naamsgelijkheid
      ,      avl.ind_chk_huisnr_toev
      ,      avl.ind_chk_verhuisdatum
      ,      avl.ind_chk_dubbele_naam
      from   rle_ext_adr_verwerkingsregels avl
      where  avl.uitspraakcode = b_uitspraakcode
      and    nvl ( avl.verhuiscode
                 , '#'
                 ) = nvl ( b_verhuiscode
                         , '#'
                         )
      and    b_score between avl.score_vanaf and avl.score_tm;
CURSOR C_RAS_036
 (B_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 )
 IS
select   ras.dat_creatie
    from     rle_relatie_adressen ras
           , rle_ext_adr_subsets eat
           , rle_ext_afgestemde_adressen eas
    where    ras.rle_numrelatie = eas.mn_numrelatie
    and      ras.rol_codrol = eat.rol_codrol
    and      ras.dwe_wrddom_srtadr = eat.soort_adres
    and      ras.adm_code >= 'A' -- forceren dat range scan op unique key wordt uitgevoerd
    and      eas.eat_id = eat.id
    and      eas.id = b_id
    and      ( ras.dateinde is null
               or ras.dateinde > trunc ( sysdate ) )
    order by ras.dat_creatie desc;
/************************************************************
Procedurenaam: rle_bepaal_resultaat_verw

Beschrijving

Functie om te bepalen hoe een adres moet worden afgehandeld.
Initieel wordt uit rle_ext_adr_verwerkingsregels het van
toepassing zijnde record opgehaald voor het resultaat.

Er worden aanvullende checks uitgevoerd als dit middels
indicatoren is aangegeven. Het opgehaalde resultaat kan in
dergelijke gevallen worden overschreven.

Datum        Wie              Wat
----------   --------------   -------------------------
19-02-2009   E. Schillemans   Creatie
05-03-2009   E. Schillemans   Verfijning aangebracht in
                              analyse
01-04-2009   E. Schillemans   Adreswijzigingen als gevolg van
                              afwijkingen op huisnummer toevoeging
                              en dubbele namen kunnen nu automatisch
                              worden doorgevoerd en hoeven niet
                              langer in de werkbak terecht te komen.
03-06-2009   J. Budding       Bevinding acceptatietest:
                              Postbus adressen worden teruggeven door Cendris.
                              Wij mogen deze niet opslaan als OA adres (is een business rule voor).
*************************************************************/
r_avl_001 c_avl_001%rowtype;
    r_ras_036 c_ras_036%rowtype;
    r_afgestemd_adres afgestemd_adres_rec;
    e_onbekend_resultaat exception;
    l_mn_voorletters rle_ext_afgestemde_adressen.mn_voorletters%type;
    l_mn_voorvoegsels rle_ext_afgestemde_adressen.mn_tussenvoegsel%type;
    l_mn_achternaam rle_ext_afgestemde_adressen.mn_achternaam%type;
    l_mn_postcode rle_ext_afgestemde_adressen.mn_postcode%type;
    l_mn_huisnr rle_ext_afgestemde_adressen.mn_huisnr%type;
    l_voorletters rle_ext_afgestemde_adressen.voorletters%type;
    l_voorvoegsels rle_ext_afgestemde_adressen.voorvoegsels%type;
    l_achternaam rle_ext_afgestemde_adressen.achternaam%type;
    l_straatnaam rle_ext_afgestemde_adressen.straatnaam%type;
    l_postcode rle_ext_afgestemde_adressen.postcode%type;
    l_huisnr rle_ext_afgestemde_adressen.huisnr%type;
    l_resultaat rle_ext_adr_verwerkingsregels.resultaat%type;

    function strip_veld ( p_veld varchar2 )
      return varchar2
    is
    begin
      -- Haal punt, spatie en min-teken uit veld en maak uppercase
      return ( upper ( replace ( replace ( replace ( p_veld
                                                   , '.'
                                                   )
                                         , ' '
                                         )
                               , '-'
                               ) ) );
    end;
begin
    r_afgestemd_adres := rle_haalop_afgestemd_adres ( p_id );
    -- Strip velden
    l_mn_voorletters := strip_veld ( r_afgestemd_adres.mn_voorletters );
    l_mn_voorvoegsels := strip_veld ( r_afgestemd_adres.mn_tussenvoegsel );
    l_mn_achternaam := strip_veld ( r_afgestemd_adres.mn_achternaam );
    l_mn_postcode := strip_veld ( r_afgestemd_adres.mn_postcode );
    l_mn_huisnr := strip_veld ( r_afgestemd_adres.mn_huisnr );
    l_voorletters := strip_veld ( r_afgestemd_adres.voorletters );
    l_voorvoegsels := strip_veld ( r_afgestemd_adres.voorvoegsels );
    l_achternaam := strip_veld ( r_afgestemd_adres.achternaam );
    l_straatnaam := strip_veld (r_afgestemd_adres.straatnaam); --xjb 03-06-2009
    l_postcode := strip_veld ( r_afgestemd_adres.postcode );
    l_huisnr := strip_veld ( r_afgestemd_adres.huisnr );

    open c_avl_001 ( r_afgestemd_adres.uitspraakcode
                   , r_afgestemd_adres.verhuiscode
                   , r_afgestemd_adres.overeenkomst_in_perc
                   );

    fetch c_avl_001
    into  r_avl_001;

    if c_avl_001%notfound
    then
      close c_avl_001;

      raise e_onbekend_resultaat;
    else
      l_resultaat := r_avl_001.resultaat;

      close c_avl_001;

      -- Extra controles indien aangegeven door indicatoren
      -- Check op naamsgelijkheid?
      if r_avl_001.ind_chk_naamsgelijkheid = 'J'
      then
        if l_mn_voorletters = l_voorletters
           and nvl ( l_mn_voorvoegsels
                   , '#'
                   ) = nvl ( l_voorvoegsels
                           , '#'
                           )
           and l_mn_achternaam = l_achternaam
        then
          -- Is combinatie postcode/huisnummer gelijk?
          if l_mn_postcode = l_postcode
             and l_mn_huisnr = l_huisnr
          then
            -- Check op huisnummer toevoeging?
            if r_avl_001.ind_chk_huisnr_toev = 'J'
            then
              if nvl(r_afgestemd_adres.mn_huisnr_toevoeging, '#') <> nvl(r_afgestemd_adres.huisnr_toevoeging,'#')
              then
                l_resultaat := cn_res_gewijzigd;
              else
                l_resultaat := cn_res_correct;
              end if;
            else
              -- Geen check op huisnummer toevoeging
              l_resultaat := cn_res_correct;
            end if;
          else
            -- Combinatie postcode / huisnr wijkt af
            -- Check op verhuisdatum?
            if r_avl_001.ind_chk_verhuisdatum = 'J'
            then
              if r_afgestemd_adres.verhuisdatum is not null
              then
                open c_ras_036 ( r_afgestemd_adres.id );

                fetch c_ras_036
                into  r_ras_036;

                if c_ras_036%found
                then
                  -- Verhuisdatum recenter dan ons adres?
                  if r_ras_036.dat_creatie < r_afgestemd_adres.verhuisdatum
                  then
                    l_resultaat := cn_res_gewijzigd;
                  end if;
                else
                  -- Geen actueel adres
                  l_resultaat := cn_res_gewijzigd;
                end if;

                close c_ras_036;
              else
                -- Geen verhuisdatum, doorvoeren
                -- Alleen als mutatiedatum ouder is dan verouderingsparameter,
                -- maar dit wordt gerealiseerd door adressen met een
                -- recentere datum uit te sluiten in het proces van selecteren
                -- van adressen.
                l_resultaat := cn_res_gewijzigd;
              end if;
            end if;
          end if;
        else
          -- Naam niet gelijk
          -- Check op dubbele naam?
          if r_avl_001.ind_chk_dubbele_naam = 'J'
          then
            -- Voorletters en voorvoegsels moeten nog altijd
            -- overeenkomen!
            if l_mn_voorletters = l_voorletters
               and nvl ( l_mn_voorvoegsels
                       , '#'
                       ) = nvl ( l_voorvoegsels
                               , '#'
                               )
            then
              -- Feitelijke controle op dubbele naam
              if instr ( r_afgestemd_adres.achternaam
                       , '-'
                       ) > 0
              then
                l_resultaat := cn_res_gewijzigd;
              end if;
            end if;
          end if;
        end if;
      end if;
      -- B+ xjb, 03-06-2009, Bevinding acceptatietest
      -- Postbus adressen worden teruggeven door Cendris.
      -- Wij mogen deze niet opslaan als OA adres (is een business rule voor).
      -- Hier een snelle oplossing voor aangebracht:
      if l_straatnaam like '%POSTBUS%'
      then
        l_resultaat := cn_res_ongewijzigd_onbekend;
      end if;
      -- E+ xjb, 03-06-2009
    end if;

    -- Vastleggen resultaat
    rle_vastleggen_resultaat ( p_id
                             , l_resultaat
                             );
    return l_resultaat;
  exception
    when e_onbekend_resultaat
    then
      rle_zet_ind_verwerkt(p_id,'N');
      raise_application_error ( -20000
                              , stm_app_error ( sqlerrm
                                              , g_package_name
                                              , 'rle_bepaal_resultaat_verw: geen resultaat voor '
                                                || r_afgestemd_adres.uitspraakcode || ','
                                                || r_afgestemd_adres.verhuiscode || ','
                                                || r_afgestemd_adres.overeenkomst_in_perc
                                              )
                                || ' ' || dbms_utility.format_error_backtrace
                              );
    when others
    then
      rollback;
      rle_zet_ind_verwerkt(p_id,null);
      commit;
      raise_application_error ( -20000
                              , stm_app_error ( sqlerrm
                                              , g_package_name
                                              , 'rle_bepaal_resultaat_verw'
                                              ) || ' ' || dbms_utility.format_error_backtrace
                              );
  end rle_bepaal_resultaat_verw;
/* Wijzigen adres toegestaan */
FUNCTION RLE_WIJZ_ADR_TOEGESTAAN
 (P_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 )
 RETURN VARCHAR2
 IS

CURSOR C_RAS_035
 (B_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 )
 IS
select null
      from   rle_relatie_adressen ras
           , rle_ext_adr_subsets eat
           , rle_ext_afgestemde_adressen eas
      where  ras.rle_numrelatie = eas.mn_numrelatie
      and    ras.rol_codrol = eat.rol_codrol
      and    ras.dwe_wrddom_srtadr = eat.soort_adres
      and    ras.adm_code >= 'A' -- forceren dat range scan op unique key wordt uitgevoerd
      and    eas.eat_id = eat.id
      and    eas.id = b_id
      and    ( ras.dateinde is null
               or ras.dateinde > trunc ( sysdate ) )
      and    ras.dat_creatie >= eat.dat_selectie;
/************************************************************
Procedurenaam: rle_wijz_adres_toegestaan

Beschrijving

Bepalen of het adres dat met een derde partij is afgestemd in
RLE mag worden gewijzigd.

Een actueel adres dat in RLE is gewijzigd NA de verouderings-
datum mag niet worden overschreven door de gegevens van de
afstemming.

Datum        Wie              Wat
----------   --------------   -------------------------
20-02-2009   E. Schillemans   Creatie
*************************************************************/
r_ras_035 c_ras_035%rowtype;
    r_afgestemd_adres afgestemd_adres_rec;
    l_wijz_adres_toegestaan varchar2 ( 1 ) := 'J';
begin
    r_afgestemd_adres := rle_haalop_afgestemd_adres ( p_id );

    open c_ras_035 ( p_id );

    fetch c_ras_035
    into  r_ras_035;

    if c_ras_035%found
    then
      l_wijz_adres_toegestaan := 'N';

      -- Vastleggen resultaat
      rle_vastleggen_resultaat(p_id,cn_res_niet_toegestaan);
    end if;

    close c_ras_035;

    return l_wijz_adres_toegestaan;
  exception
    when others
    then
      rollback;
      rle_zet_ind_verwerkt(p_id,'N');
      raise_application_error ( -20000
                              , stm_app_error ( sqlerrm
                                              ,  g_package_name
                                              , 'rle_wijz_adres_toegestaan'
                                              ) || ' ' || dbms_utility.format_error_backtrace
                              );
END RLE_WIJZ_ADR_TOEGESTAAN;
/* Vastleggen nieuw adres na verhuizing externe validatie adressen */
PROCEDURE RLE_VASTLEGGEN_NIEUW_ADRES
 (P_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 )
 IS

L_WOONPL RLE_WOONPLAATSEN.WOONPLAATS%TYPE;
L_STRAAT RLE_STRATEN.STRAATNAAM%TYPE;
L_TOEVNUM RLE_ADRESSEN.TOEVHUISNUM%TYPE;
L_HUISNUM RLE_ADRESSEN.HUISNUMMER%TYPE;
L_POSTCODE RLE_ADRESSEN.POSTCODE%TYPE;
L_CODLAND RLE_RELATIE_ADRESSEN.ROL_CODROL%TYPE;
L_NUMADRES RLE_ADRESSEN.NUMADRES%TYPE;
R_AFGESTEMD_ADRES rle_ext_afg_adressen.AFGESTEMD_ADRES_REC;
   /************************************************************
   Procedurenaam: rle_nieuw_adres

   Beschrijving

   Call naar RLE0032: onderhouden relatie_adres met nieuwe adres gegevens
   van Cendris

   Datum        Wie              Wat
   ----------   --------------   -------------------------
   23-02-2009   Peter Rurenga    Creatie
   09-03-2009   E. Schillemans   Aan parameter begindatum
                                 moet de systeemdatum worden
                                 meegegeven i.p.v. de
                                 verhuisdatum.
   *************************************************************/
begin
    -- ophalen adresgevens.
   r_afgestemd_adres := rle_haalop_afgestemd_adres ( p_id => p_id);
   -- voor IN OUT parameters moeten waarde eerst worden toegewezen aan lokale variabelen.
  l_codland  := stm_algm_get.sysparm(NULL,'LAND',sysdate);
  -- verwijder eventuele spaties uit postcode
  l_postcode := replace(r_afgestemd_adres.postcode, ' ');
  l_huisnum  := r_afgestemd_adres.huisnr;
  l_toevnum  := r_afgestemd_adres.huisnr_toevoeging;
  l_straat   := r_afgestemd_adres.straatnaam;
  l_woonpl   := r_afgestemd_adres.woonplaats;

  RLE_RLE_ADS_TOEV
    (P_NUMRELATIE    => r_afgestemd_adres.mn_numrelatie
    ,P_CODROL        => r_afgestemd_adres.eat_codrol
    ,P_SRT_ADRES     => r_afgestemd_adres.eat_soort_adres
    ,P_BEGINDATUM    => trunc(sysdate)
    ,P_CODLAND       => l_codland
    ,P_POSTCOD       => l_postcode
    ,P_HUISNUM       => l_huisnum
    ,P_TOEVNUM       => l_toevnum
    ,P_WOONPL        => l_woonpl
    ,P_STRAAT        => l_straat
    ,P_EINDDATUM     => ''   -- einde huidige adres wordt afgeleid in triggers op RAS
    ,P_LOCATIE       => ''
    ,P_NUMADRES      => l_numadres -- nieuw ID,  niet van belang
    ,P_PROVINCIE     => ''
    );
  exception
    when others
    then
      rollback;
      rle_zet_ind_verwerkt(p_id,'N');
      raise_application_error ( -20000
                              , stm_app_error ( sqlerrm
                                              ,  g_package_name
                                              , 'rle_vastleggen_nieuw_adres'
                                              ) || ' ' || dbms_utility.format_error_backtrace
                              );

  end rle_vastleggen_nieuw_adres;
/* Uitlezen bericht en vastleggen in externe tabel */
FUNCTION RLE_VASTLEGGEN_BERICHT
 (P_INHOUD_BERICHT IN VARCHAR2
 )
 RETURN NUMBER
 IS

L_DAT_ONTVANGEN RLE_EXT_ADR_SUBSETS.DAT_ONTVANGEN%TYPE;
L_BERICHTREGEL VARCHAR2(4000);
L_KEY VARCHAR2(30);
L_DUMMY VARCHAR2(30);
L_VALUE VARCHAR2(240);
L_ROL_CODROL RLE_EXT_ADR_SUBSETS.ROL_CODROL%TYPE;
L_SOORT_ADRES RLE_EXT_ADR_SUBSETS.SOORT_ADRES%TYPE;
L_DAT_SELECTIE RLE_EXT_ADR_SUBSETS.DAT_SELECTIE%TYPE;
L_EAT_ID RLE_EXT_ADR_SUBSETS.ID%TYPE;
L_EAS_ID RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE;
L_MN_NUMRELATIE RLE_EXT_AFGESTEMDE_ADRESSEN.MN_NUMRELATIE%TYPE;
L_UITSPRAAKCODE RLE_EXT_AFGESTEMDE_ADRESSEN.UITSPRAAKCODE%TYPE;
L_OVEREENKOMST_IN_PERC RLE_EXT_AFGESTEMDE_ADRESSEN.OVEREENKOMST_IN_PERC%TYPE;
L_POSTCODE RLE_EXT_AFGESTEMDE_ADRESSEN.POSTCODE%TYPE;
L_WOONPLAATS RLE_EXT_AFGESTEMDE_ADRESSEN.WOONPLAATS%TYPE;
L_VOORLETTERS RLE_EXT_AFGESTEMDE_ADRESSEN.VOORLETTERS%TYPE;
L_VOORVOEGSELS RLE_EXT_AFGESTEMDE_ADRESSEN.VOORVOEGSELS%TYPE;
L_ACHTERNAAM RLE_EXT_AFGESTEMDE_ADRESSEN.ACHTERNAAM%TYPE;
L_ACHTERVOEGSELS RLE_EXT_AFGESTEMDE_ADRESSEN.ACHTERVOEGSELS%TYPE;
L_STRAATNAAM RLE_EXT_AFGESTEMDE_ADRESSEN.STRAATNAAM%TYPE;
L_HUISNR RLE_EXT_AFGESTEMDE_ADRESSEN.HUISNR%TYPE;
L_HUISNR_TOEVOEGING RLE_EXT_AFGESTEMDE_ADRESSEN.HUISNR_TOEVOEGING%TYPE;
L_VERHUISCODE RLE_EXT_AFGESTEMDE_ADRESSEN.VERHUISCODE%TYPE;
L_VERHUISDATUM RLE_EXT_AFGESTEMDE_ADRESSEN.VERHUISDATUM%TYPE;
L_DATUM_NAZENDEN_TM RLE_EXT_AFGESTEMDE_ADRESSEN.DATUM_NAZENDEN_TM%TYPE;

E_SUBSET_NOT_FOUND EXCEPTION;
OTHERS EXCEPTION;
E_EAS_NOT_FOUND EXCEPTION;
/************************************************************
   Functienaam: rle_vastleggen_bericht

   Beschrijving
   Uitlezen bericht en updaten van RLE_EXT_AFGESTEMDE_ADRESSEN
   met externe informatie.
   Retourneren ID van record.

   Datum        Wie              Wat
   ----------   --------------   -------------------------
   25-02-2009   Peter Rurenga    Creatie
   *************************************************************/
BEGIN
  l_berichtregel := p_inhoud_bericht;
  -- 1 Over t_berichtregel heenlopen
  while l_berichtregel is not null
  loop
    --
    l_key             := stm_get_word(l_berichtregel);
    l_dummy           := stm_get_word(l_berichtregel);
    l_value           := stm_get_word(l_berichtregel);
    --
    case l_key
    when 'ROL_CODROL'
    then
      l_rol_codrol                :=  l_value;
    when 'SOORT_ADRES'
    then
      l_soort_adres               :=  l_value;
    when 'DAT_SELECTIE'
    then
      l_dat_selectie              :=  to_date(l_value, 'dd-mm-yyyy hh24:mi:ss')  ;
    when 'MN_NUMRELATIE'
    then
      l_mn_numrelatie             := l_value;
    when 'UITSPRAAKCODE'
    then
      l_uitspraakcode             := l_value;
    when 'OVEREENKOMST_IN_PERC'
    then
      l_overeenkomst_in_perc      := to_number(l_value);
    when 'POSTCODE'
    then
      l_postcode                  := l_value;
    when 'WOONPLAATS'
    then
      l_woonplaats                := l_value;
    when 'VOORLETTERS'
    then
      l_voorletters               := l_value;
    when 'VOORVOEGSELS'
    then
      l_voorvoegsels              := l_value;
    when 'ACHTERNAAM'
    then
      l_achternaam                := l_value;
    when 'ACHTERVOEGSELS'
    then
      l_achtervoegsels            := l_value;
    when 'STRAATNAAM'
    then
      l_straatnaam                := l_value;
    when 'HUISNR'
    then
      l_huisnr                    := to_number(l_value);
    when 'HUISNR_TOEVOEGING'
    then
      l_huisnr_toevoeging         := l_value;
    when 'VERHUISCODE'
    then
      l_verhuiscode               := l_value;
    when 'VERHUISDATUM'
    then
      l_verhuisdatum              := to_date(l_value, 'dd-mm-yyyy');
    when 'DATUM_NAZENDEN_TM'
    then
      l_datum_nazenden_tm         := to_date(l_value, 'dd-mm-yyyy');
    else
      null; -- in principe alleen uitvragen wat van belang is
    end case;
  --
  end loop;

  -- 2 Bepalen subset a.d.h.v. rol_codrol, aoort_adres en datum_selectie
  --   Zou één record moeten opleveren.
  begin
    select id
    ,      dat_ontvangen
    into   l_eat_id
    ,      l_dat_ontvangen
    from   rle_ext_adr_subsets        eat
    where  eat.rol_codrol             =  l_rol_codrol
    and    eat.soort_adres            =  l_soort_adres
    and    eat.dat_selectie           =  l_dat_selectie;
  exception
    when others then   -- no_data_found / too_many_rows
       raise e_subset_not_found;
  end;

  -- 3 update subset met datum ontvangen, dit hoeft voor de hele set maar 1 keer
  if l_dat_ontvangen is null
  then
    update rle_ext_adr_subsets           eat
    set    eat.dat_ontvangen          =  sysdate
    where  eat.id                     =  l_eat_id;
  end if;

  -- 4 Bijwerken.
  --   Update velden in tabel met extern aangeleverde data.
  --   Zet ook ind_verwerkt op 'J'
  update rle_ext_afgestemde_adressen   eas
  set    eas.uitspraakcode          =  l_uitspraakcode
  ,      eas.overeenkomst_in_perc   =  l_overeenkomst_in_perc
  ,      eas.postcode               =  l_postcode
  ,      eas.woonplaats             =  l_woonplaats
  ,      eas.voorletters            =  l_voorletters
  ,      eas.voorvoegsels           =  l_voorvoegsels
  ,      eas.achternaam             =  l_achternaam
  ,      eas.achtervoegsels         =  l_achtervoegsels
  ,      eas.straatnaam             =  l_straatnaam
  ,      eas.huisnr                 =  l_huisnr
  ,      eas.huisnr_toevoeging      =  l_huisnr_toevoeging
  ,      eas.verhuiscode            =  l_verhuiscode
  ,      eas.verhuisdatum           =  l_verhuisdatum
  ,      eas.datum_nazenden_tm      =  l_datum_nazenden_tm
  ,      eas.ind_verwerkt           =  'J'
  where  eat_id                     =  l_eat_id
  and    mn_numrelatie              =  l_mn_numrelatie
  returning eas.id                into l_eas_id;

  if l_eas_id is null
  then
     raise e_eas_not_found;
  end if;

  return l_eas_id;


EXCEPTION
 WHEN E_SUBSET_NOT_FOUND THEN
        raise_application_error ( -20000

                              , stm_app_error ( g_package_name

                                              , 'rle_vastleggen_bericht: geen subset gevonden voor '

                                                || to_char(l_dat_selectie, 'dd-mm-yyyy hh24:mi:ss') || ','

                                                || ' soort adres: '||l_soort_adres

                                              )

                                || ' ' || dbms_utility.format_error_backtrace

                              );


 WHEN E_EAS_NOT_FOUND THEN
        raise_application_error ( -20000

                              , stm_app_error ( g_package_name

                                              , 'rle_vastleggen_bericht: geen record gevonden in rle_ext_afgestemde_adressen voor relatie'

                                                || to_char(l_mn_numrelatie)

                                                || ' met eat_id: '|| l_eat_id

                                              )

                                || ' ' || dbms_utility.format_error_backtrace

                              );


 WHEN OTHERS THEN
        raise_application_error ( -20000

                              , stm_app_error ( sqlerrm

                                              , g_package_name

                                              , 'rle_vastleggen_bericht'

                                              ) || ' ' || dbms_utility.format_error_backtrace

                              );
END RLE_VASTLEGGEN_BERICHT;
/* Uitlezen bestand en als XML bericht aanbieden aan BPL */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN NUMBER
 ,P_VERWERKINGSAANTAL IN NUMBER := NULL
 )
 IS

CN_MODULE CONSTANT VARCHAR2(50) := 'RLE_EXT_AFG_ADRESSEN.VERWERK';

L_VOLGNUMMER PLS_INTEGER := 0;
L_ADRES_COUNT PLS_INTEGER := 0;
L_ADRES_COUNT_CUMULATIEF PLS_INTEGER := 0;
L_REGELNUMMER PLS_INTEGER := 1;
L_LIJSTNUMMER PLS_INTEGER := 1;
L_MELDINGEN CLOB;
L_MN_NUMRELATIE VARCHAR2(9);
cursor c_eab  ( b_mn_numrelatie varchar2 )
is
select xmlelement(verwerk_afgestemd_adres
      ,xmlattributes('http://mn-services.nl/berichten/cendris/verwerk-afgestemd-adres' as "xmlns")
      ,xmlelement(rol_codrol          ,rol_codrol           )
      ,xmlelement(soort_adres         ,soort_adres          )
      ,xmlelement(datum_selectie      ,dat_selectie         )
      ,xmlelement(datum_ontvangen                           )
      ,xmlelement(mn_numrelatie       ,mn_numrelatie        )
      ,xmlelement(mn_persoonsnummer   ,mn_persoonsnummer    )
      ,xmlelement(mn_voorletters      ,mn_voorletters       )
      ,xmlelement(mn_tussenvoegsel    ,mn_tussenvoegsel     )
      ,xmlelement(mn_achternaam       ,mn_achternaam        )
      ,xmlelement(mn_straat           ,mn_straat            )
      ,xmlelement(mn_huisnr           ,mn_huisnr            )
      ,xmlelement(mn_huisnr_toevoeging,mn_huisnr_toevoeging )
      ,xmlelement(mn_postcode         ,mn_postcode          )
      ,xmlelement(mn_woonplaats       ,mn_woonplaats        )
      ,xmlelement(uitspraakcode       ,uitspraakcode        )
      ,xmlelement(overeenkomst_in_perc,overeenkomst_in_perc )
      ,xmlelement(postcode            ,replace(postcode,' '))
      ,xmlelement(woonplaats          ,woonplaats           )
      ,xmlelement(voorletters         ,voorletters          )
      ,xmlelement(voorvoegsels        ,voorvoegsels         )
      ,xmlelement(achternaam          ,achternaam           )
      ,xmlelement(achtervoegsels      ,achtervoegsels       )
      ,xmlelement(straatnaam          ,straatnaam           )
      ,xmlelement(huisnr              ,huisnr               )
      ,xmlelement(huisnr_toevoeging   ,huisnr_toevoeging    )
      ,xmlelement(verhuiscode         ,verhuiscode          )
      ,xmlelement(verhuisdatum        ,to_char(to_date(verhuisdatum, 'yyyymmdd')
                                              ,'dd-mm-yyyy')
                                                            )
      ,xmlelement(datum_nazenden_tm   ,to_char(to_date(datum_nazenden_tm, 'yyyymmdd')
                                              ,'dd-mm-yyyy')
                                                            )
                ).extract('/*') adres_xml
      ,eab.mn_numrelatie
from   rle_ext_adressen_bestand eab
where  eab.mn_numrelatie > b_mn_numrelatie
order by eab.mn_numrelatie;

-- XML type kan niet in Designer gedefinieerd, datatype daarom hier gedeclareerd.

l_adres_xml xmltype;
l_bericht   xmltype;

   /************************************************************
   Procedurenaam: verwerk

   Beschrijving

   Uitlezen data uit RLE_EXT_ADRESSEN_BESTAND en als XML aanbieden aan BPL

   Datum        Wie              Wat
   ----------   --------------   -------------------------
   25-02-2009   Peter Rurenga    Creatie
   31-03-2009   E. Schillemans   Mogelijkheid ingebouwd om het
                                 aantal adressen op te geven dat
                                 verwerkt moet worden. Geimplementeerd
                                 met herstart sleutels.
   *************************************************************/
BEGIN
   stm_util.t_procedure     := cn_module;
   stm_util.t_script_naam   := p_script_naam;
   stm_util.t_script_id     := p_script_id;
   stm_util.t_programma_naam:= 'RLE_EXT_AFG_ADRESSEN';

   if stm_util.herstart
   then
     l_mn_numrelatie          := stm_util.lees_herstartsleutel('l_mn_numrelatie');
     l_adres_count_cumulatief := stm_util.lees_herstarttelling('l_adres_count_cumulatief');
     l_lijstnummer            := stm_util.lees_herstarttelling('l_lijstnummer') + 1;
   else
     l_mn_numrelatie          := '0';
     l_adres_count_cumulatief := 0;
     l_lijstnummer            := 1;
   end if;

   stm_util.insert_lijsten(l_lijstnummer
                          ,l_regelnummer
                          ,'*** Procedure '||cn_module||' ,inlezen ext gecontrl adres aan BPL'
                          ||' start: '||to_char(sysdate, 'dd-mm-yyyy hh24:mi')||' ***'
                          );

   open c_eab ( l_mn_numrelatie );
   fetch c_eab into l_adres_xml, l_mn_numrelatie;
   while c_eab%found
   loop
     -- bericht maken
     l_bericht := berichten_portaal.MAAK_BERICHT
       (p_payload     => l_adres_xml
       ,p_berichttype => 'VERWERK_AFGESTEMD_ADRES'
       ,p_berichtid   => 'CENDRIS:'||l_mn_numrelatie
       ,p_bronsysteem => 'CENDRIS'
       ,p_administratie => 'PMT');

     -- bericht aanbieden aan BPL:
     berichtenportaal.verstuur (p_bericht             =>  l_bericht.getclobval
                               ,p_validatie_meldingen => l_meldingen ) ;

     -- teller bijhouden
     l_adres_count := l_adres_count + 1;
     l_adres_count_cumulatief := l_adres_count_cumulatief + 1;

     if p_verwerkingsaantal is not null and
        l_adres_count = p_verwerkingsaantal
     then
       stm_util.schrijf_herstartsleutel ('l_mn_numrelatie', l_mn_numrelatie);
       stm_util.schrijf_herstarttelling ('l_adres_count_cumulatief', l_adres_count_cumulatief);
       stm_util.schrijf_herstarttelling ('l_lijstnummer', l_lijstnummer);

       -- Extra fetch om te bepalen of het laatste record is opgehaald
       fetch c_eab into l_adres_xml, l_mn_numrelatie;
       exit;
     else
       fetch c_eab into l_adres_xml, l_mn_numrelatie;
     end if;

   end loop;
   if c_eab%found
   then
     -- Verwerking nog niet compleet
     stm_util.insert_lijsten(l_lijstnummer
                            ,l_regelnummer
                            ,'Verwerking nog niet compleet, aantal verwerkte adressen: '||l_adres_count
                            );
   else
     -- Verwerking compleet
     stm_util.verwijder_herstart;
     stm_util.insert_lijsten(l_lijstnummer
                            ,l_regelnummer
                            ,'Verwerking compleet, totaal aantal verwerkte adressen: '||l_adres_count_cumulatief
                            );
   end if;
   close c_eab;

   stm_util.insert_job_statussen(l_volgnummer, 'S', '0');
   commit;

 EXCEPTION
   when others
   then
      rollback;
      if c_eab%isopen
        then
           close c_eab;
      end if;
      stm_util.insert_job_statussen(l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
      stm_util.insert_job_statussen(l_volgnummer
                                  , 'I'
                                  , substr(cn_module,1,80)
                                  );
      stm_util.insert_job_statussen(l_volgnummer
                                  , 'I'
                                  , substr(sqlerrm,1,255)
                                  );
      commit;
      raise_application_error(-20000,cn_module||' '||substr(sqlerrm,1,250));
END verwerk;
/* Vastleggen resultaat */
PROCEDURE RLE_VASTLEGGEN_RESULTAAT
 (P_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 ,P_RESULTAAT IN RLE_EXT_AFGESTEMDE_ADRESSEN.RESULTAAT%TYPE
 )
 IS
/************************************************************
   Procedurenaam: rle_update_resultaat

   Beschrijving

   Het invullen van het resultaat voor een afgestemd adres
   zodat de vervolgstap bekend is.

   Datum        Wie              Wat
   ----------   --------------   -------------------------
   25-02-2009   E. Schillemans   Creatie
   06-11-2009   M. Koenen        Procedure valid gemaakt. Na end
                                 stond rle_update_resultaat.
   *************************************************************/
begin
  update rle_ext_afgestemde_adressen
  set resultaat = p_resultaat
  where id = p_id;
exception
  when others
  then
    raise_application_error ( -20000
                            , stm_app_error ( sqlerrm
                                            , g_package_name
                                            , 'rle_update_resultaat'
                                            ) || ' ' || dbms_utility.format_error_backtrace
                            );
--end rle_update_resultaat;
end rle_vastleggen_resultaat;
/* Geef indicatie verwerkt een waarde */
PROCEDURE RLE_ZET_IND_VERWERKT
 (P_ID IN RLE_EXT_AFGESTEMDE_ADRESSEN.ID%TYPE
 ,P_IND_VERWERKT IN RLE_EXT_AFGESTEMDE_ADRESSEN.IND_VERWERKT%TYPE
 )
 IS
/************************************************************
   Procedurenaam: rle_zet_ind_verwerkt

   Beschrijving

   Het invullen van een waarde voor ind_verwerkt. Deze wordt
   initieel gevuld door procedure rle_vastleggen_bericht maar
   procedures die later worden aangeroepen kunnen foutgaan en
   dan dient ind_verwerkt te worden aangepast.

   Datum        Wie              Wat
   ----------   --------------   -------------------------
   12-03-2009   E. Schillemans   Creatie
   *************************************************************/
begin
  update rle_ext_afgestemde_adressen
  set ind_verwerkt = p_ind_verwerkt
  where id = p_id;
exception
  when others
  then
    raise_application_error ( -20000
                            , stm_app_error ( sqlerrm
                                            , g_package_name
                                            , 'rle_zet_ind_verwerkt'
                                            ) || ' ' || dbms_utility.format_error_backtrace
                            );
end rle_zet_ind_verwerkt;

END RLE_EXT_AFG_ADRESSEN;
/