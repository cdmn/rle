CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_VERWERK_SYN_GBA IS


/****************************************************************

  Packagenaam: RLE_VERWERK_SYN_GBA
  Beschrijving
    Verwerken synchronisatie bestand GBA => RLE.
    De door het GBA aangeleverde rubrieken worden verwerkt in RLE.
    Dit kunnen door het GBA geconstateerde verschillen zijn
    (n.a.v. het door RLEPRC62 aan het GBA verzonden synchronisatie bestand)
    of een willekeurige set met (te synchroniseren) rubrieken. Beide situaties
    worden op dezelfde wijze verwerkt.


  Versie Datum      Wie        Wat
  ------ --------   ---------- --------------------------------
  1.0    28-08-2013 XKV        Creatie
  1.1    31-03-2013 XSW/WHR    Aanpassingen HERBA deel 2
*****************************************************************/  --
  -- Globals
  --
  g_volgnummer                 pls_integer := 0;
  g_adressen_gemuteerd         pls_integer := 0;
  g_geboortedatum_gemuteerd    pls_integer := 0;
  g_partners_gemuteerd         pls_integer := 0;
  g_overlijdensdatum_gemuteerd pls_integer := 0;
  g_aantal_gemuteerd           pls_integer := 0;
  g_i                          pls_integer := 0;

  g_gba_tab t_gba_tab;
  --
  -- Constanten
  --
  cn_package     constant varchar2(30) := 'RLE_VERWERK_SYN_GBA';
  cn_module      constant stm_util.t_procedure%type := cn_package ||'.VERWERK';

  -- Aangeleverde bestand
  cursor c_gba
  is
   SELECT anummer
         ,bsn
         ,rubrieknummer
         ,nvl(ebrpwaarde, 'null') ebrpwaarde
   FROM   rle_syn_van_gba
   WHERE  anummer IS NOT NULL
   OR     bsn IS NOT NULL
   ORDER  BY anummer
            ,bsn
            ,rubrieknummer ASC
   ;

  -- relatienummer
  cursor c_ext ( b_extern_relatie rle_relatie_externe_codes.extern_relatie%type,
                 b_dwe_wrddom_extsys rle_relatie_externe_codes.dwe_wrddom_extsys%type)
  is
   SELECT rle_numrelatie
   FROM   rle_relatie_externe_codes
   WHERE  dwe_wrddom_extsys = b_dwe_wrddom_extsys
   AND    extern_relatie = b_extern_relatie;

  -- persoonsgegevens
  cursor c_per(b_relatienummer rle_relatie_externe_codes.rle_numrelatie%type)
  is
   SELECT per.voornaam
         ,per.voorvoegsels
         ,dwe_wrddom_vvgsl
         ,per.namrelatie AS naamrelatie
         ,to_char(per.datgeboorte, 'yyyymmdd') AS datgeboorte
         ,to_char(per.datum_overlijden, 'yyyymmdd') AS datum_overlijden
         ,per.relatienummer
         ,asa.afnemers_indicatie
         ,dwe_wrddom_geslacht AS geslacht
         ,code_aanduiding_naamgebruik AS code_naamgebruik
         ,per.voorletter
         ,rel.indinstelling
         ,rel.handelsnaam
         ,rel.datbegin
         ,rel.dateinde
         ,rel.datoprichting
         ,rel.code_gebdat_fictief
         ,rel.regdat_overlbevest
         ,rel.datemigratie
         ,rel.naam_partner
         ,rel.vvgsl_partner
         ,scs.rle_gewenste_indicatie
         ,per.persoonsnummer
   FROM   rle_v_personen               per
         ,rle_relaties                 rel
         ,rle_afstemmingen_gba         asa
         ,rle_synchronisatie_statussen scs
   WHERE  per.relatienummer = rel.numrelatie
   AND    per.relatienummer = asa.rle_numrelatie(+)
   AND    per.relatienummer = scs.rle_numrelatie(+)
   AND    per.relatienummer = b_relatienummer;

  -- nationaliteit
  cursor c_nat ( b_rle_numrelatie in rle_nationaliteiten.rle_numrelatie%type)
  is
   SELECT nat.code_nationaliteit
         ,nat.dat_einde
   FROM   rle_nationaliteiten nat
   WHERE  nat.rle_numrelatie = b_rle_numrelatie
   AND    nat.dat_begin >=
          (SELECT MAX(nat2.dat_begin)
           FROM rle_nationaliteiten nat2
           WHERE nat2.rle_numrelatie = b_rle_numrelatie
          );

  -- huwelijk/geregistreerd partnerschap
  cursor c_huw ( b_rle_numrelatie       in rle_relatie_koppelingen.rle_numrelatie%type
               , b_rle_numrelatie_prtnr in rle_relatie_koppelingen.rle_numrelatie%type
               )
  is
    SELECT to_char(rkn.dat_begin, 'yyyymmdd') AS dat_begin
          ,to_char(rkn.dat_einde, 'yyyymmdd') AS dat_einde
    FROM   rle_relatie_koppelingen            rkn
    WHERE  rkn.rle_numrelatie                 = b_rle_numrelatie
    AND    rkn.rle_numrelatie_voor            = b_rle_numrelatie_prtnr;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );
PROCEDURE CONTROLEER_VERWERKING
 (P_TYPE_VERWERKING IN varchar2
 );
PROCEDURE VERWERK_RUBRIEKSET
 (P_REC_GBA IN REC_GBA
 ,P_CONTROLEER_VERWERKING IN VARCHAR2
 ,P_TYPE_VERWERKING IN varchar2
 ,P_GEWIJZIGD OUT BOOLEAN
 ,P_SUCCES OUT BOOLEAN
 ,P_FOUTMELDING OUT VARCHAR2
 );


FUNCTION MAAK_UITBERICHT
 RETURN BOOLEAN
 IS
begin
  return g_maak_uit_bericht;
end;
PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS


    /**********************************************************************
    Naam:         WEGSCHRIJVEN STM_LIJST
    Beschrijving:
    **********************************************************************/

    cn_lijst constant pls_integer := 1;
begin
  stm_util.insert_lijsten(cn_lijst, g_volgnummer, p_text);
  stm_util.debug('LIJST :' || p_text);
end lijst;
PROCEDURE CONTROLEER_VERWERKING
 (P_TYPE_VERWERKING IN varchar2
 )
 IS
/***************************************************************************
    Naam:         CONTROLEER_VERWERKING
    Beschrijving: Indien door de gebruiker aangegeven kan de batch achteraf
                  controleren of alles in RLE is verwerkt. Let op, deze optie
                  kan de doorlooptijd aanzienlijk vertragen!
  ***************************************************************************/
  i  pls_integer;
  r_per            c_per%rowtype;
  r_par            c_per%rowtype;
  r_nat            c_nat%rowtype;
  r_huw            c_huw%rowtype;
  l_relatienummer  rle_relatie_externe_codes.rle_numrelatie%type;
  l_relnr_prtner   rle_relatie_externe_codes.rle_numrelatie%type;
  l_postcode       varchar2( 4000 );
  l_huisnr         number;
  l_toevnum        varchar2( 4000 );
  l_woonplaats     varchar2( 4000 );
  l_straat         varchar2( 4000 );
  l_codland        varchar2( 4000 );
  l_locatie        varchar2( 4000 );
  l_ind_woonboot   varchar2( 4000 );
  l_ind_woonwagen  varchar2( 4000 );
  l_landnaam       varchar2( 4000 );
  l_begindatum     date;
  l_einddatum      date;
  l_opdrachtgever  number;
  l_produkt        varchar2( 4000 );
  l_verschil       boolean:=false;
  l_bestand        utl_file.file_type;
  l_bestandnaam    varchar2(300);

 -- Code voorvoegsel
 cursor c_vvl (b_vvl varchar2)
 is
  select code
  from   rfe_v_domeinwaarden  rfe
  where  rfe.domeincode    = 'VVL'
  and    waarde = lower(b_vvl);

  r_vvl            c_vvl%rowtype;
  r_vvl_prtnr      c_vvl%rowtype;
begin
 lijst('');
 lijst('**** NIET VERWERKTE VERSCHILLEN GBA => RLE ****');
 lijst('');

 stm_util.debug('**** Maak bestand niet_verw_afwijkingen.cvs ****');
 l_bestandnaam := 'RLEPRC64_NIETVERWERKT'||to_char(sysdate,'YYYYMMDDHH24MI')||'.csv';
 l_bestand:=utl_file.fopen ( 'RLE_OUTBOX',l_bestandnaam, 'W' );
 utl_file.put_line ( l_bestand,'A-nummer;BSN;Rubrieknummer;E-BRP waarde;RLE waarde;Foutmelding;Persoonsnummer');

 for i in nvl ( g_gba_tab.first, 0) .. nvl ( g_gba_tab.last, -1) loop
   r_per           := null;
   r_par           := null;
   r_nat           := null;
   r_huw           := null;
   l_relatienummer := null;
   l_relnr_prtner  := null;
   l_postcode      := null;
   l_huisnr        := null;
   l_toevnum       := null;
   l_woonplaats    := null;
   l_straat        := null;
   l_codland       := null;
   l_locatie       := null;
   l_ind_woonboot  := null;
   l_ind_woonwagen := null;
   l_landnaam      := null;
   l_begindatum    := null;
   l_einddatum     := null;

  ----------------------------------
  -- Haal de gegevens op uit RLE  --
  ----------------------------------
  -- Type verwerking (R)elatie
  stm_util.debug('**** Relatienummer bepalen voor A/Sofi-nummer '||nvl(g_gba_tab(i).a_nummer,g_gba_tab(i).bsn)||' ****');
  open c_ext ( b_extern_relatie    => g_gba_tab(i).bsn
            , b_dwe_wrddom_extsys => 'SOFI'
            );
  fetch c_ext
  into l_relatienummer;
  close c_ext;

  if l_relatienummer is null
  then
    open c_ext( b_extern_relatie    => g_gba_tab(i).a_nummer
              , b_dwe_wrddom_extsys => 'GBA'
              );
   fetch c_ext
    into l_relatienummer;
   close c_ext;
  end if;

  if p_type_verwerking = 'R'
  then

    stm_util.debug('**** Bepaal persoonsgegevens voor relatienummer '||l_relatienummer||' ****');
    open  c_per( b_relatienummer => l_relatienummer );
    fetch c_per
    into  r_per;
    close c_per;

    stm_util.debug('**** Bepaal nationaliteit voor relatienummer '||l_relatienummer||' ****');
    if g_gba_tab(i).nationaliteit is not null
    then
      open c_nat( b_rle_numrelatie => l_relatienummer);
     fetch c_nat
      into r_nat;
     close c_nat;
    end if;

    if g_gba_tab(i).straatnaam is not null or
      g_gba_tab(i).huisnummer is not null or
      g_gba_tab(i).huisnummer_toevoeging is not null or
      g_gba_tab(i).postcode is not null or
      g_gba_tab(i).woonplaatsnaam is not null or
      g_gba_tab(i).land_naar_vertrokken is not null or
      g_gba_tab(i).datum_vertrek_nl is not null
    then
     stm_util.debug('**** Adres gegevens bepalen ****');
    --Bepaal de default opdrachtgever
    l_opdrachtgever := to_number( stm_algm_get.sysparm( p_systeem => 'PMA'
                                                      , p_sysparm => 'OPDR_GEVER'
                                                      , p_datum   => sysdate)
                                 );
    -- Bepaal het default produkt
    l_produkt := stm_algm_get.sysparm( p_systeem => 'PMA'
                                     , p_sysparm => 'PRODUKT'
                                     , p_datum   => sysdate
                                     );
    -- Bepaal Adresvelden
    rle_get_adrsvelden( p_opdrachtgever => l_opdrachtgever
                      , p_produkt       => l_produkt
                      , p_numrelatie    => r_per.relatienummer
                      , p_codrol        => 'PR'
                      , p_srtadres      => 'DA'
                      , p_datump        => sysdate
                      , p_postcode      => l_postcode
                      , p_huisnr        => l_huisnr
                      , p_toevnum       => l_toevnum
                      , p_woonplaats    => l_woonplaats
                      , p_straat        => l_straat
                      , p_codland       => l_codland
                      , p_locatie       => l_locatie
                      , p_ind_woonboot  => l_ind_woonboot
                      , p_ind_woonwagen => l_ind_woonwagen
                      , p_landnaam      => l_landnaam
                      , p_begindatum    => l_begindatum
                      , p_einddatum     => l_einddatum
                      );

    end if;

    r_vvl := null;
    if g_gba_tab(i).voorvoegsel is not null
    then
      stm_util.debug('**** Bepaal code voorvoegsel voor '||g_gba_tab(i).voorvoegsel||' ****');
      open  c_vvl( b_vvl => lower(g_gba_tab(i).voorvoegsel) );
      fetch c_vvl
      into  r_vvl;
      close c_vvl;
    end if;

  end if;

  -- Type verwerking (P)artner
  if p_type_verwerking = 'P'
  then

    if g_gba_tab(i).a_nummer_prtner is not null or
       g_gba_tab(i).bsn_prtner is not null or
       g_gba_tab(i).voornamen_prtner is not null or
       g_gba_tab(i).voorvoegsel_prtner is not null or
       g_gba_tab(i).geslachtsnaam_prtner is not null or
       g_gba_tab(i).geboortedatum_prtner is not null
    then
      stm_util.debug('**** Relatienummer bepalen voor A-nummer '||g_gba_tab(i).a_nummer_prtner||' ****');
      open c_ext ( b_extern_relatie    => g_gba_tab(i).bsn_prtner
                 , b_dwe_wrddom_extsys => 'SOFI'
                 );
      fetch c_ext
      into l_relnr_prtner;
      close c_ext;

      if l_relnr_prtner is null
      then
       stm_util.debug('**** Relatienummer bepalen voor Sofinummer '||g_gba_tab(i).bsn_prtner||' ****');
        open c_ext ( b_extern_relatie    => g_gba_tab(i).a_nummer_prtner
                   , b_dwe_wrddom_extsys => 'GBA'
                   );
       fetch c_ext
        into l_relnr_prtner;
       close c_ext;
      end if;

      stm_util.debug('**** Bepaal partnergegevens voor relatienummer '||l_relnr_prtner||' ****');
      open  c_per( b_relatienummer => l_relnr_prtner );
      fetch c_per
      into r_par;
      close c_per;

      stm_util.debug('**** Bepaal datum sluiting voor relatienummer '||l_relatienummer||' ****');
      if g_gba_tab(i).datum_sluiting is not null
      then
        open  c_huw( b_rle_numrelatie       => l_relatienummer
                   , b_rle_numrelatie_prtnr => l_relnr_prtner
                   );
       fetch c_huw
        into r_huw;
       close c_huw;
      end if;

    end if;

  end if;

  r_vvl_prtnr := null;
  if g_gba_tab(i).voorvoegsel_prtner is not null
  then
    stm_util.debug('**** Bepaal code voorvoegsel voor '||g_gba_tab(i).voorvoegsel_prtner||' ****');
    open  c_vvl( b_vvl => lower(g_gba_tab(i).voorvoegsel_prtner) );
    fetch c_vvl
    into  r_vvl_prtnr;
    close c_vvl;
  end if;

  --------------------------------------------------
  -- Controleer of de verschillen verwerkt zijn   --
  --------------------------------------------------

  -- Type verwerking (R)elatie
  if p_type_verwerking = 'R'
  then

    if g_gba_tab(i).voornamen is not null and
      g_gba_tab(i).voornamen <> nvl(r_per.voornaam,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';010210;'||
                                   case g_gba_tab(i).voornamen when 'null' then null else g_gba_tab(i).voornamen end||';'||
                                   r_per.voornaam||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).voorvoegsel is not null and
      nvl(r_vvl.code,g_gba_tab(i).voorvoegsel) <> nvl(r_per.voorvoegsels,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';010230;'||
                                   case g_gba_tab(i).voorvoegsel when 'null' then null else g_gba_tab(i).voorvoegsel end||';'||
                                   r_per.voorvoegsels||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).geslachtsnaam is not null and
      g_gba_tab(i).geslachtsnaam <> nvl(r_per.naamrelatie,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';010240;'||
                                   case g_gba_tab(i).geslachtsnaam when 'null' then null else g_gba_tab(i).geslachtsnaam end||';'||
                                   r_per.naamrelatie||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).geboortedatum is not null and
      g_gba_tab(i).geboortedatum <> nvl(r_per.datgeboorte,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';010310;'||
                                   case g_gba_tab(i).geboortedatum when 'null' then null else g_gba_tab(i).geboortedatum end||';'||
                                   r_per.datgeboorte||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).geslachts_aanduiding is not null and
      g_gba_tab(i).geslachts_aanduiding  <> nvl(r_per.geslacht,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';010410;'||
                                   case g_gba_tab(i).geslachts_aanduiding  when 'null' then null else g_gba_tab(i).geslachts_aanduiding  end||';'||
                                   r_per.geslacht||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).aanduiding_naamgebr is not null and
      g_gba_tab(i).aanduiding_naamgebr  <> nvl(r_per.code_naamgebruik,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';016110;'||
                                   case g_gba_tab(i).aanduiding_naamgebr  when 'null' then null else g_gba_tab(i).aanduiding_naamgebr  end||';'||
                                   r_per.code_naamgebruik||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).nationaliteit is not null and
      g_gba_tab(i).nationaliteit  <> nvl(r_nat.code_nationaliteit,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';040510;'||
                                   case g_gba_tab(i).nationaliteit  when 'null' then null else g_gba_tab(i).nationaliteit  end||';'||
                                   r_nat.code_nationaliteit||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;


    if g_gba_tab(i).voorvoegsel_prtner is not null and
      nvl(r_vvl_prtnr.code,g_gba_tab(i).voorvoegsel_prtner)  <> nvl(r_per.vvgsl_partner,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';050230;'||
                                   case g_gba_tab(i).voorvoegsel_prtner  when 'null' then null else g_gba_tab(i).voorvoegsel_prtner end||';'||
                                   r_per.vvgsl_partner||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).geslachtsnaam_prtner is not null and
      g_gba_tab(i).geslachtsnaam_prtner <> nvl(r_per.naam_partner,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';050240;'||
                                   case g_gba_tab(i).geslachtsnaam_prtner  when 'null' then null else g_gba_tab(i).geslachtsnaam_prtner end||';'||
                                   r_per.naam_partner||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).datum_overlijden is not null and
      g_gba_tab(i).datum_overlijden  <> nvl(r_per.datum_overlijden,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';060810;'||
                                   case g_gba_tab(i).datum_overlijden  when 'null' then null else g_gba_tab(i).datum_overlijden end||';'||
                                   r_per.datum_overlijden||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
     end if;

    if g_gba_tab(i).straatnaam is not null and
      g_gba_tab(i).straatnaam  <> nvl(l_straat,'null') and
      nvl(g_gba_tab(i).land_naar_vertrokken,'null') <> 'null'
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';081110;'||
                                   case g_gba_tab(i).straatnaam  when 'null' then null else g_gba_tab(i).straatnaam end||';'||
                                   l_straat||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).huisnummer is not null and
      g_gba_tab(i).huisnummer  <> nvl(to_char(l_huisnr),'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';081120;'||
                                   case g_gba_tab(i).huisnummer  when 'null' then null else g_gba_tab(i).huisnummer end||';'||
                                   to_char(l_huisnr)||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).huisnummer_toevoeging is not null and
      upper(g_gba_tab(i).huisnummer_toevoeging)  <> nvl(upper(l_toevnum),'NULL')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';081140;'||
                                   case g_gba_tab(i).huisnummer_toevoeging  when 'null' then null else g_gba_tab(i).huisnummer_toevoeging end||';'||
                                    l_toevnum||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                    'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).postcode is not null and
      g_gba_tab(i).postcode  <> nvl(l_postcode,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';081160;'||
                                   case g_gba_tab(i).postcode when 'null' then null else g_gba_tab(i).postcode end||';'||
                                   l_postcode||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    -- controle woonplaats/straat alleen voor buitenlandse adressen
    if g_gba_tab(i).woonplaatsnaam is not null and
      upper(g_gba_tab(i).woonplaatsnaam)  <> nvl(upper(l_woonplaats),'NULL') and nvl(g_gba_tab(i).land_naar_vertrokken,'null') <> 'null'
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';081170;'||
                                   case g_gba_tab(i).woonplaatsnaam when 'null' then null else g_gba_tab(i).woonplaatsnaam end||';'||
                                   initcap(l_woonplaats)||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).land_naar_vertrokken is not null and
      g_gba_tab(i).land_naar_vertrokken  <> nvl(case l_codland when 'NL' then null else l_codland end,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';081310;'||
                                   case g_gba_tab(i).land_naar_vertrokken when 'null' then null else g_gba_tab(i).land_naar_vertrokken end||';'||
                                   l_codland||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).datum_vertrek_nl is not null and
      g_gba_tab(i).datum_vertrek_nl  <> nvl(to_char(case l_codland when 'NL' then null else l_begindatum end,'yyyymmdd'),'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';081320;'||
                                   to_char(case l_codland when 'NL' then null else l_begindatum end,'yyyymmdd')||';'||
                                   to_char(l_begindatum,'yyyymmdd')||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_per.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

  end if;

  -- Type verwerking (P)artner
  if p_type_verwerking = 'P'
  then

    if g_gba_tab(i).voornamen_prtner is not null and
      g_gba_tab(i).voornamen_prtner  <> nvl(r_par.voornaam,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';050210;'||
                                   case g_gba_tab(i).voornamen_prtner  when 'null' then null else g_gba_tab(i).voornamen_prtner  end||';'||
                                   r_par.voornaam||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_par.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).voorvoegsel_prtner is not null and
      lower(g_gba_tab(i).voorvoegsel_prtner)  <> nvl(lower(r_par.voorvoegsels),'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';050230;'||
                                   case g_gba_tab(i).voorvoegsel_prtner  when 'null' then null else g_gba_tab(i).voorvoegsel_prtner end||';'||
                                   r_par.voorvoegsels||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_par.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).geslachtsnaam_prtner is not null and
      g_gba_tab(i).geslachtsnaam_prtner <> nvl(r_par.naamrelatie,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';050240;'||
                                   case g_gba_tab(i).geslachtsnaam_prtner  when 'null' then null else g_gba_tab(i).geslachtsnaam_prtner end||';'||
                                   r_par.naamrelatie||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_par.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).geboortedatum_prtner is not null and
      g_gba_tab(i).geboortedatum_prtner  <> nvl(r_par.datgeboorte,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';050310;'||
                                   case g_gba_tab(i).geboortedatum_prtner  when 'null' then null else g_gba_tab(i).geboortedatum_prtner end||';'||
                                   r_par.datgeboorte||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_par.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

    if g_gba_tab(i).datum_sluiting is not null and
      g_gba_tab(i).datum_sluiting  <> nvl(r_huw.dat_begin,'null')
    then
     l_verschil:= true;
     utl_file.put_line ( l_bestand,convert(g_gba_tab(i).a_nummer||';'||g_gba_tab(i).bsn||';050610;'||
                                   case g_gba_tab(i).datum_sluiting  when 'null' then null else g_gba_tab(i).datum_sluiting end||';'||
                                   r_huw.dat_begin||';'||replace(g_gba_tab(i).foutmelding,chr(10),'')||';'||r_par.persoonsnummer,
                                   'UTF8', 'WE8MSWIN1252'));
    end if;

  end if;

 end loop;

 if not l_verschil and g_aantal_gemuteerd > 0
 then
   lijst('');
   lijst('Alle rubrieken zijn correct verwerkt!');
   lijst('');
 elsif not l_verschil and g_aantal_gemuteerd = 0
 then
   lijst('');
   lijst('Gegevens zijn gelijk, er is niets aangepast!');
   lijst('');
 else
   if g_aantal_gemuteerd = 0
   then
   lijst('');
   lijst('Gegevens zijn gelijk, er is niets aangepast!');
   lijst('');
   end if;
   --
   lijst('');
   lijst('Bestand '||l_bestandnaam||' aangemaakt in RLE inbox op de ftp pool!');
   lijst('');
 end if;

 utl_file.fclose(l_bestand);
end controleer_verwerking;
PROCEDURE VERWERK_RUBRIEKSET
 (P_REC_GBA IN REC_GBA
 ,P_CONTROLEER_VERWERKING IN VARCHAR2
 ,P_TYPE_VERWERKING IN varchar2
 ,P_GEWIJZIGD OUT BOOLEAN
 ,P_SUCCES OUT BOOLEAN
 ,P_FOUTMELDING OUT VARCHAR2
 )
 IS


/***************************************************************************
    Naam:         VERWERK_RUBRIEKSET
    Beschrijving: Alle per deelnemer aangeleverde rubrieken zijn platgeslagen
                  naar 1 record per deelnemer (rubriekset). Eerst wordt bekeken
                  welke rubrieken zijn aangeleverd (not null of 'null') voordat
                  de gegevens in RLE worden opgehaald. Lege rubrieken (null) zijn
                  dus niet aangeleverd en behoeven niet te worden vergeleken met RLE.
                  Vervolgens wordt per dienst bekeken of deze moet worden aangeroepen
                  om de GBA gegevens met RLE te synchroniseren. Bij het
                  synchroniseren moeten de 'null' waardes weer vervangen worden
                  door null en de ontbrekende rubrieken(niet aangeleverd) moeten
                  worden aangevuld uit RLE.
  ***************************************************************************/
 -- Code voorvoegsel
 cursor c_vvl (b_vvl varchar2)
 is
  select code
  from   rfe_v_domeinwaarden  rfe
  where  rfe.domeincode    = 'VVL'
  and    waarde = lower(b_vvl);

  r_vvl            c_vvl%rowtype;
  r_vvl_prtnr      c_vvl%rowtype;
  r_per            c_per%rowtype;
  r_par            c_per%rowtype;
  r_nat            c_nat%rowtype;
  r_huw            c_huw%rowtype;
  l_relatienummer  rle_relatie_externe_codes.rle_numrelatie%type;
  l_relnr_prtner   rle_relatie_externe_codes.rle_numrelatie%type;
  l_postcode       varchar2( 4000 );
  l_huisnr         number;
  l_toevnum        varchar2( 4000 );
  l_woonplaats     varchar2( 4000 );
  l_straat         varchar2( 4000 );
  l_codland        varchar2( 4000 );
  l_locatie        varchar2( 4000 );
  l_ind_woonboot   varchar2( 4000 );
  l_ind_woonwagen  varchar2( 4000 );
  l_landnaam       varchar2( 4000 );
  l_begindatum     date;
  l_einddatum      date;
  l_opdrachtgever  number;
  l_produkt        varchar2( 4000 );
  l_ind_succes     varchar2( 1 );
  l_melding        varchar2( 32767 );
  l_dummy          number( 10 );
  l_start_errorpos number;
  l_errorcode      varchar2(9);
  l_errormessage   varchar2(4000);

  stop_verwerking  exception;
begin
  p_succes:= true;

  if p_controleer_verwerking = 'J'
  then
    stm_util.debug('**** Bewaar rubriek set voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||'in array voor controle achteraf. ****');
    g_i:= g_i + 1;
    g_gba_tab(g_i):= p_rec_gba;
  end if;

  savepoint start_verwerking_deeln;
  --------------------------------------------------------------------------
  -- Bepaal de gegevens uit RLE (verschillen bepalen en aanvullen dienst) --
  --------------------------------------------------------------------------
  stm_util.debug('**** Bepalen of sleutel rubrieken afwijkt voor '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' ****');
  if p_rec_gba.bsn_gba is not null
  then
    l_melding := 'Fout voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' opgetreden: sofinummer GBA '||p_rec_gba.bsn_gba||' wijkt af van sofinummer in RLE'||p_rec_gba.bsn;
    raise stop_verwerking;
  elsif p_rec_gba.geboortedatum is not null
  then
    l_melding := 'Fout voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' opgetreden: geboortedatum GBA '||p_rec_gba.geboortedatum||' wijkt af van geboortedatum in RLE';
    raise stop_verwerking;
  elsif p_rec_gba.geslachts_aanduiding is not null
  then
    l_melding := 'Fout voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' opgetreden: geslachtsaanduiding GBA '||p_rec_gba.geslachts_aanduiding||' wijkt af van geslachtsaanduiding in RLE';
    raise stop_verwerking;
  end if;

  stm_util.debug('**** Relatienummer bepalen voor A/Sofi-nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' ****');
  open  c_ext ( b_extern_relatie    => p_rec_gba.bsn
              , b_dwe_wrddom_extsys => 'SOFI'
              );
  fetch c_ext into l_relatienummer;
  close c_ext;

  if l_relatienummer is null
  then
    open  c_ext ( b_extern_relatie    => p_rec_gba.a_nummer
                , b_dwe_wrddom_extsys => 'GBA'
                );
    fetch c_ext into l_relatienummer;
    close c_ext;
  end if;

  if l_relatienummer is null
  then
    stm_util.debug('**** Geen relatie gevonden voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' ****');
    l_melding := 'Geen relatie gevonden voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn);
    raise stop_verwerking;
  end if;

  -- Type verwerking (R)elatie
  if p_type_verwerking = 'R'
  then

    stm_util.debug('**** Bepaal persoonsgegevens voor relatienummer '||l_relatienummer||' ****');
    if l_relatienummer is not null
    then
      open  c_per( b_relatienummer => l_relatienummer );
      fetch c_per
       into r_per;
      close c_per;
    end if;

    if r_per.relatienummer is null
    then
      stm_util.debug('**** Geen relatie gevonden voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' ****');
      l_melding := 'Geen relatie gevonden voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn);
      raise stop_verwerking;
    end if;

    stm_util.debug('**** Bepaal nationaliteit voor relatienummer '||l_relatienummer||' ****');
    if p_rec_gba.nationaliteit is not null
    then
      open  c_nat( b_rle_numrelatie => l_relatienummer );
      fetch c_nat
       into r_nat;
      close c_nat;
    end if;

    stm_util.debug('**** Bepaal adresgegevens: p_rec_gba.straatnaam = '||p_rec_gba.straatnaam
                                            ||'p_rec_gba.huisnummer = '||p_rec_gba.huisnummer
                                            ||'p_rec_gba.huisnummer_toevoeging = '||p_rec_gba.huisnummer_toevoeging
                                            ||'p_rec_gba.postcode = '||p_rec_gba.postcode
                                            ||'p_rec_gba.woonplaatsnaam = '||p_rec_gba.woonplaatsnaam
                                            ||'p_rec_gba.land_naar_vertrokken = '||p_rec_gba.land_naar_vertrokken
                                            ||'p_rec_gba.datum_vertrek_nl = '||p_rec_gba.datum_vertrek_nl||' ****');
    if p_rec_gba.straatnaam is not null or
       p_rec_gba.huisnummer is not null or
       p_rec_gba.huisnummer_toevoeging is not null or
       p_rec_gba.postcode is not null or
       p_rec_gba.woonplaatsnaam is not null or
       p_rec_gba.land_naar_vertrokken is not null or
       p_rec_gba.datum_vertrek_nl is not null
    then
      stm_util.debug('**** Adres gegevens bepalen ****');
      --Bepaal de default opdrachtgever
      l_opdrachtgever := to_number( stm_algm_get.sysparm( p_systeem => 'PMA'
                                                        , p_sysparm => 'OPDR_GEVER'
                                                        , p_datum   => sysdate)
                                   );
      -- Bepaal het default produkt
      l_produkt := stm_algm_get.sysparm( p_systeem => 'PMA'
                                       , p_sysparm => 'PRODUKT'
                                       , p_datum   => sysdate
                                       );
      -- Bepaal Adresvelden
      rle_get_adrsvelden( p_opdrachtgever => l_opdrachtgever
                        , p_produkt       => l_produkt
                        , p_numrelatie    => r_per.relatienummer
                        , p_codrol        => 'PR'
                        , p_srtadres      => 'DA'
                        , p_datump        => sysdate
                        , p_postcode      => l_postcode
                        , p_huisnr        => l_huisnr
                        , p_toevnum       => l_toevnum
                        , p_woonplaats    => l_woonplaats
                        , p_straat        => l_straat
                        , p_codland       => l_codland
                        , p_locatie       => l_locatie
                        , p_ind_woonboot  => l_ind_woonboot
                        , p_ind_woonwagen => l_ind_woonwagen
                        , p_landnaam      => l_landnaam
                        , p_begindatum    => l_begindatum
                        , p_einddatum     => l_einddatum
                        );

    end if;

  end if;

  -- Type verwerking (P)artner
  if  p_type_verwerking = 'P'
  and p_rec_gba.bsn_prtner       is not null
  and p_rec_gba.datum_ontbinding is null
  then

    stm_util.debug('**** Bepaal Partner gegevens: p_rec_gba.a_nummer_prtner = '||p_rec_gba.a_nummer_prtner
                                               ||'p_rec_gba.bsn_prtner = '||p_rec_gba.bsn_prtner||' ****');
    if p_rec_gba.a_nummer_prtner      is not null or
       p_rec_gba.bsn_prtner           is not null
    then
      stm_util.debug('**** Relatienummer bepalen voor A-nummer '||p_rec_gba.a_nummer_prtner||' ****');
      open  c_ext ( b_extern_relatie    => p_rec_gba.bsn_prtner
                  , b_dwe_wrddom_extsys =>'SOFI'
                  );
      fetch c_ext
       into l_relnr_prtner;
      close c_ext;

      if l_relnr_prtner is null
      then
        stm_util.debug('**** Relatienummer bepalen voor Sofinummer '||p_rec_gba.bsn_prtner||' ****');
        open  c_ext ( b_extern_relatie    => p_rec_gba.a_nummer_prtner
                    , b_dwe_wrddom_extsys => 'GBA'
                    );
        fetch c_ext
         into l_relnr_prtner;
        close c_ext;
      end if;

      stm_util.debug('**** Bepaal partnergegevens voor relatienummer '||l_relnr_prtner||' ****');
      if l_relnr_prtner is not null
      then
        open  c_per( b_relatienummer => l_relnr_prtner );
        fetch c_per
         into r_par;
        close c_per;
      end if;

      if l_relnr_prtner is null or
         r_par.relatienummer is null
      then
        stm_util.debug('**** Geen relatie gevonden voor A nummer partner '||p_rec_gba.a_nummer_prtner||' of sofinummer partner '||p_rec_gba.bsn_prtner||' ****');
        l_melding := 'Geen relatie gevonden voor A nummer partner '||p_rec_gba.a_nummer_prtner||' of sofinummer partner '||p_rec_gba.bsn_prtner||chr(10)||
                     '(registratie partner wordt door een ander proces uitgevoerd).';
        raise stop_verwerking;
      end if;

      stm_util.debug('**** Bepaal relatiekoppeling '||l_relatienummer||' en '||l_relnr_prtner||' ****');
      if   l_relatienummer is not null
      and  l_relnr_prtner  is not null
      then
        open  c_huw( b_rle_numrelatie       => l_relatienummer
                   , b_rle_numrelatie_prtnr => l_relnr_prtner
                   );
        fetch c_huw
         into r_huw;
        close c_huw;
      end if;

    end if;

  end if;

  -------------------------------------------------------
  -- Synchroniseer de verschillen met RLE (GBA => RLE) --
  -------------------------------------------------------
  -- Type verwerking (R)elatie
  if p_type_verwerking = 'R'
  then

    stm_util.debug('**** Synchroniseer persoonsgegevens van de deelnemer:'||
                          nvl(p_rec_gba.geslachtsnaam,'null')||'<>'||nvl(r_per.naamrelatie,'null')||'or(2)'||
                          nvl(p_rec_gba.voorvoegsel,'null')||'<>'||nvl(r_per.voorvoegsels,'null')||'or(3)'||
                          nvl(bepaal_voorletters (p_rec_gba.voornamen),'null')||'<>'||nvl(r_per.voorletter,'null')||'or(4)'||
                          substr(nvl(p_rec_gba.voornamen,'null'),1,30)||'<>'||nvl(r_per.voornaam,'null')||'or(5)'||
                          nvl(p_rec_gba.datum_overlijden,'null')||'<>'||nvl(r_per.datum_overlijden,'null')||'or(6)'||
                          nvl(p_rec_gba.geslachtsnaam_prtner,'null')||'<>'||nvl(r_per.naam_partner,'null')||'or(7)'||
                          nvl(p_rec_gba.voorvoegsel_prtner,'null')||'<>'||nvl(r_per.vvgsl_partner,'null')||'or(8)'||
                          nvl(p_rec_gba.aanduiding_naamgebr,'null')||'<>'||nvl(r_per.code_naamgebruik,'null')||'****');

    if  (nvl(p_rec_gba.geslachtsnaam,'null') <> nvl(r_per.naamrelatie,'null') and
         p_rec_gba.geslachtsnaam is not null)
        or
        (nvl(lower(p_rec_gba.voorvoegsel),'null') <> nvl(r_per.voorvoegsels,'null') and
         p_rec_gba.voorvoegsel is not null)
        or
        (nvl(bepaal_voorletters (p_rec_gba.voornamen),'null') <> nvl(r_per.voorletter,'null') and
         p_rec_gba.voornamen is not null)
        or
        (substr(nvl(p_rec_gba.voornamen,'null'),1,30) <> nvl(r_per.voornaam,'null') and
         p_rec_gba.voornamen is not null)
        or
        (nvl(p_rec_gba.datum_overlijden,'null') <> nvl(r_per.datum_overlijden,'null') and
         p_rec_gba.datum_overlijden is not null)
        or
        (nvl(p_rec_gba.aanduiding_naamgebr,'null') <> nvl(r_per.code_naamgebruik,'null') and
         p_rec_gba.aanduiding_naamgebr is not null)
        or
         (nvl(p_rec_gba.geslachtsnaam_prtner,'null') <> nvl(r_per.naam_partner,'null') and
         p_rec_gba.geslachtsnaam_prtner is not null)
        or
        (nvl(lower(p_rec_gba.voorvoegsel_prtner),'null') <> nvl(r_per.vvgsl_partner,'null') and
         p_rec_gba.voorvoegsel_prtner is not null)
    then
      if nvl(p_rec_gba.voorvoegsel,'null') <> 'null'
      then
        stm_util.debug('**** Bepaal code voorvoegsel voor '||p_rec_gba.voorvoegsel||' ****');
        open  c_vvl( b_vvl => lower(p_rec_gba.voorvoegsel) );
        fetch c_vvl
        into  r_vvl;
        if c_vvl%notfound
        then
          close c_vvl;
          l_melding:= 'Voorvoegsel '||lower(p_rec_gba.voorvoegsel)||' niet gevonden in de referentiegegevens!';
          raise stop_verwerking;
        end if;
        close c_vvl;
      end if;

      if nvl(p_rec_gba.voorvoegsel_prtner,'null') <> 'null'
      then
        stm_util.debug('**** Bepaal code voorvoegsel voor '||p_rec_gba.voorvoegsel_prtner||' ****');
        open  c_vvl( b_vvl => lower(p_rec_gba.voorvoegsel_prtner) );
        fetch c_vvl
        into  r_vvl_prtnr;
        if c_vvl%notfound
        then
          close c_vvl;
          l_melding:= 'Voorvoegsel '||lower(p_rec_gba.voorvoegsel_prtner)||' niet gevonden in de referentiegegevens!';
          raise stop_verwerking;
        end if;
        close c_vvl;
      end if;

      rle_rle_toev
        ( p_numrelatie                  => l_relatienummer
        , p_namrelatie                  => case p_rec_gba.geslachtsnaam
                                                when 'null' then null
                                                else nvl(p_rec_gba.geslachtsnaam,r_per.naamrelatie) end
        , p_rol_codrol                  => 'PR'
        , p_indinstelling               => r_per.indinstelling
        , p_wrddom_geslacht             => r_per.geslacht
        , p_wrddom_vvgsl                => case p_rec_gba.voorvoegsel
                                                when 'null' then null
                                                else nvl(r_vvl.code,r_per.dwe_wrddom_vvgsl) end
        , p_voorletter                  => case when p_rec_gba.voornamen = 'null' then r_per.voorletter
                                                else nvl(bepaal_voorletters (p_rec_gba.voornamen),r_per.voorletter) end
        , p_voornaam                    => substr(case p_rec_gba.voornamen
                                                       when 'null' then null
                                                       else nvl(p_rec_gba.voornamen,r_per.voornaam) end,1,30)
        , p_handelsnaam                 => r_per.handelsnaam
        , p_datbegin                    => r_per.datbegin
        , p_dateinde                    => r_per.dateinde
        , p_datoprichting               => r_per.datoprichting
        , p_datgeboorte                 => to_date(r_per.datgeboorte,'yyyymmdd')
        , p_code_gebdat_fictief         => r_per.code_gebdat_fictief
        , p_datoverlijden               => case p_rec_gba.datum_overlijden
                                                when 'null' then null
                                                else to_date(nvl(p_rec_gba.datum_overlijden,r_per.datum_overlijden),'yyyymmdd') end
        , p_datregoverlijdenbevestiging => r_per.regdat_overlbevest
        , p_code_aanduiding_naamgebruik => case p_rec_gba.aanduiding_naamgebr
                                                when 'null' then null
                                                else nvl(p_rec_gba.aanduiding_naamgebr,r_per.code_naamgebruik) end
        , p_naam_partner                => case p_rec_gba.geslachtsnaam_prtner
                                                when 'null' then null
                                                else nvl(p_rec_gba.geslachtsnaam_prtner,r_per.naam_partner) end
        , p_vvgsl_partner               => case p_rec_gba.voorvoegsel_prtner
                                                when 'null' then null
                                                else nvl(r_vvl_prtnr.code,r_per.vvgsl_partner) end
        , p_datemigratie                => r_per.datemigratie
        , p_wrddom_gewatitel            => null
        , p_ind_succes                  => l_ind_succes
        , p_melding                     => l_melding
        , p_externe_code                => l_dummy
        );

      if l_ind_succes = 'N'
      then
        raise stop_verwerking;
      else
        if p_rec_gba.datum_overlijden is not null
        then
          g_overlijdensdatum_gemuteerd:= g_overlijdensdatum_gemuteerd + 1;
        end if;
        if p_rec_gba.geboortedatum is not null
        then
          g_geboortedatum_gemuteerd:= g_geboortedatum_gemuteerd + 1;
        end if;
        p_gewijzigd:= true;
      end if;
    end if;

    stm_util.debug('**** Synchroniseer de nationaliteit van de deelnemer:'||nvl(p_rec_gba.nationaliteit,'null')||'<>'||nvl(r_nat.code_nationaliteit,'null'));
    if nvl(p_rec_gba.nationaliteit,'null') <> nvl(r_nat.code_nationaliteit,'null') and
       p_rec_gba.nationaliteit is not null
    then
      l_melding:= null;
      l_melding:= rle_nat.nat_insert ( p_rle_numrelatie     => l_relatienummer
                                     , p_code_nationaliteit => p_rec_gba.nationaliteit
                                     , p_dat_begin          => nvl(r_nat.dat_einde,sysdate)
                                     , p_dat_einde          => null
                                     );
      if l_melding is not null
      then
        raise stop_verwerking;
      else
        p_gewijzigd:= true;
      end if;
    end if;

    stm_util.debug('**** Synchroniseer de adres gegevens:'||
                         nvl(p_rec_gba.datum_vertrek_nl,'null')||'<>'||nvl(to_char(l_begindatum,'yyyymmdd'),'null')||'or(2)'||
                         nvl(p_rec_gba.land_naar_vertrokken,'null')||'<>'||nvl(l_codland,'null')||'or(3)'||
                         nvl(p_rec_gba.postcode,'null')||'<>'||nvl(l_postcode,'null')||'or(4)'||
                         nvl(p_rec_gba.huisnummer,'null')||'<>'||nvl(to_char(l_huisnr),'null')||'or(5)'||
                         nvl(p_rec_gba.huisnummer_toevoeging,'null')||'<>'||nvl(l_toevnum,'null')||'or(6)'||
                         nvl(upper(p_rec_gba.woonplaatsnaam),'null')||'<>'||nvl(upper(l_woonplaats),'null')||'or(7)'||
                         nvl(p_rec_gba.straatnaam,'null')||'<>'||nvl(l_straat,'null')||'****');


    if (nvl(p_rec_gba.datum_vertrek_nl,'null') <> nvl(to_char(case l_codland when 'NL' then null else l_begindatum end,'yyyymmdd'),'null') and
        p_rec_gba.datum_vertrek_nl is not null)
       or
       (nvl(p_rec_gba.land_naar_vertrokken,'null') <> nvl(case l_codland when 'NL' then null else l_codland end,'null') and
        p_rec_gba.land_naar_vertrokken is not null)
       or
       (nvl(p_rec_gba.postcode,'null') <> nvl(l_postcode,'null') and
        p_rec_gba.postcode is not null)
       or
       (nvl(p_rec_gba.huisnummer,'null') <> nvl(to_char(l_huisnr),'null') and
        p_rec_gba.huisnummer is not null)
       or
       (nvl(upper(p_rec_gba.huisnummer_toevoeging),'NULL') <> nvl(upper(l_toevnum),'NULL') and
        p_rec_gba.huisnummer_toevoeging is not null)
       or
       -- woonplaats en straatnaam alleen voor buitenlandse adressen controleren
       (nvl(upper(p_rec_gba.woonplaatsnaam),'NULL') <> nvl(upper(l_woonplaats),'NULL') and
        p_rec_gba.woonplaatsnaam is not null and nvl(p_rec_gba.land_naar_vertrokken,'null') <> 'null')
       or
       (nvl(p_rec_gba.straatnaam,'null') <> nvl(l_straat,'null') and
        p_rec_gba.straatnaam is not null and nvl(p_rec_gba.land_naar_vertrokken,'null') <> 'null')
    then
      l_codland    := case p_rec_gba.land_naar_vertrokken  when 'null' then 'NL' else nvl(p_rec_gba.land_naar_vertrokken,'NL') end;
      l_postcode   := case p_rec_gba.postcode              when 'null' then null else nvl(p_rec_gba.postcode,l_postcode) end;
      l_huisnr     := case p_rec_gba.huisnummer            when 'null' then null else nvl(p_rec_gba.huisnummer,l_huisnr) end;
      l_toevnum    := case p_rec_gba.huisnummer_toevoeging when 'null' then null else nvl(p_rec_gba.huisnummer_toevoeging,l_toevnum) end;
      l_woonplaats := case p_rec_gba.woonplaatsnaam        when 'null' then null else nvl(p_rec_gba.woonplaatsnaam,l_woonplaats) end;
      l_straat     := case p_rec_gba.straatnaam            when 'null' then null else nvl(p_rec_gba.straatnaam,l_straat) end;

      if p_rec_gba.datum_vertrek_nl = 'null'
      then
        l_begindatum:= null;
      elsif p_rec_gba.datum_vertrek_nl is not null
      then
        -- buitenlands adres
        l_begindatum:= to_date(p_rec_gba.datum_vertrek_nl,'yyyymmdd');
      end if;

      if r_per.datum_overlijden is not null or
         nvl(p_rec_gba.datum_overlijden,'null') <> 'null'
      then
        -- Begindatum overschrijven met overlijdensdatum
        l_begindatum:= case p_rec_gba.datum_overlijden when 'null' then to_date(r_per.datum_overlijden,'yyyymmdd') else to_date(nvl(p_rec_gba.datum_overlijden,r_per.datum_overlijden),'yyyymmdd') end;
      end if;

      l_dummy:= null;
      rle_rle_ads_toev ( p_numrelatie    => l_relatienummer
                       , p_codrol        => 'PR'
                       , p_srt_adres     => 'DA'
                       , p_begindatum    => l_begindatum
                       , p_codland       => l_codland
                       , p_postcod       => l_postcode
                       , p_huisnum       => l_huisnr
                       , p_toevnum       => l_toevnum
                       , p_woonpl        => l_woonplaats
                       , p_straat        => l_straat
                       , p_einddatum     => l_einddatum
                       , p_locatie       => l_locatie
                       , p_numadres      => l_dummy
                       , p_ind_woonwagen => l_ind_woonwagen
                       , p_ind_woonboot  => l_ind_woonboot
                       , p_provincie     => null
                       );
      p_gewijzigd:= true;
      g_adressen_gemuteerd:= g_adressen_gemuteerd + 1;
    end if;

  end if;

  -- Type verwerking (P)artner
  -- gewenste afnemersindicatie = N
  -- relatiekoppeling gevonden
  if  p_type_verwerking = 'P'
  then

    if  r_par.rle_gewenste_indicatie = 'J'
    then
        -- Alleen melden, Gewenste afnemersindicatie voor partrner = J.
        p_succes:= false;
        p_foutmelding:= 'Gewenste afnemersindicatie voor partrner = J, niet verwerken.';
        g_gba_tab(g_i).foutmelding:= substr(p_foutmelding,1,4000);

    elsif r_par.rle_gewenste_indicatie = 'N'
    and   r_huw.dat_begin              is null
    then

        -- Geen relatiekoppeling gevonden
        p_succes:= false;
        p_foutmelding:= 'Geen relatiekoppeling gevonden, niet verwerken.';
        g_gba_tab(g_i).foutmelding:= substr(p_foutmelding,1,4000);

    elsif  r_par.rle_gewenste_indicatie = 'N'
    and    r_huw.dat_begin              is not null
    then

      stm_util.debug('**** Synchroniseer persoonsgegevens van de partner:'||
                           nvl(p_rec_gba.geslachtsnaam_prtner,'null')||'<>'||nvl(r_par.naamrelatie,'null')||'or(2)'||
                           nvl(p_rec_gba.voorvoegsel_prtner,'null')||'<>'||nvl(r_par.voorvoegsels,'null')||'or(3)'||
                           nvl(bepaal_voorletters(p_rec_gba.voornamen_prtner),'null')||'<>'||nvl(r_par.voorletter,'null')||'or(4)'||
                           nvl(p_rec_gba.voornamen_prtner,'null')||'<>'||nvl(r_par.voornaam,'null')||'****');

      if (nvl(p_rec_gba.geslachtsnaam_prtner,'null') <> nvl(r_par.naamrelatie,'null') and
          p_rec_gba.geslachtsnaam_prtner is not null)
         or
         (nvl(lower(p_rec_gba.voorvoegsel_prtner),'null') <> nvl(r_par.voorvoegsels,'null') and
          p_rec_gba.voorvoegsel_prtner is not null)
         or
         (nvl(bepaal_voorletters(p_rec_gba.voornamen_prtner),'null') <> nvl(r_par.voorletter,'null') and
          p_rec_gba.voornamen_prtner is not null)
         or
         (nvl(p_rec_gba.voornamen_prtner,'null') <> nvl(r_par.voornaam,'null') and
          p_rec_gba.voornamen_prtner is not null)
      then
        if nvl(p_rec_gba.voorvoegsel_prtner,'null') <> 'null'
        then
          stm_util.debug('**** Bepaal code voorvoegsel partner voor '||p_rec_gba.voorvoegsel_prtner||' ****');
          r_vvl:=null;
          open c_vvl( b_vvl => lower(p_rec_gba.voorvoegsel_prtner));
          fetch c_vvl into r_vvl;
          if c_vvl%notfound
          then
            close c_vvl;
            l_melding:= 'Voorvoegsel partner'||lower(p_rec_gba.voorvoegsel_prtner)||' niet gevonden in de referentiegegevens!';
            raise stop_verwerking;
          end if;
          close c_vvl;
        end if;

        rle_rle_toev
          ( p_numrelatie                  => l_relnr_prtner
          , p_namrelatie                  => case p_rec_gba.geslachtsnaam_prtner
                                                  when 'null' then null
                                                  else nvl(p_rec_gba.geslachtsnaam_prtner,r_par.naamrelatie) end
          , p_rol_codrol                  => 'PR'
          , p_indinstelling               => r_par.indinstelling
          , p_wrddom_geslacht             => r_par.geslacht
          , p_wrddom_vvgsl                => case p_rec_gba.voorvoegsel_prtner
                                                  when 'null' then null
                                                  else nvl(r_vvl.code,r_par.dwe_wrddom_vvgsl) end
          , p_voorletter                  => case when p_rec_gba.voornamen_prtner = 'null' then r_par.voorletter
                                                  else nvl(bepaal_voorletters (p_rec_gba.voornamen_prtner),r_par.voorletter)  end
          , p_voornaam                    => substr(case p_rec_gba.voornamen_prtner
                                                         when 'null' then null
                                                         else nvl(p_rec_gba.voornamen_prtner,r_par.voornaam) end,1,30)
          , p_handelsnaam                 => r_par.handelsnaam
          , p_datbegin                    => r_par.datbegin
          , p_dateinde                    => r_par.dateinde
          , p_datoprichting               => r_par.datoprichting
          , p_datgeboorte                 => to_date(r_par.datgeboorte,'yyyymmdd')
          , p_code_gebdat_fictief         => r_par.code_gebdat_fictief
          , p_datoverlijden               => to_date(r_par.datum_overlijden,'yyyymmdd')
          , p_datregoverlijdenbevestiging => r_par.regdat_overlbevest
          , p_code_aanduiding_naamgebruik => r_par.code_naamgebruik
          , p_naam_partner                => r_par.naam_partner
          , p_vvgsl_partner               => r_par.vvgsl_partner
          , p_datemigratie                => r_par.datemigratie
          , p_wrddom_gewatitel            => null
          , p_ind_succes                  => l_ind_succes
          , p_melding                     => l_melding
          , p_externe_code                => l_dummy
          );

        if l_ind_succes = 'N'
        then
          raise stop_verwerking;
        else
          p_gewijzigd:= true;
          g_partners_gemuteerd:= g_partners_gemuteerd + 1;
        end if;
      end if;

      stm_util.debug('**** Melding maken van afwijking in datum huwelijk ****');
      if nvl(p_rec_gba.datum_sluiting,'null') <> nvl(r_huw.dat_begin,'null') and
         p_rec_gba.datum_sluiting is not null
      then
        -- Alleen melden, overige deelnemergegevens verwerken.
        p_succes:= false;
        p_foutmelding:= 'Onverwachte foutmelding voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' Datum huwelijk wijkt af: '|| p_rec_gba.datum_sluiting||
                        '(:GBA)-(RLE:)'||r_huw.dat_begin||'. Handmatige aanpassing vereist!'||case when p_gewijzigd then 'De overige wijzigingen zijn verwerkt!' else null end;
        g_gba_tab(g_i).foutmelding:= substr(p_foutmelding,1,4000);
      end if;

    else

        -- overige met partner gegevens, niet verwerkt
        p_succes:= false;
        p_foutmelding:= 'Overige met partner gegevens, niet verwerkt.';
        g_gba_tab(g_i).foutmelding:= substr(p_foutmelding,1,4000);

    end if;

  end if;

exception
when stop_verwerking
then
  rollback to start_verwerking_deeln;
  p_succes      := false;
  p_gewijzigd   := false;

  if instr( l_melding , 'RFE-' ) > 0
  then
    l_start_errorpos := instr( l_melding , 'RFE-' );
  elsif instr( l_melding , 'RLE-' ) > 0
  then
    l_start_errorpos := instr( l_melding , 'RLE-' );
  end if;

  if l_start_errorpos > 0
  then
    l_errorcode := substr( l_melding , l_start_errorpos , 9 );
    l_errormessage := stm_get_batch_message( l_errorcode );
    p_foutmelding := substr( l_errorcode||' '||l_errormessage||' '||chr(10)||l_melding,1,200 );
  else
    p_foutmelding := l_melding;
  end if;

  p_foutmelding := substr('Onverwachte foutmelding voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' opgetreden: '||p_foutmelding,1,300);
  g_gba_tab(g_i).foutmelding:= substr(p_foutmelding,1,4000);
when others
then
  rollback to start_verwerking_deeln;
  p_succes      := false;
  p_gewijzigd   := false;

  l_melding:= substr(sqlerrm,1,32767);

  if instr( l_melding , 'RFE-' ) > 0
  then
    l_start_errorpos := instr( l_melding , 'RFE-' );
  elsif instr( l_melding , 'RLE-' ) > 0
  then
    l_start_errorpos := instr( l_melding , 'RLE-' );
  end if;

  if l_start_errorpos > 0
  then
    l_errorcode := substr( l_melding , l_start_errorpos , 9 );
    l_errormessage := stm_get_batch_message( l_errorcode );
    p_foutmelding := substr( l_errorcode||' '||l_errormessage||' '||chr(10)||l_melding,1,200 );
  else
    p_foutmelding := l_melding;
  end if;

  p_foutmelding := substr('Onverwachte foutmelding voor A/Sofi nummer '||nvl(p_rec_gba.a_nummer,p_rec_gba.bsn)||' opgetreden: '||p_foutmelding,1,300);
  g_gba_tab(g_i).foutmelding:= substr(p_foutmelding,1,4000);
end verwerk_rubriekset;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_TYPE_VERWERKING IN VARCHAR2 := 'R'
 ,P_CONTROLEER_VERWERKING IN VARCHAR2 := 'N'
 )
 IS


 /**********************************************************************
    Naam:         VERWERK
    Beschrijving: Verwerk door GBA aangeleverde rubrieken.
  **********************************************************************/
   gba rec_gba;
   l_laatste_nr    varchar2(300);
   l_succes        boolean;
   l_gewijzigd     boolean;
   l_foutmelding   varchar2(32767);
   l_land          varchar2 (50);
   l_huisletter    varchar2 (5);
   l_huisnumtoev   varchar2 (5);
   l_aand_huisnum  varchar2 (5);
   l_aangeboden    number:=0;
   l_rec_aangeb    number:=0;
   l_gemuteerd     number:=0;
   l_aantal_fout   number:=0;
   l_aantal_gelijk number:=0;
   l_fout_1        number:=0;
   l_fout_2        number:=0;
   l_fout_3        number:=0;
   l_fout_4        number:=0;
   l_fout_5        number:=0;
   l_fout_6        number:=0;
   l_fout_7        number:=0;
   l_fout_8        number:=0;
   l_fout_9        number:=0;
   l_fout_10       number:=0;
   l_fout_11       number:=0;
   l_fout_12       number:=0;
   l_fout_13       number:=0;
   l_fout_14       number:=0;
   l_fout_15       number:=0;
   l_starttijd     date:=sysdate;
begin
 --stm_util.set_debug_on;
  dbms_output.enable(null);
  stm_util.module_start(cn_module);
  stm_util.t_script_naam    := P_SCRIPT_NAAM;
  stm_util.t_script_id      := P_SCRIPT_ID;
  stm_util.t_programma_naam := cn_package;
  stm_util.debug('**** P_CONTROLEER_VERWERKING = '||P_CONTROLEER_VERWERKING||' ****');

  stm_util.debug('**** Voorkom aanmaken uitbericht GBA (wordt uitgelezen in PER_MK_PLAATSING_GBT) ****');
  g_maak_uit_bericht:= false;
  --
  lijst('');
  lijst ( rpad ('***** LOGVERSLAG Verwerken synchronisatie bestand GBA => RLE *****', 70)
               || ' STARTDATUM: ' || to_char (sysdate,'dd-mm-yyyy hh24:mi:ss'));

  lijst ( ' ' );
  --
  stm_util.debug('**** Loop over de door de GBA aangeleverde rubrieken ****');
  -- aantal records aangeboden
  select count (*)
  into   l_rec_aangeb
  from   comp_rle.rle_syn_van_gba
  ;
  --
  for r_gba in c_gba loop

   stm_util.debug('**** Als '||nvl(l_laatste_nr,nvl(r_gba.anummer,r_gba.bsn))||' <> '||nvl(r_gba.anummer,r_gba.bsn)||' is het record compleet. Nu record verwerken. ****');
   if nvl(l_laatste_nr,nvl(r_gba.anummer,r_gba.bsn)) <> nvl(r_gba.anummer,r_gba.bsn)
   then
     stm_util.debug('**** Verwerk rubriek set voor A/Sofi nummer '||nvl(r_gba.anummer,r_gba.bsn)||' ****');
     l_aangeboden:= l_aangeboden + 1;
     verwerk_rubriekset
       ( p_rec_gba               => gba
       , p_controleer_verwerking => p_controleer_verwerking
       , p_type_verwerking       => p_type_verwerking
       , p_gewijzigd             => l_gewijzigd
       , p_succes                => l_succes
       , p_foutmelding           => l_foutmelding
       );

     gba:=null;
     --xsw
     l_land := null;
     l_huisletter := null;
     l_huisnumtoev := null;
     l_aand_huisnum := null;
     --

     if not l_succes
     then
       l_aantal_fout:= l_aantal_fout + 1;
       --
       if l_foutmelding like '%Geen relatie gevonden voor A/Sofi nummer%'
       then
         l_fout_1:= l_fout_1 + 1;
       elsif l_foutmelding like '%Geen relatie gevonden voor A nummer partner%'
       then
         l_fout_2:= l_fout_2 + 1;
       elsif l_foutmelding like '%Datum huwelijk wijkt af%'
       then
         l_fout_3:= l_fout_3 + 1;
       elsif l_foutmelding like '%Ongeldige geboortedatum%'
       then
         l_fout_4:= l_fout_4 + 1;
       elsif l_foutmelding like '%Een relatiekoppeling mag niet beginnen voor de geboortedatum van één van de twee personen%'
       then
         l_fout_5:= l_fout_5 + 1;
       elsif l_foutmelding like '%Woonplaats is leeg%'
       then
         l_fout_6:= l_fout_6 + 1;
       elsif l_foutmelding like '%De periode van dit huwelijk, geregistreerd partnerschap of samenlevingscontract overlapt met een andere relatiekoppeling voor (minimaal) één van deze twee personen%'
       then
         l_fout_7:= l_fout_7 + 1;
       elsif l_foutmelding like '%RLE-00407 Geboortedatum na begindatum voor%'
       then
         l_fout_8:= l_fout_8 + 1;
       elsif l_foutmelding like '%Voorvoegsel%'
       then
         l_fout_9:= l_fout_9 + 1;
       elsif l_foutmelding like '%wijkt af van sofinummer in RLE%'
       then
         l_fout_11:= l_fout_11 + 1;
       elsif l_foutmelding like '%wijkt af van geboortedatum in RLE%'
       then
         l_fout_12:= l_fout_12 + 1;
       elsif l_foutmelding like '%wijkt af van geslachtsaanduiding in RLE%'
       then
         l_fout_13:= l_fout_13 + 1;
       elsif l_foutmelding like 'Geen relatiekoppeling gevonden%'
       then
         l_fout_14:= l_fout_14 + 1;
       elsif l_foutmelding like 'Gewenste afnemersindicatie voor partrner%'
       then
         l_fout_15:= l_fout_15 + 1;
       else
         l_fout_10:= l_fout_10 + 1;
       end if;
     end if;

     if l_gewijzigd
     then
       l_gemuteerd:= l_gemuteerd + 1;
     end if;

     if l_gewijzigd or
        not l_succes
     then
       -- personen gemuteerd of op uitvalverslag
       null;
     else
       -- personen zonder verschil
       l_aantal_gelijk:= l_aantal_gelijk + 1;
     end if;

   end if;

   if l_aangeboden > 1000 -- Niet te hoog zetten anders blijft deelnemer in RLE onnodig lang gelocked.
   then
     commit;
   end if;

   stm_util.debug('**** Bewaar rubriek '||r_gba.rubrieknummer||' voor A/Sofi nummer '||nvl(r_gba.anummer,r_gba.bsn)||' in record GBA ****');
   gba.a_nummer:= r_gba.anummer;
   gba.bsn:= r_gba.bsn;
   if r_gba.rubrieknummer = '010120'    -- BSN
   then
     gba.bsn_gba:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '010210' -- Voornamen
   then
     gba.voornamen:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '010230' -- Voorvoegsel
   then
     gba.voorvoegsel:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '010240' -- Geslachtsnaam
   then
     gba.geslachtsnaam:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '010310' -- Geboortedatum
   then
     gba.geboortedatum:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '010410' -- Geslachtsaanduiding
   then
     gba.geslachts_aanduiding := r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '016110' -- Aanduiding naamgebruik
   then
     gba.aanduiding_naamgebr:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '040510' -- Nationaliteit
   then
     gba.nationaliteit:= r_gba.ebrpwaarde;
   --
   -- xsw: partner rubrieken verwerken
   --
   elsif r_gba.rubrieknummer = '050110' -- A nummer partner
   then
     gba.a_nummer_prtner:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '050120' -- BSN partner
   then
     gba.bsn_prtner:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '050210' -- Voornamen partner
   then
     gba.voornamen_prtner:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '050230' -- Voorvoegsel partner
   then
     gba.voorvoegsel_prtner := r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '050240' -- Geslachtsnaam partner
   then
     gba.geslachtsnaam_prtner:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '050310' -- Geboortedatum partner
   then
     gba.geboortedatum_prtner:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '050610' -- Datum sluiting
   then
     gba.datum_sluiting:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '050710' -- Datum ontbinding
   then
     gba.datum_ontbinding := r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '060810' -- Datum overlijden
   then
     gba.datum_overlijden:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '081110' -- Straatnaam
   then
     gba.straatnaam:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '081120' -- Huisnummer
   then
     gba.huisnummer:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '081130' -- Huisletter
   then
     if r_gba.ebrpwaarde <>'null'
     then
       l_huisletter := r_gba.ebrpwaarde;
     end if;
   elsif r_gba.rubrieknummer = '081140' -- Huisnummertoevoeging
   then
     if r_gba.ebrpwaarde <>'null'
     then
       l_huisnumtoev := r_gba.ebrpwaarde;
     end if;
   elsif r_gba.rubrieknummer = '081150' -- Aanduiding bij huisnummer
   then
     if r_gba.ebrpwaarde <>'null'
     then
       l_aand_huisnum := r_gba.ebrpwaarde;
     end if;
   elsif r_gba.rubrieknummer = '081160' -- Postcode
   then
     gba.postcode:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '081170' -- Woonplaatsnaam
   then
     gba.woonplaatsnaam:= r_gba.ebrpwaarde;
   elsif r_gba.rubrieknummer = '081310' -- Land waarnaar vertrokken
   then
     -- xsw transformatie land
     syn_transformatie.land_code ( p_land_code_new => l_land
                                 , p_land_code_old => r_gba.ebrpwaarde
                                 );
     if l_land = 'NL'
     then
       gba.land_naar_vertrokken:= null;
     else
       gba.land_naar_vertrokken:= l_land;
     end if;
   elsif r_gba.rubrieknummer = '081320' -- Datum vetrek uit Nederland
   then
     gba.datum_vertrek_nl:= r_gba.ebrpwaarde;
   end if;

   -- Samenvoegen Huisletter en Huisnummertoevoeging
   if  l_huisletter   is not null
   or  l_huisnumtoev  is not null
   then
     if length (ltrim( rtrim ( replace( l_huisletter||' '||l_huisnumtoev, '  ',' '))))> 5
     then
       gba.huisnummer_toevoeging:= substr (ltrim( rtrim ( replace( l_huisletter||l_huisnumtoev, '  ',' '))),1,5);
     else
       gba.huisnummer_toevoeging:= ltrim( rtrim ( replace( l_huisletter||' '||l_huisnumtoev, '  ',' ')));
     end if;
   else
     gba.huisnummer_toevoeging := 'null';
   end if;

   l_laatste_nr:= nvl(r_gba.anummer,r_gba.bsn);

  end loop;

  -- nogmaals voor de laatste set
  stm_util.debug('**** Verwerk rubriek set voor A/Sofi nummer '||nvl(gba.a_nummer,gba.bsn)||' ****');
  l_aangeboden:= l_aangeboden + 1;

   verwerk_rubriekset
     ( p_rec_gba               => gba
     , p_controleer_verwerking => p_controleer_verwerking
     , p_type_verwerking       => p_type_verwerking
     , p_gewijzigd             => l_gewijzigd
     , p_succes                => l_succes
     , p_foutmelding           => l_foutmelding
     );

  if not l_succes
  then
    l_aantal_fout:= l_aantal_fout + 1;
    --
    if l_foutmelding like '%Geen relatie gevonden voor A/Sofi nummer%'
    then
      l_fout_1:= l_fout_1 + 1;
    elsif l_foutmelding like '%Geen relatie gevonden voor A nummer partner%'
    then
      l_fout_2:= l_fout_2 + 1;
    elsif l_foutmelding like '%Datum huwelijk wijkt af%'
    then
      l_fout_3:= l_fout_3 + 1;
    elsif l_foutmelding like '%Ongeldige geboortedatum%'
    then
      l_fout_4:= l_fout_4 + 1;
    elsif l_foutmelding like '%Een relatiekoppeling mag niet beginnen voor de geboortedatum van één van de twee personen%'
    then
      l_fout_5:= l_fout_5 + 1;
    elsif l_foutmelding like '%Woonplaats is leeg%'
    then
      l_fout_6:= l_fout_6 + 1;
    elsif l_foutmelding like '%De periode van dit huwelijk, geregistreerd partnerschap of samenlevingscontract overlapt met een andere relatiekoppeling voor (minimaal) één van deze twee personen%'
    then
      l_fout_7:= l_fout_7 + 1;
    elsif l_foutmelding like '%RLE-00407 Geboortedatum na begindatum voor%'
    then
      l_fout_8:= l_fout_8 + 1;
    elsif l_foutmelding like '%Voorvoegsel%'
    then
      l_fout_9:= l_fout_9 + 1;
    elsif l_foutmelding like '%wijkt af van sofinummer in RLE%'
    then
      l_fout_11:= l_fout_11 + 1;
    elsif l_foutmelding like '%wijkt af van geboortedatum in RLE%'
    then
      l_fout_12:= l_fout_12 + 1;
    elsif l_foutmelding like '%wijkt af van geslachtsaanduiding in RLE%'
    then
     l_fout_13:= l_fout_13 + 1;
    elsif l_foutmelding like 'Geen relatiekoppeling gevonden%'
    then
      l_fout_14:= l_fout_14 + 1;
    elsif l_foutmelding like 'Gewenste afnemersindicatie voor partrner%'
    then
      l_fout_15:= l_fout_15 + 1;
    else
      l_fout_10:= l_fout_10 + 1;
    end if;
  end if;

  if l_gewijzigd
  then
    l_gemuteerd:= l_gemuteerd + 1;
  end if;

  if l_gewijzigd or
     not l_succes
  then
    -- personen gemuteerd of op uitvalverslag
    null;
  else
    -- personen zonder verschil
    l_aantal_gelijk:= l_aantal_gelijk + 1;
  end if;

  lijst ( '--------------------------------------------------------------------------------------------------------' );
  lijst ('Invoerparameter:');
  lijst ('=======================================');
  lijst('P_TYPE_VERWERKING = '||P_TYPE_VERWERKING );
  lijst('');
  lijst ('=======================================');
  lijst('');
  lijst ('Invoerbestand: afwijkingen.csv ');
  lijst ('=======================================');
  lijst('');
  lijst('Aantal personen aangeboden                   : '||l_aangeboden);
  lijst('Aantal records aangeboden                    : '||l_rec_aangeb);
  lijst('Aantal personen zonder verschil (GBA => RLE) : '||l_aantal_gelijk);
  lijst('Aantal personen op uitvalverslag             : '||l_aantal_fout);
  lijst('');
  lijst('waarvan: ');

  if l_fout_1 > 0
  then
    lijst (lpad(l_fout_1,7)||' gevallen waarvoor geen relatie gevonden voor A/Sofi nummer.');
  end if;
  if l_fout_2 > 0
  then
    lijst (lpad(l_fout_2,7)||' gevallen waarvoor geen relatie gevonden voor A nummer partner.');
  end if;
  if l_fout_3 > 0
  then
    lijst (lpad(l_fout_3,7)||' gevallen waarvoor datum huwelijk afwijkt (deze dienen handmatig te worden aangepast).');
  end if;
  if l_fout_4 > 0
  then
    lijst (lpad(l_fout_4,7)||' gevallen met een ongeldige geboortedatum (bijv. 1967000).');
  end if;
  if l_fout_5 > 0
  then
    lijst (lpad(l_fout_5,7)||' gevallen waarvoor de relatiekoppeling begint voor de geboortedatum van één van de twee personen.');
  end if;
  if l_fout_6 > 0
  then
    lijst (lpad(l_fout_6,7)||' gevallen waarvoor de woonplaats leeg is (niet bepaald kan worden op basis van postcode/huisnummer).');
  end if;
  if l_fout_7 > 0
  then
    lijst (lpad(l_fout_7,7)||' gevallen waarvoor de periode van het huwelijk overlapt met een andere relatiekoppeling voor één van deze twee personen.');
  end if;
  if l_fout_8 > 0
  then
    lijst (lpad(l_fout_8,7)||' gevallen waarvoor de begindatum voor de geboortedatum ligt.');
  end if;
  if l_fout_9 > 0
  then
    lijst (lpad(l_fout_9,7)||' gevallen waarvoor het voorvoegsel niet voorkomt.');
  end if;
  if l_fout_11 > 0
  then
    lijst (lpad(l_fout_11,7)||' gevallen waarvan het sofinummer afwijkt van het sofinummer in RLE.');
  end if;
  if l_fout_12 > 0
  then
    lijst (lpad(l_fout_12,7)||' gevallen waarvan de geboortedatum afwijkt van de geboortedatum in RLE.');
  end if;
  if l_fout_13 > 0
  then
    lijst (lpad(l_fout_13,7)||' gevallen waarvan de geslachtsaanduiding afwijkt van de geslachtsaanduiding in RLE.');
  end if;
  if l_fout_14 > 0
  then
    lijst (lpad(l_fout_14,7)||' gevallen waarvan geen relatiekoppeling gevonden in RLE.');
  end if;
  if l_fout_15 > 0
  then
    lijst (lpad(l_fout_15,7)||' gevallen waarvan de gewenste afnemersindicatie voor de partner J is.');
  end if;
  if l_fout_10 > 0
  then
    lijst (lpad(l_fout_10,7)||' overige uitval.');
  end if;

  lijst('');
  lijst('==========================================');
  lijst('Personen gemuteerd                    : '||l_gemuteerd);
  lijst('');

  if p_controleer_verwerking = 'J'
  then
    stm_util.debug('**** Controleer de verwerking ****');
    --
    g_aantal_gemuteerd := l_gemuteerd;
    --
    controleer_verwerking (p_type_verwerking => p_type_verwerking);
  end if;
  --  Voettekst van lijsten
  lijst ( '********** EINDE VERSLAG Verwerken synchronisatie bestand GBA => RLE **********'|| ' EINDDATUM: ' || to_char (sysdate,'dd-mm-yyyy hh24:mi:ss'));
  stm_util.insert_job_statussen(g_volgnummer, 'S', '0');
exception
  when others then
    stm_util.t_foutmelding := sqlerrm;
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(g_volgnummer, 'S', '1');
    stm_util.insert_job_statussen(g_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                  ' procedure : ' || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null then
      stm_util.insert_job_statussen(g_volgnummer,
                                    'I',
                                    stm_util.t_foutmelding);
    end if;
    --
    if stm_util.t_tekst is not null then
      stm_util.insert_job_statussen(g_volgnummer, 'I', stm_util.t_tekst);
    end if;
end verwerk;
END RLE_VERWERK_SYN_GBA;
/