CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_DUBBEL IS
/* **********************************************************
   PACKAGE:    rle_dubbel

   DOEL:       Controle op dubbele relaties

   Wie    Wanneer         Wat
   ------ ---------- ----------------------------------------
   XCW    11-02-2010 Header toegevoegd
********************************************************** */


/* Bepalen aanwezigeheid */
PROCEDURE RELATIE_AANWEZIG
 (P_NAAM IN varchar2
 ,P_DUBBEL_GEVONDEN OUT VARCHAR2
 ,P_GEVONDEN_RELATIES IN OUT RLE_DUBBEL.TAB_DUBBEL_RLE
 ,P_POSTCODE IN VARCHAR2 := null
 ,P_HUISNUMMER IN NUMBER := null
 ,P_GEBOORTEDATUM IN DATE := null
 ,P_VOORLETTERS IN varchar2 := null
 )
 IS
/* 10-06-2015 Ook personen zonder adres moeten gevonden kunnen worden als mogelijke dubbele , daarom left outer joins in de cursors toegevoegd*/
cursor c_rle01(b_naam varchar2,b_instelling varchar2,b_firstchars number)
is
select r.numrelatie
,      r.zoeknaam
,      r.datgeboorte
,      r.voorletter
,      a.postcode
,      a.huisnummer
,      ra.dwe_wrddom_srtadr
,      a.numadres
,      0   score
from   rle_relaties r
left outer join   rle_relatie_adressen ra
on     r.numrelatie = ra.rle_numrelatie
left outer join   rle_adressen a
on     ra.ads_numadres = a.numadres
where  r.zoeknaam like upper(substr(convert(upper(replace(replace(replace(replace(b_naam,' & ',' EN ')
                                                                        ,' &',' EN ')
                                                                ,'& ',' EN ')
                                                        ,'&',' EN ')
                                                )
                          ,'US7ASCII'),1,b_firstchars)||'%')
and   r.indinstelling = b_instelling
and   nvl(ra.datingang,sysdate) <= sysdate
and   nvl(ra.dateinde,sysdate) >= sysdate
and   (    (ra.dwe_wrddom_srtadr in ('FA','SZ') and b_instelling = 'J')
        or ((ra.dwe_wrddom_srtadr in ('DA','OA') or ra.dwe_wrddom_srtadr is null) and b_instelling = 'N')
      );
--
/* Telling voor controle dubbele personen */
cursor c_rle02 (b_naam varchar2,b_instelling varchar2,b_firstchars number)
is
select  count(r.numrelatie) aantal_relaties
    from   rle_relaties r
    left outer join   rle_relatie_adressen ra
    on     r.numrelatie = ra.rle_numrelatie
    left outer join   rle_adressen a
    on     ra.ads_numadres = a.numadres
    where  r.zoeknaam like upper(substr(convert(upper(replace(replace(replace(replace(b_naam,' & ',' EN ')
                                                                            ,' &',' EN ')
                                                                    ,'& ',' EN ')
                                                            ,'&',' EN ')
                                                    )
                              ,'US7ASCII'),1,b_firstchars)||'%')
    and   r.indinstelling = b_instelling
    and   nvl(ra.datingang,sysdate) <= sysdate
    and   nvl(ra.dateinde,sysdate) >= sysdate
    and   (    (ra.dwe_wrddom_srtadr in ('FA','SZ') and b_instelling = 'J')
            or ((ra.dwe_wrddom_srtadr in ('DA','OA') or ra.dwe_wrddom_srtadr is null) and b_instelling = 'N')
           )
    and   rownum < 102;
--
cursor c_rle03(b_naam varchar2, b_voorletter varchar2, b_instelling varchar2, b_firstchars number)
is
select r.numrelatie
,      r.zoeknaam
,      r.datgeboorte
,      r.voorletter
,      a.postcode
,      a.huisnummer
,      ra.dwe_wrddom_srtadr
,      a.numadres
,      0   score
from   rle_relaties r
left outer join   rle_relatie_adressen ra
on     r.numrelatie = ra.rle_numrelatie
left outer join   rle_adressen a
on     ra.ads_numadres = a.numadres
where  r.zoeknaam like upper(substr(convert(upper(replace(replace(replace(replace(b_naam,' & ',' EN ')
                                                                        ,' &',' EN ')
                                                                ,'& ',' EN ')
                                                        ,'&',' EN ')
                                                )
                          ,'US7ASCII'),1,b_firstchars)||'%')
and   r.voorletter    = b_voorletter
and   r.indinstelling = b_instelling
and   nvl(ra.datingang,sysdate) <= sysdate
and   nvl(ra.dateinde,sysdate) >= sysdate
and   (    (ra.dwe_wrddom_srtadr in ('FA','SZ') and b_instelling = 'J')
        or ((ra.dwe_wrddom_srtadr in ('DA','OA') or ra.dwe_wrddom_srtadr is null) and b_instelling = 'N')
      );
--
r_rle01       rec_dubbel_rle; --c_rle01%rowtype;
r_hulp        rec_dubbel_rle;
r_rle01_vorig rec_dubbel_rle;
/* Ophalen wegingsfaktoren voor bepalen dubbelen */
CURSOR C_DFR_001
 (B_PROCES VARCHAR2
-- ,B_RUBRIEK VARCHAR2
 )
 IS
select  PROCES
,       RUBRIEK
,       FAKTOR_LENGTE
,       FAKTOR_INHOUD
,       FAKTOR_VOLGORDE
,       FAKTOR_LEEG
,       FAKTOR_ZWAARTE
from rle_dubbele_faktoren
where proces = upper(b_proces)
;
r_dfr_001 c_dfr_001%rowtype;
type tab_factoren is table of c_dfr_001%rowtype index by binary_integer;
t_matching_factoren tab_factoren;
-- Program Data
G_IDX_DUBBELEN BINARY_INTEGER;
--G_TAB_DUBBELEN RLE_PCK_DUBBELEN_TAB;
G_AANT_ZEKEREN NUMBER(7);
--G_TAB_LEEG RLE_PCK_DUBBELEN_TAB;
l_score_max number;
l_score_nam number;
l_score_geboortedatum number;
l_score_huisnummer number;
l_score_postcode number;
l_score_voorletters number;
l_zwaarte_nam number;
l_zwaarte_geboortedatum number;
l_zwaarte_huisnummer number;
l_zwaarte_postcode number;
l_zwaarte_voorletters number;
l_score_totaal number;
l_grenswaarde_score number := stm_algm_get.sysparm('RLE','PERCDUP',sysdate);
i number;
l_aantal_dubbele number := 0;
procedure vul_factorentabel(p_proces in varchar2)
is
 l_teller binary_integer := 1;
begin
 for r_dfr_001 in c_dfr_001(p_proces)
 loop
    t_matching_factoren(l_teller) := r_dfr_001;
    l_teller := l_teller + 1;
 end loop;
end;
function matching_factoren(p_rubriek varchar2) return c_dfr_001%rowtype
is
begin
  for i in 1 .. t_matching_factoren.last
  loop
     if t_matching_factoren.exists(i)
     then
        if t_matching_factoren(i).rubriek = upper(p_rubriek)
	then
	   return t_matching_factoren(i);
	end if;
     end if;
  end loop;
  raise_application_error(-20000,'Kan matching factoren niet vinden');
end;
FUNCTION SCORE
 (P_VAR2 IN VARCHAR2
 ,P_VAR1 IN VARCHAR2
 ,P_FAKTOR_LENGTE NUMBER
 ,P_FAKTOR_INHOUD NUMBER
 ,P_FAKTOR_VOLGORDE NUMBER
 ,P_FAKTOR_LEEG NUMBER
 ,P_FAKTOR_ZWAARTE NUMBER
 )
 RETURN NUMBER
 IS
-- Datastructure Definitions
TYPE LETTERS_TAB IS TABLE OF NUMBER(3) INDEX BY BINARY_INTEGER;
-- Program Data
L_INX_SHORT NUMBER(3);
L_INX_LONG NUMBER;
L_POS NUMBER(3);
L_TAB_INX NUMBER(3);
L_LETTERS_LONG_TAB LETTERS_TAB;
L_LETTERS_SHORT_TAB LETTERS_TAB;
L_SHORT_SIZE NUMBER(3);
L_SHORT_VAR VARCHAR2(40);
L_SCORE NUMBER(10, 2);
L_LONG_VAR VARCHAR2(40);
L_SCORE_LENGTE NUMBER(10, 2);
L_LONG_SIZE NUMBER(3);
L_SCORE_INHOUD NUMBER(10, 2);
L_SCORE_VOLGORDE NUMBER(10, 2);
l_var2 varchar2(200) := p_var2;
-- PL/SQL Block
BEGIN
/* RLE_PCK_AANWEZIG_SCR */
    /* score indien het veld leeg is bepalen */
    stm_util.debug('vergelijk '||p_var1||' met '||p_var2);
    if p_var1 is null or
       p_var2 is null then
       if p_faktor_leeg is null then
          return 0;
       else
          return p_faktor_leeg * p_faktor_zwaarte;
       end if;
    end if;
    /* score voor lengte bepalen */
    if length(p_var1) > length(p_var2) then
       l_long_size   := length(p_var1);
       l_long_var  := p_var1;
       l_short_size  := length(p_var2);
       l_short_var := p_var2;
    else
       l_long_size := length(p_var2);
       l_long_var  := p_var2;
       l_short_size := length(p_var1);
       l_short_var := p_var1;
    end if;
    l_score_lengte := p_faktor_lengte -
    ((l_long_size - l_short_size) * (p_faktor_lengte / l_long_size));
    stm_util.debug('score lengte: '||l_score_lengte);
    /* score voor inhoud bepalen */
    l_pos := 0;
    /* kijk voor ieder character uit p_var1 of deze ook in p_var2 voorkomt,
       maar tel ze niet dubbel. */
    for i in 1 .. length(p_var1)
    loop
       if nvl(instr(l_var2,substr(p_var1,i,1)),0) > 0
       then
          l_pos := l_pos + 1;
	  l_var2 := substr(l_var2,1,instr(l_var2,substr(p_var1,i,1))-1)
	          ||substr(l_var2,instr(l_var2,substr(p_var1,i,1))+1,length(l_var2));
       end if;
   end loop;
   l_score_inhoud := p_faktor_inhoud * (l_pos/greatest(length(p_var1),length(p_var2)));
   stm_util.debug('score inhoud: '||l_score_inhoud);
    /* score voor volgorde bepalen */
	l_pos := 0;
	for i in 1 .. length(p_var1)
	loop
	   if substr(p_var1,i,1) = substr(p_var2,i,1)
	   then
	      l_pos := l_pos + 1;
	   end if;
	end loop;
	l_score_volgorde := p_faktor_volgorde * l_pos/length(p_var1);
        stm_util.debug('score volgorde: '||l_score_volgorde);
    /* zwaarte voor dit specifieke attribuut laten meewegen */
    return (l_score_inhoud + l_score_lengte + l_score_volgorde) *
            p_faktor_zwaarte;
END; /* score */
procedure rubriekscore( p_proces  in varchar2
                      , p_rubriek in varchar2
                      , p_waarde1 in varchar2
                      , p_waarde2 in varchar2
                      , p_score   out number
                      , p_zwaarte out number)
is
 l_score number := 0;
 l_score_max number := 0;
begin
   stm_util.debug('bepaal score voor '||p_rubriek||' van '||p_proces);
--   open  c_dfr_001(p_proces,p_rubriek);
--   fetch c_dfr_001 into r_dfr_001;
   r_dfr_001 := matching_factoren(p_rubriek);
   stm_util.debug('factoren: '||r_dfr_001.faktor_lengte||' '||r_dfr_001.faktor_inhoud||' '||r_dfr_001.faktor_volgorde);
   if p_waarde1 is not null
   or r_dfr_001.faktor_leeg is not null
   then
      l_score_max := l_score_max +
                  ((r_dfr_001.faktor_lengte +
                    r_dfr_001.faktor_inhoud +
                    r_dfr_001.faktor_volgorde) * r_dfr_001.faktor_zwaarte);
      l_score := score(p_waarde1
                      ,p_waarde2
                      ,r_dfr_001.faktor_lengte
                      ,r_dfr_001.faktor_inhoud
                      ,r_dfr_001.faktor_volgorde
                      ,r_dfr_001.faktor_leeg
                      ,r_dfr_001.faktor_zwaarte
                      );
      stm_util.debug('score '||p_rubriek||':  '||l_score);
   end if;
--   close c_dfr_001;
   p_score :=  l_score;
   p_zwaarte := r_dfr_001.faktor_zwaarte;
end;
/*  hoofdprocedure  */
begin
 stm_util.debug('RLE_DUBBEL naam: '||p_naam);
 p_dubbel_gevonden := 'N';
 r_rle01_vorig.numrelatie := 0;
 r_rle01_vorig.numadres := 0;
    if p_geboortedatum is null
    then
/* het betreft een werkgever */
       vul_factorentabel('WERKGEVER');
       open c_rle01(p_naam,'J',10); /* zoek alle instellingen waarvan de naam op de eerste tien posities
                                       overeenkomt */
       loop
          fetch c_rle01 into r_rle01;
          exit when c_rle01%notfound;
          stm_util.debug('gevonden: '||r_rle01.zoeknaam||' '||r_rle01.numrelatie||' '||r_rle01.postcode);
          /* **score naam** */
          rubriekscore('werkgever','Naam',r_rle01.zoeknaam,convert(upper(p_naam),'US7ASCII'),l_score_nam,l_zwaarte_nam);
          /* **score huisnummer** */
          rubriekscore('werkgever','Huisnummer',p_huisnummer,r_rle01.huisnummer                                             ,l_score_huisnummer,l_zwaarte_huisnummer);
          /* **score postcode** */
          rubriekscore('werkgever','Postcode',p_postcode,r_rle01.postcode
                      ,l_score_postcode,l_zwaarte_postcode);
          l_score_totaal := (l_score_nam+l_score_huisnummer+l_score_postcode)
                            /((l_zwaarte_nam+l_zwaarte_huisnummer+l_zwaarte_postcode)*100);

          if l_score_totaal >= l_grenswaarde_score
          then
             p_dubbel_gevonden := 'J';
             i := nvl(p_gevonden_relaties.last,0)+1;
             p_gevonden_relaties(i) := r_rle01;
             p_gevonden_relaties(i).score := l_score_totaal;
          end if;
       end loop;
       close c_rle01;
    else
/* het betreft een persoon */
      vul_factorentabel('PERSOON');
      --
      open  c_rle02(p_naam,'N',7);
      fetch c_rle02 into l_aantal_dubbele;
      close c_rle02;
      --
      if l_aantal_dubbele <= 100
        then
          --
          open c_rle01(p_naam,'N',7); /*zoek alle natuurlijke personen waarvan de naam op de eerste 7 posities
                                     overeenkomt */
          loop
            fetch c_rle01 into r_rle01;
            exit when c_rle01%notfound;
            /* **score naam** */
            rubriekscore('persoon','Naam',r_rle01.zoeknaam,convert(upper(p_naam),'US7ASCII'),l_score_nam,l_zwaarte_nam);
            /*  AVR - 10-01-2003 geen controle op huisnummer en postcode. */
            /* **score geboortedatum** */
            rubriekscore('persoon','Geboortedatum'
                        ,to_char(p_geboortedatum,'ddmmyy')
                        ,to_char(r_rle01.datgeboorte,'ddmmyy')
                       ,l_score_geboortedatum,l_zwaarte_geboortedatum);
            /* **score voorletters** */
            rubriekscore('persoon','Voorletters',p_voorletters,r_rle01.voorletter
                        ,l_score_voorletters,l_zwaarte_voorletters);
            l_score_totaal := (l_score_nam+l_score_geboortedatum+l_score_voorletters)
                              /((l_zwaarte_nam+l_zwaarte_geboortedatum+l_zwaarte_voorletters )*100);
            if (l_score_totaal >= l_grenswaarde_score
                and r_rle01_vorig.numrelatie <> r_rle01.numrelatie)
            or  (l_score_totaal >= l_grenswaarde_score
                 and r_rle01_vorig.numrelatie = r_rle01.numrelatie
                 and  (nvl(r_rle01_vorig.numadres,0) <> nvl(r_rle01.numadres,0)))
                 /* 09-06-2015 personen zonder adres kunnen ook gevonden worden, maar lege numadressen kunnen niet vergeleken worden, daarom nvl*/
            then
               p_dubbel_gevonden := 'J';
               i := nvl(p_gevonden_relaties.last,0)+1;
               p_gevonden_relaties(i) := r_rle01;
               p_gevonden_relaties(i).score := l_score_totaal;
            end if;
            r_rle01_vorig := r_rle01;
          end loop;
          close c_rle01;
      else
         open c_rle03(p_naam, p_voorletters, 'N',7); /*zoek alle natuurlijke personen waarvan de naam op de eerste 7 posities
                                     overeenkomt */
         loop
           fetch c_rle03 into r_rle01;
           exit when c_rle03%notfound;
           /* **score naam** */
           rubriekscore('persoon','Naam',r_rle01.zoeknaam,convert(upper(p_naam),'US7ASCII'),l_score_nam,l_zwaarte_nam);
           /*  AVR - 10-01-2003 geen controle op huisnummer en postcode. */
           /* **score geboortedatum** */
           rubriekscore('persoon','Geboortedatum'
                       ,to_char(p_geboortedatum,'ddmmyy')
                       ,to_char(r_rle01.datgeboorte,'ddmmyy')
                       ,l_score_geboortedatum,l_zwaarte_geboortedatum);
           /* **score voorletters** */
           rubriekscore('persoon','Voorletters',p_voorletters,r_rle01.voorletter
                       ,l_score_voorletters,l_zwaarte_voorletters);
           l_score_totaal := (l_score_nam+l_score_geboortedatum+l_score_voorletters)
                             /((l_zwaarte_nam+l_zwaarte_geboortedatum+l_zwaarte_voorletters )*100);
            if (l_score_totaal >= l_grenswaarde_score
                and r_rle01_vorig.numrelatie <> r_rle01.numrelatie)
            or  (l_score_totaal >= l_grenswaarde_score
                 and r_rle01_vorig.numrelatie = r_rle01.numrelatie
                 and  (nvl(r_rle01_vorig.numadres,0) <> nvl(r_rle01.numadres,0)))
                 /* 09-06-2015 personen zonder adres kunnen ook gevonden worden, maar lege numadressen kunnen niet vergeleken worden, daarom nvl*/
            then
              p_dubbel_gevonden := 'J';
              i := nvl(p_gevonden_relaties.last,0)+1;
              p_gevonden_relaties(i) := r_rle01;
              p_gevonden_relaties(i).score := l_score_totaal;
           end if;
           r_rle01_vorig := r_rle01;
         end loop;
         close c_rle03;
      end if;
    end if;
    /* sorteer gevonden records op score */
    if p_gevonden_relaties.last is not null then
    for i in 1 .. p_gevonden_relaties.last
    loop
       for j in i+1 .. p_gevonden_relaties.last
       loop
          if p_gevonden_relaties(j).score > p_gevonden_relaties(i).score
          then
             r_hulp := p_gevonden_relaties(j);
             p_gevonden_relaties(j) := p_gevonden_relaties(i);
             p_gevonden_relaties(i) := r_hulp;
          end if;
       end loop;
    end loop;
    end if;
end;

END RLE_DUBBEL;
/