CREATE OR REPLACE PACKAGE BODY comp_rle.RLE_SCR_RELATIES IS

-----------------------------------------------------------------------------------------------------------------------------------------
  -- Wijzigingshistorie
  -- Wanneer     Wie               Wat
  -- ---------- ----------------- -------------------------------------------------------------------------------------------------------
  -- 7-07-2011   Rudie Siwpersad   creatie: screenen van relaties in het kader van WWFT
  -- 3-08-2016   PLW               Aanpassen nav PE-673: SDM 12305351 batchjob Rleprc54 plaatst onjuist OA-adres wanneer leeg op relatie.
  --                               verder omgebruikt variabelen en parameters worden opgeruimd.
-----------------------------------------------------------------------------------------------------------------------------------------
   -- global t.b.v. logverslag en herstart
   g_lijstnummer                 number        := 1;
   g_regelnr                     number        :=0;
   g_herstart_sat_id             number;
   g_herstart_numrelatie         number;
   g_tel_scr1                    number := 0;
   g_tel_scr2                    number := 0;

   g_scheiding                   varchar2 (10) := ' : ';
   g_descript_lengte             pls_integer   := 70;
   g_volgnummer                  stm_job_statussen.volgnummer%type := 0;
   g_methode_onderzoek           varchar2 (50);
   g_foutmelding                 varchar2(200);
 -- globals t.b.v. rapportage wwft-verdachten
   g_bestand                     utl_file.file_type;

PROCEDURE LIJST
 (P_TEXT IN varchar2
 );


PROCEDURE LIJST
 (P_TEXT IN varchar2
 )
 IS
begin
  stm_util.insert_lijsten( g_lijstnummer
                         , g_regelnr
                         , p_text
                         );
end lijst;
FUNCTION CONTROLE_ONVERW_BESTANDEN
 RETURN BOOLEAN
 IS

  cursor c_controle
  is
   select count (*) aantal
      from evt_file_definitions fdn,
                evt_event_log elg
   where fdn.filename_pattern like '%WWFT%'
       and fdn.event_code = elg.event_code
       and elg.event_creation > trunc (sysdate) - 1
       and not exists
              (select 'x'
                  from evt_event_log_event elt
                where elt.elg_id = elg.id and elt.status = 'PROCESSED');
  --
  l_return_value    boolean := false;
  r_controle        c_controle%rowtype;
begin
  -- Openen cursor te controleren of er niet verwerkte bestanden zijn
  open c_controle;
  fetch c_controle into r_controle;
  if c_controle%found and
     r_controle.aantal > 0
  then
    -- Indien niet verwerkte events gevonden geef 'true'terug
    l_return_value := true;
    -- Logbestand bijwerken
    lijst ('Batchjob gestopt, '||r_controle.aantal||' niet verwerkte events gevonden!');
    g_foutmelding:= 'Batchjob gestopt, '||r_controle.aantal||' niet verwerkte events gevonden!';
    --
  else
    l_return_value := false;
  end if;

  -- Cursor sluiten
  close c_controle;

  --
  return l_return_value;
  --

end controle_onverw_bestanden;
PROCEDURE WEGSCHRIJVEN_NAAR_TABEL
 (P_SIG_DATUM IN date
 ,P_SAT_ID IN number
 ,P_NUMRELATIE IN number
 ,P_NUMADRES IN number
 ,P_MATCH_VOORLETTER IN varchar2
 ,P_MATCH_NAAM IN varchar2
 ,P_MATCH_GEBOORTEJAAR IN varchar2
 ,P_MATCH_STRAATNAAM IN varchar2
 ,P_MATCH_HUISNUMMER IN varchar2
 ,P_MATCH_POSTCODE IN varchar2
 ,P_MATCH_WOONPLAATS IN varchar2
 )
 IS
begin
  --
  stm_util.debug ('Bezig met het inserten in wwft-tabel');
  --
  insert into rle_wwft_verdachten ( dat_signalering
                                  , sat_id
                                  , rle_numrelatie
                                  , ads_numadres
                                  , overeenkomst_voorletter
                                  , overeenkomst_naam
                                  , overeenkomst_geboortejaar
                                  , overeenkomst_straatnaam
                                  , overeenkomst_huisnummer
                                  , overeenkomst_postcode
                                  , overeenkomst_woonplaats
                                  )
  values                          ( p_sig_datum
                                  , p_sat_id
                                  , p_numrelatie
                                  , p_numadres
                                  , p_match_voorletter
                                  , p_match_naam
                                  , p_match_geboortejaar
                                  , p_match_straatnaam
                                  , p_match_huisnummer
                                  , p_match_postcode
                                  , p_match_woonplaats
                                  );
exception
  when dup_val_on_index
  then
    update rle_wwft_verdachten
    set dat_signalering           = p_sig_datum
      , overeenkomst_voorletter   = p_match_voorletter
      , overeenkomst_naam         = p_match_naam
      , overeenkomst_geboortejaar = p_match_geboortejaar
      , overeenkomst_straatnaam   = p_match_straatnaam
      , overeenkomst_huisnummer   = p_match_huisnummer
      , overeenkomst_postcode     = p_match_postcode
      , overeenkomst_woonplaats   = p_match_woonplaats
   where sat_id         = p_sat_id
   and   rle_numrelatie = p_numrelatie
   and   ads_numadres   = p_numadres
   ;

end wegschrijven_naar_tabel;
FUNCTION OPHALEN_REGELING
 (P_NUMRELATIE IN NUMBER
 ,P_SOORT IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
  cursor c_rec (b_numrelatie number)
  is
    select distinct (rgg_code)
    from   ret_deelnemerschappen
    where  rle_numrelatie = b_numrelatie
    ;

  cursor c_wgr (b_numrelatie number)
  is
    select distinct (rgg_code)
    from   ovt_wgr_overeenkomsten
    where  rle_numrelatie = b_numrelatie
    ;
  -- variabelen
  l_rgg_code   varchar2 (200);
  r_rec        c_rec%rowtype;
begin
  stm_util.debug ('Bezig met het ophalen van regeling t.b.v. rapportage');
  --
  if p_soort = 'I'
  then
  for r_rec in c_rec (p_numrelatie)
    loop
      l_rgg_code:= l_rgg_code || r_rec.rgg_code ||', ';
    end loop;

  else
    for r_wgr in c_wgr (p_numrelatie)
    loop
      l_rgg_code:= l_rgg_code || r_rec.rgg_code ||', ';
    end loop;
  end if;

  --
  l_rgg_code := rtrim (l_rgg_code, ', ');
  return l_rgg_code;
  --
end ophalen_regeling;
PROCEDURE MAAK_OUTPUT_BESTAND
 (P_DATUM_VANAF IN date
 ,P_PEILDATUM IN date
 )
 IS

  -- Cursor c_rec gebruiken voor het ophalen van verdachten
  cursor c_rec (b_datum_vanaf date, b_peildatum date)
  is
    select dat_signalering
         , sat_id
         , rle_numrelatie
         , ads_numadres
         , overeenkomst_voorletter
         , overeenkomst_naam
         , overeenkomst_geboortejaar
         , overeenkomst_straatnaam
         , overeenkomst_huisnummer
         , overeenkomst_postcode
         , overeenkomst_woonplaats
         , dat_creatie
    from rle_wwft_verdachten
    where trunc(dat_signalering) >= trunc(nvl(b_datum_vanaf, b_peildatum))
    ;

  -- Cursor c_adr gebruiken voor het ophalen van adresgegevens o.b.v. gevonden relatienummer
  cursor c_adr ( b_rle_numrelatie number
               , b_ads_numadres   number
               )
  is
    select ras.adm_code
         , ras.dwe_wrddom_srtadr
         , ads.numadres
         , ads.huisnummer
         , ads.postcode
         , stt.straatnaam
         , wps.woonplaats
         , ras.rol_codrol
    from   rle_relatie_adressen ras
         , rle_adressen ads
         , rle_straten  stt
         , rle_woonplaatsen wps
    where  ras.rle_numrelatie = b_rle_numrelatie
    and    ras.ads_numadres   = nvl (b_ads_numadres, ras.ads_numadres)
    and    ads.numadres       = ras.ads_numadres
    and    stt.straatid       = ads.stt_straatid
    and    wps.woonplaatsid   = ads.wps_woonplaatsid
   ;

  -- Cursor c_san gebruiken voor het ophalen van NAW-gegevens uit sanctielijsten
  cursor c_san (b_id number)
  is
    select id
         , bron
         , bron_referentie
         , soort
         , status
         , voorletter
         , achternaam
         , volledigenaam
         , zoeknaam
         , datgeboorte
         , geboortejaar
         , straatnaam
         , huisnummer
         , postcode
         , woonplaats
         , dat_creatie
    from   rle_sanctielijsten
    where  id = b_id
    ;

 -- Cursor c_rle2 gebruiken voor het ophalen van relatiegegevens
  cursor c_rle ( b_numrelatie  number )
  is
    select rle.numrelatie
         , rle.voorletter
         , rle.namrelatie
         , rle.zoeknaam
         , rle.datgeboorte
         , (select extern_relatie
            from   rle_relatie_externe_codes rec
            where  rec.rle_numrelatie = b_numrelatie
            and    rec.dwe_wrddom_extsys = 'SOFI' ) sofinummer
         ,  ( case
                when datgeboorte is not null
                then
                  'I'
                else
                  'O'
              end
            ) soort
    from   rle_relaties rle
    where  rle.numrelatie = b_numrelatie

    ;

  -- Variabelen
  l_straatnaam_da       varchar2 (100);
  l_straatnaam_oa       varchar2 (100);
  l_huisnummer_da       number;
  l_huisnummer_oa       number;
  l_postcode_da         varchar2 (10);
  l_postcode_oa         varchar2 (10);
  l_woonplaats_da       varchar2 (100);
  l_woonplaats_oa       varchar2 (100);
  l_bestandsnaam        varchar2 (255);
  l_database            varchar2 (200);
  l_rgg_code            varchar2 (200);
  l_tel_aantal_rapport  number :=0;
  r_adr                 c_adr%rowtype;
  r_rec                 c_rec%rowtype;
  r_san                 c_san%rowtype;
  r_rle                 c_rle%rowtype;
begin
  -- Selectie omgevingnaam; indien productie deze weglaten
  stm_util.debug ('Bezig met het opstellen van rapportage wwft-verdachten');
  --
  select case
           when regexp_substr ( global_name
                              , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;

  l_bestandsnaam:= l_database  || 'WWFT_VERDACHTEN' || to_char (sysdate, 'ddmmyyyy')|| '.CSV';

  g_bestand:= utl_file.fopen ( 'RLE_OUTBOX'
                             , l_bestandsnaam
                             , 'W');

  stm_util.debug ('Naam rapportage: '|| l_bestandsnaam);
  -- Schrijf de header record
  utl_file.put_line ( g_bestand, 'SIGNALERINGSDATUM;BRON;BRON_REF;SOORT;REGELING;BSN;NUMRELATIE;NUMADRES;OVEREENKOMST_VOORLETTER;OVEREENKOMST_NAAM;OVEREENKOMST_GEBOORTEJAAR;OVEREENKOMST_STRAATNAAM;OVEREENKOMST_HUISNUMMER;OVEREENKOMST_POSTCODE;OVEREENKOMST_WOONPLAATS;ZOEKNAAM_SANCTIELIJST;ZOEKNAAM_RLE;GEBOORTEDATUM_SANCTIELIJST;GEBOORTEDATUM_RLE; VOORNAAM_SANCTIELIJST;VOORLETTER_RLE;NAAM_SANCTIELIJST;NAAM_RLE;STRAATNAAM_SANCTIELIJST;STRAATNAAM_GBA;STRAATNAAM_OPGEGEVEN;HUISNUMMER_SANCTIELIJST;HUISNUMMER_GBA;HUISNUMMER_OPGEGEVEN;POSTCODE_SANCTIELIJST;POSTCODE_GBA;POSTCODE_OPGEGEVEN;WOONPLAATS_SANCTIELIJST;WOONPLAATS_GBA;WOONPLAATS_OPGEGEVEN');
  --
  for r_rec in c_rec ( p_datum_vanaf
                     , p_peildatum
                     )
  loop
    --
    stm_util.debug ('Bezig met het ophalen van naw-gegevens wwft-verdachten');
    -- Zoeken adresgegevens o.b.v. relatienummer
    for r_adr in c_adr ( r_rec.rle_numrelatie
                       , r_rec.ads_numadres
                       )
    loop
      -- initialisatie van variabelen.
      l_straatnaam_da       :='';
      l_straatnaam_oa       :='';
      l_huisnummer_da       :=null;
      l_huisnummer_oa       :=null;
      l_postcode_da         :='';
      l_postcode_oa         :='';
      l_woonplaats_da       :='';
      l_woonplaats_oa       :='';
      ----
      stm_util.debug ('Relatienummer wwft-verdachte: '|| to_char (r_rec.rle_numrelatie));
      if r_adr.rol_codrol = 'PR'
      then
      --Vullen lokale DA/OA variabelen
        if r_adr.dwe_wrddom_srtadr = 'DA'
          then
            l_straatnaam_da := r_adr.straatnaam;
        else  -- = straatnaam OA
          if r_adr.dwe_wrddom_srtadr = 'OA'
          then
            l_straatnaam_oa := r_adr.straatnaam;
          end if;
        end if;

        if r_adr.dwe_wrddom_srtadr = 'DA'
        then
          l_huisnummer_da := r_adr.huisnummer;
        else  -- = huisnummer OA
          if  r_adr.dwe_wrddom_srtadr = 'OA'
          then
            l_huisnummer_oa := r_adr.huisnummer;
          end if;
        end if;

        if r_adr.dwe_wrddom_srtadr = 'DA'
        then
          l_postcode_da := r_adr.postcode;
        else  -- = postcode OA
          if r_adr.dwe_wrddom_srtadr = 'OA'
          then
            l_postcode_oa := r_adr.postcode;
          end if;
        end if;

        if r_adr.dwe_wrddom_srtadr = 'DA'
        then
          l_woonplaats_da := r_adr.woonplaats;
        else  -- woonplaats OA
          if r_adr.dwe_wrddom_srtadr = 'OA'
          then
            l_woonplaats_oa := r_adr.woonplaats;
          end if;
        end if;

      else
        -- Indien de relatie geen persoon is dan wordt voor DA-adres het SZ-adres
        -- en voor OA-adres het CA-adres weggeschreven
         -- Vullen lokale DA/OA variabelen
        if r_adr.dwe_wrddom_srtadr = 'SZ'
        then
          l_straatnaam_da := r_adr.straatnaam;
        else  -- = straatnaam OA
         if r_adr.dwe_wrddom_srtadr = 'CA'
         then
           l_straatnaam_oa := r_adr.straatnaam;
         end if;
        end if;

        if r_adr.dwe_wrddom_srtadr = 'SZ'
        then
          l_huisnummer_da := r_adr.huisnummer;
        else  -- = huisnummer OA
          if  r_adr.dwe_wrddom_srtadr = 'CA'
          then
            l_huisnummer_oa := r_adr.huisnummer;
          end if;
        end if;


        if r_adr.dwe_wrddom_srtadr = 'SZ'
        then
          l_postcode_da := r_adr.postcode;
        else  -- = postcode OA
          if r_adr.dwe_wrddom_srtadr = 'CA'
          then
            l_postcode_oa := r_adr.postcode;
          end if;
        end if;

        if r_adr.dwe_wrddom_srtadr = 'SZ'
        then
          l_woonplaats_da := r_adr.woonplaats;
        else  -- woonplaats OA
          if r_adr.dwe_wrddom_srtadr = 'CA'
          then
            l_woonplaats_oa := r_adr.woonplaats;
          end if;
        end if;

      end if;

    end loop;
    --
    r_san := null;

    -- Ophalen gegevens uit sanctielijsten
    open c_san (r_rec.sat_id);
    fetch c_san into r_san;
    --
    r_rle := null;
    -- Ophalen relatiegegevens
    open c_rle (r_rec.rle_numrelatie);
    fetch c_rle into r_rle;
    -- ophalen regeling o.b.v. relatienummer
    l_rgg_code := ophalen_regeling ( r_rec.rle_numrelatie
                                   , r_rle.soort
                                   );
    --
    stm_util.debug ('Schrijven naar csv ');
    -- Wegschrijven naar het csv bestand
    utl_file.put_line ( g_bestand,
                        r_rec.dat_signalering             ||';'||
                        r_san.bron                        ||';'||
                        r_san.bron_referentie             ||';'||
                        r_san.soort                       ||';'||
                        l_rgg_code                        ||';'||
                        r_rle.sofinummer                  ||';'||
                        r_rec.rle_numrelatie              ||';'||
                        r_rec.ads_numadres                ||';'||
                        r_rec.overeenkomst_voorletter     ||';'||
                        r_rec.overeenkomst_naam           ||';'||
                        r_rec.overeenkomst_geboortejaar   ||';'||
                        r_rec.overeenkomst_straatnaam     ||';'||
                        r_rec.overeenkomst_huisnummer     ||';'||
                        r_rec.overeenkomst_postcode       ||';'||
                        r_rec.overeenkomst_woonplaats     ||';'||
                        r_san.zoeknaam                    ||';'||
                        r_rle.zoeknaam                    ||';'||
                        r_san.datgeboorte                 ||';'||
                        r_rle.datgeboorte                 ||';'||
                        r_san.voorletter                  ||';'||
                        r_rle.voorletter                  ||';'||
                        r_san.achternaam                  ||';'||
                        r_rle.namrelatie                  ||';'||
                        r_san.straatnaam                  ||';'||
                        l_straatnaam_da                   ||';'||
                        l_straatnaam_oa                   ||';'||
                        r_san.huisnummer                  ||';'||
                        l_huisnummer_da                   ||';'||
                        l_huisnummer_oa                   ||';'||
                        r_san.postcode                    ||';'||
                        l_postcode_da                     ||';'||
                        l_postcode_oa                     ||';'||
                        r_san.woonplaats                  ||';'||
                        l_woonplaats_da                   ||';'||
                        l_woonplaats_oa );
    -- sluiten cursor
    close c_san;
    --
    close c_rle;
    --
    l_tel_aantal_rapport := l_tel_aantal_rapport +1;

  end loop;
  stm_util.debug ('Aantal gerapporteerden: '|| to_char (l_tel_aantal_rapport));

end maak_output_bestand;
PROCEDURE MATCH_NAW_GEGEVENS
 (P_REC_ID IN number
 ,P_RLE_NUMRELATIE IN number
 ,P_REC_VOORLETTER IN varchar2
 ,P_REC_ZOEKNAAM IN varchar2
 ,P_REC_GEBOORTEJAAR IN number
 ,P_RLE_VOORLETTER IN varchar2
 ,P_RLE_ZOEKNAAM IN varchar2
 ,P_RLE_DATGEBOORTE IN date
 ,P_NUMADRES IN number
 ,P_REC_STRAATNAAM IN varchar2
 ,P_REC_HUISNUMMER IN varchar2
 ,P_REC_POSTCODE IN varchar2
 ,P_REC_WOONPLAATS IN varchar2
 ,P_ZOEKLENGTE_NAAM IN number
 ,P_ZOEKLENGTE_STRAATNAAM IN number
 ,P_ZOEKLENGTE_WOONPLAATS IN number
 )
 IS

  cursor c_adr ( b_numrelatie number
               , b_numadres   number
                )
  is
    select ads.numadres
         , ads.huisnummer
         , ads.postcode
         , stt.straatnaam
         , wps.woonplaats
    from   rle_adressen ads
         , rle_straten stt
         , rle_woonplaatsen wps
    where  stt.straatid = ads.stt_straatid
    and    wps.woonplaatsid = ads.wps_woonplaatsid
    and    ads.numadres in ( select ras.ads_numadres
                             from   rle_relatie_adressen ras
                             where  ras.rle_numrelatie = b_numrelatie
                             and    ras.ads_numadres   = nvl (b_numadres, ras.ads_numadres))
   ;


  -- Variabelen
  l_match_voorletter    varchar2 (1):= null;
  l_match_naam          varchar2 (1):= null;
  l_match_geboortejaar  varchar2 (1):= null;
  l_match_straatnaam    varchar2 (1):= null;
  l_match_huisnummer    varchar2 (1):= null;
  l_match_postcode      varchar2 (1):= null;
  l_match_woonplaats    varchar2 (1):= null;
  r_adr                 c_adr%rowtype;
begin
  --
  stm_util.debug ('Bezig met het vergelijken van naw-gegevens voor zoeknaam: '|| p_rec_zoeknaam);

  -- Vergelijken naam en geboortedatum
  stm_util.debug ('Bezig met het vergelijken van voorletter');
  --
   if p_rec_voorletter = substr (p_rle_voorletter,1,1)
   then
     l_match_voorletter := 'J';
   end if;
  --
  stm_util.debug ('Bezig met het vergelijken van achternaam');
  --
  if p_zoeklengte_naam is null
  then
    if p_rle_zoeknaam = p_rec_zoeknaam
    then
      l_match_naam := 'J';
    end if;
  else
    if p_rle_zoeknaam like p_rec_zoeknaam ||'%'
    then
      l_match_naam := 'J';
    end if;
  end if;

  stm_util.debug ('Bezig met het vergelijken van geboortejaar');
  --
  if p_rec_geboortejaar = extract(year from p_rle_datgeboorte)
  then
    l_match_geboortejaar := 'J';
  end if;
  --
  stm_util.debug ('Openen cursor');
  -- open cursor voor het ophalen van adresgevens indien de match niet o.b.v. adresgegevens is gebeurd

  for r_adr in c_adr ( p_rle_numrelatie
                     , p_numadres
                     )
  loop
    --
    l_match_straatnaam := null;
    l_match_huisnummer := null;
    l_match_postcode   := null;
    l_match_woonplaats := null;

    -- vergelijken adresgegevens
     stm_util.debug ('Bezig met het vergelijken van straatnaam');
    --
    if p_zoeklengte_straatnaam is null
    then
      if upper (p_rec_straatnaam) = upper (r_adr.straatnaam)
      then
        l_match_straatnaam := 'J';
      end if;
    else
      if upper (r_adr.straatnaam) like upper (p_rec_straatnaam||'%')
      then
        l_match_straatnaam := 'J';
      end if;
    end if;
   --
   stm_util.debug ('Bezig met het vergelijken van huisnummer');
   --
    if p_rec_huisnummer = to_char (r_adr.huisnummer)
    then
      l_match_huisnummer := 'J';
    end if;
    --
    stm_util.debug ('Bezig met het vergelijken van postcode');
    --
    if p_rec_postcode = r_adr.postcode
    then
      l_match_postcode := 'J';
    end if;
    --
    stm_util.debug ('Bezig met het vergelijken van woonplaats');
    --
    if p_zoeklengte_woonplaats is null
    then
      if upper (p_rec_woonplaats) = upper (r_adr.woonplaats)
      then
        l_match_woonplaats := 'J';
      end if;
    else
      if upper (r_adr.woonplaats) like upper (p_rec_woonplaats|| '%')
      then
        l_match_woonplaats := 'J';
      end if;
    end if;
    -- Wegschrijven naar tabel wwft-verdachten
    wegschrijven_naar_tabel ( p_sig_datum           => sysdate
                            , p_sat_id              => p_rec_id
                            , p_numrelatie          => p_rle_numrelatie
                            , p_numadres            => r_adr.numadres
                            , p_match_voorletter    => l_match_voorletter
                            , p_match_naam          => l_match_naam
                            , p_match_geboortejaar  => l_match_geboortejaar
                            , p_match_straatnaam    => l_match_straatnaam
                            , p_match_huisnummer    => l_match_huisnummer
                            , p_match_postcode      => l_match_postcode
                            , p_match_woonplaats    => l_match_woonplaats
                            );
  end loop;

end;
PROCEDURE START_MATCH_MUTATIES
 (P_DATUM_VORIGE_RUN IN date
 ,P_ZOEKLENGTE_NAAM IN number
 ,P_ZOEKLENGTE_STRAATNAAM IN number
 ,P_ZOEKLENGTE_WOONPLAATS IN number
 ,P_HERSTART_NUMRELATIE IN varchar2
 )
 IS

  -- Cursor c_rec ophalen relaties met mutatiedatum groter vorige draaidag
  cursor c_rec ( b_mutatie_datum       date
               , b_herstart_numrelatie varchar2
               )
   is
     select rle.numrelatie
          , rle.voorletter
          , rle.namrelatie
          , rle.zoeknaam
          , rle.datgeboorte
          , ras.adm_code
          , ras.dwe_wrddom_srtadr
          , ras.rol_codrol
          , ads.numadres
          , ads.huisnummer
          , ads.postcode
          , stt.straatnaam
          , wps.woonplaats
     from rle_relaties rle
     left outer join rle_relatie_adressen ras
       on   ras.rle_numrelatie = rle.numrelatie
     join rle_adressen ads
       on   ads.numadres = ras.ads_numadres
     join rle_straten  stt
       on   stt.straatid =ads.stt_straatid
     join rle_woonplaatsen wps
       on   wps.woonplaatsid = ads.wps_woonplaatsid
    where  (rle.dat_mutatie   > b_mutatie_datum
          or
            ras.dat_mutatie    > b_mutatie_datum)
    and   rle.numrelatie > to_number (b_herstart_numrelatie)
    order by rle.numrelatie asc;

  -- Cursor c_san voor het ophalen van alle records uit rle_sanctielijsten
  cursor c_san ( b_zoeknaam              varchar2
               , b_geboortejaar          number
               , b_straatnaam            varchar2
               , b_huisnummer            varchar2
               , b_postcode              varchar2
               , b_woonplaats            varchar2
               , b_zoeklengte_naam       number
               , b_zoeklengte_straatnaam number
               , b_zoeklengte_woonplaats number
               )
  is
    select id
         , bron
         , bron_referentie
         , soort
         , status
         , voorletter
         , achternaam
         , volledigenaam
         , zoeknaam
         , datgeboorte
         , geboortejaar
         , straatnaam
         , huisnummer
         , postcode
         , woonplaats
         , dat_creatie
    from   rle_sanctielijsten
    where (b_zoeknaam like (case
                            when b_zoeklengte_naam is not null
                            then
                              substr (zoeknaam,1,p_zoeklengte_naam) ||'%'
                            else
                              zoeknaam
                           end)
      and   geboortejaar = b_geboortejaar)
    or(b_straatnaam like (upper (case
                                  when b_zoeklengte_straatnaam is not null
                                  then
                                    substr (straatnaam,1,p_zoeklengte_straatnaam) ||'%'
                                  else
                                    straatnaam
                                  end))

         and (b_woonplaats like upper(case
                                      when b_zoeklengte_woonplaats is not null
                                      then
                                        substr (woonplaats,1,p_zoeklengte_woonplaats) ||'%'
                                      else
                                        woonplaats
                                      end) )

         and to_char (huisnummer) = nvl ( b_huisnummer
                                        , to_char (huisnummer)
                                        ))

    or ( postcode = b_postcode
       and to_char (huisnummer) = b_huisnummer)
    ;

  -- Tellers
  l_tel_i_zkn             number := 0;
  l_tel_o_zkn             number := 0;
  l_tel_aw_zkn            number := 0;
begin
  -- Openen cursor om populatie te matchen relaties te bepalen
  for r_rec in c_rec ( p_datum_vorige_run
                     , p_herstart_numrelatie
                     )
  loop
   -- Start match indien het een persoon betreft en/ of zoeklengte opgegeven is
   stm_util.debug ('Start screenen rle-mutaties');

    if upper (r_rec.rol_codrol) = 'PR'
       and r_rec.zoeknaam is not null
    then
       -- Open cursor o.b.v. volledigenaam
      for r_san in c_san ( r_rec.zoeknaam
                         , extract (year from r_rec.datgeboorte)
                         , r_rec.straatnaam
                         , r_rec.huisnummer
                         , r_rec.postcode
                         , r_rec.woonplaats
                         , p_zoeklengte_naam
                         , p_zoeklengte_straatnaam
                         , p_zoeklengte_woonplaats
                         )

      loop
      -- Matchen naw-gegevens voor personen

        match_naw_gegevens ( p_rec_id                 => r_san.id
                           , p_rle_numrelatie         => r_rec.numrelatie
                           , p_rec_voorletter         => r_san.voorletter
                           , p_rec_zoeknaam           => r_san.zoeknaam
                           , p_rec_geboortejaar       => r_san.geboortejaar
                           , p_rle_voorletter         => r_rec.voorletter
                           , p_rle_zoeknaam           => r_rec.zoeknaam
                           , p_rle_datgeboorte        => r_rec.datgeboorte
                           , p_numadres               => null
                           , p_rec_straatnaam         => r_san.straatnaam
                           , p_rec_huisnummer         => r_san.huisnummer
                           , p_rec_postcode           => r_san.postcode
                           , p_rec_woonplaats         => r_san.woonplaats
                           , p_zoeklengte_naam        => p_zoeklengte_naam
                           , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
                           , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats);

        -- Teller personen die o.b.v. zoeknaam en/ of zoeklengte gevonden zijn
        l_tel_i_zkn := l_tel_i_zkn + 1;
      end loop;

    end if;


  -- Start match indien het geen persoon betreft en/ of geen zoeklengte opgegeven is
    if upper (r_rec.rol_codrol) <> 'PR'
       and r_rec.zoeknaam is not null
    then
      for r_san in c_san ( r_rec.zoeknaam
                         , null
                         , r_rec.straatnaam
                         , r_rec.huisnummer
                         , r_rec.postcode
                         , r_rec.woonplaats
                         , p_zoeklengte_naam
                         , p_zoeklengte_straatnaam
                         , p_zoeklengte_woonplaats
                         )
      loop
      -- Matchen naw-gegevens overigen (ongelijk aan pesonen)
      match_naw_gegevens ( p_rec_id                 => r_san.id
                         , p_rle_numrelatie         => r_rec.numrelatie
                         , p_rec_voorletter         => null
                         , p_rec_zoeknaam           => r_san.zoeknaam
                         , p_rec_geboortejaar       => null
                         , p_rle_voorletter         => null
                         , p_rle_zoeknaam           => r_rec.zoeknaam
                         , p_rle_datgeboorte        => null
                         , p_numadres               => null
                         , p_rec_straatnaam         => r_san.straatnaam
                         , p_rec_huisnummer         => r_san.huisnummer
                         , p_rec_postcode           => r_san.postcode
                         , p_rec_woonplaats         => r_san.woonplaats
                         , p_zoeklengte_naam        => p_zoeklengte_naam
                         , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
                         , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats
                         );

      -- Teller overigen die o.b.v. zoeknaam gevonden zijn
      l_tel_o_zkn := l_tel_o_zkn + 1;
      --

      end loop;

    end if;

    -- Teller: zoeken o.b.v. a/w combinatie
    l_tel_aw_zkn := l_tel_aw_zkn + 1;


    -- Teller bijhouden voor log
    g_tel_scr2 := g_tel_scr2 + 1;

    -- Hestart sleutel wegschrijven
    stm_util.schrijf_herstarttelling ( 'g_tel_scr2'
                                     , g_tel_scr2
                                     );

    stm_util.schrijf_herstarttelling ( 'g_regelnr'
                                     , g_regelnr
                                     );

    stm_util.schrijf_herstartsleutel ( 'g_herstart_numrelatie'
                                     , to_char (r_rec.numrelatie)
                                     );
    --
    commit;
    --

  end loop;

  -- Aantalen matchen mutaties wegschrijven naar logbestand
  lijst ('-----Overzicht aantallen te matchen vanuit rle-mutaties------');

  lijst ( rpad ('Aantal gemuteerde relaties te onderzoeken '
               , g_descript_lengte
               ) || g_scheiding || to_char (g_tel_scr2));

  lijst ( rpad ('Aantal gemuteerde personen (rol_codrol = PR) gecontroleerd '
               , g_descript_lengte
               ) || g_scheiding || to_char (l_tel_i_zkn));

  lijst ( rpad ('Aantal overigen (rol_codrol <> PR) gecontroleerd '
               , g_descript_lengte
               ) || g_scheiding || to_char (l_tel_o_zkn));

  lijst ( rpad ('Aantal gecontroleerd op combinatie adres/ woonplaats'
               , g_descript_lengte
               ) || g_scheiding || to_char (l_tel_aw_zkn));

  lijst ( '---------------------------------------------' );

end start_match_mutaties;
PROCEDURE START_MATCH
 (P_METHODE_ONDERZOEK IN varchar2
 ,P_ZOEKLENGTE_NAAM IN number
 ,P_ZOEKLENGTE_STRAATNAAM IN number
 ,P_ZOEKLENGTE_WOONPLAATS IN number
 ,P_HERSTART_SAT_ID IN varchar2
 )
 IS

  -- Cursor c_rec bevat de te matchen populatie
  cursor c_rec ( b_methode_onderzoek varchar2
               , b_herstart_sat_id   varchar2
               )
  is
    select id
         , bron
         , bron_referentie
         , soort
         , status
         , voorletter
         , achternaam
         , volledigenaam
         , zoeknaam
         , datgeboorte
         , geboortejaar
         , straatnaam
         , huisnummer
         , postcode
         , woonplaats
         , dat_creatie
    from rle_sanctielijsten
    where status = (case
                      when b_methode_onderzoek = 'GEWIJZIGD'
                        then
                          'N'
                        else
                          status   --'AFGESTEMD' en 'NIEUW'
                    end)
    and id > to_number (b_herstart_sat_id)
    order by id asc ;

  -- Cursor c_rle1 gebruiken indien voor zoeknaam geen zoeklengte opgegeven is
  cursor c_rle1 ( b_zoeknaam        varchar2
                , b_geboortejaar    number
                , b_zoeklengte_naam number
                )
  is
    select rle.numrelatie
         , rle.voorletter
         , rle.namrelatie
         , rle.zoeknaam
         , rle.datgeboorte
    from  rle_relaties rle
    where (rle.zoeknaam like (case
                              when b_zoeklengte_naam is null
                              then
                                b_zoeknaam
                              else
                               (b_zoeknaam||'%')
                              end)
           and   nvl (extract(year from rle.datgeboorte), extract (year from sysdate)) = nvl (b_geboortejaar, extract (year from sysdate)))
    ;

  -- Cursor c_rle2 gebruiken indien matchen o.b.v. straatnaam/huisnummer/woonplaats of postcode/huisnummer
  cursor c_rle2 ( b_numrelatie  number )
  is
    select rle.numrelatie
         , rle.voorletter
         , rle.namrelatie
         , rle.zoeknaam
         , rle.datgeboorte
    from  rle_relaties rle
    where rle.numrelatie = b_numrelatie
    ;

  -- Cursor c_rle3 gebruiken indien matchen o.b.v. zoeknaam, voorletter en geen zoeklengte opgegeven is
  cursor c_rle3 ( b_zoeknaam        varchar2
                , b_voorletter      varchar2
                , b_zoeklengte_naam number
                )
  is
    select rle.numrelatie
         , rle.voorletter
         , rle.namrelatie
         , rle.zoeknaam
         , rle.datgeboorte
    from  rle_relaties rle
    where (rle.zoeknaam like (case
                              when b_zoeklengte_naam is null
                              then
                                b_zoeknaam
                              else
                                (b_zoeknaam||'%')
                              end)
         and   upper(substr (rle.voorletter,1,1)) = upper(b_voorletter))
    ;

  --  Cursor c_adr gebruiken indien ophalen persoonsgegevens o.b.v. combinatie's(straatnaam/ huisnummer/woonplaats)
  --  en (postcode/ huisnummer)  en/ of zoekstring opgegeven voor straatnaam en/ of woonplaats
  cursor c_adr ( b_straatnaam            varchar2
               , b_huisnummer            varchar2
               , b_postcode              varchar2
               , b_woonplaats            varchar2
               , b_zoeklengte_straatnaam number
               , b_zoeklengte_woonplaats number
               )
  is
    select ras.adm_code
         , ras.dwe_wrddom_srtadr
         , ads.numadres
         , to_char (ads.huisnummer)
         , ads.postcode
         , stt.straatnaam
         , wps.woonplaats
         , ras.rle_numrelatie
    from   rle_relatie_adressen ras
         , rle_adressen ads
         , rle_straten  stt
         , rle_woonplaatsen wps
    where  ads.numadres       = ras.ads_numadres
    and    stt.straatid       = ads.stt_straatid
    and    wps.woonplaatsid   = ads.wps_woonplaatsid
    and    ras.dateinde is null
    and    ((upper (stt.straatnaam)    like (case
                                            when b_zoeklengte_straatnaam is null
                                            then
                                              upper (b_straatnaam)
                                            else
                                              upper (b_straatnaam||'%')
                                            end)
            and upper (wps.woonplaats)  like (case
                                             when b_zoeklengte_woonplaats is null
                                             then
                                               upper (b_woonplaats)
                                             else
                                               upper (b_woonplaats||'%')
                                             end)
           and to_char(ads.huisnummer) = nvl (b_huisnummer,to_char (ads.huisnummer)))
         or
           (ads.postcode               = b_postcode
           and to_char(ads.huisnummer) = b_huisnummer));

  -- Variabelen
  r_rec       c_rec%rowtype;
  r_rle1      c_rle1%rowtype;
  r_adr       c_adr%rowtype;
  r_rle2      c_rle2%rowtype;
  r_rle3      c_rle3%rowtype;

  l_substr_naam           varchar2 (500);
  l_substr_straatnaam     varchar2 (500);
  l_substr_woonplaats     varchar2 (500);
  l_numrelatie            number;
  l_tel_i_zkn             number := 0;
  l_tel_datgeb_onb        number := 0;
  l_tel_o_zkn             number := 0;
  l_tel_aw_zkn            number := 0;
begin
  -- Openen cursor om populatie te matchen relaties te bepalen
  stm_util.debug ('Start screenen vanuit sanctielijsten');
  stm_util.debug ('Populatie te screenen personen wordt bepaald');
  stm_util.debug ('Gevonden populatie wordt gescreend');

  for r_rec in c_rec ( p_methode_onderzoek
                     , p_herstart_sat_id
                     )
  loop
    if p_zoeklengte_naam is not null
    then
      l_substr_naam := substr (r_rec.zoeknaam,1,p_zoeklengte_naam);
    else
      l_substr_naam := r_rec.zoeknaam;
    end if;

    if p_zoeklengte_straatnaam is not null
    then
      l_substr_straatnaam := substr (r_rec.straatnaam,1,p_zoeklengte_straatnaam);
    else
      l_substr_straatnaam := r_rec.straatnaam;
    end if;

    if p_zoeklengte_woonplaats is not null
    then
      l_substr_woonplaats := substr (r_rec.woonplaats,1,p_zoeklengte_woonplaats);
    else
      l_substr_woonplaats := r_rec.woonplaats;
    end if;
   -- Start match indien het een persoon betreft, geboortejaar bekend en/ of zoeklengte opgegeven is
    if r_rec.soort = 'I'
       and r_rec.zoeknaam is not null
       and r_rec.geboortejaar is not null
    then
     -- Open cursor o.b.v. zoeknaam en geboortejaar
     for r_rle1 in c_rle1 ( l_substr_naam
                          , r_rec.geboortejaar
                          , p_zoeklengte_naam)
      loop
      -- Controle match nawgegevens

      match_naw_gegevens ( p_rec_id                 => r_rec.id
                         , p_rle_numrelatie         => r_rle1.numrelatie
                         , p_rec_voorletter         => r_rec.voorletter
                         , p_rec_zoeknaam           => l_substr_naam
                         , p_rec_geboortejaar       => r_rec.geboortejaar
                         , p_rle_voorletter         => r_rle1.voorletter
                         , p_rle_zoeknaam           => r_rle1.zoeknaam
                         , p_rle_datgeboorte        => r_rle1.datgeboorte
                         , p_numadres               => null
                         , p_rec_straatnaam         => l_substr_straatnaam
                         , p_rec_huisnummer         => r_rec.huisnummer
                         , p_rec_postcode           => r_rec.postcode
                         , p_rec_woonplaats         => l_substr_woonplaats
                         , p_zoeklengte_naam        => p_zoeklengte_naam
                         , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
                         , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats
                          );
        --  Teller personen die o.b.v. zoeknaam, geboortejaar en zonder zoeklengte gevonden zijn
        l_tel_i_zkn := l_tel_i_zkn + 1;

        l_numrelatie := r_rle1.numrelatie;
      end loop;

    end if;

    -- Start match indien het een persoon betreft, geboortejaar onbekend en/ of geen zoekstring opgegeven voor zoeknaam
    if r_rec.soort = 'I'
       and r_rec.zoeknaam is not null
       and r_rec.datgeboorte is null
    then
      -- Open cursor o.b.v. volledigenaam en voorletter
      for r_rle3 in c_rle3 ( l_substr_naam
                           , r_rec.voorletter
                           , p_zoeklengte_naam)
      loop
       -- Matchen naw-gegevens
        match_naw_gegevens ( p_rec_id                 => r_rec.id
                           , p_rle_numrelatie         => r_rle3.numrelatie
                           , p_rec_voorletter         => r_rec.voorletter
                           , p_rec_zoeknaam           => l_substr_naam
                           , p_rec_geboortejaar       => r_rec.geboortejaar
                           , p_rle_voorletter         => r_rle3.voorletter
                           , p_rle_zoeknaam           => r_rle3.zoeknaam
                           , p_rle_datgeboorte        => r_rle3.datgeboorte
                           , p_numadres               => null
                           , p_rec_straatnaam         => l_substr_straatnaam
                           , p_rec_huisnummer         => r_rec.huisnummer
                           , p_rec_postcode           => r_rec.postcode
                           , p_rec_woonplaats         => l_substr_woonplaats
                           , p_zoeklengte_naam        => p_zoeklengte_naam
                           , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
                           , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats
                           );
        -- Teller: personen die o.b.v. zoeknaam, geboortejaar onbekend en geen zoeklengte gevonden zijn
        l_tel_datgeb_onb := l_tel_datgeb_onb + 1;

        l_numrelatie := r_rle3.numrelatie;
        --
      end loop;
      --

    end if;

    -- Start match indien het geen persoon betreft en/ of geen zoeklengte opgegeven is
    if r_rec.soort = 'O'
       and r_rec.zoeknaam is not null
    then

      for r_rle1 in c_rle1 ( l_substr_naam
                           , null
                           , p_zoeklengte_naam)
      loop

        -- Matchen naw-gegevens
        match_naw_gegevens (  p_rec_id                 => r_rec.id
                            , p_rle_numrelatie         => r_rle1.numrelatie
                            , p_rec_voorletter         => null
                            , p_rec_zoeknaam           => l_substr_naam
                            , p_rec_geboortejaar       => null
                            , p_rle_voorletter         => null
                            , p_rle_zoeknaam           => r_rle1.zoeknaam
                            , p_numadres               => null
                            , p_rle_datgeboorte        => null
                            , p_rec_straatnaam         => l_substr_straatnaam
                            , p_rec_huisnummer         => r_rec.huisnummer
                            , p_rec_postcode           => r_rec.postcode
                            , p_rec_woonplaats         => l_substr_woonplaats
                            , p_zoeklengte_naam        => p_zoeklengte_naam
                            , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
                            , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats
                           );

        -- Teller: overigen die o.b.v. zoeknaam gevonden zijn
        l_tel_o_zkn := l_tel_o_zkn + 1;

        l_numrelatie := r_rle1.numrelatie;
      end loop;
    end if;

    -- Matchen o.b.v. combinatie (straatnaam/ huisnummer/woonplaats) en (postcode/ huisnummer)  en/ of zoekstring opgegeven voor straatnaam en/ of woonplaats
    if ( r_rec.straatnaam     is not null
       and r_rec.huisnummer   is not null
       and r_rec.woonplaats   is not null
       )
       or
       ( r_rec.huisnummer        is not null
       and r_rec.postcode        is not null
       )
    then
      for r_adr in c_adr ( l_substr_straatnaam
                         , r_rec.huisnummer
                         , r_rec.postcode
                         , l_substr_woonplaats
                         , p_zoeklengte_straatnaam
                         , p_zoeklengte_woonplaats
                         )
      loop
        if r_adr.rle_numrelatie <> nvl(l_numrelatie,0)
        then
          open c_rle2 (r_adr.rle_numrelatie);
          fetch c_rle2 into r_rle2;

          if c_rle2%found
          then
            close c_rle2;
            -- Matchen naw-gegevens
            match_naw_gegevens ( p_rec_id                 => r_rec.id
                               , p_rle_numrelatie         => r_rle2.numrelatie
                               , p_rec_voorletter         => r_rec.voorletter
                               , p_rec_zoeknaam           => r_rec.zoeknaam
                               , p_rec_geboortejaar       => r_rec.geboortejaar
                               , p_rle_voorletter         => r_rle2.voorletter
                               , p_rle_zoeknaam           => r_rle2.zoeknaam
                               , p_rle_datgeboorte        => r_rle2.datgeboorte
                               , p_numadres               => r_adr.numadres
                               , p_rec_straatnaam         => l_substr_straatnaam
                               , p_rec_huisnummer         => r_rec.huisnummer
                               , p_rec_postcode           => r_rec.postcode
                               , p_rec_woonplaats         => l_substr_woonplaats
                               , p_zoeklengte_naam        => p_zoeklengte_naam
                               , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
                               , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats
                               );
            -- teller: adres woonplaats combinatie
            l_tel_aw_zkn := l_tel_aw_zkn + 1;

          else
            close c_rle2;

          end if;
        end if;

      end loop;

    end if;
    -- Teller: hoofdselectie bijhouden voor logbestand
    g_tel_scr1 := g_tel_scr1 + 1;

    stm_util.debug ('Aantal uit populatie gescreend'|| to_char (g_tel_scr1));

    -- Herstartsleutel wegschrijven
    stm_util.schrijf_herstarttelling ( 'g_tel_scr1'
                                     , g_tel_scr1
                                      );

    stm_util.schrijf_herstarttelling ( 'g_regelnr'
                                     , g_regelnr
                                      );

    stm_util.schrijf_herstartsleutel ( 'g_herstart_sat_id'
                                     , to_char (r_rec.id)
                                      );

    stm_util.schrijf_herstartsleutel ( 'g_herstart_numrelatie'
                                     , '0'
                                      );
    -- Status gematchte personen updaten naar 'AFGESTEMD'
    update rle_sanctielijsten
    set status = 'A'
    where id = r_rec.id;
    --
    commit;
    --
  end loop;

  stm_util.debug ('Screenen vanuit santielijsten voltooid');

  -- Aantalen wegschrijven naar logbestand
  lijst ( '-----Overzicht aantallen matchen vanuit sanctielijsten------' );

  lijst ( rpad ( 'Aantal te onderzoeken records uit sanctielijsten'
               , g_descript_lengte
               ) || g_scheiding || to_char (g_tel_scr1));

  lijst ( rpad ('Aantal personen (soort I en geboortedatum bekend) gecontroleerd '
               , g_descript_lengte
               ) || g_scheiding || to_char (l_tel_i_zkn));

  lijst ( rpad ('Aantal personen (soort I en geboortedatum onbekend) gecontroleerd '
               , g_descript_lengte
               ) || g_scheiding || to_char (l_tel_datgeb_onb));

  lijst ( rpad ('Aantal overigen (soort O) gecontroleerd '
               , g_descript_lengte
               ) || g_scheiding || to_char (l_tel_o_zkn));

  lijst ( rpad ('Aantal gecontroleerd op combinatie adres/ woonplaats'
               , g_descript_lengte
               ) || g_scheiding || to_char (l_tel_aw_zkn));

  lijst ( '---------------------------------------------' );

  --
  commit;
  --

end start_match;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 ,P_METHODE_ONDERZOEK IN varchar2
 ,P_DATUM_VANAF IN date
 ,P_NEGEREN_BESTANDEN IN varchar2
 ,P_ZOEKLENGTE_NAAM IN number
 ,P_ZOEKLENGTE_STRAATNAAM IN number
 ,P_ZOEKLENGTE_WOONPLAATS IN number
 )
 IS

  -- Variabelen en constanten
  cn_module             constant varchar2(50) := 'RLE_SCR_RELATIES';
  l_vorige_procedure    varchar2 (200);

  l_controle            boolean:= false;
  e_abort_batch         exception;
  l_negeer_best         varchar2 (50);
  l_foutmelding         varchar2 (1000);
  l_dat_vorige_run      date;
  l_aantal_match        number := 0;
  l_peildatum           date;
begin
  l_vorige_procedure         := stm_util.t_procedure ;
  stm_util.t_procedure       := cn_module;
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_SCR_RELATIES';
  stm_util.t_tekst           := NULL;
  --

  stm_util.debug ('Controle herstart');
  if stm_util.herstart
  then
    stm_util.debug ('Betreft herstart; ophalen herstartsleutel');
    -- Ophalen herstartsleutel
    g_herstart_sat_id      := to_number (stm_util.lees_herstartsleutel ( 'g_herstart_sat_id'));
    g_herstart_numrelatie  := to_number (stm_util.lees_herstartsleutel ( 'g_herstart_numrelatie'));

    stm_util.debug ('Herstartsleutel1:'||g_herstart_sat_id );
    stm_util.debug ('Herstartsleutel2:'||g_herstart_numrelatie );

    -- Herinitialiseren tellingen
    stm_util.debug ('Herinitialiseren tellingen');
    if g_herstart_numrelatie = 0
    then
      g_tel_scr1 := stm_util.lees_herstarttelling('g_tel_scr1');
    else
      g_tel_scr2 := stm_util.lees_herstarttelling('g_tel_scr2');
    end if;

    g_regelnr := to_number (stm_util.lees_herstarttelling('g_regelnr'));

    -- Schrijf herstart punt en tijd in logverslag
    lijst ( 'Procedure op dit punt geherstart op '
          || to_char(sysdate,'dd-mm-yyyy hh24:mi')
           );
    if g_herstart_numrelatie = '0'
    then
      lijst ( rpad ( 'Herstart bij het matchen van sanctielijsten vanaf id'
            , g_descript_lengte
            ) || g_scheiding || to_char (g_tel_scr1));
    else
      lijst ( rpad ( 'Herstart bij het matchen van mutaties vanaf relatienummer'
            , g_descript_lengte
            ) || g_scheiding || to_char (g_tel_scr2));
    end if;

  else
    stm_util.debug ('Initialiseren herstartsleutel');
    -- Initialiseren herstartsleutel
    g_herstart_sat_id      := '0';
    g_herstart_numrelatie  := '0';

    -- Initialiseren tellingen
    g_tel_scr1  := 0;
    g_tel_scr1  := 0;

  end if;
  -- peildatum is gelijk aan het moment van starten dit t.b.v. rapportage
  l_peildatum := sysdate;

  stm_util.debug ('Betreft een normale start op: '||to_char (l_peildatum, 'dd-mm-yyyy'));

  -- Schrijven logbestand
  lijst ( rpad ('***** LOGVERSLAG SCREEENEN RELATIES *****', 50)
               || 'DRAAIDATUM' || to_char (sysdate));

  lijst ( ' ' );

  lijst ( rpad ('Parameters bij het proces: ', 27));

  lijst ( '---------------------------------------------' );

  lijst ( rpad ( 'Onderzoeksmethode'
               , g_descript_lengte
               ) || g_scheiding || p_methode_onderzoek );

  lijst ( rpad ( 'Datum vanaf'
               , g_descript_lengte
                ) || g_scheiding || nvl(to_char(p_datum_vanaf), to_char(trunc (sysdate))));

  lijst ( rpad ('Negeren niet verwerkte bestanden'
               , g_descript_lengte
               ) || g_scheiding || p_negeren_bestanden);

  lijst ( rpad ('Zoeklengte Naam'
               , g_descript_lengte
               ) || g_scheiding || nvl(to_char(p_zoeklengte_naam), 'Niet gevuld'));

  lijst ( rpad ('Zoeklengte straat'
               , g_descript_lengte
               ) || g_scheiding || nvl(to_char(p_zoeklengte_straatnaam), 'Niet gevuld'));

  lijst ( rpad ('Zoeklengte woonplaats'
               , g_descript_lengte
               ) || g_scheiding || nvl(to_char(p_zoeklengte_woonplaats), 'Niet gevuld'));

  lijst ( ' ' );

  lijst ( '---------------------------------------------' );

  lijst ( ' ' );

  --
  g_methode_onderzoek := p_methode_onderzoek;
  --

  -- Controle of er niet verwerkte bestanden in evt staan
  l_negeer_best:= p_negeren_bestanden;
  if l_negeer_best = 'NEE'
  then
    stm_util.debug ('Controle onverwerkte bestanden');

    l_controle:= controle_onverw_bestanden;
    if l_controle = true
    then
      stm_util.debug ('Proces afgebroken n.a.v. onverwerkte events');

      raise e_abort_batch;
    end if;

  end if;

  -- Bepaal begindatum vorige run; nodig voor het selecteren van gemuteerde relaties
  select nvl(max (begindatum), sysdate)
  into l_dat_vorige_run
  from rle_tech_data
  where modulenaam = 'RLE_SCR_RELATIES';

  stm_util.debug ('Begindatum vorige run: '||to_char (l_dat_vorige_run));
  -- Start proces matchen
  stm_util.debug ('Proces screenen relaties wordt gestart');

  start_match ( p_methode_onderzoek      => p_methode_onderzoek
              , p_zoeklengte_naam        => p_zoeklengte_naam
              , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
              , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats
              , p_herstart_sat_id        => g_herstart_sat_id
              );

  -- Matchen mutaties in RLE na vorige run
  if (l_dat_vorige_run is not null or p_methode_onderzoek <> 'VOLLEDIG')-- Bij de initiële run matchen mutatie niet uitvoeren (technische tabel is leeg)
  then
    stm_util.debug ('Proces screenen mutaties rle wordt gestart');
    start_match_mutaties ( p_datum_vorige_run       => l_dat_vorige_run
                         , p_zoeklengte_naam        => p_zoeklengte_naam
                         , p_zoeklengte_straatnaam  => p_zoeklengte_straatnaam
                         , p_zoeklengte_woonplaats  => p_zoeklengte_woonplaats
                         , p_herstart_numrelatie    => g_herstart_numrelatie
                         );
  end if;

  -- Aanmaken csv-bestand t.b.v. rapportage aan compliance
  stm_util.debug ('Rapportage WWFT-Verdachten wordt opgesteld');

   maak_output_bestand ( p_datum_vanaf  => p_datum_vanaf
                       , p_peildatum    => l_peildatum
                       );


  -- Aantal match gevonden
  select count (*)
  into l_aantal_match
  from rle_wwft_verdachten
  where dat_creatie >= l_peildatum;

  stm_util.debug ('Aantal match gevonden: '|| to_char (l_aantal_match));
  -- Aantal gevonden match wegschrijven naar logbestand
  lijst ( rpad ('Aantal gevonden match'
               , g_descript_lengte
               ) || g_scheiding || to_char (l_aantal_match ));

  --  Voettekst van lijsten
  lijst ( rpad ('********** EINDE VERSLAG SCREENEN RELATIES **********', 40));

  -- Zet begin- en einddatum in rle_tech_data; nodig voor het ophalen einddatum vorige run
  insert into rle_tech_data ( modulenaam
                            , begindatum
                            , einddatum
                            , script_id
                            )
  values                    ( 'RLE_SCR_RELATIES'
                            , l_peildatum
                            , sysdate
                            , p_script_id
                           );
  --
  utl_file.fclose(g_bestand);

  -- Jobstatussen goed zetten
  stm_util.insert_job_statussen ( g_volgnummer
                                , 'S'
                                , '0'
                                 );
  -- Verwijderen herstart
  stm_util.verwijder_herstart ;
  --
  commit;
  --
  stm_util.t_procedure := l_vorige_procedure ;
  --
exception
  when e_abort_batch
  then
    rollback;
    stm_util.insert_job_statussen ( g_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( g_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( g_volgnummer
                                  , 'I'
                                  , g_foutmelding
                                  );
  commit;

  when others
  then
    l_foutmelding := sqlerrm;

    lijst ( rpad ('Foutmelding'
                 , g_descript_lengte
                 ) || g_scheiding || l_foutmelding);

    stm_util.insert_job_statussen ( g_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( g_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
     stm_util.insert_job_statussen ( g_volgnummer
                                  , 'I'
                                  , l_foutmelding
                                  );
  commit;

end verwerk;
end rle_scr_relaties;
/