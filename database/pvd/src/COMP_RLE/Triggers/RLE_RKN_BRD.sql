CREATE OR REPLACE TRIGGER comp_rle.RLE_RKN_BRD
 BEFORE DELETE
 ON comp_rle.RLE_RELATIE_KOPPELINGEN
 FOR EACH ROW
declare
  l_soort_relatie varchar2(3);
begin
  /* BR IER0024
  ** Werkgever 000001 mag niet gemuteerd of verwijderd worden
  */
  rle_wgr_000001.prc_chk_wgr_000001(pin_numrelatie => :old.rle_numrelatie);
  --
  /* synchronisatie met component gba */
  select rol_codrol_beperkt_tot
  into   l_soort_relatie
  from   rle_rol_in_koppelingen
  where  id = :old.rkg_id
  ;
   --
  if l_soort_relatie = 'PR'
  then
    rle_gba_verwijder_verwant( p_rkn_id => :old.id
                             , p_numrelatie => :old.rle_numrelatie
                             , p_numrelatie_voor => :old.rle_numrelatie_voor
                             , p_dat_begin => :old.dat_begin
                             , p_rol => :old.rkg_id
                             );
  else
    -- 01-06-2005, VER:
    -- PR-UII Consolidatie, alleen publicaties indien het GEEN verwantschap betreft
    --
    -- 02-03-2006. NKU, Project Levensloop
    --                           Relaties met rol WA, WB, WC of WD
    --                           worden niet gepubliceerd.
    --
    if    not rle_chk_rol_niet_mtb(p_relatienr => :new.rle_numrelatie)
       and not rle_chk_rol_niet_mtb(p_relatienr => :new.rle_numrelatie_voor)
    then
      ibm.publish_event
        ( 'MUT RLE KOPPELINGEN'
        , 'D'
        , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
        , :old.ID                                           , NULL
        , :old.RKG_ID                                       , NULL
        , :old.RLE_NUMRELATIE                               , NULL
        , :old.RLE_NUMRELATIE_VOOR                          , NULL
        , to_char(:old.DAT_BEGIN, 'dd-mm-yyyy hh24:mi:ss')  , NULL
        , to_char(:old.DAT_EINDE, 'dd-mm-yyyy hh24:mi:ss')  , NULL
        , :old.CREATIE_DOOR                                 , NULL
        , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
        , :old.MUTATIE_DOOR                                 , NULL
        , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
        );
    end if;
  end if;
end;
/