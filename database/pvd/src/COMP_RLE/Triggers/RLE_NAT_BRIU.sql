CREATE OR REPLACE TRIGGER comp_rle.RLE_NAT_BRIU
 BEFORE INSERT OR UPDATE
 ON comp_rle.RLE_NATIONALITEITEN
 FOR EACH ROW
DECLARE

  --
/************************************************************

Triggernaam: rle_nat_briu

Datum       Wie        Wat

--------    ---------- -----------------------------------
21-10-2009  UAO        Creation
30-10-2009  XMK        Controle domein NAT toegevoegd.
04-11-2009  XMK        Controle br ier0039 nat toegevoegd.
01-12-2009  XFZ        Controle br ier0039 nat verwijderd :P
                       verplaatst naar scherm rle0061F ivm verplicht
                       nationaliteit igv KV.
*************************************************************/
--
  cursor c_seq
  is
  select rle_nat_seq1.nextval
  from dual;
  --
  cursor c_dual
  is
  select sysdate
  ,      user
  from   dual;
  --
  l_sysdate   date;
  l_user      varchar2(30);
  l_seq       number;
  --
begin
  --
  rfe_chk_refwrd(p_ref_code    => 'NAT'
                ,p_waarde_kort => :new.code_nationaliteit
                 );
  --
  open c_dual;
  fetch c_dual
  into  l_sysdate
  ,     l_user;
  close c_dual;
  --
  if inserting
  then
    --
    open c_seq;
    fetch c_seq into l_seq;
    close c_seq;
    --
    :new.id           := l_seq;
    :new.dat_creatie  := l_sysdate;
    :new.creatie_door := l_user;
    :new.dat_mutatie  := l_sysdate;
    :new.mutatie_door := l_user;
    --
  else
    --
    :new.dat_mutatie  := l_sysdate;
    :new.mutatie_door := l_user;
    --
  end if;
END;
/