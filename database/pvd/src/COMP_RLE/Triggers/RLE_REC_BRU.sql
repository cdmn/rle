CREATE OR REPLACE TRIGGER comp_rle.RLE_REC_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_RELATIE_EXTERNE_CODES
 FOR EACH ROW
DECLARE

/* Zoeken afstemming gba met NUMRELATIE */
CURSOR C_ASA_001
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT *
FROM   rle_afstemmingen_gba  asa
WHERE  asa.rle_numrelatie = cpn_numrelatie
;

r_asa_001  c_asa_001%ROWTYPE;
l_event    xmltype;

cn_event_BSNGewijzigd  constant varchar2(20) := 'rle.BSNgewijzigd';
-- 17-02-2015 ZSF Extra attribuut voor event rle.BSNgewijzigd
-- 17-02-2015 ZSF Opnieuw wgr.GewijzigdBovagNummerWerkgever
-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeIdentificatie
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 14-07-2014 ZSF rle.BSNGewijzigd => rle.BSNgewijzigd
-- 30-05-2014 EZW   Nieuw event salarismutatie voor aansluiting Bancs
cn_event_WgrId  constant varchar2(30) := 'wgr.GewijzigdeIdentificatie';
--
BEGIN
--
-- igv 'null' mutatie niets doen  ... bevinding 858
-- deze mutaties komen veel vanuit GBA
--
IF  stm_dbproc.column_modified(:new.ID,:old.ID) OR
    stm_dbproc.column_modified(:new.RLE_NUMRELATIE,:old.RLE_NUMRELATIE) OR
    stm_dbproc.column_modified(:new.DWE_WRDDOM_EXTSYS,:old.DWE_WRDDOM_EXTSYS) OR
    stm_dbproc.column_modified(:new.DATINGANG,:old.DATINGANG) OR
    stm_dbproc.column_modified(:new.CODORGANISATIE,:old.CODORGANISATIE) OR
    stm_dbproc.column_modified(:new.ROL_CODROL,:old.ROL_CODROL) OR
    stm_dbproc.column_modified(:new.EXTERN_RELATIE,:old.EXTERN_RELATIE) OR
    stm_dbproc.column_modified(:new.IND_GEVERIFIEERD,:old.IND_GEVERIFIEERD) OR
    stm_dbproc.column_modified(:new.DATEINDE,:old.DATEINDE)
THEN
   --
   IF    :new.dwe_wrddom_extsys = 'SOFI'
   THEN
      /* SOFI-nummer moet verplicht 9 cijfers hebben */
      :new.extern_relatie := LTRIM( TO_CHAR( TO_NUMBER( :new.extern_relatie ), '000000000' ) ) ;
      /* BRATT0014
      controle op de volgende range: tussen 001000007 and 999999990 */
      IF    TO_NUMBER(:new.extern_relatie) < 1000007
         OR TO_NUMBER(:new.extern_relatie) > 999999990 -- JPG 10-12-2007 Incident 94911
      THEN
         stm_dbproc.raise_error( 'RLE-10330' ) ;
      END IF;
   ELSIF :new.dwe_wrddom_extsys = 'WGR'
   THEN
      /* Werkgever nummer moet verplicht 6 cijfers hebben */
      :new.extern_relatie := LTRIM( TO_CHAR( TO_NUMBER( :new.extern_relatie ), '000000' ) ) ;
   END IF ;
   /* BRIER0004
      Controle rol bij communicatienummer
   */
   IF stm_dbproc.column_modified( :new.rol_codrol, :old.rol_codrol )
   THEN
      IF :new.rol_codrol IS NOT NULL
      THEN
         rle_rrl_chk_fk( :new.rle_numrelatie
                       , :new.rol_codrol
                       ) ;
      END IF ;
   END IF ;
   /* END BRIER0004 */
   /* RLE_REC_BRU */
   IF    stm_dbproc.column_modified( :new.rle_numrelatie,:old.rle_numrelatie)
      OR stm_dbproc.column_modified(:new.dwe_wrddom_extsys,:old.dwe_wrddom_extsys)
      OR stm_dbproc.column_modified(:new.datingang, :old.datingang)
      OR stm_dbproc.column_modified(:new.rol_codrol, :old.rol_codrol)
      OR stm_dbproc.column_modified(:new.extern_relatie, :old.extern_relatie)
      OR stm_dbproc.column_modified(:new.dateinde, :old.dateinde)
   THEN
      IF    stm_dbproc.column_modified(:new.datingang, :old.datingang)
         OR stm_dbproc.column_modified(:new.dateinde, :old.dateinde)
      THEN
         rle_rle_childper( :new.rle_numrelatie
                         , :new.datingang
                         , :new.dateinde);
      END IF;
      IF     :new.dwe_wrddom_extsys = 'RBD'
         AND (stm_dbproc.column_modified(:old.dwe_wrddom_extsys
                 ,:new.dwe_wrddom_extsys)
              OR stm_dbproc.column_modified(:old.extern_relatie
                  ,:new.extern_relatie)
             )
      THEN
         rle_rec_chk_nr_rbd (:new.extern_relatie);
      END IF;
      IF :new.dwe_wrddom_extsys IN ( 'SOFI','GBA')
      THEN
         :new.rol_codrol := 'PR';
      END IF;
      IF     :new.dwe_wrddom_extsys = 'SOFI'
        AND (stm_dbproc.column_modified(:old.dwe_wrddom_extsys
                  ,:new.dwe_wrddom_extsys)
                  OR
                  stm_dbproc.column_modified(:old.extern_relatie
                  ,:new.extern_relatie)
             )
      THEN
         stm_algm_check.elf_proef_sofi (TO_NUMBER(:new.extern_relatie));
      END IF;
      /* BRATT0015 A-nummer moet voldoen aan GBA 11-proef */
      IF  :new.dwe_wrddom_extsys = 'GBA'
        AND (   stm_dbproc.column_modified( :old.dwe_wrddom_extsys, :new.dwe_wrddom_extsys )
        OR  stm_dbproc.column_modified( :old.extern_relatie,    :new.extern_relatie    )
            )
      THEN
         IF NOT rle_gba_elfproef( :new.extern_relatie )
         THEN
            stm_dbproc.raise_error( 'RLE-10303' ) ;
         END IF ;
      END IF ;
      --
      --
      -- RTJ 08-12-2003: Call 91515: Als CODE_GBA_STATUS 'AF' is, dan mag
      --                             geen Sofinummer of a-nummer worden opgevoerd.
      IF :new.dwe_wrddom_extsys in ('GBA', 'SOFI')
      THEN
         IF c_asa_001%ISOPEN
         THEN
            CLOSE c_asa_001;
         END IF;
         OPEN  c_asa_001(:new.rle_numrelatie);
         FETCH c_asa_001 INTO r_asa_001;
         IF    r_asa_001.code_gba_status = 'AF'
         THEN
            raise_application_error(-20000,'Code GBA status is AF, daarom mag geen Sofinummer of A-nummer worden opgevoerd.');
         END IF;
         CLOSE c_asa_001;
      END IF;
      --
      -- Aanmaken voorkomen voor transformatie
      IF     :new.dwe_wrddom_extsys IN ( 'SOFI','FISC','RBD')
         AND (stm_dbproc.column_modified(:old.dwe_wrddom_extsys
               ,:new.dwe_wrddom_extsys)
             OR stm_dbproc.column_modified(:old.extern_relatie
             ,:new.extern_relatie)
             )
      THEN
         /*     rle_rme_insert(:new.rle_numrelatie
                 ,	:new.id
                 ,	:new.rol_codrol
                 ,	'RLE142'
                 ,	'2'
                 ,	'RECORD EXTERN_RELATIE'
                 ,	NULL
                 ,	:new.datingang
                 ,	:old.extern_relatie --NULL --RZA16012001
                 ,	:new.extern_relatie);
         */
         NULL;
      END IF;
      /* QMS$DATA_AUDITING */
      :new.DAT_MUTATIE := SYSDATE;
      :new.MUTATIE_DOOR := alg_sessie.gebruiker;
      /* QMS$JOURNALLING */
      BEGIN
         INSERT INTO   rle_relatie_externe_codes_jn
            (      jn_user
            ,      jn_date_time
            ,      jn_operation
            ,      id
            ,      rle_numrelatie
            ,      dwe_wrddom_extsys
            ,      datingang
            ,      codorganisatie
            ,      rol_codrol
            ,      extern_relatie
            ,      ind_geverifieerd
            ,      dateinde
            ,      dat_creatie
            ,      creatie_door
            ,      dat_mutatie
            ,      mutatie_door
            )
         VALUES
            (      :new.mutatie_door
            ,      :new.dat_mutatie
            ,      'UPD'
            ,      :old.id
            ,      :old.rle_numrelatie
            ,      :old.dwe_wrddom_extsys
            ,      :old.datingang
            ,      :old.codorganisatie
            ,      :old.rol_codrol
            ,      :old.extern_relatie
            ,      :old.ind_geverifieerd
            ,      :old.dateinde
            ,      :old.dat_creatie
            ,      :old.creatie_door
            ,      :old.dat_mutatie
            ,      :old.mutatie_door
            );
      END;
   END IF;
   /* Publicatie voor synchronisatie */
   -- 12-11-2003, VER, alleen een publicatie bij A-nummer en SOFI-nummer
   -- 26-11-2003, VER, nogmaals uitvragen old- en new-values, want die worden
   --                  gedurende deze trigger gemanipuleerd.
   -- 10-11-2004 JWB uitbreiding ook BOVAG nummers synchroniseren
   -- 22-07-2005, VER, PR-UII, Consolidatie ZEUS/HERA, GBA- en SOFI-nummers worden niet meer gesynchroniseerd
   IF  (   :new.DWE_WRDDOM_EXTSYS in ('KVK','BOVAG')
       )
	 AND
       (   :old.ID                 <> :new.ID
        OR :old.RLE_NUMRELATIE     <> :new.RLE_NUMRELATIE
        OR :old.DWE_WRDDOM_EXTSYS  <> :new.DWE_WRDDOM_EXTSYS
        OR :old.DATINGANG          <> :new.DATINGANG
        OR :old.CODORGANISATIE     <> :new.CODORGANISATIE
        OR :old.ROL_CODROL         <> :new.ROL_CODROL
        OR :old.EXTERN_RELATIE     <> :new.EXTERN_RELATIE
        OR :old.IND_GEVERIFIEERD   <> :new.IND_GEVERIFIEERD
        OR :old.DATEINDE           <> :new.DATEINDE
	   )
   THEN
      IBM.PUBLISH_EVENT
         ( 'MUT RLE EXTERNE CODES'
         , 'U'
         , TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
         , :old.ID                                           , :new.ID
         , :old.RLE_NUMRELATIE                               , :new.RLE_NUMRELATIE
         , :old.DWE_WRDDOM_EXTSYS                            , :new.DWE_WRDDOM_EXTSYS
         , TO_CHAR(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , TO_CHAR(:new.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')
         , :old.CODORGANISATIE                               , :new.CODORGANISATIE
         , :old.ROL_CODROL                                   , :new.ROL_CODROL
         , :old.EXTERN_RELATIE                               , :new.EXTERN_RELATIE
         , :old.IND_GEVERIFIEERD                             , :new.IND_GEVERIFIEERD
         , TO_CHAR(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , TO_CHAR(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
         , TO_CHAR(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), TO_CHAR(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
         , :old.CREATIE_DOOR                                 , :new.CREATIE_DOOR
         , TO_CHAR(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), TO_CHAR(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
         , :old.MUTATIE_DOOR                                 , :new.MUTATIE_DOOR
         );
   END IF;

   -- wanneer een sofinummer is gewijzigd, publiceren van event BSNGewijzigd.
   -- 10-03-2010 AAM voor Nationaal Pensioen Register
   IF   ( :new.dwe_wrddom_extsys =   'SOFI' or
          :old.dwe_wrddom_extsys =   'SOFI')
         AND (  stm_dbproc.column_modified(:old.dwe_wrddom_extsys,:new.dwe_wrddom_extsys)
             OR stm_dbproc.column_modified(:old.extern_relatie,:new.extern_relatie)
             )
        then
        l_event := eventhandler.create_event(p_event_code => cn_event_BSNGewijzigd);
        eventhandler.add_num_value(p_event  => l_event
                                  ,p_key    => 'RELATIENUMMER'
                                  ,p_value  => :new.rle_numrelatie);
        eventhandler.add_date_value( p_event => l_event
                                   , p_key => 'DATUM_MUTATIE'
                                   , p_value => :new.dat_mutatie
                                   );
        eventhandler.publish_asynch_event(p_event => l_event);
   END IF;
   if (:new.dwe_wrddom_extsys = 'KVK'
     or :new.dwe_wrddom_extsys = 'WGR'
     or :new.dwe_wrddom_extsys = 'RSIN'
     or :old.dwe_wrddom_extsys = 'KVK'
     or :old.dwe_wrddom_extsys = 'WGR'
     or :old.dwe_wrddom_extsys = 'RSIN' )
     and :old.extern_relatie <> :new.extern_relatie
   then
      l_event := eventhandler.create_event(p_event_code => cn_event_WgrId);
      eventhandler.add_num_value(p_event  => l_event
                                ,p_key    => 'RELATIENUMMER'
                                ,p_value  => :new.rle_numrelatie);
      eventhandler.add_date_value( p_event => l_event
                                 , p_key => 'DATUM_MUTATIE'
                                 , p_value => :new.dat_mutatie
                                 );
      eventhandler.publish_asynch_event(p_event => l_event);
   end if;
--
  if (  (   :new.dwe_wrddom_extsys = 'BOVAG'
        and stm_dbproc.column_modified(:new.EXTERN_RELATIE,:old.EXTERN_RELATIE)
        )
     or (   stm_dbproc.column_modified(:new.DWE_WRDDOM_EXTSYS,:old.DWE_WRDDOM_EXTSYS)
        and ( :old.dwe_wrddom_extsys = 'BOVAG' or :new.dwe_wrddom_extsys = 'BOVAG' )
        )
     )
  then
    l_event := eventhandler.create_event( p_event_code => 'wgr.GewijzigdBovagNummerWerkgever' );
    eventhandler.add_num_value( p_event  => l_event
                              , p_key    => 'RELATIENUMMER'
                              , p_value  => :new.rle_numrelatie
                              );
    eventhandler.add_date_value( p_event => l_event
                               , p_key => 'DATUM_MUTATIE'
                               , p_value => :new.dat_mutatie
                               );
    eventhandler.publish_asynch_event( p_event => l_event );
  end if;
END IF;
END ;
/