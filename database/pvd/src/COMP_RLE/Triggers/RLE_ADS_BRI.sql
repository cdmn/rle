CREATE OR REPLACE TRIGGER comp_rle.RLE_ADS_BRI
 BEFORE INSERT
 ON comp_rle.RLE_ADRESSEN
 FOR EACH ROW
-- RLE_ADS_BRI
BEGIN
if RLE_CHK_LND_BLOCKED(:new.lnd_codland )
then
   raise_application_error(-20000, 'RLE-10386');
end if;


if :new.codorganisatie is null
then
   :new.codorganisatie := 'A';
end if;
RLE_ADS_CHK_VERPL( :new.stt_straatid
       , :new.postcode
       , :new.huisnummer
       , :new.wps_woonplaatsid
       , :new.lnd_codland);
--
-- als batches postcode mutatie en/of vernummering voor
-- adres insert/mutatie zorgt, dan deze checks niet
--
if nvl( rle_npe_pck.g_ind_npe_mutatie, 'N') <> 'J'
then
   RLE_ADS_CHK_WPS( :new.wps_woonplaatsid
                  , :new.lnd_codland);
   RLE_ADS_CHK_STRWPL( :new.wps_woonplaatsid
                     , :new.stt_straatid);
--
   if :new.lnd_codland = stm_algm_get.sysparm( NULL
                                             , 'LAND'
                                             , sysdate)  AND
      :new.postcode is not null
   then
      RLE_NPE_CHK_AANW( :new.lnd_codland
                      , :new.huisnummer
                      , :new.postcode);
   end if;
end if;
--
if :new.numadres is null
then
   :new.numadres := rle_ads_seq1_next_id;
end if;
-- QMS$DATA_AUDITING
:new.DAT_CREATIE  := sysdate;
:new.CREATIE_DOOR := alg_sessie.gebruiker;
:new.DAT_MUTATIE  := sysdate;
:new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_ADS_BRI;
/