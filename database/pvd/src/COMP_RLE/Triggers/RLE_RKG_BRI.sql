CREATE OR REPLACE TRIGGER comp_rle.RLE_RKG_BRI
 BEFORE INSERT
 ON comp_rle.RLE_ROL_IN_KOPPELINGEN
 FOR EACH ROW

BEGIN
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_RKG_BRI;
/