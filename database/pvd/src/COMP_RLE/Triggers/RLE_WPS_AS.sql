CREATE OR REPLACE TRIGGER comp_rle.RLE_WPS_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_WOONPLAATSEN
DECLARE
/*****************************************************************************************************/
/* Trigger RLE.RLE_WPS_AS:                                                                           */
/*                                                                                                   */
/*                                                                                                   */
/* Datum      Wie         Omschrijving                                                               */
/* ---------- ----------- ---------------------------------------------------------------------------*/
/* 02-09-2016 DFU         Creatie voor mutaties naar personen EVENT STORE                            */
/* 05-09-2016 OKR         old_woonplaats toegevoegd om schijnmutaties  af te vangen                  */
/*****************************************************************************************************/

   l_stack            VARCHAR2( 128 ) ;
   l_rowid            ROWID           ;
   l_job              VARCHAR2( 1 )   ;
   l_woonplaatsid     rle_woonplaatsen.woonplaatsid%type;
   l_lnd_codland      rle_woonplaatsen.lnd_codland%type;
   l_woonplaats       rle_woonplaatsen.woonplaats%type;
   l_old_woonplaats   rle_woonplaatsen.woonplaats%type;
   --
BEGIN
   --
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   --
   WHILE l_rowid IS NOT NULL
   LOOP
     /* Ophalen standaard variabelen */
     l_job := stm_get_word( l_stack ) ;

     /* Ophalen variabelen per soort statement */
     IF    l_job in ('U')
     THEN
       /* Ophalen variabelen voor update */

       l_woonplaatsid   := stm_get_word( l_stack ) ;
       l_lnd_codland    := stm_get_word( l_stack ) ;
       l_woonplaats     := stm_get_word( l_stack ) ;
       l_old_woonplaats := stm_get_word( l_stack ) ;

     END IF ;

      /* Verwerking */

      --
      -- Wijzigingen van WOONPLAATSEN moeten - mogelijk - worden gecommuniceerd naar de personen EVENT STORE
      -- Hier loggen we simpelweg updates WOONPLAATSEN. Een subscriber beoordeelt of de UPDATE gecommuniceerd moet worden
      -- We hoeven alleen UPDATES te loggen. DELETES en INSERTS worden al getracked via RELATIE ADRESSEN
      if l_job in ('U')
        and not l_woonplaats = l_old_woonplaats -- schijnmutaties niet loggen naar event store
      then
        appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_wps(p_event_title => 'Adres gewijzigd', p_woonplaatsid => l_woonplaatsid);
        --
      end if;
      --
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
      --
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      RAISE ;
END ;
/