CREATE OR REPLACE TRIGGER comp_rle.RLE_ROL_BDR
 BEFORE DELETE
 ON comp_rle.RLE_ROLLEN
 FOR EACH ROW

BEGIN
/* RLE_ROL_BDR */
rle_rol_chk_wijz(:old.codrol,:old.indwijzigbaar);
END RLE_ROL_BDR;
/