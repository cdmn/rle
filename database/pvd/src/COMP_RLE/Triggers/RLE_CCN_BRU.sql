CREATE OR REPLACE TRIGGER comp_rle.RLE_CCN_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_COMMUNICATIE_CATEGORIEEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN


/* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_MPE_BRU;
/