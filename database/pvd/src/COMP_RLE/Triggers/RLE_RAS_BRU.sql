CREATE OR REPLACE TRIGGER comp_rle.RLE_RAS_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_RELATIE_ADRESSEN
 FOR EACH ROW
DECLARE


-- Program Data
L_WOONPLAATS VARCHAR2(40);
L_LAND VARCHAR2(40);
L_POSTCODE VARCHAR2(15);
L_HUISNR NUMBER(10);
L_TOEVNUM VARCHAR2(30);
L_STRAAT VARCHAR2(40);
l_datoverlijden rle_relaties.datoverlijden%type;
l_indinstelling rle_relaties.indinstelling%type;
l_ras_einddatum_ter_controle date;
--
-- PL/SQL Block
BEGIN
--
-- igv 'null' mutatie niets doen  ... bevinding 858
-- deze mutaties komen veel vanuit GBA
--
   IF stm_dbproc.column_modified(:new.PROVINCIE,:old.PROVINCIE) OR
      stm_dbproc.column_modified(:new.RLE_NUMRELATIE,:old.RLE_NUMRELATIE) OR
      stm_dbproc.column_modified(:new.ROL_CODROL,:old.ROL_CODROL) OR
      stm_dbproc.column_modified(:new.DATINGANG,:old.DATINGANG) OR
      stm_dbproc.column_modified(:new.DATEINDE,:old.DATEINDE) OR
      stm_dbproc.column_modified(:new.NUMKAMER,:old.NUMKAMER) OR
      stm_dbproc.column_modified(:new.DWE_WRDDOM_SRTADR,:old.DWE_WRDDOM_SRTADR) OR
      stm_dbproc.column_modified(:new.LOCATIE,:old.LOCATIE) OR
      stm_dbproc.column_modified(:new.IND_WOONBOOT,:old.IND_WOONBOOT) OR
      stm_dbproc.column_modified(:new.IND_WOONWAGEN,:old.IND_WOONWAGEN) OR
      stm_dbproc.column_modified(:new.ID,:old.ID) OR
      stm_dbproc.column_modified(:new.CODORGANISATIE,:old.CODORGANISATIE) OR
      stm_dbproc.column_modified(:new.ADS_NUMADRES,:old.ADS_NUMADRES) OR
      stm_dbproc.column_modified(:new.INDINHOWN,:old.INDINHOWN)
   THEN
      /* BR IER0024: Werkgever 000001 mag niet gemuteerd of verwijderd worden */
      rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);
      /* BRIER0004
      ** Controle rol bij communicatienummer
      */
      IF stm_dbproc.column_modified( :new.rol_codrol, :old.rol_codrol )
      THEN
         IF :new.rol_codrol IS NOT NULL
         THEN
            rle_rrl_chk_fk( :new.rle_numrelatie
                          , :new.rol_codrol
                          ) ;
         END IF ;
      END IF ;
      /* END: BRIER0004 */
      /* BRIER????? /BR MOD0003
        "Postbus" of "Antwoordnummer" alleen toegestaan bij "CA"
        (Correspondentie Adres)
      */
      IF    stm_dbproc.column_modified( :new.ads_numadres, :old.ads_numadres)
         OR stm_dbproc.column_modified( :new.dwe_wrddom_srtadr, :old.dwe_wrddom_srtadr)
      THEN
         /* Controle referentiewaarde
         */
         rfe_chk_refwrd( 'SAD', :new.dwe_wrddom_srtadr );
          --
         IF      :new.dwe_wrddom_srtadr != 'CA'
            AND (   rle_get_straatnaam_ads( :new.ads_numadres) = 'ANTWOORDNUMMER'
                 OR rle_get_straatnaam_ads( :new.ads_numadres) = 'POSTBUS'
                )
         THEN
            stm_dbproc.raise_error( 'RLE-10290' ) ;
         END IF ;
      END IF ;
      /* END: BRIER????? */
      /* BRIER0016
      **   Overlap relatie adres t.o.v. relatie
      */
      IF stm_dbproc.column_modified( :new.datingang
                                   , :old.datingang
                                   )
      THEN
         rle_chk_rle_persdat( :new.rle_numrelatie
                            , :new.datingang
                            , 'relatieadressen'
                            ) ;
      END IF ;
      /* END: BIER0016 */
      IF    stm_dbproc.column_modified(:new.dateinde, :old.dateinde)
         OR stm_dbproc.column_modified(:new.datingang, :old.datingang)
         OR stm_dbproc.column_modified(:new.rle_numrelatie, :old.rle_numrelatie)
      THEN
         -- xjb, 06-05-2015, Check op einddatum in geval van overlijden van een persoon niet meer doen, n.a.l.v. Jira 409 (bug)
         --                  Adressen worden nu altijd opgevoerd per systeemdatum en beeindigd per systeemdatum -1
         --                  Dit hoeft niet meer per se op of voor de overlijdensdatum te liggen

         if :new.dateinde is not null
         then
           l_ras_einddatum_ter_controle := :new.dateinde;

           select rle.datoverlijden, rle.indinstelling
           into   l_datoverlijden, l_indinstelling
           from  rle_relaties rle
           where  rle.numrelatie = :new.rle_numrelatie;

           if l_indinstelling <> 'J' and l_datoverlijden is not null
           then
             if :new.dateinde > l_datoverlijden
             then
               l_ras_einddatum_ter_controle := l_datoverlijden;
             end if;
           end if;
         end if;

         rle_rle_childper(:new.rle_numrelatie
                         ,:new.datingang
                         ,l_ras_einddatum_ter_controle
                         );
      END IF;
      /* QMS$DATA_AUDITING */
      :new.DAT_MUTATIE := SYSDATE;
      :new.MUTATIE_DOOR := alg_sessie.gebruiker;
      /* QMS$JOURNALLING */
      BEGIN
         INSERT
         INTO rle_relatie_adressen_jn
            ( jn_user
            , jn_date_time
            , jn_operation
            , id
            , datingang
            , rle_numrelatie
            , dwe_wrddom_srtadr
            , ads_numadres
            , codorganisatie
            , indinhown
            , rol_codrol
            , numkamer
            , dateinde
            , locatie
            , ind_woonboot
            , ind_woonwagen
            , dat_creatie
            , creatie_door
            , dat_mutatie
            , mutatie_door
            )
         VALUES
            ( :new.mutatie_door
            , :new.dat_mutatie
            , 'UPD'
            , :old.id
            , :old.datingang
            , :old.rle_numrelatie
            , :old.dwe_wrddom_srtadr
            , :old.ads_numadres
            , :old.codorganisatie
            , :old.indinhown
            , :old.rol_codrol
            , :old.numkamer
            , :old.dateinde
            , :old.locatie
            , :old.ind_woonboot
            , :old.ind_woonwagen
            , :old.dat_creatie
            , :old.creatie_door
            , :old.dat_mutatie
            , :old.mutatie_door
            );


      END;
      --
      -- 12-11-2003, VER, SYN/PARO: adres-mutaties van personen met binnenlands adres alleen publiceren bij GBA-status OV
      -- 22-07-2005, VER, PR-UII, Consolidatie ZEUS/HERA, adressen behorend bij rol 'PR' worden niet meer gesynchroniseerd
      -- 27-02-2006, NKU, project Levensloop: adressen voor niet-MTB deelnemers moeten
      --                   niet gesynchroniseerd worden, niet-MTB werkgevers wel tbv CODA (rol WB en WD).
      -- 27-02-2006 SKL chg 48779: OA adres mag niet gesynchroniseerd worden.
      -- 02-02-2009, XMB  Project Levensloop. Niet MTB deelnemers hebben nu rol 'PR'.
      --
      if :old.rol_codrol != 'PR'
      or   :new.dwe_wrddom_srtadr <> 'OA'
      then
         --dbms_output.put_line('pub event'); xcw
         IBM.PUBLISH_EVENT
            ( 'MUT RLE ADRESSEN'
            , 'U'
            , TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
            , :old.PROVINCIE                                    , :new.PROVINCIE
            , :old.ID                                           , :new.ID
            , TO_CHAR(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , TO_CHAR(:new.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')
            , :old.RLE_NUMRELATIE                               , :new.RLE_NUMRELATIE
            , :old.DWE_WRDDOM_SRTADR                            , :new.DWE_WRDDOM_SRTADR
            , :old.ADS_NUMADRES                                 , :new.ADS_NUMADRES
            , :old.CODORGANISATIE                               , :new.CODORGANISATIE
            , :old.INDINHOWN                                    , :new.INDINHOWN
            , :old.ROL_CODROL                                   , :new.ROL_CODROL
            , :old.NUMKAMER                                     , :new.NUMKAMER
            , TO_CHAR(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , TO_CHAR(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
            , :old.LOCATIE                                      , :new.LOCATIE
            , :old.IND_WOONBOOT                                 , :new.IND_WOONBOOT
            , :old.IND_WOONWAGEN                                , :new.IND_WOONWAGEN
            , TO_CHAR(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), TO_CHAR(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
            , :old.CREATIE_DOOR                                 , :new.CREATIE_DOOR
            , TO_CHAR(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), TO_CHAR(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
            , :old.MUTATIE_DOOR                                 , :new.MUTATIE_DOOR
            );
      end if;

      /**
      ** 17-03-2016 rln jira MNGBA-1008
      ** Onderstaande code is een kort-door-de-bocht oplossing voor het bijwerken van de DER-datum bij een verhuizing van of naar het buitenland
      **
      */
      declare
         cursor c_ads_btl(b_numadres rle_adressen.numadres%type)
         is
           select 1
           from   rle_adressen ads
           where  ads.numadres = b_numadres
           and    ads.lnd_codland != 'NL'
         ;
         cursor c_asa (b_rle_numrelatie rle_afstemmingen_gba.rle_numrelatie%type)
         is
            select asa.code_gba_status
            ,      asa.afnemers_indicatie
            from   rle_afstemmingen_gba asa
           where  asa.rle_numrelatie = :new.rle_numrelatie
         ;
         r_asa    c_asa%rowtype;
         l_dummy  pls_integer;
      begin
         open c_asa(:new.rle_numrelatie);
         fetch c_asa into r_asa;
         close c_asa;

         open c_ads_btl(:new.ads_numadres);
         fetch c_ads_btl into l_dummy;
         if c_ads_btl%found
         then
            ibm.publish_event ( 'MUT RLE AFSTEMMINGEN GBA'
                              , 'U'
                              , to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss')
                              , :old.creatie_door , :new.creatie_door
                              , to_char(:old.dat_creatie, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.dat_creatie, 'dd-mm-yyyy hh24:mi:ss')
                              , :old.mutatie_door , :new.mutatie_door
                              , to_char(:old.dat_mutatie, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.dat_mutatie, 'dd-mm-yyyy hh24:mi:ss')
                              , r_asa.code_gba_status , r_asa.code_gba_status
                              , 'X' , r_asa.afnemers_indicatie -- forceren opnieuw bepalen der-datum
                              , :old.rle_numrelatie , :new.rle_numrelatie
                              );
          end if;
          close c_ads_btl;
      end;
   END IF;
END RLE_RAS_BRU;
/