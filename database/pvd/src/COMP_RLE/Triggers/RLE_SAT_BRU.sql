CREATE OR REPLACE TRIGGER comp_rle.RLE_SAT_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_SANCTIELIJSTEN
 FOR EACH ROW
/* QMS$DATA_AUDITING */
BEGIN
:new.dat_mutatie  := SYSDATE;
  :new.mutatie_door := alg_sessie.gebruiker;
END;
/