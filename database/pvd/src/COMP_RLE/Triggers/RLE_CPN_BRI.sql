CREATE OR REPLACE TRIGGER comp_rle.RLE_CPN_BRI
 BEFORE INSERT
 ON comp_rle.RLE_CONTACTPERSONEN
 FOR EACH ROW

BEGIN
   /* Controle code_functie */
   IF :new.code_functie IS NOT NULL
   THEN
      ibm.ibm( 'CHK_REFWRD'
             , 'RLE'
             , 'FTE'
             , :new.code_functie
             ) ;
   END IF ;
   /* Controle code_soort_contactpersoon */
   IF :new.code_soort_contactpersoon IS NOT NULL
   THEN
      ibm.ibm( 'CHK_REFWRD'
             , 'RLE'
             , 'SCN'
             , :new.code_soort_contactpersoon
             ) ;
   END IF ;
   /* QMS$DATA_AUDITING */
   :new.DAT_CREATIE := SYSDATE;
   :new.CREATIE_DOOR := alg_sessie.gebruiker;
   :new.DAT_MUTATIE := SYSDATE;
   :new.MUTATIE_DOOR := alg_sessie.gebruiker;
   /* Publicatie voor synchronisatie */
   IBM.PUBLISH_EVENT( 'MUT RLE CONTACTPERSONEN'
                    , 'I'
                    , TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
                    , NULL, :new.RAS_ID
                    , NULL, :new.RLE_NUMRELATIE
                    , NULL, :new.CODE_FUNCTIE
                    , NULL, :new.CODE_SOORT_CONTACTPERSOON
                    , NULL, :new.CREATIE_DOOR
                    , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
                    , NULL, :new.MUTATIE_DOOR
                    , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
                    ) ;
END ;
/