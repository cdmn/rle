CREATE OR REPLACE TRIGGER comp_rle.RLE_FNV_BRU
 INSTEAD OF UPDATE
 ON comp_rle.RLE_V_FNR_0080F
begin

   if :old.codwijzebetaling != :new.codwijzebetaling
    then

         update rle_financieel_nummers fnr
         set fnr.dateinde = sysdate - 1
         where fnr.id = :old.id;

         insert
         into   rle_financieel_nummers
             (      rle_numrelatie
             ,      numrekening
             ,      lnd_codland
             ,      dwe_wrddom_srtrek
             ,      datingang
             ,      indinhown
             ,      dateinde
             ,      indfiat
             ,      rol_codrol
             ,      codorganisatie
             ,      tenaamstelling
             ,      coddoelrek
             ,      codwijzebetaling
             ,      BIC
             ,      IBAN
             )

        values
             (      :old.rle_numrelatie
             ,      :old.numrekening
             ,      :old.lnd_codland
             ,      :old.dwe_wrddom_srtrek
             ,      sysdate
             ,      :old.indinhown
             ,      null
             ,      :old.indfiat
             ,      :old.rol_codrol
             ,      :old.codorganisatie
             ,      :old.tenaamstelling
             ,      :old.coddoelrek
             ,      :new.codwijzebetaling
             ,      :old.BIC
             ,      :old.IBAN
             );

    else

         update rle_financieel_nummers fnr
         set fnr.dateinde = :new.dateinde
         where fnr.id = :old.id;

    end if;

end;
/