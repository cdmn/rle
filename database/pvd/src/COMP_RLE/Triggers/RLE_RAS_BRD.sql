CREATE OR REPLACE TRIGGER comp_rle.RLE_RAS_BRD
 BEFORE DELETE
 ON comp_rle.RLE_RELATIE_ADRESSEN
 FOR EACH ROW
BEGIN
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
   rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);

/* Oude code

-- RLE_RAS_BRD
stm_dbproc.raise_error('RLE-00610','','RLE_RAS_BRD');
*/

/* BRDEL0001
   Adres mag alleen verwijderd worden als het geen
   statutair (SZ) of domicilie (DA) adres betreft
   >> PKG: toegevoegd ivm RRGG: OA adres
   mag ook niet verwijderd worden.
   <<
*/
IF :old.dwe_wrddom_srtadr IN ( 'SZ', 'DA' , 'OA')
THEN
    stm_dbproc.raise_error( 'RLE-10283' ) ;
END
IF ;
--
-- 12-11-2003, VER, bij rol PR en binnenlands adres alleen een publicatie indien gba-status = 'OV'
-- 22-07-2005, VER, PR-UII, Consolidatie ZEUS/HERA: de adressen bij de rol 'PR' worden niet meer gesynchroniseerd
-- 27-02-2006, NKU, project Levensloop: adressen voor niet-MTB deelnemers moeten
--                  niet gesynchroniseerd worden, niet-MTB werkgevers wel tbv CODA (rol WB en WD).
-- 02-02-2009, XMB  Project Levensloop. Niet MTB deelnemers hebben nu rol 'PR'.
if :old.rol_codrol != 'PR'
then
   IBM.PUBLISH_EVENT
      ( 'MUT RLE ADRESSEN'
      , 'D'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.PROVINCIE                                    , NULL
      , :old.ID                                           , NULL
      , to_char(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , NULL
      , :old.RLE_NUMRELATIE                               , NULL
      , :old.DWE_WRDDOM_SRTADR                            , NULL
      , :old.ADS_NUMADRES                                 , NULL
      , :old.CODORGANISATIE                               , NULL
      , :old.INDINHOWN                                    , NULL
      , :old.ROL_CODROL                                   , NULL
      , :old.NUMKAMER                                     , NULL
      , to_char(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , NULL
      , :old.LOCATIE                                      , NULL
      , :old.IND_WOONBOOT                                 , NULL
      , :old.IND_WOONWAGEN                                , NULL
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.CREATIE_DOOR                                 , NULL
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.MUTATIE_DOOR                                 , NULL
      );
end if;
END RLE_RAS_BRD;
/