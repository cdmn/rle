CREATE OR REPLACE TRIGGER comp_rle.RLE_USS_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_UITVOER_STATUSSEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE

   l_stack   varchar2 (128);
begin
   if inserting
   then
      stm_add_word (l_stack, 'I');
      stm_algm_muttab.add_rowid (:new.rowid, l_stack);
   elsif updating
   then
      stm_add_word (l_stack, 'U');
      stm_algm_muttab.add_rowid (:new.rowid, l_stack);
   elsif deleting
   then
      stm_add_word (l_stack, 'D');
      stm_add_word (l_stack, :old.bsd_naam);
      stm_add_word (l_stack, :old.vanaf_datum);
      stm_algm_muttab.add_rowid (:old.rowid, l_stack);
   end if;
exception
   when others
   then
      stm_algm_muttab.clear_array;
      raise;
END RLE_USS_AR;
/