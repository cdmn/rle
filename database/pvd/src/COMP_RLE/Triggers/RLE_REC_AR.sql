CREATE OR REPLACE TRIGGER comp_rle.RLE_REC_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIE_EXTERNE_CODES
 FOR EACH ROW

DECLARE
-- Program Data
L_STACK VARCHAR2(128);
-- PL/SQL Block
BEGIN
/* RLE_REC_AR */
/* Per rij de bewerking meegeven */
IF  inserting
THEN
   stm_add_word (l_stack, 'I');
   stm_add_word (l_stack, :new.rle_numrelatie);
   stm_add_word (l_stack, :new.extern_relatie);
   stm_add_word (l_stack, :new.dwe_wrddom_extsys) ;
ELSIF  updating
THEN
   stm_add_word (l_stack, 'U');
   stm_add_word (l_stack, :new.rle_numrelatie);
   stm_add_word (l_stack, :new.extern_relatie);
   stm_add_word (l_stack, :new.dwe_wrddom_extsys);
   stm_add_word (l_stack, :old.extern_relatie);
   stm_add_word( l_stack, TO_CHAR( :old.datingang ) ) ;
ELSIF deleting
THEN
   stm_add_word (l_stack, 'D');
   stm_add_word( l_stack, :old.rol_codrol ) ;
   stm_add_word( l_stack, :old.rle_numrelatie ) ;
   stm_add_word( l_stack, TO_CHAR( :old.datingang ) ) ;
   stm_add_word( l_stack, TO_CHAR( :old.dateinde ) ) ;
   stm_add_word( l_stack, :old.dwe_wrddom_extsys ) ;
   /* QMS$JOURNALLING */
   BEGIN
   INSERT
   INTO   rle_relatie_externe_codes_jn
   (      jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      id
   ,      rle_numrelatie
   ,      dwe_wrddom_extsys
   ,      datingang
   ,      codorganisatie
   ,      rol_codrol
   ,      extern_relatie
   ,      ind_geverifieerd
   ,      dateinde
   ,      dat_creatie
   ,      creatie_door
   ,      dat_mutatie
   ,      mutatie_door
   )
   VALUES
   (      alg_sessie.gebruiker
   ,      SYSDATE
   ,      'DEL'
   ,      :old.id
   ,      :old.rle_numrelatie
   ,      :old.dwe_wrddom_extsys
   ,      :old.datingang
   ,      :old.codorganisatie
   ,      :old.rol_codrol
   ,      :old.extern_relatie
   ,      :old.ind_geverifieerd
   ,      :old.dateinde
   ,      :old.dat_creatie
   ,      :old.creatie_door
   ,      :old.dat_mutatie
   ,      :old.mutatie_door
   );
   END;
END IF;
/* per rij de benodigde old values meegeven */
/* vullen rowid en linkstring in de interne tabel */
IF  deleting THEN
   stm_algm_muttab.add_rowid(:old.rowid, l_stack);
ELSE
   stm_algm_muttab.add_rowid(:new.rowid, l_stack);
END IF;
END RLE_REC_AR;
/