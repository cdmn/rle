CREATE OR REPLACE TRIGGER comp_rle.RLE_ADS_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_ADRESSEN
DECLARE
/*****************************************************************************************************/
/* Trigger RLE.RLE_ADS_AS:                                                                           */
/*                                                                                                   */
/*                                                                                                   */
/* Datum      Wie         Omschrijving                                                               */
/* ---------- ----------- ---------------------------------------------------------------------------*/
/* 15-10-2010 UKM         Meldingsnummer: PRD-INC-186820                                             */
/*                        Trigger aangepast eerder(19-03-2010) door UKV, verder niet opgeleverd in   */
/*                        PVCS maar komt overeen met mijn aapassingen. Deze zijn:                    */
/*                        Cursor c_ads aangepast. Parameter b_stt_straatid, b_wps_woonplaatsid       */
/*                        en condities toegevoegd. l_stt_straatid en l_wps_woonplaatsid gedeclareerd.*/
/*                        Deze worden gevuld met een waarde bij het ophalen van de variabelen en     */
/*                        meegegeven bij het openen van de cursor.                                   */
/*                        UKV heeft de code ook nog netjes gemaakt. UKM gaat ermee akkoord!          */
/* 01-09-2016 OKR         Eventstore: bij update old values uitlezen vd stack voor publicatie        */
/*****************************************************************************************************/

   l_stack  VARCHAR2( 2000 ) ;
   l_rowid  ROWID           ;
   l_job    VARCHAR2( 1 )   ;
   l_codland          rle_adressen.lnd_codland%TYPE ;
   l_postcode         rle_adressen.postcode%TYPE ;
   l_stt_straatid     rle_adressen.stt_straatid%type;
   l_wps_woonplaatsid rle_adressen.wps_woonplaatsid%type;
   l_huisnummer       rle_adressen.huisnummer%TYPE ;
   l_toevhuisnum      rle_adressen.toevhuisnum%TYPE ;
   l_numadres         rle_adressen.numadres%TYPE;
   l_old_codland          rle_adressen.lnd_codland%TYPE ;
   l_old_postcode         rle_adressen.postcode%TYPE ;
   l_old_stt_straatid     rle_adressen.stt_straatid%type;
   l_old_wps_woonplaatsid rle_adressen.wps_woonplaatsid%type;
   l_old_huisnummer       rle_adressen.huisnummer%TYPE ;
   l_old_toevhuisnum      rle_adressen.toevhuisnum%TYPE ;
   l_old_numadres         rle_adressen.numadres%TYPE;
   --
   cursor c_ads
      ( b_postcode         in rle_adressen.postcode%type
      , b_wps_woonplaatsid in rle_adressen.wps_woonplaatsid%type
      , b_stt_straatid     in rle_adressen.stt_straatid%type
      , b_huisnummer       in rle_adressen.huisnummer%type
      , b_toevhuisnum      in rle_adressen.toevhuisnum%type
      , b_codland          in rle_adressen.lnd_codland%type
      , b_numadres         in rle_adressen.numadres%type
      ) is
      select ('x')
      from   rle_adressen  ads
      where  ads.postcode         = b_postcode
      and    ads.wps_woonplaatsid = b_wps_woonplaatsid
      and    ads.stt_straatid     = b_stt_straatid
      and    ads.huisnummer       = b_huisnummer
      and  ( ads.toevhuisnum      = b_toevhuisnum or
           ( ads.toevhuisnum     is null and b_toevhuisnum is null ))
      and    ads.lnd_codland      = b_codland
      and    ads.numadres        != b_numadres
      ;
   l_count_ads  VARCHAR2( 10 ) ;
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;
      /* Ophalen variabelen per soort statement */
      IF    l_job = 'I'
      THEN
         /* Ophalen variabelen voor insert of update*/
         l_codland          := stm_get_word( l_stack ) ;
         l_postcode         := stm_get_word( l_stack ) ;
         l_wps_woonplaatsid := stm_get_word( l_stack ) ;
         l_stt_straatid     := stm_get_word( l_stack ) ;
         l_huisnummer       := stm_get_word( l_stack ) ;
         l_toevhuisnum      := stm_get_word( l_stack ) ;
         l_numadres         := stm_get_word( l_stack ) ;
       ELSIF l_job = 'U'
       THEN
         /* Ophalen variabelen voor insert of update*/
         l_codland          := stm_get_word( l_stack ) ;
         l_postcode         := stm_get_word( l_stack ) ;
         l_wps_woonplaatsid := stm_get_word( l_stack ) ;
         l_stt_straatid     := stm_get_word( l_stack ) ;
         l_huisnummer       := stm_get_word( l_stack ) ;
         l_toevhuisnum      := stm_get_word( l_stack ) ;
         l_numadres         := stm_get_word( l_stack ) ;
         --OKR
         l_old_codland          := stm_get_word( l_stack ) ;
         l_old_postcode         := stm_get_word( l_stack ) ;
         l_old_wps_woonplaatsid := stm_get_word( l_stack ) ;
         l_old_stt_straatid     := stm_get_word( l_stack ) ;
         l_old_huisnummer       := stm_get_word( l_stack ) ;
         l_old_toevhuisnum      := stm_get_word( l_stack ) ;
         l_old_numadres         := stm_get_word( l_stack ) ;
       END IF ;
      /* Verwerking */
      IF l_job IN ( 'I', 'U' )
      THEN
         /* BRENT0001
            Controle dubbel adres
         */
         IF l_codland = 'NL'
         THEN
            OPEN c_ads( b_postcode         => l_postcode
                      , b_wps_woonplaatsid => l_wps_woonplaatsid
                      , b_stt_straatid     => l_stt_straatid
                      , b_huisnummer       => l_huisnummer
                      , b_toevhuisnum      => l_toevhuisnum
                      , b_codland          => l_codland
                      , b_numadres         => l_numadres
                      ) ;
            FETCH c_ads INTO l_count_ads ;
            IF c_ads%FOUND
            THEN
               CLOSE c_ads ;
               stm_dbproc.raise_error( 'RLE-10284' ) ;
            ELSE
               CLOSE c_ads;
            END IF ;
         END IF ;
      END IF ;
      --
      -- OKR: Wijzigingen van ADRESSEN moeten - mogelijk - worden gecommuniceerd naar de personen EVENT STORE
      -- Hier loggen we simpelweg updates van ADRESSEN. Een subscriber beoordeelt of de UPDATE gecommuniceerd moet worden
      -- We hoeven alleen UPDATES te loggen. DELETES en INSERTS worden al getracked via RELATIE ADRESSEN
      if l_job in ('U')
         and not -- schijnmutaties niet loggen naar event store
         (
          nvl(to_char(l_stt_straatid), '__null__') = nvl(to_char(l_old_stt_straatid), '__null__')
          and
          nvl(to_char(l_wps_woonplaatsid), '__null__') = nvl(to_char(l_old_wps_woonplaatsid), '__null__')
          and
          nvl(l_codland, '__null__') = nvl(l_old_codland, '__null__')
          and
          nvl(to_char(l_huisnummer), '__null__') = nvl(to_char(l_old_huisnummer), '__null__')
          and
          nvl(l_postcode, '__null__') = nvl(l_old_postcode, '__null__')
          and
          nvl(l_toevhuisnum, '__null__') = nvl(l_old_toevhuisnum, '__null__')
         )
      then
          appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_ads(p_event_title => 'Adres gewijzigd', p_numadres => l_numadres);
          --
      end if;
      --
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
      --
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      RAISE ;
END ;
/