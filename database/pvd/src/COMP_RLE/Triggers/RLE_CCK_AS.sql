CREATE OR REPLACE TRIGGER comp_rle.RLE_CCK_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_COMMUNICATIE_CAT_KANALEN
DECLARE

-- Program Data
L_OLDNUMCOMMUNICATIE VARCHAR2(50);
L_ROWID ROWID;
L_STACK VARCHAR2(128);
L_OLDCCNID NUMBER(20);
L_JOB VARCHAR2(1);
L_OLDDATUMBEGIN DATE;
L_OLDDATUMEINDE DATE;
L_OLDSRTCOM VARCHAR2(30);
-- PL/SQL Specification
/* RLE_CCK_AS */
  CURSOR c_cck_000(
         b_rowid ROWID
         )
  IS
  SELECT ID,
         CCN_ID,
         DWE_WRDDOM_SRTCOM,
         DATUM_BEGIN,
         DATUM_EINDE,
		 IND_DEFAULT
  FROM   rle_communicatie_cat_kanalen cck
  WHERE  cck.rowid = b_rowid
  ;
  r_cck_000 c_cck_000%rowtype ;
  /* C_CCK_003: Ophalen van communicatie cat kanaal op numrelatie ccn_id, dwe_wrddom_srt
  */
  CURSOR C_CCK_003
       ( b_srtcom     IN VARCHAR2
       , b_ccn_id     IN NUMBER
       )
  IS
  SELECT id,
         DATUM_BEGIN,
         DATUM_EINDE
    FROM rle_communicatie_cat_kanalen cck
   WHERE cck.dwe_wrddom_srtcom   = b_srtcom
     AND cck.ccn_id              = b_ccn_id
  ;
  /* Check aantal keren default  per categorie*/
  cursor c_cck_004 ( b_id in number, b_ccn_id in number )
  is
    select 'x'
    from   rle_communicatie_cat_kanalen cck
    where  cck.id <> b_id
    and    cck.ccn_id = b_ccn_id
    and    cck.ind_default = 'J';

  r_cck_004           c_cck_004%rowtype;
-- PL/SQL Block
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   open c_cck_000 ( l_rowid );
   fetch c_cck_000
   into   r_cck_000;

   close c_cck_000;
   /* Check aantal keren default  per categorie*/
   if r_cck_000.ind_default = 'J'
   then
     open c_cck_004 ( r_cck_000.id, r_cck_000.ccn_id );
     fetch c_cck_004
     into   r_cck_004;

     if c_cck_004%found
     then
	   close c_cck_004;
       raise_application_error ( -20000, 'Er kan slechts één kanaal default zijn' );
     end if;
     close c_cck_004;
   end if;


   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;
      /*  Ophalen variabelen per soort statement */
      IF l_job <> 'I'
      THEN
         l_olddatumbegin      := TO_DATE( stm_get_word( l_stack ) ) ;
         l_olddatumeinde      := TO_DATE( stm_get_word( l_stack ) ) ;
         l_oldsrtcom          := stm_get_word (l_stack ) ;
         l_oldccnid           := TO_NUMBER( stm_get_word( l_stack ) ) ;
      END IF;
      /* Zoek betreffende record indien mogelijk */
      IF l_job <> 'D'
      THEN
         OPEN c_cck_000( l_rowid ) ;
         FETCH c_cck_000 INTO r_cck_000 ;
         CLOSE c_cck_000;
      END IF;
      /* Bij toevoegen nieuw voorkomen, afsluiten voorliggende voorkomen. */
      IF l_job = 'I'
      THEN
         UPDATE rle_communicatie_cat_kanalen cck
         SET    cck.datum_einde  = r_cck_000.datum_begin - 1
         WHERE  cck.id                        <> r_cck_000.id
         AND    cck.dwe_wrddom_srtcom         =  r_cck_000.dwe_wrddom_srtcom
         AND    cck.ccn_id                    =  r_cck_000.ccn_id
         AND    cck.datum_einde               IS NULL
         ;
      END IF;
      /* Bij wijzigen begindatum */
      IF l_job          =  'U'                  AND
         l_olddatumbegin <> r_cck_000.datum_begin
      THEN
         UPDATE rle_communicatie_cat_kanalen cck
         SET    cck.datum_einde  = r_cck_000.datum_begin - 1
         WHERE  cck.id                        <> r_cck_000.id
         AND    cck.dwe_wrddom_srtcom         =  r_cck_000.dwe_wrddom_srtcom
         AND    cck.ccn_id                    =  r_cck_000.ccn_id
         AND    cck.datum_einde               =  l_olddatumbegin - 1
         ;
      END IF ;
      /* Bij verwijderen */
      /* als een cck verwijderd wordt waarvan de einddatum naar een broeder
         record overerft was, dan schuift de einddatum van de broeder op naar de
         einddatum van het verwijderde record.
      */
      IF l_job = 'D'
      THEN
         UPDATE rle_communicatie_cat_kanalen cck
         SET    cck.datum_einde  = l_olddatumeinde
         WHERE   cck.dwe_wrddom_srtcom         = l_oldsrtcom
         AND    cck.ccn_id                    = l_oldccnid
         AND    cck.datum_einde               = l_olddatumbegin - 1
         ;
      END IF ;
      /* Overlapcontrole voor "broederrecords" */
      IF l_job <> 'D'
      THEN
         FOR r_cck_003 IN c_cck_003( r_cck_000.dwe_wrddom_srtcom
                                   , r_cck_000.ccn_id
                                   )
         LOOP
            IF  r_cck_003.id <> r_cck_000.id
            THEN
               STM_ALGM_CHECK.ALGM_DATOVLP( r_cck_000.datum_begin
                                          , r_cck_000.datum_einde
                                          , r_cck_003.datum_begin
                                          , r_cck_003.datum_einde
                                          ) ;
            END IF ;
         END LOOP ;
      END IF ;
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
  RAISE ;
END ;
/