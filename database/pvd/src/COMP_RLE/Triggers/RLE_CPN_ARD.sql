CREATE OR REPLACE TRIGGER comp_rle.RLE_CPN_ARD
 AFTER DELETE
 ON comp_rle.RLE_CONTACTPERSONEN
 FOR EACH ROW

BEGIN
/* QMS$JOURNALLING */
   insert
   into rle_contactpersonen_jn
   (     jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      ras_id
   ,      rle_numrelatie
   ,      code_functie
   ,      code_soort_contactpersoon
   ,      creatie_door
   ,      dat_creatie
   ,      mutatie_door
   ,      dat_mutatie
   )
   values
   (      alg_sessie.gebruiker
   ,      sysdate
   ,      'DEL'
   ,     :old.ras_id
   ,      :old.rle_numrelatie
   ,      :old.code_functie
   ,      :old.code_soort_contactpersoon
   ,      :old.creatie_door
   ,      :old.dat_creatie
   ,      :old.mutatie_door
   ,      :old.dat_mutatie
   );
END;
/