CREATE OR REPLACE TRIGGER comp_rle.RLE_WVE_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_WWFT_VERDACHTEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
begin
  :new.dat_mutatie := sysdate;
  :new.mutatie_door := alg_sessie.gebruiker;
end rle_wve_bru;
/