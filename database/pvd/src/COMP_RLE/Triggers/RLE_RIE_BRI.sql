CREATE OR REPLACE TRIGGER comp_rle.RLE_RIE_BRI
 BEFORE INSERT
 ON comp_rle.RLE_RELATIEROLLEN_IN_ADM
 FOR EACH ROW
DECLARE
/*
wijzigingshistorie

Datum           Auteur   Omschrijving
23-11-2012  PKG       ivm Marval call 293480: Stamgegevens naar WIA uitkeringsbedrijven in CODA:
                                 publicatie van relatie rol in admin toevoegen, zodat CODA deze mutaties oppakt,
								 en ook de stamgegevens aan het uitkeingsbedrijf kan toevoegen.
*/
begin
:new.DAT_CREATIE := SYSDATE;
:new.CREATIE_DOOR := USER;
:new.DAT_MUTATIE := SYSDATE;
:new.MUTATIE_DOOR := USER;
--
/* publicatie van insert*/
ibm.publish_event ('MUT RLE ROL IN ADMIN'
                           ,'I'
                           ,to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')
                           ,null,:new.rle_numrelatie
                           ,null,:new.rol_codrol
                           ,null,:new.adm_code
                           ,null,to_char(:new.dat_creatie,'dd-mm-yyyy hh24:mi:ss')
                           ,null,:new.creatie_door
                           ,null,to_char(:new.dat_mutatie,'dd-mm-yyyy hh24:mi:ss')
                           ,null,:new.mutatie_door
                           );
end;
/