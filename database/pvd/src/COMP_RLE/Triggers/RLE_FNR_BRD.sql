CREATE OR REPLACE TRIGGER comp_rle.RLE_FNR_BRD
 BEFORE DELETE
 ON comp_rle.RLE_FINANCIEEL_NUMMERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
   rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);
/* Publicatie voor synchronisatie */
BEGIN
   IBM.PUBLISH_EVENT
      ( 'MUT RLE FINANCIEEL NRS'
      , 'D'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.RLE_NUMRELATIE                               , NULL
      , :old.NUMREKENING                                  , NULL
      , :old.LND_CODLAND                                  , NULL
      , :old.DWE_WRDDOM_SRTREK                            , NULL
      , to_char(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , NULL
      , :old.INDINHOWN                                    , NULL
      , to_char(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , NULL
      , :old.INDFIAT                                      , NULL
      , :old.ROL_CODROL                                   , NULL
      , :old.CODORGANISATIE                               , NULL
      , :old.TENAAMSTELLING                               , NULL
      , :old.CODDOELREK                                   , NULL
      , :old.CODWIJZEBETALING                             , NULL
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.CREATIE_DOOR                                 , NULL
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.MUTATIE_DOOR                                 , NULL
      , :old.ID                                           , NULL
      , :old.iban                                         , null
      , :old.adm_code                                     , null
      );
END;
END;
/