CREATE OR REPLACE TRIGGER comp_rle.RLE_CNR_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_COMMUNICATIE_NUMMERS
 FOR EACH ROW
DECLARE

-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeBasisGegevens
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 30-05-2014 EZW   Nieuw event salarismutatie voor aansluiting Bancs
L_IND_GEWIJZIGD VARCHAR2(1) := 'N';
L_FOUT VARCHAR2(30);
cn_event_WgrBasgeg  constant varchar2(30) := 'wgr.GewijzigdeBasisGegevens';
l_event xmltype;
l_relnum number;
cursor c_rec (b_relnum number)
is
select rle_numrelatie
from rle_relatie_externe_codes rec
where rec.rle_numrelatie = :new.rle_numrelatie
and rec.dwe_wrddom_extsys = 'WGR'
and rec.rle_numrelatie = b_relnum;

-- PL/SQL Block
BEGIN
/* BR IER0024  Werkgever 000001 mag niet gemuteerd of verwijderd worden */
rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);
/* BRIER0004  Controle rol bij communicatienummer*/
IF stm_dbproc.column_modified( :new.rol_codrol, :old.rol_codrol )
THEN
  IF :new.rol_codrol IS NOT NULL
  THEN
    rle_rrl_chk_fk( :new.rle_numrelatie
                               , :new.rol_codrol);
  END IF ;
END IF ;
/* END BRIER0004 */

/* RLE_CNR_BRU  */
IF    stm_dbproc.column_modified(:new.numcommunicatie      , :old.numcommunicatie)
OR stm_dbproc.column_modified(:new.rle_numrelatie              , :old.rle_numrelatie)
OR stm_dbproc.column_modified(:new.dwe_wrddom_srtcom, :old.dwe_wrddom_srtcom)
OR stm_dbproc.column_modified(:new.datingang                       , :old.datingang)
OR stm_dbproc.column_modified(:new.dateinde                         , :old.dateinde)
OR stm_dbproc.column_modified(:new.rol_codrol                      , :old.rol_codrol)
OR stm_dbproc.column_modified(:new.ras_id                              , :old.ras_id)
THEN
  l_ind_gewijzigd := 'J';
END IF;
--
IF l_ind_gewijzigd = 'J'
THEN
  IF STM_ALGM_CHECK.ALGM_DATCOMP(:new.dateinde, '=', :old.dateinde) = 'N'
  OR :new.datingang <> :old.datingang
  THEN
    rle_rle_childper( :new.rle_numrelatie
                                  , :new.datingang
                                  , :new.dateinde);
  END IF;
   --
  IF :new.datingang <> :old.datingang
  THEN
     rle_chk_rle_instdat( :old.rle_numrelatie
                                          , :new.datingang
                                          , 'communicatienummers');
  END IF;
   /* Einde toegevoegde code  */
   --
  IF :new.dwe_wrddom_srtcom <> :old.dwe_wrddom_srtcom
  THEN
    -- Controle referentiewaarde
    ibm.ibm( 'chk_refwrd'
                   , 'RLE'
                   , 'SC'
                   , :new.dwe_wrddom_srtcom);
  END IF;
   --
END IF;
--
IF l_ind_gewijzigd = 'J'
THEN
   /* QMS$DATA_AUDITING */
   :new.DAT_MUTATIE := SYSDATE;
   :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END IF;


-- Controle op Telefoonnummer
if :new.dateinde is null
then
  l_fout := rle_chk_telnum(:new.numcommunicatie
                          ,:new.ras_id
                          ,:new.rle_numrelatie
                          ,:new.dwe_wrddom_srtcom);
end if;

IF l_fout <> 'J' THEN
   stm_dbproc.raise_error(l_fout,NULL,'rle_bru');
END IF;


--
/* Publicatie voor synchronisatie */
BEGIN
  IBM.PUBLISH_EVENT
    ( 'MUT RLE COMMUNICATIE NRS'
    , 'U'
    , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
    , :old.RAS_ID                                       , :new.RAS_ID
    , :old.ID                                           , :new.ID
    , :old.NUMCOMMUNICATIE                              , :new.NUMCOMMUNICATIE
    , :old.RLE_NUMRELATIE                               , :new.RLE_NUMRELATIE
    , :old.DWE_WRDDOM_SRTCOM                            , :new.DWE_WRDDOM_SRTCOM
    , :old.CODORGANISATIE                               , :new.CODORGANISATIE
    , to_char(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , to_char(:new.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')
    , :old.INDINHOWN                                    , :new.INDINHOWN
    , to_char(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , to_char(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
    , :old.ROL_CODROL                                   , :new.ROL_CODROL
    , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
    , :old.CREATIE_DOOR                                 , :new.CREATIE_DOOR
    , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
    , :old.MUTATIE_DOOR                                 , :new.MUTATIE_DOOR
    );
END;
begin
  if (:new.dwe_wrddom_srtcom in ('FAX','TEL','EMAIL')
    or :old.dwe_wrddom_srtcom in ('FAX','TEL','EMAIL'))
    and :old.numcommunicatie <> :new.numcommunicatie
  then
    open c_rec(:new.rle_numrelatie);
    fetch c_rec into l_relnum;
    if c_rec%found
    then
      l_event := eventhandler.create_event(p_event_code => cn_event_WgrBasgeg);
      eventhandler.add_num_value(p_event  => l_event
                                ,p_key    => 'RELATIENUMMER'
                                ,p_value  => :new.rle_numrelatie);
      eventhandler.add_date_value( p_event => l_event
                                 , p_key => 'DATUM_MUTATIE'
                                 , p_value => :new.dat_mutatie
                                 );
      eventhandler.publish_asynch_event(p_event => l_event);
    end if;
    close c_rec;
  end if;
exception
  when others then
    null;
end;
END RLE_CNR_BRU;
/