CREATE OR REPLACE TRIGGER comp_rle.RLE_PAT_BRI
 BEFORE INSERT
 ON comp_rle.RLE_POSTSOORT_ADRESSOORTEN
 FOR EACH ROW
DECLARE

   cursor c_seq
   is
   select rle_pat_seq1.nextval
   from   dual;

   l_id number;
begin

   if :new.id is null then
       open c_seq;
       fetch c_seq
       into  l_id;
       close c_seq;

       :new.id := l_id;
   end if;
end;
/