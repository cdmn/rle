CREATE OR REPLACE TRIGGER comp_rle.RLE_RIE_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_RELATIEROLLEN_IN_ADM
 FOR EACH ROW
DECLARE
/*
wijzigingshistorie

Datum           Auteur   Omschrijving
23-11-2012  PKG       ivm Marval call 293480: Stamgegevens naar WIA uitkeringsbedrijven in CODA:
                                 publicatie van relatie rol in admin toevoegen, zodat CODA deze mutaties oppakt,
								 en ook de stamgegevens aan het uitkeingsbedrijf kan toevoegen.
*/
begin
:new.DAT_MUTATIE := SYSDATE;
:new.MUTATIE_DOOR := USER;
--
/* publicatie van mutatie */
/* publicatie van update voor CODA */
ibm.publish_event ('MUT RLE ROL IN ADMIN'
                           ,'U'
                           ,to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')
                           ,:old.rle_numrelatie,:new.rle_numrelatie
                           ,:old.rol_codrol,:new.rol_codrol
                           ,:old.adm_code,:new.adm_code
                           ,to_char(:old.dat_creatie,'dd-mm-yyyy hh24:mi:ss'),to_char(:new.dat_creatie,'dd-mm-yyyy hh24:mi:ss')
                           ,null,:new.creatie_door
                           ,to_char(:old.dat_mutatie,'dd-mm-yyyy hh24:mi:ss'),to_char(:new.dat_mutatie,'dd-mm-yyyy hh24:mi:ss')
                           ,null,:new.mutatie_door
                           );
end;
/