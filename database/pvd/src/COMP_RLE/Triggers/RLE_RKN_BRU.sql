CREATE OR REPLACE trigger comp_rle.rle_rkn_bru
 before update
 on comp_rle.rle_relatie_koppelingen
 for each row
declare
  cursor c_rkg (b_rkg_id in number)
  is
  select rkg.rol_codrol_beperkt_tot
  from   rle_rol_in_koppelingen rkg
  where  rkg.id = b_rkg_id
  ;
  --
  cursor c_rkg2 (b_rkg_id in number)
  is
  select 'J'
  from   rle_rol_in_koppelingen rkg
  where  rkg.code_rol_in_koppeling in ('H', 'G', 'S')
  and    rkg.id = b_rkg_id
  ;
  --
  cursor c_rle ( b_rle_numrelatie in number
               , b_rle_numrelatie_voor in number
               )
  is
  select 'J'
  from   rle_relaties rle
  where  (  rle.numrelatie = b_rle_numrelatie
         or rle.numrelatie = b_rle_numrelatie_voor
         )
  and    rle.datoverlijden is not null
  ;
  --
  l_rol_in_rkg        rle_rol_in_koppelingen.rol_codrol_beperkt_tot%type;
  l_dummy             varchar2(1);
  l_rkg2_fnd          boolean := false;
  l_rle_fnd           boolean := false;
  --
begin
  /* BR IER0024
     Werkgever 000001 mag niet gemuteerd of verwijderd worden
  */
  rle_wgr_000001.prc_chk_wgr_000001(pin_numrelatie => :old.rle_numrelatie);

  /* QMS$DATA_AUDITING */
  :new.dat_mutatie := sysdate;
  :new.mutatie_door := alg_sessie.gebruiker;

  /* QMS$JOURNALLING */
  insert into rle_relatie_koppelingen_jn
    ( jn_user
    , jn_date_time
    , jn_operation
    , id
    , rkg_id
    , rle_numrelatie
    , rle_numrelatie_voor
    , dat_begin
    , code_dat_begin_fictief
    , dat_einde
    , code_dat_einde_fictief
    , creatie_door
    , dat_creatie
    , mutatie_door
    , dat_mutatie
    )
  values
    ( alg_sessie.gebruiker
    , sysdate
    , 'UPD'
    , :old.id
    , :old.rkg_id
    , :old.rle_numrelatie
    , :old.rle_numrelatie_voor
    , :old.dat_begin
    , :old.code_dat_begin_fictief
    , :old.dat_einde
    , :old.code_dat_einde_fictief
    , :old.creatie_door
    , :old.dat_creatie
    , :old.mutatie_door
    , :old.dat_mutatie
    );

  /* Publicatie voor synchronisatie */
  -- 12-11-2003, VER, nulmutaties niet synchroniseren.
  --                  en bij verwantschappen alleen publicaties indien beide personen GBA-status OV hebben
  -- 01-06-2005, VER, PR-UII Consolidatie ZEUS/HERA: alleen een publicatie indien de relatie-koppeling
  --                  geen verwantschap is.
  --
  if   stm_dbproc.column_modified( p_column_old => :new.id
                                 , p_column_new => :old.id
                                 )
    or stm_dbproc.column_modified( p_column_old => :new.rkg_id
                                 , p_column_new => :old.rkg_id
                                 )
    or stm_dbproc.column_modified( p_column_old => :new.rle_numrelatie
                                 , p_column_new => :old.rle_numrelatie
                                 )
    or stm_dbproc.column_modified( p_column_old => :new.rle_numrelatie_voor
                                 , p_column_new => :old.rle_numrelatie_voor
                                 )
    or stm_dbproc.column_modified( p_column_old => :new.dat_begin
                                 , p_column_new => :old.dat_begin
                                 )
    or stm_dbproc.column_modified( p_column_old => :new.dat_einde
                                 , p_column_new => :old.dat_einde
                                 )
  then
    --
    -- 02-03-2006. NKU, Project Levensloop
    --                           Relaties met rol WA, WB, WC of WD
    --                           worden niet gepubliceerd.
    --
    if    not rle_chk_rol_niet_mtb(p_relatienr => :new.rle_numrelatie)
      and not rle_chk_rol_niet_mtb(p_relatienr => :new.rle_numrelatie_voor)
    then
      open c_rkg (b_rkg_id => :new.rkg_id);
      fetch c_rkg into l_rol_in_rkg;
      if c_rkg%found
      then
        if l_rol_in_rkg <> 'PR'
        then
          ibm.publish_event
            ( 'MUT RLE KOPPELINGEN'
            , 'U'
            , to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss')
            , :old.id                                           , :new.id
            , :old.rkg_id                                       , :new.rkg_id
            , :old.rle_numrelatie                               , :new.rle_numrelatie
            , :old.rle_numrelatie_voor                          , :new.rle_numrelatie_voor
            , to_char(:old.dat_begin, 'dd-mm-yyyy hh24:mi:ss')  , to_char(:new.dat_begin, 'dd-mm-yyyy hh24:mi:ss')
            , to_char(:old.dat_einde, 'dd-mm-yyyy hh24:mi:ss')  , to_char(:new.dat_einde, 'dd-mm-yyyy hh24:mi:ss')
            , :old.creatie_door                                 , :new.creatie_door
            , to_char(:old.dat_creatie, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.dat_creatie, 'dd-mm-yyyy hh24:mi:ss')
            , :old.mutatie_door                                 , :new.mutatie_door
            , to_char(:old.dat_mutatie, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.dat_mutatie, 'dd-mm-yyyy hh24:mi:ss')
            , l_rol_in_rkg
            );
         end if;
      end if;
      close c_rkg;
      --
    end if;
  end if;

  --
  --B+ XJB, 07-10-2008, Afsplitsen BNP
  -- BR_CEV0025: RKN Afhandelen beeindigen relatie
  if   (   :old.dat_einde is null
       and :new.dat_einde is not null
       )
    and :new.dat_einde <= sysdate
    --and :new.dat_einde >= cn_ingangsdatum_bnp_afsplitsen
    and  rle_rkn.get_scheiding_partner_enabled    -- xcw 2-11-2012
  then
    --
    -- is het een huwelijk/gegistreerd partnerschap of samenlevingsovereenkomst
    open c_rkg2 (b_rkg_id => :new.rkg_id);
    fetch c_rkg2 into l_dummy;
    l_rkg2_fnd := c_rkg2%found;
    close c_rkg2;

    if l_rkg2_fnd
    then
      -- is een van de partners al overleden?
      open c_rle ( b_rle_numrelatie => :new.rle_numrelatie
                 , b_rle_numrelatie_voor => :new.rle_numrelatie_voor
                 );
      fetch c_rle into l_dummy;
      l_rle_fnd := c_rle%found;
      close c_rle;
      --
      if not l_rle_fnd
      then
        stm_util.debug('Event SCHEIDING_PARTNERS publishen.');
        ibm.publish_event
          ( 'SCHEIDING_PARTNERS'
          , 'RLE'
          , :new.rle_numrelatie
          , to_char(:new.dat_einde, 'dd-mm-yyyy hh24:mi:ss')
          );
      end if;
      --
    end if;
    --
  end if;
  --e+
end rle_rkn_bru;
/