CREATE OR REPLACE TRIGGER comp_rle.RLE_MIG_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_MEDIUM_INZENDINGEN
 FOR EACH ROW

BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
BEGIN
   /* Add variabelen */
   IF inserting
   THEN
      stm_add_word( l_stack, 'I' ) ;
   ELSIF updating
   THEN
      stm_add_word( l_stack, 'U' ) ;
      /* BRCEV0007 */
      stm_add_word( l_stack, TO_CHAR( :old.dat_begin ) ) ;
   ELSE /* deleting */
      stm_add_word( l_stack, 'D' ) ;
      /* BRCEV0007 */
      stm_add_word( l_stack, :old.rle_numrelatie ) ;
      stm_add_word( l_stack, TO_CHAR( :old.dat_begin ) ) ;
      stm_add_word( l_stack, TO_CHAR( :old.dat_einde ) ) ;
      stm_add_word( l_stack, :old.code_soort_inzending ) ;
   END IF ;
   /* Add ROWID */
   IF inserting OR updating
   THEN
      stm_algm_muttab.add_rowid( :new.rowid, l_stack ) ;
   ELSE
      stm_algm_muttab.add_rowid( :old.rowid, l_stack ) ;
   END IF ;
END ;
END RLE_MIG_AR;
/