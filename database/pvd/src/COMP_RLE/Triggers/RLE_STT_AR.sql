CREATE OR REPLACE TRIGGER comp_rle.RLE_STT_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_STRATEN
 FOR EACH ROW
DECLARE
/*****************************************************************************************************/
/* Trigger RLE.RLE_STT_AR:                                                                           */
/*                                                                                                   */
/*                                                                                                   */
/* Datum      Wie         Omschrijving                                                               */
/* ---------- ----------- ---------------------------------------------------------------------------*/
/* 02-09-2016 DFU         Creatie voor mutaties naar eventstore                                      */
/* 05-09-2016 OKR         Old_straatnaam voor tegenhouden schijnmutaties                             */
/*****************************************************************************************************/

   l_stack  VARCHAR2( 128 ) ;
BEGIN
   /* Add variabelen */
   IF inserting
   THEN
      stm_add_word( l_stack, 'I' ) ;
    /* niets nodig voor insert */
   ELSIF updating
   THEN
      stm_add_word( l_stack, 'U' ) ;
      stm_add_word( l_stack, :old.straatid);
      stm_add_word( l_stack, :new.wps_woonplaatsid);
      stm_add_word( l_stack, :new.straatnaam ) ;
      stm_add_word( l_stack, :old.straatnaam ) ;
   ELSE /* deleting */
      stm_add_word( l_stack, 'D' ) ;
      /* niets nodig voor delete */
   END IF ;
   /* Add ROWID */
   IF inserting OR updating
   THEN
      stm_algm_muttab.add_rowid( :new.rowid, l_stack ) ;
   ELSE
      stm_algm_muttab.add_rowid( :old.rowid, l_stack ) ;
   END IF ;
END ;
/