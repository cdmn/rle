CREATE OR REPLACE TRIGGER comp_rle.RLE_SAR_BRI
 BEFORE INSERT
 ON comp_rle.RLE_SADRES_ROLLEN
 FOR EACH ROW

BEGIN
-- Controle referentiewaarde
ibm.ibm('chk_refwrd'
              , 'RLE'
              , 'SAD'
              , :new.dwe_wrddom_sadres);
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_SAR_BRI;
/