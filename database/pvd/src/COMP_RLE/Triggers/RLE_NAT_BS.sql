CREATE OR REPLACE TRIGGER comp_rle.RLE_NAT_BS
 BEFORE DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_NATIONALITEITEN
/************************************************************

Triggernaam: rle_nat_bs

Datum       Wie        Wat

--------    ---------- -----------------------------------
21-10-2009  UAO        Creation
*************************************************************/
--
BEGIN
  --
  stm_algm_muttab.init_array;
  --
END;
/