CREATE OR REPLACE TRIGGER comp_rle.RLE_FNC_BRI
 BEFORE INSERT
 ON comp_rle.RLE_FINNR_CODA
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
begin
  ---
  :new.creatie_door := user;
  :new.mutatie_door := user;
  :new.dat_creatie  := sysdate;
  :new.dat_mutatie  := sysdate;
  --
end;
/