CREATE OR REPLACE TRIGGER comp_rle.RLE_AMG_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_AANMELDINGEN
 FOR EACH ROW

BEGIN
   /* DATA_AUDITING */
   :new.DAT_MUTATIE  := SYSDATE ;
   :new.MUTATIE_DOOR := USER    ;
   /* Publicatie voor synchronisatie */
   NULL ;
END ;
/