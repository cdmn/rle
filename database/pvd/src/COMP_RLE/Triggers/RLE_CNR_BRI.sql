CREATE OR REPLACE TRIGGER comp_rle.RLE_CNR_BRI
 BEFORE INSERT
 ON comp_rle.RLE_COMMUNICATIE_NUMMERS
 FOR EACH ROW
DECLARE

-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeBasisGegevens
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 30-05-2014 EZW   Nieuw event salarismutatie voor aansluiting Bancs
-- 20-01-2017 XYO   MB-55  Muteren contactgegevens na overlijden mogelijk maken
cn_event_WgrBasgeg  constant varchar2(30) := 'wgr.GewijzigdeBasisGegevens';
l_event xmltype;
l_relnum number;
L_IND_AANGEMAAKT VARCHAR2(1);
L_ERROR VARCHAR2(2000);
L_FOUT VARCHAR2(30);
cursor c_rec (b_relnum number)
is
select rle_numrelatie
from rle_relatie_externe_codes rec
where rec.rle_numrelatie = :new.rle_numrelatie
and rec.dwe_wrddom_extsys = 'WGR'
and rec.rle_numrelatie = b_relnum;

-- Aanroep  br_ier0040_rcn verwijderd (MB-55)
/* BR IER0024
Werkgever 000001 mag niet gemuteerd of verwijderd worden */
BEGIN
rle_wgr_000001.prc_chk_wgr_000001(:new.rle_numrelatie);
/* PGE toegevoegd vanwege wijziging project RLE
automatisch aanmaken RLE_RELATIEROLLEN_IN_ADM
(inclusief bugfix B4093) */
IF :new.rol_codrol IS NOT NULL
THEN
  rle_insert_rie(p_adm_code => :new.adm_code
               , p_rle_numrelatie => :new.rle_numrelatie
               , p_rol_codrol => :new.rol_codrol
               , p_ind_aangemaakt => l_ind_aangemaakt
               , p_error => l_error
               , p_gba_sync_indien_al_aanwezig => 'N'
               );
  IF l_error IS NOT NULL
  THEN
     stm_dbproc.raise_error(l_error, NULL, 'rle_bri');

END IF;
END IF;
/* einde toevoeging PGE */

/* Begin toegevoegde code   */
IF :new.rol_codrol IS NOT NULL
THEN
  rle_rrl_chk_fk(:new.rle_numrelatie
	           ,:new.rol_codrol);
END IF;
rle_chk_rle_instdat( :new.rle_numrelatie
                                      , :new.datingang
                                      , 'communicatienummers');
:new.codorganisatie := 'A';
l_fout := rle_chk_telnum(:new.numcommunicatie
                                              ,:new.ras_id
                                              ,:new.rle_numrelatie
                                              ,:new.dwe_wrddom_srtcom);
IF l_fout <> 'J' THEN
   stm_dbproc.raise_error(l_fout,NULL,'rle_bri');
END IF;
-- Controle referentiewaarde
ibm.ibm('chk_refwrd'
               ,'RLE'
               ,'SC'
               ,:new.dwe_wrddom_srtcom) ;
IF :new.id IS NULL
THEN
  :new.id := rle_cnr_seq1_next_id;
END IF;
/* QMS$DATA_AUDITING */
:new.DAT_CREATIE := SYSDATE;
:new.CREATIE_DOOR := alg_sessie.gebruiker;
:new.DAT_MUTATIE := SYSDATE;
:new.MUTATIE_DOOR := alg_sessie.gebruiker;
/* Publicate voor synchronisatie */
BEGIN
  IBM.PUBLISH_EVENT
    ( 'MUT RLE COMMUNICATIE NRS'
    ,'I'
    , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
    , NULL, :new.RAS_ID
    , NULL, :new.ID
    , NULL, :new.NUMCOMMUNICATIE
    , NULL, :new.RLE_NUMRELATIE
    , NULL, :new.DWE_WRDDOM_SRTCOM
    , NULL, :new.CODORGANISATIE
    , NULL, to_char(:new.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')
    , NULL, :new.INDINHOWN
    , NULL, to_char(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
    , NULL, :new.ROL_CODROL
    , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
    , NULL, :new.CREATIE_DOOR
    , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
    , NULL, :new.MUTATIE_DOOR
    );
END;
begin
  if :new.dwe_wrddom_srtcom in ('FAX','TEL','EMAIL')
  then
    open c_rec(:new.rle_numrelatie);
    fetch c_rec into l_relnum;
    if c_rec%found
    then
      l_event := eventhandler.create_event(p_event_code => cn_event_WgrBasgeg);
      eventhandler.add_num_value(p_event  => l_event
                                ,p_key    => 'RELATIENUMMER'
                                ,p_value  => :new.rle_numrelatie);
      eventhandler.add_date_value( p_event => l_event
                                 , p_key => 'DATUM_MUTATIE'
                                 , p_value => :new.dat_mutatie
                                 );
      eventhandler.publish_asynch_event(p_event => l_event);
    end if;
    close c_rec;
  end if;
exception
  when others then
    null;
end;
END RLE_CNR_BRI;
/