CREATE OR REPLACE TRIGGER comp_rle.RLE_GBD_BRI
 BEFORE INSERT
 ON comp_rle.RLE_GEMOEDSBEZWAARDHEDEN
 FOR EACH ROW
/*
-- 19-02-2015 ZSF Extra attribuut voor event rle.GemoedsbezwaardPersoonGemuteerd
17-11-2014 ZSF nieuw event voor aansluiting Bancs
*/
BEGIN
   /* QMS$DATA_AUDITING */
   :new.DAT_CREATIE  := SYSDATE ;
   :new.CREATIE_DOOR := USER ;
   :new.DAT_MUTATIE  := SYSDATE ;
   :new.MUTATIE_DOOR := USER ;

   /* Publicatie voor synchronisatie */
   IBM.PUBLISH_EVENT
      ( 'MUT GEMOEDSBEZWAARDHEDEN'
      , 'I'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.RLE_NUMRELATIE
      , NULL, to_char(:new.DAT_BEGIN, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, to_char(:new.DAT_EINDE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.CREATIE_DOOR
      , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.MUTATIE_DOOR
      , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
      );
--
    declare
      l_event xmltype;
    begin
      if (  :new.dat_begin <= sysdate
         and ( :new.dat_einde is null or :new.dat_einde > sysdate )
         )
      then
        l_event := eventhandler.create_event ( p_event_code => 'rle.GemoedsbezwaardPersoonGemuteerd' );
        eventhandler.add_num_value( p_event => l_event
                                  , p_key => 'RELATIENUMMER'
                                  , p_value => :new.rle_numrelatie
                                  );
        eventhandler.add_value( p_event => l_event
                              , p_key => 'INDICATIE'
                              , p_value => 'J'
                              );
        eventhandler.add_value( p_event => l_event
                              , p_key => 'CODROL'
                              , p_value => 'PR'
                              );
        eventhandler.add_date_value( p_event => l_event
                                   , p_key => 'DATUM_MUTATIE'
                                   , p_value => :new.dat_mutatie
                                   );
        eventhandler.publish_asynch_event ( p_event => l_event );
      end if;
    end;
END ;
/