CREATE OR REPLACE TRIGGER comp_rle.RLE_VBG_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_VERZENDBESTEMMINGEN

DECLARE
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_RAS_021
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 ,B_SRTADRES IN VARCHAR2
 ,B_DATUMP IN DATE
 )
 IS
/* C_RAS_021 */
select	ras.ads_numadres
from	rle_relatie_adressen ras
where 	ras.rle_numrelatie = b_numrelatie
and    	(ras.rol_codrol is null
	or ras.rol_codrol = b_codrol)
and   	ras.dwe_wrddom_srtadr = b_srtadres
and     nvl(b_datump,trunc(sysdate))
             between ras.datingang
             and     nvl(ras.dateinde
                        ,nvl(b_datump,trunc(sysdate))
                        )
order by ras.rle_numrelatie, ras.rol_codrol;
-- Program Data
L_IND_GELDIG VARCHAR2(1);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
DECLARE
CURSOR c_cpn
(b_relatie IN NUMBER
,b_srt_adres IN VARCHAR2
)
IS
SELECT 'x'
FROM   rle_relatie_adressen 	ras
,      rle_relaties		rle
WHERE  ras.rle_numrelatie     = rle.numrelatie
AND    rle.numrelatie 		= b_relatie
AND    ras.datingang             <= SYSDATE
AND    NVL(ras.dateinde,SYSDATE) >= SYSDATE
AND    ras.dwe_wrddom_srtadr 	= b_srt_adres
;
--
CURSOR c_cpa
(b_relatie_wgr     IN NUMBER
,b_relatie_contact IN NUMBER
,b_srt_adres       IN VARCHAR2
)
IS
SELECT 'x'
FROM   rle_contactpersonen  cpn
,      rle_relatie_adressen ras
WHERE  cpn.rle_numrelatie = b_relatie_contact
AND    ras.rle_numrelatie = b_relatie_wgr
AND    cpn.ras_id = ras.id
AND    ras.datingang             <= SYSDATE
AND    NVL(ras.dateinde,SYSDATE) >= SYSDATE
AND    ras.dwe_wrddom_srtadr 	= b_srt_adres
;
--
   l_dummy		VARCHAR2(1);
   l_stack  	VARCHAR2( 128 ) ;
   l_rowid  	ROWID           ;
   l_job    	VARCHAR2( 1 )   ;
   l_code_soort_post	rle_verzendbestemmingen.code_soort_post%TYPE  ;
   l_code_soort_adres	rle_verzendbestemmingen.code_soort_adres%TYPE ;
   l_rrl_rol_codrol      rle_verzendbestemmingen.rrl_rol_codrol%TYPE ;
   l_rrl_rle_numrelatie	 rle_verzendbestemmingen.rrl_rle_numrelatie%TYPE ;
   l_rrl_rol_codrol_hoort_bij
     rle_verzendbestemmingen.rrl_rol_codrol_hoort_bij%TYPE ;
   l_rrl_rle_numrelatie_hoort_bij
     rle_verzendbestemmingen.rrl_rle_numrelatie_hoort_bij%TYPE ;
   l_rrl_rol_codrol_betreft
     rle_verzendbestemmingen.rrl_rol_codrol_betreft%TYPE ;
   l_rrl_rle_numrelatie_betreft
     rle_verzendbestemmingen.rrl_rle_numrelatie_betreft%TYPE ;
   --
   l_ind_bestaat 			VARCHAR2(2);
   l_chk_adres_rle      rle_verzendbestemmingen.rrl_rle_numrelatie%TYPE ;
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;
      /* Ophalen variabelen per soort statement */
      IF    l_job = 'I'
      THEN
         /* Ophalen variabelen voor insert */
         l_code_soort_post  := stm_get_word( l_stack ) ;
         l_code_soort_adres := stm_get_word( l_stack ) ;
	   l_rrl_rol_codrol     := stm_get_word( l_stack ) ;
         l_rrl_rle_numrelatie	:= stm_get_word( l_stack ) ;
	   l_rrl_rol_codrol_hoort_bij       := stm_get_word( l_stack ) ;
         l_rrl_rle_numrelatie_hoort_bij	:= stm_get_word( l_stack ) ;
	   l_rrl_rol_codrol_betreft         := stm_get_word( l_stack ) ;
         l_rrl_rle_numrelatie_betreft	:= stm_get_word( l_stack ) ;
      ELSIF l_job = 'U'
      THEN
         /* Ophalen variabelen voor update */
         l_code_soort_post  := stm_get_word( l_stack ) ;
         l_code_soort_adres := stm_get_word( l_stack ) ;
	   l_rrl_rol_codrol     := stm_get_word( l_stack ) ;
         l_rrl_rle_numrelatie	:= stm_get_word( l_stack ) ;
	   l_rrl_rol_codrol_hoort_bij       := stm_get_word( l_stack ) ;
         l_rrl_rle_numrelatie_hoort_bij	:= stm_get_word( l_stack ) ;
	   l_rrl_rol_codrol_betreft         := stm_get_word( l_stack ) ;
         l_rrl_rle_numrelatie_betreft	:= stm_get_word( l_stack ) ;
      ELSE
         /* Ophalen variabelen voor delete */
         NULL ;
      END IF ;
      /* Verwerking */
      IF l_job IN ( 'I', 'U' )
      THEN
         /* BRATT0022
            Postsoort moet voorkomen IN Output
         */
         ibm.ibm( 'CONTROLEER_POSTSOORT'
	    		  , 'RLE'
 	    		  , l_code_soort_post
	      	  , l_ind_geldig);
         IF l_ind_geldig = 'N'
         THEN
            stm_dbproc.raise_error('RLE-10314');
         END IF;
         /* BRATT0023
            Soort adres moet voorkomen in referentie
         */
         IF l_code_soort_adres IS NOT NULL THEN
            ibm.ibm( 'CHK_REFWRD'
                , 'RLE'
                , 'SAD'
                , l_code_soort_adres
                ) ;
         END IF;
         /* BRMOD0009
            Controle of de op te voeren adres info
            juist is en beschikbaar
         */
         IF  l_rrl_rle_numrelatie_hoort_bij IS NULL
         AND l_rrl_rol_codrol_hoort_bij     IS NULL
         AND l_rrl_rle_numrelatie_betreft   IS NULL
         AND l_rrl_rol_codrol_betreft       IS NULL THEN
         --
         -- check igv situatie 1 (FO)
         --
           /* Check verderop of deze relatie het beoogde adres heeft */
         l_chk_adres_rle := l_rrl_rle_numrelatie;
         --
         -- check igv situatie 2 (FO)
         --
         ELSIF l_rrl_rle_numrelatie_hoort_bij IS NULL
         AND   l_rrl_rol_codrol_hoort_bij     IS NULL
         AND   l_rrl_rle_numrelatie_betreft   IS NOT NULL
         AND   l_rrl_rol_codrol_betreft       IS NOT NULL THEN
         /* controle of beide RELATIES een relatie hebben */
           l_ind_bestaat := 'N';
           --
           l_ind_bestaat := rle_chk_rkn_exists
                               ( l_rrl_rle_numrelatie
	   			  	 , l_rrl_rol_codrol
   	   			       , l_rrl_rle_numrelatie_betreft
  				       , l_rrl_rol_codrol_betreft
 				       , SYSDATE
				       );
           IF l_ind_bestaat = 'N' THEN
              stm_dbproc.raise_error('RLE-00342'
                        ,'#1'||l_rrl_rol_codrol||
                         '#2'||l_rrl_rle_numrelatie||
                         '#3'||l_rrl_rol_codrol_betreft||
                         '#4'||l_rrl_rle_numrelatie_betreft
                         );
           END IF;
           /* Check verderop of deze relatie het beoogde adres heeft */
           l_chk_adres_rle := l_rrl_rle_numrelatie_betreft;
         --
         -- check igv situatie 3 (FO)
         --
         ELSIF l_rrl_rle_numrelatie_hoort_bij IS NOT NULL
         AND   l_rrl_rol_codrol_hoort_bij     IS NOT NULL
         AND   l_rrl_rle_numrelatie_betreft   IS NULL
         AND   l_rrl_rol_codrol_betreft       IS NULL THEN
        /* controle of beide RELATIES een relatie hebben */
           l_ind_bestaat := 'N';
           --
           l_ind_bestaat := rle_chk_rkn_exists
                               ( l_rrl_rle_numrelatie
	   			  	 , l_rrl_rol_codrol
   	   			       , l_rrl_rle_numrelatie_hoort_bij
  				       , l_rrl_rol_codrol_hoort_bij
 				       , SYSDATE
				       );
           IF l_ind_bestaat = 'N' THEN
                          stm_dbproc.raise_error('RLE-00342'
                        ,'#1'||l_rrl_rol_codrol||
                         '#2'||l_rrl_rle_numrelatie||
                         '#3'||l_rrl_rol_codrol_hoort_bij||
                         '#4'||l_rrl_rle_numrelatie_hoort_bij
                         );
           END IF;
           /* Check verderop of deze relatie het beoogde adres heeft */
           /* en check of de contact persoon bekend is op het adres */
           l_chk_adres_rle := l_rrl_rle_numrelatie;
         --
         -- check igv situatie 4 (FO)
         --
         ELSIF l_rrl_rle_numrelatie_hoort_bij IS NOT NULL
         AND   l_rrl_rol_codrol_hoort_bij     IS NOT NULL
         AND   l_rrl_rle_numrelatie_betreft   IS NOT NULL
         AND   l_rrl_rol_codrol_betreft       IS NOT NULL THEN
         /* controle of beide RELATIES een relatie hebben */
           l_ind_bestaat := 'N';
           --
           l_ind_bestaat := rle_chk_rkn_exists
                               ( l_rrl_rle_numrelatie
	   			  	 , l_rrl_rol_codrol
   	   			       , l_rrl_rle_numrelatie_betreft
  				       , l_rrl_rol_codrol_betreft
 				       , SYSDATE
				       );
           IF l_ind_bestaat = 'N' THEN
                          stm_dbproc.raise_error('RLE-00342'
                        ,'#1'||l_rrl_rol_codrol||
                         '#2'||l_rrl_rle_numrelatie||
                         '#3'||l_rrl_rol_codrol_betreft||
                         '#4'||l_rrl_rle_numrelatie_betreft
                         );
           END IF;
        /* controle of beide RELATIES een relatie hebben */
           l_ind_bestaat := 'N';
           --
           l_ind_bestaat := rle_chk_rkn_exists
                               ( l_rrl_rle_numrelatie_betreft
	   			  	 , l_rrl_rol_codrol_betreft
   	   			       , l_rrl_rle_numrelatie_hoort_bij
  				       , l_rrl_rol_codrol_hoort_bij
 				       , SYSDATE
				       );
           IF l_ind_bestaat = 'N' THEN
                          stm_dbproc.raise_error('RLE-00342'
                        ,'#1'||l_rrl_rol_codrol_hoort_bij||
                         '#2'||l_rrl_rle_numrelatie_hoort_bij||
                         '#3'||l_rrl_rol_codrol_betreft||
                         '#4'||l_rrl_rle_numrelatie_betreft
                         );
           END IF;
           /* Check verderop of deze relatie het beoogde adres heeft */
           /* en check of de contact persoon bekend is op het adres */
           l_chk_adres_rle := l_rrl_rle_numrelatie_betreft;
         ELSE
         --
         -- meer smaken zijn er niet
         --
            stm_dbproc.raise_error('RLE-00346');
         END IF;
         /* Check of deze relatie het beoogde adres heeft */
         OPEN  c_cpn( l_chk_adres_rle
     	              , l_code_soort_adres   );
         FETCH c_cpn INTO l_dummy;
         IF c_cpn%NOTFOUND  THEN
                     CLOSE c_cpn;
                          stm_dbproc.raise_error('RLE-00340'
                        ,'#1'||l_chk_adres_rle||
                         '#2'||' '||  -- rol is niet van belang
                         '#3'||l_code_soort_adres
                         );
         ELSE
                     CLOSE c_cpn;
         END IF;
         /* Check of de contact persoon bekend is op het adres */
         IF    l_rrl_rle_numrelatie_hoort_bij IS NOT NULL
         AND   l_rrl_rol_codrol_hoort_bij     IS NOT NULL THEN
               OPEN  c_cpa( l_chk_adres_rle
                          , l_rrl_rle_numrelatie_hoort_bij
     	                    , l_code_soort_adres   );
               FETCH c_cpa INTO l_dummy;
               IF c_cpa%NOTFOUND  THEN
                     CLOSE c_cpa;
                          stm_dbproc.raise_error('RLE-00344'
                        ,'#1'||l_chk_adres_rle||
                         '#2'||' '||  -- rol is niet van belang
                         '#3'||l_code_soort_adres||
                         '#4'||' '||  -- rol is niet van belang
                         '#5'||l_rrl_rle_numrelatie_hoort_bij
                         );
               ELSE
                     CLOSE c_cpa;
               END IF;
         END IF;
      --
      END IF;
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      RAISE ;
END ;
END RLE_VBG_AS;
/