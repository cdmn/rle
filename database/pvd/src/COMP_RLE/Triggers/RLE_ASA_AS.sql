CREATE OR REPLACE TRIGGER comp_rle."RLE_ASA_AS"
  after delete or insert or update on comp_rle."RLE_AFSTEMMINGEN_GBA"
declare
  /************************************************************

  Datum       Wie        Wat

  --------    ---------- -----------------------------------
  24-10-2016  OKR        Aanmaak trigger tbv sync naar Personen Event Store
  *************************************************************/

  l_stack                 varchar2(128);
  l_rowid                 rowid;
  l_job                   varchar2(1);
  l_new_numrelatie        rle_afstemmingen_gba.rle_numrelatie%type;
  l_old_numrelatie        rle_afstemmingen_gba.rle_numrelatie%type;
  l_new_afnemersindicatie rle_afstemmingen_gba.afnemers_indicatie%type;
  l_old_afnemersindicatie rle_afstemmingen_gba.afnemers_indicatie%type;
  l_new_code_gba_status   rle_afstemmingen_gba.code_gba_status%type;
  l_old_code_gba_status   rle_afstemmingen_gba.code_gba_status%type;
begin
  stm_algm_muttab.get_rowid(l_rowid, l_stack);
  --
  while l_rowid is not null
  loop
    /* Ophalen standaard variabelen */
    l_job := stm_get_word(l_stack);
    /* Ophalen variabelen per soort statement */
    if l_job = 'I'
    then
      l_new_numrelatie        := stm_get_word(l_stack);
      l_new_afnemersindicatie := stm_get_word(l_stack);
      l_new_code_gba_status   := stm_get_word(l_stack);
    elsif l_job = 'U'
    then
      l_new_numrelatie        := stm_get_word(l_stack);
      l_new_afnemersindicatie := stm_get_word(l_stack);
      l_new_code_gba_status   := stm_get_word(l_stack);
      l_old_numrelatie        := stm_get_word(l_stack);
      l_old_afnemersindicatie := stm_get_word(l_stack);
      l_old_code_gba_status   := stm_get_word(l_stack);
    elsif l_job = 'D'
    then
      l_old_numrelatie        := stm_get_word(l_stack);
      l_old_afnemersindicatie := stm_get_word(l_stack);
      l_old_code_gba_status   := stm_get_word(l_stack);
    end if;
    --
    if l_job <> 'U' or l_new_afnemersindicatie <> l_old_afnemersindicatie
    then
      appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_asa(p_event_title => 'Afnemersindicatie gewijzigd'
                                                                     ,p_relatienr => nvl(l_new_numrelatie, l_old_numrelatie)
                                                                     ,p_afnemersindicatie => l_new_afnemersindicatie);
    end if;
    --
    /* Ophalen volgende record */
    stm_algm_muttab.get_rowid(l_rowid, l_stack);
  end loop;
  --
  stm_algm_muttab.clear_array;
exception
  when no_data_found then
    null;
  when others then
    stm_algm_muttab.clear_array;
    raise;
end;
/