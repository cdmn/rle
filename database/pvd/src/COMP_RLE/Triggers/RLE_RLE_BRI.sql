CREATE OR REPLACE TRIGGER comp_rle.RLE_RLE_BRI
 BEFORE INSERT
 ON comp_rle.RLE_RELATIES
 FOR EACH ROW
DECLARE

-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeBasisGegevens
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 30-05-2014 EZW   Nieuw event salarismutatie voor aansluiting Bancs
l_event    xmltype;
cn_event_WgrBasgeg  constant varchar2(30) := 'wgr.GewijzigdeBasisGegevens';
BEGIN
:new.namrelatie :=  nls_initcap(:new.namrelatie
                               ,'NLS_SORT=XDUTCH'
                               );

--voorletters kunnen met of zonder puntjes worden ingevoerd, maar zonder puntjes opslaan
--dit geldt tevens voor spaties / jku / change 10860 / 16-08-2004
:new.voorletter := REPLACE(REPLACE(:new.voorletter,'.'),' ');
--voorletters alleen letters (met diacrieten)
IF :new.indinstelling = 'N'
AND (NOT rle_chk_voorletter_geldig(NVL(:new.voorletter,'X'))
        )
THEN
   raise_application_error(-20000,'Als voorletters mogen alleen letters (evt. met diacrieten) opgegeven worden');
END IF;
/* Begin toegevoegde code  */
IF :new.codorganisatie IS NULL
THEN
  :new.codorganisatie := 'A';
END IF;
IF :new.indgewnaam IS NULL
THEN
  :new.indgewnaam := 'N';
END IF;
IF :new.indinstelling IS NULL
THEN
  :new.indinstelling := 'N';
END IF;
IF :new.zoeknaam IS NULL
THEN
  :new.zoeknaam := CONVERT(UPPER(SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(:new.namrelatie,' & ',' EN ')
                                                               ,' &',' EN ')
                                                       ,'& ',' EN ')
                                               ,'&',' EN ')
                                       ,1,30)
                                )
                          ,'US7ASCII');
END IF;
IF :new.numrelatie IS NULL
THEN
  :new.numrelatie := rle_rle_seq1_next_id;
END IF;
IF :new.dwe_wrddom_gewatitel IS NOT NULL
THEN
  -- Controle referentiewaarde
  rfe_chk_refwrd('GWT'
                , :new.dwe_wrddom_gewatitel);
END IF;
IF :new.code_gebdat_fictief IS NOT NULL
THEN
  rfe_chk_refwrd('FGD'
                , :new.code_gebdat_fictief);
END IF;
--
IF  :new.indinstelling = 'N'
THEN
  IF :new.code_aanduiding_naamgebruik IS NULL
  THEN
    :new.code_aanduiding_naamgebruik := 'E';
  END IF;
  IF :new.dwe_wrddom_vvgsl IS NOT NULL
  THEN
  -- Controle referentiewaarde
     rfe_chk_refwrd('VVL'
                   , :new.dwe_wrddom_vvgsl);
  END IF;
  --
  -- RTJ 26-11-2003 nav call 91690 check op voorvoegsel partner
  --
  IF :new.vvgsl_partner IS NOT NULL
  THEN
  -- Controle referentiewaarde
     rfe_chk_refwrd('VVL'
                   , :new.vvgsl_partner);
  END IF;
  -- EINDE RTJ
  IF :new.dwe_wrddom_titel IS NOT NULL
  THEN
  -- Controle referentiewaarde
     rfe_chk_refwrd('TIL'
                   , :new.dwe_wrddom_titel);
  END IF;
  IF :new.dwe_wrddom_brgst IS NOT NULL
  THEN
    -- Controle referentiewaarde
    rfe_chk_refwrd('BST'
                  , :new.dwe_wrddom_brgst);
  END IF;
  IF :new.dwe_wrddom_ttitel IS NOT NULL
  THEN
   -- Controle referentiewaarde
    rfe_chk_refwrd('TTL'
	              ,:new.dwe_wrddom_ttitel);
  END IF;
  IF :new.dwe_wrddom_avgsl IS NOT NULL
  THEN
   -- Controle referentiewaarde
     rfe_chk_refwrd('AVL'
	               , :new.dwe_wrddom_avgsl);
  END IF;
  IF :new.dwe_wrddom_geslacht IS NOT NULL
  THEN
    -- Controle referentiewaarde
    rfe_chk_refwrd('GES'
                  , :new.dwe_wrddom_geslacht);
  END IF;
  /* Relatie is geen instelling */
  IF (:new.datoprichting IS NOT NULL      OR
      :new.dwe_wrddom_rvorm IS NOT NULL)
  THEN
    stm_dbproc.raise_error('RLE-00513',NULL,'RLE_RLE_BRI');
  END IF;
--
ELSE -- Controles voor instelling
  IF :new.dwe_wrddom_rvorm IS NOT NULL
  THEN
    -- Controle referentiewaarde
     rfe_chk_refwrd('REV'
                   , :new.dwe_wrddom_rvorm);
  END IF;
  IF  (:new.datgeboorte IS NOT NULL
    OR :new.datoverlijden IS NOT NULL
    OR :new.dwe_wrddom_geslacht IS NOT NULL
    OR :new.dwe_wrddom_brgst IS NOT NULL)
  THEN
    stm_dbproc.raise_error('RLE-00512',NULL,'RLE_RLE_BRI');
  END IF;
END IF;
/* QMS$DATA_AUDITING */
:new.DAT_CREATIE  := SYSDATE;
:new.CREATIE_DOOR := alg_sessie.gebruiker;
:new.DAT_MUTATIE  := SYSDATE;
:new.MUTATIE_DOOR := alg_sessie.gebruiker;
if :new.handelsnaam is not null
then
    l_event := eventhandler.create_event(p_event_code => cn_event_WgrBasgeg);
    eventhandler.add_num_value(p_event  => l_event
                              ,p_key    => 'RELATIENUMMER'
                              ,p_value  => :new.numrelatie);
    eventhandler.add_date_value( p_event => l_event
                               , p_key => 'DATUM_MUTATIE'
                               , p_value => :new.dat_mutatie
                               );
    eventhandler.publish_asynch_event(p_event => l_event);
end if;
END RLE_RLE_BRI;
/