CREATE OR REPLACE TRIGGER comp_rle.RLE_STT_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_STRATEN
DECLARE
/*****************************************************************************************************/
/* Trigger RLE.RLE_STT_AS:                                                                           */
/*                                                                                                   */
/*                                                                                                   */
/* Datum      Wie         Omschrijving                                                               */
/* ---------- ----------- ---------------------------------------------------------------------------*/
/* 02-09-2016 DFU         Creatie voor mutaties naar personen EVENT STORE                            */
/* 05-09-2016 OKR         Old_straatnaam voor tegenhouden schijnmutaties                             */
/*****************************************************************************************************/

   l_stack            VARCHAR2( 128 ) ;
   l_rowid            ROWID           ;
   l_job              VARCHAR2( 1 )   ;
   l_straatid         rle_straten.straatid%type;
   l_wps_woonplaatsid rle_straten.wps_woonplaatsid%type;
   l_straatnaam       rle_straten.straatnaam%type;
   l_old_straatnaam   rle_straten.straatnaam%type;
   --
BEGIN
   --
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   --
   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;

      /* Ophalen variabelen per soort statement */
      IF    l_job in ('U')
      THEN
         /* Ophalen variabelen voor update */

        l_straatid          := stm_get_word( l_stack ) ;
        l_wps_woonplaatsid  := stm_get_word( l_stack ) ;
        l_straatnaam        := stm_get_word( l_stack ) ;
        l_old_straatnaam    := stm_get_word( l_stack ) ;
      END IF ;

      /* Verwerking */

      --
      -- Wijzigingen van STRATEN moeten - mogelijk - worden gecommuniceerd naar de personen EVENT STORE
      -- Hier loggen we simpelweg updates STRATEN. Een subscriber beoordeelt of de UPDATE gecommuniceerd moet worden
      -- We hoeven alleen UPDATES te loggen. DELETES en INSERTS worden al getracked via RELATIE ADRESSEN
     if l_job = 'U'
       and not l_straatnaam = l_old_straatnaam -- schijnmutaties niet loggen naar event store
     then
       appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_stt(p_event_title => 'Adres gewijzigd', p_straatid => l_straatid);
       --
      end if;
      --
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
      --
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      RAISE ;
END ;
/