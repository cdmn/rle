CREATE OR REPLACE TRIGGER comp_rle.RLE_ADS_BS
 BEFORE DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_ADRESSEN

BEGIN
   stm_algm_muttab.init_array ;
END ;
/