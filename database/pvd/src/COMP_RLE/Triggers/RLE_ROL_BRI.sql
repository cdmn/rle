CREATE OR REPLACE TRIGGER comp_rle.RLE_ROL_BRI
 BEFORE INSERT
 ON comp_rle.RLE_ROLLEN
 FOR EACH ROW

DECLARE
-- Program Data
L_DOWA_INDVERVALLEN1 VARCHAR2(1) := 'N';
-- PL/SQL Block
BEGIN
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_ROL_BRI;
/