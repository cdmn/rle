CREATE OR REPLACE TRIGGER comp_rle.RLE_RRL_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIE_ROLLEN

DECLARE
-- Sub-Program Unit Declarations
CURSOR C_RRL_010
 (B_NUMRELATIE NUMBER
 ,B_CODROL VARCHAR2
 )
 IS
/* C_RRL_010 */
select '1'
from   rle_relatie_rollen rrl
,      rle_rollen rol
where  rrl.rle_numrelatie = b_numrelatie
and    rrl.rol_codrol = b_codrol
and   rol.indschonen = 'N'
/* (rol.indschonen = 'N'
    or rrl.rle_numrelatie not in
       ( select gre.rle_numrelatie
         from   rle_gebruik_relaties gre
         where  gre.rle_numrelatie = b_numrelatie
         and    gre.rol_codrol = b_codrol
         and (  gre.dateinde is null
             or gre.dateinde > sysdate)
       )
    )
*/
;
CURSOR C_RLE_029
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_029 */
   select rle.handelsnaam
     from rle_relaties rle
    where rle.numrelatie = b_numrelatie
        ;
-- Program Data
L_RRL_RLE_NUMRELATIE NUMBER(9);
L_CODE_SOORT_ADRES VARCHAR2(30);
L_DUMMY_CHAR VARCHAR2(1);
L_RRL_ROL_CODROL VARCHAR2(2);
L_OLD_DATSCHONING DATE;
L_SOORT_OVERWAT VARCHAR2(30);
L_NEW_DATSCHONING DATE;
L_OMS_POSTSOORT VARCHAR2(30);
L_ACTIE VARCHAR2(1);
R_RLE_029 C_RLE_029%ROWTYPE;
L_ROWID ROWID;
L_POSTSOORT VARCHAR2(30);
L_STACK VARCHAR2(128);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_RRL_AS */
stm_algm_muttab.get_rowid (l_rowid, l_stack);
WHILE l_rowid IS NOT NULL
LOOP
/* Ophalen gegevens (per rij) nodig om in de as_proc checks uit te voeren*/
  l_actie := stm_get_word (l_stack);
  IF l_actie <> 'D'
  THEN
     IF l_actie = 'U'
     THEN
        l_rrl_rle_numrelatie := TO_NUMBER( stm_get_word( l_stack));
        l_rrl_rol_codrol := stm_get_word( l_stack);
        l_old_datschoning := TO_DATE( stm_get_word( l_stack));
        l_new_datschoning := TO_DATE( stm_get_word( l_stack));
        --
        IF ( l_old_datschoning IS NULL  AND
             l_new_datschoning IS NOT NULL)
        OR ( l_old_datschoning <> l_new_datschoning)
        THEN
           rle_upd_fnr_einddat( l_rrl_rle_numrelatie
                              , l_rrl_rol_codrol
                              , l_old_datschoning
                              , l_new_datschoning);
           rle_upd_ras_einddat( l_rrl_rle_numrelatie
                              , l_rrl_rol_codrol
                              , l_old_datschoning
                              , l_new_datschoning);
           rle_upd_cnr_einddat( l_rrl_rle_numrelatie
                              , l_rrl_rol_codrol
                              , l_old_datschoning
                              , l_new_datschoning);
        END IF;
     ELSIF l_actie = 'I'
     THEN
        l_rrl_rle_numrelatie := TO_NUMBER( stm_get_word( l_stack));
        l_rrl_rol_codrol := stm_get_word( l_stack);
     /* BR-IER0035
     ** 30-08-2002, VER, Bevinding 198
     **                  Bij rol WG mag handelsnaam niet langer dan 35 pos. zijn
     */
     IF l_rrl_rol_codrol = 'WG'
     THEN
        OPEN c_rle_029 (l_rrl_rle_numrelatie);
        FETCH c_rle_029 INTO r_rle_029;
        IF LENGTH(r_rle_029.handelsnaam) > 35
        THEN
           CLOSE c_rle_029;
           stm_dbproc.raise_error('RLE-00357', NULL, 'RLE_RRL_AS');
        END IF;
        CLOSE c_rle_029;
     END IF;
     /* Einde BR-IER0035
     */
  ELSE
     l_rrl_rle_numrelatie := TO_NUMBER( stm_get_word( l_stack));
     l_rrl_rol_codrol := stm_get_word( l_stack);
     OPEN c_rrl_010( l_rrl_rle_numrelatie, l_rrl_rol_codrol);
     FETCH c_rrl_010 INTO l_dummy_char;
     IF c_rrl_010%found
     THEN
        CLOSE c_rrl_010;
        stm_dbproc.raise_error( 'RLE-00536', NULL, 'RLE_RRL_AS');
     ELSE
        CLOSE c_rrl_010;
        rle_rrl_del_childs( l_rrl_rle_numrelatie
                          , l_rrl_rol_codrol);
     END IF;
  END IF;
  END IF;
  stm_algm_muttab.get_rowid(l_rowid, l_stack);
END LOOP;
stm_algm_muttab.clear_array;
EXCEPTION
WHEN NO_DATA_FOUND THEN
   NULL;
WHEN OTHERS THEN
   stm_algm_muttab.clear_array;
   RAISE;
END RLE_RRL_AS;
/