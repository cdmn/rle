CREATE OR REPLACE TRIGGER comp_rle.RLE_RAS_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIE_ADRESSEN
DECLARE
  /******************************************************************************************************
  Naam:         RLE_RAS_AS
  Beschrijving: After statement trigger op tabel RLE_RELATIE_ADRESSEN

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  ??-??-????  ???  Creatie
  19-02-2008  PAS  GBA-aanpassing Zorgketen 2008
                   Aanroep naar rle_gba_muteren_adres of rle_gba_toevoegen_adres is ook nodig
                   voor personen die niet met het GBA afgestemd mogen worden. Middels deze aanroep(en) wordt
                   namelijk geregeld dat in de tabel per_aanmeldingen het adres wordt bijgewerkt.
                   Opmerking: Personen die niet mogen worden afgestemd hebben minimaal een OA adres
                              en in elk geval geen DA adres.
  25-07-2011  XCW  alleen communicatie nummers beeindigen van ras
  05-06-2012  JPG  Aanpassing ivm nazorg optimalisatie NP. Personen met allen CA adres worden niet afgestemd. Daarom
                   ook een OA adres toevoegen.
  21-06-2012  XSW  OA adres toevoegen indien het een persoon betreft
  01-09-2016  DFU/OKR Evenstore: Variabelen l_oldadmcode, l_indiener, t_persoonMutatie toegevoegd en publicatie
                   naar AQ persoonmutatie
  ******************************************************************************************************/
  --
L_OLDID NUMBER;
L_STACK VARCHAR2(128);
L_ROWID ROWID;
L_JOB VARCHAR2(1);
L_OLDNUMRELATIE NUMBER(9);
L_OLDCODROL VARCHAR2(2);
L_OLDDATINGANG DATE;
L_OLDDATEINDE DATE;
L_OLDINDINHOWN VARCHAR2(1) := 'N';
L_OLDSRTADR VARCHAR2(30);
L_OLDNUMADRES NUMBER(7, 0);
L_OLDADMCODE varchar2(12);--dfu
l_indiener varchar2(50) := NVL(SYS_CONTEXT('userenv', 'client_identifier'), USER); --dfu

l_adres_gevonden BOOLEAN; -- JPG 05-06-2012
  /* Ophalen van relatie-adres */
  CURSOR C_RAS_008
   (B_NUMRELATIE IN NUMBER
   ,B_SRTADRES IN VARCHAR2
   ,B_CODROL IN VARCHAR2
   /*   toevoeging pge vanwege introductie adm_code */
   ,B_ADM_CODE IN VARCHAR2
   /*   einde toevoeging pge */
   )
   IS
  /* C_RAS_008
     HVI 20021009: Aangepast om * in cursor te vermijden en daarom
                   hard in Pack. Spec. gezet ipv als Sub Program Unit
  */
  select  ras.id
       ,  ras.datingang
       ,  ras.dateinde
  from    rle_relatie_adressen ras
  where   ras.rle_numrelatie = b_numrelatie
  and     ras.dwe_wrddom_srtadr  = b_srtadres
  and     nvl(ras.rol_codrol,'@') = nvl(b_codrol,'@')
  /*   toevoeging pge vanwege introductie adm_code */
  and     ras.adm_code = b_adm_code
  /*   einde toevoeging pge */
  ;
  /* Ophalen van eerste RAS van een bepaalde relatie,srt ...
     HVI 20021009: Hard in Pack. Spec. gezet ipv als Sub Program Unit
  */
  CURSOR C_RAS_027
   (B_ID IN NUMBER
   ,B_RLE_NUMRELATIE IN NUMBER
   ,B_ROL_CODROL IN VARCHAR2
   ,B_DWE_WRDDOM_SRTADR IN VARCHAR2
   /*   toevoeging pge vanwege introductie adm_code */
   ,B_ADM_CODE IN VARCHAR2
   /*   einde toevoeging pge */
   )
   IS
  /* C_RAS_027 */
  select   ras.id, ras.datingang,ras.dateinde
  from     rle_relatie_adressen ras
  where    ras.id                  <> b_id
    and    ras.rle_numrelatie      =  b_rle_numrelatie
    and    ras.dwe_wrddom_srtadr   =  b_dwe_wrddom_srtadr
    and    nvl(ras.rol_codrol,'@') =  nvl(b_rol_codrol,'@')
    /*   toevoeging pge vanwege introductie adm_code */
    and     ras.adm_code = b_adm_code
    /*   einde toevoeging pge */
  order by ras.datingang desc;
  /* HVI 20021009: Cursoren uit PL/SQL block gehaald en in
                   Pack. Spec. gezet conform standaard.
  */
  CURSOR c_ras_000(b_rowid ROWID)
  IS
     SELECT     *
     FROM       rle_relatie_adressen ras
     WHERE      ras.rowid = b_rowid
  ;
  CURSOR c_ras_018(b_numadres NUMBER)
  IS
     SELECT     *
     FROM       rle_relatie_adressen ras
     WHERE      ras.ads_numadres = b_numadres
  ;
  CURSOR c_ras_prov_en_land(cpr_rowid  IN  ROWID)
  IS
     SELECT     ras.provincie
          ,     ads.lnd_codland
     FROM       rle_relatie_adressen  ras
        ,       rle_adressen          ads
     WHERE      ras.rowid        = cpr_rowid
     AND        ras.ads_numadres = ads.numadres
  ;
  CURSOR c_srtadr_en_straatnaam(cpr_rowid  IN  ROWID)
  IS
     SELECT     ras.dwe_wrddom_srtadr
          ,     stt.straatnaam
     FROM       rle_relatie_adressen  ras
        ,       rle_adressen          ads
        ,       rle_straten           stt
     WHERE      ras.rowid        = cpr_rowid
     AND        ads.numadres     = ras.ads_numadres
     AND        stt.straatid     = ads.stt_straatid
  ;
  --
  r_srtadr_en_straatnaam  c_srtadr_en_straatnaam%ROWTYPE;
  r_ras_027               c_ras_027%rowtype;
  r_ras_000               c_ras_000%rowtype;
  r_ras_018               c_ras_018%rowtype;
  r_ras_prov_en_land      c_ras_prov_en_land%ROWTYPE;
  --
  --
BEGIN
  stm_algm_muttab.get_rowid (l_rowid, l_stack);
  WHILE l_rowid IS NOT NULL
  LOOP
    l_job  := stm_get_word (l_stack);
    stm_util.debug('l_stack = '||l_stack);
    stm_util.debug('l_job   = '||l_job);
    stm_util.debug('l_rowid = '||l_rowid);
    -- Ophalen gegevens (per rij)
    IF l_job  <> 'D'
    THEN
      OPEN c_ras_000(l_rowid);
      FETCH c_ras_000 INTO r_ras_000;
      CLOSE c_ras_000;
      --
      stm_util.debug('r_ras_000.id                = '||r_ras_000.id);
      stm_util.debug('r_ras_000.datingang         = '||to_char(r_ras_000.datingang, 'dd-mm-yyyy'));
      stm_util.debug('r_ras_000.dwe_wrddom_srtadr = '||r_ras_000.dwe_wrddom_srtadr);
      --
    END IF;
    IF l_job <> 'I'
    THEN
       l_olddatingang  := TO_DATE(stm_get_word(l_stack));
       l_olddateinde   := TO_DATE(stm_get_word(l_stack));
       l_oldindinhown  := stm_get_word (l_stack);
       l_oldnumrelatie := stm_get_word (l_stack);
       l_oldsrtadr     := stm_get_word (l_stack);
       l_oldcodrol     := stm_get_word (l_stack);
       l_oldnumadres   := stm_get_word (l_stack);
       l_oldid         := TO_NUMBER( stm_get_word( l_stack ) ) ;
       L_OLDADMCODE    := stm_get_word( l_stack);--dfu
    END IF;

    IF l_job = 'I'
    THEN
      /*Bij toevoegen nieuw voorkomen, afsluiten voorliggende
      voorkomen of nieuwrecord afsluiten als er latere voorkomens zijn
      selecteer laatste record
      */
      OPEN c_ras_027(r_ras_000.id
                    ,r_ras_000.rle_numrelatie
                    ,r_ras_000.rol_codrol
                    ,r_ras_000.dwe_wrddom_srtadr
                    /*   toevoeging pge vanwege introductie adm_code */
                    ,r_ras_000.adm_code
                    /*   einde toevoeging pge */
                    );
      FETCH c_ras_027 INTO r_ras_027;
      IF c_ras_027%found
      THEN
        --kijken of nieuwe record na dit laatste record valt
        IF r_ras_027.dateinde IS NULL
        THEN
          --afsluiten is nodig
          IF   r_ras_000.datingang > r_ras_027.datingang
          THEN
            -- erna, dus afsluiten laatste record
            -- afsluiten laatste record op begindatum -1 van
            -- nieuwe record.
            UPDATE rle_relatie_adressen ras
            SET    ras.dateinde    = r_ras_000.datingang - 1
            ,      ras.indinhown    = 'J'
            WHERE  ras.id           = r_ras_027.id;
          END IF;
        END IF;
      END IF;
      CLOSE c_ras_027;
    end if;

    /* Bij wijzigen begindatum */
    IF  l_job = 'U'
    AND l_olddatingang <> r_ras_000.datingang
    THEN
      UPDATE  rle_relatie_adressen ras
      SET    ras.dateinde            = r_ras_000.datingang - 1
      WHERE  ras.id                 <> r_ras_000.id
      AND    ras.rle_numrelatie      = r_ras_000.rle_numrelatie
      AND    ras.dwe_wrddom_srtadr   = r_ras_000.dwe_wrddom_srtadr
      AND    NVL(ras.rol_codrol,'@') = NVL(r_ras_000.rol_codrol,'@')
      AND    ras.indinhown           = 'J'
      AND    ras.dateinde            = l_olddatingang - 1
      /*   toevoeging pge vanwege introductie adm_code */
      AND    ras.adm_code            = r_ras_000.adm_code
      /*   einde toevoeging pge */
      ;
    END IF;
    --
    -- JPG 05-06-2012
    -- BR CEV0030 RAS
    -- Bij opvoer van een binnenlands CA adres, moet ook automatisch een OA adres opgevoerd worden indien dit ontbreekt bij de relatie.
    -- XSW 21-06-2012 bovenstaande alleen bij personen
    --
    IF l_job = 'I' and r_ras_000.rol_codrol = 'PR'
    THEN
      IF r_ras_000.dwe_wrddom_srtadr = 'CA'
      THEN
        --
        -- Controleer of de relatie al een OA of DA adres heeft
        --
        OPEN c_ras_027(r_ras_000.id
                      ,r_ras_000.rle_numrelatie
                      ,r_ras_000.rol_codrol
                      ,'DA'
                      ,r_ras_000.adm_code
                       );
        FETCH c_ras_027 INTO r_ras_027;

        l_adres_gevonden := c_ras_027%FOUND;

        CLOSE c_ras_027;

        IF NOT l_adres_gevonden
        THEN
          OPEN c_ras_027(r_ras_000.id
                        ,r_ras_000.rle_numrelatie
                        ,r_ras_000.rol_codrol
                        ,'OA'
                        ,r_ras_000.adm_code
                        );
          FETCH c_ras_027 INTO r_ras_027;

          l_adres_gevonden := c_ras_027%FOUND;

          CLOSE c_ras_027;
        END IF;

        IF NOT l_adres_gevonden
        THEN
          INSERT INTO rle_relatie_adressen
            (  id
             , rle_numrelatie
             , rol_codrol
             , dwe_wrddom_srtadr
             , datingang
             , dateinde
             , ads_numadres
             , provincie
             , locatie
             , ind_woonboot
             , ind_woonwagen
             , indinhown
             , adm_code
             , numkamer
             , codorganisatie
             )
           VALUES
             ( rle_ras_seq1.nextval
             , r_ras_000.rle_numrelatie
             , r_ras_000.rol_codrol
             , 'OA'
             , r_ras_000.datingang
             , r_ras_000.dateinde
             , r_ras_000.ads_numadres
             , r_ras_000.provincie
             , r_ras_000.locatie
             , r_ras_000.ind_woonboot
             , r_ras_000.ind_woonwagen
             , r_ras_000.indinhown
             , r_ras_000.adm_code
             , r_ras_000.numkamer
             , r_ras_000.codorganisatie
             );
          --
          INSERT INTO rle_relatie_adressen
            (  id
             , rle_numrelatie
             , rol_codrol
             , dwe_wrddom_srtadr
             , datingang
             , dateinde
             , ads_numadres
             , provincie
             , locatie
             , ind_woonboot
             , ind_woonwagen
             , indinhown
             , adm_code
             , numkamer
             , codorganisatie
             )
           VALUES
             ( rle_ras_seq1.nextval
             , r_ras_000.rle_numrelatie
             , r_ras_000.rol_codrol
             , 'DA'
             , r_ras_000.datingang
             , r_ras_000.dateinde
             , r_ras_000.ads_numadres
             , r_ras_000.provincie
             , r_ras_000.locatie
             , r_ras_000.ind_woonboot
             , r_ras_000.ind_woonwagen
             , r_ras_000.indinhown
             , r_ras_000.adm_code
             , r_ras_000.numkamer
             , r_ras_000.codorganisatie
             );
        END IF;
      END IF; --  r_ras_000.dwe_wrddom_srtadr = 'CA'

    END IF;
    /* PWA 6-12-2004 Tevens updaten van het communicatie_nummer bij het wijzigen van einddatum */
    IF  l_job = 'U'
    AND NVL(l_olddateinde,TO_DATE('31-12-3000','dd-mm-yyyy')) <> r_ras_000.dateinde
    THEN
       null;
       /* xcw 18-08-2011 communicatie nummers van ras hangen aan relatie
       UPDATE rle_communicatie_nummers com
       SET    com.ras_id           = null
       WHERE  com.rle_numrelatie   = r_ras_000.rle_numrelatie
       AND    com.ras_id           = r_ras_000.id
       AND    com.dateinde         is null; */
    END IF;
    /* Bij verwijderen
     * als een ras verwijderd wordt waarvan de einddatum naar een broeder record
     * overerft was, dan schuift de einddatum van de broeder op naar de
     * einddatum van het verwijderde record.
     */
    IF l_job = 'D'
    THEN
      UPDATE rle_relatie_adressen ras
      SET    ras.dateinde            = l_olddateinde
      ,      ras.indinhown           = l_oldindinhown
      WHERE  ras.rle_numrelatie      = l_oldnumrelatie
      AND    ras.dwe_wrddom_srtadr   = l_oldsrtadr
      AND    NVL(ras.rol_codrol,'@') = NVL(l_oldcodrol,'@')
      AND    ras.dateinde            = l_olddatingang - 1
      AND    ras.indinhown           = 'J';
      /* controleren of adres nog door een andere ras wordt gebruikt.
       * Zo nee, dan ook adres weggooien
       */
      DELETE FROM rle_communicatie_nummers
      WHERE  rle_numrelatie = l_oldnumrelatie
      AND    ras_id  = l_oldid;
      /* controleren of adres nog door een andere ras wordt gebruikt.
       * Zo nee, dan ook adres weggooien
       */
      OPEN c_ras_018(l_oldnumadres);
      FETCH c_ras_018 INTO r_ras_018;
      IF c_ras_018%notfound
      THEN
        DELETE FROM rle_adressen
        WHERE numadres = l_oldnumadres;
      END IF;
      CLOSE c_ras_018;
    END IF;
    IF l_job <> 'D'
    THEN
      -- Overlap check met "Broederrecords"
      FOR r_ras_008 IN c_ras_008( r_ras_000.rle_numrelatie
                                , r_ras_000.dwe_wrddom_srtadr
                                , r_ras_000.rol_codrol
                                /*   toevoeging pge vanwege introductie adm_code */
                                ,r_ras_000.adm_code
                                /*   einde toevoeging pge */
                                )
      LOOP
        IF r_ras_000.id <> r_ras_008.id
        THEN
          STM_ALGM_CHECK.ALGM_DATOVLP( r_ras_000.datingang
                                     , r_ras_000.dateinde
                                     , r_ras_008.datingang
                                     , r_ras_008.dateinde);
        END IF;
      END LOOP;
      rle_ras_chk_aanw( r_ras_000);
    END IF ;

    /* Business rule BRATT0018
       Provincie mag alleen gevuld zijn bij een buitenlands adres
    */
    IF l_job IN ( 'I', 'U' )
    THEN
       OPEN c_ras_prov_en_land( l_rowid ) ;
       FETCH c_ras_prov_en_land INTO r_ras_prov_en_land ;
       CLOSE c_ras_prov_en_land ;

       IF r_ras_prov_en_land.provincie   IS NOT NULL  AND
          r_ras_prov_en_land.lnd_codland =  'NL'
       THEN
          stm_dbproc.raise_error( 'RLE-10282' ) ;
       END IF ;
    END IF ;
    /* End BRATT0018 */

      /* Ophalen gegevens voor
         BRIER0009
         BRIER0027
      */ /* */
      IF l_job IN ( 'I', 'U' )
      THEN
         OPEN c_srtadr_en_straatnaam( l_rowid ) ;
         FETCH c_srtadr_en_straatnaam INTO r_srtadr_en_straatnaam ;
         CLOSE c_srtadr_en_straatnaam ;
      END IF ;

      /* BRIER0027
         Voorkomen dat als in de naam van de STRAAT postbus of antwoordnummer
         ingevuld staat de indicatie woonboot of indicatie woonwagen aangevinkt
         wordt of dat de locatie gevuld wordt.
      */ /* */
      IF l_job IN ( 'I', 'U' )
      THEN
         IF  (   UPPER( LTRIM( RTRIM( r_srtadr_en_straatnaam.straatnaam ) ) ) = 'ANTWOORDNUMMER'
             OR  UPPER( LTRIM( RTRIM( r_srtadr_en_straatnaam.straatnaam ) ) ) = 'POSTBUS'
             )
         AND (   r_ras_000.ind_woonboot  =  'J'
             OR  r_ras_000.ind_woonwagen =  'J'
             OR  r_ras_000.locatie       IS NOT NULL
             )
         THEN
            stm_dbproc.raise_error( 'RLE-10347' ) ;
         END IF ;
      END IF ;
      --
      /* Toegevoegd nav RRGG:
      BR ENT0011 Een persoon of een werkgever moet altijd een lopend adres hebben
      */
       if l_job = 'I'
       and r_ras_000.rol_codrol = 'PR'
       and r_ras_000.dwe_wrddom_srtadr = 'OA'
       then
          /* IN dit geval kan het een eerste opvoer van en adres
          betreffen. ER is dan nog GEEN DA adres aanwezig,
          dus kan controle niet uitgevoerd worden.
          */
           OPEN c_ras_027(r_ras_000.id
                         ,r_ras_000.rle_numrelatie
                         ,r_ras_000.rol_codrol
                         ,r_ras_000.dwe_wrddom_srtadr
                         /*   toevoeging pge vanwege introductie adm_code */
                         ,r_ras_000.adm_code
                         /*   einde toevoeging pge */
                         );
           FETCH c_ras_027 INTO r_ras_027;
           IF c_ras_027%found
           then
              rle_rle_chk_adres(r_ras_000.rle_numrelatie
                               ,r_ras_000.rol_codrol);
            end if;
            close c_ras_027;
       --
       else
           -- 12-11-2008 AAM Nvl toegevoegd, zodat de controle ook goed gaat bij een delete
           rle_rle_chk_adres(nvl(r_ras_000.rle_numrelatie,l_oldnumrelatie)
                            ,nvl(r_ras_000.rol_codrol,l_oldcodrol));
       end if;

       /* CHANGE-REC-U: U_GBA_011
         Toevoegen adressen van personen
      */ /* */

     IF (
          (     r_ras_000.rol_codrol        = 'PR'
            AND r_ras_000.dwe_wrddom_srtadr = 'DA'
          )
        OR
          /* 19-02-2007 PAS GBA-aanpassing Zorgketen 2008  */
          (     r_ras_000.rol_codrol        = 'PR'
            AND r_ras_000.dwe_wrddom_srtadr = 'OA'
            AND rle_chk_gba_afst( p_rle_numrelatie => r_ras_000.rle_numrelatie
                                , p_persoonsnummer => NULL
                                ) = 'N'
          )
        )
     THEN
      IF l_job = 'I'
      THEN
         rle_gba_toevoegen_adres( r_ras_000.id ) ;

      /* CHANGE-REC-U: U_GBA_013
         Muteren adressen van personen
      */ /* */
      ELSIF l_job = 'U'
      THEN
        --
        -- xcw 30-10-2013: bij afsluiten adres niet de gegevens doorsturen naar gba.
        -- in gba-c wordt einddatum toch niet gebruikt en hiermee voorkom je schijnmutaties naar gba.
        -- het afsluiten wordt toch gevolgd door nieuw adres.
        if r_ras_000.dateinde is null
        then
         rle_gba_muteren_adres( r_ras_000.id ) ;
        end if;
      ELSE
        NULL;
      END IF ;
     END IF;
     --
     stm_util.debug('l_stack        = '||l_stack);
     stm_util.debug('l_oldcodrol    = '||l_oldcodrol);
     stm_util.debug('l_olddatingang = '||l_olddatingang);
     stm_util.debug('l_oldsrtadr    = '||l_oldsrtadr);
     stm_util.debug('l_indiener     = '||l_indiener);
     --
     -- DFU/OKR Wijzigingen voor eventstore
     if nvl(r_ras_000.rol_codrol, l_oldcodrol) = 'PR'
     then
       -- Allereerst: detectie van schijnmutaties
       if l_job = 'U'
          and
		  l_oldnumrelatie = r_ras_000.rle_numrelatie
          and
          l_olddatingang = r_ras_000.datingang
          and
          l_oldsrtadr = r_ras_000.dwe_wrddom_srtadr
          and
          l_oldadmcode = r_ras_000.adm_code
          and
          nvl(to_char(l_olddateinde, 'YYYYMMDD'), '__null__') = nvl(to_char(r_ras_000.dateinde, 'YYYYMMDD'), '__null__')
          and
          l_oldnumadres = r_ras_000.ads_numadres
          and
          nvl(l_oldcodrol, '__null__') = nvl(r_ras_000.rol_codrol, '__null__')
       then
         -- Schijnmutatie: niet loggen naar event store
         null;
         --
       elsif l_job = 'U' and (
                              (l_oldnumrelatie <> r_ras_000.rle_numrelatie)
                               or
                              (l_olddatingang <> r_ras_000.datingang)
                               or
                              (l_oldsrtadr <> r_ras_000.dwe_wrddom_srtadr)
                          )
       then
         -- wijzigingen van functionele sleutel rapporten als DELETE en INSERT (in plaats van als UPDATE)
         -- dit maakt synchroniseren naar de event store veel gemakkelijker - anders zou de event store
         -- technische sleutels uit RLE moeten gaan bijhouden
         --
         -- DELETE:
         appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_ras(p_event_title => 'Adres gewijzigd'
                                                                        ,p_relatienr => l_oldnumrelatie
                                                                        ,p_ads_numadres => l_oldnumadres
                                                                        ,p_soort_adres => l_oldsrtadr
                                                                        ,p_begindatum => l_olddatingang
                                                                        ,p_einddatum => l_olddateinde
                                                                        ,p_administratie => l_oldadmcode
                                                                        ,p_dml_actie => 'D'
                                                                        );

         -- INSERT:
         appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_ras(p_event_title => 'Adres gewijzigd'
                                                                        ,p_relatienr => r_ras_000.rle_numrelatie
                                                                        ,p_ads_numadres => r_ras_000.ads_numadres
                                                                        ,p_soort_adres => r_ras_000.dwe_wrddom_srtadr
                                                                        ,p_begindatum => r_ras_000.datingang
                                                                        ,p_einddatum => r_ras_000.dateinde
                                                                        ,p_administratie => r_ras_000.adm_code
                                                                        ,p_dml_actie => 'I'
                                                                        );
       --
       elsif l_job in ( 'I', 'U')
       then
         appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_ras(p_event_title => 'Adres gewijzigd'
                                                                        ,p_relatienr => r_ras_000.rle_numrelatie
                                                                        ,p_ads_numadres => r_ras_000.ads_numadres
                                                                        ,p_soort_adres => r_ras_000.dwe_wrddom_srtadr
                                                                        ,p_begindatum => r_ras_000.datingang
                                                                        ,p_einddatum => r_ras_000.dateinde
                                                                        ,p_administratie => r_ras_000.adm_code
                                                                        ,p_dml_actie => l_job
                                                                        );
       --
       elsif l_job = 'D'
       then
         appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_ras(p_event_title => 'Adres gewijzigd'
                                                                        ,p_relatienr => l_oldnumrelatie
                                                                        ,p_ads_numadres => l_oldnumadres
                                                                        ,p_soort_adres => l_oldsrtadr
                                                                        ,p_begindatum => l_olddatingang
                                                                        ,p_einddatum => l_olddateinde
                                                                        ,p_administratie => l_oldadmcode
                                                                        ,p_dml_actie => 'D'
                                                                        );
       --
       end if;
       --
     end if;
     --
     stm_algm_muttab.get_rowid (l_rowid, l_stack);
     --
  END LOOP;
  stm_algm_muttab.clear_array;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    NULL;
  WHEN OTHERS THEN
    stm_algm_muttab.clear_array;
    RAISE;
END RLE_RAS_AS;
/