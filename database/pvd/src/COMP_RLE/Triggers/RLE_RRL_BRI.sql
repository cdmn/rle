CREATE OR REPLACE TRIGGER comp_rle.RLE_RRL_BRI
 BEFORE INSERT
 ON comp_rle.RLE_RELATIE_ROLLEN
 FOR EACH ROW

BEGIN
BEGIN
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := SYSDATE;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END ;
/* Publicatie voor synchronisatie */
BEGIN
   --
   -- 05-11-2002, VER, RLE_ROLLEN worden niet gesynchroniseerd.
   -- 18-01-2005, PWA, RLE_ROLLEN worden wel gesynchroniseerd: t.b.v. Chg-18331.
   -- 23-03-2005, RBT, SAS70 : geen onderscheid in rollen maken bij publicatie
   --
    IBM.PUBLISH_EVENT
      ( 'MUT RLE ROLLEN'
      , 'I'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.RLE_NUMRELATIE
      , NULL, :new.ROL_CODROL
      , NULL, :new.CODORGANISATIE
      , NULL, :new.CODPRESENTATIE
      , NULL, to_char(:new.DATSCHONING, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.CREATIE_DOOR
      , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.MUTATIE_DOOR
      );
END;
END;
/