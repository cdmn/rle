CREATE OR REPLACE TRIGGER comp_rle.RLE_GBD_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_GEMOEDSBEZWAARDHEDEN
 FOR EACH ROW
/*
-- 19-02-2015 ZSF Extra attribuut voor event rle.GemoedsbezwaardPersoonGemuteerd
17-11-2014 ZSF nieuw event voor aansluiting Bancs
*/
BEGIN
   /* QMS$DATA_AUDITING */
   :new.DAT_MUTATIE  := SYSDATE ;
   :new.MUTATIE_DOOR := USER ;

   /* Publicatie voor synchronisatie */
   IBM.PUBLISH_EVENT
      ( 'MUT GEMOEDSBEZWAARDHEDEN'
      , 'U'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.RLE_NUMRELATIE                               , :new.RLE_NUMRELATIE
      , to_char(:old.DAT_BEGIN, 'dd-mm-yyyy hh24:mi:ss')  , to_char(:new.DAT_BEGIN, 'dd-mm-yyyy hh24:mi:ss')
      , to_char(:old.DAT_EINDE, 'dd-mm-yyyy hh24:mi:ss')  , to_char(:new.DAT_EINDE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.CREATIE_DOOR                                 , :new.CREATIE_DOOR
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.MUTATIE_DOOR                                 , :new.MUTATIE_DOOR
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
      );
--
    declare
      l_event xmltype;
    begin
      if (  :new.dat_begin  <= sysdate
         and :new.dat_einde <= sysdate -- zijn alleen geinteresseerd als het NU geen gemoedsbezwaar is
         )
      then
        l_event := eventhandler.create_event ( p_event_code => 'rle.GemoedsbezwaardPersoonGemuteerd' );
        eventhandler.add_num_value( p_event => l_event
                                  , p_key => 'RELATIENUMMER'
                                  , p_value => :new.rle_numrelatie
                                  );
        eventhandler.add_value( p_event => l_event
                              , p_key => 'INDICATIE'
                              , p_value => 'N'
                              );
        eventhandler.add_value( p_event => l_event
                              , p_key => 'CODROL'
                              , p_value => 'PR'
                              );
        eventhandler.add_date_value( p_event => l_event
                                   , p_key => 'DATUM_MUTATIE'
                                   , p_value => :new.dat_mutatie
                                   );
        eventhandler.publish_asynch_event ( p_event => l_event );
      end if;
    end;
END ;
/