CREATE OR REPLACE TRIGGER comp_rle.RLE_CNR_BRD
 BEFORE DELETE
 ON comp_rle.RLE_COMMUNICATIE_NUMMERS
 FOR EACH ROW
DECLARE

-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeBasisGegevens
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 30-05-2014 EZW   Nieuw event salarismutatie voor aansluiting Bancs
cn_event_WgrBasgeg  constant varchar2(30) := 'wgr.GewijzigdeBasisGegevens';
l_event xmltype;
l_relnum number;
cursor c_rec (b_relnum number)
is
select rle_numrelatie
from rle_relatie_externe_codes rec
where rec.rle_numrelatie = :new.rle_numrelatie
and rec.dwe_wrddom_extsys = 'WGR'
and rec.rle_numrelatie = b_relnum;
BEGIN
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
   rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);
/* Publicatie voor synchronisatie */
BEGIN
   IBM.PUBLISH_EVENT
      ( 'MUT RLE COMMUNICATIE NRS'
      , 'D'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.RAS_ID                                       , NULL
      , :old.ID                                           , NULL
      , :old.NUMCOMMUNICATIE                              , NULL
      , :old.RLE_NUMRELATIE                               , NULL
      , :old.DWE_WRDDOM_SRTCOM                            , NULL
      , :old.CODORGANISATIE                               , NULL
      , to_char(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , NULL
      , :old.INDINHOWN                                    , NULL
      , to_char(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , NULL
      , :old.ROL_CODROL                                   , NULL
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.CREATIE_DOOR                                 , NULL
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.MUTATIE_DOOR                                 , NULL
      );
END;
begin
  if :old.dwe_wrddom_srtcom in ('FAX','TEL','EMAIL')
  then
    open c_rec(:old.rle_numrelatie);
    fetch c_rec into l_relnum;
    if c_rec%found
    then
      l_event := eventhandler.create_event(p_event_code => cn_event_WgrBasgeg);
      eventhandler.add_num_value(p_event  => l_event
                                ,p_key    => 'RELATIENUMMER'
                                ,p_value  => :old.rle_numrelatie);
      eventhandler.add_date_value( p_event => l_event
                                 , p_key => 'DATUM_MUTATIE'
                                 , p_value => :old.dat_mutatie
                                 );
      eventhandler.publish_asynch_event(p_event => l_event);
    end if;
    close c_rec;
  end if;
exception
  when others then
    null;
end;
END;
/