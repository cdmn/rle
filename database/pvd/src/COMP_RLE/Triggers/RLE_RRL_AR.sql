CREATE OR REPLACE TRIGGER comp_rle.RLE_RRL_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIE_ROLLEN
 FOR EACH ROW

DECLARE
-- Program Data
L_STACK VARCHAR2(128);
-- PL/SQL Block
BEGIN
/* RLE_RRL_AR */
/* Per rij de bewerking meegeven */
IF	inserting THEN
	stm_add_word (l_stack, 'I');
	stm_add_word (l_stack, TO_CHAR(:new.rle_numrelatie));
	stm_add_word (l_stack, :new.rol_codrol);
ELSIF	updating THEN
	stm_add_word (l_stack, 'U');
	stm_add_word (l_stack, TO_CHAR(:old.rle_numrelatie));
	stm_add_word (l_stack, :old.rol_codrol);
	stm_add_word (l_stack, TO_CHAR(:old.datschoning));
	stm_add_word (l_stack, TO_CHAR(:new.datschoning));
ELSIF	deleting THEN
	stm_add_word (l_stack, 'D');
	stm_add_word (l_stack, TO_CHAR(:old.rle_numrelatie));
	stm_add_word (l_stack, :old.rol_codrol);
ELSE
	stm_add_word (l_stack, ' ');
END IF;
/* per rij de benodigde old values meegeven */
/* vullen rowid en linkstring in de interne tabel */
IF	deleting THEN
	stm_algm_muttab.add_rowid(:old.rowid, l_stack);
ELSE
	stm_algm_muttab.add_rowid(:new.rowid, l_stack);
END IF;
END RLE_RRL_AR;
/