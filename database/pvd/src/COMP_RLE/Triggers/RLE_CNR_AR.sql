CREATE OR REPLACE TRIGGER comp_rle.RLE_CNR_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_COMMUNICATIE_NUMMERS
 FOR EACH ROW

DECLARE
-- Program Data
L_STACK VARCHAR2(128);
-- PL/SQL Block
BEGIN
/* RLE_CNR_AR */
  /* Per rij de bewerking meegeven */
  IF	inserting THEN
  	stm_add_word (l_stack, 'I');
  ELSIF	updating THEN
  	stm_add_word (l_stack, 'U');
/*  ELSIF	deleting THEN
  	rle_rme_insert(:old.rle_numrelatie
	,   :old.id
  	,	:old.rol_codrol
  	,	'RLE133'
  	,	'3'
  	,	:old.dwe_wrddom_srtcom
  	,	:old.datingang
  	,	NULL
  	,	:old.numcommunicatie
  	,	NULL);
  	stm_add_word (l_stack, 'D');
*/
  ELSE
  	stm_add_word (l_stack, ' ');
  END IF;
  /* per rij de benodigde old values meegeven */
  IF	NOT inserting THEN
  	stm_add_word (l_stack, TO_CHAR(:old.datingang));
  	stm_add_word (l_stack, TO_CHAR(:old.dateinde));
  	stm_add_word (l_stack, :old.indinhown);
  	stm_add_word (l_stack, :old.rle_numrelatie);
  	stm_add_word (l_stack, :old.rol_codrol);
  	stm_add_word (l_stack, :old.dwe_wrddom_srtcom);
                   stm_add_word (l_stack, :old.numcommunicatie);
                   stm_add_word (l_stack, :old.ras_id);
  END IF;
  /* vullen rowid en linkstring in de interne tabel */
  IF	deleting THEN
  	stm_algm_muttab.add_rowid(:old.rowid, l_stack);
  ELSE
  	stm_algm_muttab.add_rowid(:new.rowid, l_stack);
  END IF;
END RLE_CNR_AR;
/