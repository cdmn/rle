CREATE OR REPLACE TRIGGER comp_rle.RLE_KVR_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_KANAALVOORKEUREN
BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
   l_rowid  ROWID           ;
   l_job    VARCHAR2( 1 )   ;
   /* Cursor voor ophalen huidige record met het ROWID */
   CURSOR lc_kvr(
        cpr_rowid  IN  ROWID
      ) IS
      SELECT *
      FROM   rle_kanaalvoorkeuren  kvr
      WHERE  kvr.rowid = cpr_rowid
      ;
   lr_kvr  lc_kvr%ROWTYPE ;
   /* Cursor voor controle overlappende records */
   CURSOR lc_kvr_2(
        cpn_numrelatie  IN  rle_kanaalvoorkeuren.rle_numrelatie%TYPE
      , cpn_cck_id    IN  rle_kanaalvoorkeuren.cck_id%TYPE
      ) IS
      SELECT kvr.rle_numrelatie
      ,      kvr.dat_begin
      ,      kvr.dat_einde
      ,      kvr.cck_id
      FROM   rle_kanaalvoorkeuren  kvr
      WHERE  kvr.rle_numrelatie = cpn_numrelatie
      and    kvr.cck_id         = cpn_cck_id
      ;
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;
      /* Ophalen variabelen per soort statement */
      IF    l_job = 'I'
      THEN
         /* Ophalen variabelen voor insert */
         NULL ;
      ELSIF l_job = 'U'
      THEN
         /* Ophalen variabelen voor update */
         NULL ;
      ELSIF l_job = 'D'
      THEN
         /* Ophalen variabelen voor delete */
         NULL ;
      END IF ;
      /* Ophalen huidige record */
      IF l_job IN ( 'I', 'U' )
      THEN
         OPEN lc_kvr( l_rowid ) ;
         FETCH lc_kvr INTO lr_kvr ;
         CLOSE lc_kvr ;
      END IF ;
      /* BRATT0024
         Soort kanaalvoorkeur moet voorkomen in referentie
      */
      IF l_job IN ( 'I', 'U' )
      THEN
        null;
/*         ibm.ibm( 'CHK_REFWRD'
                , 'RLE'
                , 'KVR'
                , lr_kvr.code_soort_kanaalvoorkeur
                ) ; */
      END IF ;
      /* BRENT0013
         Voorkomen overlap bij kanaalvoorkeuren
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         FOR ir_kvr_2 IN lc_kvr_2( lr_kvr.rle_numrelatie, lr_kvr.cck_id )
         LOOP
            IF ir_kvr_2.rle_numrelatie != lr_kvr.rle_numrelatie  OR
               ir_kvr_2.cck_id         != lr_kvr.cck_id  OR
               ir_kvr_2.dat_begin      != lr_kvr.dat_begin
            THEN
               stm_algm_check.algm_datovlp( lr_kvr.dat_begin
                                          , lr_kvr.dat_einde
                                          , ir_kvr_2.dat_begin
                                          , ir_kvr_2.dat_einde
                                          ) ;
            END IF ;
         END LOOP ;
      END IF ;
      /* BRIER0023
         Overlap kanaalvoorkeur t.o.v. relatie
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         rle_chk_rle_persdat( lr_kvr.rle_numrelatie
                            , lr_kvr.dat_begin
                            , 'kanaalvoorkeuren'
                            ) ;
      END IF ;
      /* Ophalen volgende record */
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      RAISE ;
END ;
END RLE_KVR_AS;
/