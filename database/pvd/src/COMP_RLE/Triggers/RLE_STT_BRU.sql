CREATE OR REPLACE TRIGGER comp_rle.RLE_STT_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_STRATEN
 FOR EACH ROW

BEGIN
if stm_dbproc.column_modified(:new.wps_woonplaatsid
       , :old.wps_woonplaatsid)
  or stm_dbproc.column_modified(:new.straatnaam, :old.straatnaam)
  then
    /* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  end if;
END RLE_STT_BRU;
/