CREATE OR REPLACE TRIGGER comp_rle.RLE_TRL_BRI
 BEFORE INSERT
 ON comp_rle.RLE_TOEGESTANE_ROLLEN
 FOR EACH ROW

BEGIN
/* Begin toegevoegde code  */
		rle_rol_chk_grprol(:new.rol_codgroep);
/* Einde toegevoegde code */
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_TRL_BRI;
/