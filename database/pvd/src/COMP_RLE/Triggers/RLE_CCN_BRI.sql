CREATE OR REPLACE TRIGGER comp_rle.RLE_CCN_BRI
 BEFORE INSERT
 ON comp_rle.RLE_COMMUNICATIE_CATEGORIEEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
    if :new.id is null then
      SELECT rle_ccn_seq1.NEXTVAL
      INTO :NEW.ID
      FROM DUAL;
    end if;

     :new.code := upper(:new.code);
     :new.datum_begin := trunc(nvl(:new.datum_begin,sysdate));
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_CCN_BRI;
/