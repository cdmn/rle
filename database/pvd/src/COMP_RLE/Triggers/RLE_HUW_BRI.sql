CREATE OR REPLACE TRIGGER comp_rle.RLE_HUW_BRI
 BEFORE INSERT
 ON comp_rle.RLE_EXP_HUWELIJKEN_GBAV
 FOR EACH ROW
DECLARE



  l_num varchar2(2):='';
  l_nummer number;
begin
   -- id vullen wanneer leeg
    if :new.id is null
    then
      l_num:='';
      begin
        select max(substr(ID,length(BSN)+1,2))
        into   l_num
        from   rle_exp_huwelijken_gbav
        where BSN = :NEW.BSN;
      exception when no_data_found
      then
        l_num:='0';
      end;
      --
      l_nummer:=nvl(to_number(l_num),0);
      l_nummer:=l_nummer+1;
      l_num:=lpad(to_char(l_nummer),2,'0');
      :NEW.ID := :NEW.BSN||l_num;
    end if;
end;
/