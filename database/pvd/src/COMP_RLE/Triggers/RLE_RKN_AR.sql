CREATE OR REPLACE trigger comp_rle.rle_rkn_ar
 after delete or insert or update
 on comp_rle.rle_relatie_koppelingen
 for each row

declare
  l_stack  varchar2(128);
begin
  /* add variabelen */
  if inserting
  then
    stm_add_word( l_stack, 'I' ) ;
    /* niets nodig voor insert */
  elsif updating
  then
    stm_add_word( p_stack => l_stack
                , p_word => 'U'
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.id
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.rkg_id
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.rle_numrelatie
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.rle_numrelatie_voor
                );
    stm_add_word( p_stack => l_stack
                , p_word => to_char(:old.dat_begin, 'dd-mm-yyyy hh24:mi:ss')
                );
    stm_add_word( p_stack => l_stack
                , p_word => to_char(:old.dat_einde, 'dd-mm-yyyy hh24:mi:ss')
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.code_dat_begin_fictief
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.code_dat_einde_fictief
                );
  else /* deleting */
    stm_add_word( p_stack => l_stack
                , p_word => 'D'
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.id
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.rkg_id
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.rle_numrelatie
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.rle_numrelatie_voor
                );
    stm_add_word( p_stack => l_stack
                , p_word => to_char(:old.dat_begin, 'dd-mm-yyyy hh24:mi:ss')
                );
    stm_add_word( p_stack => l_stack
                , p_word => to_char(:old.dat_einde, 'dd-mm-yyyy hh24:mi:ss')
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.code_dat_begin_fictief
                );
    stm_add_word( p_stack => l_stack
                , p_word => :old.code_dat_einde_fictief
                );
  end if ;

  /* add rowid */
  if inserting or updating
  then
    stm_algm_muttab.add_rowid( p_rowid => :new.rowid
                             , p_varchar2 => l_stack
                             );
  else
    stm_algm_muttab.add_rowid( p_rowid =>  :old.rowid
                             , p_varchar2 => l_stack
                             );
  end if ;
end rle_rkn_ar;
/