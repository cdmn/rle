CREATE OR REPLACE TRIGGER comp_rle.RLE_RLE_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_RELATIES
 FOR EACH ROW
DECLARE

-- 19-02-2015 ZSF Extra attribuut voor event rle.GeboortedatumGewijzigd, rle.DeelnemerOverleden
--                event rle.NaamPersoonGemuteerd toegevoegd
-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeBasisGegevens
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 18-11-2014 ZSF nieuw event voor aansluiting Bancs
-- 08-05-2015 WSH MUTATIE_DATUM altijd vullen - JIRA probleem IN-4267

  cursor c_rkn_012 ( b_numrelatie in rle_relaties.numrelatie%type )
  is
    select rkn.dat_begin
    from   rle_relatie_koppelingen rkn
         , rle_rol_in_koppelingen rkg
    where  rkg.id = rkn.rkg_id
    and    (  ( rkn.rle_numrelatie = b_numrelatie
                and rkg.rol_codrol_beperkt_tot = 'PR' )
             or ( rkn.rle_numrelatie_voor = b_numrelatie
                  and rkg.rol_codrol_met = 'PR' )
           );

-- Program Data
-- jwb 20042006 code regeling (opdrachtgever) als constante gedefinieerd. RLE is nu nog niet ingericht voor meerdere opdrachtgevers.
  cn_event_WgrBasgeg  constant varchar2(30) := 'wgr.GewijzigdeBasisGegevens';
  cn_event_deelnemeroverleden constant varchar2 ( 30 ) := 'rle.DeelnemerOverleden';
  cn_event_geboortedatum constant varchar2(30) := 'rle.GeboortedatumGewijzigd';
  l_event xmltype;

--
-- PL/SQL Specification
  cursor c_asa ( cpn_numrelatie in number )
  is
    select asa.code_gba_status
         , asa.afnemers_indicatie
    from   rle_afstemmingen_gba asa
    where  asa.rle_numrelatie = cpn_numrelatie;

--
  r_asa c_asa%rowtype;

--
  cursor c_ras ( b_numrelatie in number )
  is
    select *
    from   rle_relatie_adressen ras
    where  ras.rle_numrelatie = b_numrelatie;

--
  r_ras c_ras%rowtype;

--
  cursor c_rie ( b_relatienr in number )
  is
    select 'x'
    from   rle_relatierollen_in_adm
    where  rle_numrelatie = b_relatienr
    and    adm_code in ( 'MELL', 'MNLL' )
    and    rol_codrol = 'PR';

--
  l_result varchar2 ( 20 );
  l_prs_niet_mtb boolean;
--
begin
  if stm_dbproc.column_modified ( :new.numrelatie
                                , :old.numrelatie
                                )
     or stm_dbproc.column_modified ( :new.codorganisatie
                                   , :old.codorganisatie
                                   )
     or stm_dbproc.column_modified ( :new.namrelatie
                                   , :old.namrelatie
                                   )
     or stm_dbproc.column_modified ( :new.zoeknaam
                                   , :old.zoeknaam
                                   )
     or stm_dbproc.column_modified ( :new.indgewnaam
                                   , :old.indgewnaam
                                   )
     or stm_dbproc.column_modified ( :new.indinstelling
                                   , :old.indinstelling
                                   )
     or stm_dbproc.column_modified ( :new.datschoning
                                   , :old.datschoning
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_titel
                                   , :old.dwe_wrddom_titel
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_ttitel
                                   , :old.dwe_wrddom_ttitel
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_avgsl
                                   , :old.dwe_wrddom_avgsl
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_vvgsl
                                   , :old.dwe_wrddom_vvgsl
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_brgst
                                   , :old.dwe_wrddom_brgst
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_geslacht
                                   , :old.dwe_wrddom_geslacht
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_gewatitel
                                   , :old.dwe_wrddom_gewatitel
                                   )
     or stm_dbproc.column_modified ( :new.dwe_wrddom_rvorm
                                   , :old.dwe_wrddom_rvorm
                                   )
     or stm_dbproc.column_modified ( :new.voornaam
                                   , :old.voornaam
                                   )
     or stm_dbproc.column_modified ( :new.voorletter
                                   , :old.voorletter
                                   )
     or stm_dbproc.column_modified ( :new.aanschrijfnaam
                                   , :old.aanschrijfnaam
                                   )
     or stm_dbproc.column_modified ( :new.datgeboorte
                                   , :old.datgeboorte
                                   )
     or stm_dbproc.column_modified ( :new.code_gebdat_fictief
                                   , :old.code_gebdat_fictief
                                   )
     or stm_dbproc.column_modified ( :new.datoverlijden
                                   , :old.datoverlijden
                                   )
     or stm_dbproc.column_modified ( :new.datoprichting
                                   , :old.datoprichting
                                   )
     or stm_dbproc.column_modified ( :new.datbegin
                                   , :old.datbegin
                                   )
     or stm_dbproc.column_modified ( :new.datcontrole
                                   , :old.datcontrole
                                   )
     or stm_dbproc.column_modified ( :new.dateinde
                                   , :old.dateinde
                                   )
     or stm_dbproc.column_modified ( :new.indcontrole
                                   , :old.indcontrole
                                   )
     or stm_dbproc.column_modified ( :new.regdat_overlbevest
                                   , :old.regdat_overlbevest
                                   )
     or stm_dbproc.column_modified ( :new.datemigratie
                                   , :old.datemigratie
                                   )
     or stm_dbproc.column_modified ( :new.naam_partner
                                   , :old.naam_partner
                                   )
     or stm_dbproc.column_modified ( :new.vvgsl_partner
                                   , :old.vvgsl_partner
                                   )
     or stm_dbproc.column_modified ( :new.code_aanduiding_naamgebruik
                                   , :old.code_aanduiding_naamgebruik
                                   )
     or stm_dbproc.column_modified ( :new.handelsnaam
                                   , :old.handelsnaam
                                   )
  then

-- JIRA probleem IN-4267: mutatie datum altijd vullen
:new.dat_mutatie  := sysdate;
:new.mutatie_door := alg_sessie.gebruiker;


/* Bevinding 121:   naam moet met INITCAPS */
    if stm_dbproc.column_modified ( :new.namrelatie
                                  , :old.namrelatie
                                  )
    then
      :new.namrelatie :=  nls_initcap(:new.namrelatie
                                     ,'NLS_SORT=XDUTCH'
                                     );
    end if;

    --net zoals de punten verwijderd worden worden nu tevens
    --de spaties verwijderd / jku / change 10860 / 16-08-2004
    :new.voorletter := replace ( replace ( :new.voorletter
                                         , '.'
                                         )
                               , ' '
                               );

    if :new.indinstelling = 'N'
       and stm_dbproc.column_modified ( :new.voorletter
                                      , :old.voorletter
                                      )
       and ( not rle_chk_voorletter_geldig ( nvl ( :new.voorletter
                                                 , 'X'
                                                 ) ) )
    then
      raise_application_error ( -20000
                              , 'Als voorletters mogen alleen letters (evt. met diacrieten) opgegeven worden'
                              );
    end if;

/* RLE_RLE_BRU */
    if stm_dbproc.column_modified ( :new.code_gebdat_fictief
                                  , :old.code_gebdat_fictief
                                  )
       and :new.code_gebdat_fictief is not null
    then
      rfe_chk_refwrd ( 'FGD'
                     , :new.code_gebdat_fictief
                     );
    end if;

/* BRUPD0001
   Wijzigen indinstelling in relatie (is niet toegestaan)
*/
    stm_dbproc.check_update ( :old.indinstelling
                            , :new.indinstelling
                            , 'RLE-10287'
                            );

/* END: BRUPD0001 */
    if :old.dateinde is null
       and :new.dateinde is not null
    then
      null;
    else
       /* RLE_RLE_BRU_PROC */
-- indien een van onderstaande kolommen is gewijzigd moet er gejournald worden
      if stm_dbproc.column_modified ( :new.namrelatie
                                    , :old.namrelatie
                                    )
         or stm_dbproc.column_modified ( :new.indinstelling
                                       , :old.indinstelling
                                       )
         or stm_dbproc.column_modified ( :new.datschoning
                                       , :old.datschoning
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_titel
                                       , :old.dwe_wrddom_titel
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_ttitel
                                       , :old.dwe_wrddom_ttitel
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_avgsl
                                       , :old.dwe_wrddom_avgsl
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_vvgsl
                                       , :old.dwe_wrddom_vvgsl
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_brgst
                                       , :old.dwe_wrddom_brgst
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_geslacht
                                       , :old.dwe_wrddom_geslacht
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_gewatitel
                                       , :old.dwe_wrddom_gewatitel
                                       )
         or stm_dbproc.column_modified ( :new.dwe_wrddom_rvorm
                                       , :old.dwe_wrddom_rvorm
                                       )
         or stm_dbproc.column_modified ( :new.voornaam
                                       , :old.voornaam
                                       )
         or stm_dbproc.column_modified ( :new.voorletter
                                       , :old.voorletter
                                       )
         or stm_dbproc.column_modified ( :new.aanschrijfnaam
                                       , :old.aanschrijfnaam
                                       )
         or stm_dbproc.column_modified ( :new.datgeboorte
                                       , :old.datgeboorte
                                       )
         or stm_dbproc.column_modified ( :new.datoverlijden
                                       , :old.datoverlijden
                                       )
         or stm_dbproc.column_modified ( :new.regdat_overlbevest
                                       , :old.regdat_overlbevest
                                       )
         or stm_dbproc.column_modified ( :new.datoprichting
                                       , :old.datoprichting
                                       )
         or stm_dbproc.column_modified ( :new.dateinde
                                       , :old.dateinde
                                       )
         or stm_dbproc.column_modified ( :new.naam_partner
                                       , :old.naam_partner
                                       )
         or stm_dbproc.column_modified ( :new.code_aanduiding_naamgebruik
                                       , :old.code_aanduiding_naamgebruik
                                       )
         or stm_dbproc.column_modified ( :new.vvgsl_partner
                                       , :old.vvgsl_partner
                                       )
      then
        if :new.namrelatie <> :old.namrelatie
        then
          :new.zoeknaam :=
            convert ( upper ( substr ( replace ( replace ( replace ( replace ( :new.namrelatie
                                                                             , ' & '
                                                                             , ' EN '
                                                                             )
                                                                   , ' &'
                                                                   , ' EN '
                                                                   )
                                                         , '& '
                                                         , ' EN '
                                                         )
                                               , '&'
                                               , ' EN '
                                               )
                                     , 1
                                     , 30
                                     ) )
                    , 'US7ASCII'
                    );
        --SUBSTR(UPPER(:new.namrelatie),1,30);
        end if;

        if stm_dbproc.column_modified ( :new.dwe_wrddom_gewatitel
                                      , :old.dwe_wrddom_gewatitel
                                      )
           and :new.dwe_wrddom_gewatitel is not null
        then
          -- Controle referentiewaarde
          rfe_chk_refwrd ( 'GWT'
                         , :new.dwe_wrddom_gewatitel
                         );
        end if;

        -- Controles voor natuurlijke personen
        if :new.indinstelling = 'N'
        then
          -- ***
          if stm_dbproc.column_modified ( :new.dwe_wrddom_vvgsl
                                        , :old.dwe_wrddom_vvgsl
                                        )
             and :new.dwe_wrddom_vvgsl is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'VVL'
                           , :new.dwe_wrddom_vvgsl
                           );
          end if;

          --
          -- RTJ 26-11-2003 nav call 91690 check op voorvoegsel partner
          --
          if stm_dbproc.column_modified ( :new.vvgsl_partner
                                        , :old.vvgsl_partner
                                        )
             and :new.vvgsl_partner is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'VVL'
                           , :new.vvgsl_partner
                           );
          end if;

            -- EINDE RTJ
          -- ***
          if stm_dbproc.column_modified ( :new.dwe_wrddom_titel
                                        , :old.dwe_wrddom_titel
                                        )
             and :new.dwe_wrddom_titel is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'TIL'
                           , :new.dwe_wrddom_titel
                           );
          end if;

          -- ***
          if stm_dbproc.column_modified ( :new.dwe_wrddom_brgst
                                        , :old.dwe_wrddom_brgst
                                        )
             and :new.dwe_wrddom_brgst is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'BST'
                           , :new.dwe_wrddom_brgst
                           );
          end if;

          -- ***
          if upper ( :new.dwe_wrddom_gewatitel ) = 'DHR'
             and upper ( :new.dwe_wrddom_geslacht ) <> 'M'
          then
            stm_dbproc.raise_error ( 'RLE-10332'
                                   , null
                                   , 'RLE_RLE_TOEV'
                                   );
          elsif upper ( :new.dwe_wrddom_gewatitel ) = 'MVR'
                and upper ( :new.dwe_wrddom_geslacht ) <> 'V'
          then
            stm_dbproc.raise_error ( 'RLE-10332'
                                   , null
                                   , 'RLE_RLE_TOEV'
                                   );
          end if;

          if stm_dbproc.column_modified ( :new.dwe_wrddom_ttitel
                                        , :old.dwe_wrddom_ttitel
                                        )
             and :new.dwe_wrddom_ttitel is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'TTL'
                           , :new.dwe_wrddom_ttitel
                           );
          end if;

          -- ***
          if stm_dbproc.column_modified ( :new.dwe_wrddom_avgsl
                                        , :old.dwe_wrddom_avgsl
                                        )
             and :new.dwe_wrddom_avgsl is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'AVL'
                           , :new.dwe_wrddom_avgsl
                           );
          end if;

          -- ***
          if stm_dbproc.column_modified ( :new.dwe_wrddom_geslacht
                                        , :old.dwe_wrddom_geslacht
                                        )
             and :new.dwe_wrddom_geslacht is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'GES'
                           , :new.dwe_wrddom_geslacht
                           );
          end if;

          -- ***
          if stm_dbproc.column_modified ( :new.datgeboorte
                                        , :old.datgeboorte
                                        )
          then
            -- geboortedatum verplicht bij natuurlijke personen
            if :new.datgeboorte is not null
            then
              rle_sysdate_chk ( :new.datgeboorte
                              , 'Geboortedatum'
                              );

              --
              -- alleen geboortedatum checken bij status OV
              -- en afnemers ind = 'J'
              -- OSP 24-03-2003
              open c_asa ( :old.numrelatie );

              fetch c_asa
              into  r_asa;

              close c_asa;

              if r_asa.code_gba_status = 'OV'
                 and r_asa.afnemers_indicatie = 'J'
              then
                rle_rle_chk_gebdat ( :old.numrelatie
                                   , :new.datgeboorte
                                   );
              end if;

              /* toegevoegd dd. 19-06-2002 door W. de Boer:
                 implementatie BR IER0018
               */
              for r_rkn_012 in c_rkn_012 ( :old.numrelatie )
              loop
                if :new.datgeboorte >= r_rkn_012.dat_begin
                then
                  stm_dbproc.raise_error ( 'RLE-10339'
                                         , null
                                         , 'RLE_RLE_BRU'
                                         );
                end if;
              end loop;
              l_event := eventhandler.create_event ( p_event_code => cn_event_geboortedatum );
              eventhandler.add_num_value ( p_event => l_event
                                         , p_key => 'RELATIENUMMER'
                                         , p_value => :new.numrelatie
                                         );

              eventhandler.add_date_value( p_event => l_event
                                         , p_key => 'DATUM_MUTATIE'
                                         , p_value => :new.dat_mutatie
                                         );
              eventhandler.publish_asynch_event ( p_event => l_event );
            else
              stm_dbproc.raise_error ( 'RLE-00552'
                                     , null
                                     , 'rle_rle_bru'
                                     );
            end if;
          end if;

          if stm_dbproc.column_modified ( :new.datoverlijden
                                        , :old.datoverlijden
                                        )
          then
            rle_sysdate_chk ( :new.datoverlijden
                            , 'Datum overlijden'
                            );

            if ( :new.regdat_overlbevest is not null
                 and :new.datoverlijden is not null )
            then
              ibm.publish_event ( 'OVERLIJDEN'
                                , 'RLE'
                                , to_char ( :new.numrelatie )
                                , 'PR'   -- bestemt voor Rol van de Relatie. Voorlopig default PR
                                , to_char ( :new.datoverlijden
                                          , 'dd-mm-yyyy'
                                          )
                                , to_char ( :new.regdat_overlbevest
                                          , 'dd-mm-yyyy'
                                          )
                                );
              -- Dit eventhandler event wordt gebruikt voor NPR. Het ibm event moet uitgefaseerd gaan worden
              l_event := eventhandler.create_event ( p_event_code => cn_event_deelnemeroverleden );
              eventhandler.add_num_value ( p_event => l_event
                                         , p_key => 'RELATIENUMMER'
                                         , p_value => :new.numrelatie
                                         );
              eventhandler.add_date_value ( p_event => l_event
                                          , p_key => 'DATUMOVERLIJDEN'
                                          , p_value => :new.datoverlijden
                                          );
              eventhandler.add_value ( p_event => l_event
                                     , p_key => 'CODROL'
                                     , p_value => 'PR'
                                     );
              eventhandler.add_date_value( p_event => l_event
                                         , p_key => 'DATUM_MUTATIE'
                                         , p_value => :new.dat_mutatie
                                         );
              eventhandler.publish_asynch_event ( p_event => l_event );
            end if;
          end if;

          if stm_dbproc.column_modified ( :new.namrelatie
                                        , :old.namrelatie
                                        )
          then
              l_event := eventhandler.create_event ( p_event_code => 'rle.NaamPersoonGemuteerd' );
              eventhandler.add_num_value ( p_event => l_event
                                         , p_key => 'RELATIENUMMER'
                                         , p_value => :new.numrelatie
                                         );

              eventhandler.add_date_value( p_event => l_event
                                         , p_key => 'DATUM_MUTATIE'
                                         , p_value => :new.dat_mutatie
                                         );
              eventhandler.publish_asynch_event ( p_event => l_event );
          end if;

          -- Velden van een instelling mogen niet gevuld worden
          if :new.datoprichting is not null
             or :new.dwe_wrddom_rvorm is not null
          then
            stm_dbproc.raise_error ( 'RLE-00513'
                                   , null
                                   , 'rle_rle_bru'
                                   );
          end if;

          /*
           * indien de burgerlijke staat gewijzigd is, moet
           * gecontroleerd worden of de naam_partner en/of
           * code_aanduiding_naamgebruik nog wel juist gevuld zijn.
           * indien de nieuwe burgstaat NIET HUW/PA is, moet de
           * naam_partner leeg zijn en de code_aanduiding_naamgebruik
           * 'E' zijn. Indien in de overige gevallen de nieuwe
           * naam_partner leeg is, moet de code_aanduiding_naamgebruik
           * ook 'E' zijn.
           */
          if stm_dbproc.column_modified ( :new.dwe_wrddom_brgst
                                        , :old.dwe_wrddom_brgst
                                        )
             and nvl ( :new.dwe_wrddom_brgst
                     , '@'
                     ) <> 'HUW/PA'
          then
            if :new.dwe_wrddom_brgst is null
            then
              :new.dwe_wrddom_brgst := 'ONBEKEND';
            end if;

           /*
            * WHR 27-03-2014 Project HERBA
            * afhankelijkheid tussen burgelijke staat en aanduiding naamgebruik ongewenst
            *
            * if :new.naam_partner is not null
            * then
            *  :new.naam_partner := null;
            * end if;
            *
            * if :new.vvgsl_partner is not null
            * then
            *  :new.vvgsl_partner := null;
            * end if;
            *
            * if nvl ( :new.code_aanduiding_naamgebruik
            *        , '@'
            *        ) <> 'E'
            * then
            *   :new.code_aanduiding_naamgebruik := 'E';
            * end if;
            */
          elsif stm_dbproc.column_modified ( :new.naam_partner
                                           , :old.naam_partner
                                           )
                and :new.naam_partner is null
          then
            if nvl ( :new.code_aanduiding_naamgebruik
                   , '@'
                   ) <> 'E'
            then
              :new.code_aanduiding_naamgebruik := 'E';
            end if;
          end if;

          /*
          * Aanpassing R. van Engelen dd. 01 mei 2001:
          * Als code-naamgebruik = (N, V of P) moet naam-partner gevuld.
          */
          if stm_dbproc.column_modified ( :new.code_aanduiding_naamgebruik
                                        , :old.code_aanduiding_naamgebruik
                                        )
             or stm_dbproc.column_modified ( :new.naam_partner
                                           , :old.naam_partner
                                           )
          then
            if :new.code_aanduiding_naamgebruik in ( 'N', 'V', 'P' )
               and :new.naam_partner is null
            then
              stm_dbproc.raise_error ( 'RLE-00554'
                                     , null
                                     , 'RLE_RLE_BRU'
                                     );
            end if;
          end if;
-- *********************************************************
        else   -- het betreft een instelling
          --
          if stm_dbproc.column_modified ( :new.dwe_wrddom_rvorm
                                        , :old.dwe_wrddom_rvorm
                                        )
             and :new.dwe_wrddom_rvorm is not null
          then
            -- Controle referentiewaarde
            rfe_chk_refwrd ( 'REV'
                           , :new.dwe_wrddom_rvorm
                           );
          end if;

          -- ***
          if ( :new.datgeboorte is not null
               or :new.datoverlijden is not null
               or :new.dwe_wrddom_geslacht is not null
               or :new.dwe_wrddom_brgst is not null
             )
          then
            stm_dbproc.raise_error ( 'RL-00512'
                                   , null
                                   , 'rle_rle_bru'
                                   );
          end if;
        end if;   -- Einde test Instelling

        /* QMS$DATA_AUDITING  - naar boven verplaatst ivm JIRA probleem 4267 */
 --       :new.dat_mutatie := sysdate;
 --       :new.mutatie_door := alg_sessie.gebruiker;

        /* QMS$JOURNALLING */
        begin
          insert into rle_relaties_jn
                      ( jn_user
                      , jn_date_time
                      , jn_operation
                      , numrelatie
                      , codorganisatie
                      , namrelatie
                      , zoeknaam
                      , indgewnaam
                      , indinstelling
                      , datschoning
                      , dwe_wrddom_titel
                      , dwe_wrddom_ttitel
                      , dwe_wrddom_avgsl
                      , dwe_wrddom_vvgsl
                      , dwe_wrddom_brgst
                      , dwe_wrddom_geslacht
                      , dwe_wrddom_gewatitel
                      , dwe_wrddom_rvorm
                      , voornaam
                      , voorletter
                      , aanschrijfnaam
                      , datgeboorte
                      , code_gebdat_fictief
                      , datoverlijden
                      , datoprichting
                      , dateinde
                      , naam_partner
                      , vvgsl_partner
                      , code_aanduiding_naamgebruik
                      , dat_creatie
                      , dat_mutatie
                      , mutatie_door
                      , creatie_door
                      , handelsnaam
                      , datbegin
                      , datcontrole
                      , indcontrole
                      , regdat_overlbevest
                      )
          values      ( :new.mutatie_door
                      , :new.dat_mutatie
                      , 'UPD'
                      , :old.numrelatie
                      , :old.codorganisatie
                      , :old.namrelatie
                      , :old.zoeknaam
                      , :old.indgewnaam
                      , :old.indinstelling
                      , :old.datschoning
                      , :old.dwe_wrddom_titel
                      , :old.dwe_wrddom_ttitel
                      , :old.dwe_wrddom_avgsl
                      , :old.dwe_wrddom_vvgsl
                      , :old.dwe_wrddom_brgst
                      , :old.dwe_wrddom_geslacht
                      , :old.dwe_wrddom_gewatitel
                      , :old.dwe_wrddom_rvorm
                      , :old.voornaam
                      , :old.voorletter
                      , :old.aanschrijfnaam
                      , :old.datgeboorte
                      , :old.code_gebdat_fictief
                      , :old.datoverlijden
                      , :old.datoprichting
                      , :old.dateinde
                      , :old.naam_partner
                      , :old.vvgsl_partner
                      , :old.code_aanduiding_naamgebruik
                      , :old.dat_creatie
                      , :old.dat_mutatie
                      , :old.mutatie_door
                      , :old.creatie_door
                      , :old.handelsnaam
                      , :old.datbegin
                      , :old.datcontrole
                      , :old.indcontrole
                      , :old.regdat_overlbevest
                      );
        end;
      end if;   -- Eind van wederom een test op gewijzigde kolommen end /* RLE_RLE_BRU_PROC */
    end if;   -- Eind check :old.dateinde IS NULL AND :new.dateinde

/* Publicatie voor synchronisatie */
    begin
      /* Conditioneel publiceren: alleen een publicatie als er een adres aanwezig
         bij de relatie: dit om te voorkomen dat SYN aborteert op een update
         terwijl de relatie nog niet volldedig opgevoerd is
         -- 28-02-2005, NKU, Project Levensloop, mutaties betreffende relaties
         --                          met rol WA en WC hoeven niet gepublished te worden
         -- 02-02-2009, XMB  Project Levensloop. Niet MTB wordt bepaald mbv adm_code.
      */
      open c_rie ( :old.numrelatie );

      fetch c_rie
      into  l_result;

      if c_rie%found
      then
        l_prs_niet_mtb := true;
      else
        l_prs_niet_mtb := false;
      end if;

      close c_rie;

      --
      open c_ras ( :new.numrelatie );

      fetch c_ras
      into  r_ras;

      if r_ras.id is not null
         and not l_prs_niet_mtb
      then
        ibm.publish_event ( 'MUT RELATIES'
                          , 'U'
                          , to_char ( sysdate
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :old.regdat_overlbevest
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.regdat_overlbevest
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :old.datbegin
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.datbegin
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :old.datemigratie
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.datemigratie
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , :old.handelsnaam
                          , :new.handelsnaam
                          , to_char ( :old.datcontrole
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.datcontrole
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , :old.indcontrole
                          , :new.indcontrole
                          , :old.indinstelling
                          , :new.indinstelling
                          , to_char ( :old.datschoning
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.datschoning
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , :old.dwe_wrddom_titel
                          , :new.dwe_wrddom_titel
                          , :old.dwe_wrddom_ttitel
                          , :new.dwe_wrddom_ttitel
                          , :old.dwe_wrddom_avgsl
                          , :new.dwe_wrddom_avgsl
                          , :old.dwe_wrddom_vvgsl
                          , :new.dwe_wrddom_vvgsl
                          , :old.dwe_wrddom_brgst
                          , :new.dwe_wrddom_brgst
                          , :old.dwe_wrddom_geslacht
                          , :new.dwe_wrddom_geslacht
                          , :old.dwe_wrddom_gewatitel
                          , :new.dwe_wrddom_gewatitel
                          , :old.dwe_wrddom_rvorm
                          , :new.dwe_wrddom_rvorm
                          , :old.voornaam
                          , :new.voornaam
                          , :old.voorletter
                          , :new.voorletter
                          , :old.aanschrijfnaam
                          , :new.aanschrijfnaam
                          , to_char ( :old.datgeboorte
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.datgeboorte
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , :old.code_gebdat_fictief
                          , :new.code_gebdat_fictief
                          , to_char ( :old.datoverlijden
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.datoverlijden
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :old.datoprichting
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.datoprichting
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :old.dateinde
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.dateinde
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , :old.naam_partner
                          , :new.naam_partner
                          , :old.vvgsl_partner
                          , :new.vvgsl_partner
                          , :old.code_aanduiding_naamgebruik
                          , :new.code_aanduiding_naamgebruik
                          , to_char ( :old.dat_creatie
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.dat_creatie
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , :old.creatie_door
                          , :new.creatie_door
                          , to_char ( :old.dat_mutatie
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , to_char ( :new.dat_mutatie
                                    , 'dd-mm-yyyy hh24:mi:ss'
                                    )
                          , :old.mutatie_door
                          , :new.mutatie_door
                          , :old.numrelatie
                          , :new.numrelatie
                          , :old.codorganisatie
                          , :new.codorganisatie
                          , :old.namrelatie
                          , :new.namrelatie
                          , :old.zoeknaam
                          , :new.zoeknaam
                          , :old.indgewnaam
                          , :new.indgewnaam
                          );
      end if;

      close c_ras;
    end;
    if :new.handelsnaam is not null and :new.handelsnaam <> nvl(:old.handelsnaam, '##')
    then
        l_event := eventhandler.create_event(p_event_code => cn_event_WgrBasgeg);
        eventhandler.add_num_value(p_event  => l_event
                                  ,p_key    => 'RELATIENUMMER'
                                  ,p_value  => :new.numrelatie);
        eventhandler.add_date_value( p_event => l_event
                                   , p_key => 'DATUM_MUTATIE'
                                   , p_value => :new.dat_mutatie
                                   );
        eventhandler.publish_asynch_event(p_event => l_event);
    end if;
  end if;   -- einde afvraging wijziging van één van alle velden
end rle_rle_bru;
/