CREATE OR REPLACE TRIGGER comp_rle.RLE_KVR_BRD
 BEFORE DELETE
 ON comp_rle.RLE_KANAALVOORKEUREN
 FOR EACH ROW

BEGIN
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
   rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);
END RLE_KVR_BRD;
/