CREATE OR REPLACE TRIGGER comp_rle.RLE_ASA_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_AFSTEMMINGEN_GBA
 FOR EACH ROW
BEGIN
   /* DATA_AUDITING */
   :new.DAT_MUTATIE  := SYSDATE ;
   :new.MUTATIE_DOOR := USER    ;

   /* BRIER0029
      Voorkomen dat een RELATIE een Afnemersindicatie 'J' heeft
      zonder A-Nummer.
   */ /* */
   IF :new.afnemers_indicatie = 'J'
   THEN
      IF rle_m03_rec_extcod( 'GBA', :new.rle_numrelatie, 'PR', SYSDATE ) IS NULL
      THEN
         stm_dbproc.raise_error( 'RLE-10354' ) ;
      END IF ;
   END IF ;

   /*  CEV0021   */
   IF :new.CODE_GBA_STATUS = 'AF' THEN
      --
      -- mbv een synchroon abbonnement op het event wordt tevens de AVH verwijderd
      --
      RLE_VERWIJDER_SOFINR (:new.RLE_NUMRELATIE);
      -- N.a.v. Call 91515 ook a-nummer verwijderen - RTJ 08-12-2003
      RLE_VERWIJDER_A_NR (:new.RLE_NUMRELATIE);
      -- N.a.v. Call 93417 ook relatie koppelingen verwijderen - JKU 16-04-2004
      RLE_VERWIJDER_KOPPELINGEN (:new.RLE_NUMRELATIE);
 END IF;

    /* JPG 23-02-2012
      BR_CEV0029_ASA: Indien de code gba status = OV en afnemersindicatie = J dan moet ind opvoer gbav N zijn
   */
   IF    :new.AFNEMERS_INDICATIE = 'J'
   AND   :new.CODE_GBA_STATUS    = 'OV'
   THEN
     :new.ind_opvoer_gbav := 'N';
     --
     -- Tevens moet het binnenlands correspondentie adres dat is opgevoerd bij de registratie obv het GBA-V verwijderd worden
     --
     if :old.ind_opvoer_gbav = 'J'
     then
       RLE_VERWIJDER_ADRES (P_RELATIENUMMER => :new.RLE_NUMRELATIE
                           ,P_SOORT_ADRES   => 'CA'
                           ,P_LANDCODE      => 'NL');
     end if;
   END IF;
   --
   -- JPG 30-03-2012
   -- Indien indicatie opvoer gbav dan registratiedatum vullen met sysdate. Anders leegmaken.
   --
   IF :new.ind_opvoer_gbav = 'N'
   THEN
    :new.dat_registratie_gbav := NULL;
   ELSIF :new.ind_opvoer_gbav = 'J'
   THEN
     :new.dat_registratie_gbav := SYSDATE;
  END IF;

   /* Publicatie voor synchronisatie */
   -- 12-11-2003, VER, alleen een publicatie indien de nieuwe gba-status = 'OV' of als de afn.ind. wijzigt
   --
   IF    :old.AFNEMERS_INDICATIE <> :new.AFNEMERS_INDICATIE
      OR :new.CODE_GBA_STATUS = 'OV'
      OR :old.CODE_GBA_STATUS = 'OV'
      OR :new.CODE_GBA_STATUS = 'AF'
   THEN
      IBM.PUBLISH_EVENT
         ( 'MUT RLE AFSTEMMINGEN GBA'
         , 'U'
         , TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
         , :old.CREATIE_DOOR                                 , :new.CREATIE_DOOR
         , TO_CHAR(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), TO_CHAR(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
         , :old.MUTATIE_DOOR                                 , :new.MUTATIE_DOOR
         , TO_CHAR(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), TO_CHAR(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
         , :old.CODE_GBA_STATUS                              , :new.CODE_GBA_STATUS
         , :old.AFNEMERS_INDICATIE                           , :new.AFNEMERS_INDICATIE
         , :old.RLE_NUMRELATIE                               , :new.RLE_NUMRELATIE
         );
   END IF;
END ;
/