CREATE OR REPLACE TRIGGER comp_rle.RLE_DFR_BRI
 BEFORE INSERT
 ON comp_rle.RLE_DUBBELE_FAKTOREN
 FOR EACH ROW

BEGIN
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_DFR_BRI;
/