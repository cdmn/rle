CREATE OR REPLACE TRIGGER comp_rle.RLE_NPE_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_NED_POSTCODE
 FOR EACH ROW

BEGIN
if stm_dbproc.column_modified(:new.postcode, :old.postcode)
or  stm_dbproc.column_modified(:new.codreeks, :old.codreeks)
or  stm_dbproc.column_modified(:new.numscheidingvan
      , :old.numscheidingvan)
or  stm_dbproc.column_modified(:new.numscheidingtm
      , :old.numscheidingtm)
or stm_dbproc.column_modified(:new.indpostchand, :old.indpostchand)
or stm_dbproc.column_modified(:new.woonplaats, :old.woonplaats)
or stm_dbproc.column_modified(:new.straatnaam, :old.straatnaam)
or stm_dbproc.column_modified(:new.gemeentenaam
      , :old.gemeentenaam)
or stm_dbproc.column_modified(:new.codprovincie
      ,:old.codprovincie)
or stm_dbproc.column_modified(:new.codcebuco, :old.codcebuco)
 then
    /* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
end if;
END RLE_NPE_BRU;
/