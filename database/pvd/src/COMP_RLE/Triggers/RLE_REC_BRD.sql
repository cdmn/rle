CREATE OR REPLACE TRIGGER comp_rle.RLE_REC_BRD
 BEFORE DELETE
 ON comp_rle.RLE_RELATIE_EXTERNE_CODES
 FOR EACH ROW
DECLARE

-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeIdentificatie
-- 17-02-2015 ZSF Opnieuw wgr.GewijzigdBovagNummerWerkgever
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 30-05-2014 EZW   Nieuw event salarismutatie voor aansluiting Bancs
cn_event_WgrId  constant varchar2(30) := 'wgr.GewijzigdeIdentificatie';
l_event xmltype;
BEGIN
   /* Publicatie voor synchronisatie */
   -- 12-11-2003, VER, alleen een publicatie bij A-nummer en SOFI-nummer
   -- 22-07-2005, VER, PR-UII, Consolidatie ZEUS/HERA: GBA en SOFI-nummer worden niet meer gesynchroniseerd
   --
   IF :old.DWE_WRDDOM_EXTSYS = 'KVK'
   THEN
      IBM.PUBLISH_EVENT
         ( 'MUT RLE EXTERNE CODES'
         , 'D'
         , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
         , :old.ID                                           , NULL
         , :old.RLE_NUMRELATIE                               , NULL
         , :old.DWE_WRDDOM_EXTSYS                            , NULL
         , to_char(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , NULL
         , :old.CODORGANISATIE                               , NULL
         , :old.ROL_CODROL                                   , NULL
         , :old.EXTERN_RELATIE                               , NULL
         , :old.IND_GEVERIFIEERD                             , NULL
         , to_char(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , NULL
         , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
         , :old.CREATIE_DOOR                                 , NULL
         , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
         , :old.MUTATIE_DOOR                                 , NULL
         );
   END IF;
   if :old.dwe_wrddom_extsys = 'KVK'
     or :old.dwe_wrddom_extsys = 'WGR'
     or :old.dwe_wrddom_extsys = 'RSIN'
   then
      l_event := eventhandler.create_event(p_event_code => cn_event_WgrId);
      eventhandler.add_num_value(p_event  => l_event
                                ,p_key    => 'RELATIENUMMER'
                                ,p_value  => :old.rle_numrelatie);
      eventhandler.add_date_value( p_event => l_event
                                 , p_key => 'DATUM_MUTATIE'
                                 , p_value => :old.dat_mutatie
                                 );
      eventhandler.publish_asynch_event(p_event => l_event);
   end if;
--
  if :old.dwe_wrddom_extsys = 'BOVAG'
  then
    l_event := eventhandler.create_event( p_event_code => 'wgr.GewijzigdBovagNummerWerkgever' );
    eventhandler.add_num_value( p_event  => l_event
                              , p_key    => 'RELATIENUMMER'
                              , p_value  => :old.rle_numrelatie
                              );
    eventhandler.add_date_value( p_event => l_event
                               , p_key => 'DATUM_MUTATIE'
                               , p_value => :old.dat_mutatie
                               );
    eventhandler.publish_asynch_event( p_event => l_event );
  end if;
END;
/