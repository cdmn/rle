CREATE OR REPLACE TRIGGER comp_rle.RLE_MIG_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_MEDIUM_INZENDINGEN

BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
   l_rowid  ROWID           ;
   l_job    VARCHAR2( 1 )   ;
   /* Cursor om het huidige record op te halen adhv het ROWID */
   CURSOR lc_mig(
        cpr_rowid  IN  ROWID
      ) IS
      SELECT *
      FROM   rle_medium_inzendingen  mig
      WHERE  mig.rowid = cpr_rowid
      ;
   lr_mig  lc_mig%ROWTYPE ;
   /* Cursor om overlappende record te bepalen */
   CURSOR lc_mig_2(
        cpn_numrelatie       IN  rle_medium_inzendingen.rle_numrelatie%TYPE
      , cpv_soort_inzending  IN  rle_medium_inzendingen.code_soort_inzending%TYPE
      ) IS
      SELECT  mig.dat_begin
      ,       mig.dat_einde
      ,       mig.code_soort_inzending
      ,       mig.rle_numrelatie
      FROM    rle_medium_inzendingen  mig
      WHERE   mig.rle_numrelatie       = cpn_numrelatie
      AND     mig.code_soort_inzending = cpv_soort_inzending
      ;
   l_old_numrelatie        rle_medium_inzendingen.rle_numrelatie%TYPE       ;
   l_old_dat_begin         rle_medium_inzendingen.dat_begin%TYPE            ;
   l_old_dat_einde         rle_medium_inzendingen.dat_einde%TYPE            ;
   l_old_soort_inzending   rle_medium_inzendingen.code_soort_inzending%TYPE ;
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;
      /* Ophalen variabelen per soort statement */
      IF    l_job = 'I'
      THEN
         /* Ophalen variabelen voor insert */
         NULL ;
      ELSIF l_job = 'U'
      THEN
         /* Ophalen variabelen voor update */
         l_old_dat_begin        := TO_DATE( stm_get_word( l_stack ) ) ;
      ELSIF l_job = 'D'
      THEN
         /* Ophalen variabelen voor delete */
         l_old_numrelatie      := stm_get_word( l_stack ) ;
         l_old_dat_begin       := TO_DATE( stm_get_word( l_stack ) ) ;
         l_old_dat_einde       := TO_DATE( stm_get_word( l_stack ) ) ;
         l_old_soort_inzending := stm_get_word( l_stack ) ;
      END IF ;
      /* Ophalen huidige record */
      IF l_job IN ( 'I', 'U' )
      THEN
         OPEN lc_mig( l_rowid ) ;
         FETCH lc_mig INTO lr_mig ;
         CLOSE lc_mig ;
      END IF ;
      /* BRATT0020
         Soort inzending moet voorkomen in referentie
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         ibm.ibm( 'CHK_REFWRD'
                , 'RLE'
                , 'SIG'
                , lr_mig.code_soort_inzending
                ) ;
      END IF ;
      /* BRATT0021
         Medium moet voorkomen in referentie
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         ibm.ibm( 'CHK_REFWRD'
                , 'RLE'
                , 'MDM'
                , lr_mig.code_medium
                ) ;
      END IF ;
      /* BRCEV0007
         Afsluiten openstaand voorkomen
      */
      /* BRCEV0007: Bij toevoegen nieuw voorkomen, afsluiten voorliggende voorkomen. */
      IF l_job = 'I'
      THEN
         UPDATE rle_medium_inzendingen  mig
         SET    mig.dat_einde = lr_mig.dat_begin - 1
         WHERE  mig.rle_numrelatie          =  lr_mig.rle_numrelatie
         AND    mig.code_soort_inzending    =  lr_mig.code_soort_inzending
         AND    mig.dat_einde               IS NULL
         AND    (  mig.rle_numrelatie       != lr_mig.rle_numrelatie
                OR mig.dat_begin            != lr_mig.dat_begin
                OR mig.code_soort_inzending != lr_mig.code_soort_inzending
                )
         ;
      /* BRCEV0007: Bij wijzigen begindatum, ook "vorige" record aanpassen
         NOTA: Dit kan niet voorkomen op dit moment omdat de begindatum
               opgenomen in in de (niet-updateble) primaire sleutel.
      */
      ELSIF l_job = 'U'
      THEN
         IF l_old_dat_begin != lr_mig.dat_begin
         THEN
            UPDATE rle_medium_inzendingen  mig
            SET    mig.dat_einde = lr_mig.dat_begin - 1
            WHERE  mig.rle_numrelatie          =  lr_mig.rle_numrelatie
            AND    mig.code_soort_inzending    =  lr_mig.code_soort_inzending
            AND    mig.dat_einde               =  l_old_dat_begin - 1
            AND    (  mig.rle_numrelatie       != lr_mig.rle_numrelatie
                   OR mig.dat_begin            != lr_mig.dat_begin
                   OR mig.code_soort_inzending != lr_mig.code_soort_inzending
                   )
            ;
         END IF ;
      /* BRCEV0007: Bij verwijderd als een voorkomen verwijderd wordt waarnaar de
         einddatum van een "broederrecord" verwijst, dan schuift de einddatum van
         dit "broederrecord" op naar de einddatum van het verwijderde record.
      */
      ELSIF l_job = 'D'
      THEN
         UPDATE rle_medium_inzendingen  mig
         SET    mig.dat_einde = l_old_dat_einde
         WHERE  mig.rle_numrelatie       = l_old_numrelatie
         AND    mig.code_soort_inzending = l_old_soort_inzending
         AND    mig.dat_einde            = l_old_dat_begin - 1
         ;
      END IF ;
      /* BRENT0012
         Voorkomen overlap RLE_MEDIUM_INZENDINGEN
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         FOR ir_mig_2 IN lc_mig_2( lr_mig.rle_numrelatie
                                 , lr_mig.code_soort_inzending
                                 )
         LOOP
            IF ir_mig_2.dat_begin            != lr_mig.dat_begin             OR
               ir_mig_2.code_soort_inzending != lr_mig.code_soort_inzending  OR
               ir_mig_2.rle_numrelatie       != lr_mig.rle_numrelatie
            THEN
               stm_algm_check.algm_datovlp( lr_mig.dat_begin
                                          , lr_mig.dat_einde
                                          , ir_mig_2.dat_begin
                                          , ir_mig_2.dat_einde
                                          ) ;
            END IF ;
         END LOOP ;
      END IF ;
      /* BRIER0022
         o.a. Voorkomen overlap RLE_MEDIUM_INZENDINGEN t.o.v. RLE_RELATIES
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         rle_chk_rle_persdat( lr_mig.rle_numrelatie
                            , lr_mig.dat_begin
                            , 'medium Inzendingen'
                            ) ;
      END IF ;
      /* Volgende rij ophalen */
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      IF lc_mig%ISOPEN
      THEN
         CLOSE lc_mig ;
      END IF ;
      RAISE ;
END ;
END RLE_MIG_AS;
/