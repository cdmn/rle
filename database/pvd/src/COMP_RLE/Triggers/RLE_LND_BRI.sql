CREATE OR REPLACE TRIGGER comp_rle.RLE_LND_BRI
 BEFORE INSERT
 ON comp_rle.RLE_LANDEN
 FOR EACH ROW
BEGIN
 /* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := SYSDATE;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END ;
/