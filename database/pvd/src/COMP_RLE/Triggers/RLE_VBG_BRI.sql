CREATE OR REPLACE TRIGGER comp_rle.RLE_VBG_BRI
 BEFORE INSERT
 ON comp_rle.RLE_VERZENDBESTEMMINGEN
 FOR EACH ROW

BEGIN
/* BR TPL0023 */
IF :new.rrl_rol_codrol = 'PR'
THEN
   IF :new.rrl_rle_numrelatie_betreft IS NOT NULL
   OR :new.rrl_rol_codrol_betreft IS NOT NULL
   OR :new.rrl_rol_codrol_hoort_bij IS NOT NULL
   OR :new.rrl_rle_numrelatie_hoort_bij IS NOT NULL
   THEN
      stm_dbproc.raise_error( 'RLE-10343' ) ;
   END IF;
END IF;
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := SYSDATE;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_VBG_BRI;
/