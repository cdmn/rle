CREATE OR REPLACE TRIGGER comp_rle.RLE_RVT_BRI
 BEFORE INSERT
 ON comp_rle.RLE_RELATIE_VERZETTEN
 FOR EACH ROW
begin
  -- BR_TPL0032_RVT controle op juiste invoer tussenpersoon/administratie
  IF not rle_vzt.rle_vzt_tp_adm( p_tussenpersoon => :new.tussenpersoon
                               , p_administratie => :new.adm_code )
  THEN
    raise_application_error(-20000,'Tussenpersoon of Administratie moet gevuld zijn');
  END IF;
  --
  -- BR_IER0038_RVT controle op uniek met bestaande verzetten
  IF rle_vzt.rle_vzt_uniek( p_numrelatie    => :new.rle_numrelatie
                          , p_tussenpersoon => :new.tussenpersoon
                          , p_administratie => :new.adm_code
                          , p_productgroep  => :new.productgroep
                          , p_verzet_kanaal => :new.verzet_kanaal )
  THEN
    raise_application_error(-20000,'Dit verzet bestaat reeds');
  END IF;
  -- BR_IER0038_RVT controle op overlap met bestaande verzetten
  IF rle_vzt.rle_vzt_overlap( p_numrelatie    => :new.rle_numrelatie
                            , p_tussenpersoon => :new.tussenpersoon
                            , p_administratie => :new.adm_code
                            , p_productgroep  => :new.productgroep
                            , p_verzet_kanaal => :new.verzet_kanaal )
  THEN
    raise_application_error(-20000,'Dit verzet heeft overlap met een ander geregistreerd verzet');
  END IF;
  --
  :new.CREATED_BY := user;
  :new.CREATION_DATE := sysdate;
  :new.LAST_UPDATED_BY := user;
  :new.LAST_UPDATE_DATE :=sysdate;
  :new.REGISTRATIE_DATUM :=sysdate;
  --
END RLE_RVT_BRI;
/