CREATE OR REPLACE trigger comp_rle.rle_rkn_ard
 after delete
 on comp_rle.rle_relatie_koppelingen
 for each row

begin
/* qms$journalling */
  insert into rle_relatie_koppelingen_jn
    ( jn_user
    , jn_date_time
    , jn_operation
    , id
    , rkg_id
    , rle_numrelatie
    , rle_numrelatie_voor
    , dat_begin
    , code_dat_begin_fictief
    , dat_einde
    , code_dat_einde_fictief
    , creatie_door
    , dat_creatie
    , mutatie_door
    , dat_mutatie
    )
  values
    ( alg_sessie.gebruiker
    , sysdate
    , 'DEL'
    , :old.id
    , :old.rkg_id
    , :old.rle_numrelatie
    , :old.rle_numrelatie_voor
    , :old.dat_begin
    , :old.code_dat_begin_fictief
    , :old.dat_einde
    , :old.code_dat_einde_fictief
    , :old.creatie_door
    , :old.dat_creatie
    , :old.mutatie_door
    , :old.dat_mutatie
    );
end;
/