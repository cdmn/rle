CREATE OR REPLACE TRIGGER comp_rle.RLE_GBD_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_GEMOEDSBEZWAARDHEDEN

BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
   l_rowid  ROWID           ;
   l_job    VARCHAR2( 1 )   ;
   CURSOR lc_gbd_000(
        cpr_rowid  IN  ROWID
      ) IS
      SELECT *
      FROM   rle_gemoedsbezwaardheden  gbd
      WHERE  gbd.rowid = cpr_rowid
      ;
   lr_gbd_000  lc_gbd_000%ROWTYPE ;
   CURSOR lc_gbd_001(
        cpn_numrelatie  IN  NUMBER
      ) IS
      SELECT *
      FROM   rle_gemoedsbezwaardheden  gbd
      WHERE  gbd.rle_numrelatie = cpn_numrelatie
      ;
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;
      /* Ophalen variabelen per soort statement */
      IF    l_job = 'I'
      THEN
         /* Ophalen variabelen voor insert */
         NULL ;
      ELSIF l_job = 'U'
      THEN
         /* Ophalen variabelen voor update */
         NULL ;
      ELSIF l_job = 'D'
      THEN
         /* Ophalen variabelen voor delete */
         NULL ;
      END IF ;
      /* Ophalen rij */
      IF l_job IN ( 'I', 'U' )
      THEN
         OPEN lc_gbd_000( l_rowid ) ;
         FETCH lc_gbd_000 INTO lr_gbd_000 ;
         CLOSE lc_gbd_000 ;
      END IF ;
      /* BRENT0015
         Voorkomen overlap bij gemoedsbezwaardheden
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         FOR ir_gbd_001 IN lc_gbd_001( lr_gbd_000.rle_numrelatie )
         LOOP
            IF ir_gbd_001.dat_begin != lr_gbd_000.dat_begin
            THEN
               stm_algm_check.algm_datovlp( lr_gbd_000.dat_begin
                                          , lr_gbd_000.dat_einde
                                          , ir_gbd_001.dat_begin
                                          , ir_gbd_001.dat_einde
                                          ) ;
            END IF ;
         END LOOP ;
      END IF ;
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      RAISE ;
END ;
END RLE_GBD_AS;
/