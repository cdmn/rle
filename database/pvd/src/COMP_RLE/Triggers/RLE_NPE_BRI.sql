CREATE OR REPLACE TRIGGER comp_rle.RLE_NPE_BRI
 BEFORE INSERT
 ON comp_rle.RLE_NED_POSTCODE
 FOR EACH ROW

BEGIN
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_NPE_BRI;
/