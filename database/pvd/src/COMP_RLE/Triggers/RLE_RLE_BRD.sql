CREATE OR REPLACE TRIGGER comp_rle.RLE_RLE_BRD
 BEFORE DELETE
 ON comp_rle.RLE_RELATIES
 FOR EACH ROW

BEGIN
/* RLE_RLE_BRD */
   rle_rle_del_childs( :old.numrelatie);
END RLE_RLE_BRD;
/