CREATE OR REPLACE TRIGGER comp_rle.RLE_CNR_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_COMMUNICATIE_NUMMERS
DECLARE
-- Program Data
L_OLDNUMCOMMUNICATIE VARCHAR2(50);
L_ROWID ROWID;
L_STACK VARCHAR2(128);
L_OLDRASID NUMBER(20);
L_JOB VARCHAR2(1);
L_OLDDATINGANG DATE;
L_OLDDATEINDE DATE;
L_OLDINDINHOWN VARCHAR2(1) := 'N';
L_OLDNUMRELATIE NUMBER(10);
L_OLDCODROL VARCHAR2(2);
L_OLDSRTCOM VARCHAR2(30);
l_admcode varchar2(12);  -- xcw

-- PL/SQL Specification
/* RLE_CNR_AS */
  CURSOR c_cnr_000(
         b_rowid ROWID
         )
  IS
  SELECT id
  ,      rle_numrelatie
  ,      rol_codrol
  ,      dwe_wrddom_srtcom
  ,      ras_id
  ,      datingang
  ,      dateinde
  ,      adm_code
  FROM   rle_communicatie_nummers cnr
  WHERE  cnr.rowid = b_rowid
  ;
  r_cnr_000 c_cnr_000%rowtype ;
  /* C_CNR_003: Ophalen van communicatienummer op rol en soort
     HVI 20021115: In Package Specs gezet ipv als Sub Program Unit
  */
  CURSOR C_CNR_003
       ( b_numrelatie IN NUMBER
       , b_srtcom     IN VARCHAR2
       , b_codrol     IN VARCHAR2
       , b_ras_id     IN NUMBER
	   , b_adm_code   IN VARCHAR2
       )
  IS
  SELECT id
       , datingang
       , dateinde
    FROM rle_communicatie_nummers cnr
   WHERE cnr.rle_numrelatie      = b_numrelatie
     AND cnr.dwe_wrddom_srtcom   = b_srtcom
     AND NVL(cnr.rol_codrol,'@') = NVL(b_codrol,'@')
     AND cnr.ras_id              = b_ras_id
	 AND cnr.adm_code            = b_adm_code
  ;

-- PL/SQL Block
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;

   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;

      /*  Ophalen variabelen per soort statement */
      IF l_job <> 'I'
      THEN
         l_olddatingang       := TO_DATE( stm_get_word( l_stack ) ) ;
         l_olddateinde        := TO_DATE( stm_get_word( l_stack ) ) ;
         l_oldindinhown       := stm_get_word (l_stack ) ;
         l_oldnumrelatie      := stm_get_word (l_stack ) ;
         l_oldcodrol          := stm_get_word (l_stack ) ;
         l_oldsrtcom          := stm_get_word (l_stack ) ;
         l_oldnumcommunicatie := stm_get_word (l_stack ) ;
         l_oldrasid           := TO_NUMBER( stm_get_word( l_stack ) ) ;
      END IF;

      /* Zoek betreffende record indien mogelijk */
      IF l_job <> 'D'
      THEN
         OPEN c_cnr_000( l_rowid ) ;
         FETCH c_cnr_000 INTO r_cnr_000 ;
         CLOSE c_cnr_000;
      END IF;

      /* Bij toevoegen nieuw voorkomen, afsluiten voorliggende voorkomen. */
      IF l_job = 'I'
      THEN
         UPDATE rle_communicatie_nummers cnr
         SET    cnr.dateinde  = (SELECT GREATEST((r_cnr_000.datingang - 1)
                                                , cnr.datingang
                                                )
                                   FROM DUAL
                                )                -- HVI 20021115 ipv ==> r_cnr_000.datingang - 1
         ,      cnr.indinhown =  'J'
         WHERE  cnr.id                        <> r_cnr_000.id
         AND    cnr.rle_numrelatie            =  r_cnr_000.rle_numrelatie
         AND    NVL(cnr.rol_codrol,'@')       =  NVL(r_cnr_000.rol_codrol,'@')
         AND    cnr.dwe_wrddom_srtcom         =  r_cnr_000.dwe_wrddom_srtcom
         AND    NVL(TO_CHAR(cnr.ras_id), '@') =  NVL(TO_CHAR(r_cnr_000.ras_id), '@')
         AND    cnr.dateinde                  IS NULL
		 and    cnr.adm_code                  = r_cnr_000.adm_code
         ;
      END IF;

      /* Bij wijzigen begindatum */
      IF l_job          =  'U'                  AND
         l_olddatingang <> r_cnr_000.datingang
      THEN
         UPDATE rle_communicatie_nummers cnr
         SET    cnr.dateinde  = (SELECT GREATEST((r_cnr_000.datingang - 1)
                                                , cnr.datingang
                                                )
                                   FROM DUAL
                                )                -- HVI 20021115 ipv ==> r_cnr_000.datingang - 1
         WHERE  cnr.id                        <> r_cnr_000.id
         AND    cnr.rle_numrelatie            =  r_cnr_000.rle_numrelatie
         AND    NVL(cnr.rol_codrol,'@')       =  NVL(r_cnr_000.rol_codrol,'@')
         AND    cnr.dwe_wrddom_srtcom         =  r_cnr_000.dwe_wrddom_srtcom
         AND    cnr.indinhown                 =  'J'
         AND    NVL(TO_CHAR(cnr.ras_id), '@') =  NVL(TO_CHAR(r_cnr_000.ras_id), '@')
         AND    cnr.dateinde                  =  l_olddatingang - 1
		 and    cnr.adm_code                  =  r_cnr_000.adm_code
         ;
      END IF ;

      /* Bij verwijderen */
      /* als een cnum verwijderd wordt waarvan de einddatum naar een broeder
         record overerft was, dan schuift de einddatum van de broeder op naar de
         einddatum van het verwijderde record.
      */
      IF l_job = 'D'
      THEN
         UPDATE rle_communicatie_nummers cnr
         SET    cnr.dateinde  = l_olddateinde
         ,      cnr.indinhown = l_oldindinhown
         WHERE  cnr.rle_numrelatie            = l_oldnumrelatie
         AND    NVL(cnr.rol_codrol,'@')       = NVL(l_oldcodrol,'@')
         AND    cnr.dwe_wrddom_srtcom         = l_oldsrtcom
         AND    NVL(TO_CHAR(cnr.ras_id), '@') =  NVL(TO_CHAR(l_oldrasid), '@')
         AND    cnr.dateinde                  = l_olddatingang - 1
         AND    cnr.indinhown                 = 'J'
         and    cnr.adm_code                  = r_cnr_000.adm_code
         ;
      END IF ;


      /* Overlapcontrole voor "broederrecords" */
      IF l_job <> 'D'
      THEN
         FOR r_cnr_003 IN c_cnr_003( r_cnr_000.rle_numrelatie
                                   , r_cnr_000.dwe_wrddom_srtcom
                                   , r_cnr_000.rol_codrol
                                   , r_cnr_000.ras_id
								   , r_cnr_000.adm_code
                                   )
         LOOP
            IF  r_cnr_003.id <> r_cnr_000.id
            THEN
               STM_ALGM_CHECK.ALGM_DATOVLP( r_cnr_000.datingang
                                          , r_cnr_000.dateinde
                                          , r_cnr_003.datingang
                                          , r_cnr_003.dateinde
                                          ) ;
            END IF ;
         END LOOP ;
      END IF ;

      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;

   END LOOP ;
   stm_algm_muttab.clear_array ;

EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
	RAISE ;
END ;
/