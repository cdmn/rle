CREATE OR REPLACE TRIGGER comp_rle.RLE_RAS_BRI
 BEFORE INSERT
 ON comp_rle.RLE_RELATIE_ADRESSEN
 FOR EACH ROW
DECLARE



L_IND_AANGEMAAKT VARCHAR2(1);
L_ERROR VARCHAR2(2000);
L_POSTCODE VARCHAR(15);
L_HUISNR NUMBER(10);
L_TOEVNUM VARCHAR2(30);
L_STRAAT VARCHAR2(40);
L_WOONPLAATS VARCHAR2(40);
L_LAND VARCHAR2(40);
l_datoverlijden date;

/* Ophalen van eerste RAS van een bepaalde relatie,srt ... */
CURSOR C_RAS_026
 (B_RLE_NUMRELATIE IN NUMBER
 ,B_ROL_CODROL IN VARCHAR2
 ,B_DWE_WRDDOM_SRTADR IN VARCHAR2
 )
 IS
/* C_RAS_026 */
select   ras.id, ras.datingang
from     rle_relatie_adressen ras
where    ras.rle_numrelatie      =  b_rle_numrelatie
  and    ras.dwe_wrddom_srtadr   =  b_dwe_wrddom_srtadr
  and    nvl(ras.rol_codrol,'@') =  nvl(b_rol_codrol,'@')
order by ras.datingang asc;
r_ras_026 c_ras_026%rowtype;
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
BEGIN
rle_wgr_000001.prc_chk_wgr_000001(:new.rle_numrelatie);

/* PGE toegevoegd vanwege wijziging project RLE
automatisch aanmaken RLE_RELATIEROLLEN_IN_ADM*/
rle_insert_rie(p_adm_code => :new.adm_code
             , p_rle_numrelatie => :new.rle_numrelatie
             , p_rol_codrol => :new.rol_codrol
             , p_ind_aangemaakt => l_ind_aangemaakt
             , p_error => l_error);
IF l_error IS NOT NULL
THEN
   stm_dbproc.raise_error(l_error, NULL, 'rle_bri');
END IF;
/* einde toevoeging PGE */
/* Vaste waardes aan vervallen kolommen geven */
IF :new.codorganisatie IS NULL
THEN
   :new.codorganisatie := 'A' ;
END
IF ;

/* Toegevoegde code  */
IF :new.indinhown IS NULL THEN
  :new.indinhown := 'N';
END IF;
IF :new.ind_woonboot IS NULL THEN
  :new.ind_woonboot := 'N';
END IF;
IF :new.ind_woonwagen IS NULL THEN
  :new.ind_woonwagen := 'N';
END IF;

-- Controle referentiewaarde
--
rfe_chk_refwrd( 'SAD', :new.dwe_wrddom_srtadr );

--13-08-2001 HVE
IF :new.dateinde IS NULL
THEN
  -- kijk of er records zijn die later in de tijd liggen.
  OPEN c_ras_026(:new.rle_numrelatie
                ,:new.rol_codrol
                ,:new.dwe_wrddom_srtadr
                );
  FETCH c_ras_026 INTO r_ras_026;
  IF c_ras_026%found
  THEN
    IF  :new.datingang < r_ras_026.datingang
    THEN
      -- afsluiten nieuwe record op begindatum -1 van
      -- voorheen eerste record.
      :new.dateinde     := r_ras_026.datingang - 1;
      :new.indinhown    := 'J';

END IF;
  END IF;
  CLOSE c_ras_026;
END IF;

/* BRIER????? /BR MOD0003
  "Postbus" of "Antwoordnummer" alleen toegestaan bij "CA"
  (Correspondentie Adres)
*/
IF  :new.dwe_wrddom_srtadr != 'CA'
AND (   rle_get_straatnaam_ads( :new.ads_numadres) = 'ANTWOORDNUMMER'
    OR  rle_get_straatnaam_ads( :new.ads_numadres) = 'POSTBUS'
    )
THEN
     stm_dbproc.raise_error( 'RLE-10290' ) ;

END IF ;

IF :new.rol_codrol IS NOT NULL THEN
   rle_chk_rrl_aanw(:new.rle_numrelatie
   , :new.rol_codrol);
END IF;
/* inc 204530 */
select	datoverlijden
into    l_datoverlijden
from	rle_relaties rle
where	rle.numrelatie = :new.rle_numrelatie;
--
-- check op ingangsdatum NIET uitvoeren bij overleden persoon en CA
-- xjb, 04-05-2015 check op ingangsdatum versus overlijden helemal niet meer uitvoeren voor personen, n.a.l.v. Jira 409 (bug)
if not ( :new.rol_codrol        = 'PR'
     and l_datoverlijden is not null
     )
then
  rle_chk_rle_persdat(:new.rle_numrelatie
     , :new.datingang
     , 'relatieadressen');
end if;
--
IF :new.id IS NULL
THEN
  :new.id := rle_ras_seq1_next_id;
END IF;

/* QMS$DATA_AUDITING */
 :new.DAT_CREATIE := SYSDATE;
 :new.CREATIE_DOOR := alg_sessie.gebruiker;
 :new.DAT_MUTATIE := SYSDATE;
 :new.MUTATIE_DOOR := alg_sessie.gebruiker;
--
-- 12-11-2003, VER, SYN/PARO: adres-mutaties van personen met binnenlands adres alleen publiceren bij GBA-status OV
-- 22-07-2005, VER, PR-UII, Consolidatie ZEUS/HERA, adressen behorend bij rol 'PR' worden niet meer gesynchroniseerd
-- 27-02-2006, NKU, project Levensloop: adressen voor niet-MTB deelnemers moeten
--                   niet gesynchroniseerd worden, niet-MTB werkgevers wel tbv CODA (rol WB en WD).
-- 27-02-2006  SKL  chg 48779: OA adres mag niet gesynchroniseerd worden.
-- 02-02-2009, XMB  Project Levensloop. Niet MTB deelnemers hebben nu rol 'PR'.
-- 24-02-2010, UKV  De ingangsdatum moet tbv synchronisatie met sysdate gevuld zijn.
--             Dit omdat de werkzaamheden en werkgeverstatussen ook altijd op sysdate worden opgevoerd
--
if :old.rol_codrol != 'PR'
or   :new.dwe_wrddom_srtadr <> 'OA'
then
   IBM.PUBLISH_EVENT
      ( 'MUT RLE ADRESSEN'
      , 'I'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.PROVINCIE
      , NULL, :new.ID
      , NULL, to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.RLE_NUMRELATIE
      , NULL, :new.DWE_WRDDOM_SRTADR
      , NULL, :new.ADS_NUMADRES
      , NULL, :new.CODORGANISATIE
      , NULL, :new.INDINHOWN
      , NULL, :new.ROL_CODROL
      , NULL, :new.NUMKAMER
      , NULL, to_char(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.LOCATIE
      , NULL, :new.IND_WOONBOOT
      , NULL, :new.IND_WOONWAGEN
      , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.CREATIE_DOOR
      , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.MUTATIE_DOOR
      );
end if;

/**
** 17-03-2016 rln jira MNGBA-1008
** Onderstaande code is een kort-door-de-bocht oplossing voor het bijwerken van de DER-datum bij een verhuizing van of naar het buitenland
**
*/
declare
  cursor c_ads_btl(b_numadres rle_adressen.numadres%type)
  is
     select 1
	 from   rle_adressen ads
	 where  ads.numadres = b_numadres
	 and    ads.lnd_codland != 'NL'
  ;
  cursor c_asa (b_rle_numrelatie rle_afstemmingen_gba.rle_numrelatie%type)
  is
     select asa.code_gba_status
     ,      asa.afnemers_indicatie
     from   rle_afstemmingen_gba asa
	 where  asa.rle_numrelatie = :new.rle_numrelatie
  ;
  r_asa    c_asa%rowtype;
  l_dummy  pls_integer;
begin
  open c_asa(:new.rle_numrelatie);
  fetch c_asa into r_asa;
  close c_asa;

  open c_ads_btl(:new.ads_numadres);
  fetch c_ads_btl into l_dummy;
  if c_ads_btl%found
  then
    ibm.publish_event ( 'MUT RLE AFSTEMMINGEN GBA'
	    			  , 'U'
					  , to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss')
					  , null /*:old.creatie_door*/ , :new.creatie_door
					  , null /*to_char(:old.dat_creatie, 'dd-mm-yyyy hh24:mi:ss')*/, to_char(:new.dat_creatie, 'dd-mm-yyyy hh24:mi:ss')
					  , null /*:old.mutatie_door*/ , :new.mutatie_door
					  , null /*to_char(:old.dat_mutatie, 'dd-mm-yyyy hh24:mi:ss')*/, to_char(:new.dat_mutatie, 'dd-mm-yyyy hh24:mi:ss')
					  , null /*r_asa.code_gba_status*/ , r_asa.code_gba_status
					  , 'X' , r_asa.afnemers_indicatie -- forceren opnieuw bepalen der-datum
					  , null /*:old.rle_numrelatie*/ , :new.rle_numrelatie
					  );
   end if;
   close c_ads_btl;
end;




END RLE_RAS_BRI;
/