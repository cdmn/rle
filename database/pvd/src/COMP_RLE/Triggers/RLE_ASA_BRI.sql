CREATE OR REPLACE TRIGGER comp_rle.RLE_ASA_BRI
 BEFORE INSERT
 ON comp_rle.RLE_AFSTEMMINGEN_GBA
 FOR EACH ROW
BEGIN
   /* DATA_AUDITING */
   :new.DAT_CREATIE  := SYSDATE ;
   :new.CREATIE_DOOR := USER    ;
   :new.DAT_MUTATIE  := SYSDATE ;
   :new.MUTATIE_DOOR := USER    ;

   /* BRIER0029
      Voorkomen dat een RELATIE een Afnemersindicatie 'J' heeft
      zonder A-Nummer.
   */ /* */
   IF :new.afnemers_indicatie = 'J'
   THEN
      IF rle_m03_rec_extcod( 'GBA', :new.rle_numrelatie, 'PR', SYSDATE ) IS NULL
      THEN
         stm_dbproc.raise_error( 'RLE-10354' ) ;
      END IF ;
   END IF ;

   /* JPG 23-02-2012
      BR_CEV0029_ASA: Indien de code gba status = OV en afnemersindicatie = J dan moet ind opvoer gbav N zijn
   */
   IF    :new.AFNEMERS_INDICATIE = 'J'
   AND   :new.CODE_GBA_STATUS    = 'OV'
   THEN
     :new.ind_opvoer_gbav := 'N';
     --
     -- Tevens moet het binnenlands correspondentie adres dat is opgevoerd bij de registratie obv het GBA-V verwijderd worden
     --
     if :old.ind_opvoer_gbav = 'J'
     then
       RLE_VERWIJDER_ADRES (P_RELATIENUMMER => :new.RLE_NUMRELATIE
                           ,P_SOORT_ADRES   => 'CA'
                           ,P_LANDCODE      => 'NL');
     end if;
   END IF;
   --
   -- JPG 30-03-2012
   -- Indien indicatie opvoer gbav dan registratiedatum vullen met sysdate. Anders leegmaken.
   --
   IF :new.ind_opvoer_gbav = 'N'
   THEN
    :new.dat_registratie_gbav := NULL;
   ELSIF :new.ind_opvoer_gbav = 'J'
   THEN
     :new.dat_registratie_gbav := SYSDATE;
  END IF;

   /* Publicatie voor synchronisatie */
   -- 12-11-2003, VER, alleen een publicatie, indien afnemersindicatie = 'J' of gba_status = 'OV'
   --
   IF    :new.AFNEMERS_INDICATIE = 'J'
      OR :new.CODE_GBA_STATUS    = 'OV'
   THEN
      IBM.PUBLISH_EVENT
         ( 'MUT RLE AFSTEMMINGEN GBA'
         , 'I'
         , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, :new.CREATIE_DOOR
         , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, :new.MUTATIE_DOOR
         , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, :new.CODE_GBA_STATUS
         , NULL, :new.AFNEMERS_INDICATIE
         , NULL, :new.RLE_NUMRELATIE
         );
   END IF;
END ;
/