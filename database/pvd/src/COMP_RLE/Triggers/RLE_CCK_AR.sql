CREATE OR REPLACE TRIGGER comp_rle.RLE_CCK_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_COMMUNICATIE_CAT_KANALEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE

-- Program Data
L_STACK VARCHAR2(128);
-- PL/SQL Block
BEGIN
/* RLE_CCK_AR */
  /* Per rij de bewerking meegeven */
  IF  inserting THEN
    stm_add_word (l_stack, 'I');
  ELSIF  updating THEN
    stm_add_word (l_stack, 'U');
  ELSIF  deleting THEN
    stm_add_word (l_stack, 'D');
  ELSE
    stm_add_word (l_stack, ' ');
  END IF;
  /* per rij de benodigde old values meegeven */
  IF  NOT inserting THEN
    stm_add_word (l_stack, TO_CHAR(:old.datum_begin));
    stm_add_word (l_stack, TO_CHAR(:old.datum_einde));
    stm_add_word (l_stack, :old.dwe_wrddom_srtcom);
        stm_add_word (l_stack, :old.ccn_id);
  END IF;
  /* vullen rowid en linkstring in de interne tabel */
  IF  deleting THEN
    stm_algm_muttab.add_rowid(:old.rowid, l_stack);
  ELSE
    stm_algm_muttab.add_rowid(:new.rowid, l_stack);
  END IF;
END RLE_CCK_AR;
/