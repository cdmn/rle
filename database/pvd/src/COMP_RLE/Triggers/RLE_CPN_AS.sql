CREATE OR REPLACE TRIGGER comp_rle.RLE_CPN_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_CONTACTPERSONEN

BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
   l_rowid  ROWID           ;
   l_job    VARCHAR2( 1 )   ;
   CURSOR lc_cpn_000(
        cpr_rowid  IN  ROWID
      ) IS
      SELECT *
      FROM   rle_contactpersonen cpn
      WHERE  cpn.rowid = cpr_rowid
      ;
   lr_cpn_000  lc_cpn_000%ROWTYPE ;
   CURSOR lc_ras_001(
        cpn_ras_id  IN  rle_relatie_adressen.id%TYPE
      ) IS
      SELECT ras.rol_codrol
      ,      ras.rle_numrelatie
      FROM   rle_relatie_adressen  ras
      WHERE  ras.id = cpn_ras_id
      ;
   lr_ras_001  lc_ras_001%ROWTYPE ;
   CURSOR lc_rkg_001(
        cpv_rol_codrol_beperkt_tot  rle_rol_in_koppelingen.rol_codrol_beperkt_tot%TYPE
      , cpv_rol_codrol_met          rle_rol_in_koppelingen.rol_codrol_met%TYPE
      ) IS
      SELECT rkg.id
      FROM   rle_rol_in_koppelingen  rkg
      WHERE  rkg.rol_codrol_beperkt_tot = cpv_rol_codrol_beperkt_tot
      AND    rkg.rol_codrol_met         = cpv_rol_codrol_met
      ;
   lr_rkg_001  lc_rkg_001%ROWTYPE ;
   l_code_soort_post   rle_verzendbestemmingen.code_soort_post%TYPE  ;
   l_code_soort_adres  rle_verzendbestemmingen.code_soort_adres%TYPE ;
BEGIN
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   WHILE l_rowid IS NOT NULL
   LOOP
      /* Ophalen standaard variabelen */
      l_job := stm_get_word( l_stack ) ;
      /* Ophalen variabelen per soort statement */
      IF    l_job = 'I'
      THEN
         /* Ophalen variabelen voor insert */
         NULL ;
      ELSIF l_job = 'U'
      THEN
         /* Ophalen variabelen voor update */
         NULL ;
      ELSIF l_job = 'D'
      THEN
         /* Ophalen variabelen voor delete */
         NULL ;
      END IF ;
      /* Ophalen halen record */
      IF l_job IN ( 'I', 'U' )
      THEN
         OPEN lc_cpn_000( l_rowid ) ;
         FETCH lc_cpn_000 INTO lr_cpn_000 ;
         CLOSE lc_cpn_000 ;
      END IF ;
      /* Een contactpersoon moet de rol CP hebben.
         Dit werd eerst gegarandeerd IN het scherm.
         Verplaatst naar de database.
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         /* Controleren of de relatie wel de rol "CP" heeft. */
         BEGIN
            rle_rrl_chk_fk( lr_cpn_000.rle_numrelatie
                          , 'CP'
                          ) ;
         EXCEPTION
            WHEN OTHERS THEN
               /* Nope, de rol is niet aanwezig */
               INSERT INTO rle_relatie_rollen(
                    rle_numrelatie
                  , rol_codrol
                  , codorganisatie
                  ) VALUES(
                    lr_cpn_000.rle_numrelatie
                  , 'CP'
                  , 'A'
                  ) ;
         END ;
      END IF ;
      /* BRENT0014
         Indien contactpersoon vastgelegd wordt bij relatie adres,
         dan ook vastleggen in relatie koppeling.
      */
      IF l_job = 'I'
      THEN
         /* Ophalen rol van relatie adres. Dit is de rol waarmee
            de relatie koppeling gemaakt moet worden.
         */
         OPEN lc_ras_001( lr_cpn_000.ras_id ) ;
         FETCH lc_ras_001 INTO lr_ras_001 ;
         CLOSE lc_ras_001 ;
         IF lr_ras_001.rol_codrol IS NOT NULL
         THEN
            /* Zoek een rol_in_koppeling met
                  ROL_CODROL_BEPERKT_TOT = 'CP'
               en ROL_CODROL_MET = lr_ras_001.rol_codrol
            */
            OPEN lc_rkg_001( 'CP'
                           , lr_ras_001.rol_codrol
                           ) ;
            FETCH lc_rkg_001 INTO lr_rkg_001 ;
            CLOSE lc_rkg_001 ;
            IF lr_rkg_001.id IS NOT NULL
            THEN
               IF rle_chk_rkn_exists( lr_cpn_000.rle_numrelatie
                                    , 'CP'
                                    , lr_ras_001.rle_numrelatie
                                    , lr_ras_001.rol_codrol
                                    , TRUNC( SYSDATE )
                                    ) = 'N'
               THEN
                  INSERT INTO rle_relatie_koppelingen(
                       id
                     , rkg_id
                     , rle_numrelatie
                     , rle_numrelatie_voor
                     , dat_begin
                     ) VALUES(
                       rle_rkn_seq1.nextval
                     , lr_rkg_001.id
                     , lr_cpn_000.rle_numrelatie
                     , lr_ras_001.rle_numrelatie
                     , SYSDATE
                     ) ;
               END IF ;
            END IF ;
         END IF ;
      END IF ;
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   END LOOP ;
   stm_algm_muttab.clear_array ;
EXCEPTION
   WHEN OTHERS THEN
      stm_algm_muttab.clear_array ;
      RAISE ;
END ;
END RLE_CPN_AS;
/