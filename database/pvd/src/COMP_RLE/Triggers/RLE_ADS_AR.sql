CREATE OR REPLACE TRIGGER comp_rle.RLE_ADS_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_ADRESSEN
 FOR EACH ROW
DECLARE
/*****************************************************************************************************/
/* Trigger RLE.RLE_ADS_AR:                                                                           */
/*                                                                                                   */
/*                                                                                                   */
/* Datum      Wie         Omschrijving                                                               */
/* ---------- ----------- ---------------------------------------------------------------------------*/
/* 18-10-2010 UKM         Meldingsnummer: PRD-INC-186820                                             */
/*                        Trigger aangepast eerder(19-03-2010) door UKV, verder niet opgeleverd in   */
/*                        PVCS maar komt overeen met mijn aapassingen. Deze zijn:                    */
/*                        De :new.wps_woonplaatsid en :new.stt_straatid worden bij Insert en Update  */
/*                        ook toegevoegd bij de l_stack. Dit is nodig om het te kunnen gebruiken bij */
/*                        de RLE_ADS_AS trigger.                                                     */
/* 01-09-2016 OKR         Eventstore, bij update ook old values op stack                                                                                                  */
/*                                                                                                   */
/*                                                                                                   */
/*****************************************************************************************************/

   l_stack  VARCHAR2( 2000 ) ;
BEGIN
   /* Add variabelen */
   IF inserting
   THEN
      stm_add_word( l_stack, 'I' ) ;
      /* BRENT0001 */
      stm_add_word( l_stack, :new.lnd_codland ) ;
      stm_add_word( l_stack, :new.postcode ) ;
      stm_add_word( l_stack, :new.wps_woonplaatsid);
      stm_add_word( l_stack, :new.stt_straatid);
      stm_add_word( l_stack, :new.huisnummer ) ;
      stm_add_word( l_stack, :new.toevhuisnum ) ;
      stm_add_word( l_stack, :new.numadres ) ;
   ELSIF updating
   THEN
      stm_add_word( l_stack, 'U' ) ;
      /* BRENT0001 */
      stm_add_word( l_stack, :new.lnd_codland ) ;
      stm_add_word( l_stack, :new.postcode ) ;
      stm_add_word( l_stack, :new.wps_woonplaatsid);
      stm_add_word( l_stack, :new.stt_straatid);
      stm_add_word( l_stack, :new.huisnummer ) ;
      stm_add_word( l_stack, :new.toevhuisnum ) ;
      stm_add_word( l_stack, :new.numadres ) ;
      -- OKR:
      stm_add_word( l_stack, :old.lnd_codland ) ;
      stm_add_word( l_stack, :old.postcode ) ;
      stm_add_word( l_stack, :old.wps_woonplaatsid);
      stm_add_word( l_stack, :old.stt_straatid);
      stm_add_word( l_stack, :old.huisnummer ) ;
      stm_add_word( l_stack, :old.toevhuisnum ) ;
      stm_add_word( l_stack, :old.numadres ) ;
   ELSE /* deleting */
      stm_add_word( l_stack, 'D' ) ;
      /* niets nodig voor delete */
   END IF ;
   /* Add ROWID */
   IF inserting OR updating
   THEN
      stm_algm_muttab.add_rowid( :new.rowid, l_stack ) ;
   ELSE
      stm_algm_muttab.add_rowid( :old.rowid, l_stack ) ;
   END IF ;
END ;
/