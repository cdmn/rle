CREATE OR REPLACE TRIGGER comp_rle.RLE_PBN_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_PCOD_BATCHRUN
 FOR EACH ROW

BEGIN
/* Handmatig toegevoegd */
-- Controle referentiewaarde
ibm.ibm('chk_refwrd'
              , 'RLE'
              ,'PBN'
              ,:new.subtype
              );
  if stm_dbproc.column_modified(
:old.id,
:new.id)
  or stm_dbproc.column_modified(
:old.draaimaand,
:new.draaimaand)
  or stm_dbproc.column_modified(
:old.subtype,
:new.subtype)
  then
    /* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  end if;
END RLE_PBN_BRU;
/