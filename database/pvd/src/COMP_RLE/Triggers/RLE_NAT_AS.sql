CREATE OR REPLACE TRIGGER comp_rle.RLE_NAT_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_NATIONALITEITEN
DECLARE

--
/************************************************************

Triggernaam: rle_nat_as

Datum       Wie        Wat

--------    ---------- -----------------------------------
21-10-2009  UAO        Creation
30-10-2009  XMK        Kolom code_nationaliteit hernoemd naar
                       code_nationaliteit.
*************************************************************/
--
cursor c_nat ( b_rowid in rowid)
is
select *
from rle_nationaliteiten
where rowid =b_rowid;
--
--
cursor c_nat_vorig ( b_rle_numrelatie in number, b_begindatum in date , b_new_id in number )
is
select nat.*
from   rle_nationaliteiten nat
where  nat.rle_numrelatie = b_rle_numrelatie
and    nat.id <> b_new_id
and    nat.dat_begin =
       ( select max(nat2.dat_begin)
        from rle_nationaliteiten nat2
        where nat2.rle_numrelatie = b_rle_numrelatie
        and nat2.dat_begin < b_begindatum
        and nat2.id <> b_new_id
       );
--
cursor c_nat_volgend ( b_rle_numrelatie in number, b_begindatum in date , b_new_id in number )
is
select nat.*
from   rle_nationaliteiten nat
where  nat.rle_numrelatie = b_rle_numrelatie
and    nat.id <> b_new_id
and    nat.dat_begin =
       ( select min(nat2.dat_begin)
        from rle_nationaliteiten nat2
        where nat2.rle_numrelatie = b_rle_numrelatie
        and nat2.dat_begin >= b_begindatum
        and nat2.id <> b_new_id
       );
--
r_nat              c_nat%rowtype;
r_nat_vorig        c_nat_vorig%rowtype;
r_nat_volgend      c_nat_volgend%rowtype;
--
l_stack                  varchar2(128);
l_rowid                  rowid;
l_job                    varchar2(1);
l_old_id                 number;
l_old_rle_numrelatie     number;
l_old_code_nationaliteit rle_nationaliteiten.code_nationaliteit%type;
l_old_dat_begin          date;
l_old_dat_einde          date;
l_new_id                 number;
l_new_rle_numrelatie     number;
l_new_code_nationaliteit rle_nationaliteiten.code_nationaliteit%type;
l_new_dat_begin          date;
l_new_dat_einde          date;
l_aantal_overlap         number := 0;
l_nat_aantal             number;
--
BEGIN
--
stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
--
while l_rowid is not null
loop
  --
  l_job                        := stm_get_word( l_stack ) ;
  l_old_id                     := stm_get_word(l_stack);
  l_old_rle_numrelatie         := stm_get_word(l_stack);
  l_old_code_nationaliteit     := stm_get_word(l_stack);
  l_old_dat_begin              := stm_get_word(l_stack);
  l_old_dat_einde              := stm_get_word(l_stack);
  --
  open  c_nat(l_rowid);
  fetch c_nat into r_nat;
    l_new_id                    := r_nat.id;
    l_new_rle_numrelatie        := r_nat.rle_numrelatie;
    l_new_code_nationaliteit    := r_nat.code_nationaliteit;
    l_new_dat_begin             := r_nat.dat_begin;
    l_new_dat_einde             := r_nat.dat_einde;
  close c_nat;
  --
  if
    l_new_dat_einde is not null and
    l_new_dat_begin > l_new_dat_einde
  then
    stm_dbproc.raise_error( 'RLE-10417' ) ;  -- einddatum voor begindatum
  end if;
  --
  open  c_nat_vorig( l_new_rle_numrelatie , l_new_dat_begin, l_new_id );
  fetch c_nat_vorig into r_nat_vorig;
  close c_nat_vorig;
  --
  open  c_nat_volgend( l_new_rle_numrelatie , l_new_dat_begin, l_new_id );
  fetch c_nat_volgend into r_nat_volgend;
  close c_nat_volgend;
  --
  if
    l_job =  'I'
  then
    --
    if r_nat_vorig.id is not null
       and r_nat_vorig.dat_einde is not null
    then
      stm_algm_check.algm_datovlp
        ( l_new_dat_begin
        , l_new_dat_einde
        , r_nat_vorig.dat_begin
        , r_nat_vorig.dat_einde
        );
    end if;
    --
    if r_nat_volgend.id is not null
    then
      stm_algm_check.algm_datovlp
        ( l_new_dat_begin
        , l_new_dat_einde
        , r_nat_volgend.dat_begin
        , r_nat_volgend.dat_einde
        );
    end if;
    --
    if r_nat_vorig.id is not null
    then
      if r_nat_vorig.dat_einde is null
      then
        update rle_nationaliteiten
        set    dat_einde = l_new_dat_begin - 1
        where  id = r_nat_vorig.id ;
      end if;
    end if;
    --
  elsif
    l_job =  'U'
  then
    --
    if r_nat_volgend.id is not null
    then
      stm_algm_check.algm_datovlp
        ( l_new_dat_begin
        , l_new_dat_einde
        , r_nat_volgend.dat_begin
        , r_nat_volgend.dat_einde
        );
    end if;
    --
    if l_new_dat_einde <> r_nat_volgend.dat_begin - 1
    then
      stm_dbproc.raise_error( 'RLE-10415' ) ;  -- einddatum voor begindatum
    end if;
    --
    if r_nat_vorig.id is not null
    then
      update rle_nationaliteiten
      set    dat_einde = l_new_dat_begin - 1
      where  id = r_nat_vorig.id;
    end if;
    --
  elsif l_job = 'D'
  then
    --
    open  c_nat_vorig( l_old_rle_numrelatie , l_old_dat_begin, l_old_id );
    fetch c_nat_vorig into r_nat_vorig;
    close c_nat_vorig;
    --
    if r_nat_vorig.id is not null
    then
      update rle_nationaliteiten
      set    dat_einde = l_old_dat_einde
      where  id = r_nat_vorig.id;
    end if;
    --
  end if;
     --
  stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
  --
end loop;
   --
stm_algm_muttab.clear_array ;
   --
END;
/