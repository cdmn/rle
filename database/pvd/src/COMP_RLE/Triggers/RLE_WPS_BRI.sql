CREATE OR REPLACE TRIGGER comp_rle.RLE_WPS_BRI
 BEFORE INSERT
 ON comp_rle.RLE_WOONPLAATSEN
 FOR EACH ROW
BEGIN
if RLE_CHK_LND_BLOCKED(:new.lnd_codland )
then
   raise_application_error(-20000, 'RLE-10386');
end if;

if :new.woonplaatsid is null
then
  :new.woonplaatsid := rle_wps_seq1_next_id;
end if;
/* QMS$DATA_AUDITING */
:new.DAT_CREATIE := sysdate;
:new.CREATIE_DOOR := alg_sessie.gebruiker;
:new.DAT_MUTATIE := sysdate;
:new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_WPS_BRI;
/