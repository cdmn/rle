CREATE OR REPLACE TRIGGER comp_rle.RLE_CCK_BRI
 BEFORE INSERT
 ON comp_rle.RLE_COMMUNICATIE_CAT_KANALEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
   if :new.id is null then
      SELECT rle_cck_seq1.NEXTVAL
      INTO :NEW.ID
      FROM DUAL;
   end if;
   :new.DWE_WRDDOM_SRTCOM := upper(:new.DWE_WRDDOM_SRTCOM);
   :new.datum_begin := trunc(nvl(:new.datum_begin,sysdate));
   RFE_CHK_REFWRD('SC', :new.DWE_WRDDOM_SRTCOM);
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_CCK_BRI;
/