CREATE OR REPLACE TRIGGER comp_rle.RLE_KVR_BRI
 BEFORE INSERT
 ON comp_rle.RLE_KANAALVOORKEUREN
 FOR EACH ROW

BEGIN
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
   rle_wgr_000001.prc_chk_wgr_000001(:new.rle_numrelatie);
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := SYSDATE;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_KVR_BRI;
/