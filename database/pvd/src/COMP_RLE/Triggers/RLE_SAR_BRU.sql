CREATE OR REPLACE TRIGGER comp_rle.RLE_SAR_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_SADRES_ROLLEN
 FOR EACH ROW

BEGIN
if :new.volgnr <>
     :old.volgnr
  or ((:new.dwe_wrddom_sadres <>
       :old.dwe_wrddom_sadres)
      or
      (:new.dwe_wrddom_sadres is not null
       and
       :old.dwe_wrddom_sadres is null)
      or
      (:new.dwe_wrddom_sadres is null
       and
       :old.dwe_wrddom_sadres is not null)
     )
  then
      -- Controle referentiewaarde
      ibm.ibm('chk_refwrd'
                    , 'RLE'
                    , 'SAD'
                    , :new.dwe_wrddom_sadres);
    /* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  end if;
END RLE_SAR_BRU;
/