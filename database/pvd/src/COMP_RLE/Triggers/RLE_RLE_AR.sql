CREATE OR REPLACE TRIGGER comp_rle.RLE_RLE_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIES
 FOR EACH ROW

DECLARE
-- Program Data
L_STACK VARCHAR2(2000);
-- PL/SQL Block
BEGIN
/* RLE_RLE_AR */
  /* Per rij de bewerking meegeven */
  IF	inserting
  THEN
    stm_add_word (l_stack, 'I');
    stm_add_word (l_stack, TO_CHAR(:new.numrelatie));
  ELSIF updating
  THEN
    stm_add_word (l_stack, 'U');
    stm_add_word (l_stack, :old.datoverlijden);
    stm_add_word (l_stack, TO_CHAR(:old.numrelatie));
    stm_add_word (l_stack, :old.datgeboorte);
    stm_add_word (l_stack, :old.datbegin);
    stm_add_word (l_stack, :old.dateinde);
    -- OKR: tbv detectie schijnmutaties ihkv de Personen Event Store:
    stm_add_word (l_stack, :old.namrelatie);
    stm_add_word (l_stack, :old.dwe_wrddom_vvgsl);
    stm_add_word (l_stack, :old.dwe_wrddom_geslacht);
    stm_add_word (l_stack, :old.voornaam);
    stm_add_word (l_stack, :old.voorletter);
    stm_add_word (l_stack, :old.code_gebdat_fictief);
    stm_add_word (l_stack, :old.naam_partner);
    stm_add_word (l_stack, :old.vvgsl_partner);
    stm_add_word (l_stack, :old.code_aanduiding_naamgebruik);
    --
  ELSIF	deleting
  THEN
     stm_add_word (l_stack, 'D');
     stm_add_word (l_stack, TO_CHAR(:old.numrelatie));
  /* QMS$JOURNALLING */
  BEGIN
   INSERT
   INTO   rle_relaties_jn
   (      jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      numrelatie
   ,      codorganisatie
   ,      namrelatie
   ,      zoeknaam
   ,      indgewnaam
   ,      indinstelling
   ,      datschoning
   ,      dwe_wrddom_titel
   ,      dwe_wrddom_ttitel
   ,      dwe_wrddom_avgsl
   ,      dwe_wrddom_vvgsl
   ,      dwe_wrddom_brgst
   ,      dwe_wrddom_geslacht
   ,      dwe_wrddom_gewatitel
   ,      dwe_wrddom_rvorm
   ,      voornaam
   ,      voorletter
   ,      aanschrijfnaam
   ,      datgeboorte
   ,      code_gebdat_fictief
   ,      datoverlijden
   ,      datoprichting
   ,      dateinde
   ,      naam_partner
   ,      vvgsl_partner
   ,      code_aanduiding_naamgebruik
   ,      dat_creatie
   ,      dat_mutatie
   ,      mutatie_door
   ,      creatie_door
   ,      handelsnaam
   ,      datbegin
   ,      datcontrole
   ,      indcontrole
   ,      regdat_overlbevest
   )
   VALUES
   (      alg_sessie.gebruiker
   ,      SYSDATE
   ,      'DEL'
   ,      :old.numrelatie
   ,      :old.codorganisatie
   ,      :old.namrelatie
   ,      :old.zoeknaam
   ,      :old.indgewnaam
   ,      :old.indinstelling
   ,      :old.datschoning
   ,      :old.dwe_wrddom_titel
   ,      :old.dwe_wrddom_ttitel
   ,      :old.dwe_wrddom_avgsl
   ,      :old.dwe_wrddom_vvgsl
   ,      :old.dwe_wrddom_brgst
   ,      :old.dwe_wrddom_geslacht
   ,      :old.dwe_wrddom_gewatitel
   ,      :old.dwe_wrddom_rvorm
   ,      :old.voornaam
   ,      :old.voorletter
   ,      :old.aanschrijfnaam
   ,      :old.datgeboorte
   ,      :old.code_gebdat_fictief
   ,      :old.datoverlijden
   ,      :old.datoprichting
   ,      :old.dateinde
   ,      :old.naam_partner
   ,      :old.vvgsl_partner
   ,      :old.code_aanduiding_naamgebruik
   ,      :old.dat_creatie
   ,      :old.dat_mutatie
   ,      :old.mutatie_door
   ,      :old.creatie_door
   ,      :old.handelsnaam
   ,      :old.datbegin
   ,      :old.datcontrole
   ,      :old.indcontrole
   ,      :old.regdat_overlbevest
   );
  END;
ELSE
  	stm_add_word (l_stack, ' ');
END IF;
  /* per rij de benodigde :old values meegeven */
  /* vullen rowid en linkstring in de interne tabel */
  IF	deleting
  THEN
  	stm_algm_muttab.add_rowid(:old.rowid, l_stack);
  ELSE
  	stm_algm_muttab.add_rowid(:new.rowid, l_stack);
  END IF;
END RLE_RLE_AR;
/