CREATE OR REPLACE TRIGGER comp_rle."RLE_ASA_BS"
  before delete or insert or update on comp_rle."RLE_AFSTEMMINGEN_GBA"

begin
  stm_algm_muttab.init_array;
end;
/