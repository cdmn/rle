CREATE OR REPLACE TRIGGER comp_rle.RLE_RKN_BRI
 BEFORE INSERT
 ON comp_rle.RLE_RELATIE_KOPPELINGEN
 FOR EACH ROW

declare
  cursor c_rkg (b_rkg_id in number)
  is
  select rkg.rol_codrol_beperkt_tot
  from   rle_rol_in_koppelingen rkg
  where rkg.id = b_rkg_id
  ;
  --
  cursor c_rkg2 (b_rkg_id in number)
  is
  select 'J'
  from   rle_rol_in_koppelingen rkg
  where  rkg.code_rol_in_koppeling in ('H', 'G', 'S')
  and    rkg.id = b_rkg_id
  ;
  --
  cursor c_rle ( b_rle_numrelatie in number
               , b_rle_numrelatie_voor in number
               )
  is
  select 'J'
  from   rle_relaties rle
  where (  rle.numrelatie = b_rle_numrelatie
        or rle.numrelatie = b_rle_numrelatie_voor
        )
  and   rle.datoverlijden is not null
  ;
  --
  l_rol_in_rkg  rle_rol_in_koppelingen.rol_codrol_beperkt_tot%type;
  l_dummy       varchar2(1);
  l_rkg2_fnd    boolean := false;
  l_rle_fnd     boolean := false;

begin
  /* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden
  */
  rle_wgr_000001.prc_chk_wgr_000001(pin_numrelatie => :new.rle_numrelatie);

  /* QMS$DATA_AUDITING */
  :new.DAT_CREATIE := sysdate;
  :new.CREATIE_DOOR := alg_sessie.gebruiker;
  :new.DAT_MUTATIE := sysdate;
  :new.MUTATIE_DOOR := alg_sessie.gebruiker;

  /* Publicatie voor synchronisatie */
  -- 12-11-2003, VER, nulmutaties niet synchroniseren.
  --                  en bij verwantschappen alleen publicaties indien beide personen GBA-status OV hebben
  -- 01-06-2005, VER, PR-UII Consolidatie ZEUS/HERA, alleen publiceren indien de relatie-koppeling GEEN
  --                  verwantschap betreft
  --
  --
  -- 02-03-2006. NKU, Project Levensloop
  --                           Relaties met rol WA, WB, WC of WD
  --                           worden niet gepubliceerd.
  --
  if    not rle_chk_rol_niet_mtb(p_relatienr => :new.rle_numrelatie)
    and not rle_chk_rol_niet_mtb(p_relatienr => :new.rle_numrelatie_voor)
  then
    open c_rkg (b_rkg_id => :new.rkg_id);
    fetch c_rkg into l_rol_in_rkg;
    if c_rkg%found
    then
      if l_rol_in_rkg <> 'PR'
      then
         ibm.publish_event
            ( 'MUT RLE KOPPELINGEN'
            , 'i'
            , to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss')
            , null, :new.id
            , null, :new.rkg_id
            , null, :new.rle_numrelatie
            , null, :new.rle_numrelatie_voor
            , null, to_char(:new.dat_begin, 'dd-mm-yyyy hh24:mi:ss')
            , null, to_char(:new.dat_einde, 'dd-mm-yyyy hh24:mi:ss')
            , null, :new.creatie_door
            , null, to_char(:new.dat_creatie, 'dd-mm-yyyy hh24:mi:ss')
            , null, :new.mutatie_door
            , null, to_char(:new.dat_mutatie, 'dd-mm-yyyy hh24:mi:ss')
            , l_rol_in_rkg
            );
      end if;
    end if;
    close c_rkg;
     --
  end if;

  --
  --b+ xjb, 07-10-2008, afsplitsen bnp
  -- br_cev0025: rkn afhandelen beeindigen relatie
  if    :new.dat_einde is not null
    and :new.dat_einde <= sysdate
    -- and :new.dat_einde >= cn_ingangsdatum_bnp_afsplitsen
    and  rle_rkn.get_scheiding_partner_enabled    -- xcw 2-11-2012
  then
    --
    -- is het een huwelijk/gegistreerd partnerschap of samenlevingsovereenkomst
    open c_rkg2 (b_rkg_id => :new.rkg_id);
    fetch c_rkg2 into l_dummy;
    l_rkg2_fnd := c_rkg2%found;
    close c_rkg2;
    --
    if l_rkg2_fnd
    then
      --
      -- is een van de partners al overleden?
      open c_rle ( b_rle_numrelatie => :new.rle_numrelatie
                 , b_rle_numrelatie_voor => :new.rle_numrelatie_voor
                 );
      fetch c_rle into l_dummy;
      l_rle_fnd := c_rle%found;
      close c_rle;
      --
      if not l_rle_fnd
      then
        ibm.publish_event
          ( 'SCHEIDING_PARTNERS'
          , 'RLE'
          , :new.rle_numrelatie
          , to_char(:new.dat_einde, 'dd-mm-yyyy hh24:mi:ss')
         );
      end if;
      --
    end if;
    --
  end if;
--
end;
/