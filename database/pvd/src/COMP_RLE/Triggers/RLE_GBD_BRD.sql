CREATE OR REPLACE TRIGGER comp_rle.RLE_GBD_BRD
 BEFORE DELETE
 ON comp_rle.RLE_GEMOEDSBEZWAARDHEDEN
 FOR EACH ROW
/*
-- 19-02-2015 ZSF Extra attribuut voor event rle.GemoedsbezwaardPersoonGemuteerd
17-11-2014 ZSF nieuw event voor aansluiting Bancs
*/
BEGIN
   /* Publicatie voor synchronisatie */
   IBM.PUBLISH_EVENT
      ( 'MUT GEMOEDSBEZWAARDHEDEN'
      , 'D'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.RLE_NUMRELATIE                               , NULL
      , to_char(:old.DAT_BEGIN, 'dd-mm-yyyy hh24:mi:ss')  , NULL
      , to_char(:old.DAT_EINDE, 'dd-mm-yyyy hh24:mi:ss')  , NULL
      , :old.CREATIE_DOOR                                 , NULL
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.MUTATIE_DOOR                                 , NULL
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      );
--
    declare
      l_event xmltype;
    begin
        l_event := eventhandler.create_event ( p_event_code => 'rle.GemoedsbezwaardPersoonGemuteerd' );
        eventhandler.add_num_value( p_event => l_event
                                  , p_key => 'RELATIENUMMER'
                                  , p_value => :old.rle_numrelatie
                                  );
        eventhandler.add_value( p_event => l_event
                              , p_key => 'INDICATIE'
                              , p_value => 'N'
                              );
        eventhandler.add_value( p_event => l_event
                              , p_key => 'CODROL'
                              , p_value => 'PR'
                              );
        eventhandler.add_date_value( p_event => l_event
                                   , p_key => 'DATUM_MUTATIE'
                                   , p_value => :old.dat_mutatie
                                   );
        eventhandler.publish_asynch_event ( p_event => l_event );
    end;
END ;
/