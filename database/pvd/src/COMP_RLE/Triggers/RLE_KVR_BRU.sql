CREATE OR REPLACE TRIGGER comp_rle.RLE_KVR_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_KANAALVOORKEUREN
 FOR EACH ROW
BEGIN
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
   rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);
   /* QMS$DATA_AUDITING */
   :new.DAT_MUTATIE := SYSDATE;
   :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  /* QMS$JOURNALLING */
    INSERT
   INTO rle_kanaalvoorkeuren_jn
   (     jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      dat_begin
   ,      rle_numrelatie
   --,     code_soort_kanaalvoorkeur
   ,      creatie_door
   ,      dat_creatie
   ,      mutatie_door
   ,      dat_mutatie
   ,      cck_id
   ,      authentiek
   )
   VALUES
   (      alg_sessie.gebruiker
   ,      SYSDATE
   ,      'UPD'
   ,      :old.dat_begin
   ,      :old.rle_numrelatie
   --,      :old.code_soort_kanaalvoorkeur
   ,      :old.creatie_door
   ,      :old.dat_creatie
   ,      :old.mutatie_door
   ,      :old.dat_mutatie
   ,      :old.cck_id
   ,      :old.authentiek
   );
END;
/