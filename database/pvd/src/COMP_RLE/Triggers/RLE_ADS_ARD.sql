CREATE OR REPLACE TRIGGER comp_rle.RLE_ADS_ARD
 AFTER DELETE
 ON comp_rle.RLE_ADRESSEN
 FOR EACH ROW

DECLARE
/* QMS$JOURNALLING */
begin
   insert
   into   rle_adressen_jn
   (      jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      numadres
   ,      lnd_codland
   ,      codorganisatie
   ,      stt_straatid
   ,      wps_woonplaatsid
   ,      huisnummer
   ,      postcode
   ,      toevhuisnum
   ,      dat_creatie
   ,      creatie_door
   ,      dat_mutatie
   ,      mutatie_door
   )
   values
   (      alg_sessie.gebruiker
   ,      sysdate
   ,      'DEL'
   ,      :old.numadres
   ,      :old.lnd_codland
   ,      :old.codorganisatie
   ,      :old.stt_straatid
   ,      :old.wps_woonplaatsid
   ,      :old.huisnummer
   ,      :old.postcode
   ,      :old.toevhuisnum
   ,      :old.dat_creatie
   ,      :old.creatie_door
   ,      :old.dat_mutatie
   ,      :old.mutatie_door
   );
end;
/