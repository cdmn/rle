CREATE OR REPLACE TRIGGER comp_rle.RLE_RRL_BRD
 BEFORE DELETE
 ON comp_rle.RLE_RELATIE_ROLLEN
 FOR EACH ROW
/* Publicatie voor synchronisatie */

BEGIN
  --
  -- 05-11-2002, VER, RLE_ROLLEN worden niet gesynchroniseerd.
  -- 18-01-2005, PWA, RLE_ROLLEN worden wel gesynchroniseerd: t.b.v. Chg-18331.
  -- 23-03-2005, RBT, SAS70 : geen onderscheid in rollen maken bij publicatie
  --
   IBM.PUBLISH_EVENT
      ( 'MUT RLE ROLLEN'
      , 'D'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.RLE_NUMRELATIE                               , NULL
      , :old.ROL_CODROL                                   , NULL
      , :old.CODORGANISATIE                               , NULL
      , :old.CODPRESENTATIE                               , NULL
      , to_char(:old.DATSCHONING, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.CREATIE_DOOR                                 , NULL
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
      , :old.MUTATIE_DOOR                                 , NULL
      );
END;
/