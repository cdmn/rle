CREATE OR REPLACE TRIGGER comp_rle.RLE_WPS_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_WOONPLAATSEN
 FOR EACH ROW

BEGIN
if :new.lnd_codland <>
     :old.lnd_codland
  or :new.woonplaats <>
     :old.woonplaats
  then
    /* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  end if;
END RLE_WPS_BRU;
/