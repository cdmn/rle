CREATE OR REPLACE TRIGGER comp_rle.RLE_NAT_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_NATIONALITEITEN
 FOR EACH ROW
DECLARE

/************************************************************

Triggernaam: rle_nat_ar

Datum       Wie        Wat

--------    ---------- -----------------------------------
21-10-2009  UAO        Creation
30-10-2009  XMK        Kolom lnd_codland hernoemd naar
                       code_nationaliteit.
*************************************************************/
--
l_stack     varchar2(128);
l_job       varchar2(1);
--
BEGIN
   /* Add variabelen */
   --
   if inserting
   then
      l_job := 'I';
      stm_add_word( l_stack, 'I' ) ;
   elsif updating
   then
      l_job := 'U';
      stm_add_word( l_stack, 'U' ) ;
      stm_add_word( l_stack, :old.id ) ;
      stm_add_word (l_stack, :old.rle_numrelatie);
      stm_add_word (l_stack, :old.code_nationaliteit);
      stm_add_word (l_stack, TO_CHAR(:old.dat_begin));
      stm_add_word (l_stack, TO_CHAR(:old.dat_einde));
   else
      l_job := 'D';
      stm_add_word( l_stack, 'D' ) ;
      stm_add_word( l_stack, :old.id ) ;
      stm_add_word (l_stack, :old.rle_numrelatie);
      stm_add_word (l_stack, :old.code_nationaliteit);
      stm_add_word (l_stack, TO_CHAR(:old.dat_begin));
      stm_add_word (l_stack, TO_CHAR(:old.dat_einde));
   end if ;
   --

   if inserting or updating
   then
      stm_algm_muttab.add_rowid( :new.rowid, l_stack ) ;
   else
      stm_algm_muttab.add_rowid( :old.rowid, l_stack ) ;
   end if;
   --
   insert
   into rle_nationaliteiten_jn
   (    jn_user
   ,    jn_date_time
   ,    jn_operation
   ,    rle_numrelatie
   ,    rle_numrelatie_new
   ,    code_nationaliteit
   ,    code_nationaliteit_new
   ,    dat_begin
   ,    dat_begin_new
   ,    dat_einde
   ,    dat_einde_new
   ,    creatie_door
   ,    dat_creatie
   ,    mutatie_door
   ,    dat_mutatie
   )
   values
   (    alg_sessie.gebruiker
   ,    sysdate
   ,    l_job
   ,    :old.rle_numrelatie
   ,    :new.rle_numrelatie
   ,    :old.code_nationaliteit
   ,    :new.code_nationaliteit
   ,    :old.dat_begin
   ,    :new.dat_begin
   ,    :old.dat_einde
   ,    :new.dat_einde
   ,    alg_sessie.gebruiker
   ,    sysdate
   ,    alg_sessie.gebruiker
   ,    sysdate
   );
   --
END;
/