CREATE OR REPLACE TRIGGER comp_rle.RLE_FNR_BRI
 BEFORE INSERT
 ON comp_rle.RLE_FINANCIEEL_NUMMERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE



L_IND_AANGEMAAKT VARCHAR2(1);
L_ERROR VARCHAR2(2000);

cursor c_seq
is
select rle_fnr_seq1.nextval  new_id
from   dual;
l_lnd varchar2(2);
l_iban varchar2(34);
BEGIN
--begindatum niet in de toekomst
if trunc(:new.datingang) > trunc(sysdate)
then
   raise_application_error(-20000, stm_get_batch_message('RLE-00432# ' || 'Vanaf'));
end if;
--
if :new.codwijzebetaling is not null
then
   if not OIS$CHECK_REFERENCE_CODE(
          :new.codwijzebetaling,
          'WIJZE VAN BETALING')
   then
      raise_application_error(-20000, 'Deze waarde staat niet in het domein WIJZE VAN BETALING');
   end if;
end if;
--  check code wijze betaling
if rle_chk_iban_lnd (:new.lnd_codland)
then
   if :new.codwijzebetaling is null
   then
      raise_application_error(-20000, stm_get_batch_message('RLE-10430'));
   end if;
else
   if :new.codwijzebetaling is not null
   then
      raise_application_error(-20000, stm_get_batch_message('RLE-10430'));
   end if;
end if;
-- Upper IBAN en moet verplicht een SEPA land zijn
if :new.IBAN is not null
then
   :new.IBAN := upper(:new.IBAN);
   :new.BIC  := upper(:new.BIC);
   if not rle_chk_iban_lnd (:new.IBAN)
   then
      l_lnd := substr(:new.IBAN, 1, 2);
      raise_application_error(-20000, stm_get_batch_message('RLE-10424# ' || l_lnd));
   end if;
end if;
-- NL vul BBAN en haal BIC op. Overige SEPA, vul numrekening met IBAN
if :new.lnd_codland = 'NL'
then
   :new.numrekening := ltrim ( substr (:new.IBAN, 9), 0);
   :new.BIC         := stm_sepa.get_bic (:new.IBAN);
elsif :new.IBAN is not null
  and :new.numrekening is null
then
   :new.numrekening := :new.IBAN;
end if;
-- Is alles goed gevuld (of leeg)?
  if :new.lnd_codland = 'NL'
  then
    if :new.numrekening is null
       or :new.bic is null
       or :new.iban is null
    then
       raise_application_error(-20000, stm_get_batch_message('RLE-10425'));
    end if;
  else
    if rle_chk_iban_lnd (:new.lnd_codland)
    then
      if :new.bic is null
         or :new.iban is null
         or ( :new.iban != :new.numrekening )
      then
        raise_application_error(-20000, stm_get_batch_message('RLE-10425'));
      end if;
     else
      if :new.iban is not null
         or :new.bic is not null
      then
        raise_application_error(-20000, stm_get_batch_message('RLE-10426'));
      end if;
    end if;
  end if;
-- 97 proef
if :new.IBAN is not null
   and not stm_sepa.chk_iban_97 (:new.IBAN)
then
    l_iban := :new.IBAN;
    raise_application_error(-20000, stm_get_batch_message('RLE-10427# ' || l_iban));
end if;
--
rle_wgr_000001.prc_chk_wgr_000001(:new.rle_numrelatie);
/* PGE toegevoegd vanwege wijziging project RLE
automatisch aanmaken RLE_RELATIEROLLEN_IN_ADM
(inclusief bugfix B4093) */
IF :new.rol_codrol IS NOT NULL
THEN
  rle_insert_rie(p_adm_code => :new.adm_code
               , p_rle_numrelatie => :new.rle_numrelatie
               , p_rol_codrol => :new.rol_codrol
               , p_ind_aangemaakt => l_ind_aangemaakt
               , p_error => l_error);
  IF l_error IS NOT NULL
  THEN
     stm_dbproc.raise_error(l_error, NULL, 'rle_bri');
  END IF;
END IF;
/* einde toevoeging PGE */

/* begin toegevoegde code */
IF :new.rol_codrol IS NOT NULL THEN
   rle_rrl_chk_fk(:new.rle_numrelatie,
   :new.rol_codrol);
END IF;
   rle_chk_rle_persdat(
   :new.rle_numrelatie,
   :new.datingang,
   'financiele nummers');
/* Einde toegevoegde code */
-- Controle referentiewaarde

IF :new.coddoelrek IS NOT NULL THEN
  ibm.ibm('chk_refwrd'
    , 'RLE'
    , 'DLR'
    ,:new.coddoelrek);
END IF;
IF :new.indinhown IS NULL THEN
  :new.indinhown:='N';
END IF;
IF :new.indfiat IS NULL THEN
  :new.indfiat:='J';
END IF;
IF  :new.lnd_codland IS NULL
THEN
    :new.lnd_codland := 'NL' ;
END IF;
:new.CODORGANISATIE:='A';
-- id vullen
for v_seq in c_seq
loop
   :new.id := v_seq.new_id;
end loop;
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := SYSDATE;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;

--
-- Extra controles SEPA II
--
if :new.codwijzebetaling is not null
   and :new.codwijzebetaling not in ('AS', 'SB')
then
  raise_application_error(-20000, 'Slechts betaalwijzen AS en SB zijn nog toegestaan. Een van deze is verplicht.');
end if;
--
--
/* Publicatie voor synchronisatie */
BEGIN
   IBM.PUBLISH_EVENT
      ( 'MUT RLE FINANCIEEL NRS'
      , 'I'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.RLE_NUMRELATIE
      , NULL, :new.NUMREKENING
      , NULL, :new.LND_CODLAND
      , NULL, :new.DWE_WRDDOM_SRTREK
      , NULL, to_char(:new.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.INDINHOWN
      , NULL, to_char(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.INDFIAT
      , NULL, :new.ROL_CODROL
      , NULL, :new.CODORGANISATIE
      , NULL, :new.TENAAMSTELLING
      , NULL, :new.CODDOELREK
      , NULL, :new.CODWIJZEBETALING
      , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.CREATIE_DOOR
      , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
      , NULL, :new.MUTATIE_DOOR
      , NULL, :new.ID
      , null, :new.iban
      , null, :new.adm_code

      );
END;
END;
/