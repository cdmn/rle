CREATE OR REPLACE TRIGGER comp_rle.RLE_RRL_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_RELATIE_ROLLEN
 FOR EACH ROW

BEGIN
BEGIN
if :new.codorganisatie <>
     :old.codorganisatie
  or ((:new.codpresentatie <>
       :old.codpresentatie)
      or
      (:new.codpresentatie is not null
       and
       :old.codpresentatie is null)
      or
      (:new.codpresentatie is null
       and
       :old.codpresentatie is not null)
     )
  or ((:new.datschoning <>
       :old.datschoning)
      or
      (:new.datschoning is not null
       and
       :old.datschoning is null)
      or
      (:new.datschoning is null
       and
       :old.datschoning is not null)
     )
  then
  /* Begin toegevoegde code  */
    if  ((:new.datschoning <>
       :old.datschoning)
      or
      (:new.datschoning is not null
        and  :old.datschoning is null)) then
        rle_rrl_chk_datschon (:old.rle_numrelatie
         ,:old.rol_codrol
         ,:new.datschoning);
     end if;
end if;
END;
      /* Einde toegevoegde code  */
BEGIN
    /* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END;
  /* Begin toegevoegde code: PWA t.b.v. Chg-18331  */
  /* SAS70 RBT : geen onderscheid in rollen maken bij publicatie */
  /* Publicatie voor synchronisatie */
BEGIN
IF  :new.rol_codrol <> :old.rol_codrol
THEN
    IBM.PUBLISH_EVENT
      ( 'MUT RLE ROLLEN'
      , 'U'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.RLE_NUMRELATIE, :new.RLE_NUMRELATIE
      , :old.ROL_CODROL, 	   :new.ROL_CODROL
      , :old.CODORGANISATIE, :new.CODORGANISATIE
      , :old.CODPRESENTATIE, :new.CODPRESENTATIE
      , to_char(:old.DATSCHONING, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DATSCHONING, 'dd-mm-yyyy hh24:mi:ss')
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.CREATIE_DOOR,   :new.CREATIE_DOOR
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.MUTATIE_DOOR,   :new.MUTATIE_DOOR
      );
END IF;
END;
  /* Einde toegevoegde code: PWA t.b.v. Chg-18331*/
END RLE_RRL_BRU;
/