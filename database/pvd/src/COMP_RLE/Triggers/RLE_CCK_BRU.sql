CREATE OR REPLACE TRIGGER comp_rle.RLE_CCK_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_COMMUNICATIE_CAT_KANALEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
   :new.DWE_WRDDOM_SRTCOM := upper(:new.DWE_WRDDOM_SRTCOM);
   :new.datum_begin := trunc(nvl(:new.datum_begin,sysdate));
   :new.datum_einde := trunc(:new.datum_einde);
   RFE_CHK_REFWRD('SC', :new.DWE_WRDDOM_SRTCOM);
/* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_CCK_BRU;
/