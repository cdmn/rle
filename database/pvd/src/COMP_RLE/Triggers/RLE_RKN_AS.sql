CREATE OR REPLACE TRIGGER comp_rle.RLE_RKN_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIE_KOPPELINGEN
DECLARE

  l_stack varchar2(128);
  l_rowid rowid;
  l_job varchar2(1);
  l_event xmltype;

  l_old_id                      rle_relatie_koppelingen.id%type;
  l_old_rkg_id                  rle_relatie_koppelingen.rkg_id%type;
  l_old_numrelatie              rle_relatie_koppelingen.rle_numrelatie%type;
  l_old_numrelatie_voor         rle_relatie_koppelingen.rle_numrelatie_voor%type;
  l_old_dat_begin               rle_relatie_koppelingen.dat_begin%type;
  l_old_dat_einde               rle_relatie_koppelingen.dat_einde%type;
  --
  l_old_code_dat_begin_fictief  rle_relatie_koppelingen.code_dat_begin_fictief%type;
  l_old_code_dat_einde_fictief  rle_relatie_koppelingen.code_dat_einde_fictief%type;
  --
  l_event_title          varchar2(30); -- OKR 11-12-2016 tbv Personen Event Store
  --
  l_result               boolean;
  l_omgekeerde_code      rle_rol_in_koppelingen.code_rol_in_koppeling%type;

  /* Cursor om een RLE_RELATIE_KOPPELING op te halen met de
     desbetreffende RLE_ROL_IN_KOPPELING
  */
  cursor c_rkn_000(b_rowid  in  rowid)
  is
  select rkn.id
  ,      rkn.rle_numrelatie
  ,      rkn.rle_numrelatie_voor
  ,      rkn.dat_begin
  ,      rkn.code_dat_begin_fictief
  ,      rkn.dat_einde
  ,      rkn.code_dat_einde_fictief
  ,      rkn.rkg_id
  ,      rkg.code_rol_in_koppeling
  ,      rkg.rol_codrol_beperkt_tot
  ,      rkg.rol_codrol_met
  from   rle_relatie_koppelingen rkn
  inner  join rle_rol_in_koppelingen rkg on rkg.id = rkn.rkg_id
  where  rkn.rowid = b_rowid
  ;
  r_rkn_000  c_rkn_000%rowtype;


  /* Cursor om dubbelen te voorkomen.
     Zoekt alle relatiekoppelingen met dezelfde rle_numrelatie
     en dezelfde code_rol_in_koppeling maar met een ander id.
  */
  cursor c_rkn_001( b_rkn_id in number
                  , b_numrelatie in number
                  , b_code_rol_in_koppeling in varchar2
                  )
  is
  select rkn.id
  ,      rkn.rle_numrelatie
  ,      rkn.rle_numrelatie_voor
  ,      rkn.dat_begin
  ,      rkn.dat_einde
  ,      rkn.rkg_id
  ,      rkg.code_rol_in_koppeling
  ,      rkg.rol_codrol_beperkt_tot
  ,      rkg.rol_codrol_met
  from   rle_relatie_koppelingen  rkn
  inner  join rle_rol_in_koppelingen  rkg on rkg.id = rkn.rkg_id
  where  rkn.id != b_rkn_id
  and    rkn.rle_numrelatie = b_numrelatie
  and    rkg.code_rol_in_koppeling = b_code_rol_in_koppeling
  ;
  r_rkn_001  c_rkn_001%rowtype;


  /* Cursor om het "omgekeerde" record te zoeken.
  */
  cursor c_rkn_002( b_numrelatie in number
                   , b_numrelatie_voor in number
                   , b_dat_begin in date
                   , b_rkg_id in number
                   )
  is
  select rkn.id
  ,      rkn.dat_begin
  ,      rkn.dat_einde
  from   rle_relatie_koppelingen rkn
  inner  join rle_rol_in_koppelingen rkg on rkg.id = rkn.rkg_id
  where  rkn.rle_numrelatie      = b_numrelatie
  and    rkn.rle_numrelatie_voor = b_numrelatie_voor
  and    rkn.dat_begin           = b_dat_begin
  and    rkn.rkg_id              = b_rkg_id
  ;
  r_rkn_002  c_rkn_002%rowtype ;

  /* Cursor om een relatie op te halen.
  */
  cursor c_rle_001(b_numrelatie in number)
  is
  select rle.datgeboorte
  ,      rle.datoverlijden
  from   rle_relaties rle
  where  rle.numrelatie = b_numrelatie
  ;
  r_rle_001a  c_rle_001%rowtype;
  r_rle_001b  c_rle_001%rowtype;

  /* Cursor om externe codes op te halen.
  */
  cursor c_rec_001( b_numrelatie in number
                  , b_rol_codrol in varchar2
                  , b_dwe_wrddom_extsys in varchar2
                   )
  is
  select rec.extern_relatie
  from   rle_relatie_externe_codes rec
  where  rec.rle_numrelatie    = b_numrelatie
  and    rec.dwe_wrddom_extsys = b_dwe_wrddom_extsys
  and    rec.rol_codrol        = b_rol_codrol
  ;
  r_rec_001a  c_rec_001%rowtype;
  r_rec_001b  c_rec_001%rowtype;

  /* Cursor om een rol in koppeling op te halen.
  */
  cursor c_rkg_001(b_code_rol_in_koppeling in varchar2)
  is
  select rkg.id
  from   rle_rol_in_koppelingen rkg
  where  rkg.code_rol_in_koppeling = b_code_rol_in_koppeling
  ;
  r_rkg_001  c_rkg_001%rowtype;

  /* Cursor om een rol in koppeling op te halen.
  */
  cursor c_rkg_002(b_rkg_id in number)
  is
  select rkg.code_rol_in_koppeling
  from   rle_rol_in_koppelingen rkg
  where  rkg.id = b_rkg_id
  ;
  r_rkg_002  c_rkg_002%rowtype;
begin
  stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;

  while l_rowid is not null
  loop
    /* Ophalen standaard variabelen */
    l_job := stm_get_word( l_stack ) ;

    /* Ophalen variabelen update en delete statement (voor insert niet nodig) */
    if    l_job in ('U', 'D')
    then
      l_old_id := stm_get_word(p_stack => l_stack);
      l_old_rkg_id := stm_get_word(p_stack => l_stack);
      l_old_numrelatie := stm_get_word(p_stack => l_stack);
      l_old_numrelatie_voor := stm_get_word(p_stack => l_stack);
      l_old_dat_begin := to_date(stm_get_word(p_stack => l_stack), 'dd-mm-yyyy hh24:mi:ss');
      l_old_dat_einde := to_date(stm_get_word(p_stack => l_stack), 'dd-mm-yyyy hh24:mi:ss');
      l_old_code_dat_begin_fictief := stm_get_word(p_stack => l_stack);
      l_old_code_dat_einde_fictief := stm_get_word(p_stack => l_stack);
    end if;

    /* Opzoeken huidig record met ROWID */
    if l_job in ('I', 'U')
    then
      open  c_rkn_000 (b_rowid => l_rowid);
      fetch c_rkn_000 into r_rkn_000;
      close c_rkn_000;
    end if;


    /* Bevinding 260 + 392
       Het moet niet mogelijk zijn een koppeling met jezelf aan
       te gaan.
    */
    if    l_job in ('I', 'U')
      and r_rkn_000.rle_numrelatie = r_rkn_000.rle_numrelatie_voor
    then
      stm_dbproc.raise_error('RLE-10327') ;
    end if;


    /* BRTPLxxxx
       Een relatiekoppeling mag niet eindigen voordat hij begonnen is.
    */
    if    l_job in ('I', 'U')
      and r_rkn_000.dat_einde is not null
    then
      if r_rkn_000.dat_begin > r_rkn_000.dat_einde
      then
        stm_dbproc.raise_error('RLE-10328');
      end if;
    end if;


    /* BRENT0008
       Voorkomen overlap van Huwelijk, Geregistreerd Partnerschap of Samenlevingscontract.
       Uitleg: Een relatie mag slechts een geldige relatiekoppeling
               met H, G of S hebben op een bepaald moment.
       Nota: Er hoeft alleen op NUMRELATIE te worden gecontroleerd,
             Ongerichte relatiekoppelingen worden ook dubbel geregistreerd.
    */
    if  (   l_job = 'I'
         or (    l_job = 'U'
             and (   r_rkn_000.dat_begin <> l_old_dat_begin
                  or nvl(r_rkn_000.dat_einde, to_date('01-01-4000','dd-mm-yyyy')) > nvl(l_old_dat_einde,to_date('01-01-4000','dd-mm-yyyy'))
                  or r_rkn_000.rkg_id <> l_old_rkg_id    -- rol inkoppeling gewijizgd hier via rkg_id
                 )
            )
        )
      and r_rkn_000.code_rol_in_koppeling in ( 'S', 'H', 'G' )
    then
      begin
        for r_rkn_001 in c_rkn_001( b_rkn_id => r_rkn_000.id
                                  , b_numrelatie => r_rkn_000.rle_numrelatie
                                  , b_code_rol_in_koppeling => 'S'
                                  )
        loop
          stm_algm_check.algm_datovlp( p_datingang1 => r_rkn_000.dat_begin
                                     , p_dateinde1 => r_rkn_000.dat_einde
                                     , p_datingang2 => r_rkn_001.dat_begin
                                     , p_dateinde2 => r_rkn_001.dat_einde
                                     ) ;
        end loop ;
        for r_rkn_001 in c_rkn_001( b_rkn_id => r_rkn_000.id
                                  , b_numrelatie => r_rkn_000.rle_numrelatie
                                  , b_code_rol_in_koppeling => 'H'
                                  )
        loop
          stm_algm_check.algm_datovlp( p_datingang1 => r_rkn_000.dat_begin
                                     , p_dateinde1 => r_rkn_000.dat_einde
                                     , p_datingang2 => r_rkn_001.dat_begin
                                     , p_dateinde2 =>r_rkn_001.dat_einde
                                     ) ;
        end loop ;
        for r_rkn_001 in c_rkn_001( b_rkn_id => r_rkn_000.id
                                  , b_numrelatie => r_rkn_000.rle_numrelatie
                                  , b_code_rol_in_koppeling => 'G'
                                  )
        loop
          stm_algm_check.algm_datovlp( p_datingang1 => r_rkn_000.dat_begin
                                     , p_dateinde1 => r_rkn_000.dat_einde
                                     , p_datingang2 => r_rkn_001.dat_begin
                                     , p_dateinde2 => r_rkn_001.dat_einde
                                     ) ;
        end loop ;
      exception
        when others
        then
          /* Andere foutmelding geven */
          stm_dbproc.raise_error('RLE-10285');
      end ;
    end if ;


    /* BRENT0009
       Voorkomen dubbel geregistreerde relatiekoppelingen.
       Uitleg: Twee relaties mogen slechts één relatiekoppeling met
               één bepaalde rol in koppeling hebben op een moment.

       Nota: Er hoeft niet omgekeerd te worden gecontroleerd op koppelingen
             omdat relatiekoppelingen dubbel worden opgeslagen.
    */
    if l_job in ( 'I', 'U' )
    then
      begin
        for r_rkn_001 in c_rkn_001( b_rkn_id => r_rkn_000.id
                                  , b_numrelatie => r_rkn_000.rle_numrelatie
                                  , b_code_rol_in_koppeling => r_rkn_000.code_rol_in_koppeling
                                  )
        loop
          if r_rkn_000.rle_numrelatie_voor = r_rkn_001.rle_numrelatie_voor
          then
            stm_algm_check.algm_datovlp( p_datingang1 => r_rkn_000.dat_begin
                                       , p_dateinde1 => r_rkn_000.dat_einde
                                       , p_datingang2 => r_rkn_001.dat_begin
                                       , p_dateinde2 => r_rkn_001.dat_einde
                                       ) ;
          end if;
        end loop;
      exception
        when others
        then
          /* Andere foutmelding geven */
          stm_dbproc.raise_error('RLE-10320');
      end ;
    end if;


    /* BRENT0010
       Waarschuwen dat een relatie van het type kind eigenlijk niet meer
       dan twee ouder kan hebben.

       Nota: Een waarschuwing kan alleen aan de client-side worden gegeven.
             Deze controle kan dus niet worden gebouwd op de database.
             Het feit dat het slechts een waarschuwing betreft betekend dus
             automatisch dat een kind WEL meer dan twee ouders kan hebben.
    */
    null;


    /* BRATT0016
       Een relatiekoppeling mag niet beginnen in de toekomst
    */
    if l_job in ('I', 'U')
    then
      if r_rkn_000.dat_begin > sysdate
      then
        stm_dbproc.raise_error('RLE-10288');
      end if ;
    end if;



    /* BRATT0017
       Een relatiekoppeling mag niet eindigen in de toekomst
    */
    if l_job in ('I', 'U')
    then
      if r_rkn_000.dat_einde > sysdate
      then
        stm_dbproc.raise_error('RLE-10289');
      end if;
    end if;

    /* Ophalen relatiegegevens voor business rules:
       BRIER0017
       BRIER0018
       BRIER0019
       BRIER0021
       BRIER0026
    */
    if l_job in ('I', 'U')
    then
      if r_rkn_000.rol_codrol_beperkt_tot = 'PR'
      then
        open  c_rle_001(b_numrelatie => r_rkn_000.rle_numrelatie);
        fetch c_rle_001 into r_rle_001a;
        close c_rle_001;
      end if ;

      if r_rkn_000.rol_codrol_met = 'PR'
      then
        open c_rle_001(b_numrelatie => r_rkn_000.rle_numrelatie_voor);
        fetch c_rle_001 into r_rle_001b;
        close c_rle_001;
      end if ;
    end if ;


    /* BRIER0017
       Een relatiekoppeling van het type 'Huwelijk', 'Geregistreerd
       partnerschap' of 'Samenlevingcontract' mag niet eindigen na
       de overlijdensdatum van een van de twee relaties.
    */
    if    l_job in ('I', 'U')
      and r_rkn_000.code_rol_in_koppeling in ('S', 'H', 'G')
      and r_rkn_000.dat_einde is not null
    then
      if   r_rkn_000.dat_einde > r_rle_001a.datoverlijden
        or r_rkn_000.dat_einde > r_rle_001b.datoverlijden
      then
        stm_dbproc.raise_error('RLE-10321');
      end if ;
    end if ;

    /* BRIER0018
       Een relatiekoppeling mag niet beginnen voor de geboortedatum
       van een van de twee relaties.

       Nota: Controle als rol_codrol_beperkt_tot of rol_codrol_met
             de rol 'PR' bevat.
    */
    if   l_job = 'I'
      or (    l_job = 'U'
         and r_rkn_000.dat_begin <> l_old_dat_begin
         ) -- xjb, 15-09-2014
    then
      if r_rkn_000.rol_codrol_beperkt_tot = 'PR'
      then
        if r_rkn_000.dat_begin < r_rle_001a.datgeboorte
        then
          stm_dbproc.raise_error('RLE-10322');
        end if ;
      end if ;

      if r_rkn_000.rol_codrol_met = 'PR'
      then
        if r_rkn_000.dat_begin < r_rle_001b.datgeboorte
        then
          stm_dbproc.raise_error('RLE-10322');
        end if ;
      end if ;
    end if ;

    /* BRIER0019
       Een relatiekoppeling van het type 'Huwelijk', 'Geregistreerd
       partnerschap' of 'Samenlevingcontract' mag niet beginnen na
       de overlijdensdatums van een van de twee relaties.
    */
    if    l_job in ('I', 'U')
      and r_rkn_000.code_rol_in_koppeling in ('S', 'H', 'G')
    then
      if   r_rkn_000.dat_begin > r_rle_001a.datoverlijden
        or r_rkn_000.dat_begin > r_rle_001b.datoverlijden
      then
        stm_dbproc.raise_error('RLE-10323');
      end if ;
    end if ;


    /* BRIER0021
       Een relatiekoppeling van het type 'Kind' mag niet meer dan 9
       maanden beginnen na de overlijdensdatum van de ouder.
    */
    if    l_job in ('I', 'U')
      and r_rkn_000.code_rol_in_koppeling = 'K'
    then
      if r_rkn_000.dat_begin > add_months(r_rle_001b.datoverlijden, 9)
      then
        stm_dbproc.raise_error('RLE-10286') ;
      end if;
    end if;

      /* BRIER0026
         Voorkomen dat een RELATIE met de ROL in KOPPELING 'Kind van' ouder
         is dan een RELATIE met de ROL in KOPPELING 'Ouder van'.
      */ /* */
      if    l_job in ('I', 'U')
        and r_rkn_000.code_rol_in_koppeling = 'K'
      then
        if r_rle_001a.datgeboorte < r_rle_001b.datgeboorte
        then
          stm_dbproc.raise_error('RLE-10346');
        end if ;
      end if ;

      /* BRIER0020
         Een relatiekoppeling van het type 'Huwelijk', 'Geregistreerd
         partnerschap' of 'Samenlevingcontract' mag niet verwijderd worden
         als de einddatum is gevuld en minimaal één van de twee relaties
         geen sofinummer en geen a-nummer heeft.
         (Met uitzondering van de batchverwerking)

         Nota: Alleen de rle_numrelatie hoeft gecontroleerd te worden.
               De andere relatie wordt gecontroleerd bij het verwijderen
               van het "omgekeerde" record.
      */
      if    l_job = 'D'
        and r_rkn_000.code_rol_in_koppeling in ('S', 'H', 'G')
        and r_rkn_000.dat_einde is not null
      then
        l_result := false;

        open  c_rec_001( b_numrelatie => r_rkn_000.rle_numrelatie
                       , b_rol_codrol => 'PR'
                       , b_dwe_wrddom_extsys => 'SOFI'
                       );
        fetch c_rec_001 into r_rec_001a;
        close c_rec_001;

        open  c_rec_001( b_numrelatie => r_rkn_000.rle_numrelatie
                       , b_rol_codrol => 'PR'
                       , b_dwe_wrddom_extsys => 'GBA'
                       );
        fetch c_rec_001 into r_rec_001b;
        close c_rec_001;

        if    r_rec_001a.extern_relatie is null
          and r_rec_001b.extern_relatie is null
        then
          stm_dbproc.raise_error('RLE-10291');
        end if ;
      end if ;

      /* BRIER0028
         Een persoon mag niet een koppeling van het type "Huwelijk",
         "Geregistreerd partnerschap" of "Samenlevingscontact" hebben
         met zijn of haar kinderen of ouders.
      */ /* */
      if    l_job in ('I', 'U')
        and r_rkn_000.code_rol_in_koppeling in ('S', 'H', 'G')
      then
        for r_rkn_001 in c_rkn_001( b_rkn_id => r_rkn_000.id
                                  , b_numrelatie => r_rkn_000.rle_numrelatie
                                  , b_code_rol_in_koppeling => 'O'
                                  )
        loop
          if r_rkn_001.rle_numrelatie_voor = r_rkn_000.rle_numrelatie_voor
          then
            stm_dbproc.raise_error('RLE-10351 #1persoon#2kind') ;
          end if ;
        end loop ;

        for r_rkn_001 in c_rkn_001( b_rkn_id => r_rkn_000.id
                                  , b_numrelatie => r_rkn_000.rle_numrelatie
                                  , b_code_rol_in_koppeling => 'K'
                                  )
        loop
          if r_rkn_001.rle_numrelatie_voor = r_rkn_000.rle_numrelatie_voor
          then
            stm_dbproc.raise_error( 'RLE-10351 #1persoon#2ouder' ) ;
          end if ;
        end loop ;
      end if ;

      /* BRTPL0025
         Voorkomen dat de einddatum RELATIE KOPPELING is gevuld als de
         ROL in KOPPELING 'Kind van' of 'Ouder van' is.
      */ /* */
      if    l_job in ('I', 'U')
        and r_rkn_000.code_rol_in_koppeling in ('O', 'K')
      then
        if r_rkn_000.dat_einde is not null
        then
          stm_dbproc.raise_error('RLE-10352');
        end if ;
      end if ;


      /* BRIER0025
       Voorkomen overlap van bij een RELATIE zijnde WERKGEVER op een
       bepaald moment met betrekking tot de RELATIE KOPPELING met
       administratiekantoor, curator en bewindvoerder.

       Nota: Er hoeft alleen op NUMRELATIE te worden gecontroleerd en
             slechts één van de twee koppelingen.
             Relatiekoppelingen worden dubbel geregistreerd en de datums
             worden bijgewerkt.
      */
      if    l_job in ('I', 'U')
        and r_rkn_000.code_rol_in_koppeling in ('VA', 'WGCR', 'WGBW')
      then
        begin
          for r_rkn_001 in c_rkn_001( b_rkn_id => r_rkn_000.id
                                    , b_numrelatie => r_rkn_000.rle_numrelatie
                                    , b_code_rol_in_koppeling => r_rkn_000.code_rol_in_koppeling
                                    )
          loop
            stm_algm_check.algm_datovlp( p_datingang1 => r_rkn_000.dat_begin
                                       , p_dateinde1 => r_rkn_000.dat_einde
                                       , p_datingang2 => r_rkn_001.dat_begin
                                       , p_dateinde2 => r_rkn_001.dat_einde
                                       ) ;
         end loop ;
       exception
         when others
         then
           /* Andere foutmelding geven */
           stm_dbproc.raise_error('RLE-10355');
       end ;
     end if ;


    /* CHANGE-RKN-U: U_GBA_012
       Muteren verwantschappen
    */
    if l_job = 'I'
    then
      if r_rkn_000.code_rol_in_koppeling in ('K', 'O', 'G', 'H', 'S')
      then
        rle_gba_toevoegen_verwant(pin_rkn_id => r_rkn_000.id);
      end if ;
    end if ;

    /* CHANGE-RKN-U: U_GBA_014
       Muteren verwantschappen
    */
    if l_job = 'U'
    then
      if r_rkn_000.code_rol_in_koppeling in ('K', 'O', 'G', 'H', 'S')
      then
        rle_gba_muteren_verwant(pin_rkn_id => r_rkn_000.id);
      end if ;
    end if ;

    /* Bepalen van "omgekeerde" rol in koppeling voor business rules:
       BRCEV0016
       BRUPDxxxx
       BRDEL0002

       Dit gaat fout wanneer de type koppeling wordt gewijzigd.
       Dat gaan we dus ook niet toestaan.
    */ /* */
    if l_job = 'U'
    then
      if r_rkn_000.rkg_id != l_old_rkg_id
      then
        stm_dbproc.raise_error('RLE-10324');
      end if ;
    end if ;

    if l_job in ('I', 'U')
    then
      rle_get_rkg_omgekeerd( piv_code_rol_in_koppeling => r_rkn_000.code_rol_in_koppeling
                           , pov_code_rol_in_koppeling => l_omgekeerde_code
                           );

    elsif l_job = 'D'
    then
      open c_rkg_002(b_rkg_id => l_old_rkg_id);
      fetch c_rkg_002 into r_rkg_002;
      close c_rkg_002;

      rle_get_rkg_omgekeerd( piv_code_rol_in_koppeling => r_rkg_002.code_rol_in_koppeling
                           , pov_code_rol_in_koppeling => l_omgekeerde_code
                           );
    end if;

    open c_rkg_001(b_code_rol_in_koppeling => l_omgekeerde_code);
    fetch c_rkg_001 into r_rkg_001 ;
    close c_rkg_001 ;

    if r_rkg_001.id is null
    then
      /* De omgekeerde rol kan niet worden gevonden!!!! */
      stm_dbproc.raise_error('RLE-10235');
    end if ;

    /* BRCEV0016
       Aanmaken "omgekeerde" koppeling
    */
    if l_job = 'I'
    then
      /* Bepalen of de "omgekeerde" koppeling reeds bestaat */
      open c_rkn_002( b_numrelatie => r_rkn_000.rle_numrelatie_voor
                    , b_numrelatie_voor => r_rkn_000.rle_numrelatie
                    , b_dat_begin => r_rkn_000.dat_begin
                    , b_rkg_id => r_rkg_001.id
                    );
      fetch c_rkn_002 into r_rkn_002;

      if c_rkn_002%found
      then
        close c_rkn_002;
      else
        close c_rkn_002;

        insert into rle_relatie_koppelingen
          ( id
          , rkg_id
          , rle_numrelatie
          , rle_numrelatie_voor
          , dat_begin
          , code_dat_begin_fictief
          , dat_einde
          , code_dat_einde_fictief
          )
        values
          ( rle_rkn_seq1.nextval
          , r_rkg_001.id
          , r_rkn_000.rle_numrelatie_voor
          , r_rkn_000.rle_numrelatie
          , r_rkn_000.dat_begin
          , r_rkn_000.code_dat_begin_fictief
          , r_rkn_000.dat_einde
          , r_rkn_000.code_dat_einde_fictief
          );
      end if ;
      /*  BRCEV0016
          Aanmaken Publicatie CRE VERWANTSCHAP
      */
      rle_aq_rkn_rle_rie( p_rle_numrelatie_van => r_rkn_000.rle_numrelatie
                        , p_rle_numrelatie_voor => r_rkn_000.rle_numrelatie_voor
                        , p_code_rol_in_kop => r_rkn_000.code_rol_in_koppeling
                        , p_datum_event => trunc(sysdate)
                        );
    end if ;

    /* BRUPDxxxx
       Wijzigen "omgekeerde" koppeling
    */
    if l_job = 'U'
    then
      /* Zoek de "omgekeerde" koppeling */
      open c_rkn_002( b_numrelatie => l_old_numrelatie_voor
                    , b_numrelatie_voor => l_old_numrelatie
                    , b_dat_begin => l_old_dat_begin
                    , b_rkg_id => r_rkg_001.id
                    ) ;
      fetch c_rkn_002 into r_rkn_002;
      if c_rkn_002%found
      then
        close c_rkn_002;
        /* Record alleen updaten als de gegevens anders zijn.
           Anders krijgen we een rondzingend effect waar Oracle een internal error (ORA-00600) op geeft.
        */
        if    r_rkn_000.dat_begin = r_rkn_002.dat_begin
          and (  r_rkn_000.dat_einde = r_rkn_002.dat_einde
              or (    r_rkn_000.dat_einde is null
                  and r_rkn_002.dat_einde is null
                 )
              )
        then
          null;
        else
          update rle_relatie_koppelingen  rkn
          set    rkn.dat_begin = r_rkn_000.dat_begin
          ,      rkn.dat_einde = r_rkn_000.dat_einde
          where  rkn.id = r_rkn_002.id
          ;
        end if ;
      else
        close c_rkn_002 ;
        stm_dbproc.raise_error('RLE-10326');
      end if ;
    end if ;


    /* BRDEL0002
       Verwijderen "omgekeerde" koppeling
    */
    if l_job = 'D'
    then
      delete rle_relatie_koppelingen  rkn
      where  rkn.rle_numrelatie = l_old_numrelatie_voor
      and    rkn.rle_numrelatie_voor = l_old_numrelatie
      and    rkn.dat_begin = l_old_dat_begin
      and    rkn.rkg_id = r_rkg_001.id
      ;
    end if;

    /* KVR, 29/6/09, project ANW-NOS:
       BR_CEV0026 Bij een opvoer of beeindiging van een huwelijk moet het event rle.MutKoppeling gepubliceerd worden
    */
    if    r_rkn_000.code_rol_in_koppeling = 'H'
      and (  l_job = 'I'
          or (   l_job = 'U'
             and l_old_dat_einde is null
             and r_rkn_000.dat_einde is not null
             )
          )
    then
      stm_util.debug('Event rle.MutKoppeling parameters');
      stm_util.debug('rle_numrelatie : '||r_rkn_000.rle_numrelatie);
      stm_util.debug('dat_begin      : '||r_rkn_000.dat_begin);
      stm_util.debug('dat_einde      : '||r_rkn_000.dat_einde);

      l_event      := eventhandler.create_event ('rle.MutKoppeling');

      eventhandler.add_num_value(l_event, 'RLE_NUMRELATIE', r_rkn_000.rle_numrelatie);
      eventhandler.add_date_value(l_event, 'DAT_BEGIN', r_rkn_000.dat_begin);
      eventhandler.add_date_value(l_event, 'DAT_EINDE', r_rkn_000.dat_einde);
      eventhandler.publish_asynch_event(l_event);
    end if;
      /* JPG, 28-10-2015, project NR WP8 ANW:
         BR_CEV0031 Bij een opvoer van een nieuw partnerschap moet het event rle.MutKoppelingAnw gepubliceerd worden
      */
      if r_rkn_000.code_rol_in_koppeling in ('H', 'S')
         and
         (  ( l_job = 'I'
              and
              r_rkn_000.dat_einde is null
            )
            or
            ( l_job = 'U'
              and
              l_old_dat_einde is not null
              and
              r_rkn_000.dat_einde is null
            )
         )
      then

        l_event      := eventhandler.create_event ( 'rle.MutKoppelingAnw' );

        eventhandler.add_num_value  ( l_event , 'RLE_NUMRELATIE' , r_rkn_000.rle_numrelatie);
        eventhandler.add_date_value ( l_event , 'DAT_BEGIN'      , r_rkn_000.dat_begin);
        eventhandler.add_date_value ( l_event , 'DAT_EINDE'      , r_rkn_000.dat_einde);
        eventhandler.add_value      ( l_event , 'SOORT'          , 'NIEUW');
        eventhandler.add_value      ( l_event , 'CODE_ROL'       , r_rkn_000.code_rol_in_koppeling);
        eventhandler.publish_asynch_event ( l_event );
      end if;
      /* JPG, 28-10-2015, project NR WP8 ANW:
         BR_CEV0032 Bij een einde van een partnerschap moet het event rle.MutKoppelingAnw gepubliceerd worden
      */
--      stm_util.debug('lr_rkn_000.code_rol_in_koppeling: '||lr_rkn_000.code_rol_in_koppeling);
--      stm_util.debug('l_job: '||l_job);
--      stm_util.debug('ld_old_dat_einde: '||ld_old_dat_einde);
--      stm_util.debug('lr_rkn_000.rle_numrelatie: '||lr_rkn_000.rle_numrelatie);
--      stm_util.debug('lr_rkn_000.dat_einde: '||lr_rkn_000.dat_einde);
--      stm_util.debug('lr_rle_001a.datoverlijden: '||lr_rle_001a.datoverlijden);
--      stm_util.debug('lr_rle_001b.datoverlijden: '||lr_rle_001b.datoverlijden);
      if r_rkn_000.code_rol_in_koppeling in ('H', 'S')
         and
         (  ( l_job = 'I'
              and
              r_rkn_000.dat_einde is not null
              and
              ( r_rkn_000.dat_einde != nvl(r_rle_001a.datoverlijden, r_rkn_000.dat_einde + 1) and r_rkn_000.dat_einde != nvl(r_rle_001b.datoverlijden, r_rkn_000.dat_einde + 1) )
            )
           or
           ( l_job = 'U'
              and
              l_old_dat_einde is null
              and
              r_rkn_000.dat_einde is not null
              and
              ( r_rkn_000.dat_einde != nvl(r_rle_001a.datoverlijden, r_rkn_000.dat_einde + 1) and r_rkn_000.dat_einde != nvl(r_rle_001b.datoverlijden, r_rkn_000.dat_einde + 1) )
            )
         )
      then
        l_event      := eventhandler.create_event ( 'rle.MutKoppelingAnw' );

        eventhandler.add_num_value  ( l_event , 'RLE_NUMRELATIE' , r_rkn_000.rle_numrelatie);
        eventhandler.add_date_value ( l_event , 'DAT_BEGIN'      , r_rkn_000.dat_begin);
        eventhandler.add_date_value ( l_event , 'DAT_EINDE'      , r_rkn_000.dat_einde);
        eventhandler.add_value      ( l_event , 'SOORT'          , 'EINDE');
        eventhandler.add_value      ( l_event , 'CODE_ROL'       , r_rkn_000.code_rol_in_koppeling);
        eventhandler.publish_asynch_event ( l_event );
      end if;
      --
      -- OKR 11-12-2016 Mutaties ook loggen naar de Event Store (Auditlog Persoonsgegevens)
      if -- UPDATES waarbij de functionele sleutel wijzigt worden naar de Event Store
            -- gecommuniceerd als een DELETE en een INSERT
         l_job = 'U' and
         (
         stm_dbproc.value_modified(l_old_dat_begin, r_rkn_000.dat_begin)
         or
         stm_dbproc.value_modified(l_old_numrelatie, r_rkn_000.rle_numrelatie)
         or
         stm_dbproc.value_modified(l_old_numrelatie_voor, r_rkn_000.rle_numrelatie_voor)
         or
         stm_dbproc.value_modified(l_old_rkg_id, r_rkn_000.rkg_id)
         )
      then
        -- DELETE
        appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_rkn(p_event_title => 'Partnerschap verwijderd'
                                                                       ,p_relatienr_van => l_old_numrelatie
                                                                       ,p_relatienr_voor => l_old_numrelatie_voor
                                                                       ,p_rol_in_koppeling => l_old_rkg_id
                                                                       ,p_begindatum => l_old_dat_begin
                                                                       ,p_begindat_code_fictief => l_old_code_dat_begin_fictief
                                                                       ,p_einddatum => l_old_dat_einde
                                                                       ,p_einddat_code_fictief => l_old_code_dat_einde_fictief
                                                                       ,p_dml_actie => 'D');
        -- INSERT
        appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_rkn(p_event_title => 'Partnerschap geregistreerd'
                                                                       ,p_relatienr_van => r_rkn_000.rle_numrelatie
                                                                       ,p_relatienr_voor => r_rkn_000.rle_numrelatie_voor
                                                                       ,p_rol_in_koppeling => r_rkn_000.rkg_id
                                                                       ,p_begindatum => r_rkn_000.dat_begin
                                                                       ,p_begindat_code_fictief => r_rkn_000.code_dat_begin_fictief
                                                                       ,p_einddatum => r_rkn_000.dat_einde
                                                                       ,p_einddat_code_fictief => r_rkn_000.code_dat_einde_fictief
                                                                       ,p_dml_actie => 'I');
      elsif -- INSERTS, of UPDATES waarbij functionele sleutel niet wijzigt (dat zal dan altijd een scheiding zijn)
         l_job = 'I' or ( l_job = 'U' and stm_dbproc.value_modified(l_old_dat_einde, r_rkn_000.dat_einde) )
      then
        --
        if l_job = 'I'
          then l_event_title := 'Partnerschap geregistreerd';
        elsif r_rkn_000.dat_einde is not null
          then l_event_title := 'Partnerschap beëindigd';
        else
          l_event_title := 'Partnerschap gewijzigd';
        end if;
        --
        appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_rkn(p_event_title => l_event_title
                                                                       ,p_relatienr_van => r_rkn_000.rle_numrelatie
                                                                       ,p_relatienr_voor => r_rkn_000.rle_numrelatie_voor
                                                                       ,p_rol_in_koppeling => r_rkn_000.rkg_id
                                                                       ,p_begindatum => r_rkn_000.dat_begin
                                                                       ,p_begindat_code_fictief => r_rkn_000.code_dat_begin_fictief
                                                                       ,p_einddatum => r_rkn_000.dat_einde
                                                                       ,p_einddat_code_fictief => r_rkn_000.code_dat_einde_fictief
                                                                       ,p_dml_actie => l_job);

      elsif l_job = 'D'
      then
        -- DELETE
        appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_rkn(p_event_title => 'Partnerschap verwijderd'
                                                                       ,p_relatienr_van => l_old_numrelatie
                                                                       ,p_relatienr_voor => l_old_numrelatie_voor
                                                                       ,p_rol_in_koppeling => l_old_rkg_id
                                                                       ,p_begindatum => l_old_dat_begin
                                                                       ,p_begindat_code_fictief => l_old_code_dat_begin_fictief
                                                                       ,p_einddatum => l_old_dat_einde
                                                                       ,p_einddat_code_fictief => l_old_code_dat_einde_fictief
                                                                       ,p_dml_actie => 'D');
      end if;
      --
      /* Volgende rij ophalen */
      stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   end loop ;

   stm_algm_muttab.clear_array ;
exception
  when others
  then
    stm_algm_muttab.clear_array ;

    raise;
end rle_rkn_as;
/