CREATE OR REPLACE TRIGGER comp_rle.RLE_FNR_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_FINANCIEEL_NUMMERS
DECLARE


L_STACK VARCHAR2(128);
L_ROWID ROWID;
L_JOB VARCHAR2(1);
L_OLDDATINGANG DATE;
L_OLDDATEINDE DATE;
L_OLDINDINHOWN VARCHAR2(1) := 'N';
L_OLDNUMRELATIE NUMBER;
L_OLDSRTREK VARCHAR2(30);
L_OLDCODROL VARCHAR2(2);
L_OLDNUMREKENING VARCHAR2(34);
L_CODWIJZEBETALING VARCHAR2(2);
/* RLE_FNR_AS */

/* Ophalen financieelnummer op rol en soort */
CURSOR C_FNR_004
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL VARCHAR2
 )
 IS
/* C_FNR_004 */
  select fnr.*, rowid
  from  rle_financieel_nummers fnr
  where  fnr.rle_numrelatie = b_numrelatie
  and  fnr.rol_codrol     = b_codrol
  and ( fnr.codwijzebetaling is null
      or
      (
          fnr.codwijzebetaling    = l_codwijzebetaling
            or fnr.codwijzebetaling = case l_codwijzebetaling
                                     when 'AI'
                                     then
                                        'AS'
                                     when 'AS'
                                     then
                                        'AI'
                                     when 'AG'
                                     then
                                        'SB'
                                     when 'SB'
                                     then
                                        'AG'
                                     else
                                        fnr.codwijzebetaling
                                     end
       ));
--
  CURSOR c_fnr_000(b_rowid ROWID)
  IS
    SELECT *
    FROM rle_financieel_nummers fnr
    WHERE fnr.rowid = b_rowid
    ;
  r_fnr_000 c_fnr_000%rowtype;
BEGIN
  stm_algm_muttab.get_rowid (l_rowid, l_stack);
  WHILE l_rowid IS NOT NULL LOOP
    l_job := stm_get_word(l_stack);
    -- Ophalen gegevens (per rij)

IF l_job <> 'D'
    THEN
      OPEN  c_fnr_000(l_rowid);
      FETCH c_fnr_000 INTO r_fnr_000;
      CLOSE c_fnr_000;
      l_codwijzebetaling := r_fnr_000.codwijzebetaling;
    END IF;
    IF l_job <> 'I'
    THEN
      l_olddatingang   := TO_DATE(stm_get_word(l_stack));
      l_olddateinde    := TO_DATE(stm_get_word(l_stack));
      l_oldindinhown   := stm_get_word (l_stack);
      l_oldnumrelatie  := stm_get_word (l_stack);
    --  l_oldsrtrek      := stm_get_word (l_stack);
      l_oldcodrol      := stm_get_word (l_stack);
      l_oldnumrekening := stm_get_word (l_stack);
 --     l_codwijzebetaling := stm_get_word (l_stack);
    END IF;
    /* Bij toevoegen nieuw voorkomen SB, afsluiten voorliggende nummer. */
    IF l_codwijzebetaling = 'SB'  --l_job = 'I'
    THEN
      UPDATE rle_financieel_nummers fnr
      SET fnr.dateinde  = r_fnr_000.datingang - 1
      ,   fnr.indinhown =  'J'
      WHERE fnr.rle_numrelatie    =  r_fnr_000.rle_numrelatie
  --    AND (fnr.numrekening       <> r_fnr_000.numrekening
  --      OR fnr.IBAN              <> r_fnr_000.IBAN
    --      )
--      AND fnr.dwe_wrddom_srtrek   =  r_fnr_000.dwe_wrddom_srtrek
      AND NVL(fnr.rol_codrol,'@') =  NVL(r_fnr_000.rol_codrol,'@')
  --    AND NVL(coddoelrek,'@')     =  NVL(r_fnr_000.coddoelrek,'@')
 --     AND NVL(codwijzebetaling, '@') = NVL(r_fnr_000.codwijzebetaling, '@')
      AND fnr.datingang           <  r_fnr_000.datingang
      AND ( fnr.codwijzebetaling is null
      or
      (
          fnr.codwijzebetaling    = l_codwijzebetaling
            or fnr.codwijzebetaling = case l_codwijzebetaling
                                     when 'AI'
                                     then
                                        'AS'
                                     when 'AS'
                                     then
                                        'AI'
                                     when 'AG'
                                     then
                                        'SB'
                                     when 'SB'
                                     then
                                        'AG'
                                     else
                                        fnr.codwijzebetaling
                                     end
       )     )
      AND fnr.dateinde            IS NULL;
--      DELETE rle_financieel_nummers fnr
--      WHERE fnr.rle_numrelatie      =  r_fnr_000.rle_numrelatie
--      AND   NVL(fnr.rol_codrol,'@') =  NVL(r_fnr_000.rol_codrol,'@')
--      AND   NVL(coddoelrek,'@')     =  NVL(r_fnr_000.coddoelrek,'@')
--      AND   NVL(codwijzebetaling, '@') = NVL(r_fnr_000.codwijzebetaling, '@')
 --     AND (fnr.numrekening       <> r_fnr_000.numrekening
   --     OR fnr.IBAN              <> r_fnr_000.IBAN
     --     )
  --    AND   fnr.dwe_wrddom_srtrek   =  r_fnr_000.dwe_wrddom_srtrek
  --    AND   fnr.datingang           =  r_fnr_000.datingang
  --    AND   fnr.dateinde            IS NULL;

      -- Als nieuwe rekening G-rekening is, sluit dan voorgaande B-rekening af
      /*ARS 31-05-2002: een bank- en girorekeningnr mogen naast elkaar voorkomen voor
        hetzelfde doel */
      /*
      IF r_fnr_000.dwe_wrddom_srtrek = 'G'
      THEN
        UPDATE rle_financieel_nummers fnr
        SET fnr.dateinde  = r_fnr_000.datingang - 1
        ,   fnr.indinhown =  'J'
        WHERE fnr.rle_numrelatie    =  r_fnr_000.rle_numrelatie
        AND fnr.numrekening         <> r_fnr_000.numrekening
        AND fnr.dwe_wrddom_srtrek   =  'B'
        AND NVL(fnr.rol_codrol,'@') =  NVL(r_fnr_000.rol_codrol,'@')
        AND NVL(coddoelrek,'@')     =  NVL(r_fnr_000.coddoelrek,'@')
        AND fnr.datingang           <  r_fnr_000.datingang
        AND fnr.dateinde            IS NULL;
      END IF;
      */
      -- Als nieuwe rekening een B-rekening voor premie is, sluit dan
      -- voorgaande G-rekening af
      /* ARS 31-05-2002: er wordt nu nog geen gebruik gemaakt van premie en uitkering */
      /*
      IF r_fnr_000.dwe_wrddom_srtrek = 'B'
      AND r_fnr_000.coddoelrek = 'PREMIE'
      THEN
        UPDATE rle_financieel_nummers fnr
        SET fnr.dateinde  = r_fnr_000.datingang - 1
        ,   fnr.indinhown =  'J'
        WHERE fnr.rle_numrelatie    =  r_fnr_000.rle_numrelatie
        AND fnr.numrekening         <> r_fnr_000.numrekening
        AND fnr.dwe_wrddom_srtrek   =  'G'
        AND NVL(fnr.rol_codrol,'@') =  NVL(r_fnr_000.rol_codrol,'@')
        AND NVL(coddoelrek,'@')     =  'PREMIE'
        AND fnr.datingang           <  r_fnr_000.datingang
        AND fnr.dateinde            IS NULL;
      END IF;
      */
      null;
    END IF;
    /* Bij verwijderen */
    /* als een fnum verwijderd wordt waarvan de einddatum naar een broeder
    record overerft was, dan schuift de einddatum van de broeder op naar de
    einddatum van het verwijderde record. */
    IF l_job = 'D'
    THEN
      UPDATE rle_financieel_nummers fnr
      SET fnr.dateinde  = l_olddateinde
      ,   fnr.indinhown = l_oldindinhown
      WHERE fnr.rle_numrelatie    = l_oldnumrelatie
 --     AND fnr.dwe_wrddom_srtrek   = l_oldsrtrek
      AND NVL(fnr.rol_codrol,'@') = NVL(l_oldcodrol,'@')
      AND fnr.dateinde            = l_olddatingang - 1
      AND fnr.indinhown           = 'J';
    END IF;
    /* Overlapcontrole */ -- voor 'broederrecords'
    if l_job <> 'D'
    then
      for r_fnr_004 in c_fnr_004( r_fnr_000.rle_numrelatie
--                                , r_fnr_000.dwe_wrddom_srtrek
                                , r_fnr_000.rol_codrol)
      loop
        if-- r_fnr_004.numrekening <> r_fnr_000.numrekening
      --  and r_fnr_004.datingang <> r_fnr_000.datingang
       -- and
        r_fnr_004.codwijzebetaling = 'SB' and
          r_fnr_004.rowid <> l_rowid
        then
          STM_ALGM_CHECK.ALGM_DATOVLP(r_fnr_000.datingang
                                     , r_fnr_000.dateinde
                                     , r_fnr_004.datingang
                                     , r_fnr_004.dateinde
                                     );
        end if;
      end loop;
      -- Rekening nummer mag geen overlap hebben met gelijk rekeningnummer
      -- AI/AS hoort leidend te zijn.
      for r_fnr_005 in (select numrekening, codwijzebetaling
                        from rle_financieel_nummers fnr
                        where fnr.rle_numrelatie = r_fnr_000.rle_numrelatie
                        and   fnr.numrekening    = r_fnr_000.numrekening
                        and   fnr.datingang     <= nvl(r_fnr_000.dateinde,sysdate)
                        and   nvl(fnr.dateinde,sysdate)  >= r_fnr_000.datingang
                        and   fnr.rowid <> l_rowid
                        )
      loop
        if l_codwijzebetaling = 'AS' then
           raise_application_error(-20000, stm_get_batch_message('Rekeningnummer ' || r_fnr_005.numrekening || ' is reeds bekend bij deze relatie. ' ||
                                                                 'Sluit eerst het andere ' || r_fnr_005.codwijzebetaling || ' record af.'));
        else
           raise_application_error(-20000, stm_get_batch_message('Rekeningnummer ' || r_fnr_005.numrekening || ' is reeds bekend bij deze relatie.'));
        end if;
      end loop;
    end if;
    stm_algm_muttab.get_rowid (l_rowid, l_stack);
  end loop;
  stm_algm_muttab.clear_array;
EXCEPTION
  when no_data_found
  then
    NULL;
  when others
  then
    stm_algm_muttab.clear_array;
    raise;
END;
/