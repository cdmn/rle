CREATE OR REPLACE TRIGGER comp_rle.RLE_RVT_BRU
 BEFORE UPDATE OF REGISTRATIE_DATUM
, VERVAL_KANAAL
, VERVAL_DATUM
, REGISTRATIE_KANAAL
 ON comp_rle.RLE_RELATIE_VERZETTEN
 FOR EACH ROW
begin
  --
  :new.last_updated_by       := user;
  :new.last_update_date      := sysdate;

  if     :old.verval_kanaal is null
     and :new.verval_kanaal is not null
  then
    :new.verval_datum          := sysdate;
  elsif     :old.verval_kanaal is not null
        and :new.verval_kanaal is null
  then
    :new.registratie_datum     := sysdate;
    :new.verval_datum          := null;
  end if;
END RLE_RVT_BRU;
/