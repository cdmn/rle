CREATE OR REPLACE TRIGGER comp_rle.RLE_FNC_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_FINNR_CODA
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
begin
  :new.mutatie_door := user;
  :new.dat_mutatie  := sysdate;
  --
end;
/