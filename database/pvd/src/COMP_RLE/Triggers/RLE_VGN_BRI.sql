CREATE OR REPLACE TRIGGER comp_rle.RLE_VGN_BRI
 BEFORE INSERT
 ON comp_rle.RLE_VERWERKINGS_GEGEVENS
 FOR EACH ROW

BEGIN
/* QMS$DATA_AUDITING */
   :new.DAT_CREATIE := sysdate;
   :new.CREATIE_DOOR := alg_sessie.gebruiker;
   :new.DAT_MUTATIE := sysdate;
   :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_VGN_BRI;
/