CREATE OR REPLACE TRIGGER comp_rle.RLE_RIE_BRD
 BEFORE DELETE OR UPDATE OF ADM_CODE
 ON comp_rle.RLE_RELATIEROLLEN_IN_ADM
 FOR EACH ROW
begin
  rle_chk_lov_ovk ( :old.rle_numrelatie
                   ,:old.adm_code
                   ,:old.rol_codrol
                  );
END RLE_RIE_BRD;
/