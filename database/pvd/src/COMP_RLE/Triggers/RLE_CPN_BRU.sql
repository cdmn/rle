CREATE OR REPLACE TRIGGER comp_rle.RLE_CPN_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_CONTACTPERSONEN
 FOR EACH ROW

BEGIN
   /* Controle code_functie */
   IF :new.code_functie IS NOT NULL
   THEN
      ibm.ibm( 'CHK_REFWRD'
             , 'RLE'
             , 'FTE'
             , :new.code_functie
             ) ;
   END IF ;
   /* Controle code_soort_contactpersoon */
   IF :new.code_soort_contactpersoon IS NOT NULL
   THEN
      ibm.ibm( 'CHK_REFWRD'
             , 'RLE'
             , 'SCN'
             , :new.code_soort_contactpersoon
             ) ;
   END IF ;
   /* QMS$DATA_AUDITING */
   :new.DAT_MUTATIE := sysdate;
   :new.MUTATIE_DOOR := alg_sessie.gebruiker;
   /* QMS$JOURNALLING */
   insert
   into rle_contactpersonen_jn
   (     jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      ras_id
   ,      rle_numrelatie
   ,      code_functie
   ,      code_soort_contactpersoon
   ,      creatie_door
   ,      dat_creatie
   ,      mutatie_door
   ,      dat_mutatie
   )
   values
   (      alg_sessie.gebruiker
   ,      sysdate
   ,      'UPD'
   ,     :old.ras_id
   ,      :old.rle_numrelatie
   ,      :old.code_functie
   ,      :old.code_soort_contactpersoon
   ,      :old.creatie_door
   ,      :old.dat_creatie
   ,      :old.mutatie_door
   ,      :old.dat_mutatie
   );
   /* Publicatie voor synchronisatie */
   IBM.PUBLISH_EVENT( 'MUT RLE CONTACTPERSONEN'
                    , 'U'
                    , TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
                    , :old.RAS_ID                                       , :new.RAS_ID
                    , :old.RLE_NUMRELATIE                               , :new.RLE_NUMRELATIE
                    , :old.CODE_FUNCTIE                                 , :new.CODE_FUNCTIE
                    , :old.CODE_SOORT_CONTACTPERSOON                    , :new.CODE_SOORT_CONTACTPERSOON
                    , :old.CREATIE_DOOR                                 , :new.CREATIE_DOOR
                    , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
                    , :old.MUTATIE_DOOR                                 , :new.MUTATIE_DOOR
                    , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
                    ) ;
END;
/