CREATE OR REPLACE TRIGGER comp_rle.RLE_ROL_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_ROLLEN
 FOR EACH ROW

DECLARE
-- Sub-Program Unit Declarations
/* Toegestane rollen bij een groepsrol ophalen */
CURSOR C_TRL_003
 (B_CODROL VARCHAR2
 )
 IS
/* C_TRL_003 */
select *
from rle_toegestane_rollen trl
where trl.rol_codgroep = b_codrol;
-- Program Data
R_TRL_003 RLE_TOEGESTANE_ROLLEN%ROWTYPE;
L_IND_FOUND VARCHAR2(1) := 'N';
-- Sub-Program Units
-- PL/SQL Block
BEGIN
IF :new.omsrol <>
     :old.omsrol
  OR :new.prioriteit <>
     :old.prioriteit
  OR :new.indwijzigbaar <>
     :old.indwijzigbaar
  OR :new.indschonen <>
     :old.indschonen
  OR :new.indgroepsrol <>
     :old.indgroepsrol
  THEN
   /* Start toegevoegde code   */
     rle_rol_chk_wijz(:old.codrol,:old.indwijzigbaar);
     IF  :new.indwijzigbaar <> :old.indwijzigbaar THEN
   	     stm_dbproc.raise_error('rle_m_rol_indwijz');
     END IF;
    /* einde toegevoegde code   */
    /* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  END IF;
IF  (:new.indgroepsrol = 'N')
AND (:old.indgroepsrol = 'J') THEN
  OPEN  c_trl_003(:old.codrol);
  FETCH c_trl_003 INTO r_trl_003;
  IF c_trl_003%found THEN
    l_ind_found := 'J';
  ELSE
    l_ind_found := 'N';
  END IF;
  CLOSE c_trl_003;
  IF l_ind_found = 'J' THEN
    stm_dbproc.raise_error('RLE-000533',NULL,'RLE_ROL_BRU');
  END IF;
END IF;
END RLE_ROL_BRU;
/