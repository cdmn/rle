CREATE OR REPLACE TRIGGER comp_rle.RLE_ADS_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_ADRESSEN
 FOR EACH ROW

DECLARE
-- Program Data
L_INDGEWIJZIGD VARCHAR2(1) := 'N';
-- PL/SQL Block
BEGIN
-- RLE_ADS_BRU
if stm_dbproc.column_modified( :new.lnd_codland, :old.lnd_codland)  OR
   stm_dbproc.column_modified( :new.stt_straatid,:old.stt_straatid) OR
   stm_dbproc.column_modified( :new.wps_woonplaatsid
                             , :old.wps_woonplaatsid) OR
   stm_dbproc.column_modified( :new.huisnummer, :old.huisnummer)    OR
   stm_dbproc.column_modified( :new.postcode, :old.postcode)        OR
   stm_dbproc.column_modified( :new.toevhuisnum,:old.toevhuisnum)
then
   l_indgewijzigd := 'J';
end if;
--
if l_indgewijzigd = 'J'
then
-- Begin toegevoegde kode
   if :new.lnd_codland <> :old.lnd_codland  OR
      STM_DBPROC.COLUMN_MODIFIED( :new.stt_straatid
                                , :old.stt_straatid)                OR
      STM_DBPROC.COLUMN_MODIFIED( :new.wps_woonplaatsid
                                , :old.wps_woonplaatsid)            OR
      STM_DBPROC.COLUMN_MODIFIED( :new.huisnummer, :old.huisnummer) OR
      STM_DBPROC.COLUMN_MODIFIED( :new.postcode, :old.postcode)
   then
      --
      -- als batches postcode mutatie en/of vernummering voor
      -- adres insert/mutatie zorgt, dan deze checks niet
      --
      if nvl( rle_npe_pck.g_ind_npe_mutatie, 'N') <> 'J'
      then
         rle_ads_chk_verpl( :new.stt_straatid
                          , :new.postcode
                          , :new.huisnummer
                          , :new.wps_woonplaatsid
                          , :new.lnd_codland
                          );
         if :new.lnd_codland = stm_algm_get.sysparm( NULL
                                                   , 'LAND'
                                                   , sysdate)  AND
            :new.postcode is not null
         then
            rle_ads_chk_pkwpl( :new.postcode
                             , :new.huisnummer
                             , :new.stt_straatid
                             , :new.wps_woonplaatsid
                             );
         end if;
      end if;
   end if;
   --
   -- als batches postcode mutatie en/of vernummering voor
   -- adres insert/mutatie zorgt, dan deze checks niet
   --
   if nvl( rle_npe_pck.g_ind_npe_mutatie, 'N') <> 'J'
   then
      if :new.lnd_codland <> :old.lnd_codland                   OR
         STM_DBPROC.COLUMN_MODIFIED( :new.wps_woonplaatsid
                                   , :old.wps_woonplaatsid)
      then
         rle_ads_chk_wps( :new.wps_woonplaatsid
                        , :new.lnd_codland);
      end if;
--
      if STM_DBPROC.COLUMN_MODIFIED( :new.stt_straatid
                                   , :old.stt_straatid)         OR
         STM_DBPROC.COLUMN_MODIFIED( :new.wps_woonplaatsid
                                   , :old.wps_woonplaatsid)
      then
         rle_ads_chk_strwpl( :new.wps_woonplaatsid
                           , :new.stt_straatid);
      end if;
--
      if :new.lnd_codland <> :old.lnd_codland         AND
         ( :new.postcode is not null                            OR
           STM_DBPROC.COLUMN_MODIFIED( :new.huisnummer
                                     , :old.huisnummer)         OR
           STM_DBPROC.COLUMN_MODIFIED( :new.postcode
                                     , :old.postcode)
         )
      then
         rle_npe_chk_aanw( :new.lnd_codland
                         , :new.huisnummer
                         , :new.postcode);
      end if;
   end if;
-- Einde toegevoegde code
end if;
--
if l_indgewijzigd = 'J'
then
   -- QMS$DATA_AUDITING
   :new.DAT_MUTATIE := sysdate;
   :new.MUTATIE_DOOR := alg_sessie.gebruiker;
   -- QMS$JOURNALLING
   BEGIN
     insert
     into   rle_adressen_jn
            ( jn_user
            , jn_date_time
            , jn_operation
            , numadres
            , lnd_codland
            , codorganisatie
            , stt_straatid
            , wps_woonplaatsid
            , huisnummer
            , postcode
            , toevhuisnum
            , dat_creatie
            , creatie_door
            , dat_mutatie
            , mutatie_door)
     values ( :new.mutatie_door
            , :new.dat_mutatie
            , 'UPD'
            , :old.numadres
            , :old.lnd_codland
            , :old.codorganisatie
            , :old.stt_straatid
            , :old.wps_woonplaatsid
            , :old.huisnummer
            , :old.postcode
            , :old.toevhuisnum
            , :old.dat_creatie
            , :old.creatie_door
            , :old.dat_mutatie
            , :old.mutatie_door
            );
   END;
end if;
END RLE_ADS_BRU;
/