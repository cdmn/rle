CREATE OR REPLACE TRIGGER comp_rle.RLE_RLE_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIES
DECLARE

L_ROWID ROWID;
L_STACK VARCHAR2(2000);

   CURSOR c_rle(b_rowid ROWID)
   IS
      SELECT *
      FROM   rle_relaties rle
      WHERE  rle.rowid = b_rowid
      ;
   r_rle c_rle%rowtype ;
--
  CURSOR c_rrl (b_numrelatie NUMBER)
  IS
     SELECT '1'
       FROM rle_relatie_rollen rrl
      WHERE rrl.rol_codrol = 'WG'
        AND rrl.rle_numrelatie = b_numrelatie
          ;
   r_rrl  c_rrl%rowtype;
-- osp uit bru
   CURSOR C_CNR_001 (B_NUMRELATIE IN NUMBER)
   IS
      SELECT *
      FROM   rle_communicatie_nummers cnr
      WHERE  cnr.rle_numrelatie = b_numrelatie;
/* Ophalen van financieel nummer */
CURSOR C_FNR_001(B_NUMRELATIE IN NUMBER)
 IS
  SELECT *
  FROM rle_financieel_nummers fnr
  WHERE fnr.rle_numrelatie = b_numrelatie;
/* Ophalen van relatie-adres */
CURSOR C_RAS_001 (B_NUMRELATIE IN NUMBER)
 IS
   SELECT *
   FROM rle_relatie_adressen ras
   WHERE ras.rle_numrelatie = b_numrelatie;
/* Ophalen van relatie-externe_codes op rowid */
CURSOR C_REC_003 (B_NUMRELATIE IN NUMBER)
 IS
  SELECT *
  FROM rle_relatie_externe_codes rec
  WHERE rec.rle_numrelatie = b_numrelatie;
-- osp uit bru einde
   CURSOR c_rle_ouders_van(
        cpr_rowid  IN  ROWID
      ) IS
      SELECT rle_kind.datgeboorte     kind_datgeboorte
      ,      rle_ouder.datgeboorte    ouder_datgeboorte
      FROM   rle_relaties             rle_kind
      ,      rle_relatie_koppelingen  rkn
      ,      rle_rol_in_koppelingen   rkg
      ,      rle_relaties             rle_ouder
      WHERE  rle_kind.rowid            = cpr_rowid
      AND    rkn.rle_numrelatie        = rle_kind.numrelatie
      AND    rkn.rle_numrelatie_voor   = rle_ouder.numrelatie
      AND    rkn.rkg_id                = rkg.id
      AND    rkg.code_rol_in_koppeling = 'K'
      ;
   l_job  VARCHAR2( 1 ) ;
   l_old_datoverlijden    DATE;
   l_old_numrelatie       NUMBER;
   l_old_datgeboorte      DATE;
   l_old_datbegin         DATE;
   l_old_dateinde         DATE;
   l_datingang            DATE;
   l_dateinde             DATE;
   -- OKR:
   l_indiener varchar2(50) := NVL(SYS_CONTEXT('userenv', 'client_identifier'), USER);
   -- OKR: toevoeging tbv detectie schijnmutaties ihkv Event Store Personen
   l_old_namrelatie              rle_relaties.namrelatie%type;
   l_old_dwe_wrddom_vvgsl        rle_relaties.dwe_wrddom_vvgsl%type;
   l_old_dwe_wrddom_geslacht     rle_relaties.dwe_wrddom_geslacht%type;
   l_old_voornaam                rle_relaties.voornaam%type;
   l_old_voorletter              rle_relaties.voorletter%type;
   l_old_code_gebdat_fictief     rle_relaties.code_gebdat_fictief%type;
   l_old_naam_partner            rle_relaties.naam_partner%type;
   l_old_vvgsl_partner           rle_relaties.vvgsl_partner%type;
   l_old_code_aanduiding_naamgeb rle_relaties.code_aanduiding_naamgebruik%type;
   -- OKR: variabalen om al dan niet gewijzigde waarden in op te slaan
   l_changed_datgeboorte         varchar2(20) := '__unchanged__';
   l_changed_datoverlijden       varchar2(20) := '__unchanged__';
   l_changed_namrelatie          rle_relaties.namrelatie%type := '__unchanged__';
   l_changed_dwe_wrddom_vvgsl    rle_relaties.dwe_wrddom_vvgsl%type := '__unchanged__';
   l_changed_dwe_wrddom_geslacht rle_relaties.dwe_wrddom_geslacht%type := '__unchanged__';
   l_changed_voornaam            rle_relaties.voornaam%type := '__unchanged__';
   l_changed_voorletter          varchar2(20) := '__unchanged__';
   l_changed_code_gebdat_fictief varchar2(20) := '__unchanged__';
   l_changed_naam_partner        rle_relaties.naam_partner%type := '__unchanged__';
   l_changed_vvgsl_partner       rle_relaties.vvgsl_partner%type := '__unchanged__';
   l_changed_code_aand_naamgeb   rle_relaties.code_aanduiding_naamgebruik%type := '__unchanged__';
--
BEGIN
   stm_util.debug('RLE_RLE_AS');
   stm_algm_muttab.get_rowid( l_rowid, l_stack ) ;
   WHILE l_rowid IS NOT NULL
   LOOP
      l_job := stm_get_word( l_stack ) ;
      IF l_job = 'U'
      THEN
         l_old_datoverlijden    := TO_DATE(stm_get_word(l_stack));
         l_old_numrelatie       := TO_NUMBER(stm_get_word(l_stack));
                 -- osp
                 l_old_datgeboorte := (stm_get_word(l_stack));
                 l_old_datbegin:= (stm_get_word(l_stack));
                 l_old_dateinde := (stm_get_word(l_stack));
                -- osp
         -- OKR:
         l_old_namrelatie              := stm_get_word(l_stack);
         l_old_dwe_wrddom_vvgsl        := stm_get_word(l_stack);
         l_old_dwe_wrddom_geslacht     := stm_get_word(l_stack);
         l_old_voornaam                := stm_get_word(l_stack);
         l_old_voorletter              := stm_get_word(l_stack);
         l_old_code_gebdat_fictief     := stm_get_word(l_stack);
         l_old_naam_partner            := stm_get_word(l_stack);
         l_old_vvgsl_partner           := stm_get_word(l_stack);
         l_old_code_aanduiding_naamgeb := stm_get_word(l_stack);
         --
      ELSIF l_job IN ('I','D')
      THEN
         l_old_numrelatie                  := TO_NUMBER(stm_get_word(l_stack));
        END IF;
      /* BR IER0024
      **  Werkgever 000001 mag niet gemuteerd of verwijderd worden
      */
      rle_wgr_000001.prc_chk_wgr_000001(l_old_numrelatie);
      /* Ophalen huidige rij als dat mogelijk is. */
      OPEN c_rle( l_rowid ) ;
      FETCH c_rle INTO r_rle ;
      CLOSE c_rle ;
      /* BR-IER0035
      ** 30-08-2002, VER, bevinding 198
      **                  Handelsnaam mag niet langer dan 35 posities zijn
      **                  indien er sprake is van een werkgever.
      */
      IF LENGTH(r_rle.handelsnaam) > 35
      THEN
         OPEN c_rrl (r_rle.numrelatie);
         FETCH c_rrl INTO r_rrl;
         IF c_rrl%found
         THEN
            CLOSE c_rrl;
            stm_dbproc.raise_error('RLE-10356' , NULL,'RLE_RLE_AS');
         END IF;
         CLOSE c_rrl;
      END IF;
      /* BRATT0019
      ** Geboorte datum kind < geboortedatum ouder
      */
      IF l_job IN ( 'I', 'U' )
      THEN
         FOR ir_rle_ouders_van IN c_rle_ouders_van( l_rowid )
         LOOP
            IF    ir_rle_ouders_van.kind_datgeboorte
               <= ir_rle_ouders_van.ouder_datgeboorte
            THEN
               stm_dbproc.raise_error( 'RLE-10281' ) ;
            END IF ;
         END LOOP ;
      END IF ;

      /* 21-04-2010 mal
      BR CEV0015  RLE Overlijden leidt tot afsluiten relatiekoppeling*/

      IF r_rle.datoverlijden IS NOT NULL
      THEN

         UPDATE rle_relatie_koppelingen  rkn
         SET    rkn.dat_einde = r_rle.datoverlijden
         WHERE  (  rkn.rle_numrelatie = r_rle.numrelatie
                OR rkn.rle_numrelatie_voor = r_rle.numrelatie
                )
         AND    rkn.rkg_id IN ( SELECT id
                                FROM   rle_rol_in_koppelingen  rkg
                                WHERE  rkg.code_rol_in_koppeling IN ( 'G', 'S', 'H' )
                              )
         AND    rkn.dat_einde IS NULL
         ;
      END IF ;


      /* CHANGE-REC-U: U_GBA_003
      **   Muteren GBA persoon
      */
      IF l_job = 'U'
      THEN
         rle_gba_muteren_persoon( r_rle.numrelatie ) ;
      END IF ;
    IF l_job = 'U'  -- osp cascade update
    THEN
       IF r_rle.datgeboorte > l_old_datgeboorte
       THEN
          UPDATE rle_relatie_adressen
          SET    datingang = r_rle.datgeboorte
          WHERE  rle_numrelatie = r_rle.numrelatie
          AND    datingang < r_rle.datgeboorte;
          UPDATE rle_relatie_externe_codes
          SET    datingang = r_rle.datgeboorte
          WHERE  rle_numrelatie = r_rle.numrelatie
          AND    datingang < r_rle.datgeboorte;
          UPDATE rle_financieel_nummers
          SET    datingang = r_rle.datgeboorte
          WHERE  rle_numrelatie = r_rle.numrelatie
          AND    datingang < r_rle.datgeboorte;
          UPDATE rle_communicatie_nummers
          SET    datingang = r_rle.datgeboorte
          WHERE  rle_numrelatie = r_rle.numrelatie
          AND    datingang < r_rle.datgeboorte;
       END IF; /* new.datgeboorte > old.datgeboorte */
       /* als einddatum wijzigt, einddatum van childs aanpassen indien nodig
        * doen we in de after statement voor het geval triggers op de childs
          weer een select op rle_relaties doen. */
       rle_upd_einddatum_childs(r_rle.numrelatie,l_old_dateinde,r_rle.dateinde);
    END IF;
        /*BR CEV0009
        **rzr 5/6/2002
        **Beeindig persoon EN Arbeidsverhoudingen EN Periodes
        */
   IF     l_job = 'U'
   AND stm_dbproc.column_modified(r_rle.datoverlijden, l_old_datoverlijden)
   THEN
      rle_rle_overlijden (r_rle.numrelatie, r_rle.datoverlijden, l_old_datoverlijden);
   END IF;
   --
   -- MVL, 18-11-2011
   -- Evt herrekenen Inkoopvep, indien het een mogelijke Inkoopvep mutatie betreft.
   IF l_job IN ( 'I', 'U', 'D' )
   and
      -- overlijdensdatum 'gewijzigd'
     (stm_dbproc.column_modified(r_rle.datoverlijden, l_old_datoverlijden)
    or
      -- geboortejaar gewijzigd
      extract (year from nvl(r_rle.datgeboorte, to_date('01-01-1700', 'dd-mm-yyyy'))) <> extract (year from nvl(l_old_datgeboorte, to_date('01-01-1700', 'dd-mm-yyyy')))
     )
   THEN
     if ret_inkoopvep_mutaties.mogelijke_mut_voor_inkoopvep(p_relatienummer_deelnemer => r_rle.numrelatie
                                                           ,p_regeling                => null
                                                           )
     then
        -- aanmaken event tbv aanmaken case voor proces Inkoopvep mutaties (RET01001)
        ret_inkoopvep_mutaties.produce_herrekening(p_aanleiding    => 'Overlijden of Geboortedatum mutatie'
                                                  ,p_modulenaam    => 'COMP_RLE.RLE_RLE_AS.TRG'
                                                  ,p_regeling      => null
                                                  ,p_relatienummer => r_rle.numrelatie
                                                  ,p_datum         => sysdate
                                                  );
     end if;
   END IF;
   -- OKR: Wijzigingen voor Event Store Personen
   if r_rle.datgeboorte is not null -- alleen voor personen, die hebben altijd een geboortedatum
   then
     if l_job = 'U'
        and not -- Schijnmutaties detecteren
        (
        stm_dbproc.value_modified(l_old_datoverlijden, r_rle.datoverlijden)
        or
        stm_dbproc.value_modified(l_old_datgeboorte, r_rle.datgeboorte)
        or
        stm_dbproc.value_modified(l_old_namrelatie, r_rle.namrelatie)
        or
        stm_dbproc.value_modified(l_old_dwe_wrddom_vvgsl, r_rle.dwe_wrddom_vvgsl)
        or
        stm_dbproc.value_modified(l_old_dwe_wrddom_geslacht, r_rle.dwe_wrddom_geslacht)
        or
        stm_dbproc.value_modified(l_old_voornaam, r_rle.voornaam)
        or
        stm_dbproc.value_modified(l_old_voorletter, r_rle.voorletter)
        or
        stm_dbproc.value_modified(l_old_code_gebdat_fictief, r_rle.code_gebdat_fictief)
        or
        stm_dbproc.value_modified(l_old_naam_partner, r_rle.naam_partner)
        or
        stm_dbproc.value_modified(l_old_vvgsl_partner, r_rle.vvgsl_partner)
        or
        stm_dbproc.value_modified(l_old_code_aanduiding_naamgeb, r_rle.code_aanduiding_naamgebruik)
        )
     then
       -- Schijnmutatie: niet loggen naar event store
       null;
       --
     elsif l_job <> 'D' -- deletes van relaties worden niet ondersteund, omdat dit nooit voorkomt
     then
       --
       if stm_dbproc.value_modified(l_old_datoverlijden, r_rle.datoverlijden)
       then
         l_changed_datoverlijden := to_char(r_rle.datoverlijden, 'YYYY-MM-DD');
       end if;
       --
       if stm_dbproc.value_modified(l_old_datgeboorte, r_rle.datgeboorte)
       then
         l_changed_datgeboorte := to_char(r_rle.datgeboorte, 'YYYY-MM-DD');
       end if;
       --
       if stm_dbproc.value_modified(l_old_namrelatie, r_rle.namrelatie)
       then
         l_changed_namrelatie := r_rle.namrelatie;
       end if;
       --
       if stm_dbproc.value_modified(l_old_dwe_wrddom_vvgsl, r_rle.dwe_wrddom_vvgsl)
       then
         l_changed_dwe_wrddom_vvgsl := r_rle.dwe_wrddom_vvgsl;
       end if;
       --
       if stm_dbproc.value_modified(l_old_dwe_wrddom_geslacht, r_rle.dwe_wrddom_geslacht)
       then
         l_changed_dwe_wrddom_geslacht := r_rle.dwe_wrddom_geslacht;
       end if;
       --
       if stm_dbproc.value_modified(l_old_voornaam, r_rle.voornaam)
       then
         l_changed_voornaam := r_rle.voornaam;
       end if;
       --
       if stm_dbproc.value_modified(l_old_voorletter, r_rle.voorletter)
       then
         l_changed_voorletter := r_rle.voorletter;
       end if;
       --
       if stm_dbproc.value_modified(l_old_code_gebdat_fictief, r_rle.code_gebdat_fictief)
       then
         l_changed_code_gebdat_fictief := r_rle.code_gebdat_fictief;
       end if;
       --
       if stm_dbproc.value_modified(l_old_naam_partner, r_rle.naam_partner)
       then
         l_changed_naam_partner := r_rle.naam_partner;
       end if;
       --
       if stm_dbproc.value_modified(l_old_vvgsl_partner, r_rle.vvgsl_partner)
       then
         l_changed_vvgsl_partner := r_rle.vvgsl_partner;
       end if;
       --
       if stm_dbproc.value_modified(l_old_code_aanduiding_naamgeb, r_rle.code_aanduiding_naamgebruik)
       then
         l_changed_code_aand_naamgeb := r_rle.code_aanduiding_naamgebruik;
       end if;
       --
       -- publiceer event naar AQ
       appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_rle(p_event_title => 'Persoonsgegevens gewijzigd'
                                                                      ,p_relatienr => r_rle.numrelatie
                                                                      ,p_naam => l_changed_namrelatie
                                                                      ,p_voornamen => l_changed_voornaam
                                                                      ,p_voorletters => l_changed_voorletter
                                                                      ,p_voorvoegsels => l_changed_dwe_wrddom_vvgsl
                                                                      ,p_naamgebruik => l_changed_code_aand_naamgeb
                                                                      ,p_geboortedatum_fictief => l_changed_datgeboorte
                                                                      ,p_code_gebdatum_fictief => l_changed_code_gebdat_fictief
                                                                      ,p_overlijdendat_fictief => l_changed_datoverlijden
                                                                      ,p_geslacht => l_changed_dwe_wrddom_geslacht
                                                                      ,p_partner_naam => l_changed_naam_partner
                                                                      ,p_partner_voorvoegsels => l_changed_vvgsl_partner);
     --
     end if;
     --
   end if;
   --
   stm_algm_muttab.get_rowid(l_rowid, l_stack);
END LOOP;
   stm_algm_muttab.clear_array;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
     NULL;
   WHEN OTHERS THEN
     stm_algm_muttab.clear_array;
     RAISE;
END;
/