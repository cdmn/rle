CREATE OR REPLACE TRIGGER comp_rle.RLE_STT_BRI
 BEFORE INSERT
 ON comp_rle.RLE_STRATEN
 FOR EACH ROW

BEGIN
if :new.straatid is null
  then
    :new.straatid := rle_stt_seq1_next_id;
  end if;
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_STT_BRI;
/