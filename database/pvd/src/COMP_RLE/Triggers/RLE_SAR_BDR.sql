CREATE OR REPLACE TRIGGER comp_rle.RLE_SAR_BDR
 BEFORE DELETE
 ON comp_rle.RLE_SADRES_ROLLEN
 FOR EACH ROW

BEGIN
/* RLE_SAR_BDR */
rle_sar_chk_ras(:old.codrol
,	:old.dwe_wrddom_sadres);
END RLE_SAR_BDR;
/