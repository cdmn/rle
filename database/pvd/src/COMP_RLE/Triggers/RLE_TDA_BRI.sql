CREATE OR REPLACE TRIGGER comp_rle.rle_tda_bri
  before insert
  on comp_rle.rle_tech_data
  for each row

  declare
  l_number   number;
begin

  if :new.id is null
  then
    select rle_tda_seq.nextval
    into   l_number
    from   dual;

    :new.id := l_number;
  end if;

end rle_tda_bri;
/