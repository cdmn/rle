CREATE OR REPLACE TRIGGER comp_rle.rle_res_briu
  before insert or update
  on comp_rle.rle_input_ex_partners
  for each row
declare
  cursor c_dual
  is
    select sysdate, user
    from   dual;
  --
  l_sysdate           date;
  l_user              varchar2 ( 30 );
begin
  open c_dual;

  fetch c_dual
  into   l_sysdate, l_user;

  close c_dual;

  if inserting
  then
    :new.dat_creatie  := l_sysdate;
    :new.creatie_door := l_user;
    :new.dat_mutatie  := l_sysdate;
    :new.mutatie_door := l_user;
  else
    :new.dat_creatie  := l_sysdate;
    :new.creatie_door := l_user;
    :new.dat_mutatie  := l_sysdate;
    :new.mutatie_door := l_user;
  end if;
end;
/