CREATE OR REPLACE TRIGGER comp_rle.RLE_KVR_ARD
 AFTER DELETE
 ON comp_rle.RLE_KANAALVOORKEUREN
 FOR EACH ROW
BEGIN
/* QMS$JOURNALLING */
   insert
   into rle_kanaalvoorkeuren_jn
   (     jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      dat_begin
   ,      rle_numrelatie
  --,     code_soort_kanaalvoorkeur
   ,      creatie_door
   ,      dat_creatie
   ,      mutatie_door
   ,      dat_mutatie
   ,      cck_id
   ,      authentiek
   )
   values
   (      alg_sessie.gebruiker
   ,      sysdate
   ,      'DEL'
   ,      :old.dat_begin
   ,      :old.rle_numrelatie
   --,      :old.code_soort_kanaalvoorkeur
   ,      :old.creatie_door
   ,      :old.dat_creatie
   ,      :old.mutatie_door
   ,      :old.dat_mutatie
   ,      :old.cck_id
   ,      :old.authentiek
   );
END;
/