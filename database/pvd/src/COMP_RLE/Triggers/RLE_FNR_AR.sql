CREATE OR REPLACE TRIGGER comp_rle.RLE_FNR_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_FINANCIEEL_NUMMERS
 FOR EACH ROW
DECLARE

L_STACK VARCHAR2(128);
BEGIN
/* RLE_FNR_AR */
/* Per rij de bewerking meegeven */
IF inserting
THEN
  /* rle_rme_insert(:new.rle_numrelatie
  	, null
    	, :new.rol_codrol
    	, 'RLE171'
    	, '1'
    	, :new.dwe_wrddom_srtrek
    	, null
    	, :new.datingang
    	, null
    	, :new.numrekening);
*/
/*
     if :new.rol_codrol = 'VV'
     then
         pls_tfs_trans(:new.rle_numrelatie,'PLS171',:new.datingang);
     end if;
*/
     stm_add_word (l_stack, 'I');
elsif updating
then
    -- 20/02/2001 Invullen van de einddatum agv opvoeren
   --  nieuw voorkomen dienen niet getransformeerd te worden.
/*    if :old.indinhown = 'N'
    and  :new.indinhown = 'J'
    then
        null;
*/
   /* else
        rle_rme_insert(:old.rle_numrelatie
  	,  null
    	, :old.rol_codrol
    	, 'RLE172'
    	, '2'
    	, :old.dwe_wrddom_srtrek
    	, :old.datingang
    	, :new.datingang
    	, :old.numrekening
    	, :new.numrekening);
*/
/*    end if;
    if :old.rol_codrol = 'VV'
    then
        pls_tfs_trans(:old.rle_numrelatie,'PLS172',:new.datingang);
    end if;
*/
   stm_add_word (l_stack, 'U');
elsif deleting
then
/*    rle_rme_insert(:old.rle_numrelatie
  	, null
    	, :old.rol_codrol
    	, 'RLE173'
    	, '3'
    	, :old.dwe_wrddom_srtrek
    	, :old.datingang
    	, null
    	, :old.numrekening
    	, null);
*/
   /* QMS$JOURNALLING */
   BEGIN
   INSERT
   INTO   rle_financieel_nummers_jn
   (      jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      rle_numrelatie
   ,      numrekening
   ,      lnd_codland
   ,      dwe_wrddom_srtrek
   ,      datingang
   ,      indinhown
   ,      dateinde
   ,      indfiat
   ,      rol_codrol
   ,      codorganisatie
   ,      tenaamstelling
   ,      coddoelrek
   ,      codwijzebetaling
   ,      BIC
   ,      IBAN
   ,      dat_creatie
   ,      creatie_door
   ,      dat_mutatie
   ,      mutatie_door
   )
   VALUES
   (      alg_sessie.gebruiker
   ,      SYSDATE
   ,      'DEL'
   ,      :old.rle_numrelatie
   ,      :old.numrekening
   ,      :old.lnd_codland
   ,      :old.dwe_wrddom_srtrek
   ,      :old.datingang
   ,      :old.indinhown
   ,      :old.dateinde
   ,      :old.indfiat
   ,      :old.rol_codrol
   ,      :old.codorganisatie
   ,      :old.tenaamstelling
   ,      :old.coddoelrek
   ,      :old.codwijzebetaling
   ,      :old.BIC
   ,      :old.IBAN
   ,      :old.dat_creatie
   ,      :old.creatie_door
   ,      :old.dat_mutatie
   ,      :old.mutatie_door
   );
  END;
  stm_add_word (l_stack, 'D');
ELSE
   stm_add_word (l_stack, ' ');
END IF;
/* per rij de benodigde old values meegeven */
IF NOT inserting
THEN
      stm_add_word (l_stack, TO_CHAR(:old.datingang));
      stm_add_word (l_stack, TO_CHAR(:old.dateinde));
      stm_add_word (l_stack, :old.indinhown);
      stm_add_word (l_stack, :old.rle_numrelatie);
  --    stm_add_word (l_stack, :old.dwe_wrddom_srtrek);
      stm_add_word (l_stack, :old.rol_codrol);
      stm_add_word (l_stack, :old.numrekening );
      stm_add_word (l_stack, :old.codwijzebetaling);
END IF;
/* vullen rowid en linkstring in de interne tabel */
IF deleting
THEN
    stm_algm_muttab.add_rowid(:old.rowid, l_stack);
ELSE
    stm_algm_muttab.add_rowid(:new.rowid, l_stack);
END IF;
END;
/