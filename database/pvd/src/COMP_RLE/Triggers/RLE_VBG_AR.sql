CREATE OR REPLACE TRIGGER comp_rle.RLE_VBG_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_VERZENDBESTEMMINGEN
 FOR EACH ROW

BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
BEGIN
   /* Add variabelen */
   IF inserting
   THEN
      stm_add_word( l_stack, 'I' ) ;
      stm_add_word( l_stack, :new.code_soort_post  ) ;
      /* BRATT0023 */
      stm_add_word( l_stack, :new.code_soort_adres ) ;
      /* BRMOD0009*/
      stm_add_word( l_stack, :new.rrl_rol_codrol ) ;
      stm_add_word( l_stack, :new.rrl_rle_numrelatie ) ;
      stm_add_word( l_stack, :new.rrl_rol_codrol_hoort_bij ) ;
      stm_add_word( l_stack, :new.rrl_rle_numrelatie_hoort_bij ) ;
      stm_add_word( l_stack, :new.rrl_rol_codrol_betreft ) ;
      stm_add_word( l_stack, :new.rrl_rle_numrelatie_betreft ) ;
   ELSIF updating
   THEN
      stm_add_word( l_stack, 'U' ) ;
      stm_add_word( l_stack, :new.code_soort_post  ) ;
      /* BRATT0023 */
      stm_add_word( l_stack, :new.code_soort_adres ) ;
      /* BRMOD0009 */
      stm_add_word( l_stack, :new.rrl_rol_codrol ) ;
      stm_add_word( l_stack, :new.rrl_rle_numrelatie ) ;
      stm_add_word( l_stack, :new.rrl_rol_codrol_hoort_bij ) ;
      stm_add_word( l_stack, :new.rrl_rle_numrelatie_hoort_bij ) ;
      stm_add_word( l_stack, :new.rrl_rol_codrol_betreft ) ;
      stm_add_word( l_stack, :new.rrl_rle_numrelatie_betreft ) ;
   ELSE /* deleting */
      stm_add_word( l_stack, 'D' ) ;
      /* niets nodig voor delete */
   END IF ;
   /* Add ROWID */
   IF inserting OR updating
   THEN
      stm_algm_muttab.add_rowid( :new.rowid, l_stack ) ;
   ELSE
      stm_algm_muttab.add_rowid( :old.rowid, l_stack ) ;
   END IF ;
END ;
END RLE_VBG_AR;
/