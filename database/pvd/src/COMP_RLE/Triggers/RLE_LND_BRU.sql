CREATE OR REPLACE TRIGGER comp_rle.RLE_LND_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_LANDEN
 FOR EACH ROW
begin
  if stm_dbproc.column_modified(:new.naam,:old.naam)
  or stm_dbproc.column_modified(:new.naamingezetene
                 ,:old.naamingezetene)
  or stm_dbproc.column_modified(:new.codlayoutadres
                 , :old.codlayoutadres)
  or stm_dbproc.column_modified(:new.netnummer,:old.netnummer)
  or stm_dbproc.column_modified(:new.indstraatverplicht,:old.indstraatverplicht)
  or stm_dbproc.column_modified(:new.indnummerverplicht
	,:old.indnummerverplicht)
  or stm_dbproc.column_modified(:new.indwnplverplicht
	,:old.indwnplverplicht)
  or stm_dbproc.column_modified(:new.indpostcodeverplicht
     	,:old.indpostcodeverplicht)
  or stm_dbproc.column_modified(:new.controletabel
	,:old.controletabel)
  or stm_dbproc.column_modified(:new.layoutpostcode
                  ,:old.layoutpostcode)
  -- nieuw ivm SEPA
  or stm_dbproc.column_modified(:new.landcode_iso3166_a2
                 ,:old.landcode_iso3166_a2)
  or stm_dbproc.column_modified(:new.ind_sepa_rekening
                 ,:old.ind_sepa_rekening)
  then
/* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  end if;
end;
/