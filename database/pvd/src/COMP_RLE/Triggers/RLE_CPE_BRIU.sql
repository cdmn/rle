CREATE OR REPLACE TRIGGER comp_rle.RLE_CPE_BRIU
 BEFORE INSERT OR UPDATE
 ON comp_rle.RLE_CAMPAGNES
 FOR EACH ROW
DECLARE

  cursor c_dual
  is
  select sysdate
  ,      user
  from   dual;
  l_sysdate   date;
  l_user      varchar2(30);
begin
  open c_dual;
  fetch c_dual
  into  l_sysdate
  ,     l_user;
  close c_dual;
  if inserting
  then
    if :new.ID is null
    then
      select rle_cpe_seq.nextval
      into :new.id
      from dual;
    end if;
    :new.dat_creatie  := l_sysdate;
    :new.creatie_door := l_user;
    :new.dat_mutatie  := l_sysdate;
    :new.mutatie_door := l_user;
  else
    :new.dat_mutatie  := l_sysdate;
    :new.mutatie_door := l_user;
  end if;
end;
/