CREATE OR REPLACE TRIGGER comp_rle."RLE_ASA_AR"
  after delete or insert or update on comp_rle."RLE_AFSTEMMINGEN_GBA"
  for each row
declare
  /************************************************************

  Datum       Wie        Wat

  --------    ---------- -----------------------------------
  24-10-2016  OKR        Aanmaak trigger tbv sync naar Personen Event Store
  *************************************************************/
  -- Program Data
  l_stack varchar2(128);
  -- PL/SQL Block
begin
  /* RLE_REC_AR */
  /* Per rij de bewerking meegeven */
  if inserting
  then
    stm_add_word(l_stack, 'I');
    stm_add_word(l_stack, :new.rle_numrelatie);
    stm_add_word(l_stack, :new.afnemers_indicatie);
    stm_add_word(l_stack, :new.code_gba_status);
  elsif updating
  then
    stm_add_word(l_stack, 'U');
    stm_add_word(l_stack, :new.rle_numrelatie);
    stm_add_word(l_stack, :new.afnemers_indicatie);
    stm_add_word(l_stack, :new.code_gba_status);
    stm_add_word(l_stack, :old.rle_numrelatie);
    stm_add_word(l_stack, :old.afnemers_indicatie);
    stm_add_word(l_stack, :old.code_gba_status);
  elsif deleting
  then
    stm_add_word(l_stack, 'D');
    stm_add_word(l_stack, :old.rle_numrelatie);
    stm_add_word(l_stack, :old.afnemers_indicatie);
    stm_add_word(l_stack, :old.code_gba_status);
  end if;
  /* per rij de benodigde old values meegeven */
  /* vullen rowid en linkstring in de interne tabel */
  if deleting
  then
    stm_algm_muttab.add_rowid(:old.rowid, l_stack);
  else
    stm_algm_muttab.add_rowid(:new.rowid, l_stack);
  end if;
end rle_rec_ar;
/