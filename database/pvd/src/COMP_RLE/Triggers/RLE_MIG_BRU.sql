CREATE OR REPLACE TRIGGER comp_rle.RLE_MIG_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_MEDIUM_INZENDINGEN
 FOR EACH ROW

BEGIN
/* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  INSERT
   INTO rle_medium_inzendingen_jn
   (     jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      dat_begin
   ,      code_soort_inzending
   ,      rle_numrelatie
   ,      creatie_door
   ,      dat_creatie
   ,      mutatie_door
   ,      dat_mutatie
   )
   VALUES
   (      alg_sessie.gebruiker
   ,      SYSDATE
   ,      'UPD'
   ,      :old.dat_begin
   ,      :old.code_soort_inzending
   ,      :old.rle_numrelatie
   ,      :old.creatie_door
   ,      :old.dat_creatie
   ,      :old.mutatie_door
   ,      :old.dat_mutatie
   );
END RLE_MIG_BRU;
/