CREATE OR REPLACE TRIGGER comp_rle.RLE_RAS_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_RELATIE_ADRESSEN
 FOR EACH ROW

declare
/*****************************************************************************************************/
/* Trigger RLE.RLE_RAS_AR:                                                                           */
/*                                                                                                   */
/*                                                                                                   */
/* Datum      Wie         Omschrijving                                                               */
/* ---------- ----------- ---------------------------------------------------------------------------*/
/* ??-??-??   ???         Creatie                                                                    */
/* 01-09-2016 DFU         Eventstore, bij Delete ook old value adm_code op de stack                  */
/*                        voor de volledigheid ook aan journal toegevoegd                            */
/*****************************************************************************************************/
-- Program Data
L_STACK VARCHAR2(128);
-- PL/SQL Block
BEGIN
   /* RLE_RAS_AR */
   -- Aangepast: 18-08-2000 MSC
   /* RLE_RAS_AR */
   /* Per rij de bewerking meegeven */
   IF	inserting
   THEN
     stm_add_word (l_stack, 'I');
   ELSIF	updating
   THEN
     stm_add_word (l_stack, 'U');
     -- 20/02/2001 Invullen van de einddatum agv opvoeren
     --  nieuw voorkomen dienen niet getransformeerd te worden.
   ELSIF	deleting
   THEN
     stm_add_word (l_stack, 'D');
    /* QMS$JOURNALLING */
	  BEGIN
   	   INSERT
   	   INTO   rle_relatie_adressen_jn
   	     (      jn_user
   	     ,      jn_date_time
   	     ,      jn_operation
  	     ,      id
         ,      datingang
   	     ,      rle_numrelatie
   	     ,      dwe_wrddom_srtadr
   	     ,      ads_numadres
  	     ,      codorganisatie
   	     ,      indinhown
   	     ,      rol_codrol
   	     ,      numkamer
   	     ,      dateinde
   	     ,      locatie
   	     ,      ind_woonboot
   	     ,      ind_woonwagen
   	     ,      dat_creatie
         ,      creatie_door
   	     ,      dat_mutatie
   	     ,      mutatie_door
         ,      adm_code --dfu
   	     )
     	 VALUES
   	     (      alg_sessie.gebruiker
   	     ,      SYSDATE
   	     ,      'DEL'
   	     ,      :old.id
  	     ,      :old.datingang
   	     ,      :old.rle_numrelatie
   	     ,      :old.dwe_wrddom_srtadr
  	     ,      :old.ads_numadres
   	     ,      :old.codorganisatie
   	     ,      :old.indinhown
   	     ,      :old.rol_codrol
  	     ,      :old.numkamer
   	     ,      :old.dateinde
         ,      :old.locatie
   	     ,      :old.ind_woonboot
   	     ,      :old.ind_woonwagen
         ,      :old.dat_creatie
   	     ,      :old.creatie_door
   	     ,      :old.dat_mutatie
   	     ,      :old.mutatie_door
         ,      :old.adm_code --dfu
  	     );
	  END;
  ELSE
   	stm_add_word (l_stack, ' ');
  END IF;

  /* per rij de benodigde old values meegeven */
  IF	NOT inserting
  THEN
   	stm_add_word (l_stack, TO_CHAR(:old.datingang));
   	stm_add_word (l_stack, TO_CHAR(:old.dateinde));
   	stm_add_word (l_stack, :old.indinhown);
   	stm_add_word (l_stack, :old.rle_numrelatie);
   	stm_add_word (l_stack, :old.dwe_wrddom_srtadr);
   	stm_add_word (l_stack, :old.rol_codrol);
   	stm_add_word (l_stack, :old.ads_numadres);
    stm_add_word (l_stack, :old.id ) ;
    stm_add_word (l_stack, :old.adm_code);--dfu
  END IF;

  /* vullen rowid en linkstring in de interne tabel */
  IF	deleting
  THEN
   	stm_algm_muttab.add_rowid(:old.rowid, l_stack);
  ELSE
   	stm_algm_muttab.add_rowid(:new.rowid, l_stack);
  END IF;

END RLE_RAS_AR;
/