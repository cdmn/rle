CREATE OR REPLACE TRIGGER comp_rle.RLE_VBG_ARD
 AFTER DELETE
 ON comp_rle.RLE_VERZENDBESTEMMINGEN
 FOR EACH ROW

BEGIN
/* QMS$JOURNALLING */
   INSERT
   INTO rle_verzendbestemmingen_jn
   (     jn_user
   ,      jn_date_time
   ,      jn_operation
   ,	    id
   ,      code_soort_post
   ,      code_soort_adres
   ,      rrl_rol_codrol
   ,      rrl_rle_numrelatie
   ,      rrl_rol_codrol_hoort_bij
   ,      rrl_rle_numrelatie_hoort_bij
   ,      rrl_rol_codrol_betreft
   ,      rrl_rle_numrelatie_betreft
   ,      creatie_door
   ,      dat_creatie
   ,      mutatie_door
   ,      dat_mutatie
   )
   VALUES
   (      alg_sessie.gebruiker
   ,      SYSDATE
   ,      'DEL'
   ,      :old.id
   ,      :old.code_soort_post
   ,      :old.code_soort_adres
   ,      :old.rrl_rol_codrol
   ,      :old.rrl_rle_numrelatie
   ,      :old.rrl_rol_codrol_hoort_bij
   ,      :old.rrl_rle_numrelatie_hoort_bij
   ,      :old.rrl_rol_codrol_betreft
   ,      :old.rrl_rle_numrelatie_betreft
   ,      :old.creatie_door
   ,      :old.dat_creatie
   ,      :old.mutatie_door
   ,      :old.dat_mutatie
   );
END;
/