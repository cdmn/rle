CREATE OR REPLACE TRIGGER comp_rle.RLE_FNR_BRU
 BEFORE UPDATE
 ON comp_rle.RLE_FINANCIEEL_NUMMERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE



L_INDGEWIJZIGD VARCHAR2(1) := 'N';
l_lnd varchar2(2);
l_bic varchar2(11);
l_iban varchar2(34);
BEGIN
rle_wgr_000001.prc_chk_wgr_000001(:old.rle_numrelatie);
--begindatum niet in de toekomst
if trunc(:new.datingang) > trunc(sysdate)
then
   raise_application_error(-20000, stm_get_batch_message('RLE-00432# ' || 'Vanaf'));
end if;
--
if :new.codwijzebetaling is not null
then
   if not OIS$CHECK_REFERENCE_CODE(
          :new.codwijzebetaling,
          'WIJZE VAN BETALING')
   then
      raise_application_error(-20000, 'Deze waarde staat niet in het domein WIJZE VAN BETALING');

end if;
end if;
--  check code wijze betaling
if rle_chk_iban_lnd (:new.lnd_codland)
then
   if :new.codwijzebetaling is null
   then
      raise_application_error(-20000, stm_get_batch_message('RLE-10430'));
   end if;
else
   if :new.codwijzebetaling is not null
   then
      raise_application_error(-20000, stm_get_batch_message('RLE-10430'));
   end if;
end if;
--
if :new.IBAN is not null
then
   :new.IBAN := upper(:new.IBAN);
   :new.BIC  := upper(:new.BIC);
   if not rle_chk_iban_lnd (:new.IBAN)
   then
      l_lnd := substr(:new.IBAN, 1, 2);
      raise_application_error(-20000, stm_get_batch_message('RLE-10424# ' || l_lnd));
   end if;
end if;
--
if :new.BIC is not null and :new.lnd_codland = 'NL'
then
   if not stm_sepa.chk_bic_exists (:new.BIC)
   then
     l_bic := :new.BIC;
     raise_application_error(-20000, stm_get_batch_message('STM-20041# ' || l_bic));
   end if;
end if;
--
  if :new.lnd_codland = 'NL'
  then
    if :new.numrekening != :old.numrekening
    then
       raise_application_error(-20000, stm_get_batch_message('RLE-10428'));
    end if;
    if :new.numrekening is null
       or :new.bic is null
       or :new.iban is null
    then
       raise_application_error(-20000, stm_get_batch_message('RLE-10425'));
    end if;
  else
    if rle_chk_iban_lnd (:new.lnd_codland)
    then
      if :new.bic is null
         or :new.iban is null
         or ( :new.iban != :new.numrekening )
      then
         raise_application_error(-20000, stm_get_batch_message('RLE-10425'));
      end if;
    else
      if :new.iban is not null
         or :new.bic is not null
      then
         raise_application_error(-20000, stm_get_batch_message('RLE-10426'));
      end if;
    end if;
  end if;
if :new.IBAN is not null
   and not stm_sepa.chk_iban_97 (:new.IBAN)
then
    l_iban := :new.IBAN;
    raise_application_error(-20000, stm_get_batch_message('RLE-10427# ' || l_iban));
end if;
--
-- Extra controles SEPA II
--
if stm_dbproc.column_modified(:new.IBAN, :old.IBAN)
then
  raise_application_error(-20000, 'IBAN mag niet gewijzigd worden. Sluit deze af en maak een nieuwe aan.');
end if;
--
if :new.codwijzebetaling is not null
   and :new.codwijzebetaling not in ('AS', 'SB')
then
  raise_application_error(-20000, 'Slechts betaalwijzen AS en SB zijn nog toegestaan. Een van deze is verplicht.');
end if;
--
if :new.dateinde is not null
  and :new.codwijzebetaling = 'AS'
  and bbs_rle_lib.get_mtg_niet_beeindigd(:new.rle_numrelatie, :new.id) is not null
then
  raise_application_error(-20000, 'Van deze bankrekening wordt nog automatisch geincasseerd; u dient eerst de machtiging te beeindigen.');
end if;
--
if :old.codwijzebetaling = 'AS' and
   :new.codwijzebetaling = 'SB' and
   bbs_rle_lib.get_mtg_niet_beeindigd(:new.rle_numrelatie, :new.id) is not null
then
   raise_application_error(-20000, 'Er is nog machtiging aanwezig voor deze bankrekening.');
end if;
--
/* RLE_FNR_BRU */
IF stm_dbproc.column_modified(:new.lnd_codland,:old.lnd_codland)
--OR stm_dbproc.column_modified(:new.dwe_wrddom_srtrek, :old.dwe_wrddom_srtrek)
OR stm_dbproc.column_modified(:new.datingang,:old.datingang)
OR stm_dbproc.column_modified(:new.dateinde,:old.dateinde)
OR stm_dbproc.column_modified(:new.rol_codrol, :old.rol_codrol)
OR stm_dbproc.column_modified(:new.IBAN, :old.IBAN)
THEN
  l_indgewijzigd  := 'J';

    if  stm_dbproc.column_modified(:new.dateinde, :old.dateinde)
    then
	  rle_rle_childper(:old.rle_numrelatie, :new.datingang, :new.dateinde);
    end if;
end if;
if l_indgewijzigd = 'J'
then
/* QMS$DATA_AUDITING */
     :new.DAT_MUTATIE := SYSDATE;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
  /* QMS$JOURNALLING */
  BEGIN
   INSERT
   INTO   rle_financieel_nummers_jn
   (      jn_user
   ,      jn_date_time
   ,      jn_operation
   ,      rle_numrelatie
   ,      numrekening
   ,      lnd_codland
   ,      dwe_wrddom_srtrek
   ,      datingang
   ,      indinhown
   ,      dateinde
   ,      indfiat
   ,      rol_codrol
   ,      codorganisatie
   ,      tenaamstelling
   ,      coddoelrek
   ,      codwijzebetaling
   ,      BIC
   ,      IBAN
   ,      dat_creatie
   ,      creatie_door
   ,      dat_mutatie
   ,      mutatie_door
   )
   VALUES
   (      :new.mutatie_door
   ,      :new.dat_mutatie
   ,      'UPD'
   ,      :old.rle_numrelatie
   ,      :old.numrekening
   ,      :old.lnd_codland
   ,      :old.dwe_wrddom_srtrek
   ,      :old.datingang
   ,      :old.indinhown
   ,      :old.dateinde
   ,      :old.indfiat
   ,      :old.rol_codrol
   ,      :old.codorganisatie
   ,      :old.tenaamstelling
   ,      :old.coddoelrek
   ,      :old.codwijzebetaling
   ,      :old.BIC
   ,      :old.IBAN
   ,      :old.dat_creatie
   ,      :old.creatie_door
   ,      :old.dat_mutatie
   ,      :old.mutatie_door
   );
  END;
END IF;


/* Publicatie voor synchronisatie */
BEGIN
   IBM.PUBLISH_EVENT
      ( 'MUT RLE FINANCIEEL NRS'
      , 'U'
      , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.RLE_NUMRELATIE                               , :new.RLE_NUMRELATIE
      , :old.NUMREKENING                                  , :new.NUMREKENING
      , :old.LND_CODLAND                                  , :new.LND_CODLAND
      , :old.DWE_WRDDOM_SRTREK                            , :new.DWE_WRDDOM_SRTREK
      , to_char(:old.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')  , to_char(:new.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')
      , :old.INDINHOWN                                    , :new.INDINHOWN
      , to_char(:old.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')   , to_char(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.INDFIAT                                      , :new.INDFIAT
      , :old.ROL_CODROL                                   , :new.ROL_CODROL
      , :old.CODORGANISATIE                               , :new.CODORGANISATIE
      , :old.TENAAMSTELLING                               , :new.TENAAMSTELLING
      , :old.CODDOELREK                                   , :new.CODDOELREK
      , :old.CODWIJZEBETALING                             , :new.CODWIJZEBETALING
      , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.CREATIE_DOOR                                 , :new.CREATIE_DOOR
      , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
      , :old.MUTATIE_DOOR                                 , :new.MUTATIE_DOOR
      , :old.ID                                           , :new.ID
      , :old.iban                                         , :new.iban
      , :old.adm_code                                     , :new.adm_code
      );
END;
END;
/