CREATE OR REPLACE TRIGGER comp_rle.RLE_WVE_BRI
 BEFORE INSERT
 ON comp_rle.RLE_WWFT_VERDACHTEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
begin

  :new.dat_creatie  := sysdate;
  :new.creatie_door := alg_sessie.gebruiker;
  :new.dat_mutatie  := SYSDATE;
  :new.mutatie_door := alg_sessie.gebruiker;
end rle_sat_bri;
/