CREATE OR REPLACE TRIGGER comp_rle.RLE_RLE_AUR
 AFTER UPDATE OF DATGEBOORTE
, DATOVERLIJDEN
 ON comp_rle.RLE_RELATIES
 FOR EACH ROW
BEGIN
  if :old.datgeboorte <> :new.datgeboorte
  then
    rle_producers.rle_prs_event_producer(p_numrelatie => :new.numrelatie);
  else
    if nvl(:old.datoverlijden,sysdate+1) <> nvl(:new.datoverlijden,sysdate+1)
    then
      rle_producers.rle_prs_event_producer(p_numrelatie => :new.numrelatie
                                         ,p_dat_begin  => least(nvl(:old.datoverlijden,sysdate+1), nvl(:new.datoverlijden,sysdate+1))
                                         );
    end if;
  end if;
END;
/