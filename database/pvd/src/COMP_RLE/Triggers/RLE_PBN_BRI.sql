CREATE OR REPLACE TRIGGER comp_rle.RLE_PBN_BRI
 BEFORE INSERT
 ON comp_rle.RLE_PCOD_BATCHRUN
 FOR EACH ROW

BEGIN
/* ==ODA-BRI-BGN== */
  if :new.id is null
  then
    :new.id := rle_pbn_seq1_next_id;
  end if;
/* ==ODA-BRI-END== */
/* Handmatig toegevoegd */
-- Controle referentiewaarde
ibm.ibm('chk_refwrd'
              , 'RLE'
              ,'PBN'
              ,:new.subtype
              );
/* QMS$DATA_AUDITING */
     :new.DAT_CREATIE := sysdate;
     :new.CREATIE_DOOR := alg_sessie.gebruiker;
     :new.DAT_MUTATIE := sysdate;
     :new.MUTATIE_DOOR := alg_sessie.gebruiker;
END RLE_PBN_BRI;
/