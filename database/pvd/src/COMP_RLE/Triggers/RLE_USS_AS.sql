CREATE OR REPLACE TRIGGER comp_rle.RLE_USS_AS
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_UITVOER_STATUSSEN
DECLARE

   l_stack             varchar2 (128);
   l_rowid             rowid;
   l_job               varchar2 (1);

   cursor c_uss001 (
      b_rowid   in   rowid)
   is
      select runnummer
            ,bsd_naam
            ,vanaf_datum
            ,tot_datum
      from   rle_uitvoer_statussen uss
      where  uss.rowid = b_rowid;

   r_uss001            c_uss001%rowtype;
   r_uss001_empty      c_uss001%rowtype;

   cursor c_uss002 (
      b_bsd_naam   in   rle_uitvoer_statussen.bsd_naam%type)
   is
      select   runnummer
              ,bsd_naam
              ,vanaf_datum
              ,tot_datum
              ,uss.rowid
      from     rle_uitvoer_statussen uss
      where    uss.bsd_naam = b_bsd_naam
      order by vanaf_datum;

   r_uss002            c_uss002%rowtype;
   r_uss002_empty      c_uss002%rowtype;
   r_uss002_previous   c_uss002%rowtype;
begin
   stm_algm_muttab.get_rowid (l_rowid, l_stack);

   while l_rowid is not null
   loop
      /* Ophalen standaard variabelen */
      l_job := stm_get_word (l_stack);

      if l_job = 'D'
      then
         /* Ophalen "old" variabelen voor delete */
         r_uss001.bsd_naam := stm_get_word (l_stack);
         r_uss001.vanaf_datum := stm_get_word (l_stack);
      else
         /* Ophalen "new" variabelen voor insert of update */
         open c_uss001 (l_rowid);

         fetch c_uss001
         into  r_uss001;

         close c_uss001;

end if;

      /* Start Business Rule "BR ENT0018 USS" */
      r_uss002_previous := r_uss002_empty;

      if l_job in ('I', 'U')
      then
         for r_uss002 in c_uss002 (r_uss001.bsd_naam)
         loop
            if -- r_uss002.rowid <> l_rowid  -- eoa reden verandert l_rowid, binnen het loop, waardoor hij toch een controle plaats vindt met zichzelf.
                r_uss002.bsd_naam  = r_uss001.bsd_naam     -- 15-07-2016 YRP  Jira RCM-282, zichzelf niet vergelijken, rowid kan tijdens run wijziggen.
            and r_uss002.runnummer <> r_uss001.runnummer   -- zichzelf niet vergelijken

            then
               stm_algm_check.algm_datovlp (r_uss001.vanaf_datum
                                           ,r_uss001.tot_datum-1
                                           ,r_uss002.vanaf_datum
                                           ,r_uss002.tot_datum-1);
            end if;

            if     r_uss002_previous.vanaf_datum is not null
               and r_uss002_previous.tot_datum < r_uss002.vanaf_datum
            then
               stm_dbproc.raise_error ('WGR-00071', null, 'RLE_USS_AS');
            end if;

            r_uss002_previous := r_uss002;
         end loop;
      elsif l_job = 'D'
      then
         delete      rle_uitvoer_statussen
         where       bsd_naam = r_uss001.bsd_naam
         and         vanaf_datum > r_uss001.vanaf_datum;
      end if;

      /* Einde Business Rule "BR ENT0018 USS" */
      stm_algm_muttab.get_rowid (l_rowid, l_stack);
   end loop;

   stm_algm_muttab.clear_array;
exception
   when others
   then
      stm_algm_muttab.clear_array;
      raise;
end rle_uss_as;
/