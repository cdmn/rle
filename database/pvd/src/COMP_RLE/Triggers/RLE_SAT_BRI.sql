CREATE OR REPLACE TRIGGER comp_rle.RLE_SAT_BRI
 BEFORE INSERT
 ON comp_rle.RLE_SANCTIELIJSTEN
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE

  l_number   number;
begin
  if :new.id is null
  then
    select rle_sat_seq1.nextval
    into   l_number
    from   dual;

    :new.id := l_number;
  end if;
  :new.status := 'N';
/* QMS$DATA_AUDITING */
  :new.dat_creatie := sysdate;
  :new.creatie_door := alg_sessie.gebruiker;
  :new.dat_mutatie  := SYSDATE;
  :new.mutatie_door := alg_sessie.gebruiker;
end rle_sat_bri;
/