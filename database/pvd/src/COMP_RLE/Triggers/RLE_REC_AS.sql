CREATE OR REPLACE TRIGGER comp_rle."RLE_REC_AS"
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle."RLE_RELATIE_EXTERNE_CODES"
DECLARE

CURSOR C_REC_017
 (B_ROWID IN ROWID
 )
 IS
select *
  from rle_relatie_externe_codes rec
  where rec.rowid = b_rowid;

R_REC_017 C_REC_017%ROWTYPE;
L_ROL_CODROL VARCHAR2(10);
L_STACK VARCHAR2(128);
L_ROWID ROWID;
L_JOB VARCHAR2(1);
L_OLD_EXTERN_RELATIE VARCHAR2(20);
L_NEW_EXTERN_RELATIE VARCHAR2(20);
L_DWE_WRDDOM_EXTSYS VARCHAR2(30);
L_NUMRELATIE NUMBER;
L_REC_ID NUMBER(9);
L_NEW_DATINGANG DATE;
L_OLD_DATINGANG DATE;
L_NEW_DATEINDE DATE;
L_OLD_DATEINDE DATE;

/* Ophalen van relatie-externe_codes op rowid */
CURSOR C_REC_001
 (B_CODSYSTEEMCOMP IN VARCHAR2
 ,B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
select rec.*, rowid
  from   rle_relatie_externe_codes rec
  where  rec.dwe_wrddom_extsys = b_codsysteemcomp
  and    rec.rle_numrelatie    = b_numrelatie
  and  ((rol_codrol is null and b_codrol is null) or
         rol_codrol = b_codrol);
/* Dubbele referenties bepalen */
CURSOR C_REC_016
 (B_SYSTEEM IN VARCHAR2
 ,B_EXTERN_RELATIE IN VARCHAR2
 ,B_NUMRELATIE IN NUMBER
 )
 IS
select '1'
  from  rle_relatie_externe_codes
  where dwe_wrddom_extsys = upper (b_systeem)
  and   extern_relatie    = b_extern_relatie
  and   rle_numrelatie    <> b_numrelatie
  and   dateinde          is null;

  r_rec_016 c_rec_016%rowtype;
CURSOR C_REC_018
 (B_SYSTEEM VARCHAR2
 ,B_EXTERN_RELATIE VARCHAR2
 ,B_NUMRELATIE NUMBER
 )
 IS
select '1'
  from   rle_relatie_externe_codes
  where  dwe_wrddom_extsys =  upper (b_systeem)
  and    extern_relatie    <> b_extern_relatie
  and    rle_numrelatie    =  b_numrelatie
  and    dateinde          is null;

  r_rec_018 c_rec_018%rowtype;
/************************************************************

Triggernaam: rle_nat_bs

Datum       Wie        Wat

--------    ---------- -----------------------------------
29-11-2000  Veldman_h	l_dwe_wrddom_extsys toegevoegd want avg
                        moet alleen worden aangeroepen
                        als l_dwe_wrddom_extsys = 'SOFI'
26-02-2001  LJa         Controle dat een geldig sofinummer of A-nummer
                        niet bij 2 relaties geldig is
29-01-2003  PKG         IVM dubbel opgevoerde werkgevers,
                        nu BR ENT0004 REC wel goed implementeren:
                        geen enkel extern_relatie mag dubbel voorkomen
                        bij hetzelfde extern systeem (Bugfix 1548)
31-01-2003 JKU          Werkg 1 controle van BRD naar de AS trigger
                        ivm mutating table problems.
25-02-2010 XMK          PRD-INC-174099 global uit trigger rle_rec_as
                        verwijderen en in rle0061f toevoegen.
25-10-2016 OKR          Loggen van mutaties naar Personen Event Store
18-11-2016 OKR          Initiele aanmaak van persoonsnummer signaleert
                        de registratie van de persoon
*************************************************************/
begin
  stm_algm_muttab.get_rowid (l_rowid, l_stack);

  while l_rowid is not null
  loop
    /* Ophalen standaard variabelen */
    l_job := stm_get_word (l_stack);

    /* Ophalen variabelen per soort statement */
    if l_job = 'I'
    then
      l_numrelatie         := stm_get_word (l_stack);
      l_new_extern_relatie := stm_get_word (l_stack);
      l_dwe_wrddom_extsys  := stm_get_word (l_stack);
    elsif l_job = 'U'
    then
      l_numrelatie         := stm_get_word (l_stack);
      l_new_extern_relatie := stm_get_word (l_stack);
      l_dwe_wrddom_extsys  := stm_get_word (l_stack);
      l_old_extern_relatie := stm_get_word (l_stack);
      l_old_datingang      := to_date (stm_get_word (l_stack));
    elsif l_job = 'D'
    then
      l_rol_codrol        := stm_get_word (l_stack);
      l_numrelatie        := stm_get_word (l_stack);
      l_old_datingang     := to_date (stm_get_word (l_stack));
      l_old_dateinde      := to_date (stm_get_word (l_stack));
      l_dwe_wrddom_extsys := stm_get_word (l_stack);
    end if;

    /* BR IER0024 Werkgever 000001 mag niet gemuteerd of verwijderd worden */
    if l_job = 'U'
    then
      rle_wgr_000001.prc_chk_wgr_000001 (l_numrelatie);
    end if;

    /* Controle dubbel voorkomen externe code */
    if l_job in ('I', 'U')
    then
    /* PKG: 29-01-2003 IVM dubbel opgevoerde werkgevers, nu BR ENT0004 REC wel goed implementeren:
              Alleen bij extern systeem PWGR mag een extern_relatie dubbel voorkomen.
              Dit moet met een unique key!!!!!!! */

      if l_dwe_wrddom_extsys <> 'PWGR'
      then
        open c_rec_016 ( b_systeem        => l_dwe_wrddom_extsys
                       , b_extern_relatie => l_new_extern_relatie
                       , b_numrelatie     => l_numrelatie);
        fetch c_rec_016 into r_rec_016;

        if c_rec_016%found then
          close c_rec_016;
          stm_dbproc.raise_error ('RLE-00550', '', 'RLE_REC_AS');
        else
          close c_rec_016;
        end if;

      end if;
    end if;

    /* BRENT0004 Voorkomen overlap externe codes */
    if l_job != 'D' then
      open  c_rec_017 (b_rowid => l_rowid);
      fetch c_rec_017 into r_rec_017;
      close c_rec_017;

      l_rec_id        := r_rec_017.id;
      l_numrelatie    := r_rec_017.rle_numrelatie;
      l_new_datingang := r_rec_017.datingang;
      l_new_dateinde  := r_rec_017.dateinde;
    end if;

    /* BRENT0004: Bij toevoegen nieuw voorkomen, afsluiten voorliggende voorkomen. */
    if l_job = 'I'
    then
      /* controleren of er meer records zijn */
      open c_rec_018 ( b_systeem        => l_dwe_wrddom_extsys
                     , b_extern_relatie => l_new_extern_relatie
                     , b_numrelatie     => l_numrelatie );

      fetch c_rec_018 into r_rec_018;

      if c_rec_018%found
      then
        close c_rec_018;

        update rle_relatie_externe_codes rec
        set    rec.dateinde              = l_new_datingang - 1
        where  rec.id                   != l_rec_id
        and    rec.rle_numrelatie        = l_numrelatie
        and    nvl (rec.rol_codrol, '@') = nvl (l_rol_codrol, '@')
        and    rec.dwe_wrddom_extsys     = l_dwe_wrddom_extsys
        and    rec.dateinde             is null;
      else
        close c_rec_018;
      end if;
      /* BRENT0004: Bij wijzigen begindatum, ook "vorige" record aanpassen */
    elsif l_job = 'U'
    then
      if l_old_datingang != l_new_datingang
      then
        update rle_relatie_externe_codes rec
        set    rec.dateinde             = l_new_datingang - 1
        where  rec.id                  != l_rec_id
        and    rec.rle_numrelatie       = l_numrelatie
        and    nvl(rec.rol_codrol, '@') = nvl (l_rol_codrol, '@')
        and    rec.dwe_wrddom_extsys    = l_dwe_wrddom_extsys
        and    rec.dateinde             = l_old_datingang - 1;
      end if;
    /* BRENT0004: Bij verwijderd als een voorkomen verwijderd wordt waarnaar de
              einddatum van een "broederrecord" verwijst, dan schuift de einddatum van
              dit "broederrecord" op naar de einddatum van het verwijderde record. */
    elsif l_job = 'D'
    then
    /* BR IER0024 Werkgever 000001 mag niet gemuteerd of verwijderd worden */
      rle_wgr_000001.prc_chk_wgr_000001 (pin_numrelatie => l_numrelatie);

      update rle_relatie_externe_codes rec
      set    rec.dateinde              = l_old_dateinde
      where  rec.rle_numrelatie        = l_numrelatie
      and    nvl (rec.rol_codrol, '@') = nvl (l_rol_codrol, '@')
      and    rec.dwe_wrddom_extsys     = l_dwe_wrddom_extsys
      and    rec.dateinde              = l_old_datingang - 1;
    end if;

    /* BRENT0004 Overlapcontrole voor "broederrecord" */
    if l_job != 'D'
    then
      for ir_rec_001 in c_rec_001 ( b_codsysteemcomp => l_dwe_wrddom_extsys
                                  , b_numrelatie     => l_numrelatie
                                  , b_codrol         => l_rol_codrol )
      loop
        if ir_rec_001.id != l_rec_id
        then
          stm_algm_check.algm_datovlp ( p_datingang1 => l_new_datingang
                                      , p_dateinde1  => l_new_dateinde
                                      , p_datingang2 => ir_rec_001.datingang
                                      , p_dateinde2  => ir_rec_001.dateinde );
        end if;
      end loop;
    end if;

    /* END: BRENT0004 */
    /* CHANGE-REC-I: U_GBA_002 Toevoegen GBA persoon */

    if l_job = 'I'
    then
      if l_dwe_wrddom_extsys = 'PRS'
      then
        /* Bij het opvoeren van het persoonsnummer "PRS" moet de persoon worden toegevoegd in het GBA. */
        rle_gba_toevoegen_persoon ( pin_numrelatie => l_numrelatie );
      end if;

      if l_dwe_wrddom_extsys = 'SOFI'
      then
        /* CHANGE-REC-U: U_GBA_015 Toevoegen sofinummer */
        rle_gba_toevoegen_sofi ( pin_numrelatie => l_numrelatie );
      end if;

      if l_dwe_wrddom_extsys = 'GBA'
      then
        /* CHANGE-REC-U: U_GBA_016 Toevoegen a-nummer */
        rle_gba_toevoegen_anummer ( pin_numrelatie => l_numrelatie );
      end if;
    end if;

    /* CHANGE-REC-U: U_GBA_003 Muteren GBA persoon  */
    if l_job in ('U', 'D')
    then
      if l_dwe_wrddom_extsys in ('SOFI', 'GBA')
      then
        rle_gba_muteren_persoon ( pin_numrelatie => l_numrelatie );
      end if;
    end if;
    --
    /* OKR Loggen mutaties naar Personen Events Store */
    if ( l_dwe_wrddom_extsys in ('SOFI', 'GBA')
         and (l_job <> 'U' or stm_dbproc.value_modified(l_old_extern_relatie, l_new_extern_relatie))
        )
       or
       ( l_dwe_wrddom_extsys = 'PRS'
         and l_job = 'I' -- Voor P-nr wordt alleen INSERT ondersteund
        )
    then
      appl_relatie.persoon_mutaties_event_store.publiceer_mutatie_rec(p_event_title       => case
                                                                                               when l_dwe_wrddom_extsys = 'SOFI' then
                                                                                                'BSN gewijzigd'
                                                                                               when l_dwe_wrddom_extsys = 'GBA' then
                                                                                                'A-nummer gewijzigd'
                                                                                               when l_dwe_wrddom_extsys = 'PRS' then
                                                                                                -- NB Adhv deze exacte titel wordt bij het
                                                                                                -- groeperen in appl_relatie.groepeer_persoon_mutaties
                                                                                                -- bepaald of het een persoonregistratie is !
                                                                                                -- In samenhang aanpassen dus.
                                                                                                'Persoon geregistreerd'
                                                                                             end
                                                                     ,p_relatienr         => l_numrelatie
                                                                     ,p_extern_relatie    => l_new_extern_relatie
                                                                     ,p_dwe_wrddom_extsys => l_dwe_wrddom_extsys
                                                                     ,p_dml_actie         => l_job);
    end if;
    --
    /* Ophalen volgende record */
    stm_algm_muttab.get_rowid (l_rowid, l_stack);
  end loop;

   stm_algm_muttab.clear_array;
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      stm_algm_muttab.clear_array;
      RAISE;
end;
/