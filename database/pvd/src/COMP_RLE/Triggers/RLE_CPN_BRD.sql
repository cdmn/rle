CREATE OR REPLACE TRIGGER comp_rle.RLE_CPN_BRD
 BEFORE DELETE
 ON comp_rle.RLE_CONTACTPERSONEN
 FOR EACH ROW
 
BEGIN
/* Publicatie voor synchronisatie */
   IBM.PUBLISH_EVENT( 'MUT RLE CONTACTPERSONEN'
                    , 'D'
                    , TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
                    , :old.RAS_ID                                       , NULL
                    , :old.RLE_NUMRELATIE                               , NULL
                    , :old.CODE_FUNCTIE                                 , NULL
                    , :old.CODE_SOORT_CONTACTPERSOON                    , NULL
                    , :old.CREATIE_DOOR                                 , NULL
                    , to_char(:old.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
                    , :old.MUTATIE_DOOR                                 , NULL
                    , to_char(:old.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss'), NULL
                    ) ;
END;
/