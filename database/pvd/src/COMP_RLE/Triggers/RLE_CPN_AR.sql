CREATE OR REPLACE TRIGGER comp_rle.RLE_CPN_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_CONTACTPERSONEN
 FOR EACH ROW

BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
BEGIN
   /* Add variabelen */
   IF inserting
   THEN
      stm_add_word( l_stack, 'I' ) ;
      /* niets nodig voor insert */
   ELSIF updating
   THEN
      stm_add_word( l_stack, 'U' ) ;
      /* niets nodig voor updete */
   ELSE /* deleting */
      stm_add_word( l_stack, 'D' ) ;
      /* niets nodig voor delete */
   END IF ;
   /* Add ROWID */
   IF inserting OR updating
   THEN
      stm_algm_muttab.add_rowid( :new.rowid, l_stack ) ;
   ELSE
      stm_algm_muttab.add_rowid( :old.rowid, l_stack ) ;
   END IF ;
END ;
END RLE_CPN_AR;
/