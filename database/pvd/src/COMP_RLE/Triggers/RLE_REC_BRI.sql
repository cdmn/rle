CREATE OR REPLACE TRIGGER comp_rle.RLE_REC_BRI
 BEFORE INSERT
 ON comp_rle.RLE_RELATIE_EXTERNE_CODES
 FOR EACH ROW
DECLARE

/* Zoeken afstemming gba met NUMRELATIE */
CURSOR C_ASA_001
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT *
FROM   rle_afstemmingen_gba  asa
WHERE  asa.rle_numrelatie = cpn_numrelatie
;
r_asa_001  c_asa_001%ROWTYPE;
-- 17-02-2015 ZSF Extra attribuut voor event rle.BSNgewijzigd
-- 17-02-2015 ZSF Opnieuw wgr.GewijzigdBovagNummerWerkgever
-- 17-02-2015 ZSF Extra attribuut voor event wgr.GewijzigdeIdentificatie
-- 17-02-2015 ZSF Opnieuw wgr.GewijzigdBovagNummerWerkgever
-- 20-01-2015 EZW   Code m.b.t. Bancs events weer actief gemaakt
-- 15-01-2015 EZW   #BANCS Code m.b.t. Bancs events afgesterd
-- 20-11-2014 ZSF nieuw event voor aansluiting Bancs
-- 30-05-2014 EZW   Nieuw event salarismutatie voor aansluiting Bancs
l_event    xmltype;

-- 14-07-2014 ZSF rle.BSNGewijzigd => rle.BSNgewijzigd
cn_event_BSNGewijzigd  constant varchar2(20) := 'rle.BSNgewijzigd';
cn_event_WgrId  constant varchar2(30) := 'wgr.GewijzigdeIdentificatie';
--
BEGIN
/* BR IER0024
   Werkgever 000001 mag niet gemuteerd of verwijderd worden */
   rle_wgr_000001.prc_chk_wgr_000001(:new.rle_numrelatie);
  IF    :new.dwe_wrddom_extsys = 'SOFI'
  THEN
   /* SOFI-nummer moet verplicht 9 cijfers hebben */
    :new.extern_relatie := LTRIM( TO_CHAR( TO_NUMBER( :new.extern_relatie ), '000000000' ) ) ;
   /* BRATT0014
   controle op de volgende range: tussen 001000007 and 999999990 */
    IF TO_NUMBER(:new.extern_relatie) < 1000007
       OR
	   TO_NUMBER(:new.extern_relatie) > 999999990 -- JPG 10-12-2007 Incident 94911
    THEN
      stm_dbproc.raise_error( 'RLE-10330' ) ;
    END IF;
  ELSIF :new.dwe_wrddom_extsys = 'WGR'
  THEN
   /* Werkgever nummer moet verplicht 6 cijfers hebben */
    :new.extern_relatie := LTRIM( TO_CHAR( TO_NUMBER( :new.extern_relatie ), '000000' ) ) ;
  END IF ;
/*  Begin toegevoegde code  */
  IF :new.dwe_wrddom_extsys ='GBA'
  THEN
    :new.rol_codrol := 'PR';
  ELSIF :new.dwe_wrddom_extsys =  'SOFI'
  THEN
    :new.rol_codrol := 'PR';
  END IF;
  IF :new.rol_codrol IS NOT NULL
  THEN
    rle_chk_rrl_aanw(:new.rle_numrelatie,  :new.rol_codrol);
  END IF;
 -- Controle referentiewaarde
  ibm.ibm( 'chk_refwrd'
          ,'RLE'
          ,'CES'
          ,:new.dwe_wrddom_extsys);
  IF :new.dwe_wrddom_extsys = 'RBD'
  THEN
    rle_rec_chk_nr_rbd (:new.extern_relatie);
  END IF;
  IF :new.dwe_wrddom_extsys = 'SOFI'
  THEN
    stm_algm_check.elf_proef_sofi (TO_NUMBER(:new.extern_relatie));
  END IF;
/* BRATT0015
   A-nummer moet voldoen aan GBA 11-proef
*/
  IF :new.dwe_wrddom_extsys = 'GBA'
  THEN
    IF NOT rle_gba_elfproef( :new.extern_relatie )
    THEN
      stm_dbproc.raise_error( 'RLE-10303' ) ;
    END IF ;
  END IF ;
  IF :new.id IS NULL
  THEN
    :new.id := rle_rec_seq1_next_id;
  END IF;
--
-- RTJ 08-12-2003: Call 91515: Als CODE_GBA_STATUS 'AF' is, dan mag
--                             geen Sofinummer of a-nummer worden opgevoerd.
  IF :new.dwe_wrddom_extsys in ('GBA', 'SOFI')
  THEN
    IF c_asa_001%ISOPEN
    THEN
      CLOSE c_asa_001;
    END IF;
    OPEN  c_asa_001(:new.rle_numrelatie);
    FETCH c_asa_001 INTO r_asa_001;
    IF    r_asa_001.code_gba_status = 'AF'
    THEN
      raise_application_error(-20000,'Code GBA status is AF, daarom mag geen Sofinummer of A-nummer worden opgevoerd.');
    END IF;
    CLOSE c_asa_001;
  END IF;
--
-- Aanmaken voorkomen voor transformatie
/* IF :new.dwe_wrddom_extsys IN ( 'SOFI','FISC','RBD')
THEN
rle_rme_insert(:new.rle_numrelatie
,	:new.id
,	:new.rol_codrol
,	'RLE141'
,	'1'
,	'RECORD EXTERN_RELATIE'
,	NULL
,	:new.datingang
,	NULL
,	:new.extern_relatie);
END IF;
*/
/* QMS$DATA_AUDITING */
  :new.DAT_CREATIE := SYSDATE;
  :new.CREATIE_DOOR := alg_sessie.gebruiker;
  :new.DAT_MUTATIE := SYSDATE;
  :new.MUTATIE_DOOR := alg_sessie.gebruiker;
   /* Publicatie voor synchronisatie */
   -- 12-11-2003, VER, alleen een publicatie, indien mutatie betrekking heeft op A-nummer of SOFI-nummer
   -- 10-11-2004 JWB uitgebreid met BOVAGnummer inzake verzekeringsnemers
   -- 22-07-2005, VER, PR-UII, Consolidatie ZEUS/HERA, GBA- en SOFI-nummer worden niet meer gesynchroniseerd
  IF :new.DWE_WRDDOM_EXTSYS in ('KVK','BOVAG')
  THEN
      IBM.PUBLISH_EVENT
         ( 'MUT RLE EXTERNE CODES'
         , 'I'
         , to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, :new.ID
         , NULL, :new.RLE_NUMRELATIE
         , NULL, :new.DWE_WRDDOM_EXTSYS
         , NULL, to_char(:new.DATINGANG, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, :new.CODORGANISATIE
         , NULL, :new.ROL_CODROL
         , NULL, :new.EXTERN_RELATIE
         , NULL, :new.IND_GEVERIFIEERD
         , NULL, to_char(:new.DATEINDE, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, to_char(:new.DAT_CREATIE, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, :new.CREATIE_DOOR
         , NULL, to_char(:new.DAT_MUTATIE, 'dd-mm-yyyy hh24:mi:ss')
         , NULL, :new.MUTATIE_DOOR
         );
  END IF;
  -- wanneer een sofinummer is gewijzigd, publiceren van event BSNGewijzigd.
  -- 10-03-2010 AAM voor Nationaal Pensioen Register
  IF   :new.dwe_wrddom_extsys =   'SOFI'
       then
       l_event := eventhandler.create_event(p_event_code => cn_event_BSNGewijzigd);
       eventhandler.add_num_value(p_event  => l_event
                                 ,p_key    => 'RELATIENUMMER'
                                 ,p_value  => :new.rle_numrelatie);
       eventhandler.add_date_value( p_event => l_event
                                  , p_key => 'DATUM_MUTATIE'
                                  , p_value => :new.dat_mutatie
                                  );
       eventhandler.publish_asynch_event(p_event => l_event);
  END IF;
  if :new.dwe_wrddom_extsys = 'KVK'
     or :new.dwe_wrddom_extsys = 'WGR'
     or :new.dwe_wrddom_extsys = 'RSIN'
  then
       l_event := eventhandler.create_event(p_event_code => cn_event_WgrId);
       eventhandler.add_num_value(p_event  => l_event
                                 ,p_key    => 'RELATIENUMMER'
                                 ,p_value  => :new.rle_numrelatie);
      eventhandler.add_date_value( p_event => l_event
                                 , p_key => 'DATUM_MUTATIE'
                                 , p_value => :new.dat_mutatie
                                 );
       eventhandler.publish_asynch_event(p_event => l_event);
  end if;
--
  if :new.dwe_wrddom_extsys = 'BOVAG'
  then
    l_event := eventhandler.create_event( p_event_code => 'wgr.GewijzigdBovagNummerWerkgever' );
    eventhandler.add_num_value( p_event  => l_event
                              , p_key    => 'RELATIENUMMER'
                              , p_value  => :new.rle_numrelatie
                              );
    eventhandler.add_date_value( p_event => l_event
                               , p_key => 'DATUM_MUTATIE'
                               , p_value => :new.dat_mutatie
                               );
    eventhandler.publish_asynch_event( p_event => l_event );
  end if;
END ;
/