CREATE OR REPLACE TRIGGER comp_rle.RLE_GBD_AR
 AFTER DELETE OR INSERT OR UPDATE
 ON comp_rle.RLE_GEMOEDSBEZWAARDHEDEN
 FOR EACH ROW

BEGIN
DECLARE
   l_stack  VARCHAR2( 128 ) ;
BEGIN
   /* Add variabelen */
   IF inserting
   THEN
      stm_add_word( l_stack, 'I' ) ;
      /* Niets nodig voor insert */
   ELSIF updating
   THEN
      stm_add_word( l_stack, 'U' ) ;
      /* Niets nodig voor update */
   ELSE /* deleting */
      stm_add_word( l_stack, 'D' ) ;
      /* niets nodig voor delete */
   END IF ;
   /* Add ROWID */
   IF inserting OR updating
   THEN
      stm_algm_muttab.add_rowid( :new.rowid, l_stack ) ;
   ELSE
      stm_algm_muttab.add_rowid( :old.rowid, l_stack ) ;
   END IF ;
END ;
END RLE_GBD_AR;
/