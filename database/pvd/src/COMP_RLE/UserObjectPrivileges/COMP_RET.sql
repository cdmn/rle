GRANT SELECT ON comp_rle.rle_relatie_externe_codes TO comp_ret WITH GRANT OPTION;
GRANT REFERENCES ON comp_rle.rle_relaties TO comp_ret;
GRANT SELECT ON comp_rle.rle_v_alle_relaties TO comp_ret WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_personen TO comp_ret WITH GRANT OPTION;