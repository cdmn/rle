GRANT REFERENCES ON comp_rle.rle_administraties TO comp_vag;
GRANT REFERENCES ON comp_rle.rle_relaties TO comp_vag;
GRANT SELECT ON comp_rle.rle_v_alle_relaties TO comp_vag WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_personen TO comp_vag WITH GRANT OPTION;