GRANT SELECT ON comp_rle.rle_administraties TO dbl_vanuit_edw_comp_rle;
GRANT EXECUTE ON comp_rle.rle_chk_rrl_exists TO dbl_vanuit_edw_comp_rle;
GRANT SELECT ON comp_rle.rle_relatie_externe_codes TO dbl_vanuit_edw_comp_rle;
GRANT SELECT ON comp_rle.rle_relaties TO dbl_vanuit_edw_comp_rle;
GRANT SELECT ON comp_rle.rle_rollen TO dbl_vanuit_edw_comp_rle;
GRANT SELECT ON comp_rle.rle_v_relatie_adressen TO dbl_vanuit_edw_comp_rle;