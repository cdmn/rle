GRANT SELECT ON comp_rle.rle_financieel_nummers TO dbl_vanuit_coda_codafin;
GRANT INSERT ON comp_rle.rle_financieel_nummers TO dbl_vanuit_coda_codafin;
GRANT SELECT ON comp_rle.rle_finnr_coda TO dbl_vanuit_coda_codafin;
GRANT INSERT ON comp_rle.rle_finnr_coda TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_get_coda_debiteur_data TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_get_finnum_sepa TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_get_fnr_ai_sepa TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_get_fnr_tag TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_get_verzendnaam TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_ins_fnr TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_ins_fnr_tag TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_lib TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_m29_finnum_sepa TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_pck_lijsten TO dbl_vanuit_coda_codafin;
GRANT EXECUTE ON comp_rle.rle_upd_fnr_tag TO dbl_vanuit_coda_codafin;