GRANT EXECUTE ON comp_rle.rle_lib TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_mv_personen_1 TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_mv_personen_2 TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_rol_in_koppelingen TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_administratiekantoren TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_administraties TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_personen_xl_portal TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_relatiekoppelingen TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_relatierollen_in_adm_zc TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_relaties TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_werkgevers_portal_lookup TO app_iri_mns;
GRANT SELECT ON comp_rle.rle_v_werkgevers_xl_portal TO app_iri_mns;