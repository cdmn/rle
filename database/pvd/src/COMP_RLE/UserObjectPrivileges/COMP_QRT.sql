GRANT SELECT ON comp_rle.rle_adressen TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_afstemmingen_gba TO comp_qrt WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_get_aanhef TO comp_qrt WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_get_verzendnaam TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_landen TO comp_qrt WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_lib TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_mv_personen_1 TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_mv_personen_2 TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relatie_adressen TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relatie_externe_codes TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relatie_koppelingen TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relaties TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_straten TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_personen TO comp_qrt WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_woonplaatsen TO comp_qrt WITH GRANT OPTION;