GRANT SELECT ON comp_rle.rle_landen TO app_pwg_pme;
GRANT SELECT ON comp_rle.rle_v_financieel_nummers TO app_pwg_pme;
GRANT SELECT ON comp_rle.rle_v_ned_postcode TO app_pwg_pme;
GRANT SELECT ON comp_rle.rle_v_personen TO app_pwg_pme;
GRANT SELECT ON comp_rle.rle_v_personen_xl TO app_pwg_pme;
GRANT SELECT ON comp_rle.rle_v_personen_xl_portal TO app_pwg_pme;
GRANT SELECT ON comp_rle.rle_v_werkgevers_xl TO app_pwg_pme;
GRANT SELECT ON comp_rle.rle_v_werkgevers_xl_portal TO app_pwg_pme;