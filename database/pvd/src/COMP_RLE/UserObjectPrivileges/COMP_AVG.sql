GRANT EXECUTE ON comp_rle.fnc_bepaal_voorletters TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_adressen TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_communicatie_nummers TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_contactpersonen TO comp_avg WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_get_m11_rle_naw TO comp_avg WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_get_voorkeursnaam TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_landen TO comp_avg WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_m03_rec_extcod TO comp_avg WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_m11_rle_naw TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_ned_postcode TO comp_avg WITH GRANT OPTION;
GRANT EXECUTE ON comp_rle.rle_pers TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relatie_adressen TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relatie_externe_codes TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relatie_koppelingen TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relatierollen_in_adm TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_relaties TO comp_avg WITH GRANT OPTION;
GRANT REFERENCES ON comp_rle.rle_relaties TO comp_avg;
GRANT SELECT ON comp_rle.rle_rol_in_koppelingen TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_straten TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_adm_kantoren TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_ned_postcode TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_personen TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_personen_xl TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_relatiekoppelingen TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_relatierollen_in_adm TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_relatierollen_in_adm_zc TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_v_werkgevers_xl TO comp_avg WITH GRANT OPTION;
GRANT SELECT ON comp_rle.rle_woonplaatsen TO comp_avg WITH GRANT OPTION;