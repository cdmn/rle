CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_FINNUM
 (P_NUMRELATIE IN VARCHAR2
 ,P_CODROL IN VARCHAR2
 ,P_SRTREK IN VARCHAR2
 ,P_DATUM IN DATE
 )
 RETURN VARCHAR2
 IS
-- Program Data
L_NAAM VARCHAR2(80);
L_NUMREKENING VARCHAR2(20);
L_SRTREK VARCHAR2(20);
L_MELDING VARCHAR2(80);
-- PL/SQL Block
BEGIN
/* RLE_GET_FINNUM */
rle_m29_finnum(p_numrelatie
, p_codrol
, p_srtrek
, p_datum
, l_numrekening
, l_srtrek
, l_naam
, l_melding);
return l_numrekening;
END RLE_GET_FINNUM;

 
/