CREATE OR REPLACE FUNCTION comp_rle.RLE_ROL_GETOMS
 (P_CODROL IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Ophalen rol */
CURSOR C_ROL_001
 (B_CODROL IN VARCHAR2
 )
 IS
/* C_ROL_001 */
select	*
from	rle_rollen rol
where	rol.codrol = b_codrol;
-- Program Data
R_ROL_001 C_ROL_001%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ROL_GETOMS */
if	p_codrol is not null then
	open c_rol_001(p_codrol);
 	fetch c_rol_001 into r_rol_001;
	if	c_rol_001%notfound then
		close c_rol_001;
		return('Rol onbekend');
	else
		close c_rol_001;
		return(r_rol_001.omsrol);
	end if;
else
	return null;
end if;
END RLE_ROL_GETOMS;

 
/