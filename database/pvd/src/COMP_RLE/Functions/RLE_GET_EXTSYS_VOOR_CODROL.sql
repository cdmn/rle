CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_EXTSYS_VOOR_CODROL
 (PIV_CODROL IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
BEGIN
   IF    piv_codrol = 'PR'  /* Persoon              */  THEN RETURN( 'PRS' ) ;
   ELSIF piv_codrol = 'WG'  /* Werkgever            */  THEN RETURN( 'WGR' ) ;
   ELSIF piv_codrol = 'CP'  /* Contactpersoon       */  THEN RETURN( 'CPN' ) ;
   ELSIF piv_codrol = 'AK'  /* Administratiekantoor */  THEN RETURN( 'AKR' ) ;
   ELSIF piv_codrol = 'CR'  /* Curator              */  THEN RETURN( 'CRR' ) ;
   ELSIF piv_codrol = 'BW'  /* Bewindvoerder        */  THEN RETURN( 'BWR' ) ;
   ELSIF piv_codrol = 'AC'  /* Accountant           */  THEN RETURN( 'ACT' ) ;
   ELSIF piv_codrol = 'GE'  /* Gemeente             */  THEN RETURN( 'GME' ) ;
   ELSIF piv_codrol = 'HC'  /* Handelscrediteuren   */  THEN RETURN( 'CRE' ) ;
   ELSIF piv_codrol = 'HD'  /* Handelsdebiteuren    */  THEN RETURN( 'DEB' ) ;
   ELSIF piv_codrol = 'AD'  /* ARBO Dienst          */  THEN RETURN( 'ABD' ) ;
   ELSIF piv_codrol = 'BA'  /* Bank                 */  THEN RETURN( 'BNK' ) ;
   ELSIF piv_codrol = 'BO'  /* Branche organisatie  */  THEN RETURN( 'BOR' ) ;
   ELSIF piv_codrol = 'VR'  /* Verzekeraar          */  THEN RETURN( 'VZR' ) ;
   ELSIF piv_codrol = 'BS'  /* Bestuurder           */  THEN RETURN( 'BSR' ) ;
   ELSIF piv_codrol = 'WN'  /* Deelnemer            */  THEN RETURN( 'PRS' ) ;
   ELSIF piv_codrol = 'BG'  /* Begunstigde          */  THEN RETURN( 'PRS' ) ;
   ELSIF piv_codrol = 'FO'  /* Fonds/opdrachtgever  */  THEN RETURN( 'FOG' ) ;
   ELSIF piv_codrol = 'DW'  /* Deurwaarder          */  THEN RETURN( 'DWR' ) ;
   ELSIF piv_codrol = 'BV'  /* Bedrijfsvereniging   */  THEN RETURN( 'BVG' ) ;
   ELSIF piv_codrol = 'KK'  /* Kamer van koophandel */  THEN RETURN( 'KVK' ) ;
   ELSIF piv_codrol = 'VK'  /* Verzekeringskamer    */  THEN RETURN( 'VKR' ) ;
   ELSIF piv_codrol = 'UI'  /* Uitkeringsinstantie  */  THEN RETURN( 'UKI' ) ;
   ELSIF piv_codrol = 'VN'  /* Verzekeringsnemer    */  THEN RETURN( 'BOVAG' );
   END IF ;

   RETURN( NULL ) ;
END;
/