CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_BR_MOD0003
 (P_SOORT_ADRES IN varchar2
 ,P_STRAAT IN varchar2
 ,P_MESSAGE_CODE OUT varchar2
 ,P_MESSAGE_TEKST OUT varchar2
 ,P_BUSINESS_RULE_CODE OUT varchar2
 )
 RETURN BOOLEAN
 IS

/******************************************************************************
   NAME:       rle_chk_br_mod0003
   PURPOSE:    Statutaire Zetel kan geen postbus of antwoordnummer zijn.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        01-11-2007              PLI  Created this function.

   NOTES:
   Check op postbus of antwoordnummer bij een Statutaire Zetel.
******************************************************************************/

t_result boolean:= TRUE;
--
--
begin
  --
  if (p_soort_adres = 'SZ' and ((lower(trim(p_straat)) like ('antwoord%')) or (lower(trim(p_straat)) like ('postbus%'))))
  then
    --
    t_result := FALSE;
    --
    p_message_code       := 'AVG-10';
    p_message_tekst      := 'Postbus of antwoordnummer is alleen toegestaan bij een correspondentieadres.';
    p_business_rule_code := 'BRMOD0003';
    --
  end if;
  --
  return t_result;
  --
exception
  when others
  then
    --
    p_message_code           := SQLCODE;
    p_message_tekst          := 'Fout bij controle op postbus / antwoordnummer: '||sqlerrm;
    p_business_rule_code     := 'BRMOD0003';
    return FALSE;
    --
END RLE_CHK_BR_MOD0003;
 
/