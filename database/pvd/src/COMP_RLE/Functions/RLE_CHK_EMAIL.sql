CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_EMAIL
 (P_EMAIL_ADRES IN varchar2
 )
 RETURN VARCHAR2
 IS

  cursor c_mail ( b_email_adres rle_communicatie_nummers.numcommunicatie%type )
  is
    select instr ( substr ( b_email_adres
                          , instr ( b_email_adres, '@' )
                          , 100 )
                 , '.' )  check_punt
           , instr ( b_email_adres, '@' ) check_apenstaartje
    from   dual;

  r_mail              c_mail%rowtype;
begin
  open c_mail ( p_email_adres );

  fetch c_mail
  into   r_mail;

  close c_mail;

  if r_mail.check_apenstaartje = 0
     or r_mail.check_punt = 0
  then
    return 'N';
  else
    return 'J';
  end if;
end rle_chk_email;
 
/