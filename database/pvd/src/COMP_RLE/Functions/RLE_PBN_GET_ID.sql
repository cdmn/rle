CREATE OR REPLACE FUNCTION comp_rle.RLE_PBN_GET_ID
 (P_SELMAAND IN VARCHAR2
 ,P_SOORT IN VARCHAR2
 )
 RETURN NUMBER
 IS
-- Sub-Program Unit Declarations
/* Ophalen postcoderunbatch gegevens op selektiemaand */
CURSOR C_PBN_001
 (B_SELMAAND IN VARCHAR2
 ,B_SOORT IN VARCHAR2
 )
 IS
/* C_PBN_001 */
select *
from rle_pcod_batchrun pbn
where pbn.draaimaand = b_selmaand
  and pbn.subtype = b_soort
;
-- Program Data
R_PBN_001 C_PBN_001%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_PBN_GET_ID */
open  c_pbn_001(nvl(p_selmaand,to_char(sysdate,'MM-YYYY')),p_soort);
fetch c_pbn_001 into r_pbn_001;
close c_pbn_001;
return r_pbn_001.id;
END RLE_PBN_GET_ID;

 
/