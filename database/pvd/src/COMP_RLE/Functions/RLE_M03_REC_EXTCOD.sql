CREATE OR REPLACE FUNCTION comp_rle.RLE_M03_REC_EXTCOD
 (P_SYSTEEMCOMP IN varchar2
 ,P_RELNUM IN number
 ,P_CODROL IN varchar2
 ,P_DATPARM IN DATE
 )
 RETURN VARCHAR2
 IS

/* Ophalen van relatie-externe_codes op rowid */
CURSOR C_REC_004
 (B_RELNUM IN NUMBER
 ,B_SYSTEEMCOMP IN VARCHAR2
 ,B_CODROL IN VARCHAR2
 ,B_DATPARM IN DATE
 )
 IS
/* C_REC_004 */
--
-- 01-07-2003, VER, nav call 86896
-- trunc's toegevoegd bij rec.datingang en nvl(b_datparm, sysdate) ivm zoeken op datum ipv datum-tijd
-- 14-11-2006 JCI bevinding 1910. Bovenstaande trunc is verdwenen op de omgevingen en meer trunc's toegevoegd

SELECT *
FROM 	 rle_relatie_externe_codes rec
WHERE  rec.rle_numrelatie     =  b_relnum
AND    rec.dwe_wrddom_extsys  =  b_systeemcomp
AND   (   b_codrol            IS NULL
       OR rec.rol_codrol      = b_codrol
      )
AND   trunc(rec.datingang)   <= trunc(NVL(b_datparm, SYSDATE))
AND  (   trunc(rec.dateinde) >= trunc(NVL(b_datparm, SYSDATE))
      OR rec.dateinde         IS NULL);

R_REC_004 C_REC_004%ROWTYPE;
L_CODROL VARCHAR2(3);
/* RLE_M03_REC_EXTCOD */
BEGIN
l_codrol := p_codrol;

OPEN	c_rec_004(p_relnum
,	p_systeemcomp
,	l_codrol
,	p_datparm);
FETCH	c_rec_004 INTO r_rec_004;
CLOSE c_rec_004;
RETURN r_rec_004.extern_relatie;
END RLE_M03_REC_EXTCOD;
 
/