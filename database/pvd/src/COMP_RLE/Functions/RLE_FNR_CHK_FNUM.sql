CREATE OR REPLACE FUNCTION comp_rle.RLE_FNR_CHK_FNUM
 (P_ROL_CODROL IN VARCHAR2
 ,P_NUMRELATIE IN NUMBER
 ,P_SRTREKENING IN VARCHAR2
 ,P_NUMREKENING IN VARCHAR2
 ,P_PEILDATUM IN DATE
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* controle of een  fnum voorkomt */
CURSOR C_FNR_012
 (B_NUMRELATIE NUMBER
 ,B_NUMREKENING VARCHAR2
 ,B_SRTREKENING VARCHAR2
 ,B_CODROL VARCHAR2
 ,B_PEILDATUM DATE
 )
 IS
/* C_FNR_012 */
Select * from rle_financieel_nummers fnr
where fnr.rle_numrelatie 	= b_numrelatie
and   fnr.numrekening  		= b_numrekening
and   fnr.dwe_wrddom_srtrek 	= b_srtrekening
and   (nvl(fnr.rol_codrol,'@') = nvl(b_codrol,'@')
		or fnr.rol_codrol 	is null)
and   fnr.datingang 				< nvl(b_peildatum,(b_peildatum + 1))
and  (fnr.dateinde  				> b_peildatum
 or   fnr.dateinde  				is null)
;
-- Program Data
R_FNR_012 C_FNR_012%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_FNR_CHK_FNUM */
open c_fnr_012(p_numrelatie
			 ,p_numrekening
			 ,p_srtrekening
			 ,p_rol_codrol
			 ,p_peildatum);
fetch c_fnr_012 into r_fnr_012;
if c_fnr_012%found then
	close c_fnr_012;
	return 'J';
else
	close c_fnr_012;
	return 'N';
end if;
END RLE_FNR_CHK_FNUM;

 
/