CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_NAMRLE_ADSNR
 (P_NAMRELATIE IN RLE_RELATIES.NAMRELATIE%TYPE
 ,P_NUMADRES IN RLE_RELATIE_ADRESSEN.ADS_NUMADRES%TYPE
 ,P_CODROL IN RLE_RELATIE_ADRESSEN.ROL_CODROL%TYPE
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Selecteer adres adhv NAMRELATIE, NUMADRES en ROL_CODROL */
CURSOR C_RAS_028
 (B_NAMRELATIE IN RLE_RELATIES.NAMRELATIE%TYPE
 ,B_NUMADRES IN RLE_RELATIE_ADRESSEN.ADS_NUMADRES%TYPE
 ,B_ROL_CODROL IN RLE_RELATIE_ADRESSEN.ROL_CODROL%TYPE
 )
 IS
/* C_RAS_028 */
SELECT null
FROM   rle_relatie_adressen  ras
,      rle_relaties          rle
WHERE  ras.rle_numrelatie         = rle.numrelatie
AND    rle.namrelatie             = b_namrelatie
AND    ras.ads_numadres           = b_numadres
AND    nvl( ras.rol_codrol, '@' ) = nvl( b_rol_codrol, '@' )
;
-- Program Data
R_RAS_028 C_RAS_028%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_CHK_NUMRLE_ADSNR: Start */
   OPEN c_ras_028( p_namrelatie, p_numadres, p_codrol ) ;
   FETCH c_ras_028 INTO r_ras_028 ;
   IF c_ras_028%FOUND
   THEN
      CLOSE c_ras_028 ;
      RETURN( 'J' ) ;
   ELSE
      CLOSE c_ras_028 ;
      RETURN( 'N' ) ;
   END IF ;
   /* RLE_CHK_NUMRLE_ADSNR: End */
END RLE_CHK_NAMRLE_ADSNR;

 
/