CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_BEDRIJFS_VERLEDEN
 (P_WERKGEVER IN VARCHAR2
 )
 RETURN VARCHAR2
 IS

CURSOR C_RKO_001
 (B_WERKGEVER IN RLE_RELATIE_EXTERNE_CODES.EXTERN_RELATIE%TYPE
 )
 IS
select ''
From rle_relatie_koppelingen   rko
,    rle_relatie_externe_codes rco
where rkg_id in (10,13,14,15,16,17,19,20)
and rco.extern_relatie = b_werkgever
and rco.rle_numrelatie = rko.rle_numrelatie
;
BEGIN
  open c_rko_001(p_werkgever);
  if c_rko_001% found
  then
    return 'TRUE';
  else
    return 'FALSE';
  end if;
  close c_rko_001;
END RLE_CHK_BEDRIJFS_VERLEDEN;
 
/