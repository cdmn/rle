CREATE OR REPLACE FUNCTION comp_rle.FNC_BEPAAL_VOORLETTERS
 (PIV_VOORLETTERS IN VARCHAR2
 )
 RETURN VARCHAR2 DETERMINISTIC
 IS
BEGIN
      IF INSTR( piv_voorletters, '.' ) = 0  AND
         INSTR( piv_voorletters, ' ' ) = 0
      THEN
         -- Geen puntjes en spaties. Dus zelf puntjes toevoegen tussen alle letters.
         RETURN( LTRIM( SUBSTR( piv_voorletters,  1, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  2, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  3, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  4, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  5, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  6, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  7, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  8, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  9, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters, 10, 1 ) || '.', '.' )
               ) ;
      ELSIF INSTR( piv_voorletters, '.' ) =  0  AND
            INSTR( piv_voorletters, ' ' ) != 0
      THEN
         -- Geen puntjes, wel spaties. Dus spaties vervangen door puntjes.
         RETURN( REPLACE( piv_voorletters, ' ', '.' ) ) ;
      ELSE
         -- Puntjes en spaties aanwezig. Gewoon zo laten.
         RETURN( piv_voorletters ) ;
      END IF ;
      RETURN( NULL ) ;
END;
 
/