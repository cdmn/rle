CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_BRANCHE_ORGANISATIES
 (P_RLE_NUMRELATIE IN NUMBER
 )
 RETURN VARCHAR2
 IS
  cursor c_rle (b_rle_numrelatie in rle_relatie_koppelingen.rle_numrelatie%type)
  is
  select ltrim(sys_connect_by_path ( extern_relatie, ', '),', ')
  from (  select rec.extern_relatie
          ,      rkn.*
          ,      row_number() over (partition by rkn.rle_numrelatie
                                    order     by rkn.rle_numrelatie_voor
                                   ) rn
          from rle_relatie_koppelingen rkn
          ,    rle_relatie_externe_codes rec
          ,    rle_rol_in_koppelingen rkg
          where rec.rle_numrelatie = rkn.rle_numrelatie_voor
          and   rkn.rkg_id = rkg.id
          and rec.dwe_wrddom_extsys = 'BOR'
          and sysdate between rkn.dat_begin
                          and nvl(rkn.dat_einde, sysdate)
          and  rkg.code_rol_in_koppeling = 'WB'
          and rkn.rle_numrelatie = b_rle_numrelatie
       )
  where connect_by_isleaf = 1
  start with rn = 1
  connect by rn = prior rn + 1
  and rle_numrelatie = prior rle_numrelatie
  ;

  l_branche_organisatie varchar2(200);
begin
   l_branche_organisatie := null;

   open c_rle ( b_rle_numrelatie => p_rle_numrelatie);
   fetch c_rle into l_branche_organisatie;
   close c_rle;

  return(l_branche_organisatie);
end rle_get_branche_organisaties;
 
/