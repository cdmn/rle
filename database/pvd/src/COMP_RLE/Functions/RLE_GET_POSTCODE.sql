CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_POSTCODE
 (P_NUMRELATIE IN NUMBER
 ,P_SRTADRES IN varchar2
 ,P_DATUMP IN date
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Ophalen postcode */
CURSOR C_RAS_023
 (B_NUMRELATIE IN NUMBER
 ,B_SRT_ADRES IN VARCHAR2
 ,B_PEILDATUM IN DATE
 )
 IS
/* C_RAS_023 */
select decode(ads.lnd_codland
             ,'NL',ads.postcode
             ,'0999') postcode
  ,      sign(b_peildatum-NVL(ras.dateinde
                             ,to_date('31-12-2999','dd-mm-rrrr')
                             )
             ) peil1
  ,      sign(b_peildatum-ras.datingang) peil2
  ,      abs(b_peildatum-ras.datingang) peil3
  from RLE_RELATIE_ADRESSEN ras
  ,    RLE_ADRESSEN ads
  where rle_numrelatie = b_numrelatie
  and ras.ads_numadres = ads.numadres
  and ras.dwe_wrddom_srtadr = b_srt_adres
  order by 2,3 desc, 4
;
-- Program Data
R_RAS_023 C_RAS_023%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_GET_POSTCODE */
open	c_ras_023(p_numrelatie
,	p_srtadres
,	p_datump);
fetch	c_ras_023 into r_ras_023;
close	c_ras_023;
return r_ras_023.postcode;
END RLE_GET_POSTCODE;

 
/