CREATE OR REPLACE FUNCTION comp_rle.RLE_LND_CHK
 (P_LND_COD IN rle_landen.codland%type
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_LND_002
 (B_CODLAND IN VARCHAR2
 )
 IS
/* C_LND_002 */
select	*
from	rle_landen lnd
where	lnd.codland = b_codland;
-- Program Data
R_LND_002 C_LND_002%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_LND_CHK */
open c_lnd_002(p_lnd_cod);
fetch c_lnd_002 into r_lnd_002;
	if c_lnd_002%found then
		close c_lnd_002;
		return 'J';
	else
		close c_lnd_002;
		return 'N';
	end if;
END RLE_LND_CHK;

 
/