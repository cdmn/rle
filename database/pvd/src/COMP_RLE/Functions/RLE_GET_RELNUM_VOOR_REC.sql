CREATE OR REPLACE function comp_rle.rle_get_relnum_voor_rec
  ( p_relext in varchar2
  , p_systeemcomp in varchar2
  , p_codrol in varchar2
  , p_datparm in date
  )
 return number
is
  -- Program Data
  l_codrol       varchar2(3);
  l_externe_code varchar2(20);
  l_wgrnr        number(20, 0);
  l_relatienummer number(10);
  -- PL/SQL Specification
  /* Ophalen van relatie-externe_codes */
  cursor c_rec
  (
    b_relext      in varchar2
   ,b_systeemcomp in varchar2
   ,b_codrol      in varchar2
   ,b_datparm     in date
  ) is
  /* C_REC */
    select rec.rle_numrelatie
    from   rle_relatie_externe_codes rec
    where  rec.extern_relatie = b_relext
    and    rec.dwe_wrddom_extsys = nvl(b_systeemcomp, rec.dwe_wrddom_extsys)
    and    rec.rol_codrol = nvl(b_codrol, rec.rol_codrol)
    and    trunc(rec.datingang) <= trunc(nvl(b_datparm, rec.datingang))
    and    (trunc(rec.dateinde) >= trunc(nvl(b_datparm, rec.datingang)) or rec.dateinde is null)
    order  by nvl(rec.rol_codrol, 'zzz');
  r_rec c_rec%rowtype;
  -- PL/SQL Block
begin
  /* xjb 30-10-2015 Dit is een kopie van de procedure RLE_M06_REC_RELNUM maar nu is het een function zodat
                    je makkelijker de functie kunt gebruiken in bijvoorbeeld Sqlplus. */
  l_codrol := p_codrol;
  if p_systeemcomp = 'WGR'
     or l_codrol = 'WG'
  then
    l_wgrnr        := to_number(p_relext);
    l_externe_code := ltrim(to_char(l_wgrnr, '099999'));
  elsif p_systeemcomp = 'SOFI'
  then
    /* sofinr wordt met voorloopnullen opgeslagen, maar wordt niet altijd zo ingevoerd */
    l_externe_code := ltrim(to_char(to_number(p_relext), '099999999'));
  else
    l_externe_code := p_relext;
  end if;
  stm_util.debug('RLE_GET_RELNUM_VOOR_REC: zoek externe code: ' || l_externe_code || ' syscomp: ' || p_systeemcomp);
  open c_rec(l_externe_code, p_systeemcomp, l_codrol, p_datparm);
  fetch c_rec
    into r_rec;
  l_relatienummer := nvl(r_rec.rle_numrelatie, l_relatienummer);
  if c_rec%notfound
  then
    l_relatienummer := null;
  end if;
  close c_rec;
  return l_relatienummer;
end rle_get_relnum_voor_rec;
/