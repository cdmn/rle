CREATE OR REPLACE FUNCTION comp_rle.RLE_ADS_SEQ1_NEXT_ID
 RETURN NUMBER
 IS
-- Program Data
L_NUMBER NUMBER;
-- PL/SQL Block
BEGIN
select rle_ads_seq1.nextval
  into   l_number
  from   sys.dual;
  return l_number;
END RLE_ADS_SEQ1_NEXT_ID;

 
/