CREATE OR REPLACE FUNCTION comp_rle.RLE_RLE_EXI
 (P_RELANUM IN NUMBER
 ,P_PEILDATUM IN DATE
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_021
 (B_NUMRELATIE IN NUMBER
 ,B_PEILDATUM IN DATE
 )
 IS
/* C_RLE_021 */
SELECT *
FROM   rle_relaties rle
WHERE  rle.numrelatie = b_numrelatie
AND    b_peildatum <= NVL( rle.dateinde, b_peildatum )
AND     (rle.indinstelling = 'N'
            AND  b_peildatum >=  rle.datgeboorte )
UNION ALL
SELECT *
FROM   rle_relaties rle
WHERE  rle.numrelatie = b_numrelatie
AND    b_peildatum <= NVL( rle.dateinde, b_peildatum )
AND      ( rle.indinstelling = 'J'
             AND  b_peildatum >=  rle.datbegin)
;
-- Program Data
R_RLE_021 C_RLE_021%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_RLE_EXI */
/* This function checks if there are details for the foreign key
     relation rle_relaties, that is, it checks if there
     are records in table rle_relaties which
     reference the record in an external system  as
     identified by the parameter(s). In that case it returns TRUE,
     otherwise FALSE. */
/* RWI: Wijziging: de functie moet ook controleren op de einddatum.
        Hiervoor is een nieuwe cursor C_RLE_021 aangemaakt.
*/
OPEN  c_rle_021( p_relanum, NVL(p_peildatum, SYSDATE)) ;
FETCH c_rle_021 INTO r_rle_021 ;
IF c_rle_021%FOUND
THEN
   CLOSE c_rle_021 ;
   RETURN( 'J' ) ;
ELSE
   CLOSE c_rle_021 ;
   RETURN( 'N' ) ;
END IF ;
END RLE_RLE_EXI;

 
/