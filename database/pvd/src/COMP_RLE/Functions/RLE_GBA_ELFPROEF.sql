CREATE OR REPLACE FUNCTION comp_rle.RLE_GBA_ELFPROEF
 (P_NUMMER IN NUMBER
 )
 RETURN BOOLEAN
 IS
-- GBA_ELFPROEF_A_NUMMER:  elfproef voor A-nummers

   l_som1       NUMBER(3) := 0;
   l_som2       NUMBER(6) := 0;
   l_cijfer1    NUMBER(1) := 0;
   l_cijfer2    NUMBER(1) := 0;
   t_rekeningnr NUMBER := p_nummer;
   l_factor     NUMBER(1) := 9;
   l_return     BOOLEAN := TRUE;
BEGIN
   WHILE t_rekeningnr > 0
   LOOP
      l_cijfer1   := MOD(t_rekeningnr,10);
      IF l_factor <> 9
      THEN
         IF l_cijfer1  = l_cijfer2
         THEN
            l_return := FALSE;
         END IF;
      END IF;
      l_som1       := l_som1 + MOD(t_rekeningnr,10);
      l_som2       := l_som2 + MOD(t_rekeningnr,10) * POWER(2,l_factor);
      l_factor     := l_factor - 1;
      t_rekeningnr := TRUNC(t_rekeningnr/10);
      l_cijfer2    := l_cijfer1;
   END LOOP;
   if mod(l_som1,11) not in (0,5) then
      l_return := FALSE;
   ELSIF MOD(l_som2,11) <> 0
   THEN
      l_return := FALSE;
   END IF;
   RETURN l_return;
END;
 
/