CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_BR_MOD0001
 (P_POSTCODE IN varchar2
 ,P_HUISNUMMER IN number
 ,P_MESSAGE_CODE OUT varchar2
 ,P_MESSAGE_TEKST OUT varchar2
 ,P_BUSINESS_RULE_CODE OUT varchar2
 )
 RETURN BOOLEAN
 IS

/******************************************************************************
   NAME:       rle_chk_br_mod0001
   PURPOSE:    Postcode en huisnummer

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        25-10-2007              PLI  Created this function.

   NOTES:
   Check postcode, huisnummer.
******************************************************************************/

t_result boolean:= TRUE;
--
/*
t_min_lf        number;
t_max_lf        number;
t_max_leeftijd  date;
*/
--
begin
  --
/*
  t_min_lf := stm_algm_get.sysparm('AVG', 'AVG-MIN-LF', SYSDATE);
  t_max_lf := stm_algm_get.sysparm('AVG', 'AVG-MAX-LF', SYSDATE);
  t_max_leeftijd := ADD_MONTHS( p_geboortedatum, t_max_lf * 12);
  t_max_leeftijd := TRUNC(t_max_leeftijd, 'MM');
  --
  if t_max_leeftijd < TO_DATE('01-01-1994', 'DD-MM-YYYY')
  then
    --
    t_max_leeftijd := ADD_MONTHS (t_max_leeftijd, 1);
    --
  end if;
  --
  if p_dat_begin <  ADD_MONTHS( p_geboortedatum, t_min_lf * 12)
     OR p_dat_begin >= t_max_leeftijd
     OR NVL( p_dat_einde, t_max_leeftijd-1) >=  t_max_leeftijd
  then
    --
    t_result := FALSE;
    --
    p_message_code       := 'AVG-3';
    p_message_tekst      := 'Leeftijd, behorende bij geboortedatum: '||p_geboortedatum||', ligt niet tussen '||t_min_lf||' en '||t_max_lf||' gedurende de arbeidsverhouding';
    p_business_rule_code := 'BRATT0004';
    --
  end if;
  --
*/
  return t_result;
  --
exception
  when others
  then
    --
    p_message_code           := SQLCODE;
    p_message_tekst          := 'Fout bij validatie van geboortedatum: '||sqlerrm;
    p_business_rule_code     := 'BRATT0004';
    return FALSE;
    --
END RLE_CHK_BR_MOD0001;
 
/