CREATE OR REPLACE FUNCTION comp_rle.FNC_MAAK_NAAM_OP
 (PIV_VOORLETTERS IN VARCHAR2
 ,PIV_VOORVOEGSEL IN VARCHAR2
 ,PIV_NAAM IN VARCHAR2
 ,PIV_OPMAAK_CODE IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
BEGIN
IF piv_opmaak_code = '1'
      THEN
         RETURN( RTRIM( RTRIM( piv_naam
                               || ', '
                               || LTRIM( LTRIM( piv_voorvoegsel
                                                || ', '
                                              , ','
                                              )
                                       )
                               || LTRIM( piv_voorletters
                                       )
                             )
                      , ','
                      )
               ) ;
      ELSIF piv_opmaak_code = '2'
      THEN
         RETURN( LTRIM( piv_voorletters || ' ' ) || LTRIM( piv_voorvoegsel || ' ' ) || piv_naam ) ;
      END IF ;
      RETURN( NULL ) ;
END FNC_MAAK_NAAM_OP;

 
/