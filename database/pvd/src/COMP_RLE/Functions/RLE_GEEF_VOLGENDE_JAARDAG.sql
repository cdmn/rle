CREATE OR REPLACE FUNCTION comp_rle.RLE_GEEF_VOLGENDE_JAARDAG
 (P_RELATIENUMMER IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_GEBOORTEDATUM IN DATE
 ,P_PEILDATUM IN DATE
 )
 RETURN DATE
 IS
 /**************************************************************************************
  Functienaam: RLE_GEEF_VOLGENDE_JAARDAG
  Beschrijving:
    Bepaal eerstvolgende jaardag

  Invoerparameters:
   Relatienummer, de geboortedatum wordt opgehaald in de database
  OF
   Willekeurige datum (welke de geboortedatum moet voorstellen)

  Restricties
   Niet beide opgeven.

  Overige bijzonderheden
    De peildatum is het referentiemoment,
    indien leeg dan wordt de systeemdatum aangehouden.

    Foutafhandeling igv overlijden bij gebruik functie voor meerdere relaties (loopen)
    zelf afhandelen of beperken tot relaties die nog niet overleden zijn.

    Personen geboren op 29-02 krijgen igv niet-schrikkeljaar 28-02

  Datum      Wie        Wat
  ---------- ---------- ---------------------------------------------------------------
  27-01-2011 DFU        Creatie
  03-05-2011 DFU        Afvangen personen geboren op 29-02
  03-10-2011 DFU        Igv geboortedag (DD) = peildag (DD) dan ook volgend jaar
                                       als eerstvolgende jaardag.
  ************************************************************************************* */


-- Sub-Program Unit Declarations
cn_module         constant varchar2 (400) := 'RLE_GEEF_VOLGENDE_JAARDAG';
cn_rol_persoon    constant varchar2(3)    := 'PR';
cn_extcod_persoon constant varchar2(4)    := 'PRS';

cursor c_rle ( b_numrelatie in rle_relaties.numrelatie%type
             , b_peildatum  in date
             )
is
select rec.extern_relatie        persoonsnummer
     , rle.datgeboorte           geboortedatum
     , rle.datoverlijden         overlijdensdatum
     , rle.dwe_wrddom_geslacht   geslacht
  from rle_relatie_externe_codes rec
     , rle_relaties              rle
 where rle.numrelatie        = rec.rle_numrelatie
   and rec.dwe_wrddom_extsys = cn_extcod_persoon
   and rec.rol_codrol        = cn_rol_persoon
   and rle.numrelatie        = b_numrelatie
   and b_peildatum          >= rle.datgeboorte
   and b_peildatum          < nvl(rle.datoverlijden, b_peildatum + 1)
   ;
--
r_rle c_rle%rowtype;
-- Sub-Program Units
l_geboortedatum        date;
l_geboortedatum_dag    number;
l_geboortedatum_maand  number;
l_geboortedatum_jaar   number;
--
l_peildatum            date;
l_peildatum_dag        number;
l_peildatum_maand      number;
l_peildatum_jaar       number;
--
l_volgende_jaardag     date;
-- Exception
e_teveel               exception;
e_te_weinig            exception;
e_geen_persoonsrelatie exception;
e_invalid_date         exception;
-- introducing ORA-01839 exception by name
pragma exception_init(e_invalid_date,-01839);

-- PL/SQL Block
BEGIN
   --
   stm_util.debug('********* START '||cn_module ||' **********');
   stm_util.debug('P_RELATIENUMMER = '||to_char(p_relatienummer));
   stm_util.debug('P_GEBOORTEDATUM = '||to_char(p_geboortedatum, 'dd-mm-yyyy'));
   stm_util.debug('P_PEILDATUM     = '||to_char(p_peildatum, 'dd-mm-yyyy'));
   --
   -- Controles
   --
   if  p_relatienummer is null
   and p_geboortedatum is null
   then
      raise e_te_weinig;
   end if;
   --
   if  p_relatienummer is not null
   and p_geboortedatum is not null
   then
      raise e_teveel;
   end if;
   --
   -- Peildatum gegevens
   --
   l_peildatum := nvl(p_peildatum,trunc(sysdate));
   --
   --stm_util.debug('l_peildatum       = '||to_char(l_peildatum, 'dd-mm-yyyy'));
   --
   l_peildatum_dag   := extract(day from l_peildatum);
   l_peildatum_maand := extract(month from l_peildatum);
   l_peildatum_jaar  := extract(year from l_peildatum);
   --
   --stm_util.debug('l_peildatum_dag   = '||to_char(l_peildatum_dag));
   --stm_util.debug('l_peildatum_maand = '||to_char(l_peildatum_maand));
   --stm_util.debug('l_peildatum_jaar  = '||to_char(l_peildatum_jaar));
   --
   if p_geboortedatum is not null
   then
     --
     l_geboortedatum := p_geboortedatum;
     --
   end if;--p_geboortedatum is not null
   --
   if p_relatienummer is not null
   then
     --
     open c_rle ( b_numrelatie => p_relatienummer
                , b_peildatum  => l_peildatum
                );
     fetch c_rle into r_rle;
     --
     if c_rle%found
     then
       --
       -- Niet overleden persoonsrelatie gevonden
       l_geboortedatum := r_rle.geboortedatum;
       --
     else
       --
       raise e_geen_persoonsrelatie;
       --
     end if;
     --
     close c_rle;
     --
   end if;--p_relatienummer is not null
   --
   --stm_util.debug('l_geboortedatum = '||to_char(l_geboortedatum, 'dd-mm-yyyy'));
   --
   -- Geboortedatum gegevens
   --
   l_geboortedatum_dag   := extract(day from l_geboortedatum);
   l_geboortedatum_maand := extract(month from l_geboortedatum);
   l_geboortedatum_jaar  := extract(year from l_geboortedatum);
   --
   --stm_util.debug('l_geboortedatum_dag   = '||to_char(l_geboortedatum_dag));
   --stm_util.debug('l_geboortedatum_maand = '||to_char(l_geboortedatum_maand));
   --stm_util.debug('l_geboortedatum_jaar  = '||to_char(l_geboortedatum_jaar));
   --
   if  l_peildatum is not null
   and l_geboortedatum is not null
   then
     --
     -- Indien de geboortemaand in de toekomst ligt
     -- tov de peilmaand dan is de eerstvolgende
     -- jaardag nog in dat peiljaar anders volgend peiljaar
     --
     if l_geboortedatum_maand >= l_peildatum_maand
     then
       --
       l_peildatum_jaar := l_peildatum_jaar;
       --
       --stm_util.debug('l_peildatum_jaar = '||to_char(l_peildatum_jaar));
       --
       -- Indien de geboortemaand de peilmaand is
       -- maar de dag is al verstreken dan ligt de
       -- eerstvolgende jaardag ook in volgend peiljaar
       --
       if  l_geboortedatum_dag  <= l_peildatum_dag
       and l_geboortedatum_maand = l_peildatum_maand
       then
         --
         l_peildatum_jaar := l_peildatum_jaar + 1;
         --
         --stm_util.debug('l_peildatum_jaar = '||to_char(l_peildatum_jaar));
         --
       end if;
       --
     else
       --
       l_peildatum_jaar := l_peildatum_jaar + 1;
       --
       --stm_util.debug('l_peildatum_jaar = '||to_char(l_peildatum_jaar));
       --
     end if;
     --
     -- samenstellen volgende jaardag
     --
     begin
        l_volgende_jaardag := to_date (  to_char(l_geboortedatum_dag)
                                       ||'-'
                                       ||to_char(l_geboortedatum_maand)
                                       ||'-'
                                       ||to_char(l_peildatum_jaar)
                                        , 'dd-mm-yyyy');
     exception
     -- uitzondering personen geboren op 29-02 afvangen in exception
     -- deze worden dan teruggezet naar 28-02
     when e_invalid_date
     then
       --
       stm_util.debug('Exception invalid_date');
       if  l_geboortedatum_dag   = 29
       and l_geboortedatum_maand = 2
       then
         stm_util.debug('Terugzetten igv 29-02 naar 28-02');
         l_geboortedatum_dag := l_geboortedatum_dag - 1;
         l_volgende_jaardag  := to_date (  to_char(l_geboortedatum_dag)
                                         ||'-'
                                         ||to_char(l_geboortedatum_maand)
                                         ||'-'
                                         ||to_char(l_peildatum_jaar)
                                         , 'dd-mm-yyyy');
       end if;
       --
     end;
     --
   end if;--l_peildatum is not null and l_geboortedatum is not null
   --
   stm_util.debug('Volgende Jaardag = '||to_char(l_volgende_jaardag, 'dd-mm-yyyy'));
   stm_util.debug('********* EINDE '||cn_module ||' **********');
   --
   return l_volgende_jaardag;
   --
exception
   when e_teveel
   then
      raise_application_error(-20000,'RET-000 Teveel parameters meegegeven');
   when e_te_weinig
   then
      raise_application_error(-20000,'RET-000 Te weinig parameters meegegeven');
   when e_geen_persoonsrelatie
   then
    --
    if c_rle%isopen then close c_rle; end if;
    --
    raise_application_error
      (-20000, stm_app_error (sqlerrm, cn_module, null)
             ||' R'||p_relatienummer||' , is geen persoonsrelatie of overleden tov gewenste peildatum = '
             ||to_char(l_peildatum, 'dd-mm-yyyy')||' in de database '
             ||dbms_utility.format_error_backtrace );
   --
   when others
   then
    --
    if c_rle%isopen then close c_rle; end if;
    --
    raise_application_error
      (-20000, stm_app_error (sqlerrm, cn_module, null)|| ' ' ||
               dbms_utility.format_error_backtrace );
  --
END RLE_GEEF_VOLGENDE_JAARDAG;
 
/