CREATE OR REPLACE FUNCTION comp_rle.RLE_UNIEK_ADRES
 (P_LND_CODLAND IN VARCHAR2
 ,P_POSTCODE IN VARCHAR2
 ,P_HUISNUMMER IN NUMBER
 ,P_TOEVHUISNUM IN VARCHAR2
 )
 RETURN varchar2
 DETERMINISTIC
 IS
BEGIN
if p_postcode is null
	or p_huisnummer is null
	or p_lnd_codland <> 'NL'  then
	  return null ;
	end if ;
    return p_lnd_codland||p_postcode||p_huisnummer||p_toevhuisnum ;
END RLE_UNIEK_ADRES;

 
/