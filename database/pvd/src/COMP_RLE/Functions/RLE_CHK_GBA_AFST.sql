CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_GBA_AFST
 (P_RLE_NUMRELATIE NUMBER
 ,P_PERSOONSNUMMER NUMBER
 )
 RETURN VARCHAR2
 IS
  /******************************************************************************************************
   Naam:         RLE_CHK_GBA_AFST
   Beschrijving: Functie om te bepalen of er met het GBA afgestemd MAG worden (return J of N)
                 Deze functie werkt op basis van het relatienummer. Indien i.p.v. het relatienummer
                 een persoonsnummer wordt meegegeven, dan wordt dus eerst het bijbehorende relatienummer
                 afgeleid.

   Datum      Wie        Wat
   ---------- ---------- ---------------------------------------------------
   16-01-2008 HSI        Nieuwbouw
   25-01-2008 PAS        Nu ook aanroep mogelijk op basis van persoonsnummer
   16-07-2008 XAA        Controle op ALG weggehaald.
  ******************************************************************************************************/
  -- Cursor c_rie
  -- Bepaal of er een administratiebestaat met indicator op J
  --
  Cursor c_rie ( b_rle_numrelatie  in number )
  is
    select 'J'
    from   rle_relatierollen_in_adm  rie
    ,      rle_administraties        adm
    where  rie.rle_numrelatie = b_rle_numrelatie
    and    adm.code           = rie.adm_code
    and    adm.ind_afstemmen_gba_tgstn = 'J'
  ;
  --
  l_bestaat                 varchar2(1);
  l_ind_afstemmen_gba_tgstn varchar2(1);
  l_relatienummer           rle_relaties.numrelatie%type;
  --
begin
  --
  if ( p_rle_numrelatie is null and p_persoonsnummer is null )
  then
    -- Als de parameters leeg zijn, dan maar afstemmen, administratie onbekend
    return 'J';
    --
  elsif ( p_rle_numrelatie is null and p_persoonsnummer is not null )
  then
    -- omzetten persoonsnummer naar relatienummer
    get_ext_rle( p_relext      =>  p_persoonsnummer
               , p_systeemcomp => 'PRS'
               , p_codrol      => 'PR'
               , p_datparm     => sysdate
               , p_o_relnum    => l_relatienummer
               );
  else
    l_relatienummer := p_rle_numrelatie;
  end if;
    --
    -- Controleer of er een rij bestaat met indicator afstemmen op J
    --
    l_ind_afstemmen_gba_tgstn := 'N';
    open  c_rie ( b_rle_numrelatie => l_relatienummer);
    fetch c_rie
    into  l_ind_afstemmen_gba_tgstn;
    close c_rie;
    --
    if   l_ind_afstemmen_gba_tgstn = 'J'   -- In de tabel komt er een administratie voor met indicator op J
    then
      return 'J';
    else
      --
      -- Er is GEEN J gevonden, dus NIET AFSTEMMEN
      --
      return 'N';
    end if;
  --
END RLE_CHK_GBA_AFST;
 
/