CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_BR_IER0037
 (P_SOFINUMMER IN number
 ,P_MESSAGE_CODE OUT varchar2
 ,P_MESSAGE_TEKST OUT varchar2
 ,P_BUSINESS_RULE_CODE OUT varchar2
 )
 RETURN BOOLEAN
 IS

/******************************************************************************
   NAME:       avg_chk_br_ier0037
   PURPOSE:    Sofinummer

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        25-10-2007              RIO  Created this function.

   NOTES:
   Sofinummer moet voldoen aan bepaalde checks.
******************************************************************************/

--
t_result   boolean:= TRUE;
t_nietgoed boolean:= FALSE;
--
begin
  --
  if (p_sofinummer is not null)
  then
    --
	BEGIN
	  --
	  STM_ALGM_CHECK.ELF_PROEF_SOFI(p_sofinummer);
	  t_result := TRUE;
      --
    exception
	when others
	then
      --
      t_result := FALSE;
      --
      p_message_code       := 'AVG-11';
      p_message_tekst      := 'Sofinummer: '||p_sofinummer||', voldoet niet aan de eisen.';
      p_business_rule_code := 'BRATT0004';
      --
    end;
	--
  end if;
  --
  return t_result;
  --
exception
  when others
  then
    --
    p_message_code           := SQLCODE;
    p_message_tekst          := 'Fout bij validatie van sofinummer: '||sqlerrm;
    p_business_rule_code     := 'BRATT0004';
    return FALSE;
    --
END RLE_CHK_BR_IER0037;
 
/