CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_TEL_TIEN
 (P_TELEFOONNUMMER IN varchar2
 ,P_MESSAGE_CODE OUT varchar2
 ,P_MESSAGE_TEKST OUT varchar2
 ,P_BUSINESS_RULE_CODE OUT varchar2
 )
 RETURN BOOLEAN
 IS


/******************************************************************************
   NAME:       rle_chk_tel_tien
   PURPOSE:    Telefoonnummer moet tenminste 10 cijfers hebben.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        06-03-2008              RIO  Created this function.
******************************************************************************/

t_result boolean:= TRUE;
--
begin
  --
  if (length(P_TELEFOONNUMMER) < 10)
  then
    --
    t_result := FALSE;
    --
    p_message_code       := 'RLE-01';
    p_message_tekst      := 'Het telefoonnummer bevat minder dan 10 cijfers.';
    p_business_rule_code := 'rle_chk_tel_tien';
    --
  end if;
  --
  return t_result;
  --
exception
  when others
  then
    --
    p_message_code           := SQLCODE;
    p_message_tekst          := 'Fout bij validatie van telefoonnummer: '||sqlerrm;
    p_business_rule_code     := 'TPLZTE01';
    return FALSE;
    --
END RLE_CHK_TEL_TIEN;
 
/