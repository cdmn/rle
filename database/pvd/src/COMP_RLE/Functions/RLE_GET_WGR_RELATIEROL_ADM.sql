CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_WGR_RELATIEROL_ADM
 (P_WGR_ID IN NUMBER
 )
 RETURN VARCHAR2
 IS
  l_adm_code   varchar2( 1000 );

  cursor c_adm(
    b_wgr_id   number
  )
  is
    select   adm_code
    from     rle_relatierollen_in_adm rie
           , rle_administraties adm
    where    adm.code = rie.adm_code
    and      rle_numrelatie in( select nr_relatie
                               from   wgr_werkgevers
                               where  id = b_wgr_id )
    order by decode( adm.type
                   , 'P', 1
                   , 'V', 2
                   , 3
                   )
           , rie.dat_creatie desc;

  r_adm        c_adm%rowtype;
begin
  open c_adm( p_wgr_id );

  fetch c_adm
  into  r_adm;

  if c_adm%found
  then
    close c_adm;

    l_adm_code := r_adm.adm_code;
  else
    close c_adm;
  end if;

  return l_adm_code;
END RLE_GET_WGR_RELATIEROL_ADM;
 
/