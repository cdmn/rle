CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_AANHEF
 (P_NUMRELATIE NUMBER
 )
 RETURN VARCHAR2
 IS

  /* ************************************************************************ */
  /* functie voor het ophalen van het aanhef van een relatie                  */
  /*                                                                          */
  /* Datum      Wie               Wat                                         */
  /* --------   ---------------- -------------------------------------------  */
  /* 14-11-2011 Rudie Siwpersad   Nieuwbouw; nodig voor het vullen van mv_s90 */
  /* **************************************************************************/
-- variabelen
  l_aanhef             varchar2(200):= null;
  l_aansp_titel        varchar2(200);
  l_aansp_titel_klein  varchar2(200);
begin
   comp_rle.rle_rle_get_aanhef( p_numrelatie    => p_numrelatie
                                                          ,  p_aanhef           => l_aanhef
                                                          ,  p_aansp_titel   => l_aansp_titel
                                                          , p_aansp_titel_klein => l_aansp_titel_klein
                                                           );
   --
   return l_aanhef;
   --
exception
   when others
   then
     return l_aanhef;
end rle_get_aanhef;
 
/