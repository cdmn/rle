CREATE OR REPLACE FUNCTION comp_rle.RLE_RAS_CHK_AANWEZIG
 (P_RAS IN rle_relatie_adressen%rowtype
 )
 RETURN NUMBER
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_RAS_010
 (B_RAS_ID IN NUMBER
 ,B_NUMRELATIE IN NUMBER
 ,B_SRTADRES IN VARCHAR2
 ,B_DATINGANG IN DATE
 ,B_DATEINDE IN DATE
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_RAS_010 */
  select	*
  from	rle_relatie_adressen ras
  where	ras.id <> b_ras_id
  and	ras.rle_numrelatie = b_numrelatie
  and	ras.dwe_wrddom_srtadr = b_srtadres
  and	(ras.rol_codrol is NULL or ras.rol_codrol = b_codrol)
  and	STM_ALGM_CHECK.DATOVLP(b_datingang
  	,	b_dateinde
  	,	ras.datingang
  	,	ras.dateinde) = 'J';
/* Ophalen van relatie-adres */
CURSOR C_RAS_011
 (B_RAS_ID IN NUMBER
 ,B_NUMRELATIE IN NUMBER
 ,B_SRTADRES IN VARCHAR2
 ,B_DATINGANG IN DATE
 ,B_DATEINDE IN DATE
 )
 IS
/* C_RAS_011 */
  select	*
  from	rle_relatie_adressen ras
  where	ras.id <> b_ras_id
  and	ras.rle_numrelatie = b_numrelatie
  and	ras.dwe_wrddom_srtadr = b_srtadres
  and	STM_ALGM_CHECK.DATOVLP(b_datingang
  	,	b_dateinde
  	,	ras.datingang
  	,	ras.dateinde) = 'J';
-- Program Data
L_AKTIE NUMBER(1);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_RAS_CHK_AANWEZIG */
  /* Controleren of de relatie al een andere ras in dezelfde periode
     heeft.
  */
  l_aktie := 1;
  if	p_ras.rol_codrol is not null then
  	for r_ras_010 in c_ras_010(p_ras.id
  	,	p_ras.rle_numrelatie
  	,	p_ras.dwe_wrddom_srtadr
  	,	p_ras.datingang
  	,	p_ras.dateinde
  	,	p_ras.rol_codrol) loop
  		if	r_ras_010.rol_codrol is NULL then
  			l_aktie := 2;
  			return l_aktie;
  		else
  			if	r_ras_010.dateinde is not NULL then
 			/*   ras in dezelfde periode, met een einddatum in de
 			   toekomst
 			*/
  				l_aktie := 2;
  				return l_aktie;
  			else
            		 /*  ras in dezelfde periode */
  				l_aktie := 3;
  				return l_aktie;
  			end if;
  		end if;
  	end loop;
  end if;
  if	p_ras.rol_codrol is NULL then
  	for r_ras_011 in c_ras_011(p_ras.id
  	,	p_ras.rle_numrelatie
  	,	p_ras.dwe_wrddom_srtadr
  	,	p_ras.datingang
  	,	p_ras.dateinde) loop
  		if	r_ras_011.rol_codrol is not NULL then
  			l_aktie := 2;
  			return l_aktie;
  		else
  			if	r_ras_011.dateinde is not NULL then
  				l_aktie := 2;
  				return l_aktie;
  			else
  				l_aktie := 3;
  				return l_aktie;
  			end if;
  		end if;
  	end loop;
  end if;
  return l_aktie;
END RLE_RAS_CHK_AANWEZIG;

 
/