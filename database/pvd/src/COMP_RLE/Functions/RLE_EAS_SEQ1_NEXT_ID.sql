CREATE OR REPLACE FUNCTION comp_rle.RLE_EAS_SEQ1_NEXT_ID
 RETURN NUMBER
 IS

L_NUMBER NUMBER;
BEGIN
select rle_eas_seq1.nextval
  into   l_number
  from   sys.dual;
  return l_number;
END RLE_EAS_SEQ1_NEXT_ID;
 
/