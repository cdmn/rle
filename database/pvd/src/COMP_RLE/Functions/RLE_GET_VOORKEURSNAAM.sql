CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_VOORKEURSNAAM
 (P_NAAM IN VARCHAR2 := null
 ,P_VOORVOEGSELS IN VARCHAR2 := null
 ,P_NAAM_PARTNER IN VARCHAR2 := null
 ,P_VOORVOEGSELS_PARTNER IN VARCHAR2 := null
 ,P_CODE_NAAMGEBRUIK IN VARCHAR2 := null
 )
 RETURN VARCHAR2
 IS
-- PL/SQL Specification
l_voorkeursnaam varchar2(105);
-- PL/SQL Block
BEGIN
if p_naam_partner is null
       or p_code_naamgebruik = 'E'
    then
        l_voorkeursnaam := p_naam;
    elsif p_code_naamgebruik = 'P'
    then
      l_voorkeursnaam := p_naam_partner;
    elsif p_code_naamgebruik = 'V'
    then
      l_voorkeursnaam := p_naam_partner||'-'||ltrim(p_voorvoegsels||' '||p_naam);
    elsif p_code_naamgebruik = 'N'
    then
      l_voorkeursnaam := p_naam||'-'||ltrim(p_voorvoegsels_partner||' '||p_naam_partner);
    end if;
    --
    return l_voorkeursnaam;
END RLE_GET_VOORKEURSNAAM;

 
/