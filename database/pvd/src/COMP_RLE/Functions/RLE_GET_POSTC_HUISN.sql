CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_POSTC_HUISN
 (P_NUMRELATIE IN NUMBER
 ,P_CODROL IN VARCHAR2
 ,P_SRTADRES IN VARCHAR2
 ,P_DATUM IN DATE
 )
 RETURN VARCHAR2
 IS
/******************************************************************************************************
Naam:         RLE_GET_POSTC_HUISN
Beschrijving: Ophalen postcode en huisnummer. Toegevoegd om performance redenen. Deze procedure mag
              niet zomaar gebruikt worden omdat niet wordt gecontroleerd of het DA adres mag
              worden gebruikt. Zie RLE_GET_ADRESVELDEN.

Datum       Wie  Wat
----------  ---  --------------------------------------------------------------
23-09-2008  XMK  Creatie
******************************************************************************************************/
cursor c_ras (b_numrelatie rle_relatie_adressen.rle_numrelatie%type
             ,b_codrol     rle_relatie_adressen.rol_codrol%type
             ,b_srtadres   rle_relatie_adressen.dwe_wrddom_srtadr%type
             ,b_datum      rle_relatie_adressen.datingang%type
             )
is
select  ads.postcode
,       ads.huisnummer
,       ads.toevhuisnum
from    rle_relatie_adressen ras
,       rle_adressen ads
where   ads.numadres = ras.ads_numadres
and      ras.rle_numrelatie = b_numrelatie
and     (ras.rol_codrol is null or
         ras.rol_codrol = b_codrol)
and     ras.dwe_wrddom_srtadr = b_srtadres
and     nvl(b_datum,trunc(sysdate))
    between ras.datingang
    and     nvl(ras.dateinde
               ,nvl(b_datum,trunc(sysdate))
               )
;
l_postcode    rle_adressen.postcode%type;
l_huisnummer  rle_adressen.huisnummer%type;
l_toevhuisnum rle_adressen.toevhuisnum%type;
begin
   open c_ras(p_numrelatie
             ,p_codrol
             ,p_srtadres
             ,p_datum
             );
   fetch c_ras into l_postcode
                   ,l_huisnummer
                   ,l_toevhuisnum;
   close c_ras;
   --
   return(l_postcode||' '||l_huisnummer||' '||l_toevhuisnum);
END RLE_GET_POSTC_HUISN;
 
/