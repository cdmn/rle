CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_PARTNER
 (P_NUMRELATIE IN NUMBER
 ,P_PEILDATUM IN DATE
 )
 RETURN NUMBER
 IS
/* ------------------------------------------------------------------------------------------------------------------
** Funktie voor het bepalen van de partnerop een peildatum (informatie voor herschikken).
** Aanroep vooralsnog in RET_HERSCHIK
** Wijzigingen
** Datum       Wie  Wijzigingen
** ==========  ===  ================================================================================
** 06-04-2006  DFU  Creatie ihkv benodigd gegeven in RET, project RAP.
** ------------------------------------------------------------------------------------------------------------------
*/
/* Ophalen partner bij opgegeven relatie en peildatum */
CURSOR C_RLE_PARTNER
 ( B_NUMRELATIE IN NUMBER
 , B_PEILDATUM  IN DATE
 )
 IS
SELECT	numrelatie_voor
FROM	rle_v_relatiekoppelingen
WHERE	numrelatie = b_numrelatie
AND     code_rol_in_koppeling in ('H', 'S')--Geregistreerd Partnerschap wordt als Huwelijk vastgelegd
AND     b_peildatum between dat_begin
                        and nvl(dat_einde, b_peildatum)
;
-- Program Data
L_PARTNER NUMBER;
BEGIN
 --
 l_partner := NULL;
 --
 open c_rle_partner( p_numrelatie
                   , p_peildatum
                   );
  fetch c_rle_partner
   into l_partner;
  close c_rle_partner;
RETURN 	l_partner;
END RLE_GET_PARTNER;
 
/