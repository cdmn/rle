CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_LANDCODE_NUMERIEK
 (P_LANDNAAM IN varchar2
 )
 RETURN NUMBER
 IS

-- Sub-Program Unit Declarations
/* Ophalen van landcode */
CURSOR C_LND
 (B_LANDNAAM IN VARCHAR2
 )
 IS
select  lnd.landkode
from  landen lnd
where  lnd.landnaam_hfdl = upper(b_landnaam)
;
R_LND C_LND%ROWTYPE;
BEGIN
  open c_lnd ( p_landnaam);
  fetch c_lnd into r_lnd;
  if c_lnd%found then
    close c_lnd;
    return r_lnd.landkode;
  else
    close c_lnd;
    return 0;
  end if;
END RLE_GET_LANDCODE_NUMERIEK;
 
/