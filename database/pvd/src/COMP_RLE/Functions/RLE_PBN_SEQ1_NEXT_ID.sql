CREATE OR REPLACE FUNCTION comp_rle.RLE_PBN_SEQ1_NEXT_ID
 RETURN NUMBER
 IS
-- Program Data
L_NUMBER NUMBER;
-- PL/SQL Block
BEGIN
/* ==ODA-GSV-BGN== */
  select rle_pbn_seq1.nextval
  into   l_number
  from   sys.dual;
  return l_number;
/* ==ODA-GSV-END== */
END RLE_PBN_SEQ1_NEXT_ID;

 
/