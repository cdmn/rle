CREATE OR REPLACE FUNCTION comp_rle.RLE_VERWERK_RAS
 (PIN_RAS_ID IN NUMBER
 ,PIN_RAS_RLE_NUMRELATIE IN NUMBER
 ,PIV_RAS_ROL_CODROL IN VARCHAR2
 ,PIV_RAS_DWE_WRDDOM_SRTADR IN VARCHAR2
 ,PID_RAS_DATINGANG IN DATE
 ,PID_RAS_DATEINDE IN DATE
 ,PIN_RAS_ADS_NUMADRES IN NUMBER
 ,PIV_RAS_PROVINCIE IN VARCHAR2
 ,PIV_RAS_LOCATIE IN VARCHAR2
 ,PIV_RAS_IND_WOONBOOT IN VARCHAR2
 ,PIV_RAS_IND_WOONWAGEN IN VARCHAR2
 ,PIV_ADS_CODLAND IN VARCHAR2
 ,PIV_ADS_POSTCODE IN VARCHAR2
 ,PIN_ADS_HUISNUMMER IN NUMBER
 ,PIV_ADS_TOEVHUISNUM IN VARCHAR2
 ,PIV_ADS_STRAATNAAM IN VARCHAR2
 ,PIV_ADS_WOONPLAATS IN VARCHAR2
 )
 RETURN NUMBER
 IS
-- Sub-Program Unit Declarations
/* Ophalen radr via radr_id */
CURSOR C_RAS_019
 (B_RAS_ID rle_relatie_adressen.id%type
 )
 IS
/* C_RAS_019 */
select *
from rle_relatie_adressen ras
where ras.id = b_ras_id;
CURSOR C_RAS_029
 (CPN_NUMRELATIE IN NUMBER
 ,CPV_CODROL IN VARCHAR2
 ,CPV_DWE_WRDDOM_SRTADR IN VARCHAR2
 ,CPD_DATINGANG IN DATE
 )
 IS
SELECT *
FROM   rle_relatie_adressen ras
WHERE  ras.rle_numrelatie      =  cpn_numrelatie
AND    (  ras.rol_codrol       =  cpv_codrol
       OR (   ras.rol_codrol   IS NULL
          AND cpv_codrol       IS NULL
          )
       )
AND    ras.dwe_wrddom_srtadr   =  cpv_dwe_wrddom_srtadr
AND    TRUNC( ras.datingang ) =  TRUNC( cpd_datingang );
-- Program Data
LN_RAS_ID NUMBER;
R_RAS_029 C_RAS_029%ROWTYPE;
LD_DATINGANG DATE;
LN_NUMADRES NUMBER;
R_RAS_019 C_RAS_019%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_VERWERK_RAS */
   ln_numadres := rle_verwerk_ads( piv_ads_codland
                                 , piv_ads_postcode
                                 , pin_ads_huisnummer
                                 , piv_ads_toevhuisnum
                                 , piv_ads_straatnaam
                                 , piv_ads_woonplaats
                                 ) ;
   IF pin_ras_id IS NOT NULL
   THEN
      OPEN c_ras_019( pin_ras_id ) ;
      FETCH c_ras_019 INTO r_ras_019 ;
      IF c_ras_019%NOTFOUND
      THEN
         ln_ras_id := NULL ;
      ELSE
         /* Van een bestaand adres mogen alleen de velden
            locatie, ind_woonboot en ind_woonwagen gewijzigd
            worden.
         */
         IF  r_ras_019.rle_numrelatie     = pin_ras_rle_numrelatie
         AND r_ras_019.dwe_wrddom_srtadr  = piv_ras_dwe_wrddom_srtadr
         AND r_ras_019.datingang          = pid_ras_datingang
         AND r_ras_019.ads_numadres       = ln_numadres
         AND (   r_ras_019.rol_codrol     = piv_ras_rol_codrol
             OR  (   r_ras_019.rol_codrol IS NULL
                 AND piv_ras_rol_codrol   IS NULL
                 )
             )
         AND (   r_ras_019.provincie      = piv_ras_provincie
             OR  (   r_ras_019.provincie  IS NULL
                 AND piv_ras_provincie    IS NULL
                 )
             )
         THEN
            ln_ras_id := r_ras_019.id ;
         ELSE
            ln_ras_id := NULL ;
         END IF ;
      END IF ;
   ELSE
      ln_ras_id := NULL ;
   END IF ;
   IF ln_ras_id IS NULL
   THEN
      /* Controleren of er een record met dezelfde NUMRELATIE, CODROL, SRTADR en DATINGANG
         bestaat. Dan namelijk gewoon keihard een udpate doen.
      */
      OPEN c_ras_029( pin_ras_rle_numrelatie
                    , piv_ras_rol_codrol
                    , piv_ras_dwe_wrddom_srtadr
                    , TRUNC( pid_ras_datingang )
                    ) ;
      FETCH c_ras_029 INTO r_ras_029 ;
      IF c_ras_029%FOUND
      THEN
         ln_ras_id := r_ras_029.id ;
      END IF ;
      CLOSE c_ras_029 ;
   END IF ;
   IF ln_ras_id IS NOT NULL
   THEN
      /* Bestaand RAS wijzigen */
      UPDATE rle_relatie_adressen  ras
      SET    ras.locatie       = piv_ras_locatie
      ,      ras.ind_woonboot  = piv_ras_ind_woonboot
      ,      ras.ind_woonwagen = piv_ras_ind_woonwagen
      ,      ras.ads_numadres  = ln_numadres
      ,      ras.provincie     = piv_ras_provincie
      ,      ras.dateinde      = TRUNC( pid_ras_dateinde )
      WHERE  ras.id            = ln_ras_id
      ;
   ELSE
      /* Nieuwe RAS toevoegen.
         Periodes worden geregeld in de triggers.
      */
      INSERT INTO rle_relatie_adressen(
           id
         , rle_numrelatie
         , rol_codrol
         , dwe_wrddom_srtadr
         , datingang
         , ads_numadres
         , provincie
         , locatie
         , ind_woonboot
         , ind_woonwagen
         ) VALUES(
           rle_ras_seq1.nextval
         , pin_ras_rle_numrelatie
         , piv_ras_rol_codrol
         , piv_ras_dwe_wrddom_srtadr
         , TRUNC( pid_ras_datingang )
         , ln_numadres
         , piv_ras_provincie
         , piv_ras_locatie
         , piv_ras_ind_woonboot
         , piv_ras_ind_woonwagen
         ) RETURNING
           id INTO ln_ras_id ;
   END IF ;
   RETURN( ln_ras_id ) ;
END RLE_VERWERK_RAS;

 
/