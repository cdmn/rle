CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_SYN_RIA
 (P_RLE_NUMRELATIE IN NUMBER
 )
 RETURN VARCHAR2
 IS
cursor c_rie (b_rle_numrelatie rle_relatierollen_in_adm.rle_numrelatie%type
             )
is
select 'x'
from rle_relatierollen_in_adm rie
where rie.rle_numrelatie = b_rle_numrelatie
and   rie.rol_codrol     = 'WG'
and   (rie.adm_code      = 'PMT'
      or (rie.adm_code   = 'ALG'
	      and not exists (select 'x'
                          from rle_relatierollen_in_adm rie_not
                          where rie_not.rle_numrelatie = rie.rle_numrelatie
                          and   rie_not.rol_codrol     = rie.rol_codrol
                          and   rie_not.adm_code  not in ('ALG', 'PMT')
                          )
		 )
      );
l_dummy   varchar2(1);
l_bestaat varchar2(1) := 'N';
--
begin
   open c_rie(p_rle_numrelatie
             );
   fetch c_rie into l_dummy;
   if c_rie%found then
     l_bestaat := 'J';
   end if;
   close c_rie;
   --
   return l_bestaat;
END RLE_CHK_SYN_RIA;
 
/