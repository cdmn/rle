CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_GEMEENTENAAM
 (P_NUMRELATIE IN VARCHAR2
 ,P_SRTADS IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Gemeente bij een postcode */
CURSOR C_NPE_007
 (B_POSTCODE IN VARCHAR2
 )
 IS
select gemeentenaam
from   rle_ned_postcode
where  postcode = b_postcode;
-- Program Data
L_GEMEENTENAAM VARCHAR2(30);
L_POSTCODE VARCHAR2(6);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_GET_GEMEENTENAAM */
  /*
   * ophalen postcode via numrelatie. Indien er geen domicilie-adres
   * aanwezig is, dan het feitelijke adres ophalen.
   */
/**
 ** RWI: Wijziging naar aanleiding van FO (I_031)
 **
  l_postcode := rle_get_postcode(p_numrelatie,'DA',TRUNC(SYSDATE));
  IF l_postcode IS NULL
  THEN
    l_postcode := rle_get_postcode(p_numrelatie,'FA',TRUNC(SYSDATE));
  END IF;
*/
  l_postcode := rle_get_postcode( p_numrelatie, p_srtads, TRUNC( SYSDATE ) ) ;
  l_gemeentenaam := 'Onbekend';
  OPEN  c_npe_007(l_postcode);
  FETCH c_npe_007 INTO l_gemeentenaam;
  CLOSE c_npe_007;
  RETURN l_gemeentenaam;
EXCEPTION
  WHEN OTHERS
  THEN
    IF c_npe_007%isopen
    THEN
      CLOSE c_npe_007;
    END IF;
    RAISE;
END RLE_GET_GEMEENTENAAM;

 
/