CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_LANDCODE
 (P_LANDNAAM IN varchar2
 )
 RETURN VARCHAR2
 IS

-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_LND
 (B_LANDNAAM IN VARCHAR2
 )
 IS
select	lnd.codland
from	rle_landen lnd
where	lnd.naam = b_landnaam
;
R_LND C_LND%ROWTYPE;
BEGIN
  open c_lnd ( p_landnaam);
  fetch c_lnd into r_lnd;
  if c_lnd%found then
    close c_lnd;
    return r_lnd.codland;
  else
    close c_lnd;
    return null;
  end if;
END RLE_GET_LANDCODE;
 
/