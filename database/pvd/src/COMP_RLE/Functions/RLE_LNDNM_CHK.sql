CREATE OR REPLACE FUNCTION comp_rle.RLE_LNDNM_CHK
 (P_LANDNAAM IN rle_landen.naam%type
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Ophalen land obv omschrijving */
CURSOR C_LND_003
 (B_NAAM IN VARCHAR2
 )
 IS
/* C_LND_003 */
select	lnd.codland
from	rle_landen lnd
where	upper(lnd.naam) = upper(b_naam);
-- Program Data
R_LND_003 C_LND_003%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_LNDNM_CHK */
open c_lnd_003(p_landnaam);
fetch c_lnd_003 into r_lnd_003;
	if c_lnd_003%found then
		close c_lnd_003;
		return 'J';
	else
		close c_lnd_003;
		return 'N';
	end if;
END RLE_LNDNM_CHK;

 
/