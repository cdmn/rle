CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_FINNUM_CCA
 (P_NUMRELATIE IN VARCHAR2
 )
 RETURN RLE_FINNUM_SEPA_TABLE
 IS

 cursor c_ref (b_low_value   cg_ref_codes.rv_low_value%type)
 is
 select rv_meaning
 from cg_ref_codes
 where rv_domain = 'WIJZE VAN BETALING'
 and rv_low_value = b_low_value;
-- Program Data
L_NAAM VARCHAR2(80);
L_NUMREKENING VARCHAR2(55);
L_SRTREK VARCHAR2(20);
L_MELDING VARCHAR2(80);
L_BIC VARCHAR2(44);
L_IBAN VARCHAR2(44);
L_BET_WIJZE VARCHAR2(2);
L_BET_WIJZE_OMS VARCHAR2(240);
l_return rle_finnum_sepa_table := rle_finnum_sepa_table();
-- PL/SQL Block
BEGIN
rle_m29_finnum_sepa(p_numrelatie
, 'WG'
, null
, sysdate
, l_numrekening
, l_srtrek
, l_naam
, l_melding
, l_bic
, l_iban
, l_bet_wijze);
for v_ref in c_ref (l_bet_wijze)
loop
   l_bet_wijze_oms := v_ref.rv_meaning;
end loop;
  l_return.extend;
  l_return (1) := rle_finnum_sepa (l_numrekening, l_bic, l_iban, l_bet_wijze, l_bet_wijze_oms);
return l_return;
END;
 
/