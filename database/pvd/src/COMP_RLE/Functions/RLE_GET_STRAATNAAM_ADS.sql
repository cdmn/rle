CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_STRAATNAAM_ADS
 (P_ADS_NUMADRES IN NUMBER
 )
 RETURN VARCHAR2
 IS
-- PL/SQL Specification
/* Op basis van het aangeleverde nummer van het adres wordt
   de straatnaam geretourneerd in UPPERCASE
*/
  CURSOR c_straatnaam (b_adresnr rle_relatie_adressen.ads_numadres%TYPE)
  IS
  SELECT UPPER(stt.straatnaam) straatnaam
    FROM rle_straten stt
   WHERE stt.straatid in (SELECT ads.stt_straatid
                            FROM rle_adressen ads
                           WHERE ads.numadres = b_adresnr
                         )
  ;
  l_straatnaam      rle_straten.straatnaam%TYPE := NULL;
-- PL/SQL Block
BEGIN
  OPEN c_straatnaam (p_ads_numadres);
  FETCH c_straatnaam into l_straatnaam;
  IF c_straatnaam%NOTFOUND
  THEN
    l_straatnaam := NULL;
  END IF;
  CLOSE c_straatnaam;
  return l_straatnaam;
END;

 
/