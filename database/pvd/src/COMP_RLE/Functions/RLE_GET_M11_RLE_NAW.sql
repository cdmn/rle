CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_M11_RLE_NAW
 (P_AANTPOS IN number
 ,P_RELNUM IN number
 ,P_COD_OPMAAK IN VARCHAR2 := '1'
 )
 RETURN VARCHAR2
 IS
BEGIN
declare
  l_o_opmaaknaam rle_relaties.namrelatie%type;
  l_o_melding    varchar2(4000);
begin
  rle_m11_rle_naw
     (p_aantpos      => p_aantpos
     ,p_relnum       => p_relnum
     ,p_o_opmaaknaam => l_o_opmaaknaam
     ,p_o_melding    => l_o_melding
     ,p_cod_opmaak   => p_cod_opmaak
     );
  return l_o_opmaaknaam;
end;
END RLE_GET_M11_RLE_NAW;
 
/