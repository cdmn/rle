CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_RIE_EXISTS
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ROL_CODROL IN VARCHAR2
 ,P_ADM_CODE IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
cursor c_rie (b_rle_numrelatie rle_relatierollen_in_adm.rle_numrelatie%type
             ,b_rol_codrol     rle_relatierollen_in_adm.rol_codrol%type
             ,b_adm_code       rle_relatierollen_in_adm.adm_code%type
             )
is
select 'x'
from rle_relatierollen_in_adm rie
where rie.rle_numrelatie = b_rle_numrelatie
and   rie.rol_codrol     = b_rol_codrol
and   rie.adm_code       = b_adm_code
;
l_dummy   varchar2(1);
l_bestaat varchar2(1) := 'N';
--
begin
   open c_rie(p_rle_numrelatie
             ,p_rol_codrol
             ,p_adm_code
             );
   fetch c_rie into l_dummy;
   if c_rie%found then
     l_bestaat := 'J';
   end if;
   close c_rie;
   --
   return l_bestaat;
END RLE_CHK_RIE_EXISTS;
 
/