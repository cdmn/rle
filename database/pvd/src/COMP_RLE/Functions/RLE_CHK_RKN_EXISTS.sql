CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_RKN_EXISTS
 (PIN_NUMRELATIE_1 IN RLE_RELATIES.NUMRELATIE%TYPE
 ,PIV_CODROL_1 IN RLE_ROLLEN.CODROL%TYPE
 ,PIN_NUMRELATIE_2 IN RLE_RELATIES.NUMRELATIE%TYPE
 ,PIV_CODROL_2 IN RLE_ROLLEN.CODROL%TYPE
 ,PID_PEILDATUM IN DATE
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
CURSOR C_RKN_003
 (CPN_NUMRELATIE_1 IN RLE_RELATIES.NUMRELATIE%TYPE
 ,CPV_CODROL_1 IN RLE_ROLLEN.CODROL%TYPE
 ,CPN_NUMRELATIE_2 IN RLE_RELATIES.NUMRELATIE%TYPE
 ,CPV_CODROL_2 IN RLE_ROLLEN.CODROL%TYPE
 ,CPD_PEILDATUM IN DATE
 )
 IS
SELECT NULL
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkn.rkg_id = rkg.id
AND    (   (   rkn.rle_numrelatie         = cpn_numrelatie_1
           AND rkg.rol_codrol_beperkt_tot = nvl( cpv_codrol_1, rkg.rol_codrol_beperkt_tot )
           AND rkn.rle_numrelatie_voor    = cpn_numrelatie_2
           AND rkg.rol_codrol_met         = nvl( cpv_codrol_2, rkg.rol_codrol_met )
           )
       OR  (   rkn.rle_numrelatie         = cpn_numrelatie_2
           AND rkg.rol_codrol_beperkt_tot = nvl( cpv_codrol_2, rkg.rol_codrol_beperkt_tot )
           AND rkn.rle_numrelatie_voor    = cpn_numrelatie_1
           AND rkg.rol_codrol_met         = nvl( cpv_codrol_1, rkg.rol_codrol_met )
           )
       )
AND    cpd_peildatum BETWEEN rkn.dat_begin AND nvl( rkn.dat_einde, cpd_peildatum );
-- Program Data
R_RKN_003 C_RKN_003%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Controleren bestaan relatie koppeling */
   OPEN c_rkn_003( pin_numrelatie_1
                 , piv_codrol_1
                 , pin_numrelatie_2
                 , piv_codrol_2
                 , nvl( pid_peildatum, TRUNC( sysdate ) )
                 ) ;
   FETCH c_rkn_003 INTO r_rkn_003 ;
   IF c_rkn_003%FOUND
   THEN
      CLOSE c_rkn_003 ;
      RETURN( 'J' ) ;
   ELSE
      CLOSE c_rkn_003 ;
      RETURN( 'N' ) ;
   END IF ;
END RLE_CHK_RKN_EXISTS;

 
/