CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_RLE_MIG_SEQ1
 RETURN number
 IS
/* QMS$GET_NEXTVAL_WGR_MIG_SEQ1 */
   CURSOR c_dual
   IS
   SELECT rle_mig_seq1.nextval
   FROM   dual;
   l_nextval   NUMBER;
BEGIN
   OPEN c_dual;
   FETCH c_dual INTO l_nextval;
   CLOSE c_dual;
   RETURN l_nextval;
END;

 
/