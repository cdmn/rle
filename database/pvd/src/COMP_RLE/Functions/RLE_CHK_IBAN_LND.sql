CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_IBAN_LND
 (P_IBAN IN VARCHAR2
 )
 RETURN BOOLEAN
 IS
/* ****************************************************
Naam: RLE_CHK_IBAN_LND
Beschrijving: Heeft een land de indicatie SEPA?
Datum Wie Wat
-------- ---------- ---------------------------------------
XFZ      creatie
*********************************************************** */
  cursor c_land (c_iso_lnd in   varchar2)
  is
  select null
  from   rle_landen
  where  ind_sepa_rekening     = 'J'
  and    landcode_iso3166_a2   = c_iso_lnd
  and    (dat_einde is null
       or dat_einde > sysdate)
  ;
  l_return     boolean  := false;
begin
  for v_land in c_land (substr(p_iban, 1, 2))
  loop
     l_return := true;
  end loop;
  return l_return;
end;
 
/