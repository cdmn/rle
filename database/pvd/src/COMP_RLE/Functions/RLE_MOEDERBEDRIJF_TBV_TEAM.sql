CREATE OR REPLACE FUNCTION comp_rle.RLE_MOEDERBEDRIJF_TBV_TEAM
 (P_RLE_NUMRELATIE RLE_RELATIE_KOPPELINGEN.RLE_NUMRELATIE%TYPE
 )
 RETURN NUMBER
 IS
-- PL/SQL Specification
cursor c_rkn (b_rle_numrelatie rle_relatie_koppelingen.rle_numrelatie%type)
   is
   select rkn.rle_numrelatie, rkn.rle_numrelatie_voor
   from rle_relatie_koppelingen rkn
   where rkn.rkg_id =
   (select rkg.id
    from rle_rol_in_koppelingen rkg
   where rkg.code_rol_in_koppeling = 'MTT')
   and (rkn.dat_einde is null or rkn.dat_einde > sysdate)
   and rkn.rle_numrelatie_voor = b_rle_numrelatie
   ;
   r_rkn c_rkn%rowtype;
-- PL/SQL Block
begin
  stm_util.debug('p_rle_numrelatie : '|| to_char(p_rle_numrelatie));
  r_rkn.rle_numrelatie := p_rle_numrelatie;
  loop
    open c_rkn(r_rkn.rle_numrelatie);
    fetch c_rkn into r_rkn;
    stm_util.debug('rle_numrelatie : '||to_char(r_rkn.rle_numrelatie) ||' rle_numrelatie_voor : '||to_char(r_rkn.rle_numrelatie_voor));
    if c_rkn%found then
      stm_util.debug('found');
      close c_rkn;
    else
      stm_util.debug('not found');
      close c_rkn;
      return r_rkn.rle_numrelatie;
    end if;
  end loop;
end;

 
/