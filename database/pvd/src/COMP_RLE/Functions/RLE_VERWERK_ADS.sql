CREATE OR REPLACE FUNCTION comp_rle.RLE_VERWERK_ADS
 (PIV_CODLAND IN VARCHAR2
 ,PIV_POSTCODE IN VARCHAR2
 ,PIN_HUISNUMMER IN NUMBER
 ,PIV_TOEVHUISNUM IN VARCHAR2
 ,PIV_STRAATNAAM IN VARCHAR2
 ,PIV_WOONPLAATS IN VARCHAR2
 )
 RETURN NUMBER
 IS
-- Sub-Program Unit Declarations
/* c_wnpl02 */
CURSOR C_WPS_002
 (B_CODLAND IN VARCHAR2
 ,B_WOONPL IN VARCHAR2
 )
 IS
/* C_WPS_002 */
SELECT	wps.woonplaatsid
FROM	rle_woonplaatsen wps
WHERE	wps.lnd_codland = b_codland
AND	UPPER( wps.woonplaats ) = UPPER( b_woonpl );
/* c_stra02 */
CURSOR C_STT_002
 (B_WPS_ID IN NUMBER
 ,B_STRAAT IN VARCHAR2
 )
 IS
/* C_STT_002 */
SELECT	*
FROM	rle_straten stt
WHERE	stt.wps_woonplaatsid = b_wps_id
AND	UPPER( stt.straatnaam ) = UPPER( b_straat ) ;
/* Bepalen adres-ID o.b.v. land, woonplaats en straat */
CURSOR C_ADS_020_NL
 (B_CODLAND IN VARCHAR2
 ,B_WPS_ID IN NUMBER
 ,B_STT_ID IN NUMBER
 ,B_POSTCOD IN VARCHAR2
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_020_NL */
SELECT ads.numadres
FROM   rle_adressen ads
WHERE  ads.lnd_codland = b_codland
AND    (ads.wps_woonplaatsid = b_wps_id
         OR  (ads.wps_woonplaatsid IS NULL
              AND b_wps_id          IS NULL
             )
       )
AND    (ads.stt_straatid = b_stt_id
        OR (ads.stt_straatid IS NULL
           AND b_stt_id      IS NULL
           )
       )
AND    ( ads.toevhuisnum = b_toevnum
        OR  (ads.toevhuisnum IS NULL
             AND b_toevnum       IS NULL
            )
       )
AND       ads.huisnummer = b_huisnum
AND       ads.postcode  = UPPER( b_postcod )
;
--
CURSOR C_ADS_020
 (B_CODLAND IN VARCHAR2
 ,B_WPS_ID IN NUMBER
 ,B_STT_ID IN NUMBER
 ,B_POSTCOD IN VARCHAR2
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_020 */
/* PKG, 15-01-2003: Hint  voor index gebruik toegevoegd om te zorgen
dat er nooit een full-tablescan plaatsvindt op rle_adressen
landcode is altijd gevuld, straat en woonplaats id kunnen leeg zijn
*/
SELECT /*+ INDEX (ADS RLE_ADS_LND_FK1) */
       ads.numadres
FROM   rle_adressen ads
WHERE  ads.lnd_codland = b_codland
AND    (   ads.wps_woonplaatsid = b_wps_id
       OR  (   ads.wps_woonplaatsid IS NULL
           AND b_wps_id             IS NULL
           )
       )
AND    (   ads.stt_straatid = b_stt_id
       OR  (   ads.stt_straatid IS NULL
           AND b_stt_id         IS NULL
           )
       )
AND    (   ads.toevhuisnum = b_toevnum
       OR  (   ads.toevhuisnum IS NULL
           AND b_toevnum       IS NULL
           )
       )
AND    (   ads.postcode  = UPPER( b_postcod )
       OR  (   ads.postcode IS NULL
           AND b_postcod    IS NULL
           )
       )
AND    (   ads.huisnummer = b_huisnum
       OR  (   ads.huisnummer IS NULL
           AND b_huisnum      IS NULL
           )
       )
;
-- Program Data
R_ADS_020 C_ADS_020%ROWTYPE;
R_WPS_002 C_WPS_002%ROWTYPE;
LN_WOONPLAATSID NUMBER;
LN_STRAATID NUMBER;
R_STT_002 C_STT_002%ROWTYPE;
LN_NUMADRES NUMBER;

-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_VERWERK_ADS */
   /* Verwerk woonplaats */
  OPEN c_wps_002( piv_codland
                 , piv_woonplaats
                 ) ;
  FETCH c_wps_002 INTO r_wps_002 ;
  IF c_wps_002%NOTFOUND
  THEN
      CLOSE c_wps_002 ;
      --
      IF piv_woonplaats IS NOT NULL
      THEN
         INSERT INTO rle_woonplaatsen(
              woonplaatsid
            , lnd_codland
            , woonplaats
            ) VALUES(
--              rle_wps_seq1.nextval
             RLE_WPS_SEQ1_NEXT_ID         -- xjo, 7 okt 2010 ivm. incident 193715
            , piv_codland
            , piv_woonplaats
            ) RETURNING
              woonplaatsid INTO ln_woonplaatsid
            ;
      ELSE
         ln_woonplaatsid := NULL ;
      END IF ;
   ELSE
      CLOSE c_wps_002 ;
      ln_woonplaatsid := r_wps_002.woonplaatsid ;
   END IF ;
   --
   --   IF ln_woonplaatsid IS NULL
   --   THEN
   --      stm_dbproc.raise_error( 'RLE-10298' ) ;
   --   END IF ;
   --
   /* Verwerk straat */
   OPEN c_stt_002( ln_woonplaatsid
                 , piv_straatnaam
                 ) ;
   FETCH c_stt_002 INTO r_stt_002 ;
   IF c_stt_002%NOTFOUND
   THEN
      CLOSE c_stt_002 ;
      --
      IF piv_straatnaam IS NOT NULL
      THEN
         INSERT INTO rle_straten(
              straatid
            , wps_woonplaatsid
            , straatnaam
            ) VALUES(
              rle_stt_seq1.nextval
            , ln_woonplaatsid
            , piv_straatnaam
            ) RETURNING
              straatid INTO ln_straatid
            ;
      ELSE
         ln_straatid := NULL ;
      END IF ;
   ELSE
      CLOSE c_stt_002 ;
      ln_straatid := r_stt_002.straatid ;
   END IF ;

--   IF ln_straatid IS NULL
--   THEN
--      stm_dbproc.raise_error( 'RLE-10298' ) ;
--   END IF ;
   if piv_codland = 'NL'
   then
     /* Verwerk adres : bij nederlandse adressen postcode verplicht*/
     OPEN c_ads_020_nl( piv_codland
                   , ln_woonplaatsid
                   , ln_straatid
                   , piv_postcode
                   , pin_huisnummer
                   , piv_toevhuisnum
                   ) ;
     --
     FETCH c_ads_020_nl INTO r_ads_020 ;
     IF c_ads_020_nl%NOTFOUND
     THEN
        CLOSE c_ads_020_nl ;
        --
        INSERT INTO rle_adressen(
             numadres
           , lnd_codland
           , stt_straatid
           , wps_woonplaatsid
           , huisnummer
           , postcode
           , toevhuisnum
           ) VALUES(
             rle_ads_seq1.nextval
           , piv_codland
           , ln_straatid
           , ln_woonplaatsid
           , pin_huisnummer
           , piv_postcode
           , piv_toevhuisnum
           ) RETURNING
             numadres INTO ln_numadres ;
     ELSE
        CLOSE c_ads_020_nl ;
        ln_numadres := r_ads_020.numadres ;
     END IF ;
   else /* buitenlands adres */
     /* Verwerk adres */
     OPEN c_ads_020( piv_codland
                   , ln_woonplaatsid
                   , ln_straatid
                   , piv_postcode
                   , pin_huisnummer
                   , piv_toevhuisnum
                   ) ;
     FETCH c_ads_020 INTO r_ads_020 ;
     IF c_ads_020%NOTFOUND
     THEN
        CLOSE c_ads_020 ;
        --
        INSERT INTO rle_adressen(
             numadres
           , lnd_codland
           , stt_straatid
           , wps_woonplaatsid
           , huisnummer
           , postcode
           , toevhuisnum
           ) VALUES(
             rle_ads_seq1.nextval
           , piv_codland
           , ln_straatid
           , ln_woonplaatsid
           , pin_huisnummer
           , piv_postcode
           , piv_toevhuisnum
           ) RETURNING
             numadres INTO ln_numadres ;
     ELSE
        CLOSE c_ads_020 ;
        ln_numadres := r_ads_020.numadres ;
     END IF ;
   end if;
  --
  RETURN( ln_numadres )
  ;
END;
 
/