CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_ADM_RIE
 (P_PRS_PERSOONSNUMMER IN VARCHAR2
 ,P_RLE_NUMRELATIE IN NUMBER
 ,P_ROL_CODROL IN VARCHAR2
 ,P_SYSTEEMCOMP IN VARCHAR2
 ,P_ADM_TYPE IN VARCHAR2
 ,P_IND_AFSTEMMEN_GBA_TGSTN IN VARCHAR2
 )
 RETURN BOOLEAN
 IS
cursor c_rie (b_rle_numrelatie          rle_relatierollen_in_adm.rle_numrelatie%type
             ,b_rol_codrol              rle_relatierollen_in_adm.rol_codrol%type
             ,b_adm_type                rle_administraties.type%type
             ,b_ind_afstemmen_gba_tgstn rle_administraties.ind_afstemmen_gba_tgstn%type
             )
is
select 'x'
from rle_relatierollen_in_adm rie
,    rle_administraties adm
where rie.adm_code                = adm.code
and   rie.rle_numrelatie          = b_rle_numrelatie
and   rie.rol_codrol              = b_rol_codrol
and   adm.type                    = b_adm_type
and   adm.ind_afstemmen_gba_tgstn = b_ind_afstemmen_gba_tgstn
;
l_dummy          varchar2(1);
l_bestaat        boolean := false;
l_rle_numrelatie rle_relatierollen_in_adm.rle_numrelatie%type;
--
begin
   if p_prs_persoonsnummer is not null
   then
      comp_rle.rle_m06_rec_relnum(p_relext      => p_prs_persoonsnummer
                                 ,p_systeemcomp => p_systeemcomp
                                 ,p_codrol      => p_rol_codrol
                                 ,p_datparm     => trunc(sysdate)
                                 ,p_o_relnum    => l_rle_numrelatie
                                 );
   else
      l_rle_numrelatie := p_rle_numrelatie;
   end if;
   --
   if l_rle_numrelatie is null
   then
      stm_dbproc.raise_error('RLE-10383 Rle numrelatie is niet te bepalen');
   end if;
   --
   open c_rie(l_rle_numrelatie
             ,p_rol_codrol
             ,p_adm_type
             ,p_ind_afstemmen_gba_tgstn
             );
   fetch c_rie into l_dummy;
   if c_rie%found then
     l_bestaat := true;
   end if;
   close c_rie;
   --
   return l_bestaat;
END RLE_CHK_ADM_RIE;
 
/