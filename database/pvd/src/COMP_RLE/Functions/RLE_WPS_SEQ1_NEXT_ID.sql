CREATE OR REPLACE FUNCTION comp_rle.RLE_WPS_SEQ1_NEXT_ID
 RETURN NUMBER
 IS
/*  Jan van Overved, 7-oktober-2010,
     ivm. i193715 (RLE : RLEPRC23 Message Ora-06502 niet aanwezig in de mess...)
     Sequence is gereset naar 1.
     Sequence is boven de 100000, en de kolom van id van rle_woonplaatsen mag max 5 groot zijn.
     Er zijn nog wel zo een 50000 id's beschikbaar op dit moment in de rle_woonplaatsen tabel.
     Daarom wordt de logica van de function RLE_WPS_SEQ1_NEXT_ID aangepast zodat de eerste beschikbare id wordt geselecteerd.
     (nog wel met de sequence RLE_WPS_SEQ1 als basis)
*/


  L_NUMBER RLE_WOONPLAATSEN.WOONPLAATSID%TYPE;
  l_vlag   boolean     := true;
  l_dummy  varchar2(1);
  cursor c_chk_id ( b_id RLE_WOONPLAATSEN.WOONPLAATSID%TYPE )
  is
    select 'x'
    from   rle_woonplaatsen
    where  woonplaatsid=b_id;
BEGIN
  select rle_wps_seq1.nextval
  into   l_number
  from   sys.dual;
  while ( l_vlag
          and
          l_number < 100000 )
  loop
     open  c_chk_id ( l_number );
     fetch c_chk_id into l_dummy;
     l_vlag := c_chk_id%found;   -- wanneer id nog niet voorkomt, dan is het goed.
     close c_chk_id;
     if l_vlag
     then
       select rle_wps_seq1.nextval
       into   l_number
       from   sys.dual;
     end if;
  end loop;
  return l_number;
END RLE_WPS_SEQ1_NEXT_ID;
 
/