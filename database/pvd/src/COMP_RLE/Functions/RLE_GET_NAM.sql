CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_NAM
 (P_NUMRELATIE NUMBER
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
select	*
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Program Data
R_RLE_001 C_RLE_001%ROWTYPE;
L_NAAM VARCHAR2(120);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
open  c_rle_001(p_numrelatie);
fetch c_rle_001 into r_rle_001;
if c_rle_001%found then
  l_naam := r_rle_001.namrelatie;
else
  l_naam := null;
end if;
close c_rle_001;
return l_naam;
END RLE_GET_NAM;

 
/