CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_VERZENDNAAM
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 )
 RETURN VARCHAR2
 IS
-- H de Bruin 21-11-2002 dienst ondersteunde aanspreekvorm van vrouwen niet goed.
-- tevens de output aangepast aan de wensen van Communicatie */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
--select	*
select namrelatie
,      indinstelling
,      dwe_wrddom_vvgsl
,      dwe_wrddom_geslacht
,      dwe_wrddom_gewatitel
,      naam_partner
,      handelsnaam
,      vvgsl_partner
,      code_aanduiding_naamgebruik
,      voorletter
,      datoverlijden
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Program Data
l_opmaaknaam VARCHAR2(500);
l_aanhef varchar2(100);
L_EIGEN_VOORLETTERS VARCHAR2(20);
l_partner_voorletters VARCHAR2(20);
L_PARTNER_FNAAM VARCHAR2(500);
L_PARTNER_VOORVOEGSEL VARCHAR2(40);
L_EIGEN_VOORVOEGSEL VARCHAR2(40);
R_RLE_001 C_RLE_001%ROWTYPE;
L_IND_IBM VARCHAR2(1) := 'N';
L_EIGEN_NAAM VARCHAR2(184);
L_LENGTE_IBM NUMBER := 40;
L_EIGEN_FNAAM VARCHAR2(500);
L_PARTNER_NAAM VARCHAR2(184);

FUNCTION FNC_BEPAAL_VOORLETTERS
 (PIV_VOORLETTERS IN VARCHAR2
 )
 RETURN VARCHAR2;
-- Sub-Program Units
/* Gebruikt in RLE_M11_GET_NAW om de voorletters van een naam op te maken */
FUNCTION FNC_BEPAAL_VOORLETTERS
 (PIV_VOORLETTERS IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
BEGIN
IF INSTR( piv_voorletters, '.' ) = 0  AND
         INSTR( piv_voorletters, ' ' ) = 0
      THEN
         -- Geen puntjes en spaties. Dus zelf puntjes toevoegen tussen alle letters.
         RETURN( LTRIM( SUBSTR( piv_voorletters,  1, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  2, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  3, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  4, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  5, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  6, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  7, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  8, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  9, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters, 10, 1 ) || '.', '.' )
               ) ;
      ELSIF INSTR( piv_voorletters, '.' ) =  0  AND
            INSTR( piv_voorletters, ' ' ) != 0
      THEN
         -- Geen puntjes, wel spaties. Dus spaties vervangen door puntjes.
         RETURN( REPLACE( piv_voorletters, ' ', '.' ) ) ;
      ELSE
         -- Puntjes en spaties aanwezig. Gewoon zo laten.
         RETURN( piv_voorletters ) ;
      END IF ;
      RETURN( NULL ) ;
END FNC_BEPAAL_VOORLETTERS;
BEGIN
  l_opmaaknaam := NULL ;
  l_aanhef := null;
  -- Ophalen eigen naam
  OPEN c_rle_001( P_NUMRELATIE ) ;
  FETCH c_rle_001 INTO r_rle_001 ;
  IF c_rle_001%FOUND
  THEN
-- eerst de aanspreekwijze bepalen
  IF r_rle_001.indinstelling =  'N'
  THEN
    l_aanhef := NULL ;
    IF r_rle_001.dwe_wrddom_gewatitel IS NOT NULL -- is er een code voor gewenste aanspreektitel
    THEN
       RFE_RFW_GET_WRD
               ( 'GWT'
               , r_rle_001.dwe_wrddom_gewatitel
               , l_ind_ibm
               , l_lengte_ibm
               , l_aanhef
              ) ;
    ELSIF r_rle_001.dwe_wrddom_geslacht = 'M'
    THEN
      l_aanhef := 'De heer' ;
    ELSIF r_rle_001.dwe_wrddom_geslacht = 'V'
    THEN
      l_aanhef := 'Mevrouw' ;
    END IF ;
-- bepalen juiste verzendnaam
-- formatteer voorletters (puntgescheiden haafdletters)
    l_eigen_voorletters := fnc_bepaal_voorletters( r_rle_001.voorletter ) ;
-- Zoek voorvoegsel
    IF r_rle_001.dwe_wrddom_vvgsl IS NOT NULL
    THEN
        RFE_RFW_GET_WRD
                ('VVL'
                , r_rle_001.dwe_wrddom_vvgsl
                , l_ind_ibm
                , l_lengte_ibm
                , l_eigen_voorvoegsel
                ) ;
    END IF ;
    IF r_rle_001.code_aanduiding_naamgebruik in ('P','V','N')
    THEN
         l_partner_voorletters := NULL ;
         IF r_rle_001.vvgsl_partner IS NOT NULL
         THEN
           RFE_RFW_GET_WRD
                   ( 'VVL'
                   , r_rle_001.vvgsl_partner
                   , l_ind_ibm
                   , l_lengte_ibm
                   , l_partner_voorvoegsel
                   ) ;
            --
            -- RTJ 02-12-2003 nav callnr 91690
            --
            IF l_partner_voorvoegsel IS NULL
            THEN
               l_partner_voorvoegsel := r_rle_001.vvgsl_partner;
            END IF;
            -- EINDE RTJ
         END IF ;
    END IF ;
    l_eigen_fnaam := ltrim(rtrim(rtrim(l_eigen_voorvoegsel)||' '||r_rle_001.namrelatie));
    l_partner_fnaam := ltrim(rtrim(rtrim(l_partner_voorvoegsel)||' '||r_rle_001.naam_partner));
    IF r_rle_001.code_aanduiding_naamgebruik = 'P'
    THEN
         l_opmaaknaam := l_partner_fnaam;
    ELSIF r_rle_001.code_aanduiding_naamgebruik = 'E'
    THEN
        l_opmaaknaam := l_eigen_fnaam;
    ELSIF r_rle_001.code_aanduiding_naamgebruik = 'V'
    THEN
         l_opmaaknaam := l_partner_fnaam||'-'||l_eigen_fnaam;
    ELSIF r_rle_001.code_aanduiding_naamgebruik = 'N'
    THEN
         l_opmaaknaam := l_eigen_fnaam||'-'||l_partner_fnaam;
    END IF;
    l_opmaaknaam := l_aanhef||' '||rtrim(l_eigen_voorletters)||' '||l_opmaaknaam;
  ELSE
    -- voor bedrijven eerst handelsnaam, anders gewone naam
    l_opmaaknaam := nvl (r_rle_001.handelsnaam, r_rle_001.namrelatie);
  END IF;   -- r_rle_001.indinstelling
  END IF;
  /* 02-10-2006 NKU */
  IF r_rle_001.datoverlijden is not null
  THEN
       l_opmaaknaam := 'De erven van '||lower(substr(l_opmaaknaam, 1, 1))||substr(l_opmaaknaam, 2);
  END IF;
  close c_rle_001;
  return l_opmaaknaam;
END RLE_GET_VERZENDNAAM;
 
/