CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_BR_TPL0029
 (P_VOORVOEGSELSP IN varchar2
 ,P_NAAMP IN varchar2
 ,P_MESSAGE_CODE OUT varchar2
 ,P_MESSAGE_TEKST OUT varchar2
 ,P_BUSINESS_RULE_CODE OUT varchar2
 )
 RETURN BOOLEAN
 IS

/******************************************************************************
   NAME:       rle_chk_br_tpl0029
   PURPOSE:    Voorvoegsel en naam partner

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        25-10-2007              PLI  Created this function.
   1.01		  25-10-2007			  RIO  p_message_tekst aangepast

   NOTES:
   Als Voorvoegsels partner gevuld zijn moet Naam partner ook gevuld zijn.
******************************************************************************/

t_result boolean:= TRUE;
--
begin
  --
  if p_voorvoegselsp is not null and p_naamp is null
  then
    --
    t_result := FALSE;
    --
    p_message_code       := 'AVG-4';
    p_message_tekst      := 'Als Voorvoegsels partner gevuld zijn moet Naam partner ook gevuld zijn.';
    p_business_rule_code := 'BRTPL0029';
    --
  end if;
  --
  return t_result;
  --
exception
  when others
  then
    --
    p_message_code           := SQLCODE;
    p_message_tekst          := 'Fout bij validatie van voorvoegsel en naam partner: '||sqlerrm;
    p_business_rule_code     := 'BRTPL0029';
    return FALSE;
    --
END RLE_CHK_BR_TPL0029;
 
/