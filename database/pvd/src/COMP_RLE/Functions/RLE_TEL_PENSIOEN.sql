CREATE OR REPLACE FUNCTION comp_rle.RLE_TEL_PENSIOEN
 (P_RLE_NUMRELATIE IN NUMBER
 )
 RETURN NUMBER
 IS
--Ophalen arbeidsverhoudingen na PARO conversie
cursor c_avg (b_werknemer avg_arbeidsverhoudingen.werknemer%type)
is
select avg.avg_id
,      avg.werkgevernummer
,      avg.ingangsdatum
,      avg.einddatum
from   avg_v_arbeidsverhoudingen avg
where  avg.persoonsnummer = b_werknemer
and    (   trunc(avg.creatiedatum) > to_date('31-12-2004','dd-mm-yyyy')
        or nvl(avg.einddatum, trunc(sysdate)) > to_date('31-12-2004','dd-mm-yyyy')
       )
;
cursor c_vwzd ( b_nr_werkgever          wgr_v_werkzaamheden.nr_werkgever%type
               ,b_dat_begin             wgr_v_werkzaamheden.dat_begin%type
               ,b_dat_einde             wgr_v_werkzaamheden.dat_einde%type
               )
is
select vwzd.code_groep_werkzhd
,      vwzd.code_subgroep_werkzhd
from   wgr_v_werkzaamheden vwzd
where  vwzd.nr_werkgever   = b_nr_werkgever
and    b_dat_begin         < nvl(vwzd.dat_einde
                            ,to_date('01-01-3500','dd-mm-yyyy'))
and    nvl(b_dat_einde
          ,to_date('01-01-3500','dd-mm-yyyy'))
                           > vwzd.dat_begin
;
cursor c_vcvc (b_wgr_werkgnr cvc_v.wgr_werkgnr%type
              ,b_dataanv     cvc_v.dataanv%type
              ,b_dateind     cvc_v.dateind%type)
is
select vcvc.vzp_nummer
,      vcvc.code_functiecategorie
from   cvc_v vcvc
where  vcvc.wgr_werkgnr    = b_wgr_werkgnr
and    b_dataanv           < nvl(vcvc.dateind
                            ,to_date('01-01-3500','dd-mm-yyyy'))
and    nvl(b_dateind
          ,to_date('01-01-3500','dd-mm-yyyy'))
                           > vcvc.dataanv
;
l_werkgevernummer       avg_v_arbeidsverhoudingen.werkgevernummer%type;
l_vorige_avg_pensioen   boolean;
l_aantal_pmt            number := 0;
l_adm_tab               wsf_adm.tab_adm;
l_code_functiecategorie cvc_v.code_functiecategorie%type;
r_adm                   rle_administraties%rowtype;
l_error                 varchar2(1000);
l_prs_persoonsnummer    rle_relatie_externe_codes.extern_relatie%type;
c_proc_name constant    varchar2(25) := 'RLE_RIE.CHK_PENSIOEN';
--
--BEGIN
begin
      l_prs_persoonsnummer := rle_m03_rec_extcod('PRS'
                                                , p_rle_numrelatie
                                                , 'PR'
                                                , sysdate);
      for r_avg in c_avg(l_prs_persoonsnummer)
      loop
         --
         l_vorige_avg_pensioen := false;
         --
         l_werkgevernummer := r_avg.werkgevernummer;
         --
         for r_vwzd in c_vwzd(l_werkgevernummer
                             ,r_avg.ingangsdatum
                             ,r_avg.einddatum)
         loop
            l_adm_tab.delete;
            --
            wsf_adm.get_adm_wsf(p_kodegvw     => r_vwzd.code_groep_werkzhd
                               ,p_kodesgw     => r_vwzd.code_subgroep_werkzhd
                               ,p_datum_begin => r_avg.ingangsdatum
                               ,p_datum_einde => r_avg.einddatum
                               ,p_adm_table   => l_adm_tab);
            --
            for i in 0 .. l_adm_tab.count-1
            loop
               if l_adm_tab(i).type = 'P'
               then
                  l_vorige_avg_pensioen := true;
                  exit;
               end if;
               --
            end loop;
            --
            if l_vorige_avg_pensioen
            then
               l_aantal_pmt := l_aantal_pmt + 1;
               exit;
            end if;
            --
         end loop;
         --
         if not l_vorige_avg_pensioen
         then
            for r_vcvc in c_vcvc(l_werkgevernummer
                                ,r_avg.ingangsdatum
                                ,r_avg.einddatum)
            loop
               --
               -- Alleen bepalen als functiecat gelijk is aan die van arbeidsverhouding
               --
               reg_get_funccat(avg_wbp.get_beroep(p_avg_id     =>  r_avg.avg_id
                                                 ,p_peil_datum =>  r_avg.ingangsdatum)
                              ,l_code_functiecategorie);
               --
               if r_vcvc.code_functiecategorie = l_code_functiecategorie
               then
                  wsf_adm.get_adm_vzp(p_vzp_nummer  => r_vcvc.vzp_nummer
                                     ,p_adm_row     => r_adm);
                  --
                  -- Administratie alleen toevoegen als deze nog niet bestaat.
                  --
                  if r_adm.type = 'P'
                  then
                     --
                     l_vorige_avg_pensioen := true;
                     --
                  end if;
               --
               end if;
               --
               if l_vorige_avg_pensioen
               then
                  l_aantal_pmt := l_aantal_pmt + 1;
                  exit;
               end if;
               --
            end loop;
         --
         end if;
         --
         if  l_aantal_pmt > 1
         then
            exit;
         end if;
         --
      end loop;
      --
      return(l_aantal_pmt);
exception
when others
then
   l_error := c_proc_name||' rle numrelatie : '||p_rle_numrelatie
                         ||' werkgever : '||l_werkgevernummer||' fout: '||sqlerrm;
   raise_application_error(-20000, l_error);
END RLE_TEL_PENSIOEN;
 
/