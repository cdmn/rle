CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_TELNUM
 (P_TELEFOON IN VARCHAR2
 ,P_RAS_ID IN NUMBER
 ,P_NUMRELATIE IN NUMBER
 ,P_WRDDOM IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
select	*
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
/* Ophalen van postcode van bij een adres */
CURSOR C_ADS_001
 (B_NUMADRES NUMBER
 )
 IS
/* C_ADS_001 */
 select        ads.postcode
 , ads.lnd_codland
 from  rle_adressen ads
 where ads.numadres = b_numadres;
/* Ophalen radr via radr_id */
CURSOR C_RAS_019
 (B_RAS_ID rle_relatie_adressen.id%type
 )
 IS
/* C_RAS_019 */
select *
from rle_relatie_adressen ras
where ras.id = b_ras_id;
-- Program Data
L_POS NUMBER;
L_LENGTH NUMBER;
L_FOUT NUMBER;
R_ADS_001 C_ADS_001%ROWTYPE;
R_RLE_001 C_RLE_001%ROWTYPE;
R_RAS_019 C_RAS_019%ROWTYPE;
L_POS2 NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_CHK_TELNUM */
-- Niets gevuld waarmee de landcode op te vragen is, dan mag alles
IF ( p_ras_id IS NULL) AND
   ( p_numrelatie IS NULL)
THEN
   RETURN 'J';
END IF;
-- Geen Telefoon nummer of fax, dan mag alles
IF ( p_wrddom <> 'TEL') AND
   ( p_wrddom <> 'FAX')
THEN
   RETURN 'J';
END IF;
-- Indien adres gevuld nemen we de landcode van het adres en anders
-- de landcode van de relatie zelf
-- we controleren alleen bij landcode NL
IF p_ras_id IS NOT NULL
THEN
   OPEN c_ras_019( p_ras_id ) ;
   FETCH c_ras_019 INTO r_ras_019 ;
   CLOSE c_ras_019 ;
   OPEN c_ads_001( r_ras_019.ads_numadres );
   FETCH c_ads_001 INTO r_ads_001;
   IF (r_ads_001.lnd_codland <> 'NL')
   THEN
      CLOSE c_ads_001;
      RETURN 'J';
   ELSE
      CLOSE c_ads_001;
   END IF;
END IF;
--
l_fout := 0;
-- Streepje op de juiste positie?
l_pos := INSTR( p_telefoon, '-');
/* zorgen dat 06-nummers ook toegestaan worden */
IF l_pos = 3
THEN
   IF SUBSTR( p_telefoon, 1, 2) <> '06'
   THEN
      l_fout := 1;
   END IF;
ELSIF l_pos = 0
THEN
   l_fout := 3;
ELSIF l_pos < 4 OR
      l_pos > 5
THEN
   l_fout := 1;
END IF;
-- Bevatten alle posities een toegestane waarde?
IF l_fout = 0
THEN
   << firstpart >>
   FOR inx IN 1..( l_pos - 1)
   LOOP
      l_pos2 := INSTR( '0123456789', SUBSTR( p_telefoon, inx, 1));
      IF l_pos2 = 0
      THEN
         l_fout := 2;
         EXIT firstpart;
      END IF;
   END LOOP firstpart;
--
   l_length := NVL( LENGTH( p_telefoon), 0);
   << lastpart >>
   FOR inx IN ( l_pos + 1)..l_length
   LOOP
      l_pos2 := INSTR( '0123456789', SUBSTR( p_telefoon, inx, 1));
      IF l_pos2 = 0
      THEN
         l_fout := 2;
         EXIT lastpart;
      END IF;
   END LOOP lastpart;
END IF;
-- Lengte van het telefoonnummer juist?
IF l_length <> 11
THEN
   l_fout := 3;
END IF;
-- Juiste fout teruggeven
IF l_fout = 0
THEN
   RETURN 'J';
ELSIF p_wrddom = 'TEL'
THEN
   IF l_fout = 1
   THEN
      RETURN 'RLE-00519';
   ELSIF l_fout = 2
   THEN
      RETURN 'RLE-00520';
   ELSIF l_fout = 3
   THEN
      RETURN 'RLE-00521';
   END IF;
ELSIF p_wrddom = 'FAX'
THEN
   IF l_fout = 1
   THEN
      RETURN 'RLE-00522';
   ELSIF l_fout = 2
   THEN
      RETURN 'RLE-00523';
   ELSIF l_fout = 3
   THEN
      RETURN 'RLE-00524';
   END IF;
END IF;
END RLE_CHK_TELNUM;

 
/