CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_ROL_NIET_MTB
 (P_RELATIENR IN NUMBER
 )
 RETURN BOOLEAN
 IS
/****************************************************************************************************************
Wijzigingshistorie:

Wanneer	      Wie                    Wat
----------------    ------------------   -------
09-02-2009    XMB                  Controle dmv rollen is vervangen door controle dmv
                                                    adm_code in (MELL,MNLL).

****************************************************************************************************************/
--
CURSOR c_rie(b_relatienr in number)
IS
SELECT 'x'
FROM   rle_relatierollen_in_adm
WHERE  rle_numrelatie = b_relatienr
AND    adm_code in ('MELL','MNLL');
--
l_result varchar2(20);
--
BEGIN
--
open  c_rie(p_relatienr);
fetch c_rie into l_result;
if c_rie%found
then
  return true;
else
  return false;
end if;
close c_rie;
--
END RLE_CHK_ROL_NIET_MTB;
 
/