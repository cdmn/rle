CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_CATEGORIE
 (P_NUMRELATIE_DEELNEMER IN number
 ,P_NUMRELATIE_BEGUNSTIGDE IN number
 ,P_PEILDATUM IN date
 )
 RETURN NUMBER
 IS

  /* ******************************************************************* */
  /* Procedure om een categorie te bepalen van een relatie               */
  /*                                                                     */
  /* De categorien zijn: 1. Partner                                      */
  /*                     2. Gewezen wettelijke partner                   */
  /*                     3. Wezen < 18 jaar                              */
  /*                     4. Wezen >= 18 jaar                             */
  /*                     9. Onbekend                                     */
  /*                                                                     */
  /* Datum      Wie          Wat                                         */
  /* --------   ------------ ------------------------------------------- */
  /* 03-11-2006 Hans Smit    Nieuwbouw voor RAP manaegment rapportage    */
  /* 30-11-2006 Hans Smit    Deelnemer/ begunstigde omgedraaid in querie */
  /* ******************************************************************* */
  --
  -- Haal de soort koppeling op (1=Huwelijk, 2=Kind, 4=Samenlevingscontract)
  --
  cursor c_rkn ( b_numrelatie_deelnemer   in number
               , b_numrelatie_begunstigde in number
               )
  is
    select rkg_id
    ,      dat_begin
    ,      dat_einde
    from   rle_relatie_koppelingen
    where  rle_numrelatie      = b_numrelatie_begunstigde
    and    rle_numrelatie_voor = b_numrelatie_deelnemer
  ;
  --
  r_rkn     c_rkn%rowtype;
  --
  -- Haal de geboortedatum van de wees
  --
  cursor c_rle (b_numrelatie in number)
  is
    select datgeboorte
    from   rle_relaties
    where  numrelatie = b_numrelatie
  ;
  --
  l_rle_datgeboorte   rle_relaties.datgeboorte%type;
  --
  l_foutomgeving      varchar2(200);
  l_leeftijd          number(3);
  l_categorie         number(1);
  --
begin
  --
  l_foutomgeving := '001 - In rle_get_categorie deelnemer = ' || to_char(p_numrelatie_deelnemer)   ||
                                            ' begunstigde = ' || to_char(p_numrelatie_begunstigde) ||
                                            ' p_peildatum = ' || to_char(p_peildatum, 'DD-MM-YYYY');
  --
  -- Haal het type koppeling uit de relatie koppelingen voor de deelnemer en de begunstigde
  --
  open  c_rkn ( b_numrelatie_deelnemer   => p_numrelatie_deelnemer
              , b_numrelatie_begunstigde => p_numrelatie_begunstigde
              );
  fetch c_rkn
  into  r_rkn;
  close c_rkn;
  --
  l_foutomgeving := '010 - Na c_rkn deelnemer = ' || to_char(p_numrelatie_deelnemer)    ||
                                ' begunstigde = ' || to_char(p_numrelatie_begunstigde)  ||
                                ' p_peildatum = ' || to_char(p_peildatum, 'DD-MM-YYYY') ||
                               ' l_rkn_rkg_id = ' || to_char(r_rkn.rkg_id);
  --
  if    r_rkn.rkg_id in (1, 4)     -- 1 = Huwelijk, 4 = Samenlevingscontract, dus partner
  then
    --
    -- Als de einddatum van de koppeling leeg is, of voorbij de peildatum, dan partner
    --                                                                     anders gewezen wettelijke partner
    --
    if   r_rkn.dat_einde is null
      or r_rkn.dat_einde > p_peildatum
    then
      --
      l_categorie := 1;
      --
    else
      --
      l_categorie := 2;
      --
    end if;
    --
  elsif r_rkn.rkg_id = 2           -- 2 = Kind
  then
    l_foutomgeving := '080 - Haal geboortedatum wees, deelnemer = ' || to_char(p_numrelatie_deelnemer) ||
                                                  ' begunstigde = ' || to_char(p_numrelatie_begunstigde);
    --
    -- Haal de geboortedatum van de wees
    --
    open  c_rle ( b_numrelatie => p_numrelatie_begunstigde );
    fetch c_rle
    into  l_rle_datgeboorte;
    close c_rle;

    --
    -- Bepaal de leeftijd van de wees
    --
    l_foutomgeving := '090 - Bepaal de leeftijd deelnemer = ' || to_char(p_numrelatie_deelnemer)          ||
                                            ' begunstigde = ' || to_char(p_numrelatie_begunstigde)        ||
                                            ' p_peildatum = ' || to_char(p_peildatum      , 'DD-MM-YYYY') ||
                                      ' l_rle_datgeboorte = ' || to_char(l_rle_datgeboorte, 'DD-MM-YYYY');
    --
    l_leeftijd := trunc(months_between ( p_peildatum, l_rle_datgeboorte)/ 12);
    --
    if l_leeftijd is null
    then
      --
      l_categorie := 9;
      --
    elsif l_leeftijd < 18
    then
      --
      l_categorie := 3;
      --
    else -- >= 18 jaar
      --
      l_categorie := 4;
      --
    end if;
  else  -- rkg_id onbekend
    --
    l_categorie := 9;
    --
  end if; -- if rkg in 1, 4
  --
  l_foutomgeving := '099 - Einde rle_get_categorie deelnemer = ' || to_char(p_numrelatie_deelnemer)    ||
                                               ' begunstigde = ' || to_char(p_numrelatie_begunstigde)  ||
                                               ' p_peildatum = ' || to_char(p_peildatum, 'DD-MM-YYYY') ||
                                              ' l_categorie  = ' || to_char(l_categorie);
  return l_categorie;
exception
when others
then
  dbms_output.put_line ( 'System: rle_get_relatie: Er is een fout opgetreden' );
  dbms_output.put_line ( l_foutomgeving );
END RLE_GET_CATEGORIE;
 
/