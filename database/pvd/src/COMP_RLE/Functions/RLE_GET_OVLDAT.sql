CREATE OR REPLACE FUNCTION comp_rle.RLE_GET_OVLDAT
 (P_NUMRELATIE IN NUMBER
 )
 RETURN DATE
 IS
-- Sub-Program Unit Declarations
/* Ophalen overlijdensdatum relatie */
CURSOR C_RLE_018
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_018 */
SELECT		rle.datoverlijden
FROM		rle_relaties rle
WHERE		rle.numrelatie = b_numrelatie;
-- Program Data
L_OVL_DAT DATE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_GET_OVLDAT */
l_ovl_dat := NULL;
open c_rle_018(p_numrelatie);
fetch c_rle_018 into l_ovl_dat;
close c_rle_018;
RETURN 	l_ovl_dat;
END RLE_GET_OVLDAT;

 
/