CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_RRL_EXISTS
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ROL_CODROL IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
l_dummy   varchar2(1);
l_bestaat varchar2(1) := 'N';
--
begin
   rle_chk_rrl_aanw(p_rle_numrelatie => p_rle_numrelatie
                   ,p_rol_codrol     => p_rol_codrol);
   --
   -- Procedure geeft een error als rrl niet gevonden
   --
   l_bestaat := 'J';
   --
   -- Er wordt geen boolean teruggegeven omdat een databaselink
   -- hier problemen mee heeft.
   --
   return l_bestaat;
exception
when others then
   return l_bestaat;
END RLE_CHK_RRL_EXISTS;
 
/