CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_VOORLETTER_GELDIG
 (P_VOORLETTERS varchar2
 )
 RETURN BOOLEAN
 IS
l_char integer;
 l_pos  number;
begin
 for l_pos in 1 .. length(p_voorletters)
 loop
    l_char := ascii(substr(p_voorletters,l_pos,1));
    if l_char between ascii('A') and ascii('Z')
    or l_char between ascii('a') and ascii('z')
    or l_char between 192 and 255 --letters met diacrieten
    or l_char in (142, 138, 159)
    then
       null;
    else
       return false;
    end if;
 end loop;
 return true;
end;
 
/