CREATE OR REPLACE FUNCTION comp_rle.RLE_CHK_LND_BLOCKED
 (P_CODLAND IN RLE_LANDEN.CODLAND%TYPE
 )
 RETURN BOOLEAN
 IS
tmpvar                        boolean;

   cursor rle_land(
      b_codland                                      rle_landen.codland%type
   )
   is
      select lnd.dat_einde
      from   rle_landen lnd
      where  lnd.codland = p_codland;
/******************************************************************************
   NAME:       A.S.Fedorova
   PURPOSE:    Blokkeren van insert van adressen of woonplaatsen met verlopen landkode

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18.02.2009  A.S.Fedorova    1. Created this function.

   NOTES:

******************************************************************************/
begin
   tmpvar := false;

   for r_land in rle_land( p_codland )
   loop
      if     r_land.dat_einde is not null
         and r_land.dat_einde < trunc( sysdate )
      then
         tmpvar := true;
      end if;
   end loop;

   return tmpvar;
exception
   when no_data_found
   then
      return false;
   when others
   then
      return false;
END RLE_CHK_LND_BLOCKED;
 
/