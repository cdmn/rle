CREATE MATERIALIZED VIEW comp_rle.rle_mv_zoek_werknemer_2 (relatienummer,zoeknaam,datgeboorte,datoverlijden,naam_partner,persoonsnummer,sofinummer,pvachmea_persoonsnummer,wgr_personeelsnummer,da_postcode,da_woonplaats,a_nummer)
REFRESH COMPLETE 
AS select *
from   rle_v_zoek_werknemer;