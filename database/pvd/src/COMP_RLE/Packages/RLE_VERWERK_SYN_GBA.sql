CREATE OR REPLACE PACKAGE comp_rle.RLE_VERWERK_SYN_GBA IS


TYPE REC_GBA IS RECORD (a_nummer              varchar2(300),
                        bsn                   varchar2(1000),
                        bsn_gba               varchar2(1000),
                        voornamen             varchar2(1000),
                        voorvoegsel           varchar2(1000),
                        geslachtsnaam         varchar2(1000),
                        geboortedatum         varchar2(1000),
                        geslachts_aanduiding  varchar2(1000),
                        aanduiding_naamgebr   varchar2(1000),
                        nationaliteit         varchar2(1000),
                        a_nummer_prtner       varchar2(1000),
                        bsn_prtner            varchar2(1000),
                        voornamen_prtner      varchar2(1000),
                        voorvoegsel_prtner    varchar2(1000),
                        geslachtsnaam_prtner  varchar2(1000),
                        geboortedatum_prtner  varchar2(1000),
                        datum_sluiting        varchar2(1000),
                        datum_ontbinding      varchar2(1000),
                        datum_overlijden      varchar2(1000),
                        straatnaam            varchar2(1000),
                        huisnummer            varchar2(1000),
                        huisnummer_toevoeging varchar2(1000),
                        postcode              varchar2(1000),
                        woonplaatsnaam        varchar2(1000),
                        land_naar_vertrokken  varchar2(1000),
                        datum_vertrek_nl      varchar2(1000),
                        foutmelding           varchar2(4000));

TYPE T_GBA_TAB IS TABLE OF REC_GBA index by pls_integer;
g_maak_uit_bericht           boolean:= true;

FUNCTION MAAK_UITBERICHT
 RETURN BOOLEAN;
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_TYPE_VERWERKING IN VARCHAR2 := 'R'
 ,P_CONTROLEER_VERWERKING IN VARCHAR2 := 'N'
 );
END RLE_VERWERK_SYN_GBA;

 
/