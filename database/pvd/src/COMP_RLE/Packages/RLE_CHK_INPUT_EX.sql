CREATE OR REPLACE PACKAGE comp_rle.RLE_CHK_INPUT_EX IS
--Het doel is het controleren van alle ingelezen regels uit de tabel rle_input_ex_partners en het wegschrijven van de uitval naar een  csv bestand.

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 ,P_REGELING IN VARCHAR2
 ,P_SCHEIDING_VROUW_VANAF IN DATE
 ,P_SCHEIDING_MAN_VANAF   IN DATE
 );
END RLE_CHK_INPUT_EX;
 
/