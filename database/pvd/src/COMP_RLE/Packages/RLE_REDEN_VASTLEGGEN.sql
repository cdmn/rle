CREATE OR REPLACE PACKAGE comp_rle.RLE_REDEN_VASTLEGGEN IS

--------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is vastleggen reden in rle_synchronisatie_statussen
-- op basis van aangeleverde lijst persoonsnummers en reden.
--------------------------------------------------------------
-- Wijzigingshistorie
-- Wanneer     Wie               Wat
--------------------------------------------------------
-- 24-02-2014  AZM               Creatie
-------------------------------------------------------------

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN number
 );
END RLE_REDEN_VASTLEGGEN;

 
/