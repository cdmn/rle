CREATE OR REPLACE PACKAGE comp_rle.RLE_NAT IS
 /******************************************************************************
NAAM:       rle_nat
DOEL:       package rondom NAT

Wie    Wanneer         Wat
UAO    03-09-2009      Creatie
XMK    30-10-2009      lnd_codland gewijzigd in code_nationaliteit
XMK    04-11-2009      br ier0039 verplaatst naar rle_nat_briu trigger   ******************************************************************************/
--
g_package varchar2(100) := 'RLE_NAT';
--

FUNCTION NAT_GET_NAT
 (P_RLE_NUMRELATIE IN RLE_NATIONALITEITEN.RLE_NUMRELATIE%TYPE
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN DATE
 )
 RETURN VARCHAR2;
FUNCTION NAT_INSERT
 (P_RLE_NUMRELATIE IN RLE_NATIONALITEITEN.RLE_NUMRELATIE%TYPE
 ,P_CODE_NATIONALITEIT IN VARCHAR2
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN DATE
 )
 RETURN VARCHAR2;
FUNCTION NAT_DEL
 (P_NAT_ID IN RLE_NATIONALITEITEN.ID%TYPE
 )
 RETURN VARCHAR2;
END RLE_NAT;
 
/