CREATE OR REPLACE PACKAGE comp_rle.RLE_NPE_PCK IS

/* Ophalen van gemuteerde postcodes */
CURSOR C_MPE_002
 (B_PBN_ID NUMBER
 ,B_HERSTART_VOLGNR VARCHAR2
 )
 IS
/* C_MPE_002 */
select rowid, mpe.*
from 	rle_mut_postcode mpe
where	pbn_id = b_pbn_id
and mpe.volgnr > b_herstart_volgnr
order by mpe.volgnr;

G_IND_NPE_MUTATIE VARCHAR2(1);
L_WOONPLAATS VARCHAR2(30);
L_STRAATNAAM VARCHAR2(30);
L_SW_WOONPL VARCHAR2(20);
L_SW_STRAAT VARCHAR2(20);
G_SELWEEK VARCHAR2(7);
G_FOUTMELDING VARCHAR2(200);

BESTAND_FOUT EXCEPTION;

/* Hoofdroutine mutatie postcode tabel */
PROCEDURE MUTATIE
 (P_WEEK_CONTROLE IN varchar2
 );
/* Vullen van RLE_PCOD_BATCHRUN EN RLE_MUT_POSTCODE */
PROCEDURE VUL_PCBR
 (P_WEEK_CONTROLE IN varchar2
 );
/* Nieuw voorkomen toevoegen in ned postcode tabel */
FUNCTION NIEUW
 (P_PCD_ROW c_mpe_002%rowtype
 )
 RETURN VARCHAR2;
/* Verwijderen uit ned postcode tabel */
FUNCTION VERWIJDER
 (P_PCD_ROW c_mpe_002%rowtype
 )
 RETURN VARCHAR2;
/* Wijzigen in ned postcode tabel */
FUNCTION WIJZIG
 (P_PCD_ROW IN OUT c_mpe_002%rowtype
 )
 RETURN VARCHAR2;
/* VERWERKEN PTT MUTATIE TAPE */
PROCEDURE VERWERK
 (P_SCRIPTNAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_WEEK_CONTROLE IN VARCHAR2 := 'J'
 );
END RLE_NPE_PCK;
 
/