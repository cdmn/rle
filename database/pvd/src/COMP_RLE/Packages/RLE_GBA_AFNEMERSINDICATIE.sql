CREATE OR REPLACE PACKAGE comp_rle.RLE_GBA_AFNEMERSINDICATIE IS
--------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is aanpassen rle_afstemmingen_gba
-- code_gba_status -> OV / AF
-- afnemersindicatie -> N
-- op basis van aangeleverde lijst persoonsnummers.
--------------------------------------------------------------
-- Wijzigingshistorie
-- Wanneer     Wie               Wat
--------------------------------------------------------
-- 13-02-2014  WHR               Creatie
-------------------------------------------------------------

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN number
 );
END RLE_GBA_AFNEMERSINDICATIE;

 
/