CREATE OR REPLACE PACKAGE comp_rle.RLE_INDICATIE IS

DOORGEVEN_AAN_GBA VARCHAR2(1);

/* Zet de indicatie op J voor gebruik in forms. */
PROCEDURE INDICATIE_GBA_J;
/* Zet de indicatie op N voor gebruik in forms. */
PROCEDURE INDICATIE_GBA_N;
/* Teruggeven GBA indicatie voro gebruik in forms */
FUNCTION INDICATIE_GBA
 RETURN VARCHAR2;
END RLE_INDICATIE;
 
/