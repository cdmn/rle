CREATE OR REPLACE PACKAGE comp_rle.RLE_EX_PARTNERS IS



type bericht is record (begindatum_huwelijk date
                       ,einddatum_huwelijk  date
                       ,kenmerk             varchar2(20)
                       ,persoonsnummer      varchar2(10)
                       ,bsn                 varchar2(10)
                       ,a_nummer            varchar2(15)
                       ,geboortedatum       date
                       ,overlijdensdatum    date
                       ,geslacht            varchar2(5)
                       ,naam                varchar2(120)
                       ,voorletters         varchar2(50)
                       ,voorvoegsels        varchar2(25)
                       ,naamgebruik         varchar2(5)
                       ,straat              varchar2(50)
                       ,huisnummer          number
                       ,huisnummer_toev     varchar2(30)
                       ,postcode            varchar2(40)
                       ,woonplaats          varchar2(50)
                       ,land                varchar2(50)
                       );

type ex_partner is table of bericht index by pls_integer;

/* voorkomen dat een GBA uitbericht wordt gestuurd */
FUNCTION MAAK_UITBERICHT
 RETURN BOOLEAN;
PROCEDURE BESTAAT_RKN
 (P_RLE_NUMRELATIE_DLN IN number
 ,P_RLE_NUMRELATIE_EX IN number
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN date
 ,P_RKN_AANWEZIG OUT VARCHAR2
 );
PROCEDURE BESTAAT_EX
 (P_NAAM IN varchar2
 ,P_VOORLETTERS IN varchar2
 ,P_GEBOORTEDATUM IN date
 ,P_BSN IN varchar2
 ,P_ANR IN VARCHAR2
 ,P_RLE_NUMRELATIE_EX OUT number
 ,P_PERSOONSNUMMER_EX OUT varchar2
 ,P_FOUTMELDING OUT varchar2
 );
PROCEDURE BIJWERKEN_KOPPELING
 (P_RLE_NUMRELATIE IN number
 ,P_RLE_NUMRELATIE_VOOR IN number
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN date
 ,P_KENMERK IN number
 ,P_FOUTMELDING OUT varchar2
 );
PROCEDURE MAAK_BERICHT
 (P_BESTANDSNAAM IN varchar2
 ,P_RELATIENUMMER IN number
 );
PROCEDURE VERWERK_BERICHT
 (P_BERICHT_ID IN varchar2
 ,P_IND_SUCCES OUT varchar2
 ,P_FOUTMELDING OUT varchar2
 );
PROCEDURE UPDATE_INDICATIE_BRONTABEL
 (P_NR_EBR IN VARCHAR2
 ,P_IND_WERKBAK IN VARCHAR2
 ,P_IND_VERWERKT IN VARCHAR2
 ,P_IND_FOUT_VERWERKING IN VARCHAR2
 );
END RLE_EX_PARTNERS;

 
/