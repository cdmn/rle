CREATE OR REPLACE PACKAGE comp_rle.RLE_LIB IS
  /******************************************************************************
      NAME:
      PURPOSE:   -

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        28-01-2010  U-Bocht          Changehistory added.
   ******************************************************************************/

/* Package aangemaakt als verzamelpunt voor allerlei handige rle programma's

De functies voor ophalen werkgevernummer/persoonsnummer/relatienummer
zijn zo gebouwd, dat geen rol en externe code meer hoeft worden opgegeven.
Dit conform architectuur standpunt dat elke persoon de rol PR moet hebben,
en elke werkgever de rol WG
*/
type contactgeg_rec is record (administratie   rle_administraties.code%type
                              ,soort_contact   rle_communicatie_nummers.dwe_wrddom_srtcom%type
                              ,nummer          rle_communicatie_nummers.numcommunicatie%type
                              );

type t_contactgeg_tab is table of contactgeg_rec index by pls_integer;

FUNCTION GET_RELATIENR_WERKGEVER
 (P_WERKGEVERNUMMER IN varchar2
 )
 RETURN PLS_INTEGER;
FUNCTION GET_RELATIENR_PERSOON
 (P_PERSOONSNUMMER IN pls_integer
 )
 RETURN PLS_INTEGER;
FUNCTION GET_WERKGEVERNR_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2;
FUNCTION GET_PERSOONSNR_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN PLS_INTEGER;
FUNCTION GET_GEBOORTEDATUM_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN DATE;
FUNCTION GET_OVERLIJDENSDATUM_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN DATE;
/* Ophalen geslacht bij een relatie */
FUNCTION GET_GESLACHT_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2;
FUNCTION GET_GEMOEDSBEZWAARD
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2;
FUNCTION GET_GBA_STATUS_RELATIE
 (P_RELATIENUMMER IN pls_integer
 )
 RETURN VARCHAR2;
FUNCTION IS_COURANT_FNR
 (P_RLE_NUMRELATIE IN PLS_INTEGER
 ,P_ADM_CODE IN VARCHAR2
 ,P_IBAN IN VARCHAR2
 )
 RETURN VARCHAR2;
PROCEDURE HAAL_CONTACTGEGEVENS
 (P_RLE_NUMRELATIE IN rle_relaties.numrelatie%type
 ,P_CONTACTGEG_TAB OUT rle_lib.t_contactgeg_tab
 );
END RLE_LIB;
/