CREATE OR REPLACE PACKAGE comp_rle.RLE_RRL IS

/* Insert van een relatierol record. */
PROCEDURE INS_RRL
 (P_RLE_NUMRELATIE IN rle_relatie_rollen.rle_numrelatie%TYPE
 ,P_ROL_CODROL IN rle_relatie_rollen.rol_codrol%TYPE
 );
/* update rol_coderol */
PROCEDURE UPD_RRL_ROL
 (P_RLE_NUMRELATIE IN rle_relatie_rollen.rle_numrelatie%TYPE
 ,P_ROL_CODROL IN rle_relatie_rollen.rol_codrol%TYPE
 ,P_OLD_ROL_CODROL IN rle_relatie_rollen.rol_codrol%TYPE
 );
END RLE_RRL;
 
/