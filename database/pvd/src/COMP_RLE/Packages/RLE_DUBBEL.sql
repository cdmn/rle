CREATE OR REPLACE PACKAGE comp_rle.RLE_DUBBEL IS

TYPE REC_DUBBEL_RLE IS RECORD
 (NUMRELATIE NUMBER(9)
 ,ZOEKNAAM varchar2(120)
 ,DATGEBOORTE date
 ,VOORLETTER varchar2(10)
 ,POSTCODE varchar2(15)
 ,HUISNUMMER number(10, 0)
 ,SOORT_ADRES varchar2(4)
 ,NUMADRES number(10)
 ,SCORE number
);

TYPE TAB_DUBBEL_RLE IS TABLE OF RLE_DUBBEL.REC_DUBBEL_RLE INDEX BY BINARY_INTEGER;
/* **********************************************************
   PACKAGE:    rle_dubbel

   DOEL:       Controle op dubbele relaties

   Wie    Wanneer         Wat
   ------ ---------- ----------------------------------------
   XCW    11-02-2010 numrelatie naar 10 in rel_dubbel_rle
   A. Aten 07-06-2010 trunc( sysdate) gezet in cursoren met rle_relatie_adressen, om de goede records op te halen
   LHT 10-06-2015 package gelijkgetrokken met PVCS rev.9, left outer joins in cursors c_rle01-3 gezet, om personen zonder adressen te kunnen vinden
 ********************************************************** */

/* Bepalen aanwezigeheid */
PROCEDURE RELATIE_AANWEZIG
 (P_NAAM IN varchar2
 ,P_DUBBEL_GEVONDEN OUT VARCHAR2
 ,P_GEVONDEN_RELATIES IN OUT RLE_DUBBEL.TAB_DUBBEL_RLE
 ,P_POSTCODE IN VARCHAR2 := null
 ,P_HUISNUMMER IN NUMBER := null
 ,P_GEBOORTEDATUM IN DATE := null
 ,P_VOORLETTERS IN varchar2 := null
 );
END RLE_DUBBEL;
/