CREATE OR REPLACE PACKAGE comp_rle.RLE_BEPAAL_GROEPEN IS

--------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is bepalen van de groepen en dit vastleggen in synchronisatie_statussen.
-- Functie: RLE1003
-- Wanneer     Wie               Wat
--------------------------------------------------------
-- 03-02-2014  WHR               Creatie
-- 29-7-2014   MAL               parameter p_detailbestand + procedure maak_detailbestand toegevoegd tbv
--                               thermometherfunctie
-------------------------------------------------------------

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN number
 ,P_DETAILBESTAND IN VARCHAR2
 );
END RLE_BEPAAL_GROEPEN;

 
/