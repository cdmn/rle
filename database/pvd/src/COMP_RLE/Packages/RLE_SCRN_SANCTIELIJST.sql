CREATE OR REPLACE PACKAGE comp_rle.RLE_SCRN_SANCTIELIJST IS
/************************************************************
Package: RLE_SCRN_SANCTIELIJST

Package voor bijwerken van de sanctielijst

Datum      Wie        Wat
---------- ---------- -----------------------------------
14-07-2010 XKV        Aanmaak
*************************************************************/

PROCEDURE ONDERHOUDEN
 (P_BRON IN VARCHAR2
 ,P_BRON_REF IN VARCHAR2
 ,P_SOORT IN VARCHAR2
 ,P_VOORNAAM IN VARCHAR2
 ,P_ACHTERNAAM IN VARCHAR2
 ,P_VOLLEDIGENAAM IN VARCHAR2
 ,P_DATGEBOORTE IN DATE
 ,P_STRAATNAAM IN VARCHAR2
 ,P_HUISNUMMER IN VARCHAR2
 ,P_POSTCODE IN VARCHAR2
 ,P_WOONPLAATS IN VARCHAR2
 ,P_TOEGEVOEGD OUT VARCHAR2
 );
END RLE_SCRN_SANCTIELIJST;
 
/