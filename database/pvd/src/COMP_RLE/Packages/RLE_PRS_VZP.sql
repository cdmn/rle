CREATE OR REPLACE PACKAGE comp_rle.RLE_PRS_VZP IS


/********************************************************************************************************
Created BY: A. Budiawan
DATE:       07-10-2009
Purpose:    Bepalen voor welke regeling(en) en regelingonderdelen (verzekeringspakketten) een persoon op
            een bepaalde peildatum is aangesloten.
Usage:
Remarks: * Wanneer de context (administratiecode) is gezet, dan wordt de uitvoer beperkt tot alle regelingen die
           behoren tot de opgegeven administratie.
         * Als er geen peildatum is opgegeven, dan wordt systeemdatum genomen.
Revision History
Label DATE           Name                   Description
1.0   07-10-2009     A. Budiawan            Initial Creation
**********************************************************************************************************/

type r_prs_typ is record
 ( id            number(6)
 , relatienummer number(9)
 , persoonsnummer varchar2(20)
 , adm_type varchar2(3)
 , adm_code varchar2(12)
 , rgg_code varchar2(10)
 , rgg_ond varchar2(30)
 , omschrijving varchar2(70)
 , datum_begin date
 , datum_eind date
 , col_ind varchar2(30) -- Collectief, Individueel
 , srt_reg_ond varchar2(30) -- Standaard, Optioneel, Dispensabel, NULL
 , srt_aansluiting varchar2(30) -- Verplicht, Vrijwillig/Vrijstelling
);

type t_prs_typ is table of r_prs_typ;


/********************************************************************************************************
Created BY: A. Budiawan
DATE:       07-10-2009
Purpose:    Deze procedure bepaalt voor welke regeling(onderdelen) een persoon is aangesloten op een
            bepaalde peildatum. Het gaat hierbij alleen om de zgn. bedrijfstakeigen regelingen of om
            verzekeringen.
Input Parameters: p_persoonsnummer  persoonsnummer (optioneel)
                  p_relatienummer  relatienummer persoon (optioneel)
                  p_peildatum  default systeemdatum
Output: t_prs_typ
Usage:
Remarks: * Wanneer de context (administratiecode) is gezet, dan wordt de uitvoer beperkt tot alle regelingen die
           behoren tot de opgegeven administratie.
         * Relatienummer persoon en/of persoonsnummer kan worden opgegeven. Minimaal één van beide is
           verplicht. Als persoonsnummer gevuld is, dan is dit leidend.
         * Als er geen peildatum is opgegeven, dan wordt systeemdatum genomen.
         * Bij het bepalen van soort aansluiting is het volgende van toepassing:
           - bij individuele opties is het altijd 'vrijwillig'
           - bij collectieve regelingen is de soort aansluiting altijd 'verplicht'
Revision History
Label DATE           Name                   Description
1.0   07-10-2009     A. Budiawan            Initial Creation
1.1   08-02-2010     Karel Vlieg            Betekenisloos uniek nummer toegevoegd tbv ADF
**********************************************************************************************************/

FUNCTION GET_RLE_PRS_VZP
 (P_PERSOONSNUMMER IN varchar2
 ,P_RELATIENUMMER IN number
 ,P_PEILDATUM IN date := sysdate
 )
 RETURN T_PRS_TYP pipelined;
END RLE_PRS_VZP;

 
/