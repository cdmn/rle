CREATE OR REPLACE PACKAGE comp_rle.RLE_BEP_RELNR IS


  type t_adm_lijst is table of varchar2(12) index by binary_integer;

  /* implementatie van rle0978 */

PROCEDURE ZOEK_NUMRELATIE
 (P_ADMINISTRATIES IN rle_bep_relnr.t_adm_lijst
 ,P_NAAM IN varchar2
 ,P_GEBOORTEDATUM IN date
 ,P_POSTCODE IN varchar2
 ,P_PEILDATUM IN date
 ,P_RLE_NUMRELATIE OUT number
 ,P_MEERDERE_RESULTATEN OUT varchar2
 );
END RLE_BEP_RELNR;

 
/