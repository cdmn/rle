CREATE OR REPLACE PACKAGE comp_rle.RLE_BULK_BERICHTEN IS
--------------------------------------------------------------
-- Doel: Het doel van deze functionaliteit is aanmaken van plaatsingsberichten
-- of verwijderberichten op basis van aangeleverde lijst met persoonsnummers.
--------------------------------------------------------------
-- Wijzigingshistorie
-- Wanneer     Wie               Wat
--------------------------------------------------------------
-- 05-06-2014  XSW               Creatie
-- 11-7-2014   MAL               Verwijderberichten toegevoegd
--------------------------------------------------------------

/* Aanmaken bulk plaatsingsberichten */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_SOORT_BERICHT IN VARCHAR2
 );
END RLE_BULK_BERICHTEN;
 
/