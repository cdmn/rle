CREATE OR REPLACE PACKAGE comp_rle.RLE_OFMW_API IS
  /******************************************************************************

  NAME:       RLE_OFMW_API
  PURPOSE:    RLE-operaties specifiek voor MN 3.0 Business Services

  REVISIONS:
  Ver        Date        Author           Description

  ---------  ----------  ---------------  ------------------------------------

  1.0        26-02-2015  OKR              Creatie

  ******************************************************************************

/* Registreert een adres of werkt deze bij */
PROCEDURE REGISTREER_ADRES
 (P_RELATIENUMMER IN NUMBER
 ,P_STRAATNAAM IN VARCHAR2
 ,P_HUISNUMMER IN NUMBER
 ,P_WOONPLAATS IN VARCHAR2
 ,P_HUISNUMMER_TOEVOEGING IN VARCHAR2
 ,P_POSTCODE IN VARCHAR2
 ,P_LANDCODE IN VARCHAR2
 ,P_LOCATIE IN VARCHAR2 := null
 ,P_IND_WOONBOOT IN VARCHAR2 := 'N'
 ,P_IND_WOONWAGEN IN VARCHAR2 := 'N'
 );
END RLE_OFMW_API;
 
/