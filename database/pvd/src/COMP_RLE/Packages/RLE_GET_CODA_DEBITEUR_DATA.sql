CREATE OR REPLACE PACKAGE comp_rle.RLE_GET_CODA_DEBITEUR_DATA IS
  gc_package                 constant varchar2(30) := 'RLE_GET_CODA_DEBITEUR_DATA';

  gc_type_admie_PENSIOEN     constant varchar2(20) := 'PENSIOEN';
  gc_type_admie_VERZEKERING  constant varchar2(20) := 'VERZEKERING';

  type r_debdata is record( relatienummer   varchar2(22)
                           ,crednmbr        varchar2(100)
                           ,debnmbr         varchar2(100)
                           ,name1_a         varchar2(50)
                           ,name1_b         varchar2(50)
                           ,street1         varchar2(300)
                           ,housenmbr       varchar2(20)
                           ,housenmbradd    varchar2(20)
                           ,zipcode         varchar2(50)
                           ,city            varchar2(50)
                           ,country         varchar2(3)
                           ,postalstraat1   varchar2(300)
                           ,postalhousenmbr varchar2(20)
                           ,postalhousenmbradd varchar2(20)
                           ,postalzipcode   varchar2(50)
                           ,postalcity      varchar2(50)
                           ,phone           varchar2(50)
                           ,fax             varchar2(50)
                           ,email           varchar2(500)
                           ,accountnmbr     varchar2(50)
                           ,swiftcode       varchar2(50)
                           ,ibancode        varchar2(50)
                           ,kvknummer       varchar2(20) -- 23072015 DRZ: CreditManagement(fase2/stroom A): toevoeging kvknr
                           ) ;

  procedure get_debiteur_data_voor_cmt
  ( p_elmcode    in  varchar2
  , p_type_admie in  varchar2 default null
  , p_debdata    out r_debdata
  );

END RLE_GET_CODA_DEBITEUR_DATA;
/