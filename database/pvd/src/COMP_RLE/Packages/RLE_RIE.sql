CREATE OR REPLACE PACKAGE comp_rle.RLE_RIE IS
/* ******************************************************************
PACKAGE: rle_rie

DOEL:    Package bevat programmatuur voor mutaties op RIE

CHANGES:
22-04-2008 XMK Creatie
05-12-2009 XKV Administratie ook via OVT
****************************************************************** */

/* Opvoeren relatiekoppeling in administratie voor RWG */
PROCEDURE KOP_RWG_CVC
 (P_WGR_WERKGNR IN VARCHAR2
 ,P_ADM_CODE IN VARCHAR2
 ,P_FUNC_CAT IN VARCHAR2
 ,P_DAT_BEGIN IN DATE
 ,P_DAT_EINDE IN DATE
 ,P_ERROR OUT VARCHAR
 );
/* Opvoeren relatiekoppeling in administratie voor WGR */
PROCEDURE KOP_WGR_WZD
 (P_WGR_WERKGNR IN VARCHAR2
 ,P_KODEGVW IN VARCHAR2
 ,P_KODESGW IN VARCHAR2
 ,P_DAT_BEGIN IN DATE
 ,P_DAT_EINDE IN DATE
 ,P_ERROR OUT VARCHAR
 );
/* Op basis van werkgve en admin. worden wrknermers gekoppeld aan admin */
PROCEDURE KOP_WNR_AVG
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ADM_CODE IN VARCHAR2
 ,P_ADM_TYPE IN VARCHAR2
 ,P_ERROR OUT VARCHAR2
 ,P_GBA_SYNC_INDIEN_AL_AANWEZIG IN VARCHAR2 := 'J'
 );
/* Opvoeren relatiekoppeling in administratie voor AVG */
PROCEDURE KOP_AVG_AVG
 (P_AVG_ID IN NUMBER
 ,P_DAT_BEGIN IN DATE
 ,P_DAT_EINDE IN DATE
 ,P_CODE_BEROEP IN VARCHAR2
 ,P_ERROR OUT VARCHAR
 ,P_GBA_SYNC_INDIEN_AL_AANWEZIG IN VARCHAR2 := 'J'
 );
/* Het koppelen van verwant deelnemer op basis administratie deelnemer */
PROCEDURE KOP_RKN_RIE
 (P_RLE_NUMRELATIE_DLNMR IN NUMBER
 ,P_RLE_NUMRELATIE_VRWNT IN NUMBER
 ,P_ERROR OUT VARCHAR
 );
/* Koppelen administratie n.a.v. waardeoverdracht */
PROCEDURE KOP_WOD
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ADM_CODE IN VARCHAR2
 ,P_ROL_CODROL IN VARCHAR2
 ,P_ERROR OUT VARCHAR2
 );
/* Het aanvullen van adm_codes van bron persoon naar doel persoon */
PROCEDURE AANVULLEN_ADM_CODES_PR
 (P_NUMRELATIE_BRON IN PLS_INTEGER
 ,P_NUMRELATIE_DOEL IN PLS_INTEGER
 );
END RLE_RIE;
 
/