CREATE OR REPLACE PACKAGE comp_rle.RLE_SYN_BESTAND IS


  --------------------------------------------------------------
  -- Doel:een bestand aanmaken voor T&T met daarin de personen waarvan
  -- wij vinden dat deze een afnemers indicatie JA moeten en mogen hebben.
  -- Het bestand voor de populatievergelijking bestaat uit een header met
  -- daarin de rubrieknummers  010110 (voor het A-nummer) en  Rubrieknummer 010120 (voor het BSN).
  -- De kolommmen zijn gescheiden door ; (puntkomma). De volgorde is A-nummer, BSN oplopend.
  --------------------------------------------------------------
  -- Wijzigingshistorie
  -- Wanneer    Wie Wat
  --------------------------------------------------------------
  -- 09-07-2013 Rudie Siwpersad   creatie
  -- 29-10-2013 XCW Diverse aanpassingen nav Requirements versie 5
  -- 27-12-2013 WHR aanpassing ivm ORU-10027: buffer overflow bij gebruik "p" procedure
  -- 30-12-2013 WHR aanpassing volgorde cursoren ivm performance
  -- 06-02-2014 WHR cursor c_dnp aanpassing selectie op ret_aanspraken
  --                performance verbetering doordat meer personen in de cursor gevonden worden
  --                hierdoor minder vaak doorrekenen totale aanspraken
  -- 18-02-2014 WHR cursor c_tzg toegevoegd selectie op ret_v_geldige_toezeggingen
  --                performance verbetering doordat meer personen in deze cursor gevonden worden
  --                hierdoor minder vaak doorrekenen totale aanspraken
  -- 13-03-2014 WHR HERBA deel 2
  -------------------------------------------------------------

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_MAANDEN_OVERLEDEN IN number := 2
 ,P_BEEINDIGD_NA_JO IN number := to_number(to_char(SYSDATE, 'yyyy'))
 );
END RLE_SYN_BESTAND;

 
/