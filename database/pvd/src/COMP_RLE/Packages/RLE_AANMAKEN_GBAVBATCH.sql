CREATE OR REPLACE PACKAGE comp_rle.RLE_AANMAKEN_GBAVBATCH IS

/********************************************************************
Package: RLE_AANMAKEN_GBAVBATCH

Doel   : Opvragen aanvullende gegevens (ex)partners bij GBA-V
De package wordt aangeroepen vanuit de RLEPRC63

Wanneer     Wie   Wat
----------  ----- ------------------------------------
08-10-2013  AZM   Creatie
04-03-2014 MAL   Div aanpassingen nav herontwerp
*********************************************************************/

type t_bestand_rec is record (naam   varchar2(200)
                             ,aantal number);

type t_gbavbestand_tab is table of t_bestand_rec index by pls_integer;

type t_fout_rec is record (bsn   varchar2(10)
                          ,dateinde_huwelijk date
                          ,fout varchar2(350));

type t_fout_tab is table of t_fout_rec index by pls_integer;


type l_gbav_rec is record
     (intern_kenmerk  rle_bron_ex_partners.intern_kenmerk%type
     ,bsn rle_bron_ex_partners.BSN_ex%type
     ,voornamen rle_bron_ex_partners.voornamen_ex%type
     ,geslachtsnaam rle_bron_ex_partners.naam_ex%type
     ,geboortedatum rle_bron_ex_partners.geboortedatum_ex%type
     ,geslachtsaanduiding rle_bron_ex_partners.geslacht_ex%type
     ,huisnummer    rle_bron_ex_partners.huisnummer_ex%type
     ,postcode      rle_bron_ex_partners.postcode_ex%type
     ,anummer      rle_bron_ex_partners.a_nummer_ex%type
     );

type t_gbav_tab  is
     table of  l_gbav_rec
     index by pls_integer;

  cn_status_gba_verwerking        constant varchar2( 10 ) := '10';
  cn_status_gba_aangevraagd       constant varchar2( 10 ) := '15';
  cn_status_gba_ontvangen         constant varchar2( 10 ) := '16';
  cn_status_aanvullend_aangevr    constant varchar2( 10 ) := '17';
  cn_status_aanvullend_exp_vast constant varchar2( 10 ) := '18';
  cn_status_aanvullen_product     constant varchar2( 10 ) := '25';
  cn_status_herbereken_bedragen   constant varchar2( 10 ) := '20';
  --cn_status_opvragen_aanvullend   constant varchar2( 10 ) := '20';
  cn_status_aanvullen_geg_par     constant varchar2( 10 ) := '27';
  cn_status_aanvullend_vastgel    constant varchar2( 10 ) := '30';
  cn_status_vastgelegd_in_rle     constant varchar2( 10 ) := '50'; --'35';
  cn_status_signaallijst          constant varchar2( 10 ) := '90';

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 ,P_BESTANDSNAAM IN VARCHAR2
 );
END RLE_AANMAKEN_GBAVBATCH;

 
/