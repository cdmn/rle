CREATE OR REPLACE PACKAGE comp_rle.RLE_PRODUCERS IS
/******************************************************************
  Package       RLE_PRODUCERS
  Beschrijving: Package met RLE event producers

  Datum       Wie  Wat
  ----------  ---  ------------------------------------------------
  06-01-2009  XCW  Aangemaakt
 ******************************************************************* */

rle_prs_producer_aktief varchar2(1) := 'N';

PROCEDURE RLE_PRS_EVENT_PRODUCER
 (P_NUMRELATIE ovt_ind_overeenkomsten.rle_numrelatie%TYPE
 ,P_DAT_BEGIN date := null
 );
END RLE_PRODUCERS;

 
/