CREATE OR REPLACE PACKAGE comp_rle.RLE_MVR_PTL IS

/* Verwerk procedure t.b.v. aanroep vanuit Cronacle */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 ,P_MV_TYPE IN VARCHAR2 := 'ALLES'
 );
END RLE_MVR_PTL;
 
/