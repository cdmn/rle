CREATE OR REPLACE PACKAGE comp_rle.RLE_MVR_PRSM IS
   /******************************************************************************
      NAME:       RLE_MVR_PRSM
      PURPOSE:    Verwerk procedure t.b.v. aanroep vanuit Cronacle.
                  --
                  Procedure is bedoeld voor de refresh van de materialized views
                  RLE_MV_PERSONEN_1 en RLE_MV_PERSONEN_2.
                  --
                  Er is een private synonym RLE_MV_PERSONEN, deze wijst naar de meest
                  recentelijk gerefreshte materialized view.
                  --
                  Deze package wordt via Cronacle gescheduled en de materialized
                  views RLE_MV_PERSONEN_1 en RLE_MV_PERSONEN_2 worden om en om gerefreshed.
                  --
                  Dit is gedaan om te zorgen voor een 100% beschikbaarheid van RLE_MV_PERSONEN.
                  --
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        20-01-2010  XJO              Created this package.
   ******************************************************************************/

PROCEDURE VERWERK
 ( P_SCRIPT_NAAM IN varchar2
 , P_SCRIPT_ID IN number
 );
END RLE_MVR_PRSM;

 
/