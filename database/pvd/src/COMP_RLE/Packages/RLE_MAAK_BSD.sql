CREATE OR REPLACE PACKAGE comp_rle.RLE_MAAK_BSD IS

CN_PACKAGE CONSTANT STM_UTIL.T_PROCEDURE%TYPE := 'RLE_MAAK_BSD';
CN_REVISION CONSTANT VARCHAR2(10) := '6';
   /******************************************************************************
      NAAM:       rle_maak_bsd
      DOEL:       aanmaken interface bestanden (o.a. richting OTIB)

      REVISIES:
      Versie     Datum       Auteur           Beschrijving
      ---------  ----------  ---------------  ------------------------------------
      1          11-02-2008  R.Podt      XRP  Aanmaak package
      5          31-03-2008  R.Podt      XRP  tellen van het "Aantal WNR" toegevoegd.
      6          13-01-2009  R.Podt      XRP  1) rechtsvorm optioneel
                                              2) geldigheidsperiode ext.code is verwijderd
                                              3) nvl om handelsnaam
   ******************************************************************************/
   cn_true            constant varchar2 (10)               := 'true';
   cn_false           constant varchar2 (10)               := 'false';
   cn_output_file     constant varchar2 (10)               := 'file';
   cn_output_screen   constant varchar2 (10)               := 'screen';
   -- Te overrulen globals
   g_output_type               varchar2 (30)               := cn_output_file;
   --
   g_use_access_path_wsf       varchar2 (10)               := cn_true;
   g_use_access_path_rle       varchar2 (10)               := cn_true;
   g_use_access_path_rwg       varchar2 (10)               := cn_true;
   --
   g_raise_exception_on_error  boolean                     := false;

/* starten van de batch verwerking voor OTIB */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN pls_integer
 ,P_BESTANDSGROEP IN VARCHAR2 := 'OTIB'
 ,P_PEILDATUM IN DATE := trunc(sysdate,'MON')
 ,P_VORIGE_PEILDATUM IN DATE := to_date(null)
 );
END RLE_MAAK_BSD;
 
/