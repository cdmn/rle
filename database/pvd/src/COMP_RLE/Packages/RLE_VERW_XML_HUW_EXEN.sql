CREATE OR REPLACE PACKAGE comp_rle.RLE_VERW_XML_HUW_EXEN IS
/*Doel: verwerking ontvangen huwelijkse gegevens in de GBA-V XML resultaat en fouten bestand
Aanroep vanuit:  RLEPRC63
  Package specification  : rle_verw_xml_huw_exen

  Deze package doet de verwerking van GBA-V XML bestanden
  De package wordt aangeroepen vanuit de RLEPRC63
  De bestanden moeten worden neergezet in de shared pool
  Worden vervolgens opgepakt en neergezet in tabel rle_huwelijken_exp_gbav
  -----------------------------------------------------------------------------------------------------------
  Datum       Auteur         Wijzigingen
  ==========  =============  =====================================
  07-10-2013  AZM            Creatie
  26-02-2014  MAL            Diverse aanpassingen nav herontwerp
  -----------------------------------------------------------------------------------------------------------*/

/* Converteer de text */
FUNCTION CONVERT_TXT
 (P_TEXT IN VARCHAR2
 )
 RETURN VARCHAR2;
/* Verwerken huwelijkse gegevens ex-partners */
PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 );
END RLE_VERW_XML_HUW_EXEN;
 
/