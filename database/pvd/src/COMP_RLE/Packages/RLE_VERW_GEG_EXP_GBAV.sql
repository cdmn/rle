CREATE OR REPLACE PACKAGE comp_rle.RLE_VERW_GEG_EXP_GBAV IS

/********************************************************************
Package: COMP_RLE.RLE_VERW_GEG_EXP_GBAV

Doel   : Verwerken ontvangen berichten van GBA-V
De package wordt aangeroepen vanuit de CNVPRC241

Wanneer     Wie   Wat
----------  ----- ------------------------------------
25-10-2013  AZM   Creatie
10-3-2014   mal     Div wijzigingen nav herontwerp
*********************************************************************/
  -- csv verwerkingsverslagen
  type l_text_rec is record
  (regel varchar2(3000));
  type t_text_tab  is
  table of  l_text_rec
  index by pls_integer;

  type t_fout_rec is record (bsn   varchar2(10)
                            ,dateinde_huwelijk date
                            ,fout varchar2(350));

  type t_fout_tab is table of t_fout_rec index by pls_integer;

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 );
END RLE_VERW_GEG_EXP_GBAV;

 
/