CREATE OR REPLACE PACKAGE comp_rle.RLE_RKN IS


  /******************************************************************************
    PACKAGE:    RLE_RKN
    DOEL:       Package Relatie Koppeling Functionaliteiten

    Wie    Wanneer         Wat
    -----  -------------   --------------------------------
    XCW    25-10-2012      Creatie

  ******************************************************************************/
  --
  g_package           varchar2 ( 100 ) := 'RLE_RKN';

  --

FUNCTION GET_SCHEIDING_PARTNER_ENABLED
 RETURN BOOLEAN;
PROCEDURE SET_SCHEIDING_PARTNER_ENABLED
 (P_SCHEIDING_PARTNER IN boolean
 );
/* Verwijderen van relatie koppeling(en). */
PROCEDURE VERWIJDER_RKN
 (P_RELATIENUMMER IN NUMBER
 ,P_RELATIENUMMER_VOOR IN NUMBER
 ,P_ROL_IN_KOPPELING IN VARCHAR2
 ,P_DAT_BEGIN IN DATE
 );
END RLE_RKN;

 
/