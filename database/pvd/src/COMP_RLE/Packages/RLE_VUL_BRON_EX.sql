CREATE OR REPLACE PACKAGE comp_rle.RLE_VUL_BRON_EX IS
/*Doel:     Vastleggen relatie en partnergegevens in de brontabel
Aanroep:  RLEPRC61

 ------------------------------------------------------------------------------
 Datum             Auteur                      Wijzigingen
 ==========  =============  =====================================
 20-08-2013    XSW                         Creatie
24-2-2014       MAL                         Div aanpassingen nav herontwerp
10-04-2014   MAL                           Als de scheiding bekend is in RLE, ook de adm toevoegen
*/
type t_bestand_rec is record (naam   varchar2(200)
                             ,aantal number);

type t_gbav_tab is table of t_bestand_rec index by pls_integer;

type t_fout_rec is record (bsn   varchar2(10)
                          ,dateinde_huwelijk date
                          ,fout varchar2(350));
type t_fout_tab is table of t_fout_rec index by pls_integer;

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 );
END RLE_VUL_BRON_EX;
 
/