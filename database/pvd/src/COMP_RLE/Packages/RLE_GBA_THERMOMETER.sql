CREATE OR REPLACE PACKAGE comp_rle.RLE_GBA_THERMOMETER IS


  -- Author  : MAL
  -- Created : 29-07-2014
  -- Purpose : Rapporteren over de verschillen die zijn vastgesteld in het verschillenverslag van T&T
  -- Functie RLE1022

PROCEDURE VERWERK
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN number
 );
END RLE_GBA_THERMOMETER;

 
/