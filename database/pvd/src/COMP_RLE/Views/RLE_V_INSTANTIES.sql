CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_instanties (instantienummer,instantienaam,locatie_omschrijving,postcode,huisnummer,huisnummer_toevoeging,straatnaam,woonplaats,land,soort) AS
SELECT to_number(rec.extern_relatie) instantienummer
         ,rle.namrelatie instantienaam
         ,null locatie_omschrijving
         ,ads.postcode postcode
         ,ads.huisnummer huisnummer
         ,ads.toevhuisnum huisnummer_toevoeging
         ,stt.straatnaam straatnaam
         ,wps.woonplaats woonplaats
         ,lnd.naam land
         ,null soort
   from   rle_adressen ads
         ,rle_landen lnd
         ,rle_straten stt
         ,rle_woonplaatsen wps
         ,rle_relatie_externe_codes rec
         ,rle_relatie_adressen ras
         ,rle_relaties rle
  WHERE rec.rle_numrelatie = rle.numrelatie
   and    rle.numrelatie = ras.rle_numrelatie(+)
   and    'CA' = ras.dwe_wrddom_srtadr(+)
   and    ras.ads_numadres = ads.numadres(+)
   and    ads.lnd_codland = lnd.codland(+)
   and    ads.stt_straatid = stt.straatid(+)
   and    ads.wps_woonplaatsid = wps.woonplaatsid(+)
   and    rec.rol_codrol = 'UI'
 ;