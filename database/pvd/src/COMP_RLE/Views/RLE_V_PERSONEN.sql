CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_personen (persoonsnummer,sofinummer,relatienummer,numrelatie,naam,namrelatie,zoeknaam,titel,tussentitel,voorvoegsels,achtervoegsels,geslacht,burgerlijke_staat,gewenste_aanschrijftitel,voornaam,voorletters,voorletter,aanschrijfnaam,geboortedatum,datgeboorte,code_geboortedatum_fictief,datum_overlijden,datum_emigratie,naam_partner,voorvoegsels_partner,voorkeursnaam,voorkeursvoorvoegsels,volledige_naam1,volledige_naam2,volledige_naam3) AS
SELECT prs.extern_relatie, sofi.extern_relatie, rle.numrelatie,
          rle.numrelatie, rle.namrelatie, rle.namrelatie, rle.zoeknaam,
          rle.dwe_wrddom_titel, rle.dwe_wrddom_ttitel, vvgsl.waarde,
          avgsl.waarde, geslacht.waarde, rle.dwe_wrddom_brgst,
          rle.dwe_wrddom_gewatitel, rle.voornaam,
          fnc_bepaal_voorletters (rle.voorletter), rle.voorletter,
          rle.aanschrijfnaam, rle.datgeboorte, rle.datgeboorte,
          rle.code_gebdat_fictief, rle.datoverlijden, rle.datemigratie,
          rle.naam_partner, LOWER (rle.vvgsl_partner),
          rle_get_voorkeursnaam (rle.namrelatie,
                                 vvgsl.waarde,
                                 rle.naam_partner,
                                 LOWER (rle.vvgsl_partner),
                                 rle.code_aanduiding_naamgebruik
                                ),
          CASE rle.code_aanduiding_naamgebruik
	       WHEN 'E' THEN vvgsl.waarde
               WHEN   'P'THEN LOWER (rle.vvgsl_partner)
               WHEN   'V'THEN LOWER (rle.vvgsl_partner)
               WHEN   'N'THEN vvgsl.waarde
                         ELSE   vvgsl.waarde
                 END
                 ,
             LTRIM (fnc_bepaal_voorletters (rle.voorletter) || ' ')
          || LTRIM (   CASE rle.code_aanduiding_naamgebruik
	       WHEN 'E' THEN vvgsl.waarde
               WHEN   'P'THEN LOWER (rle.vvgsl_partner)
               WHEN   'V'THEN LOWER (rle.vvgsl_partner)
               WHEN   'N'THEN vvgsl.waarde
                         ELSE   vvgsl.waarde
                 END
                    || ' '
                   )
          || rle_get_voorkeursnaam (rle.namrelatie,
                                    vvgsl.waarde,
                                    rle.naam_partner,
                                    LOWER (rle.vvgsl_partner),
                                    rle.code_aanduiding_naamgebruik
                                   )
          || RTRIM (' ' || avgsl.waarde),
             RTRIM (   rle_get_voorkeursnaam (rle.namrelatie,
                                              vvgsl.waarde,
                                              rle.naam_partner,
                                              LOWER (rle.vvgsl_partner),
                                              rle.code_aanduiding_naamgebruik
                                             )
                    || ' '
                    || avgsl.waarde
                   )
          || ', '
          || RTRIM (fnc_bepaal_voorletters (rle.voorletter) || ' ')
          || CASE rle.code_aanduiding_naamgebruik
	       WHEN 'E' THEN vvgsl.waarde
               WHEN   'P'THEN LOWER (rle.vvgsl_partner)
               WHEN   'V'THEN LOWER (rle.vvgsl_partner)
               WHEN   'N'THEN vvgsl.waarde
                         ELSE   vvgsl.waarde
                         END,
          SUBSTR (   CASE rle.dwe_wrddom_geslacht
	             WHEN 'M' THEN 'dhr. '
		     WHEN 'V' THEN 'mevr. '
		     END
	          || LTRIM (fnc_bepaal_voorletters (rle.voorletter) || ' ')
                  || LTRIM (   CASE rle.code_aanduiding_naamgebruik
                                  WHEN 'E'
                                     THEN vvgsl.waarde
                                  WHEN 'P'
                                     THEN LOWER (rle.vvgsl_partner)
                                  WHEN 'V'
                                     THEN LOWER (rle.vvgsl_partner)
                                  WHEN 'N'
                                     THEN vvgsl.waarde
                                  ELSE vvgsl.waarde
                               END
                            || ' '
                           )
                  || rle_get_voorkeursnaam (rle.namrelatie,
                                            LOWER (rle.dwe_wrddom_vvgsl),
                                            rle.naam_partner,
                                            LOWER (rle.vvgsl_partner),
                                            rle.code_aanduiding_naamgebruik
                                           )
                  || RTRIM (' ' || rle.dwe_wrddom_avgsl),
                  1,
                  255
                 )
from   comp_rle.rle_relaties rle
,      comp_rle.rle_relatie_externe_codes prs
,      comp_rle.rle_relatie_externe_codes sofi
,      domeinwaarden vvgsl
,      domeinwaarden geslacht
,      domeinwaarden avgsl
  WHERE RLE.NUMRELATIE = PRS.RLE_NUMRELATIE
AND    PRS.ROL_CODROL = 'PR'
AND    PRS.DWE_WRDDOM_EXTSYS = 'PRS'
AND    RLE.NUMRELATIE = SOFI.RLE_NUMRELATIE(+)
AND    SOFI.ROL_CODROL(+) = 'PR'
AND    SOFI.DWE_WRDDOM_EXTSYS(+) = 'SOFI'
AND    VVGSL.DOMEINCODE(+) = 'VVL'
AND    VVGSL.CODE(+) = RLE.DWE_WRDDOM_VVGSL
AND    AVGSL.DOMEINCODE(+) = 'AVL'
AND    AVGSL.CODE(+) = RLE.DWE_WRDDOM_AVGSL
AND    GESLACHT.DOMEINCODE = 'GES'
AND    GESLACHT.CODE = RLE.DWE_WRDDOM_GESLACHT
 ;