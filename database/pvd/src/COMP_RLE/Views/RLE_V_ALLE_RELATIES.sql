CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_alle_relaties (numrelatie,extern_relatie,namrelatie,voornaam,voorletter,aanschrijfnaam,naam_partner,vvgsl_partner,zoeknaam,codorganisatie,indgewnaam,indinstelling,datschoning,dwe_wrddom_titel,dwe_wrddom_ttitel,dwe_wrddom_avgsl,dwe_wrddom_vvgsl,dwe_wrddom_brgst,dwe_wrddom_geslacht,dwe_wrddom_gewatitel,dwe_wrddom_rvorm,datgeboorte,code_gebdat_fictief,datoverlijden,datoprichting,datbegin,datcontrole,dateinde,indcontrole,regdat_overlbevest,datemigratie,code_aanduiding_naamgebruik,handelsnaam,"ID",dwe_wrddom_extsys,datingang,ind_geverifieerd,cod_dateinde,codrol,indgroepsrol,indschonen,indwijzigbaar,omsrol,prioriteit,rle_vrol_plus_rol_codrol,codpresentatie,rrl_datschoning,naam_sgs) AS
SELECT rel.numrelatie,
cod.extern_relatie,
rel.namrelatie,
rel.voornaam,
rel.voorletter,
rel.aanschrijfnaam,
rel.naam_partner,
rel.vvgsl_partner,
rel.zoeknaam,
rel.codorganisatie,
rel.indgewnaam,
rel.indinstelling,
rel.datschoning,
rel.dwe_wrddom_titel,
rel.dwe_wrddom_ttitel,
rel.dwe_wrddom_avgsl,
rel.dwe_wrddom_vvgsl,
rel.dwe_wrddom_brgst,
rel.dwe_wrddom_geslacht,
rel.dwe_wrddom_gewatitel,
rel.dwe_wrddom_rvorm,
rel.datgeboorte,
rel.code_gebdat_fictief,
rel.datoverlijden,
rel.datoprichting,
rel.datbegin,
rel.datcontrole,
rel.dateinde,
rel.indcontrole,
rel.regdat_overlbevest,
rel.datemigratie,
rel.code_aanduiding_naamgebruik,
rel.handelsnaam,
cod.id,
cod.dwe_wrddom_extsys,
cod.datingang,
cod.ind_geverifieerd,
cod.dateinde cod_dateinde,
rol.codrol,
rol.indgroepsrol,
rol.indschonen,
rol.indwijzigbaar,
rol.omsrol,
rol.prioriteit,
rol.rle_vrol_plus_rol_codrol,
rrl.codpresentatie,
rrl.datschoning rrl_datschoning
, substr(decode(rel.dwe_wrddom_geslacht,'M','dhr. ','V','mevr. ')
       ||ltrim(fnc_bepaal_voorletters(REL.VOORLETTER)||' ')
       ||ltrim(decode(rel.code_aanduiding_naamgebruik, 'E', lower(rel.dwe_wrddom_vvgsl), 'P', lower(REL.VVGSL_PARTNER), 'V', lower(REL.VVGSL_PARTNER), 'N', lower(rel.dwe_wrddom_vvgsl), lower(rel.dwe_wrddom_vvgsl))||' ')
       ||rle_get_voorkeursnaam(REL.NAMRELATIE, lower(rel.dwe_wrddom_vvgsl), REL.NAAM_PARTNER, lower(REL.VVGSL_PARTNER), REL.CODE_AANDUIDING_NAAMGEBRUIK)
       ||rtrim(' '||rel.dwe_wrddom_avgsl),1,255) naam_sgs
from rle_rollen rol
, rle_relaties rel
, rle_relatie_externe_codes cod
, rle_relatie_rollen rrl
  WHERE cod.rol_codrol = rol.codrol
and rrl.rol_codrol = rol.codrol
and cod.rle_numrelatie = rel.numrelatie
and rrl.rle_numrelatie = rel.numrelatie
and  ((rol.codrol = 'PR' and cod.dwe_wrddom_extsys = 'PRS') or (rol.codrol = 'WG'))
 ;