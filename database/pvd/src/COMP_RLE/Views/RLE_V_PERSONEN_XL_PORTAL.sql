CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_personen_xl_portal (persoonsnr,numrelatie,sofinummer,datgeboorte,zoeknaam,pens_admin_id,wgr_personeelsnummer,burgelijke_staat,namrelatie,voornaam,da_straat,da_huisnummer,da_huisnummer_tv,da_postcode,da_woonplaats,naam_partner,datoverlijden,volledige_naam1,volledige_naam2,da_landcode,da_landnaam,oms_geslacht,a_nummer,kanaalvoorkeur,kruispuntteam_naam,kruispuntteam_tel,gba_status,afnemers_indicatie,oa_straat,oa_huisnummer,oa_huisnummer_tv,oa_postcode,oa_woonplaats,oa_landcode,oa_landnaam,ca_straat,ca_huisnummer,ca_huisnummer_tv,ca_postcode,ca_woonplaats,ca_landcode,ca_landnaam,quality_tag) AS
SELECT zwn.persoonsnummer persoonsnr
       , zwn.relatienummer numrelatie
       , zwn.sofinummer
       , zwn.datgeboorte
       , zwn.zoeknaam
       , zwn.pvachmea_persoonsnummer pens_admin_id
       , zwn.wgr_personeelsnummer
       , ( select rkg.omschrijving
          from rle_relatie_koppelingen rrk
             , rle_rol_in_koppelingen rkg
          where rrk.rle_numrelatie = zwn.relatienummer
                and rkg.code_rol_in_koppeling in ('H', 'S')
                and rrk.rkg_id = rkg.id
                and rrk.dat_begin <= sysdate
                and ( rrk.dat_einde is null
                     or rrk.dat_einde > sysdate ) )
       , rel.namrelatie
       , rel.voornaam
       , rle_pers.get_string_attr ( 'Attr16'
                                  , adresstring
                                  ) da_straat
       , to_number ( rle_pers.get_string_attr ( 'Attr12'
                                              , adresstring
                                              ) ) da_huisnummer
       , rle_pers.get_string_attr ( 'Attr13'
                                  , adresstring
                                  ) da_huisnummer_tv
       , zwn.da_postcode
       , zwn.da_woonplaats
       , zwn.naam_partner
       , zwn.datoverlijden
       , rle_pers.get_string_attr ( 'Attr21'
                                  , adresstring
                                  ) volledige_naam1
       , rtrim ( rle_get_voorkeursnaam ( rel.namrelatie
                                       , vvgsl.waarde
                                       , rel.naam_partner
                                       , lower ( rel.vvgsl_partner )
                                       , rel.code_aanduiding_naamgebruik
                                       ) || ' ' || decode(avgsl.waarde, null, null, avgsl.waarde || ' ') ) || ', '  ||
           trim(fnc_bepaal_voorletters ( rel.voorletter )) ||
           case rel.code_aanduiding_naamgebruik
           when 'E'
             then decode(vvgsl.waarde     , null, null, ' ' || vvgsl.waarde)
           when 'P'
             then decode(rel.vvgsl_partner, null, null, ' ' || lower ( rel.vvgsl_partner ))
           when 'V'
             then decode(rel.vvgsl_partner, null, null, ' ' || lower ( rel.vvgsl_partner ))
           when 'N'
             then decode(vvgsl.waarde     , null, null, ' ' || vvgsl.waarde)
           else   decode(vvgsl.waarde     , null, null, ' ' || vvgsl.waarde)
         end volledige_naam2
--       , rle_pers.get_string_attr ( 'Attr22'
--                                  , adresstring
--                                  ) volledige_naam2
       , rle_pers.get_string_attr ( 'Attr17'
                                  , adresstring
                                  ) da_landcode
       , rle_pers.get_string_attr ( 'Attr20'
                                  , adresstring
                                  ) da_landnaam
       , rle_pers.get_pers_omschr ( 'GES'
                                  , rel.dwe_wrddom_geslacht
                                  , 'N'
                                  ) oms_geslacht
       ,zwn.a_nummer
       , rle_pers.get_kanaal ( rel.numrelatie ) kanaalvoorkeur
       , rle_pers.get_string_attr ( 'Attr34'
                                  , adresstring
                                  ) kruispuntteam_naam
       , rle_pers.get_string_attr ( 'Attr35'
                                  , adresstring
                                  ) kruispuntteam_tel
       , rle_pers.get_gba ( 'STATUS'
                          , rel.numrelatie
                          ) gba_status
       , rle_pers.get_gba ( 'INDICATIE'
                                   , rel.numrelatie
                                   ) afnemers_indicatie
       , rle_pers.get_string_attr ( 'Attr06'
                                  , adresstring
                                  ) oa_straat
       , to_number ( rle_pers.get_string_attr ( 'Attr02'
                                              , adresstring
                                              ) ) oa_huisnummer
       , rle_pers.get_string_attr ( 'Attr03'
                                  , adresstring
                                  ) oa_huisnummer_tv
       , rle_pers.get_string_attr ( 'Attr01'
                                  , adresstring
                                  ) oa_postcode
       , rle_pers.get_string_attr ( 'Attr04'
                                  , adresstring
                                  ) oa_woonplaats
       , rle_pers.get_string_attr ( 'Attr07'
                                  , adresstring
                                  ) oa_landcode
       , rle_pers.get_string_attr ( 'Attr10'
                                  , adresstring
                                  ) oa_landnaam
       , rle_pers.get_string_attr ( 'Attr29'
                                  , adresstring
                                  ) ca_straat
       , to_number ( rle_pers.get_string_attr ( 'Attr25'
                                              , adresstring
                                              ) ) ca_huisnummer
       , rle_pers.get_string_attr ( 'Attr26'
                                  , adresstring
                                  ) ca_huisnummer_tv
       , rle_pers.get_string_attr ( 'Attr24'
                                  , adresstring
                                  ) ca_postcode
       , rle_pers.get_string_attr ( 'Attr27'
                                  , adresstring
                                  ) ca_woonplaats
       , rle_pers.get_string_attr ( 'Attr30'
                                  , adresstring
                                  ) ca_landcode
       , rle_pers.get_string_attr ( 'Attr33'
                                  , adresstring
                                  ) ca_landnaam
       , comp_qtg.qtg_lib.indicatie_quality_tag_adm( p_numrelatie => numrelatie
                                                    ,p_adm_code   => stm_context.administratie) quality_tag
  from   ( select rle.*
                , rle_pers.get_adres ( numrelatie ) adresstring
             from rle_relaties rle ) rel
       , rle_mv_zoek_werknemer_syn   zwn
       , domeinwaarden               vvgsl
       , domeinwaarden               geslacht
       , domeinwaarden               avgsl
  WHERE zwn.relatienummer = rel.numrelatie
and vvgsl.domeincode(+)       = 'VVL'
and vvgsl.code(+)             = rel.dwe_wrddom_vvgsl
and avgsl.domeincode(+)       = 'AVL'
and avgsl.code(+)             = rel.dwe_wrddom_avgsl
and geslacht.domeincode       = 'GES'
and geslacht.code             = rel.dwe_wrddom_geslacht
and    ( nvl ( stm_context.administratie, 'ALG' ) = 'ALG'
         or exists (
             select 1
             from   rle_relatierollen_in_adm  rel
             ,      rle_administraties        adm
             where  rel.rol_codrol     = 'PR'
             and    rel.rle_numrelatie = zwn.relatienummer
             and    adm.code = rel.adm_code
             and    adm.type = 'P'
             and    adm.code = stm_context.administratie )
       );