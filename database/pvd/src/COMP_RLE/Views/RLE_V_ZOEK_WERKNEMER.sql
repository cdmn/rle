CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_zoek_werknemer (relatienummer,zoeknaam,datgeboorte,datoverlijden,naam_partner,persoonsnummer,sofinummer,pvachmea_persoonsnummer,wgr_personeelsnummer,da_postcode,da_woonplaats,a_nummer) AS
SELECT rel.numrelatie relatienummer
      , convert(upper(regexp_replace(rel.namrelatie, '\ *\&\ *', ' EN ')), 'US7ASCII')  zoeknaam
       , rel.datgeboorte
       , rel.datoverlijden
       , rel.naam_partner
       , prs.extern_relatie persoonsnummer
       , sof.extern_relatie sofinummer
       , pens.extern_relatie pvachmea_persoonsnummer
       , pwgr.extern_relatie wgr_personeelsnummer
       , da.postcode da_postcode
       , da.woonplaats da_woonplaats
       , anum.extern_relatie a_nummer
  from   rle_relaties rel
       , rle_relatie_externe_codes prs
       , rle_relatie_externe_codes sof
       , rle_relatie_externe_codes pens
       , rle_relatie_externe_codes pwgr
       , ( select ads.postcode
                , wpl.woonplaats
                , ads.huisnummer
                , ras.rle_numrelatie
          from   rle_relatie_adressen ras
               , rle_adressen ads
               , rle_woonplaatsen wpl
          where  ras.ads_numadres = ads.numadres
          and    ads.wps_woonplaatsid = wpl.woonplaatsid
          and    trunc( sysdate ) between ras.datingang and nvl ( ras.dateinde
                                                       , trunc( sysdate )
                                                       )
          and    ras.dwe_wrddom_srtadr = 'DA'
          and    ras.rol_codrol = 'PR' ) da
        , rle_relatie_externe_codes anum
  WHERE prs.rle_numrelatie = rel.numrelatie
  and    rel.numrelatie = sof.rle_numrelatie(+)
  and    sysdate between sof.datingang(+) and nvl ( sof.dateinde(+)
                                                  , sysdate
                                                  )
  and    sof.dwe_wrddom_extsys(+) = 'SOFI'
  and    sof.rol_codrol(+) = 'PR'
  and    prs.dwe_wrddom_extsys = 'PRS'
  and    prs.rol_codrol = 'PR'
  and    trunc ( prs.datingang ) <= trunc ( sysdate )
  and    nvl ( prs.dateinde
             , trunc ( sysdate )
             ) >= trunc ( sysdate )
  and    rel.indinstelling = 'N'   -- Alleen personen
  and    rel.numrelatie = da.rle_numrelatie(+)
  and    rel.numrelatie = pens.rle_numrelatie(+)
  and    trunc ( pens.datingang(+) ) <= trunc ( sysdate )
  and    nvl ( pens.dateinde(+)
             , trunc ( sysdate )
             ) >= trunc ( sysdate )
  and    pens.dwe_wrddom_extsys(+) = 'PENS'
  and    rel.numrelatie = pwgr.rle_numrelatie(+)
  and    trunc ( pwgr.datingang(+) ) <= trunc ( sysdate )
  and    nvl ( pwgr.dateinde(+)
             , trunc ( sysdate )
             ) >= trunc ( sysdate )
  and    pwgr.dwe_wrddom_extsys(+) = 'PWGR'
and    rel.numrelatie = anum.rle_numrelatie(+)
  and    trunc ( anum.datingang(+) ) <= trunc ( sysdate )
  and    nvl ( anum.dateinde(+)
             , trunc ( sysdate )
             ) >= trunc ( sysdate )
  and    anum.dwe_wrddom_extsys(+) = 'GBA'
  and    anum.rol_codrol(+) = 'PR'
 ;