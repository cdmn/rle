CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_adm_kantoren (rle_numrelatie,rle_namrelatie,ca_straatnaam,ca_huisnummer,ca_toevhuisnum,ca_postcode,ca_woonplaats,ca_landnaam,ca_telefoon_cpn,ca_naam_cpn,fa_straatnaam,fa_huisnummer,fa_toevhuisnum,fa_postcode,fa_woonplaats,fa_landnaam,fa_telefoon_cpn,fa_naam_cpn,ak_telefoon,ak_email) AS
SELECT rle.numrelatie numrelatie
     , rle.namrelatie namrelatie
     , stt_ca.straatnaam ca_straatnaam
     , ads_ca.huisnummer ca_huisnummer
     , ads_ca.toevhuisnum ca_toevhuisnum
     , ads_ca.postcode ca_postcode
     , wps_ca.woonplaats ca_woonplaats
     , lnd_ca.naam ca_naam_lnd
     , ( select cnr_ca.numcommunicatie
        from   rle_communicatie_nummers cnr_ca
             , rle_contactpersonen cpn_ca
        where  cpn_ca.rle_numrelatie = cnr_ca.rle_numrelatie
        and    cnr_ca.rol_codrol = 'CP'
        and    cnr_ca.dwe_wrddom_srtcom = 'TEL'
        and    cnr_ca.adm_code = 'ALG'
        and    cnr_ca.datingang <= sysdate
        and    nvl( cnr_ca.dateinde
                  , sysdate
                  ) >= sysdate
        and    ras_ca.id = cpn_ca.ras_id
        and    cpn_ca.dat_creatie = ( select max( cpn_ca_max.dat_creatie )
                                     from   rle_contactpersonen cpn_ca_max
                                     where  cpn_ca_max.ras_id = cpn_ca.ras_id )) ca_telefoon_cpn
     , rle_get_m11_rle_naw( 255
                          , ( select cpn_ca.rle_numrelatie
                             from   rle_contactpersonen cpn_ca
                             where  ras_ca.id = cpn_ca.ras_id
                             and    cpn_ca.dat_creatie = ( select max( cpn_ca_max.dat_creatie )
                                                          from   rle_contactpersonen cpn_ca_max
                                                          where  cpn_ca_max.ras_id = cpn_ca.ras_id ))
                          ) ca_naam_cpn
     , stt_fa.straatnaam fa_straatnaam
     , ads_fa.huisnummer fa_huisnummer
     , ads_fa.toevhuisnum fa_toevhuisnum
     , ads_fa.postcode fa_postcode
     , wps_fa.woonplaats fa_woonplaats
     , lnd_fa.naam fa_naam_lnd
     , ( select cnr_fa.numcommunicatie
        from   rle_communicatie_nummers cnr_fa
             , rle_contactpersonen cpn_fa
        where  cpn_fa.rle_numrelatie = cnr_fa.rle_numrelatie
        and    cnr_fa.rol_codrol = 'CP'
        and    cnr_fa.dwe_wrddom_srtcom = 'TEL'
        and    cnr_fa.adm_code = 'ALG'
        and    cnr_fa.datingang <= sysdate
        and    nvl( cnr_fa.dateinde
                  , sysdate
                  ) >= sysdate
        and    ras_fa.id = cpn_fa.ras_id
        and    cpn_fa.dat_creatie = ( select max( cpn_fa_max.dat_creatie )
                                     from   rle_contactpersonen cpn_fa_max
                                     where  cpn_fa_max.ras_id = cpn_fa.ras_id )) fa_telefoon_cpn
     , rle_get_m11_rle_naw( 255
                          , ( select cpn_fa.rle_numrelatie
                             from   rle_contactpersonen cpn_fa
                             where  ras_fa.id = cpn_fa.ras_id
                             and    cpn_fa.dat_creatie = ( select max( cpn_fa_max.dat_creatie )
                                                          from   rle_contactpersonen cpn_fa_max
                                                          where  cpn_fa_max.ras_id = cpn_fa.ras_id ))
                          ) fa_naam_cpn
     , ( select cnr_ak.numcommunicatie
        from   rle_communicatie_nummers cnr_ak
        where  cnr_ak.rle_numrelatie = rec.rle_numrelatie
        and    cnr_ak.rol_codrol = 'AK'
        and    cnr_ak.dwe_wrddom_srtcom = 'TEL'
        and    cnr_ak.adm_code = 'ALG'
        and    cnr_ak.datingang <= sysdate
        and    nvl( cnr_ak.dateinde
                  , sysdate
                  ) >= sysdate
        and    cnr_ak.ras_id is null ) ak_telefoon
     , ( select cnr_ak.numcommunicatie
        from   rle_communicatie_nummers cnr_ak
        where  cnr_ak.rle_numrelatie    = rec.rle_numrelatie
        and    cnr_ak.rol_codrol        = 'AK'
        and    cnr_ak.dwe_wrddom_srtcom = 'EMAIL'
        and    cnr_ak.adm_code          = 'ALG'
        and    cnr_ak.datingang        <= sysdate
        and    nvl( cnr_ak.dateinde
                  , sysdate
                  )                    >= sysdate
        and    cnr_ak.ras_id is null ) ak_email
from   rle_relatie_externe_codes rec
     , rle_relaties rle
     , rle_relatie_adressen ras_ca
     , rle_adressen ads_ca
     , rle_straten stt_ca
     , rle_landen lnd_ca
     , rle_woonplaatsen wps_ca
     , rle_relatie_adressen ras_fa
     , rle_adressen ads_fa
     , rle_straten stt_fa
     , rle_landen lnd_fa
     , rle_woonplaatsen wps_fa
where  rec.rle_numrelatie = rle.numrelatie
and    rec.dwe_wrddom_extsys = 'AKR'
and    rle.numrelatie = ras_ca.rle_numrelatie(+)
and    ras_ca.dwe_wrddom_srtadr(+) = 'CA'
and    ras_ca.datingang(+) <= sysdate
and    nvl( ras_ca.dateinde(+)
          , sysdate
          ) >= sysdate
and    ras_ca.ads_numadres = ads_ca.numadres(+)
and    ads_ca.stt_straatid = stt_ca.straatid(+)
and    ads_ca.wps_woonplaatsid = wps_ca.woonplaatsid(+)
and    ads_ca.lnd_codland = lnd_ca.codland(+)
and    rle.numrelatie = ras_fa.rle_numrelatie(+)
and    ras_fa.dwe_wrddom_srtadr(+) = 'FA'
and    ras_fa.datingang(+) <= sysdate
and    nvl( ras_fa.dateinde(+)
          , sysdate
          ) >= sysdate
and    ras_fa.ads_numadres = ads_fa.numadres(+)
and    ads_fa.stt_straatid = stt_fa.straatid(+)
and    ads_fa.wps_woonplaatsid = wps_fa.woonplaatsid(+)
and    ads_fa.lnd_codland = lnd_fa.codland(+)
 ;