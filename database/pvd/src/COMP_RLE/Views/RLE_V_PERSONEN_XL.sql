CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_personen_xl (namrelatie,zoeknaam,indinstelling,cod_geslacht,oms_geslacht,cod_vvgsl,oms_vvgsl,oms_vvgsl_portal,voorletter,voornaam,datgeboorte,cod_datgeb_fictief,oms_datgeb_fictief,indcontrole,cod_naamgebruik,oms_naamgebruik,cod_aanschrijftitel,oms_aanschrijftitel,datoverlijden,regdatum_overlbevest,datemigratie,dat_creatie,creatie_door,sofinummer,naam_partner,voorvoegsel_partner,oms_voorvoegsel_partner,numrelatie,persoonsnr,pens_admin_id,wgr_personeelsnummer,a_nummer,kanaalvoorkeur,gba_status,afnemers_indicatie,oa_postcode,oa_huisnummer,oa_huisnummer_tv,oa_woonplaats,oa_locatie,oa_straat,oa_landcode,oa_ind_woonboot,oa_ind_woonwagen,oa_landnaam,da_postcode,da_huisnummer,da_huisnummer_tv,da_woonplaats,da_locatie,da_straat,da_landcode,da_ind_woonboot,da_ind_woonwagen,da_landnaam,pensioenadministratie,volledige_naam1,volledige_naam2,volledige_naam3,ca_postcode,ca_huisnummer,ca_huisnummer_tv,ca_woonplaats,ca_locatie,ca_straat,ca_landcode,ca_ind_woonboot,ca_ind_woonwagen,ca_landnaam,kruispuntteam_naam,kruispuntteam_tel,oa_gemeentenaam,da_gemeentenaam,ca_gemeentenaam) AS
SELECT pers.namrelatie
       , pers.zoeknaam
       , pers.indinstelling
       , pers.cod_geslacht
       , pers.oms_geslacht
       , pers.cod_vvgsl
       , pers.oms_vvgsl
       , pers.oms_vvgsl_portal
       , pers.voorletter
       , pers.voornaam
       , pers.datgeboorte
       , pers.cod_datgeb_fictief
       , pers.oms_datgeb_fictief
       , pers.indcontrole
       , pers.cod_naamgebruik
       , pers.oms_naamgebruik
       , pers.cod_aanschrijftitel
       , pers.oms_aanschrijftitel
       , pers.datoverlijden
       , pers.regdatum_overlbevest
       , pers.datemigratie
       , pers.dat_creatie
       , pers.creatie_door
       , pers.sofinummer
       , pers.naam_partner
       , pers.voorvoegsel_partner
       , pers.oms_vvgsl_partner
       , pers.numrelatie
       , pers.persoonsnr
       , pers.pvachmea_persoonsnummer
       , pers.wgr_personeelsnummer
       , pers.a_nummer
       , pers.kanaalvoorkeur
       , pers.gba_status
       , pers.afnemers_indicatie
       , rle_pers.get_string_attr( 'Attr01'
                                 , pers.adresstring
                                 ) oa_postcode
       , to_number( rle_pers.get_string_attr( 'Attr02'
                                            , pers.adresstring
                                            )) oa_huisnummer
       , rle_pers.get_string_attr( 'Attr03'
                                 , pers.adresstring
                                 ) oa_huisnummer_tv
       , rle_pers.get_string_attr( 'Attr04'
                                 , pers.adresstring
                                 ) oa_woonplaats
       , rle_pers.get_string_attr( 'Attr05'
                                 , pers.adresstring
                                 ) oa_locatie
       , rle_pers.get_string_attr( 'Attr06'
                                 , pers.adresstring
                                 ) oa_straat
       , rle_pers.get_string_attr( 'Attr07'
                                 , pers.adresstring
                                 ) oa_landcode
       , rle_pers.get_string_attr( 'Attr08'
                                 , pers.adresstring
                                 ) oa_ind_woonboot
       , rle_pers.get_string_attr( 'Attr09'
                                 , pers.adresstring
                                 ) oa_ind_woonwagen
       , rle_pers.get_string_attr( 'Attr10'
                                 , pers.adresstring
                                 ) oa_landnaam
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr11'
                                         , pers.adresstring
                                         )
               ) da_postcode
       , to_number( decode( pers.pensioenadministratie
                          , null, null
                          , rle_pers.get_string_attr( 'Attr12'
                                                    , pers.adresstring
                                                    )
                          )) da_huisnummer
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr13'
                                         , pers.adresstring
                                         )
               ) da_huisnummer_tv
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr14'
                                         , pers.adresstring
                                         )
               ) da_woonplaats
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr15'
                                         , pers.adresstring
                                         )
               ) da_locatie
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr16'
                                         , pers.adresstring
                                         )
               ) da_straat
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr17'
                                         , pers.adresstring
                                         )
               ) da_landcode
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr18'
                                         , pers.adresstring
                                         )
               ) da_ind_woonboot
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr19'
                                         , pers.adresstring
                                         )
               ) da_ind_woonwagen
       , decode( pers.pensioenadministratie
               , null, null
               , rle_pers.get_string_attr( 'Attr20'
                                         , pers.adresstring
                                         )
               ) da_landnaam
       , pers.pensioenadministratie
       , rle_pers.get_string_attr( 'Attr21'
                                 , pers.adresstring
                                 ) volledige_naam1
       , ( select vpr.volledige_naam2
          from   rle_v_personen vpr
          where  vpr.numrelatie = pers.numrelatie
          and    rownum = 1 ) volledige_naam2
       , rle_pers.get_string_attr( 'Attr23'
                                 , pers.adresstring
                                 ) volledige_naam3
       , rle_pers.get_string_attr( 'Attr24'
                                 , pers.adresstring
                                 ) ca_postcode
       , rle_pers.get_string_attr( 'Attr25'
                                 , pers.adresstring
                                 ) ca_huisnummer
       , rle_pers.get_string_attr( 'Attr26'
                                 , pers.adresstring
                                 ) ca_huisnummer_tv
       , rle_pers.get_string_attr( 'Attr27'
                                 , pers.adresstring
                                 ) ca_woonplaats
       , rle_pers.get_string_attr( 'Attr28'
                                 , pers.adresstring
                                 ) ca_locatie
       , rle_pers.get_string_attr( 'Attr29'
                                 , pers.adresstring
                                 ) ca_straat
       , rle_pers.get_string_attr( 'Attr30'
                                 , pers.adresstring
                                 ) ca_landcode
       , rle_pers.get_string_attr( 'Attr31'
                                 , pers.adresstring
                                 ) ca_ind_woonboot
       , rle_pers.get_string_attr( 'Attr32'
                                 , pers.adresstring
                                 ) ca_ind_woonwagen
       , rle_pers.get_string_attr( 'Attr33'
                                 , pers.adresstring
                                 ) ca_landnaam
       , rle_pers.get_string_attr( 'Attr34'
                                 , pers.adresstring
                                 ) kruispuntteam_naam
       , rle_pers.get_string_attr( 'Attr35'
                                 , pers.adresstring
                                 ) kruispuntteam_tel
       , rle_get_gemeentenaam(pers.numrelatie, 'OA') oa_gemeentenaam
       , rle_get_gemeentenaam(pers.numrelatie, 'DA') da_gemeentenaam
       , rle_get_gemeentenaam(pers.numrelatie, 'CA') ca_gemeentenaam
  from   ( select rel.namrelatie
                , rel.zoeknaam
                , rel.indinstelling
                , rel.dwe_wrddom_geslacht cod_geslacht
                , rle_pers.get_pers_omschr( 'GES'
                                          , rel.dwe_wrddom_geslacht
                                          , 'N'
                                          ) oms_geslacht
                , rel.dwe_wrddom_vvgsl cod_vvgsl
                , rle_pers.get_pers_omschr( 'VVL'
                                          , rel.dwe_wrddom_vvgsl
                                          , 'N'
                                          ) oms_vvgsl
                , rle_pers.get_pers_omschr( 'VVL'
                                          , rel.dwe_wrddom_vvgsl
                                          , 'N'
                                          ) oms_vvgsl_portal
                , rel.voorletter
                , rel.voornaam
                , rel.datgeboorte
                , rel.code_gebdat_fictief cod_datgeb_fictief
                , rle_pers.get_pers_omschr( 'FGD'
                                          , rel.code_gebdat_fictief
                                          , 'N'
                                          ) oms_datgeb_fictief
                , rel.indcontrole
                , rel.code_aanduiding_naamgebruik cod_naamgebruik
                , rle_pers.get_pers_omschr( 'ANG'
                                          , rel.code_aanduiding_naamgebruik
                                          , 'N'
                                          ) oms_naamgebruik
                , rel.dwe_wrddom_gewatitel cod_aanschrijftitel
                , rle_pers.get_pers_omschr( 'GWT'
                                          , rel.dwe_wrddom_gewatitel
                                          , 'N'
                                          ) oms_aanschrijftitel
                , rel.datoverlijden
                , rel.regdat_overlbevest regdatum_overlbevest
                , rel.datemigratie
                , rel.dat_creatie
                , rel.creatie_door
                , comp_rle.rle_m03_rec_extcod( 'SOFI'
                                             , rel.numrelatie
                                             , 'PR'
                                             , sysdate
                                             ) sofinummer
                , rel.naam_partner
                , rel.vvgsl_partner voorvoegsel_partner
                , nvl( rle_pers.get_pers_omschr( 'VVL'
                                               , rel.vvgsl_partner
                                               , 'N'
                                               )
                     , rel.vvgsl_partner
                     ) oms_vvgsl_partner
                , rel.numrelatie
                , prs.extern_relatie persoonsnr
                , rle_pers.get_adm( rel.numrelatie ) pensioenadministratie
                , rle_m03_rec_extcod( 'PENS'
                                    , rel.numrelatie
                                    , null
                                    , null
                                    ) pvachmea_persoonsnummer
                , rle_m03_rec_extcod( 'PWGR'
                                    , rel.numrelatie
                                    , null
                                    , null
                                    ) wgr_personeelsnummer
                , rle_m03_rec_extcod( 'GBA'
                                    , rel.numrelatie
                                    , null
                                    , null
                                    ) a_nummer
                , rle_pers.get_kanaal( rel.numrelatie ) kanaalvoorkeur
                , rle_pers.get_gba( 'STATUS'
                                  , rel.numrelatie
                                  ) gba_status
                , rle_pers.get_gba( 'INDICATIE'
                                  , rel.numrelatie
                                  ) afnemers_indicatie
                , ( select rle_pers.get_adres( rel.numrelatie )
                   from   dual ) adresstring
          from   rle_relaties rel
               , rle_relatie_externe_codes prs
          where  prs.rle_numrelatie = rel.numrelatie
          and    prs.dwe_wrddom_extsys = 'PRS'
          and    prs.rol_codrol = 'PR'
          and    trunc( prs.datingang ) <= trunc( sysdate )
          and    nvl( prs.dateinde
                    , trunc( sysdate )
                    ) >= trunc( sysdate )
          and    rel.indinstelling = 'N'   -- Alleen personen
                                        ) pers
  WHERE rle_pers.valide_admin( pers.pensioenadministratie ) = 'J'   -- Check administratie context
 ;