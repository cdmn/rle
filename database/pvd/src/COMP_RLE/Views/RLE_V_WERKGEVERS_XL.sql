CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_werkgevers_xl (relatienr,werkgevernr,kvk_nummer,pens_admin_id,bovag_nr,incasso_nr,bic_ai,bic_ag,iban_ai,iban_ag,bet_wijze_ai,bet_wijze_ag,algemeen_nr,naam_relatie,zoeknaam,rechtsvorm,datum_oprichting,datum_begin,datum_einde,handelsnaam,sz_postcode,sz_huisnummer,sz_huisnummer_toevoeging,sz_woonplaats,sz_straat,sz_land_code,sz_indicatiewoonboot,sz_indicatiewoonwagen,sz_locatie,sz_landnaam,sz_relatienr_contactpersoon,sz_naam_contactpersoon,sz_telnr_contactpersoon,sz_email_adres,sz_tel_nummer,fa_postcode,fa_huisnummer,fa_huisnummer_toevoeging,fa_woonplaats,fa_straat,fa_land_code,fa_indicatiewoonboot,fa_indicatiewoonwagen,fa_locatie,fa_landnaam,fa_relatienr_contactpersoon,fa_naam_contactpersoon,fa_telnr_contactpersoon,ia_postcode,ia_huisnummer,ia_huisnummer_toevoeging,ia_woonplaats,ia_straat,ia_land_code,ia_indicatiewoonboot,ia_indicatiewoonwagen,ia_locatie,ia_landnaam,ia_relatienr_contactpersoon,ia_naam_contactpersoon,ia_telnr_contactpersoon,ca_postcode,ca_huisnummer,ca_huisnummer_toevoeging,ca_woonplaats,ca_straat,ca_land_code,ca_indicatiewoonboot,ca_indicatiewoonwagen,ca_locatie,ca_landnaam,ca_relatienr_contactpersoon,ca_naam_contactpersoon,ca_telnr_contactpersoon,ba_postcode,ba_huisnummer,ba_huisnummer_toevoeging,ba_woonplaats,ba_straat,ba_land_code,ba_indicatiewoonboot,ba_indicatiewoonwagen,ba_locatie,ba_landnaam,ba_relatienr_contactpersoon,ba_naam_contactpersoon,ba_telnr_contactpersoon,la_relatienr,la_naam,la_straat,la_huisnr,la_toevnum,la_postcode,la_woonplaats,la_land,la_telnr_contactpersoon,la_naam_contactpersoon,kanaalvoorkeur,tel_nummer,fax_nummer,email_adres,actmgr_naam,actmgr_telefoonnummer,actmgr_voorletters,actmgr_voorvoegsels,klntteam_naam,klntteam_tel) AS
SELECT rec.rle_numrelatie relatienr
     , rec.extern_relatie werkgevernr
     , rle_wgr.kvk_nr( rec.rle_numrelatie ) kvk_nummer
     , rle_wgr.pens_admin( rec.rle_numrelatie ) pens_admin_id
     , rle_wgr.bovag_nr( rec.rle_numrelatie ) bovag_nr
     , rle_wgr.incasso_nr( rec.rle_numrelatie ) incasso_nr
     , rle_wgr.bic_ai( rec.rle_numrelatie ) bic_ai
     , rle_wgr.bic_ag( rec.rle_numrelatie ) bic_ag
     , rle_wgr.iban_ai( rec.rle_numrelatie ) iban_ai
     , rle_wgr.iban_ag( rec.rle_numrelatie ) iban_ag
     , rle_wgr.bet_wijze_ai( rec.rle_numrelatie ) bet_wijze_ai
     , rle_wgr.bet_wijze_ag( rec.rle_numrelatie ) bet_wijze_ag
     , rle_wgr.algemeen_nr( rec.rle_numrelatie ) algemeen_nr
     , rle.namrelatie naam_relatie
     , rle.zoeknaam zoeknaam
     , rle.dwe_wrddom_rvorm rechtsvorm
     , rle.datoprichting datum_oprichting
     , rle.datbegin datum_begin
     , rle.dateinde datum_einde
     , rle.handelsnaam handelsnaam
     , rle_wgr.sz_postcode( rec.rle_numrelatie ) sz_postcode
     , rle_wgr.sz_huisnummer( rec.rle_numrelatie ) sz_huisnummer
     , rle_wgr.sz_huisnummer_toevoeging( rec.rle_numrelatie ) sz_huisnummer_toevoeging
     , rle_wgr.sz_woonplaats( rec.rle_numrelatie ) sz_woonplaats
     , rle_wgr.sz_straat( rec.rle_numrelatie ) sz_straat
     , rle_wgr.sz_land_code( rec.rle_numrelatie ) sz_land_code
     , rle_wgr.sz_indicatiewoonboot( rec.rle_numrelatie ) sz_indicatiewoonboot
     , rle_wgr.sz_indicatiewoonwagen( rec.rle_numrelatie ) sz_indicatiewoonwagen
     , rle_wgr.sz_locatie( rec.rle_numrelatie ) sz_locatie
     , rle_wgr.sz_landnaam( rec.rle_numrelatie ) sz_landnaam
     , rle_wgr.sz_relatienr_contactpersoon( rec.rle_numrelatie ) sz_relatienr_contactpersoon
     , rle_wgr.sz_naam_contactpersoon( rec.rle_numrelatie ) sz_naam_contactpersoon
     , rle_wgr.sz_telnr_contactpersoon( rec.rle_numrelatie ) sz_telnr_contactpersoon
     , rle_wgr.sz_email_adres( rec.rle_numrelatie ) sz_email_adres
     , rle_wgr.sz_tel_nummer( rec.rle_numrelatie ) sz_tel_nummer
     , rle_wgr.fa_postcode( rec.rle_numrelatie ) fa_postcode
     , rle_wgr.fa_huisnummer( rec.rle_numrelatie ) fa_huisnummer
     , rle_wgr.fa_huisnummer_toevoeging( rec.rle_numrelatie ) fa_huisnummer_toevoeging
     , rle_wgr.fa_woonplaats( rec.rle_numrelatie ) fa_woonplaats
     , rle_wgr.fa_straat( rec.rle_numrelatie ) fa_straat
     , rle_wgr.fa_land_code( rec.rle_numrelatie ) fa_land_code
     , rle_wgr.fa_indicatiewoonboot( rec.rle_numrelatie ) fa_indicatiewoonboot
     , rle_wgr.fa_indicatiewoonwagen( rec.rle_numrelatie ) fa_indicatiewoonwagen
     , rle_wgr.fa_locatie( rec.rle_numrelatie ) fa_locatie
     , rle_wgr.fa_landnaam( rec.rle_numrelatie ) fa_landnaam
     , rle_wgr.fa_relatienr_contactpersoon( rec.rle_numrelatie ) fa_relatienr_contactpersoon
     , rle_wgr.fa_naam_contactpersoon( rec.rle_numrelatie ) fa_naam_contactpersoon
     , rle_wgr.fa_telnr_contactpersoon( rec.rle_numrelatie ) fa_telnr_contactpersoon
     , rle_wgr.ia_postcode( rec.rle_numrelatie ) ia_postcode
     , rle_wgr.ia_huisnummer( rec.rle_numrelatie ) ia_huisnummer
     , rle_wgr.ia_huisnummer_toevoeging( rec.rle_numrelatie ) ia_huisnummer_toevoeging
     , rle_wgr.ia_woonplaats( rec.rle_numrelatie ) ia_woonplaats
     , rle_wgr.ia_straat( rec.rle_numrelatie ) ia_straat
     , rle_wgr.ia_land_code( rec.rle_numrelatie ) ia_land_code
     , rle_wgr.ia_indicatiewoonboot( rec.rle_numrelatie ) ia_indicatiewoonboot
     , rle_wgr.ia_indicatiewoonwagen( rec.rle_numrelatie ) ia_indicatiewoonwagen
     , rle_wgr.ia_locatie( rec.rle_numrelatie ) ia_locatie
     , rle_wgr.ia_landnaam( rec.rle_numrelatie ) ia_landnaam
     , rle_wgr.ia_relatienr_contactpersoon( rec.rle_numrelatie ) ia_relatienr_contactpersoon
     , rle_wgr.ia_naam_contactpersoon( rec.rle_numrelatie ) ia_naam_contactpersoon
     , rle_wgr.ia_telnr_contactpersoon( rec.rle_numrelatie ) ia_telnr_contactpersoon
     , rle_wgr.ca_postcode( rec.rle_numrelatie ) ca_postcode
     , rle_wgr.ca_huisnummer( rec.rle_numrelatie ) ca_huisnummer
     , rle_wgr.ca_huisnummer_toevoeging( rec.rle_numrelatie ) ca_huisnummer_toevoeging
     , rle_wgr.ca_woonplaats( rec.rle_numrelatie ) ca_woonplaats
     , rle_wgr.ca_straat( rec.rle_numrelatie ) ca_straat
     , rle_wgr.ca_land_code( rec.rle_numrelatie ) ca_land_code
     , rle_wgr.ca_indicatiewoonboot( rec.rle_numrelatie ) ca_indicatiewoonboot
     , rle_wgr.ca_indicatiewoonwagen( rec.rle_numrelatie ) ca_indicatiewoonwagen
     , rle_wgr.ca_locatie( rec.rle_numrelatie ) ca_locatie
     , rle_wgr.ca_landnaam( rec.rle_numrelatie ) ca_landnaam
     , rle_wgr.ca_relatienr_contactpersoon( rec.rle_numrelatie ) ca_relatienr_contactpersoon
     , rle_wgr.ca_naam_contactpersoon( rec.rle_numrelatie ) ca_naam_contactpersoon
     , rle_wgr.ca_telnr_contactpersoon( rec.rle_numrelatie ) ca_telnr_contactpersoon
     , rle_wgr.ba_postcode( rec.rle_numrelatie ) ba_postcode
     , rle_wgr.ba_huisnummer( rec.rle_numrelatie ) ba_huisnummer
     , rle_wgr.ba_huisnummer_toevoeging( rec.rle_numrelatie ) ba_huisnummer_toevoeging
     , rle_wgr.ba_woonplaats( rec.rle_numrelatie ) ba_woonplaats
     , rle_wgr.ba_straat( rec.rle_numrelatie ) ba_straat
     , rle_wgr.ba_land_code( rec.rle_numrelatie ) ba_land_code
     , rle_wgr.ba_indicatiewoonboot( rec.rle_numrelatie ) ba_indicatiewoonboot
     , rle_wgr.ba_indicatiewoonwagen( rec.rle_numrelatie ) ba_indicatiewoonwagen
     , rle_wgr.ba_locatie( rec.rle_numrelatie ) ba_locatie
     , rle_wgr.ba_landnaam( rec.rle_numrelatie ) ba_landnaam
     , rle_wgr.ba_relatienr_contactpersoon( rec.rle_numrelatie ) ba_relatienr_contactpersoon
     , rle_wgr.ba_naam_contactpersoon( rec.rle_numrelatie ) ba_naam_contactpersoon
     , rle_wgr.ba_telnr_contactpersoon( rec.rle_numrelatie ) ba_telnr_contactpersoon
     , rle_wgr.la_relatienr( rec.rle_numrelatie ) la_relatienr
     , rle_wgr.la_naam( rec.rle_numrelatie ) la_naam
     , rle_wgr.la_straat( rec.rle_numrelatie ) la_straat
     , rle_wgr.la_huisnr( rec.rle_numrelatie ) la_huisnr
     , rle_wgr.la_toevnum( rec.rle_numrelatie ) la_toevnum
     , rle_wgr.la_postcode( rec.rle_numrelatie ) la_postcode
     , rle_wgr.la_woonplaats( rec.rle_numrelatie ) la_woonplaats
     , rle_wgr.la_land( rec.rle_numrelatie ) la_land
     , rle_wgr.la_telnr_contactpersoon( rec.rle_numrelatie ) la_telnr_contactpersoon
     , rle_wgr.la_naam_contactpersoon( rec.rle_numrelatie ) la_naam_contactpersoon
     , rle_wgr.kanaalvoorkeur( rec.rle_numrelatie ) kanaalvoorkeur
     , rle_wgr.tel_nummer( rec.rle_numrelatie ) tel_nummer
     , rle_wgr.fax_nummer( rec.rle_numrelatie ) fax_nummer
     , rle_wgr.email_adres( rec.rle_numrelatie ) email_adres
     , rle_wgr.actmgr_naam( rec.extern_relatie ) actmgr_naam
     , rle_wgr.actmgr_telefoonnummer( rec.extern_relatie ) actmgr_telefoonnummer
     , rle_wgr.actmgr_voorletters( rec.extern_relatie ) actmgr_voorletters
     , rle_wgr.actmgr_voorvoegsels( rec.extern_relatie ) actmgr_voorvoegsels
     , rle_wgr.klntteam_naam( rec.extern_relatie ) klntteam_naam
     , rle_wgr.klntteam_tel( rec.extern_relatie ) klntteam_tel
from   rle_relatie_externe_codes rec
     , rle_relaties rle
  WHERE rec.rle_numrelatie = rle.numrelatie(+)
 and rec.rol_codrol = 'WG'
 and rec.dwe_wrddom_extsys = 'WGR'
 and rec.dateinde is null
 and wsf_sec.get_sec( 'WGR'
                    , sysdate
                    , null
                    , rec.extern_relatie
                    , null
                    , 'N'
                    ) like basis_ctx.sector
 ;