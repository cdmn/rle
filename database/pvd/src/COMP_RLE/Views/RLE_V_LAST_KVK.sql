CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_last_kvk (rle_numrelatie,kvk_nr) AS
SELECT KVK.RLE_NUMRELATIE RLE_NUMRELATIE
          ,KVK.EXTERN_RELATIE KVK_NR
FROM RLE_RELATIE_EXTERNE_CODES KVK
  WHERE kvk.dwe_wrddom_extsys = 'KVK'
and    kvk.rol_codrol = 'WG'
and    kvk.datingang = ( select max( kvk2.datingang )
                        from   rle_relatie_externe_codes kvk2
                        where  kvk2.dwe_wrddom_extsys = 'KVK'
                        and    kvk2.rol_codrol = 'WG'
                        and    sysdate between kvk2.datingang and nvl( kvk2.dateinde
                                                                     , sysdate
                                                                     )
                        and    kvk2.rle_numrelatie = kvk.rle_numrelatie )
 ;