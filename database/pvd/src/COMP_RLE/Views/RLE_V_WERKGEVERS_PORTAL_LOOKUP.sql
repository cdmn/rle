CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_werkgevers_portal_lookup (relatienr,werkgevernr,naam_relatie,zoeknaam,rechtsvorm,datum_oprichting,datum_begin,datum_einde,handelsnaam) AS
SELECT REC.RLE_NUMRELATIE RELATIENR
          ,REC.EXTERN_RELATIE WERKGEVERNR
          ,RLE.NAMRELATIE NAAM_RELATIE
          ,RLE.ZOEKNAAM ZOEKNAAM
          ,RLE.DWE_WRDDOM_RVORM RECHTSVORM
          ,RLE.DATOPRICHTING DATUM_OPRICHTING
          ,RLE.DATBEGIN DATUM_BEGIN
          ,RLE.DATEINDE DATUM_EINDE
          ,RLE.HANDELSNAAM HANDELSNAAM
FROM RLE_RELATIE_EXTERNE_CODES REC
    ,RLE_RELATIES RLE
  WHERE rec.rle_numrelatie = rle.numrelatie(+)
  and    rec.rol_codrol = 'WG'
  and    rec.dwe_wrddom_extsys = 'WGR'
 ;