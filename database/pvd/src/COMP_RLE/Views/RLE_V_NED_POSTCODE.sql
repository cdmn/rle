CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_ned_postcode (postcode,codreeks,numscheidingvan,numscheidingtm,codorganisatie,indpostchand,woonplaats,straatnaam,gemeentenaam,gemeentenummer,codprovincie,codcebuco,dat_creatie,creatie_door,dat_mutatie,mutatie_door) AS
SELECT NPE.POSTCODE POSTCODE
          ,NPE.CODREEKS CODREEKS
          ,NPE.NUMSCHEIDINGVAN NUMSCHEIDINGVAN
          ,NPE.NUMSCHEIDINGTM NUMSCHEIDINGTM
          ,NPE.CODORGANISATIE CODORGANISATIE
          ,NPE.INDPOSTCHAND INDPOSTCHAND
          ,NPE.WOONPLAATS WOONPLAATS
          ,NPE.STRAATNAAM STRAATNAAM
          ,NPE.GEMEENTENAAM GEMEENTENAAM
          ,NPE.GEMEENTENUMMER GEMEENTENUMMER
          ,NPE.CODPROVINCIE CODPROVINCIE
          ,NPE.CODCEBUCO CODCEBUCO
          ,NPE.DAT_CREATIE DAT_CREATIE
          ,NPE.CREATIE_DOOR CREATIE_DOOR
          ,NPE.DAT_MUTATIE DAT_MUTATIE
          ,NPE.MUTATIE_DOOR MUTATIE_DOOR
FROM RLE_NED_POSTCODE NPE
 ;