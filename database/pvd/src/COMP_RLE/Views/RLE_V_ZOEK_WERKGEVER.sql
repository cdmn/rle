CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_zoek_werkgever (relatienummer,werkgevernummer,zoeknaam,sz_postcode,sz_woonplaats,pens_admin_id) AS
SELECT rec.rle_numrelatie relatienummer
       , rec.extern_relatie werkgevernummer
       , convert(upper(regexp_replace(rle.namrelatie, '\ *\&\ *', ' EN ')), 'US7ASCII')  zoeknaam
       , sz.postcode sz_postcode
       , sz.woonplaats sz_woonplaats
       , pens_admin_id
  from   rle_relatie_externe_codes rec
       , rle_relaties rle
       , ( select ads.postcode
                , wpl.woonplaats
                , ras.rle_numrelatie
          from   rle_relatie_adressen ras
               , rle_adressen ads
               , rle_woonplaatsen wpl
          where  ras.ads_numadres = ads.numadres
          and    ads.wps_woonplaatsid = wpl.woonplaatsid
          and    sysdate between ras.datingang and nvl ( ras.dateinde
                                                       , sysdate
                                                       )
          and    ras.dwe_wrddom_srtadr = 'SZ' ) sz
       , ( select rec.rle_numrelatie
                , rec.extern_relatie pens_admin_id
          from   rle_relatie_externe_codes rec
          where  rec.dwe_wrddom_extsys = 'PENS'
          and    trunc ( rec.datingang ) <= trunc ( sysdate )
          and    ( trunc ( rec.dateinde ) >= trunc ( sysdate )
                   or rec.dateinde is null ) ) pens
  WHERE rec.rle_numrelatie = rle.numrelatie
  and    rec.rle_numrelatie = sz.rle_numrelatie(+)
  and    rec.rol_codrol = 'WG'
  and    rec.dwe_wrddom_extsys = 'WGR'
  and    rec.dateinde is null
  and    pens.rle_numrelatie(+) = rle.numrelatie
 ;