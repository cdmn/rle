CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_communicatie_cat_kanalen (code,omschrijving,dwe_wrddom_srtcom,srtcom_omschrijving,adm_code) AS
select ccn.code
       , ccn.omschrijving
       , cck.dwe_wrddom_srtcom
       , substr ( rfe_get_rwe_oms ( 'SC'
                                  , cck.dwe_wrddom_srtcom
                                  , 'J'
                                  , 40 )
                , 1
                , 40 )
           srtcom_omschrijving
       , ccn.adm_code
  from   rle_communicatie_categorieen ccn, rle_communicatie_cat_kanalen cck
  where  ccn.code != 'ALG'
  and    cck.ccn_id(+) = ccn.id
  and    sysdate between ccn.datum_begin and nvl ( ccn.datum_einde, sysdate + 1 )
  and    sysdate between cck.datum_begin(+) and nvl ( cck.datum_einde(+), sysdate + 1 )
 ;