CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_contactpersonen (relatienummer,relatienummer_contactpersoon,naam,zoeknaam,titel,tussentitel,voorvoegsels,achtervoegsels,geslacht,gewenste_aanschrijftitel,voornaam,voorletters,aanschrijfnaam,datum_overlijden,naam_partner,voorvoegsels_partner,voorkeursnaam,voorkeursvoorvoegsels,volledige_naam1,volledige_naam2,soort_adres,ras_id,code_soort_contactpersoon) AS
SELECT RAS.RLE_NUMRELATIE
,      RLE.NUMRELATIE
,      RLE.NAMRELATIE
,      RLE.ZOEKNAAM
,      RLE.DWE_WRDDOM_TITEL
,      RLE.DWE_WRDDOM_TTITEL
,      VVGSL.WAARDE
,      AVGSL.WAARDE
,      GESLACHT.WAARDE
,      RLE.DWE_WRDDOM_GEWATITEL
,      RLE.VOORNAAM
,      fnc_bepaal_voorletters(RLE.VOORLETTER)
,      RLE.AANSCHRIJFNAAM
,      RLE.DATOVERLIJDEN
,      RLE.NAAM_PARTNER
,      lower(RLE.VVGSL_PARTNER)
,      rle_get_voorkeursnaam(RLE.NAMRELATIE, VVGSL.WAARDE, RLE.NAAM_PARTNER, lower(RLE.VVGSL_PARTNER), RLE.CODE_AANDUIDING_NAAMGEBRUIK)
,      decode(rle.code_aanduiding_naamgebruik, 'E', VVGSL.WAARDE, 'P', lower(RLE.VVGSL_PARTNER), 'V', lower(RLE.VVGSL_PARTNER), 'N', VVGSL.WAARDE, VVGSL.WAARDE)
,      ltrim(fnc_bepaal_voorletters(RLE.VOORLETTER)||' ')
       ||ltrim(decode(rle.code_aanduiding_naamgebruik, 'E', VVGSL.WAARDE, 'P', lower(RLE.VVGSL_PARTNER), 'V', lower(RLE.VVGSL_PARTNER), 'N', VVGSL.WAARDE, VVGSL.WAARDE)||' ')
       ||rle_get_voorkeursnaam(RLE.NAMRELATIE, VVGSL.WAARDE, RLE.NAAM_PARTNER, lower(RLE.VVGSL_PARTNER), RLE.CODE_AANDUIDING_NAAMGEBRUIK)
       ||rtrim(' '||AVGSL.WAARDE)
,      rtrim(rle_get_voorkeursnaam(RLE.NAMRELATIE, VVGSL.WAARDE, RLE.NAAM_PARTNER, lower(RLE.VVGSL_PARTNER), RLE.CODE_AANDUIDING_NAAMGEBRUIK)||' '||AVGSL.WAARDE)
       ||', '
       ||rtrim(fnc_bepaal_voorletters(RLE.VOORLETTER)||' ')
       ||decode(rle.code_aanduiding_naamgebruik, 'E', VVGSL.WAARDE, 'P', lower(RLE.VVGSL_PARTNER), 'V', lower(RLE.VVGSL_PARTNER), 'N', VVGSL.WAARDE, VVGSL.WAARDE)
,      ras.dwe_wrddom_srtadr
,      ras.id ras_id
,      cpn.code_soort_contactpersoon
from   rle_relatie_adressen ras
,      rle_contactpersonen cpn
,      rle_relaties rle
,      DOMEINWAARDEN VVGSL
,      DOMEINWAARDEN AVGSL
,      DOMEINWAARDEN GESLACHT
  WHERE cpn.rle_numrelatie = rle.NUMRELATIE
and    cpn.ras_id = ras.id
AND    VVGSL.DOMEINCODE(+) = 'VVL'
AND    VVGSL.CODE(+) = RLE.DWE_WRDDOM_VVGSL
AND    AVGSL.DOMEINCODE(+) = 'AVL'
AND    AVGSL.CODE(+) = RLE.DWE_WRDDOM_AVGSL
AND    GESLACHT.DOMEINCODE(+) = 'GES'
AND    GESLACHT.CODE(+) = RLE.DWE_WRDDOM_GESLACHT
 ;