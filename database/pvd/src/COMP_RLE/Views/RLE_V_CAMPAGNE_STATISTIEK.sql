CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_campagne_statistiek ("ID",aantal_relaties,aantal_geselecteerd,aantal_toegevoegd,aantal_aangepast,aantal_uitgesloten,aantal_overleden,aantal_beeindigd,aantal_brieven_verstuurd,aantal_brieven_niet_verstuurd,aantal_brieven_onbestelbaar,aantal_respons_ontvangen,aantal_nagebeld,aantal_te_bezoeken,aantal_bezocht) AS
SELECT stat.id
         ,   stat.aantal_relaties
         ,   stat.aantal_geselecteerd
         ,   stat.aantal_toegevoegd
         ,   stat.aantal_aangepast
         ,   stat.aantal_uitgesloten
         ,   stat.aantal_overleden
         ,   stat.aantal_beeindigd
         ,   stat.aantal_brieven_verstuurd
         ,   stat.aantal_brieven_niet_verstuurd -
            ( stat.aantal_uitgesloten + stat.aantal_overleden + stat.aantal_beeindigd ) aantal_brieven_niet_verstuurd
         ,   stat.aantal_brieven_onbestelbaar
         ,   stat.aantal_respons_ontvangen
         ,   stat.aantal_nagebeld
         ,   stat.aantal_te_bezoeken
         ,   stat.aantal_bezocht
FROM( select cpe.id
                     ,    count(*)                       aantal_relaties
                     ,    sum(decode(ric.ind_toegevoegd,'N',1,0))  aantal_geselecteerd
                     ,    sum(decode(ric.ind_toegevoegd,'J',1,0))  aantal_toegevoegd
                     ,    sum(decode(ric.ind_aangepast,'J',1,0))   aantal_aangepast
                     ,    sum(decode(ric.ind_uitgesloten,'J',1,0)) aantal_uitgesloten
                     ,    sum(decode(ric.ind_overleden,'J',1,0))   aantal_overleden
                     ,    sum(decode(ric.ind_beeindigd,'J',1,0))   aantal_beeindigd
                     ,    sum(decode( ric.status, 'VSD', 1 , 0 ))  aantal_brieven_verstuurd
                     ,    sum(decode( ric.status, 'AGT', 1 , 0 ))  aantal_brieven_niet_verstuurd
                     ,    sum(decode( ric.status, 'ONB', 1 , 0 ))  aantal_brieven_onbestelbaar
                     ,    sum(decode( ric.status, 'RFO', 1 , 0 ))  aantal_respons_ontvangen
                     ,    sum(decode( ric.status, 'NGD', 1 , 0 ))  aantal_nagebeld
                     ,    sum(decode( ric.status, 'BPL', 1 , 0 ))  aantal_te_bezoeken
                     ,    sum(decode( ric.status, 'BZT', 1 , 0 ))  aantal_bezocht
            from rle_relaties_in_campagnes ric
                 ,    rle_campagnes cpe
           where ric.cpe_id = cpe.id
           group by cpe.id ) stat
 ;