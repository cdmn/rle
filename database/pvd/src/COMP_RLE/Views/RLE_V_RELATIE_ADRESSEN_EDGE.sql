CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_relatie_adressen_edge (werkgevernummer,relatienummer,namrelatie,handelsnaam,zoeknaam,upper_handelsnaam,straat,woonplaats,postcode,huisnummer,aansluitnummer,wss_cod_status) AS
SELECT wss.werkgevernummer
  ,      rle.numrelatie relatienummer
  ,      rle.namrelatie
  ,      rle.handelsnaam
  ,      rle.zoeknaam
  ,      upper(rle.handelsnaam) upper_handelsnaam
  ,      stt.straatnaam straat
  ,      wps.woonplaats
  ,      ads.postcode
  ,      ads.huisnummer||ads.toevhuisnum huisnummer
  ,      asg.aansluitnummer
  ,      wss.code_status wss_cod_status
  from   rle_adressen ads
  ,      rle_relatie_adressen ras
  ,      rle_relaties rle
  ,      rle_straten stt
  ,      rle_woonplaatsen wps
  ,      wgr_v_aansluitingen asg
  ,      wgr_v_werkgever_statussen wss
  WHERE ads.numadres = ras.ads_numadres
  and    rle.numrelatie = ras.rle_numrelatie
  and    ads.stt_straatid = stt.straatid
  and    ads.wps_woonplaatsid =  wps.woonplaatsid
  and    wss.nr_relatie = rle.numrelatie
  and    asg.nr_relatie(+) = rle.numrelatie
  and    ras.dwe_wrddom_srtadr in ('CA', 'SZ')
  and    sysdate between ras.datingang
                     and nvl(ras.dateinde, sysdate)
  and    sysdate between wss.dat_begin
                     and nvl(wss.dat_einde, sysdate);