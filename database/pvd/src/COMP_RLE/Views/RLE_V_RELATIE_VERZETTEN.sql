CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_relatie_verzetten ("ID",tussenpersoon,productgroep,adm_code,verzet_kanaal,rle_numrelatie,registratie_kanaal,registratie_datum,verval_kanaal,verval_datum) AS
SELECT RVT.ID ID
          ,RVT.TUSSENPERSOON TUSSENPERSOON
          ,RVT.PRODUCTGROEP PRODUCTGROEP
          ,RVT.ADM_CODE ADM_CODE
          ,RVT.VERZET_KANAAL VERZET_KANAAL
          ,RVT.RLE_NUMRELATIE RLE_NUMRELATIE
          ,RVT.REGISTRATIE_KANAAL REGISTRATIE_KANAAL
          ,RVT.REGISTRATIE_DATUM REGISTRATIE_DATUM
          ,RVT.VERVAL_KANAAL VERVAL_KANAAL
          ,RVT.VERVAL_DATUM VERVAL_DATUM
FROM RLE_RELATIE_VERZETTEN RVT
  WHERE ( nvl(adm_code,'Xyz') like nvl(stm_context.administratie,'%')
 or stm_context.administratie = 'ALG'
)
 ;