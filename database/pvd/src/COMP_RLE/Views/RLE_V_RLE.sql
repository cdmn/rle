CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_rle (numrelatie,codorganisatie,namrelatie,zoeknaam,indgewnaam,indinstelling,datschoning,dwe_wrddom_titel,dwe_wrddom_ttitel,dwe_wrddom_avgsl,dwe_wrddom_vvgsl,dwe_wrddom_brgst,dwe_wrddom_geslacht,dwe_wrddom_gewatitel,dwe_wrddom_rvorm,voornaam,voorletter,aanschrijfnaam,datgeboorte,code_gebdat_fictief,datoverlijden,datoprichting,datbegin,dateinde,datcontrole,indcontrole,regdat_overlbevest,datemigratie,naam_partner,vvgsl_partner,code_aanduiding_naamgebrui,handelsnaam) AS
SELECT RLE.NUMRELATIE NUMRELATIE
          ,RLE.CODORGANISATIE CODORGANISATIE
          ,RLE.NAMRELATIE NAMRELATIE
          ,RLE.ZOEKNAAM ZOEKNAAM
          ,RLE.INDGEWNAAM INDGEWNAAM
          ,RLE.INDINSTELLING INDINSTELLING
          ,RLE.DATSCHONING DATSCHONING
          ,RLE.DWE_WRDDOM_TITEL DWE_WRDDOM_TITEL
          ,RLE.DWE_WRDDOM_TTITEL DWE_WRDDOM_TTITEL
          ,RLE.DWE_WRDDOM_AVGSL DWE_WRDDOM_AVGSL
          ,RLE.DWE_WRDDOM_VVGSL DWE_WRDDOM_VVGSL
          ,RLE.DWE_WRDDOM_BRGST DWE_WRDDOM_BRGST
          ,RLE.DWE_WRDDOM_GESLACHT DWE_WRDDOM_GESLACHT
          ,RLE.DWE_WRDDOM_GEWATITEL DWE_WRDDOM_GEWATITEL
          ,RLE.DWE_WRDDOM_RVORM DWE_WRDDOM_RVORM
          ,RLE.VOORNAAM VOORNAAM
          ,RLE.VOORLETTER VOORLETTER
          ,RLE.AANSCHRIJFNAAM AANSCHRIJFNAAM
          ,RLE.DATGEBOORTE DATGEBOORTE
          ,RLE.CODE_GEBDAT_FICTIEF CODE_GEBDAT_FICTIEF
          ,RLE.DATOVERLIJDEN DATOVERLIJDEN
          ,RLE.DATOPRICHTING DATOPRICHTING
          ,RLE.DATBEGIN DATBEGIN
          ,RLE.DATEINDE DATEINDE
          ,RLE.DATCONTROLE DATCONTROLE
          ,RLE.INDCONTROLE INDCONTROLE
          ,RLE.REGDAT_OVERLBEVEST REGDAT_OVERLBEVEST
          ,RLE.DATEMIGRATIE DATEMIGRATIE
          ,RLE.NAAM_PARTNER NAAM_PARTNER
          ,RLE.VVGSL_PARTNER VVGSL_PARTNER
          ,RLE.CODE_AANDUIDING_NAAMGEBRUIK CODE_AANDUIDING_NAAMGEBRUI
          ,RLE.HANDELSNAAM HANDELSNAAM
FROM RLE_RELATIES RLE
 ;