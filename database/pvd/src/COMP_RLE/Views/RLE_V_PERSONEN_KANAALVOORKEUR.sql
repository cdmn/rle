CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_personen_kanaalvoorkeur (numrelatie,persoonsnummer,adm_code,ccn_code,ccn_omschrijving,code_soort_kanaalvoorkeur,omschrijving_kanaalvoorkeur) AS
select vkr.numrelatie
     , vkr.persoonsnummer
     , vkr.adm_code
     , vkr.ccn_code
     , vkr.ccn_omschrijving
     , vkr.code_soort_kanaalvoorkeur
     , vkr.omschrijving_kanaalvoorkeur
from   (select un.numrelatie
             , un.persoonsnummer
             , un.adm_code
             , un.ccn_code
             , un.ccn_omschrijving
             , un.code_soort_kanaalvoorkeur
             , un.omschrijving_kanaalvoorkeur
             , un.ind_default
             , lag ( ccn_code
                   , 1
                   , 0 )
               over ( partition by un.numrelatie, un.adm_code, un.ccn_code
                      order by
                        un.numrelatie
                      , un.adm_code
                      , un.ccn_code
                      , un.ind_default desc nulls last )
                 prev_ccn_code
        from   (select rle.numrelatie numrelatie
                     , prs.extern_relatie persoonsnummer
                     , ria.adm_code adm_code
                     , ccn.code ccn_code
                     , ccn.omschrijving ccn_omschrijving
                     , cck.dwe_wrddom_srtcom code_soort_kanaalvoorkeur
                     , substr ( rfe_get_rwe_oms ( 'SC'
                                                , cck.dwe_wrddom_srtcom
                                                , 'J'
                                                , 40 )
                              , 1
                              , 40 )
                         omschrijving_kanaalvoorkeur
                     , cck.ind_default
                from   rle_communicatie_categorieen ccn
                     , rle_communicatie_cat_kanalen cck
                     , rle_relaties rle
                     , rle_relatierollen_in_adm ria
                     , rle_relatie_externe_codes prs
                where  ccn.id = cck.ccn_id
                and    ccn.code <> 'ALG'
                and    cck.ind_default = 'J'
                and    sysdate between ccn.datum_begin and nvl ( ccn.datum_einde, sysdate + 1 )
                and    sysdate between cck.datum_begin and nvl ( cck.datum_einde, sysdate + 1 )
				and    ccn.adm_code = ria.adm_code
                and    ria.rle_numrelatie = rle.numrelatie
                and    rle.numrelatie = prs.rle_numrelatie
                and    prs.rol_codrol = 'PR'
                and    prs.dwe_wrddom_extsys = 'PRS'
                and    ria.rol_codrol = 'PR'
                and    sysdate between prs.datingang and nvl ( prs.dateinde, sysdate )
                union all
                select rle.numrelatie numrelatie
                     , prs.extern_relatie persoonsnummer
                     , ria.adm_code adm_code
                     , ccn.code ccn_code
                     , ccn.omschrijving ccn_omschrijving
                     , cck.dwe_wrddom_srtcom code_soort_kanaalvoorkeur
                     , substr ( rfe_get_rwe_oms ( 'SC'
                                                , cck.dwe_wrddom_srtcom
                                                , 'J'
                                                , 40 )
                              , 1
                              , 40 )
                         omschrijving_kanaalvoorkeur
                     , 'N' ind_default
                from   rle_communicatie_categorieen ccn
                     , rle_communicatie_cat_kanalen cck
                     , rle_kanaalvoorkeuren kvk
                     , rle_relaties rle
                     , rle_relatierollen_in_adm ria
                     , rle_relatie_externe_codes prs
                where  ccn.id = cck.ccn_id
                and    ccn.code <> 'ALG'
                and    sysdate between ccn.datum_begin and nvl ( ccn.datum_einde, sysdate + 1 )
                and    sysdate between cck.datum_begin and nvl ( cck.datum_einde, sysdate + 1 )
                and    sysdate between kvk.dat_begin and nvl ( kvk.dat_einde, sysdate + 1 )
                and    kvk.cck_id = cck.id
                and    kvk.rle_numrelatie = rle.numrelatie
                and    kvk.authentiek = 'J'
                and    ria.rle_numrelatie = rle.numrelatie
                and    rle.numrelatie = prs.rle_numrelatie
                and    ccn.adm_code = ria.adm_code
                and    prs.rol_codrol = 'PR'
                and    prs.dwe_wrddom_extsys = 'PRS'
                and    ria.rol_codrol = 'PR'
                and    sysdate between prs.datingang and nvl ( prs.dateinde, sysdate )) un) vkr
where  vkr.prev_ccn_code = '0'
 ;