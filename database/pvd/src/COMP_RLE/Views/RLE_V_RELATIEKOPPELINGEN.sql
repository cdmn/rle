CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_relatiekoppelingen (numrelatie,numrelatie_voor,naam,nr_werkgever,persoonsnr,per_overlijdensdatum,rol_codrol_beperkt_tot,rol_codrol_met,soort_koppeling,dat_begin,dat_einde,code_dat_begin_fictief,code_dat_einde_fictief,mutatie_door,dat_mutatie,code_rol_in_koppeling) AS
SELECT rkn.rle_numrelatie numrelatie
     ,rkn.rle_numrelatie_voor numrelatie_voor
     , case
         when rkg.rol_codrol_met = 'PR'
           then per.naam
         else per.namrelatie
       end naam
     , case
         when rkg.rol_codrol_met = 'WG'
           then rle_lib.get_werkgevernr_relatie( rkn.rle_numrelatie_voor)
         else null
       end nr_werkgever
     , case
         when rkg.rol_codrol_met = 'PR'
           then rle_lib.get_persoonsnr_relatie( rkn.rle_numrelatie_voor)
         else null
       end persoonsnr
     , case
         when rkg.rol_codrol_met = 'PR'
           then per.datoverlijden
         else null
       end per_overlijdensdatum
     , rkg.rol_codrol_beperkt_tot rol_codrol_beperkt_tot
     , rkg.rol_codrol_met rol_codrol_met
     , rkg.omschrijving soort_koppeling
     , rkn.dat_begin dat_begin
     , rkn.dat_einde dat_einde
     , rkn.code_dat_begin_fictief code_dat_begin_fictief
     , rkn.code_dat_einde_fictief code_dat_einde_fictief
     , rkn.mutatie_door
     , rkn.dat_mutatie
     , rkg.code_rol_in_koppeling code_rol_in_koppeling
from   rle_relatie_koppelingen rkn
     , rle_rol_in_koppelingen rkg
     , ( select    ltrim( fnc_bepaal_voorletters( rle.voorletter ) || ' ' )
                || ltrim( case rle.code_aanduiding_naamgebruik
                            when 'E'
                              then vvgsl.waarde
                            when 'P'
                              then lower( rle.vvgsl_partner )
                            when 'V'
                              then lower( rle.vvgsl_partner )
                            when 'N'
                              then vvgsl.waarde
                            else vvgsl.waarde
                          end || ' ' )
                || rle_get_voorkeursnaam( rle.namrelatie
                                        , vvgsl.waarde
                                        , rle.naam_partner
                                        , lower( rle.vvgsl_partner )
                                        , rle.code_aanduiding_naamgebruik
                                        )
                || rtrim( ' ' || avgsl.waarde )
              , rtrim( rle_get_voorkeursnaam( rle.namrelatie
                                            , vvgsl.waarde
                                            , rle.naam_partner
                                            , lower( rle.vvgsl_partner )
                                            , rle.code_aanduiding_naamgebruik
                                            ) || ' ' || avgsl.waarde ) || ', ' || rtrim( fnc_bepaal_voorletters( rle.voorletter ) || ' ' ) naam
              , rle.namrelatie
              , rle.numrelatie
              , rle.datoverlijden
        from   comp_rle.rle_relaties rle
             , domeinwaarden vvgsl
             , domeinwaarden avgsl
        where  vvgsl.domeincode(+) = 'VVL'
        and    vvgsl.code(+) = rle.dwe_wrddom_vvgsl
        and    avgsl.domeincode(+) = 'AVL'
        and    avgsl.code(+) = rle.dwe_wrddom_avgsl ) per
  WHERE rkn.rkg_id = rkg.id
and    per.numrelatie = rkn.rle_numrelatie_voor
;