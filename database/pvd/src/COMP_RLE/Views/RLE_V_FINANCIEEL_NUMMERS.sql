CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_financieel_nummers ("ID",rle_numrelatie,bic,iban,datingang,dateinde,codwijzebetaling,machtiging_code,tekendatum,status,status_omschrijving) AS
SELECT fnr.id || nvl2(mtg.id, '-' || mtg.id, null)
       , fnr.rle_numrelatie
       , fnr.bic
       , fnr.iban
       , fnr.datingang
       , fnr.dateinde
       , fnr.codwijzebetaling
       , mtg.machtiging_code
       , mtg.tekendatum
       , mtg.status
       , (select rv_meaning from cg_ref_codes where rv_domain = 'MACHTIGING_STATUS' and rv_low_value =  mtg.status)
  from   rle_financieel_nummers fnr, bbs_machtigingen mtg
  WHERE mtg.fnr_id(+) = fnr.id
  and    iban is not null
  order by fnr.dateinde desc nulls first
 ;