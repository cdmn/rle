CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_relatiekoppelingen_cci (werkgevernummer,werkgevernummer2,rkn_dat_begin,rkn_dat_einde,werkgever_naam,soort_koppeling) AS
SELECT rec1.extern_relatie werkgevernummer
,      rec2.extern_relatie werkgevernummer2
,      rkn.dat_begin
,      rkn.dat_einde
,      nvl(rle.handelsnaam, rle.namrelatie) werkgever_naam
,      rkg.omschrijving soort_koppeling
from rle_relaties rle
inner join rle_relatie_koppelingen rkn on rle.numrelatie = rkn.rle_numrelatie_voor
inner join rle_relatie_externe_codes rec1 on rec1.rle_numrelatie = rkn.rle_numrelatie
inner join rle_relatie_externe_codes rec2 on rec2.rle_numrelatie = rkn.rle_numrelatie_voor
inner join rle_rol_in_koppelingen rkg on rkn.rkg_id = rkg.id
    where rec1.dwe_wrddom_extsys = 'WGR'
and   rec2.dwe_wrddom_extsys = 'WGR'
and   sysdate between rkn.dat_begin
                  and nvl(rkn.dat_einde, sysdate)
order by 1
 ;