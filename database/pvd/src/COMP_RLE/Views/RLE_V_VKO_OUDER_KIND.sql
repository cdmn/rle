CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_vko_ouder_kind (prsnr_ouder,numrelatie_ouder,zoeknaam_ouder,namrelatie_ouder,vvgsl_ouder,voorletter_ouder,datgeboorte_ouder,datoverlijden_ouder,numrelatie_kind,namrelatie_kind,voorletter_kind,vvgsl_kind,datgeboorte_kind,datoverlijden_kind,prsnr_kind) AS
SELECT rec.extern_relatie    prsnr_ouder
,      rec.rle_numrelatie    numrelatie_ouder
,      rle.zoeknaam          zoeknaam_ouder
,      rle.namrelatie        namrelatie_ouder
,      rle.dwe_wrddom_vvgsl  vvgsl_ouder
,      rle.voorletter        voorletter_ouder
,      rle.datgeboorte       datgeboorte_ouder
,      rle.datoverlijden     datoverlijden_ouder
,      rle1.numrelatie       numrelatie_kind
,      rle1.namrelatie       namrelatie_kind
,      rle1.voorletter       voorletter_kind
,      rle1.dwe_wrddom_vvgsl vvgsl_kind
,      rle1.datgeboorte      datgeboorte_kind
,      rle1.datoverlijden    datoverlijden_kind
,      rec1.extern_relatie   prsnr_kind
from rle_relaties              rle
,    rle_relatie_externe_codes rec
,    rle_relatie_koppelingen   rkn
,    rle_relaties              rle1
,    rle_relatie_externe_codes rec1
    where 1=1
and   rle.numrelatie            = rec.rle_numrelatie
and   rkn.rle_numrelatie        = rec.rle_numrelatie
and   rkn.rkg_id                = 3
and   rle1.numrelatie           = rkn.rle_numrelatie_voor
and   rec1.rle_numrelatie(+)    = rle1.numrelatie
and   rec1.dwe_wrddom_extsys(+) = 'PRS'
and   rec1.rol_codrol(+)        = 'PR'
and   rec.dwe_wrddom_extsys     = 'PRS'
and   rec.rol_codrol            = 'PR';