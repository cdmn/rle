CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_fnr_0080f (tenaamstelling,codorganisatie,indfiat,indinhown,dwe_wrddom_srtrek,"ID",rle_numrelatie,numrekening,bic,iban,codwijzebetaling,lnd_codland,datingang,dateinde,adm_code,coddoelrek,rol_codrol) AS
SELECT  TENAAMSTELLING
          , CODORGANISATIE
          , INDFIAT
          , INDINHOWN
          , DWE_WRDDOM_SRTREK
          , ID
          , RLE_NUMRELATIE
          , NUMREKENING
          , BIC
          , IBAN
          , CODWIJZEBETALING
          , LND_CODLAND
          , DATINGANG
          , DATEINDE
          , ADM_CODE
          , CODDOELREK
          , ROL_CODROL
FROM RLE_FINANCIEEL_NUMMERS FNR
 ;