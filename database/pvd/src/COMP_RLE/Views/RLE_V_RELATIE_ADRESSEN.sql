CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_relatie_adressen (externe_code_relatie,soort_relatie,rol_relatie,relatienummer,straat,woonplaats,locatie_omschrijving,postcode,huisnummer,huisnummer_toevoeging,landcode,ingangsdatum,einddatum,soort_adres,landnaam,ind_woonboot,ind_woonwagen,rol_voor_adres,kamernummer,ras_id) AS
SELECT rec.extern_relatie externe_code_relatie
  ,      rec.DWE_WRDDOM_EXTSYS soort_relatie
  ,      rec.ROL_CODROL rol_relatie
  ,      rle.numrelatie relatienummer
  ,      stt.STRAATNAAM straat
  ,      wps.WOONPLAATS
  ,      ras.LOCATIE locatie_omschrijving
  ,      decode(adr.lnd_codland, 'NL', substr(adr.POSTCODE, 1, 4)||' '||substr(adr.POSTCODE, 5), adr.POSTCODE) postcode
  ,      adr.HUISNUMMER
  ,      adr.TOEVHUISNUM huisnummer_toevoeging
  ,      adr.lnd_codland landcode
  ,      ras.DATINGANG ingangsdatum
  ,      nvl(ras.DATEINDE, to_date('31124000', 'ddmmyyyy')) einddatum
  ,      ras.DWE_WRDDOM_SRTADR soort_adres
  ,      lnd.NAAM landnaam
  ,      ras.IND_WOONBOOT
  ,      ras.IND_WOONWAGEN
  ,      ras.rol_codrol rol_voor_adres
  ,      ras.NUMKAMER kamernummer
  ,      ras.id ras_id
  from   rle_adressen adr
  ,      rle_relatie_adressen ras
  ,      rle_relaties rle
  ,      rle_relatie_externe_codes rec
  ,      rle_straten stt
  ,      rle_woonplaatsen wps
  ,      rle_landen lnd
  WHERE numadres = ras.ADS_NUMADRES
  and    rle.numrelatie = ras.rle_numrelatie
  and    rec.rle_numrelatie = rle.numrelatie
  and    adr.STT_STRAATID = stt.STRAATID
  and    adr.WPS_WOONPLAATSID =  wps.WOONPLAATSID
  and    adr.LND_CODLAND = lnd.CODLAND
 ;