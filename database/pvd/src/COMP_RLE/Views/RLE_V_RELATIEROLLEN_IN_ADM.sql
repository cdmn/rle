CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_relatierollen_in_adm (adm_code,adm_omschrijving,adm_type,adm_ind_afstemmen_gba_tgstn,rol_codrol,rle_numrelatie) AS
SELECT rie.adm_code adm_code
, adm.omschrijving adm_omschrijving
, adm.type adm_type
, adm.ind_afstemmen_gba_tgstn
, rie.rol_codrol rol_codrol
, rie.rle_numrelatie rle_numrelatie
from
  rle_relatierollen_in_adm rie
, rle_administraties adm
  WHERE rie.adm_code = adm.code and
  rie.adm_code like nvl(stm_context.administratie,rie.adm_code)
 ;