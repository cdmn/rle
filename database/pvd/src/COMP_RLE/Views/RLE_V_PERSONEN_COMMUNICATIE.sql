CREATE OR REPLACE FORCE VIEW comp_rle.rle_v_personen_communicatie (numrelatie,persoonsnummer,adm_code,dwe_wrddom_srtcom,srtcom_omschrijving,numcommunicatie) AS
with relatie_communicatie
       as (select distinct rle.numrelatie
                         , ccn.adm_code
                         , cck.dwe_wrddom_srtcom
                         , substr ( rfe_get_rwe_oms ( 'SC'
                                                    , cck.dwe_wrddom_srtcom
                                                    , 'J'
                                                    , 40 )
                                  , 1
                                  , 40 )
                             srtcom_omschrijving
           from   rle_relaties rle
                , rle_relatierollen_in_adm ria
                , rle_communicatie_categorieen ccn
                , rle_communicatie_cat_kanalen cck
           where  ria.rle_numrelatie = rle.numrelatie
           and    ria.rol_codrol = 'PR'
           and    ccn.adm_code = ria.adm_code
           and    ccn.adm_code <> 'ALG'
		   and    ccn.code != 'ALG'
           and    cck.ccn_id = ccn.id
           and    sysdate between ccn.datum_begin and nvl ( ccn.datum_einde, sysdate + 1 )
           and    sysdate between cck.datum_begin and nvl ( cck.datum_einde, sysdate + 1 )
		   and    cck.dwe_wrddom_srtcom NOT IN ('NIETGEWENST','POST')
           union
           select rle.numrelatie
                , ria.adm_code
                , 'MOBIEL'
                , substr ( rfe_get_rwe_oms ( 'SC'
                                           , 'MOBIEL'
                                           , 'J'
                                           , 40 )
                         , 1
                         , 40 )
                    srtcom_omschrijving
           from   rle_relaties rle, rle_relatierollen_in_adm ria
           where  ria.rle_numrelatie = rle.numrelatie
           and    ria.rol_codrol = 'PR'
           union
           select rle.numrelatie
                , ria.adm_code
                , 'EMAIL2'
                , substr ( rfe_get_rwe_oms ( 'SC'
                                           , 'EMAIL2'
                                           , 'J'
                                           , 40 )
                         , 1
                         , 40 )
                    srtcom_omschrijving
           from   rle_relaties rle, rle_relatierollen_in_adm ria
           where  ria.rle_numrelatie = rle.numrelatie
           and    ria.rol_codrol = 'PR'
		   union
           select rle.numrelatie
                , ria.adm_code
                , 'EMAIL'
                , substr ( rfe_get_rwe_oms ( 'SC'
                                           , 'EMAIL'
                                           , 'J'
                                           , 40 )
                         , 1
                         , 40 )
                    srtcom_omschrijving
           from   rle_relaties rle, rle_relatierollen_in_adm ria
           where  ria.rle_numrelatie = rle.numrelatie
           and    ria.rol_codrol = 'PR'
		   union
           select rle.numrelatie
                ,  ria.adm_code
                ,  'TEL'
                ,  substr ( rfe_get_rwe_oms ( 'SC'
                                            ,  'TEL'
                                            ,  'J'
                                            ,  40 )
                          ,  1
                          ,  40 )
                     srtcom_omschrijving
           from   rle_relaties rle, rle_relatierollen_in_adm ria
           where  ria.rle_numrelatie = rle.numrelatie
           and    ria.rol_codrol = 'PR'
		   )
  select rcm."NUMRELATIE"
       , rle_lib.get_persoonsnr_relatie ( rcm.numrelatie ) persoonsnummer
       , rcm."ADM_CODE"
       , rcm."DWE_WRDDOM_SRTCOM"
       , rcm."SRTCOM_OMSCHRIJVING"
       , cnr.numcommunicatie
  from   relatie_communicatie rcm, rle_communicatie_nummers cnr
  where  cnr.rle_numrelatie(+) = rcm.numrelatie
  and    cnr.dwe_wrddom_srtcom(+) = rcm.dwe_wrddom_srtcom
  and    cnr.adm_code(+) = rcm.adm_code
  and    sysdate between trunc ( cnr.datingang(+) ) and trunc ( nvl ( cnr.dateinde(+), sysdate + 1 ) )
  and    cnr.authentiek(+) = 'J'
 ;