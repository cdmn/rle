CREATE OR REPLACE procedure comp_rle.rle_mut_wgr_werkzaamheden
 (p_wgr_werkgnr in varchar2
 ,p_kodegvw in varchar2
 ,p_kodesgw in varchar2
 ,p_dat_begin in date
 ,p_dat_einde in date
 ,p_succes out boolean
 ,p_errorcode out varchar2
 )
 is
l_errorcode varchar2(2000);
begin
  -- doe de verwerking o.b.v. ingekomen parameters
  rle_rie.kop_wgr_wzd(p_wgr_werkgnr => p_wgr_werkgnr
                     ,p_kodegvw     => p_kodegvw
                     ,p_kodesgw     => p_kodesgw
		     ,p_dat_begin   => p_dat_begin
		     ,p_dat_einde   => p_dat_einde
                     ,p_error       => l_errorcode
                     );
  --
  if l_errorcode is null
  then
     p_succes := true;
  else
    p_errorcode := l_errorcode;
    p_succes := false;
  end if;
exception
  when others
  then
    p_errorcode := sqlerrm;
    p_succes    := false;
end rle_mut_wgr_werkzaamheden;
 
/