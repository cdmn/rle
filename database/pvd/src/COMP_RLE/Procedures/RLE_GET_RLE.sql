CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_RLE
 (P_NUMRELATIE IN NUMBER
 ,P_CODORGANISATIE IN OUT VARCHAR2
 ,P_NAMRELATIE IN OUT VARCHAR2
 ,P_INDGEWNAAM IN OUT VARCHAR2
 ,P_INDINSTELLING IN OUT VARCHAR2
 ,P_DATSCHONING IN OUT DATE
 ,P_TITEL IN OUT VARCHAR2
 ,P_TTITEL IN OUT VARCHAR2
 ,P_AVGSL IN OUT VARCHAR2
 ,P_VVGSL IN OUT VARCHAR2
 ,P_BRGST IN OUT VARCHAR2
 ,P_GESLACHT IN OUT VARCHAR2
 ,P_GEWATITEL IN OUT VARCHAR2
 ,P_VOORNAAM IN OUT VARCHAR2
 ,P_RVORM IN OUT VARCHAR2
 ,P_AANSCHRIJFNAAM IN OUT VARCHAR2
 ,P_VOORLETTER IN OUT VARCHAR2
 ,P_DATGEBOORTE IN OUT VARCHAR2
 ,P_CODE_GEBDAT_FICTIEF IN OUT VARCHAR2
 ,P_DATOVERLIJDEN IN OUT DATE
 ,P_DATOPRICHTING IN OUT DATE
 ,P_DATEINDE IN OUT DATE
 ,P_NAAM_PARTNER IN OUT VARCHAR2
 ,P_VVGSL_PARTNER IN OUT VARCHAR2
 ,P_CODE_AANDUIDING_NGK IN OUT VARCHAR2
 ,P_HANDELSNAAM OUT RLE_RELATIES.HANDELSNAAM%TYPE
 ,P_DATBEGIN OUT RLE_RELATIES.DATBEGIN%TYPE
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
select	*
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Program Data
R_RLE_001 C_RLE_001%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
OPEN  c_rle_001(p_numrelatie);
  FETCH c_rle_001 INTO r_rle_001;
  IF c_rle_001%found THEN
    p_codorganisatie      := r_rle_001.codorganisatie;
    p_namrelatie          := r_rle_001.namrelatie;
    p_indgewnaam          := r_rle_001.indgewnaam;
    p_indinstelling       := r_rle_001.indinstelling;
    p_datschoning         := r_rle_001.datschoning;
    p_titel               := r_rle_001.dwe_wrddom_titel;
    p_ttitel              := r_rle_001.dwe_wrddom_ttitel;
    p_avgsl               := r_rle_001.dwe_wrddom_avgsl;
    p_vvgsl               := r_rle_001.dwe_wrddom_vvgsl;
    p_brgst               := r_rle_001.dwe_wrddom_brgst;
    p_geslacht            := r_rle_001.dwe_wrddom_geslacht;
    p_gewatitel           := r_rle_001.dwe_wrddom_gewatitel;
    p_rvorm               := r_rle_001.dwe_wrddom_rvorm;
    p_voornaam            := r_rle_001.voornaam;
    p_voorletter          := r_rle_001.voorletter;
    p_aanschrijfnaam      := r_rle_001.aanschrijfnaam;
    p_datgeboorte         := TO_CHAR(r_rle_001.datgeboorte,'DD-MM-YYYY');
    p_code_gebdat_fictief := r_rle_001.code_gebdat_fictief;
    p_datoverlijden       := r_rle_001.datoverlijden;
    p_dateinde            := r_rle_001.dateinde;
    p_naam_partner        := r_rle_001.naam_partner;
    p_vvgsl_partner       := r_rle_001.vvgsl_partner;
    p_code_aanduiding_ngk := r_rle_001.code_aanduiding_naamgebruik;
    p_handelsnaam         := r_rle_001.handelsnaam ;
    p_datbegin            := r_rle_001.datbegin ;
  ELSE
    p_codorganisatie      := NULL;
    p_namrelatie          := NULL;
    p_indgewnaam          := NULL;
    p_indinstelling       := NULL;
    p_datschoning         := NULL;
    p_titel               := NULL;
    p_ttitel              := NULL;
    p_avgsl               := NULL;
    p_vvgsl               := NULL;
    p_brgst               := NULL;
    p_geslacht            := NULL;
    p_gewatitel           := NULL;
    p_rvorm               := NULL;
    p_voornaam            := NULL;
    p_voorletter          := NULL;
    p_aanschrijfnaam      := NULL;
    p_datgeboorte         := NULL;
    p_code_gebdat_fictief := NULL;
    p_datoverlijden       := NULL;
    p_datoprichting       := NULL;
    p_dateinde            := NULL;
    p_naam_partner        := NULL;
    p_vvgsl_partner       := NULL;
    p_code_aanduiding_ngk := NULL;
    p_handelsnaam         := NULL ;
    p_datbegin            := NULL ;
  END IF;
  CLOSE c_rle_001;
END RLE_GET_RLE;

 
/