CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_WIJZIG_PERSOON
 (PIN_PERSOONSNUMMER IN NUMBER
 ,PIN_SOFINUMMER IN NUMBER
 ,PIN_A_NUMMER IN NUMBER
 ,PIV_GBA_STATUS IN VARCHAR2
 ,PIV_GBA_AFNEMERS_IND IN VARCHAR2
 ,PIV_NAAM IN VARCHAR2
 ,PIV_VOORLETTERS IN VARCHAR2
 ,PIV_VOORVOEGSELS IN VARCHAR2
 ,PID_GEBOORTEDATUM IN DATE
 ,PIV_GESLACHT IN VARCHAR2
 ,PID_OVERLIJDENSDATUM IN DATE
 ,PID_REGDAT_OVERLIJDEN IN DATE
 ,PID_EMIGRATIEDATUM IN DATE
 ,PIV_NAAM_PARTNER IN VARCHAR2
 ,PIV_VVGSL_PARTNER IN VARCHAR2
 ,POV_VERWERKT OUT VARCHAR2
 ,POV_FOUTMELDING OUT VARCHAR2
 ,P_AANDUIDING_NAAMGEBRUIK IN VARCHAR2 := 'null'
 ,PIV_OFMW_MUTATIE IN BOOLEAN := false
 ,PIV_CODE_GEBOORTEDATUM_FICTIEF IN VARCHAR2 := '0'
 ,PIV_VOORNAAM IN VARCHAR2 := null
 )
 IS

LN_NUMRELATIE NUMBER;
L_CODE_AANDUIDING_NAAMGEBRUIK VARCHAR2(30);

OTHERS EXCEPTION;

CURSOR C_RLE_030
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_030 */
select rle.naam_partner
from   rle_relaties rle
where  rle.numrelatie = b_numrelatie
;
r_rle_030  c_rle_030%ROWTYPE;
l_aanduiding_naamgebruik   varchar2(1);
begin
   /* De volgende acties niet doorgeven aan het GBA. */
   if not piv_ofmw_mutatie  -- xjb 5-5-2014, wel/niet doorgeven aan comp_gba nalv mutaties via PersoonService op de middleware
   then
     rle_indicatie.doorgeven_aan_gba := 'N' ;
   else
     rle_indicatie.doorgeven_aan_gba := 'J' ;
   end if;

   /* Default uitvoerwaarden */
   pov_verwerkt    := 'N' ;
   pov_foutmelding := NULL ;


   /* Bepalen relatienummer */
   rle_m06_rec_relnum( pin_persoonsnummer
                     , 'PRS'
                     , 'PR'
                     , SYSDATE
                     , ln_numrelatie
                     ) ;


   /* Als het relatienummer niet gevonden kan
      worden dan hebben we een probleem.
   */ /* */
   IF ln_numrelatie IS NULL
   THEN
      pov_verwerkt := 'N' ;
      pov_foutmelding := 'Het relatienummer kan niet worden bepaald met het gegeven       persoonsnummer.' ;

      RETURN ;
  END IF ;

  IF NVL(PIV_GBA_STATUS, 'OV') <> 'AF' THEN   -- xcw: indien gba status leeg toch relatiegegevens bijwerken

   if not piv_ofmw_mutatie
   then

     /* Verwerken sofinummer */
     rle_gba_wijzig_rec( ln_numrelatie
                       , 'SOFI'
                       , 'PR'
                       , pin_sofinummer
                       ) ;

     /* Verwerken anummer */
     rle_gba_wijzig_rec( ln_numrelatie
                       , 'GBA'
                       , 'PR'
                       , pin_a_nummer
                       ) ;

     /* Verwerken gba-status en afnemersindicatie */
     rle_gba_wijzig_gba( ln_numrelatie
                       , piv_gba_status
                       , piv_gba_afnemers_ind
                       , NULL
                       ) ;
   else
     -- piv_ofmw_mutatie
     /* Verwerken sofinummer, andere aanroep want anders wordt de doorgeef indicatie aan gba op N gezet */
     IF pin_sofinummer != NVL( rle_m03_rec_extcod( 'SOFI'
                                                 , ln_numrelatie
                                                 , 'PR'
                                                 , SYSDATE
                                                 )
                             , '-1'
                             )
     THEN
        rle_m16_rec_verwerk( pin_sofinummer
                           , 'SOFI'
                           , 'PR'
                           , ln_numrelatie
                           , TRUNC( SYSDATE )
                           , NULL
                           ) ;
     END IF ;

     /* Verwerken anummer, andere aanroep want anders wordt de doorgeef indicatie aan gba op N gezet */
     IF pin_a_nummer != NVL( rle_m03_rec_extcod( 'GBA'
                                               , ln_numrelatie
                                               , 'PR'
                                               , SYSDATE
                                               )
                             , '-1'
                             )
     THEN
        rle_m16_rec_verwerk( pin_a_nummer
                           , 'GBA'
                           , 'PR'
                           , ln_numrelatie
                           , TRUNC( SYSDATE )
                           , NULL
                           ) ;
     END IF ;

     if    piv_gba_status is not null
        or piv_gba_afnemers_ind is not null
     then
       rle_gba_wijzig_gba( ln_numrelatie
                         , piv_gba_status
                         , piv_gba_afnemers_ind
                         , NULL
                         );
       if piv_gba_status is not null
       then
         gba_ofmw_api.set_status_aanmelding( p_persoonsnummer => pin_persoonsnummer
                                           , p_status => piv_gba_status
                                           );
       end if;
       if piv_gba_afnemers_ind is not null
       then
         gba_ofmw_api.set_afnemersindicatie( p_persoonsnummer => pin_persoonsnummer
                                           , p_afnemersindicatie => piv_gba_afnemers_ind
                                           );
       end if;
     end if;
   end if;

   /* xcw : indien geen partner naam bekend aanduiding_naamgebruik = E */
   l_aanduiding_naamgebruik := p_aanduiding_naamgebruik;
   if l_aanduiding_naamgebruik in ( 'N', 'V', 'P' )
      and piv_naam_partner is null
   then
     l_aanduiding_naamgebruik := 'E';
   end if;

  if not piv_ofmw_mutatie  -- xjb 05-05-2014 verwerking vanaf middleware op eigen wijze, zie de else tak
  then
     /* Wijzigen relatie */
     UPDATE rle_relaties  rle
     SET    rle.namrelatie          = piv_naam
     ,      rle.voorletter          = piv_voorletters
     ,      rle.dwe_wrddom_vvgsl    = UPPER( REPLACE( piv_voorvoegsels, ' ' ) )
     ,      rle.datgeboorte         = pid_geboortedatum
     ,      rle.dwe_wrddom_geslacht = piv_geslacht
     ,      rle.datoverlijden       = pid_overlijdensdatum
     ,      rle.regdat_overlbevest  = pid_regdat_overlijden
     ,      rle.datemigratie        = pid_emigratiedatum
     ,      rle.naam_partner        = piv_naam_partner
     ,      rle.vvgsl_partner       = UPPER( REPLACE( piv_vvgsl_partner, ' ' ) )
     ,      rle.code_aanduiding_naamgebruik = NVL(l_aanduiding_naamgebruik,rle.code_aanduiding_naamgebruik )
     WHERE  rle.numrelatie = ln_numrelatie
     ;
  else
     -- xjb 05-05-2014
     /* Wijzigen relatie, wijziging afkomstig vanaf Oracle Fusion Middleware (via de PersoonService) */
     -- De velden wijzigen zoals opgegeven, we krijgen vanuit registreerPersoon of wijzigPersoon
     UPDATE rle_relaties  rle
     SET    rle.namrelatie          = nvl(piv_naam, rle.namrelatie)
     ,      rle.voorletter          = piv_voorletters
     ,      rle.dwe_wrddom_vvgsl    = UPPER( REPLACE( piv_voorvoegsels, ' ' ) )
     ,      rle.datgeboorte         = pid_geboortedatum
     ,      rle.dwe_wrddom_geslacht = piv_geslacht
     ,      rle.datoverlijden       = pid_overlijdensdatum
     ,      rle.regdat_overlbevest  = pid_regdat_overlijden
     ,      rle.datemigratie        = pid_emigratiedatum
     ,      rle.naam_partner        = piv_naam_partner
     ,      rle.vvgsl_partner       = UPPER( REPLACE( piv_vvgsl_partner, ' ' ) )
     ,      rle.code_aanduiding_naamgebruik = NVL(l_aanduiding_naamgebruik,rle.code_aanduiding_naamgebruik )
     ,      rle.code_gebdat_fictief = piv_code_geboortedatum_fictief
     ,      rle.voornaam            = piv_voornaam
     WHERE  rle.numrelatie = ln_numrelatie
     ;
   end if; -- xjb 05-05-2014

   /* Verwerking succesvol.
      Er hoeft niet getest te worden of de relatie bestaat.
      Dit is inherent aan het vinden van het relatienummer
      in de tabel RLE_RELATIE_EXTERNE_CODES.
   */

   ELSE
     /* Verwerken gba-status en afnemersindicatie */
     if not piv_ofmw_mutatie
     then
       rle_gba_wijzig_gba( ln_numrelatie
                         , piv_gba_status
                         , piv_gba_afnemers_ind
                         , NULL
                         ) ;
     else
       -- piv_ofmw_mutatie
       if    piv_gba_status is not null
          or piv_gba_afnemers_ind is not null
       then
         rle_gba_wijzig_gba( ln_numrelatie
                           , piv_gba_status
                           , piv_gba_afnemers_ind
                           , NULL
                           );
         if piv_gba_status is not null
         then
           gba_ofmw_api.set_status_aanmelding( p_persoonsnummer => pin_persoonsnummer
                                             , p_status => piv_gba_status
                                             );
         end if;
         if piv_gba_afnemers_ind is not null
         then
           gba_ofmw_api.set_afnemersindicatie( p_persoonsnummer => pin_persoonsnummer
                                             , p_afnemersindicatie => piv_gba_afnemers_ind
                                             );
         end if;
       end if;
     end if;

   END IF;


   pov_verwerkt := 'J' ;


   /* Hierna volgende acties wel weer doorgeven aan GBA */
   rle_indicatie.doorgeven_aan_gba := 'J' ;



EXCEPTION
 WHEN OTHERS THEN
     /* Reset indicatie */

   rle_indicatie.doorgeven_aan_gba := 'J' ;



   /* Uitvoerwaarden */

   pov_verwerkt    := 'N' ;

   pov_foutmelding := SQLERRM ;
end rle_gba_wijzig_persoon;
/