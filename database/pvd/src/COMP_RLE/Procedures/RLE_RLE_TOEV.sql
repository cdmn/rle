CREATE OR REPLACE procedure comp_rle.rle_rle_toev
 (p_numrelatie in out number
 ,p_namrelatie in varchar2
 ,p_rol_codrol in varchar2
 ,p_indinstelling in varchar2
 ,p_wrddom_geslacht in varchar2
 ,p_wrddom_vvgsl in varchar2
 ,p_voorletter in varchar2
 ,p_voornaam in varchar2
 ,p_handelsnaam in varchar2
 ,p_datbegin in date
 ,p_dateinde in date
 ,p_datoprichting in date
 ,p_datgeboorte in date
 ,p_code_gebdat_fictief in varchar2
 ,p_datoverlijden in date
 ,p_datregoverlijdenbevestiging in date
 ,p_code_aanduiding_naamgebruik in varchar2
 ,p_naam_partner in varchar2
 ,p_vvgsl_partner in varchar2
 ,p_datemigratie in date
 ,p_wrddom_gewatitel in varchar2
 ,p_ind_vervallen in varchar2 := 'N'
 ,p_ind_commit in varchar2 := 'N'
 ,p_ind_succes out varchar2
 ,p_melding out varchar2
 ,p_externe_code out varchar2
 ,p_code_land in varchar2 := null
 ,p_srt_adres in varchar2 := null
 ,p_postcode in varchar2 := null
 ,p_huisnummer in varchar2 := null
 ,p_toevoeging_huisnummer in varchar2 := null
 ,p_woonplaats in varchar2 := null
 ,p_straat in varchar2 := null
 ,p_ingangsdatum_adres in date := null
 ,p_einddatum_adres in date := null
 ,p_adresnummer in number := null
 ,p_locatie in varchar2 := null
 ,p_ind_woonboot in varchar2 := null
 ,p_ind_woonwagen in varchar2 := null
 ,p_provincie in varchar2 := null
 )
 is
  /*
  WIJZIGINGSHISTORIE
  ------------------
  Datum    Wie    Omschrijving
  25-04-2000  Veldman    p_jurnaam is vervallen i.v.m. vervallen
                        kolom jurnaam.
  12-10-2000  Willem de Boer  update-gedeelte aangepast: velden die niet
                              wijzigen, moeten niet worden aangepast.
  21-03-2001  A.v.Brummelen  Code naamgebruik wordt default V ipv P
  27-02-2002  R. Willems     Wijzigingen n.a.v. F.O. RLE0001
  23-09-2002  PKG   Bev.282: Rol 'DN' vervangen door 'WN'
  10-11-2002  lme    toevoegen inputparameters voor adres
                    om een controle op mogelijk reeds voorkomen
                    van relatie te doen.
  14-11-2002  iko    bev. 538, controle op geboortedatum toegevoegd.
  28-11-2002  gbe    voorwaarden aan aanroep van rle_rle_ads_toev
                     toegevoegd (rol_codrol)
  08-04-2003  iko   bev.1172, opvoeren van CR toegevoegd.
  31-08-2004  jku   ivm change 11632 de raise aangepast
                    zodat de lokatie zichtbaar wordt in staffware.
                    Tevens bij de insert op relaties de waarde van
                    datoprichting verwijderd. Dit was door iemand in
                    designer gezet maar was nooit opgeleverd ?
  01-09-2008  NKU    Toevoegen relatie Verzekeraar (rol VR) mogelijk gemaakt.
  15-04-2009  XPT   Indien systeem parameter PERCDUP 100% ofwel 1 oplevert
                    dient geen ontdubbelslag te worden uitgevoerd.
  03-04-2012  XCW   Afsplitsing van huisnummer toevoeging
                   (vanwege buitenlandse adressen via ABzend)
  21-01-2014 WHR    Herba code_naamgebruik werd leeggemaakt bij een update rle_relaties
                    l_code_naamgebruik vervangen door p_code_aanduiding_naamgebruik
  27-03-2014 WHR    Herba P_NAAM_PARTNER en P_VVGSL_PARTNER ook bij de update van relaties gebruiken
                    namen parameters bij aanroep rle_rle_ads_toev
  28-06-2016 VER    MNGBA-1090: bij reeds bekende personen ook OA opvoeren indien nog niet aanwezig
  22-12-2016 VER    MNGBA-1090/PE-949, controle op voorvoegsels toegevoegd, incl. exception
  25-01-2017 LHT    MNGBA-1090/PE-949 controle op voorvoegsels partner toegevoegd, incl. exception
  */

  cursor c_rle_001(b_numrelatie number)
  is
  select 1 from rle_relaties
  where numrelatie = b_numrelatie
  ;
  --
  t_mogelijke_dubbelen  rle_dubbel.tab_dubbel_rle;
  l_dubbelen_gevonden  varchar2(1);
  l_nieuwe_relatie  boolean;
  r_rle_001  c_rle_001%rowtype;
  l_wrddom_vvgsl varchar2(20);
  l_vvgsl_partner varchar2(20);
  l_numadres  number;
  l_code_land  varchar2(100);
  l_postcode  varchar2(15);
  l_huisnummer  number;
  l_toevoeging_huisnummer  varchar2(100);
  l_straat  varchar2(200);
  l_woonplaats  varchar2(100);
  l_rol_codrol  varchar2(2);
  l_lokatie  varchar2(6);

  l_foutmelding varchar2(2000);
  e_onbekend_voorvoegsel exception;
begin
  p_melding := null;
  p_ind_succes := 'J';
  l_nieuwe_relatie := false;
  l_code_land := p_code_land;
  l_postcode := p_postcode;

  if p_wrddom_vvgsl is not null
  then
    begin
      l_wrddom_vvgsl := rfe_get_waarde_kort ( p_rfe_code => 'VVL'
                                            , p_waarde => p_wrddom_vvgsl
                                            );
      exception
      when others
      then
        l_foutmelding := sqlerrm;
        l_wrddom_vvgsl := upper(p_wrddom_vvgsl);
        raise e_onbekend_voorvoegsel;
    end;
  end if;

  if p_vvgsl_partner is not null
  then
    begin
      l_vvgsl_partner := rfe_get_waarde_kort ( p_rfe_code => 'VVL'
                                            , p_waarde => p_vvgsl_partner
                                            );
      exception
      when others
      then
        l_foutmelding := sqlerrm;
        l_vvgsl_partner := upper(p_vvgsl_partner);
        raise e_onbekend_voorvoegsel;
    end;
  end if;

  begin
    l_huisnummer := to_number(p_huisnummer);
  exception
    when others
    then
      l_huisnummer := to_number(regexp_substr(p_huisnummer,'^[[:digit:]]+',1));
      l_toevoeging_huisnummer := trim(replace(p_huisnummer,regexp_substr(p_huisnummer,'^[[:digit:]]+ *',1),''));
  end;

  l_toevoeging_huisnummer := nvl(p_toevoeging_huisnummer, l_toevoeging_huisnummer);
  l_straat := p_straat;
  l_woonplaats  := p_woonplaats;

  l_lokatie := '000010';
  if upper(p_ind_vervallen) = 'J'
  then
    l_lokatie := '000020';
    delete from rle_relaties
    where numrelatie = p_numrelatie
    ;
  else
    /* controles uitvoeren */
    l_lokatie := '000030';
    if p_rol_codrol IN( 'PR', 'WN', 'BG' )
    then
      if p_datgeboorte is null
      then
        l_lokatie := '000031';
        stm_dbproc.raise_error( 'RLE-00552', null, 'RLE_RLE_TOEV' ) ;
      else
        l_lokatie := '000040';
        rle_sysdate_chk( p_datum =>  p_datgeboorte
                       , p_datumoms => 'Geboortedatum'
                       );
      end if;

      if    upper( p_wrddom_gewatitel ) = 'DHR'
        and upper( p_wrddom_geslacht ) <> 'M'
      then
        l_lokatie := '000050';
        stm_dbproc.raise_error( 'RLE-10332', null, 'RLE_RLE_TOEV' ) ;
      elsif upper( p_wrddom_gewatitel ) = 'MVR'
        and upper( p_wrddom_geslacht ) <> 'V'
      then
        l_lokatie := '000060';
        stm_dbproc.raise_error( 'RLE-10332', null, 'RLE_RLE_TOEV' ) ;
      end if;
    end if;
    l_lokatie := '000070';

    /* kijken of relatie al voorkomt. Indien niet, dan toevoegen
    */
    if p_numrelatie is null
    then
      l_lokatie := '000080';
      p_numrelatie := rle_rle_seq1_next_id ;
    end if ;

    l_lokatie := '000090';
    open c_rle_001(b_numrelatie => p_numrelatie) ;
    fetch c_rle_001 into r_rle_001 ;

    if c_rle_001%notfound
    then
      l_lokatie := '000100';
      /* controle of deze relatie mogelijk al voorkomt
         indien de systeem parm PERCDUP 1 retourneert hoeft geen rekening gehouden te worden
         met ontdubbelen.
      */
      if p_rol_codrol not in ('VR') and stm_algm_get.sysparm('RLE','PERCDUP',sysdate) != '1'
      then
        if p_code_land is not null
        then
          l_lokatie := '000110';
          rle_dubbel.relatie_aanwezig( p_naam => p_namrelatie
                                     , p_dubbel_gevonden => l_dubbelen_gevonden
                                     , p_gevonden_relaties => t_mogelijke_dubbelen
                                     , p_postcode => p_postcode
                                     , p_huisnummer => l_huisnummer
                                     , p_geboortedatum => p_datgeboorte
                                     , p_voorletters => p_voorletter
                                     );
          if l_dubbelen_gevonden = 'J'
          then
            l_lokatie := '000120';
            raise_application_error(-20000,'RLE-00397');
          end if;
        end if;
      end if;

      l_lokatie := '000130';
      l_nieuwe_relatie := true;
      insert into rle_relaties
        ( numrelatie
        , namrelatie
        , indinstelling
        , dwe_wrddom_geslacht
        , dwe_wrddom_vvgsl
        , voorletter
        , voornaam
        , handelsnaam
        , datbegin
        , dateinde
        , datgeboorte
        , code_gebdat_fictief
        , datoverlijden
        , regdat_overlbevest
        , code_aanduiding_naamgebruik
        , codorganisatie
        , indgewnaam
        , zoeknaam
        , dwe_wrddom_titel
        , dwe_wrddom_ttitel
        , dwe_wrddom_avgsl
        , dwe_wrddom_brgst
        , dwe_wrddom_gewatitel
        , dwe_wrddom_rvorm
        , aanschrijfnaam
        , datschoning
        , datemigratie
        , naam_partner
        , vvgsl_partner
        )
      values
        ( p_numrelatie
        , ltrim(rtrim(p_namrelatie))
        , upper(p_indinstelling)
        , upper(p_wrddom_geslacht)
        , l_wrddom_vvgsl
        , upper(p_voorletter)
        , p_voornaam
        , p_handelsnaam
        , p_datbegin
        , p_dateinde
        , p_datgeboorte
        , p_code_gebdat_fictief
        , p_datoverlijden
        , p_datregoverlijdenbevestiging
        , upper(nvl(p_code_aanduiding_naamgebruik,'E'))
        , 'A'   -- codorganisatie
        , 'N'   -- indgewnaam
        , null  -- zoeknaam
        , null  -- dwe_wrddom_titel
        , null  -- dwe_wrddom_ttitel
        , null  -- dwe_wrddom_avgsl
        , null  -- dwe_wrddom_brgst
        , upper(p_wrddom_gewatitel)
        , null  -- dwe_wrddom_rvorm
        , null  -- aanschrijfnaam
        , null  -- datschoning
        , p_datemigratie
        , p_naam_partner
        , l_vvgsl_partner
        ) ;
    else
      l_lokatie := '000140';

      update rle_relaties
      set    namrelatie = ltrim(rtrim( p_namrelatie))
      ,      indinstelling = upper(p_indinstelling)
      ,      dwe_wrddom_geslacht = upper(p_wrddom_geslacht)
      ,      dwe_wrddom_vvgsl = l_wrddom_vvgsl
      ,      voorletter = upper(p_voorletter)
      ,      voornaam = p_voornaam
      ,      handelsnaam = p_handelsnaam
      ,      datbegin = p_datbegin
      ,      dateinde = p_dateinde
      ,      datoprichting = p_datoprichting
      ,      datgeboorte = p_datgeboorte
      ,      code_gebdat_fictief = p_code_gebdat_fictief
      ,      datoverlijden = p_datoverlijden
      ,      regdat_overlbevest = p_datregoverlijdenbevestiging
      ,      code_aanduiding_naamgebruik = upper(nvl(p_code_aanduiding_naamgebruik,code_aanduiding_naamgebruik))
      ,      datemigratie = p_datemigratie
      ,      naam_partner = p_naam_partner
      ,      vvgsl_partner = l_vvgsl_partner
      where  numrelatie = p_numrelatie
      ;
      l_lokatie := '000150';

    end if;
    close c_rle_001;
    /* toevoegen rol */
    if l_nieuwe_relatie
    then
      l_lokatie := '000160';
      insert into rle_relatie_rollen
        ( rle_numrelatie
        , rol_codrol
        , codorganisatie
        )
      values
        ( p_numrelatie
        , p_rol_codrol
        , 'A'
        );

      if p_rol_codrol IN ( 'BG', 'WN' )
      then
        insert into rle_relatie_rollen
          ( rle_numrelatie
          , rol_codrol
          , codorganisatie
          )
        values
          ( p_numrelatie
          , 'PR'
          , 'A'
          );
      end if;
    end if;

    l_lokatie := '000180';
    p_externe_code := null ;

    /* Toevoegen persoonsnummer */
    if    l_nieuwe_relatie
      and p_rol_codrol in ( 'PR', 'WN', 'BG' )
    then
      l_lokatie := '000190';
      insert into rle_relatie_externe_codes
        ( id
        , rle_numrelatie
        , dwe_wrddom_extsys
        , datingang
        , codorganisatie
        , rol_codrol
        , extern_relatie
        )
      values
        ( rle_rec_seq1.nextval
        , p_numrelatie
        , 'PRS'
        , sysdate
        , 'A'
        , 'PR'
        , rle_persoonsnummer_seq1.nextval
        )
      returning extern_relatie into p_externe_code;
    end if ;

    l_lokatie := '000200';
    /* toevoegen administratiekantoornummer */
    if     l_nieuwe_relatie
       and p_rol_codrol     in ( 'AK' )
    then
      l_lokatie := '000210';
      insert into rle_relatie_externe_codes
        ( id
        , rle_numrelatie
        , dwe_wrddom_extsys
        , datingang
        , codorganisatie
        , rol_codrol
        , extern_relatie
        )
      values
        ( rle_rec_seq1.nextval
        , p_numrelatie
        , rle_get_extsys_voor_codrol(piv_codrol => 'AK')
        , sysdate
        , 'A'
        , 'AK'
        , rle_adminkantoornummer_seq1.nextval
        )
      returning extern_relatie into p_externe_code;
    end if;

    l_lokatie := '000220';
    /* Toevoegen curatornummer */
    if     l_nieuwe_relatie
       and p_rol_codrol = 'CR'
    then
      l_lokatie := '000230';
      insert into rle_relatie_externe_codes
        ( id
        , rle_numrelatie
        , dwe_wrddom_extsys
        , datingang
        , codorganisatie
        , rol_codrol
        , extern_relatie
        )
      values
        ( rle_rec_seq1.nextval
        , p_numrelatie
        , rle_get_extsys_voor_codrol(piv_codrol => 'CR')
        , sysdate
        , 'A'
        , 'CR'
        , rle_curatornummer_seq1.nextval
        )
      returning extern_relatie into p_externe_code;
    end if ;

     /* Toevoegen Verzekeraarsnummer */
    if     l_nieuwe_relatie
       and p_rol_codrol = 'VR'
    then
      l_lokatie := '000231';
      insert into rle_relatie_externe_codes
        ( id
        , rle_numrelatie
        , dwe_wrddom_extsys
        , datingang
        , codorganisatie
        , rol_codrol
        , extern_relatie
        )
      values
        ( rle_rec_seq1.nextval
        , p_numrelatie
        , rle_get_extsys_voor_codrol(piv_codrol => 'VR')
        , sysdate
        , 'A'
        , 'VR'
        , p_numrelatie
        )
      returning extern_relatie into p_externe_code;
    end if;
    l_lokatie := '000240';

    /* Toevoegen adres */
    if   p_code_land is not null
       or p_rol_codrol = 'VR'
    then
      if p_rol_codrol in ('WN','BG')
      then
        l_rol_codrol := 'PR';
      else
        l_rol_codrol := p_rol_codrol;
      end if;

      l_lokatie := '000250';
      rle_rle_ads_toev ( p_numrelatie => p_numrelatie
                       , p_codrol => l_rol_codrol
                       , p_srt_adres => p_srt_adres
                       , p_begindatum => p_ingangsdatum_adres
                       , p_codland => l_code_land
                       , p_postcod => l_postcode
                       , p_huisnum => l_huisnummer
                       , p_toevnum => l_toevoeging_huisnummer
                       , p_woonpl => l_woonplaats
                       , p_straat => l_straat
                       , p_einddatum => p_einddatum_adres
                       , p_locatie => p_locatie
                       , p_numadres => l_numadres
                       , p_ind_woonwagen => p_ind_woonwagen
                       , p_ind_woonboot => p_ind_woonboot
                       , p_provincie => p_provincie
                       );
    end if;

    l_lokatie := '000260';
    /* Opzoeken extern nummer */

    if p_externe_code is null
    then
      begin
        if p_rol_codrol in ( 'PR', 'WN', 'BG' )
        then
          l_lokatie := '000270';
          p_externe_code := rle_m03_rec_extcod( p_systeemcomp => 'PRS'
                                              , p_relnum => p_numrelatie
                                              , p_codrol => 'PR'
                                              , p_datparm => sysdate
                                              ) ;
        else
          l_lokatie := '000280';
          p_externe_code := rle_m03_rec_extcod( p_systeemcomp => rle_get_extsys_voor_codrol( piv_codrol => p_rol_codrol )
                                              , p_relnum => p_numrelatie
                                              , p_codrol => p_rol_codrol
                                              , p_datparm => sysdate
                                              ) ;
        end if;
      exception
        when others
        then
          p_externe_code := null;
      end;
    end if;
  end if;

  l_lokatie := '000290';
  if p_ind_commit = 'J'
  then
    l_lokatie := '000300';
    commit;
  end if;

  l_lokatie := '000310';
exception
  when e_onbekend_voorvoegsel
  then
    p_ind_succes := 'N';
    p_melding    := l_foutmelding ;
    stm_dbproc.raise_error('RLE_RLE_TOEV: '||l_foutmelding);

  when others
  then
    p_ind_succes := 'N';
    p_melding    := sqlerrm ;
    stm_dbproc.raise_error('RLE_RLE_TOEV: '||nvl(l_lokatie,'leeg')||' '||sqlerrm);
end rle_rle_toev;
/