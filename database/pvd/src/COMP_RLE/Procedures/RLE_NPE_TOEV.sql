CREATE OR REPLACE PROCEDURE comp_rle.RLE_NPE_TOEV
 (P_POSTCODE VARCHAR2
 ,P_WOONPLAATS VARCHAR2
 ,P_STRAATNAAM VARCHAR2
 ,P_HUISNUMMER NUMBER
 )
 IS
-- Program Data
L_CODREEKS NUMBER;
-- PL/SQL Block
BEGIN
if mod(p_huisnummer,2)= 0 then
  l_codreeks := 1;
else
  l_codreeks := 2;
end if;
insert into rle_ned_postcode(
    POSTCODE
  , CODREEKS
  , NUMSCHEIDINGVAN
  , NUMSCHEIDINGTM
  , CODORGANISATIE
  , INDPOSTCHAND
  , WOONPLAATS
  , STRAATNAAM
  , GEMEENTENAAM
  , CODPROVINCIE
  , CODCEBUCO
  )
VALUES
  ( p_postcode
  , l_codreeks
  , p_huisnummer
  , p_huisnummer
  , 'A'
  , 'J'
  , p_woonplaats
  , p_straatnaam
  , NULL
  , NULL
  , NULL
  );
END RLE_NPE_TOEV;

 
/