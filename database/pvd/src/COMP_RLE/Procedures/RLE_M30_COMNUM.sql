CREATE OR REPLACE PROCEDURE comp_rle.RLE_M30_COMNUM
 (P_NUMRELATIE IN number
 ,P_CODROL IN varchar2
 ,P_SRTCOM IN varchar2
 ,P_DATUM IN date
 ,P_NUMCOMMUNICATIE IN OUT varchar2
 ,P_O_SRTCOM IN OUT varchar2
 ,P_MELDING IN OUT varchar2
 )
 IS
-- Program Data
L_GEVONDEN VARCHAR2(1) := 'N';
L_DATUM DATE;
-- PL/SQL Specification
/* Ophalen van communicatie nummer op rol, soort en peildatum
   Hardcopied into Package Spec. (HVI 2002-11-08)
*/
CURSOR C_CNR_002
 (B_SELROL IN VARCHAR2
 ,B_SELSRT IN VARCHAR2
 ,B_NUMRELATIE IN number
 ,B_CODROL IN varchar2
 ,B_SRTCOM IN varchar2
 ,B_DATUM IN DATE
 )
IS
/* C_CNR_002 */
select cnr.numcommunicatie
     , cnr.dwe_wrddom_srtcom
  from rle_communicatie_nummers cnr
 where nvl(cnr.rol_codrol,'@') = decode( b_selrol
                                        , 'J', b_codrol
                                        , '@')
   and cnr.dwe_wrddom_srtcom   = decode( b_selsrt
                                        , 'J', b_srtcom
                                        , cnr.dwe_wrddom_srtcom)
   and b_datum between cnr.datingang
                and     nvl(dateinde, b_datum)
   and cnr.rle_numrelatie      = b_numrelatie
order by cnr.datingang desc
;
r_cnr_002 c_cnr_002%ROWTYPE;
-- PL/SQL Block
BEGIN
/* RLE_M30_COMNUM */
l_gevonden        := 'N';
p_numcommunicatie := NULL;
p_o_srtcom        := NULL;
p_melding         := NULL;
l_datum           := NVL(p_datum, SYSDATE);
IF p_codrol IS NOT NULL
   AND
   p_srtcom IS NOT NULL
THEN  --  tak 1:  rol en soort gevuld
  OPEN c_cnr_002( 'J'
                , 'J'
                , p_numrelatie
                , p_codrol
                , p_srtcom
                , l_datum);
  FETCH c_cnr_002 INTO r_cnr_002;
  IF c_cnr_002%FOUND
  THEN
    p_numcommunicatie := r_cnr_002.numcommunicatie;
    p_o_srtcom        := r_cnr_002.dwe_wrddom_srtcom;
    l_gevonden        := 'J';
  END IF;
ELSIF p_codrol IS NOT NULL
      AND
      p_srtcom IS NULL
THEN --  tak 2:  rol gevuld en soort niet gevuld
  OPEN c_cnr_002( 'J'
                , 'N'
                , p_numrelatie
                , p_codrol
                , p_srtcom
                , l_datum);
  FETCH c_cnr_002 INTO r_cnr_002;
  IF c_cnr_002%FOUND
  THEN
    p_numcommunicatie := r_cnr_002.numcommunicatie;
    p_o_srtcom        := r_cnr_002.dwe_wrddom_srtcom;
    l_gevonden        := 'J';
  END IF;
ELSIF p_codrol IS NULL
      AND
      p_srtcom IS NOT NULL
THEN  --  tak 3:  rol niet gevuld en soort gevuld
  OPEN c_cnr_002( 'N'
                , 'J'
                , p_numrelatie
                , p_codrol
                , p_srtcom
                , l_datum);
  FETCH c_cnr_002 INTO r_cnr_002;
  IF c_cnr_002%FOUND
  THEN
    p_numcommunicatie := r_cnr_002.numcommunicatie;
    p_o_srtcom        := r_cnr_002.dwe_wrddom_srtcom;
    l_gevonden        := 'J';
  END IF;
ELSE
  OPEN c_cnr_002( 'N'
                , 'N'
                , p_numrelatie
                , p_codrol
                , p_srtcom
                , l_datum);
  FETCH c_cnr_002 INTO r_cnr_002;
  IF c_cnr_002%FOUND
  THEN
    p_numcommunicatie := r_cnr_002.numcommunicatie;
    p_o_srtcom        := r_cnr_002.dwe_wrddom_srtcom;
    l_gevonden        := 'J';
  END IF;
END IF;
IF c_cnr_002%ISOPEN
THEN
  CLOSE c_cnr_002;
END IF;
/* Het volgende is NIET gewenst!
   (De situatie hieronder is als ELSE-tak in de afvraging hierboven opgenomen)
   Dus is onderstaande uitgesterd (HVI 2002-11-12):
*/
/*
   Indien "Niet gevonden" (l_gevonden = 'N') dan is er nog een mogelijkheid
   om te zoeken met geen selectie op rol EN geen selectie op comm.soort.
   Dit had wel als laatste ELSE in de vorige afvraging kunnen staan, maar
   dan was niet meegenomen dat de ander afvragingen NIETS op leverden en er
   toch een comm.nr. moet worden opgehaald indien er - van welke soort dan
   ook -  een comm.nr. aanwezig is.
IF l_gevonden = 'N'
THEN  --  tak 4:  rol en soort niet gevuld of vorige speurtocht leverde niets op
  OPEN c_cnr_002( 'N'
                , 'N'
                , p_numrelatie
                , p_codrol
                , p_srtcom
                , l_datum);
  FETCH c_cnr_002 INTO r_cnr_002;
  IF c_cnr_002%FOUND
  THEN
    p_numcommunicatie := r_cnr_002.numcommunicatie;
    p_o_srtcom        := r_cnr_002.dwe_wrddom_srtcom;
    l_gevonden        := 'J';
  END IF;
END IF;
IF c_cnr_002%ISOPEN
THEN
  CLOSE c_cnr_002;
END IF;
*/
--
-- Niets gevonden, dan is er echt geen comm.nr. aanwezig:
IF	l_gevonden = 'N' THEN
	p_melding := 'Geen communicatie nummer gevonden';
END IF;
END RLE_M30_COMNUM;

 
/