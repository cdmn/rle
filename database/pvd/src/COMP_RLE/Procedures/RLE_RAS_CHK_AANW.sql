CREATE OR REPLACE PROCEDURE comp_rle.RLE_RAS_CHK_AANW
 (P_RAS IN rle_relatie_adressen%rowtype
 )
 IS
-- Program Data
L_PLAATS NUMBER(2);
L_AKTIE NUMBER(1);
-- PL/SQL Block
BEGIN
/* RLE_RAS_CHK_AANW */
  l_plaats := 1;
  l_aktie	:= rle_ras_chk_aanwezig(p_ras);
  if	l_aktie = 2 then
  	l_plaats := 6;
  	stm_dbproc.raise_error('RLE-00392',
  	'#1'||p_ras.rle_numrelatie||
  	'#2'||p_ras.dwe_wrddom_srtadr,
          'rle_ras_chk_aanw'
  	);
  else
  	l_plaats := 7;
  	if	l_aktie = 3
  	and	inserting then
  		l_plaats := 8;
  		begin
  		  update	rle_relatie_adressen ras
    		  set	ras.dateinde = p_ras.datingang
  		  where	ras.rle_numrelatie  = p_ras.rle_numrelatie
  		  and	ras.dwe_wrddom_srtadr = p_ras.dwe_wrddom_srtadr
  		  and	nvl(ras.rol_codrol,'@') = nvl(p_ras.rol_codrol,'@')
  		  and	ras.dateinde is NULL
  		  and	ras.datingang <= p_ras.datingang
  		  and	ras.id <> p_ras.id;
  		EXCEPTION
  		when NO_DATA_FOUND then
  			NULL;
  		end;
  		l_plaats := 9;
  	end if;
  end if;
  l_plaats := 10;
END RLE_RAS_CHK_AANW;

 
/