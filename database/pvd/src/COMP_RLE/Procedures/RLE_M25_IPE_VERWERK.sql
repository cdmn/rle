CREATE OR REPLACE PROCEDURE comp_rle.RLE_M25_IPE_VERWERK
 IS
BEGIN
DECLARE
           l_rprt_regnum      NUMBER;
           l_sw_woonpl        VARCHAR2(20);
           l_sw_straat        VARCHAR2(20);
           l_straatnaam       rle_ned_postcode.straatnaam%TYPE;
           l_woonplaats       rle_ned_postcode.woonplaats%TYPE;
           l_verw             NUMBER;
  /* ************ HOOFDROUTINE **************** */
  BEGIN
  -- aanpassen rapport-id van voorloop en sluitrecord postcodetape
  -- (door data-loader weggeschreven)
  stm_batch.init_batch('RLE_M25_IPE_VERWERK');
  -- ophalen schrijfwijze woonplaats en straatnaam
  l_sw_woonpl := stm_algm_get.sysparm(NULL,'SWPL',SYSDATE);
  l_sw_straat := stm_algm_get.sysparm(NULL,'SSTR',SYSDATE);
  -- Verwijderen van nederlandse postcodes
  stm_batch.log_regel('RLE_M25_IPE_VERWERK'
        ,'VOORTGANG'
        ,'Start verwijderen alle nederlandse postcodes');
  DELETE FROM rle_ned_postcode;
  COMMIT;
  stm_batch.log_regel('RLE_M25_IPE_VERWERK'
        ,'VOORTGANG'
        ,'Einde verwijderen alle nederlandse postcodes');
  -- verwerking gegevens
  l_verw     :=0;
     INSERT  INTO rle_ned_postcode
          (       postcode
          ,       codreeks
          ,       numscheidingvan
          ,       numscheidingtm
          ,       woonplaats
          ,       straatnaam
          ,       gemeentenaam
          ,       codprovincie
          ,       codcebuco
          ,       indpostchand
          ,       codorganisatie)
     select rip.POSTCODE
          , rip.REEKSCOD
          , rip.NUMSCHEIDINGVAN
          , rip.NUMSCHEIDINGTM
          , DECODE ( l_sw_woonpl
                   , 'PTT' , rip.WOONPLAATS_PTT
                           , rip.WOONPLAATS_NEN)
          , decode ( l_sw_straat
                   , 'PTT' , rip.STRAATNAAM_PTT
                   , 'NEN' , rip.STRAATNAAM_NEN
                           , STRAATNAAM_OFF)
          , rip.GEMEENTENAAM
          , rip.CODPROV
          , rip.CODCEBUCO
          , 'N'
          , 'A'
       from rle_init_postcode rip
      where
        NOT (NVL (SUBSTR(rip.postcode,5,2),'  ') = '  '
             OR (rip.reekscod NOT IN (2,3)
             AND     NVL(rip.numscheidingvan,0) = '0'
             AND     NVL(rip.numscheidingtm ,0) = '0')) ;
  l_verw := SQL%ROWCOUNT;
  dbms_output.put_line ('verwerkt : ' || l_verw );
  commit;
  stm_batch.log_regel('RLE_M25_IPE_VERWER','MELDING'
        ,'Aantal postkodes verwerkt     : '||TO_CHAR(l_verw)
  );
  stm_batch.einde_batch('J');
  EXCEPTION
  WHEN OTHERS THEN
        ROLLBACK;
        stm_batch.log_fout('RLE_M25_IPE_VERWERK');
        stm_batch.einde_batch('N');
  END;
END RLE_M25_IPE_VERWERK;

 
/