CREATE OR REPLACE PROCEDURE comp_rle.RLE_BEREIKEN_PENSIOEN
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN VARCHAR2
 ,P_BEDRIJFSSECTOR IN VARCHAR2 := 'MN'
 ,P_PEILDATUM IN DATE
 )
 IS
/*
Datum       Wie        Wat
--------    ---------- -----------------------------------
13-04-2010  UMS	       wsf_sec.get_sec uit cursor where clause gehaald ivm inc 175301
20-03-2012  XCW        Sector gezet en ibm.publish_event verwijderd.
02-04-2012  XCW        Laatste dag van maand ook meenemen.
28-11-2012  XCW        max_leeftijd berekenen vanuit geboortedatum
01-07-2013  JKU        Aanpassinger ivm PA doorgevoerd , gebruik was al goed , de oude parameter werd nog wel opgehaald
19-09-2013  JKU        Naam van de op te halen parameter is gewijzigd van PENSIOENLFMND naar AVG_MAX_LF_MND
*/

  cn_module              CONSTANT varchar2(30)   := 'RLE_BEREIKEN_PENSIOEN';
  cn_lengte_sqlerrm      CONSTANT number(3)      := 255;
  cn_lengte_tekst        CONSTANT number(3)      := 255;  -- max lengte bij debug = 255, lijsten => 500 (HERO!)
  cn_commitaantal        CONSTANT number(5)      := 500;

  TYPE tellingen_rectype IS RECORD
       ( aantal_aangeboden    number(12)
       , aantal_ok        number(12)
       , aantal_fout      number(12)
       );
  --
  r_tellingen       tellingen_rectype;
  --
  CURSOR c_opn ( b_peildatum DATE
               , b_bedrijfssector VARCHAR2
               )
  IS
   SELECT * from (
        SELECT   av2.numrelatie
                ,av2.persoonsnummer
                ,av2.datgeboorte
                ,av2.namrelatie
                ,av2.max_leeftijd_prs
                ,add_months (av2.datgeboorte, av2.max_leeftijd_prs ) max_leeftijd
                ,av2.min_avg_id
                ,av2.avg_id
        FROM   (SELECT prs.numrelatie
                     , prs.persoonsnummer
                     , prs.datgeboorte
                     , prs.namrelatie
                     , prs.geboortedatum
                     , wsf_cpw.get_waarde('AVG_MAX_LF_MND', b_bedrijfssector, prs.datgeboorte) max_leeftijd_prs
                     , MIN (avh.ID) OVER (PARTITION BY avh.werknemer) min_avg_id
                     , avh.ID avg_id
                FROM   rle_mv_personen prs
                     , avg_arbeidsverhoudingen avh
                     , wgr_werkgevers wgr
                     , wgr_bedrijfstakken btk
                     , bdt bdt
                WHERE  avh.werknemer = prs.persoonsnummer
                AND    prs.datum_overlijden IS NULL
                AND    prs.datgeboorte IS NOT NULL
                AND    avh.dat_einde IS NULL
                AND    bdt.kodebds = b_bedrijfssector
                AND    avh.werkgever = wgr.nr_werkgever
                AND    wgr.ID = btk.wgr_id
                AND    btk.code_hoofdgr_bedrtak = bdt.kodebdt
                AND    (   NVL (avh.dat_einde, TO_DATE ('01-01-4712', 'dd-mm-yyyy'))
                              BETWEEN TRUNC (btk.dat_begin)
                                  AND NVL (btk.dat_einde, TO_DATE ('01-01-4712', 'dd-mm-yyyy'))
                        OR avh.dat_begin BETWEEN TRUNC (btk.dat_begin)
                                             AND NVL (btk.dat_einde, avh.dat_begin)
                       )) av2
        WHERE  av2.avg_id = av2.min_avg_id )
   WHERE  max_leeftijd < last_day(b_peildatum) + 1;
   --
   r_opn c_opn%ROWTYPE;
   -- tbv lijsten:
   t_tekst                    VARCHAR2(255)  := NULL;
   t_fout_tekst               VARCHAR2(255)  := NULL;
   l_fout_tekst2              VARCHAR2(255)  := NULL;
   t_sqlerrm                  VARCHAR2(255)  := NULL;
   t_locatie                  VARCHAR2(4)    := '0000';
   t_volgnummer               NUMBER(8)      := 0;
   t_lijstnummer              NUMBER(3)      := 1;
   t_regelnummer              NUMBER(7)      := 0;
   t_foutlijst_nr             NUMBER(3)      := 2;
   t_foutlijst_regelnr        NUMBER(7)      := 0;
   -- variabelen tbv herstart.
   t_peildatum                DATE           := NULL;
   -- variablen tbv wrapper/diversen.
   t_reden_einde VARCHAR2(20) := 'PENSIONERING';
   --
   foutieve_verwerking_AVH EXCEPTION;
   PRAGMA EXCEPTION_INIT(foutieve_verwerking_AVH, -20000);
   --
BEGIN
  stm_util.t_script_naam      := p_script_naam;
  stm_util.t_script_id        := TO_NUMBER(p_script_id);
  stm_util.t_programma_naam   := cn_module;
  stm_util.t_procedure        := cn_module;
  t_locatie        := '0010';
  r_tellingen.aantal_aangeboden  := 0;
  r_tellingen.aantal_OK      := 0;
  r_tellingen.aantal_fout    := 0;
  --
  basis_ctx.set_sector(p_bedrijfssector);    -- xcw
  --
  -- JWB initialisatie toevoegen
  /* kopregel van het logverslag printen */
  t_tekst := SUBSTR( 'Logverslag van Module '||cn_module
                   ||'. Gestart op '
                   ||TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS')
                   , 1, cn_lengte_tekst);
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , t_tekst
                         );
  stm_util.debug( t_tekst
                );
  /* inforegel iz meegegeven parameters */
  t_tekst := SUBSTR( cn_module
                   ||TO_CHAR(trunc(add_months(sysdate, 1))
                            ,'DD-MM-YYYY')
                   ||', Reden Einde='||NVL(t_reden_einde, 'Niet opgegeven')
                   , 1, cn_lengte_tekst);
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , t_tekst
                         );
  stm_util.debug( t_tekst
                );
  -- Foutlijst kopregels
  t_fout_tekst := 'Foutmeldingen:';
  stm_util.insert_lijsten( t_foutlijst_nr
                         , t_foutlijst_regelnr
                         , t_fout_tekst
                         );
  t_fout_tekst := SUBSTR( RPAD('Relnr'       ,  9)
                        ||RPAD('Prsnr'       , 12)
                        ||RPAD('Geb_datum'   , 11)
                        ||RPAD('Naam'        , 20)
                        ||'Melding'
                        , 1, cn_lengte_tekst);
  stm_util.insert_lijsten( t_foutlijst_nr
                         , t_foutlijst_regelnr
                         , t_fout_tekst
                         );
  -- Bepalen peildatum
  t_peildatum := trunc(nvl(p_peildatum,add_months(sysdate, 1)));

  t_tekst := SUBSTR( cn_module||' - '||t_locatie
                   ||': Loop start met datum '
                   ||TO_CHAR(t_peildatum, 'DD-MM-YYYY HH24:MI:SS')
                   ||' en check op leeftijd is afhankelijk van leeftijd (WSF parameter = AVG_MAX_LF_MND) '
                   , 1, cn_lengte_tekst);
  stm_util.debug( t_tekst
                );
  FOR r_opn in c_opn ( t_peildatum
                     , p_bedrijfssector)
  LOOP
    t_locatie        := 'OPN1';
    BEGIN
        t_locatie        := 'OPN2';
        stm_util.t_tekst := NULL;
        t_tekst := SUBSTR( cn_module||' - '||t_locatie
                         ||': De werknemer '||r_opn.persoonsnummer
                         ||' bereikt de max leeftijd ('||to_char(r_opn.max_leeftijd_prs)||' mnd) op '
                         ||to_char(r_opn.max_leeftijd, 'DD-MM-YYYY')
                         , 1, cn_lengte_tekst);
        stm_util.debug( t_tekst );
        -- aanroep wrapper.
        r_tellingen.aantal_aangeboden := r_tellingen.aantal_aangeboden + 1;
        --
        avg_beeindig_avh_werknemer (p_werknemer       => r_opn.persoonsnummer
                                   ,p_reden_einde     => t_reden_einde
                                   ,p_opgegeven_datum => r_opn.max_leeftijd);

        r_tellingen.aantal_ok := r_tellingen.aantal_ok + 1;

        COMMIT;
    EXCEPTION
      WHEN foutieve_verwerking_AVH
      THEN
        t_sqlerrm := SUBSTR( SQLERRM
                           , 1, cn_lengte_sqlerrm);
        r_tellingen.aantal_fout := r_tellingen.aantal_fout + 1;
        t_fout_tekst := SUBSTR( RPAD(to_char(NVL(r_opn.numrelatie, 0)),  9)
                              ||RPAD(NVL(r_opn.persoonsnummer, '0')   , 12)
                              ||RPAD(NVL( to_char( r_opn.datgeboorte
                                                 , 'DD-MM-YYYY')
                                        , 'NULL')                     , 11)
                              ||RPAD(NVL(r_opn.namrelatie, 'geen')    , 20)
                              ||' : '||t_sqlerrm
                              , 1, cn_lengte_tekst
                              );
        stm_util.insert_lijsten( t_foutlijst_nr
                               , t_foutlijst_regelnr
                               , t_fout_tekst
                               );
        stm_util.debug(t_fout_tekst);
        t_fout_tekst := SUBSTR( t_sqlerrm, cn_lengte_tekst - 56);  -- 56 = aantal posities vd details
        IF t_fout_tekst IS NOT NULL
        THEN
          t_fout_tekst := SUBSTR( RPAD('', 55)||t_fout_tekst
                                , 1, cn_lengte_tekst);
          stm_util.debug(t_fout_tekst);
          stm_util.insert_lijsten( t_foutlijst_nr
                                 , t_foutlijst_regelnr
                                 , t_fout_tekst
                                 );
        END IF;
        IF stm_util.t_foutmelding IS NOT NULL
        THEN
          t_fout_tekst := SUBSTR( stm_util.t_foutmelding
                                , 1, cn_lengte_tekst);
          stm_util.debug(t_fout_tekst);
          stm_util.insert_lijsten( t_foutlijst_nr
                                 , t_foutlijst_regelnr
                                 , t_fout_tekst
                                 );
        END IF;

        --- xcw 16-07-2010 betere foutmelding
        l_fout_tekst2 := trim(t_sqlerrm);
        if l_fout_tekst2 like 'ORA-20000: AVG-00049 #%' then
          l_fout_tekst2 := substr(l_fout_tekst2,24);
        end if;
        if upper(l_fout_tekst2) like 'ORA-20000%' then
          l_fout_tekst2 := substr(l_fout_tekst2,12);
          if instr(l_fout_tekst2,'ORA-') > 0 then
            l_fout_tekst2 := substr(l_fout_tekst2,1, instr(l_fout_tekst2,'ORA-') -1);
          end if;
        end if;
        l_fout_tekst2 := replace(l_fout_tekst2,' ', '');
        l_fout_tekst2 := STM_GET_BATCH_MESSAGE(l_fout_tekst2);
        IF l_fout_tekst2 IS NOT NULL
        THEN
          stm_util.debug(l_fout_tekst2);
          stm_util.insert_lijsten( t_foutlijst_nr
                                 , t_foutlijst_regelnr
                                 , l_fout_tekst2
                                 );
        END IF;
        -- einde betere foutmelding


    END;  -- van BEGIN + EXCEPTION subdeel


  END LOOP;
  -- Laatste regels logverslag:
  t_tekst := SUBSTR( RPAD('Aantal ter verwerking aangeboden', 35)||'= '
                   ||TO_CHAR(NVL(r_tellingen.aantal_aangeboden, 0))
                   , 1, cn_lengte_tekst);
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , t_tekst
                         );
  stm_util.debug( t_tekst
                );
  t_tekst := SUBSTR( RPAD('Aantal goed verwerkt', 35)||'= '
                   ||TO_CHAR(NVL(r_tellingen.aantal_OK, 0))
                   , 1, cn_lengte_tekst);
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , t_tekst
                         );
  stm_util.debug( t_tekst
                );
  t_tekst := SUBSTR( RPAD('Aantal fout', 35)||'= '
                   ||to_char(NVL(r_tellingen.aantal_fout, 0))
                   , 1, cn_lengte_tekst);
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , t_tekst
                         );
  stm_util.debug( t_tekst
                );
  t_tekst := SUBSTR( 'Einde logverslag van Module '||cn_module
                   ||'. Geeindigd op '
                   ||TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS')
                   , 1, cn_lengte_tekst);
  stm_util.insert_lijsten( t_lijstnummer
                         , t_regelnummer
                         , t_tekst
                         );
  stm_util.debug( t_tekst
                );
  stm_util.insert_job_statussen ( t_volgnummer, 'S', '0' );
  commit;
EXCEPTION
  WHEN OTHERS
  THEN
    t_sqlerrm := SUBSTR( SQLERRM
                       , 1, cn_lengte_sqlerrm);
    --
    ROLLBACK;
    --
    stm_util.insert_job_statussen ( t_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    --
    t_tekst := SUBSTR( 'MODULE '||cn_module
                     ||' Is onjuist gestopt op '
                     ||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS')
                     ||'. Parameters: Peildatum='
                     ||TO_CHAR(trunc(t_peildatum)
                              ,'DD-MM-YYYY')
                     ||',RedenEinde='||NVL(t_reden_einde, 'Niet opgegeven')
                     , 1, cn_lengte_tekst);
    --
    stm_util.insert_job_statussen ( t_volgnummer
                                  , 'I'
                                  , t_tekst
                                  );
    --
    stm_util.insert_job_statussen ( t_volgnummer
                                  , 'I'
                                  , t_sqlerrm
                                  );
    --
    IF stm_util.t_foutmelding IS NOT NULL
    THEN
      t_fout_tekst := SUBSTR( stm_util.t_foutmelding
                            , 1, cn_lengte_tekst);
      stm_util.debug(t_fout_tekst);
      stm_util.insert_job_statussen ( t_volgnummer
                                    , 'I'
                                    , t_fout_tekst
                                    );
    END IF;
    --
    COMMIT;
END RLE_BEREIKEN_PENSIOEN;
 
/