CREATE OR REPLACE procedure comp_rle.rle_aq_mut_col_contract
is
  l_consumer_name varchar2 ( 27 ) := 'RLE_MUT_COL_CONTRACT';
  l_message_handle raw ( 16 );
  l_event_name varchar2 ( 23 );
  l_params varchar2 ( 4000 );
  l_params_save varchar2 ( 4000 );
  l_succes boolean;
  l_errorcode varchar2 ( 2000 );
  l_looping boolean := true;
  l_publisher varchar2 ( 100 );
  l_wgr_werkgnr varchar2 ( 100 );
  l_adm_code varchar2 ( 100 );
  l_code_functiecategorie varchar2 ( 100 );
  l_dat_begin varchar2 ( 100 );
  l_dat_einde varchar2 ( 100 );
  l_otherinfo varchar2( 250 )
    := 'Advanced Queue listener comp_rle.rle_aq_mut_col_contract is gestopt. Deze moet opnieuw worden gestart zodra het fout gegane bericht verwerkt kan worden.';
begin
  while l_looping
  loop
    -- Lezen bericht voor abonnementhouder ret_aq_overlijden_relatie
    -- Indien geen bericht klaar staat, en ook geen stop_queue bericht
    -- dan wordt gewacht op het eerstvolgende bericht.
    stm_aq.dequeue( p_consumer_name  => l_consumer_name
                  , p_dequeue_mode   => 'REMOVE'
                  , p_navigation     => 'FIRST_MESSAGE'
                  , p_message_handle => l_message_handle
                  , p_event_name     => l_event_name
                  , p_params         => l_params
                  , p_succes         => l_succes
                  , p_errorcode      => l_errorcode
                  );
    if l_succes
    then
      l_params_save := l_params;

      -- Lees de parameters en roep de verwerkende module aan.
      l_publisher := stm_get_word ( l_params );
      l_wgr_werkgnr := stm_get_word ( l_params );
      l_dat_begin := stm_get_word ( l_params );
      l_dat_einde := stm_get_word ( l_params );
      l_adm_code := stm_get_word ( l_params );
      l_code_functiecategorie := stm_get_word ( l_params );
      rle_mut_col_contract(p_wgr_werkgnr            => l_wgr_werkgnr
                          , p_adm_code              => l_adm_code
                          , p_code_functiecategorie => l_code_functiecategorie
                          , p_dat_begin             => to_date(l_dat_begin, 'dd-mm-yyyy hh24:mi:ss')
                          , p_dat_einde             => to_date(l_dat_einde, 'dd-mm-yyyy hh24:mi:ss')
                          , p_succes                => l_succes
			  , p_errorcode             => l_errorcode
                          );
    end if;
    --
    if l_succes
    then
      commit;
    else
      -- Verwerking fout gegaan OF lezen queue fout gegaan.
      -- Doe een rollback en roep de standaard error handler aan
      -- voor achtergrondprocessen.
      -- Na een fout moet de listener altijd worden beeindigd!!
      rollback;
      stm_bgproc.error_handler ( p_type_verwerking                    => stm_bgproc.c_advanced_queuing
                               , p_procedure                          => 'RLE_AQ_MUT_COL_CONTRACT'
                               , p_errormsg                           => l_errorcode
                               , p_params                             => l_params_save
                               , p_otherinfo                          => l_otherinfo
                               , p_raise_exception                    => false
                               );
      l_looping := false;
    end if;
  end loop;
exception
  --
  -- Stop alleen een exception handler in de listener en niet in de verwerkende procedure(s).
  -- In geval van een onverwachte fout zal stm_bgproc.error_handler de backtrace afdrukken
  -- en die ben je kwijt na een doorlopen exception.
  -- Dat betekent verlies van nuttige informatie over de echte fout.
  when others
  then
    rollback;
    stm_bgproc.error_handler ( p_type_verwerking                      => stm_bgproc.c_advanced_queuing
                             , p_procedure                            => 'RLE_AQ_MUT_COL_CONTRACT'
                             , p_errormsg                             => sqlerrm
                             , p_params                               => l_params_save
                             , p_otherinfo                            => l_otherinfo
                             , p_raise_exception                      => false
                             );
end rle_aq_mut_col_contract;
 
/