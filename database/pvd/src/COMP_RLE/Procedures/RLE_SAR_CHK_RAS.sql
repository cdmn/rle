CREATE OR REPLACE PROCEDURE comp_rle.RLE_SAR_CHK_RAS
 (P_CODROL rle_sadres_rollen.codrol%TYPE
 ,P_SADRES VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_RAS_009
 (B_SRTADRES IN VARCHAR2
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_RAS_009 */
select	*
from	rle_relatie_adressen ras
where	ras.dwe_wrddom_srtadr  = b_srtadres
and	nvl(ras.rol_codrol,'@') = nvl(b_codrol,'@');
-- Program Data
R_RAS_009 C_RAS_009%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_SAR_CHK_RAS */
for	r_ras_009 in c_ras_009(p_sadres,p_codrol) loop
	stm_dbproc.raise_error('RLE-000427',
	'#1 '||p_codrol,'rle_sar_chk_ras');
end loop;
END RLE_SAR_CHK_RAS;

 
/