CREATE OR REPLACE PROCEDURE comp_rle.RLE_RLE_DEL_CHILDS
 (P_NUMRELATIE rle_relaties.numrelatie%TYPE
 )
 IS
BEGIN
/* RLE_RLE_DEL_CHILDS */
/*
delete
FROM   rle_dubbelen
WHERE  rle_numrelatie = p_numrelatie;
--
DELETE
FROM   rle_dubbelen
WHERE  rle_numrelatie_dub = p_numrelatie;
*/
--
DELETE
FROM   rle_relatie_externe_codes
WHERE  rle_numrelatie = p_numrelatie;
--
DELETE
FROM   rle_relatie_rollen
WHERE  rle_numrelatie = p_numrelatie;
--
DELETE
FROM   rle_relatie_adressen
WHERE  rle_numrelatie = p_numrelatie;
--
DELETE
FROM   rle_communicatie_nummers
WHERE  rle_numrelatie = p_numrelatie;
--
DELETE
FROM   rle_financieel_nummers
WHERE  rle_numrelatie = p_numrelatie;
--
/*
DELETE
FROM   rle_dubbele_gegevens
WHERE  rle_numrelatie = p_numrelatie;
*/
END RLE_RLE_DEL_CHILDS;

 
/