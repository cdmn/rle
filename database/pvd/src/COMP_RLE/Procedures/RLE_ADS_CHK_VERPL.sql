CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADS_CHK_VERPL
 (P_NEW_ADS_STT_STRAATID IN NUMBER
 ,P_NEW_ADS_POSTCODE IN VARCHAR2
 ,P_NEW_ADS_HUISNUMMER IN NUMBER
 ,P_NEW_ADS_WPS_WOONPLAATSID IN VARCHAR2
 ,P_NEW_ADS_LND_CODLAND IN OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_LND_002
 (B_CODLAND IN VARCHAR2
 )
 IS
/* C_LND_002 */
select	*
from	rle_landen lnd
where	lnd.codland = b_codland;
-- Program Data
L_DUMMY NUMBER(1, 0) := 0;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ADS_CHK_VERPL */
/*  controles op verplichte velden in  tabel rle_adressen  */
/*  aan de hand van indicaties in de tabel rle_landen      */
for r_lnd_002 in c_lnd_002(p_new_ads_lnd_codland) loop
	l_dummy := 1;
	if	r_lnd_002.indstraatverplicht = 'J'
	and	p_new_ads_stt_straatid is NULL then
		stm_dbproc.raise_error('RLE-00401',
		'#1'||'straat','rle_ads_chk_verpl');
	end if;
	if	r_lnd_002.indpostcodeverplicht = 'J'
	and	p_new_ads_postcode is NULL then
		stm_dbproc.raise_error('RLE-00401',
		'#1'||'postcode ','rle_ads_chk_verpl');
	end if;
	if	r_lnd_002.indnummerverplicht = 'J'
	and	p_new_ads_huisnummer IS NULL then
		stm_dbproc.raise_error('RLE-00401',
		'#1'||'huisnummer','rle_ads_chk_verpl');
	end if;
	if	r_lnd_002.indwnplverplicht = 'J'
	and	p_new_ads_wps_woonplaatsid IS NULL then
		stm_dbproc.raise_error('RLE-00401',
		'#1'||'woonplaats','rle_ads_chk_verpl');
	end if;
end loop;
if	l_dummy = 0 then
	 stm_dbproc.raise_error('RLE-00440',
	 '#1 '||p_new_ads_lnd_codland,
	 'rle_ads_chk_verpl');
end if;
END RLE_ADS_CHK_VERPL;

 
/