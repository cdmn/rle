CREATE OR REPLACE PROCEDURE comp_rle.RLE_RKN_BEEIND_WG_AK
 (PIV_NUM_WGR IN RLE_RELATIE_EXTERNE_CODES.EXTERN_RELATIE%TYPE
 ,PIV_NUM_AK IN RLE_RELATIE_EXTERNE_CODES.EXTERN_RELATIE%TYPE
 ,PID_EINDDATUM IN DATE
 ,POV_IND_VERWERKT OUT VARCHAR2
 ,POV_MELDING OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-externe_codes op rowid */
CURSOR C_REC_005
 (B_RELEXT IN VARCHAR2
 ,B_SYSTEEMCOMP IN VARCHAR2
 ,B_CODROL IN VARCHAR2
 ,B_DATPARM IN DATE
 )
 IS
/* C_REC_005 */
select	rec.rle_numrelatie
from   	rle_relatie_externe_codes rec
where  	rec.extern_relatie       = b_relext
and    	(b_systeemcomp          is null
   or    rec.dwe_wrddom_extsys   = b_systeemcomp)
and     (b_codrol               is null
   or    rec.rol_codrol          = b_codrol)
and    	trunc(rec.datingang)    <= trunc(nvl(b_datparm,sysdate))
and    	(trunc(rec.dateinde)    >= trunc(nvl(b_datparm,sysdate))
      	or rec.dateinde         is null)
order by nvl(rec.rol_codrol, 'zzz');
CURSOR C_RKN_008
 (CPN_NUMRELATIE_1 IN RLE_RELATIES.NUMRELATIE%TYPE
 ,CPN_NUMRELATIE_2 IN RLE_RELATIES.NUMRELATIE%TYPE
 ,CPD_PEILDATUM IN DATE
 )
 IS
SELECT rkn.id
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkn.rkg_id = rkg.id
AND    (   (   rkn.rle_numrelatie         = cpn_numrelatie_1
           AND rkn.rle_numrelatie_voor    = cpn_numrelatie_2
           )
       OR  (   rkn.rle_numrelatie         = cpn_numrelatie_2
           AND rkn.rle_numrelatie_voor    = cpn_numrelatie_1
           )
       )
AND    cpd_peildatum BETWEEN rkn.dat_begin AND nvl( rkn.dat_einde, cpd_peildatum );
-- Program Data
L_NUMREL_AKR RLE_RELATIES.NUMRELATIE%TYPE;
L_RKN_ID RLE_RELATIE_KOPPELINGEN.ID%TYPE;
L_NUMREL_WGR RLE_RELATIES.NUMRELATIE%TYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
BEGIN
   -- eerst ophalen relatienummer wgr en ak;
   -- vervolgens kijken of er met deze combinatie een lopend record
   -- gevonden kan worden in relatie_koppelingen. Is dat er, dan
   -- deze beeindigen per <einddatum>.
   pov_ind_verwerkt := 'N';
   OPEN C_REC_005( piv_num_wgr
                 , 'WGR'
                 , NULL  -- p_codrol
                 , NULL  -- p_datparm
                 );
   FETCH c_rec_005 INTO l_numrel_wgr;
   CLOSE c_rec_005;
   OPEN C_REC_005( piv_num_ak
                 , 'AKR'
                 , NULL  -- p_codrol
                 , NULL  -- p_datparm
                 );
   FETCH c_rec_005 INTO l_numrel_akr;
   CLOSE c_rec_005;
   -- indien beide numrel aanwezig, dan kijken of er een koppeling is.
   IF    l_numrel_akr IS NOT NULL
     AND l_numrel_wgr IS NOT NULL
   THEN
      OPEN C_RKN_008( l_numrel_wgr
                    , l_numrel_akr
                    , pid_einddatum
                    );
      FETCH c_rkn_008 INTO l_rkn_id;
      IF c_rkn_008%FOUND
      THEN
         -- er is een lopende koppeling aanwezig; deze mag beeindigd worden.
         CLOSE c_rkn_008;
         UPDATE rle_relatie_koppelingen  rkn
         SET    rkn.dat_einde = pid_einddatum
         WHERE  rkn.id        = l_rkn_id;
         pov_ind_verwerkt := 'J';
      ELSE
         CLOSE c_rkn_008;
      END IF;
   END IF;
   IF pov_ind_verwerkt = 'N'
   THEN
      pov_melding := 'Beeindiging relatie tussen werkgever en ' ||
                     'administratiekantoor is niet succesvol verlopen';
   END IF;
END ;
END RLE_RKN_BEEIND_WG_AK;

 
/