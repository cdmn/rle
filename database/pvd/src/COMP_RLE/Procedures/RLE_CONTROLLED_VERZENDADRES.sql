CREATE OR REPLACE PROCEDURE comp_rle.RLE_CONTROLLED_VERZENDADRES
 (P_RELATIENUMMER IN number
 ,P_REGELING IN varchar2 := null
 ,P_CODROL IN VARCHAR2 := null
 ,P_POSTSOORT IN VARCHAR2 := null
 ,P_DATUM_GEBEURTENIS IN date := sysdate
 ,P_FOUTMELDING OUT varchar2
 ,P_NAAM2 OUT varchar2
 ,P_NAAM3 OUT varchar2
 ,P_POSTCODE OUT varchar2
 ,P_HUISNUMMER OUT number
 ,P_HUISNR_TOEV OUT varchar2
 ,P_WOONPLAATS OUT varchar2
 ,P_STRAAT OUT varchar2
 ,P_LANDNAAM OUT varchar2
 )
 IS
--
l_dummy varchar2(12);
--
begin
 --
 rle_controlled_verzend_int
 (p_relatienummer => p_relatienummer
 ,p_regeling => p_regeling
 ,p_codrol => p_codrol
 ,p_postsoort => p_postsoort
 ,p_datum_gebeurtenis => p_datum_gebeurtenis
 ,p_foutmelding => p_foutmelding
 ,p_naam2 => p_naam2
 ,p_naam3 => p_naam3
 ,p_postcode => p_postcode
 ,p_huisnummer => p_huisnummer
 ,p_huisnr_toev => p_huisnr_toev
 ,p_woonplaats => p_woonplaats
 ,p_straat => p_straat
 ,p_landnaam => p_landnaam
 ,p_soort_adres => l_dummy
 );
--
END RLE_CONTROLLED_VERZENDADRES;
 
/