CREATE OR REPLACE PROCEDURE comp_rle.RLE_CONVERSIE_ALTER_AKT_SEQ
 IS
-- PL/SQL Specification
   ln_current_number_akt  NUMBER ;
   ln_highest_number_akt  NUMBER ;
   ln_current_number_gem  NUMBER ;
   ln_highest_number_gem  NUMBER ;
-- PL/SQL Block
BEGIN
   select nvl( max( to_number( rec.extern_relatie ) ),  0 ) + 1
   into   ln_highest_number_akt
   from   rle_relatie_externe_codes  rec
   where  rec.dwe_wrddom_extsys = 'AKR'
   ;
   select last_number
   into   ln_current_number_akt
   from   user_sequences
   where  sequence_name = 'RLE_ADMINKANTOORNUMMER_SEQ1'
   ;
   IF ln_current_number_akt != ln_highest_number_akt
   THEN
      execute immediate
      'alter sequence comp_rle.rle_adminkantoornummer_seq1
      increment by ' || to_char( ln_highest_number_akt - ln_current_number_akt )
	  ;
      select rle_adminkantoornummer_seq1.nextval
      into   ln_current_number_akt
	  from   dual
	  ;
      execute immediate
      'alter sequence comp_rle.rle_adminkantoornummer_seq1
	  increment by 1'
	  ;
   END IF ;
   --
   select nvl( max( to_number( rec.extern_relatie ) ),  0 ) + 1
   into   ln_highest_number_gem
   from   rle_relatie_externe_codes  rec
   where  rec.dwe_wrddom_extsys = 'GME'
   ;
   select last_number
   into   ln_current_number_gem
   from   user_sequences
   where  sequence_name = 'GEM_SEQ'
   ;
   IF ln_current_number_gem != ln_highest_number_gem
   THEN
      execute immediate
      'alter sequence comp_rle.gem_seq
      increment by ' || to_char( ln_highest_number_gem - ln_current_number_gem )
	  ;
      select gem_seq.nextval
      into   ln_current_number_gem
	  from   dual
	  ;
      execute immediate
      'alter sequence comp_rle.gem_seq
	  increment by 1'
	  ;
   END IF ;
   --
END ;

 
/