CREATE OR REPLACE PROCEDURE comp_rle.RLE_CONVERSIE_RLE_SEQUENCES
 IS
-- PL/SQL Specification
/* Procedure om rle_sequences aan te passen
 zodat er geen Primary key violation optreedt
 tijdens conversie. Is nodig omdat in verband met performance
  straten, woonplaatsen, en adressen geimporteerd worden
 vanuit proefconversie database*/

ln_current_number_ads  NUMBER ;
ln_highest_number_ads  NUMBER ;
--
ln_current_number_stt  NUMBER ;
ln_highest_number_stt  NUMBER ;
--
ln_current_number_wps  NUMBER ;
ln_highest_number_wps  NUMBER ;
--

-- PL/SQL Block
BEGIN
  /* RLE_WPS_SEQ1 aanpassen */
  --stm_util.debug('max woonplaats');
  select (max(woonplaatsid) + 1)
  into  ln_highest_number_wps
  from rle_woonplaatsen;
  --
  --stm_util.debug('select last values woonplaats');
  select last_number
  into   ln_current_number_wps
  from   all_sequences
  where  sequence_name = 'RLE_WPS_SEQ1';
  --
  --stm_util.debug('sequence wps aanpassen');
  IF ln_current_number_wps != ln_highest_number_wps
  THEN
      --stm_util.debug('voor execute immediate 1');
      execute immediate
          'alter sequence comp_rle.rle_wps_seq1
          increment by '||to_char(ln_highest_number_wps - ln_current_number_wps);
      --
      --stm_util.debug('na execute immediate 1');
      select rle_wps_seq1.nextval
      into   ln_current_number_wps
      from   dual;
      --
      --stm_util.debug('execute immediate 2');
      execute immediate
      'alter sequence comp_rle.rle_wps_seq1
       increment by 1';
       --
   END IF ;
  --
  /* RLE_STT_SEQ1 aanpassen */
   --stm_util.debug('max straat');
  select (max(straatid) +1)
  into  ln_highest_number_stt
  from  rle_straten;
  --
  --stm_util.debug('select last_value straat');
  select last_number
  into   ln_current_number_stt
  from   all_sequences
  where  sequence_name = 'RLE_STT_SEQ1';
   --
  --stm_util.debug('sequence stt aanpassen');
  IF ln_current_number_stt != ln_highest_number_stt
  THEN
      execute immediate
          'alter sequence comp_rle.rle_stt_seq1
          increment by ' || to_char( ln_highest_number_stt - ln_current_number_stt );
      --
      select rle_stt_seq1.nextval
      into   ln_current_number_stt
      from   dual;
      --
      execute immediate
      'alter sequence comp_rle.rle_stt_seq1
          increment by 1';
       --
   END IF ;
  --
  /* RLE_ADS_SEQ1 aanpassen */
   --stm_util.debug('max adres');
  select (max(numadres) + 1)
  into  ln_highest_number_ads
  from rle_adressen;
  --
  --stm_util.debug('last number adres');
  select last_number
  into   ln_current_number_ads
  from   all_sequences
  where  sequence_name = 'RLE_ADS_SEQ1'
  ;
  --
  --stm_util.debug('sequence ads aanpassen');
  IF ln_current_number_ads != ln_highest_number_ads
  THEN
      execute immediate
      'alter sequence comp_rle.rle_ads_seq1
      increment by ' || to_char( ln_highest_number_ads - ln_current_number_ads );
      --
      select rle_ads_seq1.nextval
      into   ln_current_number_ads
      from   dual;
      --
      execute immediate
      'alter sequence comp_rle.rle_ads_seq1
      increment by 1';
      --
   END IF ;
   --
END;

 
/