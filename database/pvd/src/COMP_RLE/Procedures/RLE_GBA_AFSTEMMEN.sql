CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_AFSTEMMEN
 (P_RLE_NUMRELATIE IN rle_relaties.numrelatie%type
 )
 IS
cursor  c_gba_afstemmen( b_rle_numrelatie rle_afstemmingen_gba.rle_numrelatie%type )
is
select  *
from    rle_afstemmingen_gba    asa
where   asa.rle_numrelatie = b_rle_numrelatie
and     asa.afnemers_indicatie <> 'J'
and     asa.code_gba_status = 'OV'
;
cursor c_pad (b_prs_persoonsnummer personen_adr_v.persoonsnummer%type)
is
select 'x'
from personen_adr_v pad
where pad.persoonsnummer = b_prs_persoonsnummer
;
cursor c_laatste_apn (b_rle_persoonsnummer per_aanmeldingen.rle_persoonsnummer%type)
-- als de meest recente rij in PER_AANMELDINGEN status 'AF' heeft, moet deze cursor niks teruggeven
-- het is dan namelijk gewenst een nieuwe rij aan te maken, met status 'AA' (zie gebruik cursor onder)
is
select 'x'
from per_v_aanmeldingen apn
where apn.RLE_PERSOONSNUMMER = b_rle_persoonsnummer
and status <> 'AF'
and not exists
(
select 1
from per_v_aanmeldingen apn2
where apn2.RLE_PERSOONSNUMMER = apn.RLE_PERSOONSNUMMER
and apn2.ID > apn.id
)
;
l_ind_verwerkt       varchar2(1);
l_gba_afstemmen      boolean;
l_prs_in_gba         boolean;
l_prs_in_apn         boolean;
l_dummy              varchar2(1);
l_prs_persoonsnummer rle_relatie_externe_codes.extern_relatie%type;
r_gba_afstemmen      rle_afstemmingen_gba%rowtype;
l_error              varchar2(2000);
c_proc_name constant varchar2(25) := 'RLE_GBA_AFSTEMMEN';
--
begin
   if rle_chk_gba_afst( p_rle_numrelatie => p_rle_numrelatie
                      , p_persoonsnummer => null ) = 'J'
   then
      open c_gba_afstemmen( p_rle_numrelatie );
      fetch c_gba_afstemmen into r_gba_afstemmen;
      l_gba_afstemmen := c_gba_afstemmen%found;
      close c_gba_afstemmen;
      --
      if l_gba_afstemmen
      then
         --Bepalen persoonsnummer
         l_prs_persoonsnummer := rle_m03_rec_extcod( 'PRS'
                                                    , p_rle_numrelatie
                                                    , 'PR'
                                                    , sysdate);
         if l_prs_persoonsnummer is null
   	     then
   	        stm_dbproc.raise_error('RLE-10388 Voor relatie '||p_rle_numrelatie
                                      ||' is persoonsnummer niet te bepalen.');
         end if;
      	 --
         open c_pad (l_prs_persoonsnummer);
         fetch c_pad into l_dummy;
         l_prs_in_gba := c_pad%found;
         close c_pad;
            -- GBA bij user defined exception niet laten klappen
            begin
            -- Initieer GBA afstemming
            if l_prs_in_gba
            then
               if not gba_chk_plaatsing(l_prs_persoonsnummer)
               then
                  -- Persoon bestaat in GBA maar moet nog worden afgestemd
                  gba_stuur_plaatsingbericht( p_persoonsnummer => l_prs_persoonsnummer
                                            , p_anummer        => null
                                            );
               end if;
            elsif not l_prs_in_gba
            then
               --Persoon bestaat niet in GBA en moet worden afgestemd dus wordt aangemeld.
               --06-08-2015 OKR JIRA-MNGBA-531: Alleen een rij aan PER_AANMELDINGEN toevoegen als
               --                               daar niet al een rij staat (die niet is afgekeurd)
               open c_laatste_apn (l_prs_persoonsnummer);
               fetch c_laatste_apn into l_dummy;
               l_prs_in_apn := c_laatste_apn%found;
               close c_laatste_apn;

               if not l_prs_in_apn
               then
                  rle_gba_toevoegen_persoon( p_rle_numrelatie ) ;
               end if;
               --
            end if;
            --
         exception
         when others then
            if sqlcode between -20999 and -20000
            then
               null;
            else
               raise;
            end if;
         end;
         --
      end if;
      --
   end if;
exception
  when others
  then
  l_error := c_proc_name||' '||sqlerrm;
  raise_application_error(-20000, l_error);
end;
/