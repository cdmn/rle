CREATE OR REPLACE PROCEDURE comp_rle.RLE_ROL_CHK_WIJZ
 (P_CODROL rle_rollen.codrol%TYPE
 ,P_ROL_INDWIJZIGBAAR rle_rollen.indwijzigbaar%TYPE
 )
 IS
BEGIN
/* RLE_ROL_CHK_WIJZ */
/* verw. of wijz. van coderol alleen toegestaan als
  indwijzigbaar = 'J'*/
IF	p_rol_indwijzigbaar <> 'J' THEN
	stm_dbproc.raise_error('RL-00419',
	'#1'||p_codrol,'rle_rol_chk_wijz');
END IF;
END RLE_ROL_CHK_WIJZ;

 
/