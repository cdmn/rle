CREATE OR REPLACE PROCEDURE comp_rle.RLE_AQ_RKN_RLE_RIE
 (P_RLE_NUMRELATIE_VAN IN NUMBER
 ,P_RLE_NUMRELATIE_VOOR IN NUMBER
 ,P_CODE_ROL_IN_KOP IN VARCHAR2
 ,P_DATUM_EVENT IN DATE
 )
 IS
BEGIN
declare
   l_rle_numrelatie_dlnmr  rle_relatie_koppelingen.rle_numrelatie%type;
   l_rle_numrelatie_vrwnt  rle_relatie_koppelingen.rle_numrelatie%type;
   l_voor_rie_gevonden     boolean;
   l_van_rie_gevonden      boolean;
begin
   if p_code_rol_in_kop in ('K', 'O')
   then
      --
      -- In geval kind
      --
      if  p_code_rol_in_kop = 'K'
      then
         l_rle_numrelatie_dlnmr := p_rle_numrelatie_voor;
         l_rle_numrelatie_vrwnt := p_rle_numrelatie_van;
      --
      -- In geval ouder
      --
      elsif p_code_rol_in_kop = 'O'
      then
         l_rle_numrelatie_dlnmr := p_rle_numrelatie_van;
         l_rle_numrelatie_vrwnt := p_rle_numrelatie_voor;
      end if;
      --
      ibm.publish_event
     ( 'CRE_VERWANTSCHAP'
     , 'RLE'
     , l_rle_numrelatie_dlnmr
     , l_rle_numrelatie_vrwnt
     , p_datum_event
     );
   --
   end if;
   --
   -- In geval huwelijk, samenlevingscontract of partnerschap
   --
   if p_code_rol_in_kop in ('H', 'S', 'G')
   then
      l_voor_rie_gevonden := rle_chk_adm_rie(p_prs_persoonsnummer      => NULL
                                            ,p_rle_numrelatie          => p_rle_numrelatie_voor
                                            ,p_rol_codrol              => 'PR'
                                            ,p_systeemcomp             => 'PRS'
                                            ,p_adm_type                => 'P'
                                            ,p_ind_afstemmen_gba_tgstn => 'J'
                                             );
      if l_voor_rie_gevonden then
         l_rle_numrelatie_dlnmr := p_rle_numrelatie_voor;
         l_rle_numrelatie_vrwnt := p_rle_numrelatie_van;
         --
         ibm.publish_event
        ( 'CRE_VERWANTSCHAP'
        , 'RLE'
        , l_rle_numrelatie_dlnmr
        , l_rle_numrelatie_vrwnt
        , p_datum_event
        );
      end if;
       --
      l_van_rie_gevonden := rle_chk_adm_rie(p_prs_persoonsnummer      => NULL
                                           ,p_rle_numrelatie          => p_rle_numrelatie_van
                                           ,p_rol_codrol              => 'PR'
                                           ,p_systeemcomp             => 'PRS'
                                           ,p_adm_type                => 'P'
                                           ,p_ind_afstemmen_gba_tgstn => 'J'
                                            );
      --
      if l_van_rie_gevonden then
         l_rle_numrelatie_dlnmr := p_rle_numrelatie_van;
         l_rle_numrelatie_vrwnt := p_rle_numrelatie_voor;
         --
         ibm.publish_event
        ( 'CRE_VERWANTSCHAP'
        , 'RLE'
        , l_rle_numrelatie_dlnmr
        , l_rle_numrelatie_vrwnt
        , p_datum_event
        );
      end if;
      --
   end if;
end;
END RLE_AQ_RKN_RLE_RIE;
 
/