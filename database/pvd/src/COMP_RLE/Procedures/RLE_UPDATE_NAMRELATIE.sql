CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPDATE_NAMRELATIE
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_NAMRELATIE IN RLE_RELATIES.NAMRELATIE%TYPE
 ,P_SUCCES OUT VARCHAR2
 )
 IS
BEGIN
   p_succes := NULL ;
   UPDATE rle_relaties
   SET    namrelatie = p_namrelatie
   WHERE  numrelatie = p_numrelatie
   ;
   p_succes := 'J' ;
EXCEPTION
   WHEN OTHERS THEN
      p_succes := 'N' ;
END ;

 
/