CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_FINNUM_SEPA
 (P_NUMRELATIE IN VARCHAR2
 ,P_CODROL IN VARCHAR2
 ,P_SRTREK IN VARCHAR2
 ,P_DATUM IN DATE
 ,P_REKENING OUT VARCHAR2
 ,P_BIC OUT VARCHAR2
 ,P_IBAN OUT VARCHAR2
 ,P_BET_WIJZE OUT VARCHAR2
 )
 IS

-- Program Data
L_NAAM VARCHAR2(80);
L_NUMREKENING VARCHAR2(55);
L_SRTREK VARCHAR2(20);
L_MELDING VARCHAR2(80);
L_BIC VARCHAR2(44);
L_IBAN VARCHAR2(44);
L_BET_WIJZE VARCHAR2(2);
-- PL/SQL Block
BEGIN
/* RLE_GET_FINNUM */
rle_m29_finnum_sepa(p_numrelatie
, p_codrol
, p_srtrek
, p_datum
, l_numrekening
, l_srtrek
, l_naam
, l_melding
, l_bic
, l_iban
, l_bet_wijze);
p_rekening := l_numrekening;
p_bic := l_bic;
p_iban := l_iban;
p_bet_wijze := l_bet_wijze;
END RLE_GET_FINNUM_SEPA;
 
/