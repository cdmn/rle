CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_WIJZIG_GBA
 (PIN_NUMRELATIE IN NUMBER
 ,PIV_GBA_STATUS IN VARCHAR2
 ,PIV_GBA_AFNEMERS_IND IN VARCHAR2
 ,PIN_GBA_AANMELDINGSNUMMER IN NUMBER
 )
 IS

/* Zoeken aanmelding met NUMRELATIE */
CURSOR C_AMG_001
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT *
FROM   rle_aanmeldingen  amg
WHERE  amg.rle_numrelatie = cpn_numrelatie
;
/* Zoeken afstemming gba met NUMRELATIE */
CURSOR C_ASA_001
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT *
FROM   rle_afstemmingen_gba  asa
WHERE  asa.rle_numrelatie = cpn_numrelatie
;

R_ASA_001 C_ASA_001%ROWTYPE;
R_AMG_001 C_AMG_001%ROWTYPE;
LV_OUDE_INDICATIE VARCHAR2(1);
BEGIN
/* Garanderen dat doorgave op "N" staat. */
lv_oude_indicatie := rle_indicatie.doorgeven_aan_gba ;
   rle_indicatie.doorgeven_aan_gba := 'N' ;


   /* Insert of update aanmeldingsnummer.
      Maar alleen als het gevuld is.
   */ /* */
   IF pin_gba_aanmeldingsnummer IS NOT NULL
   THEN
      OPEN c_amg_001( pin_numrelatie ) ;
      FETCH c_amg_001 INTO r_amg_001 ;

      IF c_amg_001%NOTFOUND
      THEN
         CLOSE c_amg_001 ;

         INSERT INTO rle_aanmeldingen(
              rle_numrelatie
            , aanmeldingsnummer
            ) VALUES(
              pin_numrelatie
            , pin_gba_aanmeldingsnummer
            ) ;
      ELSE
         CLOSE c_amg_001 ;

         UPDATE rle_aanmeldingen  amg
         SET    amg.aanmeldingsnummer = pin_gba_aanmeldingsnummer
         WHERE  amg.rle_numrelatie    = pin_numrelatie
         ;
      END IF ;
   END IF ;


   /* Insert of update status gba en afnemers indicatie */
   OPEN c_asa_001( pin_numrelatie ) ;
   FETCH c_asa_001 INTO r_asa_001 ;

   IF c_asa_001%NOTFOUND
   THEN
      CLOSE c_asa_001 ;

      INSERT INTO rle_afstemmingen_gba(
           rle_numrelatie
         , afnemers_indicatie
         , code_gba_status
         ) VALUES(
           pin_numrelatie
         , NVL( piv_gba_afnemers_ind, 'N' )
         , NVL( piv_gba_status, 'AA' )
         ) ;
   ELSE
      CLOSE c_asa_001 ;

      UPDATE rle_afstemmingen_gba  asa
      SET    asa.afnemers_indicatie = NVL( piv_gba_afnemers_ind, asa.afnemers_indicatie ) --LHT, 09-04-2015, afnemersindicatie niet naar N, maar bestaande waarde indien lege invoerparameter
      ,      asa.code_gba_status    = NVL( piv_gba_status, asa.code_gba_status )
      WHERE  asa.rle_numrelatie     = pin_numrelatie
      ;
   END IF ;


   /* Doorgave terugzetten */
   rle_indicatie.doorgeven_aan_gba := lv_oude_indicatie ;
END RLE_GBA_WIJZIG_GBA;
/