CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_CONTACTPERSOON
 (P_NUMRELATIE IN NUMBER
 ,P_SRT_ADRES  IN VARCHAR2
 ,P_SRT_CONTACTPERS IN VARCHAR2
 ,P_NUMRELATIE_CONTACTPERS OUT NUMBER
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen contactpersoon */
CURSOR C_CPN
 (B_NUMRELATIE IN NUMBER
 ,B_SRT_ADRES  IN VARCHAR2
 ,B_SRT_CONTACTPERS IN VARCHAR2
 )
 IS
/* C_CPN */
select cpn.rle_numrelatie
from   rle_contactpersonen cpn
,      rle_relatie_adressen ras
where  cpn.ras_id = ras.id
and    nvl(cpn.code_soort_contactpersoon, '@') = nvl(b_srt_contactpers, nvl(cpn.code_soort_contactpersoon, '@'))
and    ras.rle_numrelatie = b_numrelatie
and    ras.dwe_wrddom_srtadr = b_srt_adres
and    ras.datingang <= sysdate
and    nvl(ras.dateinde, sysdate) >= sysdate
order by cpn.dat_creatie desc
;
cn_module  constant  varchar2(30):= 'RLE_GET_CONTACTPERSOON';
-- Sub-Program Units
-- PL/SQL Block
BEGIN
 /* ophalen naam contactpersoon */
  OPEN  c_cpn(p_numrelatie
             ,p_srt_adres
             ,p_srt_contactpers);
  FETCH c_cpn INTO p_numrelatie_contactpers;
  IF c_cpn%notfound
  then
     p_numrelatie_contactpers:= null;
  end if;
  CLOSE c_cpn;
EXCEPTION
   WHEN OTHERS THEN
      IF c_cpn%ISOPEN
      THEN
         CLOSE c_cpn;
         raise_application_error(-20000,stm_app_error(sqlerrm,
                                                     cn_module
                                                     ));
      END IF ;
END RLE_GET_CONTACTPERSOON;

 
/