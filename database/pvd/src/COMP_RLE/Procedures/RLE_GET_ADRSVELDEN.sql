CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_ADRSVELDEN
 (P_OPDRACHTGEVER IN NUMBER
 ,P_PRODUKT IN VARCHAR2
 ,P_NUMRELATIE IN NUMBER
 ,P_CODROL IN VARCHAR2
 ,P_SRTADRES IN varchar2
 ,P_DATUMP IN date
 ,P_POSTCODE OUT VARCHAR2
 ,P_HUISNR OUT number
 ,P_TOEVNUM OUT VARCHAR2
 ,P_WOONPLAATS OUT varchar2
 ,P_STRAAT OUT varchar2
 ,P_CODLAND OUT VARCHAR2
 ,P_LOCATIE OUT VARCHAR2
 ,P_IND_WOONBOOT OUT varchar2
 ,P_IND_WOONWAGEN OUT varchar2
 ,P_LANDNAAM OUT varchar2
 ,P_BEGINDATUM OUT date
 ,P_EINDDATUM OUT date
 )
 IS

-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_WPS_001 (B_NUMADRES IN NUMBER)
IS
 select	wps.woonplaats
 from	rle_woonplaatsen wps
 ,	rle_adressen ads
 where	wps.woonplaatsid = ads.wps_woonplaatsid
 and	numadres = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_STT_001 (B_NUMADRES IN NUMBER)
IS
 select	stt.straatnaam
 from	rle_straten stt
 ,	rle_adressen ads
 where	stt.straatid  = ads.stt_straatid
 and	ads.numadres  = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_RAS_021
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL     IN VARCHAR2
 ,B_SRTADRES   IN VARCHAR2
 ,B_DATUMP     IN DATE)
IS
 select	ras.ads_numadres
 ,      ras.locatie
 ,      ras.ind_woonboot
 ,      ras.ind_woonwagen
 ,      ras.datingang
 ,      ras.dateinde
 from	rle_relatie_adressen ras
 where 	ras.rle_numrelatie = b_numrelatie
 and    	(ras.rol_codrol is null
 	or ras.rol_codrol = b_codrol)
 and   	ras.dwe_wrddom_srtadr = b_srtadres
 and     nvl(b_datump,trunc(sysdate))
             between ras.datingang
             and     nvl(ras.dateinde
                        ,nvl(b_datump,trunc(sysdate))
                        )
 order by ras.rle_numrelatie, ras.rol_codrol;
/* Ophalen van relatie-adres */
CURSOR C_ADS_002 (B_NUMADRES IN NUMBER)
IS
 select ads.huisnummer
 ,      ads.toevhuisnum
 ,      ads.postcode
 ,      ads.lnd_codland
 from   rle_adressen ads
 where  ads.numadres = b_numadres;
--
R_STT_001                C_STT_001%ROWTYPE;
R_WPS_001                C_WPS_001%ROWTYPE;
R_RAS_021                C_RAS_021%ROWTYPE;
--
l_gebruik_toegestaan     varchar2(2);
l_soort_adres            varchar2(4);
l_peildatum              date;
l_opdrachtgever          number(10);
l_gegeven_type           varchar2(10):= 'ADRES';
l_leveringsovereenkomst  varchar2(10):= 'GBA';
--
BEGIN

   /* RLE_GET_ADRSVELDEN */
   p_postcode      := NULL ;
   p_huisnr        := NULL ;
   p_toevnum       := NULL ;
   p_woonplaats    := NULL ;
   p_straat        := NULL ;
   p_codland       := NULL ;
   p_locatie       := NULL;
   p_ind_woonboot  := NULL;
   p_ind_woonwagen := NULL;
   p_landnaam      := NULL;
   p_begindatum    := NULL;
   p_einddatum     := NULL;
   --
   /* Aanpassing IVM RRGG: bij opvragen Domicilie adres
    eerst controleren of opdrachtgever/product wel geautoriseerd zijn
   */

   l_soort_adres   := p_srtadres;
   l_peildatum     := sysdate;
   l_opdrachtgever := p_opdrachtgever;
   --

   if l_soort_adres = 'DA'
   then
    ibm.ibm ('CHK_GEGEVENSGEBRUIK'
             ,'RLE'
             ,p_produkt
             ,l_opdrachtgever
             ,l_gegeven_type
             ,l_leveringsovereenkomst
             ,l_peildatum
             ,l_gebruik_toegestaan);
      --
      if l_gebruik_toegestaan = 'N'
      then
         l_soort_adres := 'OA';
      end if;
   end if;
   --
   --dbms_output.put_line('open c_ras021 A');
   --dbms_output.put_line('p_numrelatie 1 '||p_numrelatie);
   OPEN  c_ras_021 (p_numrelatie
                  , p_codrol
                  , l_soort_adres
                  , p_datump);
   FETCH c_ras_021 INTO r_ras_021;
   IF  c_ras_021%FOUND
   THEN
        -- dbms_output.put_line('c_ras_021 found 1');
		-- dbms_output.put_line('r_ras_021.ads_numadres 1 '||r_ras_021.ads_numadres);
         OPEN  c_ads_002( r_ras_021.ads_numadres);
         FETCH  c_ads_002
         INTO     p_huisnr
         ,        p_toevnum
         ,        p_postcode
         ,        p_codland
          ;
                 /* Controleer of er een geldig adres is voor DA anders CA ophalen
                 */
           --    dbms_output.put_line('p_huisnr'||p_huisnr);
           --    dbms_output.put_line('p_postcode'||p_postcode);
           --    dbms_output.put_line('p_codland'||p_codland);
           --    dbms_output.put_line('l_soort_adres'||l_soort_adres);


         IF (p_huisnr   IS NULL
             OR p_postcode IS NULL
             or p_codland  IS NULL)
            -- or
			 and UPPER(P_CODLAND)= 'ONB'
                   --  )
             and  l_soort_adres ='DA'
             then
             CLOSE c_ras_021;
             CLOSE c_ads_002;
              --        dbms_output.put_line('Open c_ras voor CA adres');
			  --	    dbms_output.put_line('p_numrelatie '||p_numrelatie);
            OPEN  c_ras_021 (p_numrelatie
                            , p_codrol
                            , 'CA'
                            , p_datump);
            FETCH c_ras_021 INTO r_ras_021;
            IF  c_ras_021%FOUND
			    THEN
                --dbms_output.put_line('c_ras found 2 ');
				--dbms_output.put_line('num adres'||r_ras_021.ads_numadres);
                OPEN  c_ads_002( r_ras_021.ads_numadres);
                FETCH  c_ads_002
                INTO     p_huisnr
                  ,        p_toevnum
                  ,        p_postcode
                  ,        p_codland
                  ;
                  CLOSE c_ads_002;
				-- dbms_output.put_line('p_codland '||p_codland);
               IF p_codland = 'NL'
                  THEN
				 -- dbms_output.put_line('landcode = NL');
                  -- Actuele postcode tabel raadplegen
                  rle_get_wps_stt(p_postcode
                                , p_huisnr
                                , p_woonplaats
                                , p_straat);
                  --
                  IF     p_woonplaats is null
                  AND p_straat    is null
                         THEN
                     OPEN   c_stt_001(r_ras_021.ads_numadres);
                     FETCH  c_stt_001 INTO   p_straat;
                     CLOSE  c_stt_001;
                     --
                     OPEN   c_wps_001(r_ras_021.ads_numadres);
                     FETCH  c_wps_001 INTO   p_woonplaats;
                     CLOSE  c_wps_001;
                  END IF;-- woonplaats is null

               Else   -- land <> NL
              -- dbms_output.put_line('land code <> NL');
        	  -- dbms_output.put_line('hier land code <> NL');
		      -- Actuele postcode tabel raadplegen
                  rle_get_wps_stt(p_postcode
                                , p_huisnr
                                , p_woonplaats
                                , p_straat);
                   --
                   IF     p_woonplaats is null
                   AND p_straat    is null
                   THEN
				    -- dbms_output.put_line('open cstt_001 met num adrs '||r_ras_021.ads_numadres);
                      OPEN   c_stt_001(r_ras_021.ads_numadres);
                      FETCH  c_stt_001 INTO   p_straat;
                      CLOSE  c_stt_001;
                      --
					 -- dbms_output.put_line('open c_wps_001 met num adrs'||r_ras_021.ads_numadres);
                      OPEN   c_wps_001(r_ras_021.ads_numadres);
                      FETCH  c_wps_001 INTO   p_woonplaats;
                      CLOSE  c_wps_001;
                   END IF; -- woonplaats is null
                 END IF; -- codland = nl

			else -- c_ras_021 found haal adres op

            --dbms_output.put_line('land code NL 2');
                              -- Actuele postcode tabel raadplegen
                  rle_get_wps_stt(p_postcode
                                , p_huisnr
                                , p_woonplaats
                                , p_straat);
                   --
              IF     p_woonplaats is null
              AND p_straat    is null
              THEN
			 -- dbms_output.put_line('open cstt_001 met num adrs '||r_ras_021.ads_numadres);
              OPEN   c_stt_001(r_ras_021.ads_numadres);
              FETCH  c_stt_001 INTO   p_straat;
              CLOSE  c_stt_001;
              --
			 -- dbms_output.put_line('open c_wps_001 met num adrs'||r_ras_021.ads_numadres);
              OPEN   c_wps_001(r_ras_021.ads_numadres);
              FETCH  c_wps_001 INTO   p_woonplaats;
              CLOSE  c_wps_001;
              END IF;


          END IF; -- c_ras_021 found
		else
		 rle_get_wps_stt(p_postcode
                                , p_huisnr
                                , p_woonplaats
                                , p_straat);

		 --dbms_output.put_line('VOOR HUISNR IS NULL');
		  OPEN   c_stt_001(r_ras_021.ads_numadres);
                      FETCH  c_stt_001 INTO   p_straat;
                      CLOSE  c_stt_001;
                      --
					 -- dbms_output.put_line('open c_wps_001 met num adrs'||r_ras_021.ads_numadres);
                      OPEN   c_wps_001(r_ras_021.ads_numadres);
                      FETCH  c_wps_001 INTO   p_woonplaats;
                      CLOSE  c_wps_001;


        end if; --huisnr null

		end if; --c_ras_021 found

       --

       p_landnaam :=  RLE_LND_GETOMS (p_codland);
       --
       p_locatie           := r_ras_021.locatie;
       p_ind_woonboot      := r_ras_021.ind_woonboot;
       p_ind_woonwagen     := r_ras_021.ind_woonwagen;
       p_begindatum        := r_ras_021.datingang;
       p_einddatum         := r_ras_021.dateinde;
--   dbms_output.put_line('p_straat'||p_straat);
--   dbms_output.put_line('p_woonplaats'||p_woonplaats);
--   dbms_output.put_line('p_landnaam'||p_landnaam);
--   dbms_output.put_line('eind');
   CLOSE   c_ras_021;
END RLE_GET_ADRSVELDEN;
 
/