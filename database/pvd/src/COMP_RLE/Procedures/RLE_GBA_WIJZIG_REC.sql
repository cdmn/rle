CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_WIJZIG_REC
 (PIN_NUMRELATIE IN NUMBER
 ,PIV_DWE_WRDDOM_EXTSYS IN VARCHAR2
 ,PIV_ROL_CODROL IN VARCHAR2
 ,PIV_EXTERN_NUMMER IN VARCHAR2
 )
 IS
-- Program Data
LV_OUDE_INDICATIE VARCHAR2(1);
-- PL/SQL Block
BEGIN
/* Garanderen dat doorgave op "N" staat. */
   lv_oude_indicatie := rle_indicatie.doorgeven_aan_gba ;
   rle_indicatie.doorgeven_aan_gba := 'N' ;
   IF piv_extern_nummer != NVL( rle_m03_rec_extcod( piv_dwe_wrddom_extsys
                                                  , pin_numrelatie
                                                  , piv_rol_codrol
                                                  , SYSDATE
                                                  )
                              , '@'
                              )
   THEN
      rle_m16_rec_verwerk( piv_extern_nummer
                         , piv_dwe_wrddom_extsys
                         , piv_rol_codrol
                         , pin_numrelatie
                         , TRUNC( SYSDATE )
                         , NULL
                         ) ;
   END IF ;
   /* Doorgave terugzetten */
   rle_indicatie.doorgeven_aan_gba := lv_oude_indicatie ;
END RLE_GBA_WIJZIG_REC;

 
/