CREATE OR REPLACE PROCEDURE comp_rle.RLE_INSERT_WGR
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ERROR OUT VARCHAR2
 )
 IS
/****************************************************************************************************************
Wijzigingshistorie:

Wanneer	             Wie                 Wat
-------------        ------------------  -------
09-02-2009           XMB                 Tbv Levensloop. Creatie.
                                         Hiermee kan de de werkgeverworden vastgelegd in
                                         de component WGR.
18-06-2009           XMB                 Ook voor adm_codes MNLL en MTLL.
07-07-2009           XMB                 Codes groep van werkzaamheden voor MTLL zijn veranderd.
10-07-2009           XMB                 Aanroep naar WGR_MAAK_WSS_DEF veranderd.
****************************************************************************************************************/
    --
    cursor c_rle ( b_numrelatie in number)
    is
    select   rle.numrelatie
           , rle.namrelatie   bedrijfsnaam
           , substr(rle.handelsnaam,1,30) handelsnaam
           , trunc(nvl (rle.datbegin, rle.dat_creatie)) begindatum
           , rle.DWE_WRDDOM_RVORM code_rechtsvorm
    from   rle_relaties             rle
    where  rle.numrelatie in
      (select rie.rle_numrelatie
       from   rle_relatierollen_in_adm rie
       where  rie.adm_code in ('MTLL','MELL','MNLL')
      )
    and    rle.numrelatie not in
     (select wgr.nr_relatie
      from   wgr_werkgevers wgr
     )
    and   rle.numrelatie = b_numrelatie;
    --
    r_rle                         c_rle%rowtype;
    --
    cursor c_rie ( b_numrelatie in number)
    is
    select rie.adm_code
    from   rle_relatierollen_in_adm rie
    where  rie.adm_code in ('MTLL','MELL','MNLL')
    and    rie.rle_numrelatie = b_numrelatie;
    --
    cursor c_ads (b_numrelatie rle_relaties.numrelatie%type)
    is
    select     ads.ads_numadres
    from       rle_relatie_adressen  ads
    where      ads.DWE_WRDDOM_SRTADR = 'SZ'
    and        ads.rol_codrol        = 'WG'
    and        ads.dateinde          is null
    and        ads.rle_numrelatie = b_numrelatie;
    --
    cursor c_gvw (b_naam gvw.naam%type)
    is
    select kodegvw
    from   gvw gvw
    where  gvw.naam = b_naam;
    --
    l_numrelatie                  number (10);
    l_begindatum                  date;
    l_bedrijfsnaam                rle_relaties.namrelatie%type;
    l_handelsnaam                 rle_relaties.handelsnaam%type;
    l_numadres                    rle_adressen.numadres%type;
    l_adm_code                    rle_relatierollen_in_adm.adm_code%type;
    l_postcode                    rle_adressen.postcode%type;
    l_straatnaam                  rle_straten.straatnaam%type;
    l_huisnummer                  rle_adressen.huisnummer%type;
    l_woonplaats                  rle_woonplaatsen.woonplaats%type;
    l_toevhuisnum                 rle_adressen.toevhuisnum%type;
    l_land                        rle_adressen.lnd_codland%type;
    l_wgr_id                      number;
    l_verwerkt                    varchar2 (1);
    l_error                       varchar2 (32767);
    l_nr_werkgever                wgr_werkgevers.nr_relatie%type;
    l_code_hoofdgr_bedrtak        wgr_bedrijfstakken.code_hoofdgr_bedrtak%type;
    l_code_rechtsvorm             rle_relaties.DWE_WRDDOM_RVORM%type;
    l_code_groep_werkzhd          varchar2 (20);
    c_proc_name          constant varchar2(20) := 'RLE_INSERT_WGR';
    e_niet_doorgaan               exception;
    --
    cn_kodebdt_mell             constant    varchar2(5)   := 'MI';      -- Code hoofdgroep bedrijfstak voor MELL
    cn_kodebdt_mnll             constant    varchar2(5)   := 'IS';      -- Code hoofdgroep bedrijfstak voor MNLL
    cn_kodebdt_mtll             constant    varchar2(5)   := 'IS';      -- Code hoofdgroep bedrijfstak voor MTLL
    cn_kodegvw_mell             constant    varchar2(100) := 'Werkzaamheden Metalektro';         -- Code groep van werkzaamheden voor MELL
    cn_kodegvw_mnll             constant    varchar2(100) := 'Onbepaald';                        -- Code groep van werkzaamheden voor MNLL
    cn_kodegvw_mtll             constant    varchar2(100) := 'Werkzaamheden gelieerd aan de Metaal en Techniek'; -- Code groep van werkzaamheden voor MTLL
    cn_code_aanleiding          constant    varchar2(5)   := 'WGR';     -- Code aanleiding inschrijving
    --
begin
    --
    open  c_rle (p_rle_numrelatie);
    fetch c_rle into r_rle;
    if    c_rle%notfound then
      close c_rle;
      raise e_niet_doorgaan;
    end if;
    --
    l_numrelatie               := p_rle_numrelatie;
    l_bedrijfsnaam             := r_rle.bedrijfsnaam;
    l_handelsnaam              := r_rle.handelsnaam;
    l_begindatum               := r_rle.begindatum;
    l_code_rechtsvorm          := r_rle.code_rechtsvorm;
    --
    close c_rle;
    --
    open  c_rie (p_rle_numrelatie);
    fetch c_rie into l_adm_code;
    close c_rie;
    --
    l_code_hoofdgr_bedrtak     := case l_adm_code
                                    when 'MELL' then cn_kodebdt_mell
                                    when 'MNLL' then cn_kodebdt_mnll
                                    when 'MTLL' then cn_kodebdt_mtll
                                    else null
                                  end;
    --
    begin
      --
      open  c_ads( p_rle_numrelatie);
      fetch c_ads into l_numadres;
      close c_ads;
      --
      RLE_GET_ADRES
             (P_NUMADRES    => l_numadres
             ,P_POSTCODE    => l_postcode
             ,P_HUISNR      => l_huisnummer
             ,P_TOEVNUM     => l_toevhuisnum
             ,P_WOONPLAATS  => l_woonplaats
             ,P_STRAAT      => l_straatnaam
             ,P_LAND        => l_land
             );
        --
    exception
      when others
      then
          l_error                    := 'Relatie met nummer '
                                            || p_rle_numrelatie
                                            || ' niet gevonden in RLE_ADRESSEN';
          raise;
      end;
    --
    -- bepalen code groep van werkzaamheden
    case when l_adm_code = 'MELL'
         then
           open  c_gvw( cn_kodegvw_mell );
           fetch c_gvw into l_code_groep_werkzhd;
           if    c_gvw%notfound then
             close c_gvw;
             stm_dbproc.raise_error('ERROR - tijdens bepalen code groep van werkzaamheden');
           end if;
           close c_gvw;
         when l_adm_code = 'MNLL'
         then
           open  c_gvw( cn_kodegvw_mnll );
           fetch c_gvw into l_code_groep_werkzhd;
           if    c_gvw%notfound then
             close c_gvw;
             stm_dbproc.raise_error('ERROR - tijdens bepalen code groep van werkzaamheden');
           end if;
           close c_gvw;
         when l_adm_code = 'MTLL'
         then
           open  c_gvw( cn_kodegvw_mtll );
           fetch c_gvw into l_code_groep_werkzhd;
           if    c_gvw%notfound then
             close c_gvw;
             stm_dbproc.raise_error('ERROR - tijdens bepalen code groep van werkzaamheden');
           end if;
           close c_gvw;
        else
          l_code_groep_werkzhd := null;
    end case;
    --
    comp_wgr.wgr_reg_wgr (piv_statutaire_naam                 => l_bedrijfsnaam
                                , piv_handelsnaam             => l_handelsnaam
                                , pid_datum_begin             => l_begindatum
                                , piv_code_inschrijving       => cn_code_aanleiding
                                , pin_nr_relatie              => l_numrelatie
                                , pon_nr_werkgever            => l_nr_werkgever
                                , pov_verwerkt                => l_verwerkt
                                , pov_foutmelding             => l_error
                                , pid_datum_einde             => null
                                , piv_code_rechtsvorm         => l_code_rechtsvorm
                                , p_srt_adres                 => 'SZ'
                                , p_straat                    => l_straatnaam
                                , p_huisnummer                => l_huisnummer
                                , p_toevoeging_huisnummer     => null
                                , p_postcode                  => upper (l_postcode)
                                , p_woonplaats                => null
                                , p_provincie                 => null
                                , p_locatie                   => null
                                , p_ind_woonboot              => null
                                , p_ind_woonwagen             => null
                                 );
    if l_verwerkt = 'N' or l_error is not null
    then
      stm_dbproc.raise_error('ERROR - tijdens opvoeren van werkgever in component WGR');
    end if;
    -- bepalen wgr_id
    l_wgr_id := wgr_get_id_wgr_alg (p_rle_numrelatie);
    if l_wgr_id is null
    then
      stm_dbproc.raise_error('ERROR - tijdens bepalen wgr_id');
    end if;
    --
    wgr_insert_btk
      ( P_RLE_NUMRELATIE       => p_rle_numrelatie
      , P_DAT_BEGIN            => l_begindatum
      , P_CODE_HOOFDGR_BEDRTAK => l_code_hoofdgr_bedrtak
      , P_ERROR                => l_error
      );
    --
    if l_error is not null
    then
      raise e_niet_doorgaan;
    end if;
    -- toevoegen groep van werkzaamheden l_code_groep_werkzhd
    wgr_insert_wzd
      ( P_RLE_NUMRELATIE     => p_rle_numrelatie
      , P_DAT_BEGIN          => l_begindatum
      , P_CODE_GROEP_WERKZHD => l_code_groep_werkzhd
      , P_ERROR              => l_error
      );
    --
    if l_error is not null
    then
      raise e_niet_doorgaan;
    end if;
    -- set status van de werkgever
    wgr_maak_wss_def
      ( P_RLE_NUMRELATIE => p_rle_numrelatie
      , P_ADM_CODE       => l_adm_code
      , P_DAT_BEGIN      => l_begindatum
      , P_ERROR          => l_error
      );
    --
    if l_error is not null
    then
      p_error := l_error;
    end if;
    --
  exception
    when e_niet_doorgaan
    then
      if l_error is not null
      then
        p_error := c_proc_name||': '||l_error;
        stm_dbproc.raise_error('ERROR - '||p_error);
      end if;
    when others
    then
      if c_gvw%isopen
      then
        close c_gvw;
      end if;
      --
      if c_rle%isopen
      then
        close c_rle;
      end if;
      --
      if c_ads%isopen
      then
        close c_ads;
      end if;
      --
      p_error := c_proc_name||': '||SQLERRM;
      stm_dbproc.raise_error('ERROR - '||p_error);
END RLE_INSERT_WGR;
 
/