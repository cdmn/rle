CREATE OR REPLACE PROCEDURE comp_rle.RLE_CHK_PERSOON
 (P_OPDRACHTGEVER IN NUMBER
 ,P_PRODUCT IN VARCHAR2
 ,PIV_PERSOONSNUMMER IN RLE_RELATIE_EXTERNE_CODES.EXTERN_RELATIE%TYPE
 ,PIV_SOFINUMMER IN RLE_RELATIE_EXTERNE_CODES.EXTERN_RELATIE%TYPE
 ,POV_IND_BESTAAT OUT VARCHAR2
 ,POV_PERSOONSNUMMER OUT RLE_RELATIE_EXTERNE_CODES.EXTERN_RELATIE%TYPE
 ,POV_SOFINUMMER OUT RLE_RELATIE_EXTERNE_CODES.EXTERN_RELATIE%TYPE
 ,PON_NUMRELATIE OUT RLE_RELATIES.NUMRELATIE%TYPE
 ,POV_STATUS_GBA OUT VARCHAR2
 ,POV_IND_AFNEMERS OUT VARCHAR2
 ,POD_DATGEBOORTE OUT RLE_RELATIES.DATGEBOORTE%TYPE
 ,POV_COD_GESLACHT OUT RLE_RELATIES.DWE_WRDDOM_GESLACHT%TYPE
 ,POV_OMS_GESLACHT OUT VARCHAR2
 ,POD_DATOVERLIJDEN OUT RLE_RELATIES.DATOVERLIJDEN%TYPE
 ,POV_DA_STRAAT OUT RLE_STRATEN.STRAATNAAM%TYPE
 ,PON_DA_HUISNUMMER OUT RLE_ADRESSEN.HUISNUMMER%TYPE
 ,POV_DA_TOEVHUISNUM OUT RLE_ADRESSEN.TOEVHUISNUM%TYPE
 ,POV_DA_POSTCODE OUT RLE_ADRESSEN.POSTCODE%TYPE
 ,POV_DA_PLAATS OUT RLE_WOONPLAATSEN.WOONPLAATS%TYPE
 ,POV_DA_COD_LAND OUT RLE_LANDEN.CODLAND%TYPE
 ,POV_DA_OMS_LAND OUT RLE_LANDEN.NAAM%TYPE
 ,POV_OPGEMAAKTE_NAAM OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
--select	*
select numrelatie
,      codorganisatie
,      namrelatie
,      zoeknaam
,      indgewnaam
,      indinstelling
,      datschoning
,      dwe_wrddom_titel
,      dwe_wrddom_ttitel
,      dwe_wrddom_avgsl
,      dwe_wrddom_vvgsl
,      dwe_wrddom_brgst
,      dwe_wrddom_geslacht
,      dwe_wrddom_gewatitel
,      dwe_wrddom_rvorm
,      voornaam
,      voorletter
,      aanschrijfnaam
,      datgeboorte
,      code_gebdat_fictief
,      datoverlijden
,      datoprichting
,      datbegin
,      datcontrole
,      dateinde
,      indcontrole
,      regdat_overlbevest
,      datemigratie
,      naam_partner
,      vvgsl_partner
,      code_aanduiding_naamgebruik
,      handelsnaam
,      dat_creatie
,      creatie_door
,      dat_mutatie
,      mutatie_door
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
/* Ophalen van relatie-adres */
CURSOR C_LND_002
 (B_CODLAND IN VARCHAR2
 )
 IS
/* C_LND_002 */
select	*
from	rle_landen lnd
where	lnd.codland = b_codland;
-- Program Data
LD_DUMMY DATE;
LN_NUMRELATIE NUMBER;
LV_DUMMY VARCHAR2(100);
R_RLE_001 C_RLE_001%ROWTYPE;
R_LND_002 C_LND_002%ROWTYPE;
LV_RAISE_ERROR VARCHAR2(1) := 'N';
OTHERS EXCEPTION;
LN_MAX_LENGTE NUMBER := 40;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* AVR. Door de alter session in de ibm wordt pod_datgeboorte in het formaat
      dd-mm-yyyy hh:mm:ss teruggegeven. Dit gaat fout in de PMA_REGISTREER_AVH.
   */
   execute immediate 'alter session set nls_date_format = ''dd-mm-yyyy''';
   --
   rle_m06_rec_relnum( piv_persoonsnummer
                     , 'PRS'
                     , 'PR'
                     , SYSDATE
                     , ln_numrelatie
                     ) ;
   IF ln_numrelatie IS NULL
   THEN
      rle_m06_rec_relnum( piv_sofinummer
                        , 'SOFI'
                        , NULL
                        , SYSDATE
                        , ln_numrelatie
                        ) ;
   END IF ;
   IF ln_numrelatie IS NULL
   THEN
      RAISE NO_DATA_FOUND ;
   ELSE
      pov_ind_bestaat := 'J' ;
      pon_numrelatie  := ln_numrelatie ;
      /* Ophalen externe codes */
      pov_persoonsnummer := rle_m03_rec_extcod( 'PRS'
                                              , ln_numrelatie
                                              , 'PR'
                                              , SYSDATE
                                              ) ;
      pov_sofinummer := rle_m03_rec_extcod( 'SOFI'
                                          , ln_numrelatie
                                          , NULL
                                          , SYSDATE
                                          ) ;
      /* Status GBA */
      rle_gba_get_status( NULL
                        , ln_numrelatie
                        , pov_status_gba
                        , pov_ind_afnemers
                        ) ;
      IF pov_status_gba = 'AF'
      THEN
         pov_ind_bestaat := 'N' ;
         pov_persoonsnummer  := NULL ;
         pov_sofinummer      := NULL ;
         pon_numrelatie      := NULL ;
         pov_status_gba      := NULL ;
         pov_ind_afnemers    := NULL ;
         pod_datgeboorte     := NULL ;
         pov_cod_geslacht    := NULL ;
         pov_oms_geslacht    := NULL ;
         pod_datoverlijden   := NULL ;
         pov_da_straat       := NULL ;
         pon_da_huisnummer   := NULL ;
         pov_da_toevhuisnum  := NULL ;
         pov_da_postcode     := NULL ;
         pov_da_plaats       := NULL ;
         pov_da_cod_land     := NULL ;
         pov_da_oms_land     := NULL ;
         pov_opgemaakte_naam := NULL ;
      ELSE
         /* Ophalen persoonsgegevens */
         OPEN c_rle_001( ln_numrelatie ) ;
         FETCH c_rle_001 INTO r_rle_001 ;
         CLOSE c_rle_001 ;
         pod_datgeboorte   := r_rle_001.datgeboorte         ;
         pov_cod_geslacht  := r_rle_001.dwe_wrddom_geslacht ;
         pod_datoverlijden := r_rle_001.datoverlijden       ;
         /* Ophalen omschrijving geslacht */
         ibm.ibm( 'RFW_GET_WRD'
                , 'RLE'
                , 'GES'
                , r_rle_001.dwe_wrddom_geslacht
                , lv_raise_error
                , ln_max_lengte
                , pov_oms_geslacht
                ) ;
         /*
            Ophalen adresvelden.
            Controle ivm RGG zit in de aangeroepen module.
            In sommige gevallen -afhankelijk v/h product en opdr.gever-
            komt dus niet het DA-adres terug, maar het OA adres.
            OSP 16-06-2003
         */
         rle_get_adrsvelden (p_opdrachtgever
                           , p_product
                           , ln_numrelatie
                           , 'PR'
                           , 'DA'
                           , SYSDATE
                           , pov_da_postcode
                           , pon_da_huisnummer
                           , pov_da_toevhuisnum
                           , pov_da_plaats
                           , pov_da_straat
                           , pov_da_cod_land
                           , lv_dummy
      			   , lv_dummy
                           , lv_dummy
                           , lv_dummy
                           , ld_dummy
                           , ld_dummy
                           ) ;
         /* Ophalen omschrijving land */
         OPEN c_lnd_002( pov_da_cod_land ) ;
         FETCH c_lnd_002 INTO r_lnd_002 ;
         CLOSE c_lnd_002 ;
         pov_da_oms_land := r_lnd_002.naam ;
         /* Ophalen opgemaakte naam */
         rle_m11_rle_naw( 255
                        , ln_numrelatie
                        , pov_opgemaakte_naam
                        , lv_dummy
                        , 2
                        ) ;
      END IF ;
   END IF;
EXCEPTION
 WHEN OTHERS THEN
        pov_ind_bestaat     := 'N' ;
      pov_persoonsnummer  := NULL ;
      pov_sofinummer      := NULL ;
      pon_numrelatie      := NULL ;
      pov_status_gba      := NULL ;
      pov_ind_afnemers    := NULL ;
      pod_datgeboorte     := NULL ;
      pov_cod_geslacht    := NULL ;
      pov_oms_geslacht    := NULL ;
      pod_datoverlijden   := NULL ;
      pov_da_straat       := NULL ;
      pon_da_huisnummer   := NULL ;
      pov_da_toevhuisnum  := NULL ;
      pov_da_postcode     := NULL ;
      pov_da_plaats       := NULL ;
      pov_da_cod_land     := NULL ;
      pov_da_oms_land     := NULL ;
      pov_opgemaakte_naam := NULL ;
END RLE_CHK_PERSOON;

 
/