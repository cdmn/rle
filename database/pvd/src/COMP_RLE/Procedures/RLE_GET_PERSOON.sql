CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_PERSOON
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_NAMRELATIE OUT RLE_RELATIES.NAMRELATIE%TYPE
 ,P_DATGEBOORTE OUT RLE_RELATIES.DATGEBOORTE%TYPE
 ,P_VOORLETTER OUT RLE_RELATIES.VOORLETTER%TYPE
 ,P_VOORVOEGSEL OUT VARCHAR2
 ,P_STRAATNAAM OUT RLE_STRATEN.STRAATNAAM%TYPE
 ,P_HUISNUMMER OUT RLE_ADRESSEN.HUISNUMMER%TYPE
 ,P_TOEVHUISNUM OUT RLE_ADRESSEN.TOEVHUISNUM%TYPE
 ,P_POSTCODE OUT RLE_ADRESSEN.POSTCODE%TYPE
 ,P_WOONPLAATS OUT RLE_WOONPLAATSEN.WOONPLAATS%TYPE
 ,P_LAND OUT RLE_LANDEN.NAAM%TYPE
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van adresvelden opbasis van id */
CURSOR C_ADS_011
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_ADS_011 */
select	*
from	rle_adressen ads
where	ads.numadres = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_LND_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_LND_001 */
select	lnd.codlayoutadres
,	lnd.codland
,	lnd.naam
from	rle_landen lnd
where	lnd.codland =
	(select	ads.lnd_codland
	from	rle_adressen ads
	where	ads.numadres = b_numadres);
/* Ophalen van relatie-adres */
CURSOR C_RAS_006
 (B_NUMRELATIE IN NUMBER
 ,B_SRTADRES IN VARCHAR2
 ,B_DATUMP IN DATE
 )
 IS
/* C_RAS_006 */
select	ras.ads_numadres
from	rle_relatie_adressen ras
where	ras.rle_numrelatie = b_numrelatie
and	ras.rol_codrol is NULL
and	ras.dwe_wrddom_srtadr = b_srtadres
and	ras.datingang <= b_datump
and	nvl(dateinde,to_date('31-DEC-4712','DD-MON-YYYY')) >
        nvl(b_datump,to_date('31-DEC-4712','DD-MON-YYYY'));
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
select	*
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
CURSOR C_STT_003
 (B_STRAATID IN NUMBER
 )
 IS
/* C_STT_003 */
select	*
from	rle_straten stt
where	stt.straatid = b_straatid;
/* c_wnpl03 */
CURSOR C_WPS_003
 (B_WPS_ID IN NUMBER
 )
 IS
/* C_WPS_003 */
select wps.*
,		 rowid
from	rle_woonplaatsen wps
where	wps.woonplaatsid = b_wps_id;
-- Program Data
R_ADS_011 C_ADS_011%ROWTYPE;
L_LENGTE_IBM NUMBER;
LB_ADRES_GEVONDEN BOOLEAN;
R_LND_001 C_LND_001%ROWTYPE;
R_RAS_006 C_RAS_006%ROWTYPE;
R_RLE_001 C_RLE_001%ROWTYPE;
R_STT_003 C_STT_003%ROWTYPE;
L_IND_IBM VARCHAR2(1);
R_WPS_003 C_WPS_003%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_GET_PERSOON */
   p_namrelatie  := NULL ;
   p_datgeboorte := NULL ;
   p_voorletter  := NULL ;
   p_voorvoegsel := NULL ;
   p_straatnaam  := NULL ;
   p_huisnummer  := NULL ;
   p_toevhuisnum := NULL ;
   p_postcode    := NULL ;
   p_woonplaats  := NULL ;
   p_land        := NULL ;
   /* Ophalen relatiegegevens */
   OPEN c_rle_001( p_numrelatie ) ;
   FETCH c_rle_001 INTO r_rle_001 ;
   IF c_rle_001%NOTFOUND
   THEN
      /* Relatie kan niet gevonden worden. */
      CLOSE c_rle_001 ;
      RETURN ;
   END IF ;
   CLOSE c_rle_001 ;
   /* Vul persoonsgegevens in uitvoer parameters */
   p_namrelatie := r_rle_001.namrelatie ;
   p_datgeboorte := r_rle_001.datgeboorte ;
   p_voorletter := r_rle_001.voorletter ;
   /* Opzoeken referentie waarde voor voorvoegsel */
   ibm.ibm( 'RFW_GET_WRD'
          , 'RLE'
          , 'VVL'
          , r_rle_001.dwe_wrddom_vvgsl
          , l_ind_ibm
          , l_lengte_ibm
          , p_voorvoegsel
          ) ;
   /* Ophalen relatie adres gegevens
      In eerste principe het DOMICILIE adres (DA), als
      dat niet bestaat het CORRESPONDENTIE adres (CA).
   */
   lb_adres_gevonden := FALSE ;
   OPEN c_ras_006( p_numrelatie, 'DA', SYSDATE ) ;
   FETCH c_ras_006 INTO r_ras_006 ;
   IF c_ras_006%FOUND
   THEN
      CLOSE c_ras_006 ;
      lb_adres_gevonden := TRUE ;
   ELSE
      CLOSE c_ras_006 ;
      OPEN c_ras_006( p_numrelatie, 'CA', SYSDATE ) ;
      FETCH c_ras_006 INTO r_ras_006 ;
      IF c_ras_006%FOUND
      THEN
         lb_adres_gevonden := TRUE ;
      END IF ;
      CLOSE c_ras_006 ;
   END IF ;
   IF lb_adres_gevonden
   THEN
      /* Ophalen adres gegevens */
      OPEN c_ads_011( r_ras_006.ads_numadres ) ;
      FETCH c_ads_011 INTO r_ads_011 ;
      CLOSE c_ads_011 ;
      /* Ophalen straat gegevens */
      OPEN c_stt_003( r_ads_011.stt_straatid ) ;
      FETCH c_stt_003 INTO r_stt_003 ;
      CLOSE c_stt_003 ;
      /* Ophalen woonplaats gegevens */
      OPEN c_wps_003( r_stt_003.wps_woonplaatsid ) ;
      FETCH c_wps_003 INTO r_wps_003 ;
      CLOSE c_wps_003 ;
      /* Ophalen land gegevens */
      OPEN c_lnd_001( r_ras_006.ads_numadres ) ;
      FETCH c_lnd_001 INTO r_lnd_001 ;
      CLOSE c_lnd_001 ;
      /* Vul adres gegevens in uitvoer parameters */
      p_straatnaam  := r_stt_003.straatnaam  ;
      p_huisnummer  := r_ads_011.huisnummer  ;
      p_toevhuisnum := r_ads_011.toevhuisnum ;
      p_postcode    := r_ads_011.postcode    ;
      p_woonplaats  := r_wps_003.woonplaats  ;
      p_land        := r_lnd_001.naam        ;
   END IF ;
END RLE_GET_PERSOON;

 
/