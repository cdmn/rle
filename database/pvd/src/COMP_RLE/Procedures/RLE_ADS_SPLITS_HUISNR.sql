CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADS_SPLITS_HUISNR
 (P_HUISNUMMER IN VARCHAR2
 ,P_HUISNR OUT VARCHAR2
 ,P_HUISNR_TOEV OUT VARCHAR2
 )
 IS
-- Program Data
L_TELLER NUMBER(3);
L_LENGTE NUMBER(3);

-- PL/SQL Block
BEGIN
l_lengte := nvl( length( p_huisnummer), 0);
l_teller := 1;
--
while l_teller <= l_lengte
  LOOP
    if substr( p_huisnummer, l_teller, 1) NOT IN
         ( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
    then
       p_huisnr := substr(ltrim(rtrim(p_huisnummer)),1,l_teller-1);
       p_huisnr_toev := substr(ltrim(rtrim(p_huisnummer)),l_teller);
       exit;
    end if;
    l_teller := l_teller + 1;
  END LOOP;
--
if l_teller > l_lengte
then
   p_huisnr := ltrim( rtrim( p_huisnummer));
   p_huisnr_toev := '';
end if;
END RLE_ADS_SPLITS_HUISNR;

 
/