CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_WIJZIG_ADRES
 (PIN_PERSOONSNUMMER IN NUMBER
 ,PIV_STRAATNAAM IN VARCHAR2
 ,PIN_HUISNUMMER IN NUMBER
 ,PIV_WOONPLAATS IN VARCHAR2
 ,PIV_HUISNUMMER_TOEVOEGING IN VARCHAR2
 ,PIV_POSTCODE IN VARCHAR2
 ,PIV_LOCATIEOMSCHRIJVING IN VARCHAR2
 ,PIV_LANDCODE IN VARCHAR2
 ,POV_VERWERKT OUT VARCHAR2
 ,POV_FOUTMELDING OUT VARCHAR2
 ,PIV_OFMW_MUTATIE IN BOOLEAN := false
 )
 IS
  /******************************************************************************************************
  Naam:         RLE_GBA_WIJZIG_ADRES
  Beschrijving: RLE0936: Wijzigen adres vanuit GBA

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  ??-??-????  ???  Creatie
  22-02-2008  PAS  GBA-aanpassing Zorgketen 2008
                   Afhankelijk van het gegeven of er met het GBA mag worden afgestemd wordt het
                   rle_relatie_adressen record geselecteerd met het domicilie (afstemmen mag)
                   of opgegeven adres (afstemmen mag niet). Betreft aanpassing in cursor C_RAS_031.
  12-08-2008  XMK  Als er alleen een OA adres aanwezig is en de persoon mag afgestemd worden dan
                   wordt op basis van het OA adres een DA adres aangemaakt. Aanpassing in cursor C_RAS_031.
  05-05-2014  XJB  Wel/niet doorgeven nalv mutaties via adresService op de middleware.
  ******************************************************************************************************/
CURSOR C_RAS_030
 (CPN_RAS_ID IN NUMBER
 )
 IS
SELECT ras.rle_numrelatie
,      ras.rol_codrol
,      ras.dwe_wrddom_srtadr
,      stt.straatnaam
,      ads.huisnummer
,      wps.woonplaats
,      ads.toevhuisnum
,      ads.postcode
,      ras.locatie
,      ras.provincie
,      lnd.codland
,      lnd.naam
FROM   rle_relatie_adressen  ras
,      rle_adressen          ads
,      rle_straten           stt
,      rle_woonplaatsen      wps
,      rle_landen            lnd
WHERE  ras.id               = cpn_ras_id
AND    ras.ads_numadres     = ads.numadres
AND    ads.stt_straatid     = stt.straatid (+)
AND    stt.wps_woonplaatsid = wps.woonplaatsid (+)
AND    ads.lnd_codland      = lnd.codland
;
--
CURSOR C_RAS_031
 (CPN_NUMRELATIE IN VARCHAR2
 )
 IS
SELECT *
FROM   rle_relatie_adressen  ras
WHERE  ras.rle_numrelatie    = cpn_numrelatie
AND    ras.rol_codrol        = 'PR'
AND (
      (     ras.dwe_wrddom_srtadr                    = 'DA'
        AND rle_chk_gba_afst( cpn_numrelatie, NULL ) = 'J'
      )
    OR
           ras.dwe_wrddom_srtadr                    = 'OA'
    )
AND    SYSDATE BETWEEN TRUNC( ras.datingang ) AND NVL( ras.dateinde, SYSDATE )
ORDER BY ras.dwe_wrddom_srtadr
;
/* Ophalen overlijdensdatum relatie */
CURSOR C_RLE_018
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_018 */
SELECT		rle.datoverlijden
FROM		rle_relaties rle
WHERE		rle.numrelatie = b_numrelatie;

-- Program Data
LV_ADRESREGEL3 VARCHAR2(255);
LN_RAS_ID NUMBER;
R_RAS_031 C_RAS_031%ROWTYPE;
LN_NUMRELATIE NUMBER;
LN_NUMADRES NUMBER;
R_RAS_030 C_RAS_030%ROWTYPE;
LV_OUDE_INDICATIE VARCHAR2(1);
R_RLE_018 C_RLE_018%ROWTYPE;
LV_ADRESREGEL1 VARCHAR2(255);
LV_ADRESREGEL2 VARCHAR2(255);
LD_BEGINDATUM DATE;
--
BEGIN
/* Garanderen dat doorgave op "N" staat. */
   lv_oude_indicatie := rle_indicatie.doorgeven_aan_gba ;

   if not piv_ofmw_mutatie  -- xjb 5-5-2014, wel/niet doorgeven nalv mutaties via adresService op de middleware
   then
     rle_indicatie.doorgeven_aan_gba := 'N' ;
   else
     rle_indicatie.doorgeven_aan_gba := 'J' ;
   end if;


   /* Default uitvoerwaarden */
   pov_verwerkt    := 'N' ;
   pov_foutmelding := NULL ;


   /* Bepalen relatienummer */
   rle_m06_rec_relnum( pin_persoonsnummer
                     , 'PRS'
                     , 'PR'
                     , SYSDATE
                     , ln_numrelatie
                     ) ;


   /* Als het relatienummer niet gevonden kan
      worden dan hebben we een probleem.
   */ /* */
   IF ln_numrelatie IS NULL
   THEN
      pov_verwerkt := 'N' ;
      pov_foutmelding := 'Relatienummer kan niet worden gevonden met het gegeven persoonsnummer.' ;
   ELSE
      /* Zoek het huidige adres */
      OPEN c_ras_031( ln_numrelatie ) ;
      FETCH c_ras_031 INTO r_ras_031 ;

      IF c_ras_031%NOTFOUND
      THEN
         CLOSE c_ras_031 ;

         pov_verwerkt    := 'N' ;
         pov_foutmelding := 'Huidige adres niet gevonden. (Relatienummer ' || ln_numrelatie || ')' ;
      ELSE
         CLOSE c_ras_031 ;
         -- Vanaf hier wordt er een DA-adres aangemaakt, tenzij er niet met het GBA mag worden afgestemd;
         IF rle_chk_gba_afst( p_rle_numrelatie => ln_numrelatie
                            , p_persoonsnummer => NULL ) = 'N'
         THEN
           pov_verwerkt := 'J' ;
         ELSE
           --
           IF piv_landcode != 'NL'
           THEN
              OPEN c_ras_030( r_ras_031.id ) ;
              FETCH c_ras_030 INTO r_ras_030 ;
              CLOSE c_ras_030 ;

              /* Buitenlandse adressen met concatenatie vergelijken vanuit GBA */
              lv_adresregel1 := substr( r_ras_030.straatnaam
                                        || RTRIM( ' ' || r_ras_030.huisnummer )
                                        || RTRIM( ' ' || r_ras_030.toevhuisnum )
                                      , 1
                                      , 35
                                      ) ;
              lv_adresregel2 := substr( r_ras_030.postcode
                                        || RTRIM( ' ' || r_ras_030.woonplaats )
                                      , 1
                                      , 35
                                      ) ;
              lv_adresregel3 := substr( r_ras_030.provincie
                                        || RTRIM( ' ' || r_ras_030.naam )
                                      , 1
                                      , 35
                                      ) ;

              IF  UPPER( lv_adresregel1 ) = UPPER( piv_straatnaam )
              AND UPPER( lv_adresregel2 ) = UPPER( piv_woonplaats )
              AND UPPER( lv_adresregel3 ) = UPPER( piv_locatieomschrijving )
              THEN
                 pov_verwerkt := 'J' ;
              END IF ;
           END IF ;

           IF pov_verwerkt != 'J'
           THEN
              /* Vergelijk adres
              */
              ln_numadres := rle_verwerk_ads( piv_landcode
                                            , piv_postcode
                                            , pin_huisnummer
                                            , piv_huisnummer_toevoeging
                                            , piv_straatnaam
                                            , piv_woonplaats
                                            ) ;

              IF r_ras_031.ads_numadres != ln_numadres
              THEN
                 /* Adres is gewijzigd. Nieuw DA adres aanmaken.
                    Begindatum ligt op systeemdatum of als
                    relatie is overleden op overlijdensdatum.
                 */
                 open c_rle_018(ln_numrelatie);
                 fetch c_rle_018 into r_rle_018;
                 if c_rle_018%NOTFOUND then
                    ld_begindatum := SYSDATE;
                 else
                    if r_rle_018.datoverlijden is not null
                    then
                       ld_begindatum := r_rle_018.datoverlijden;
                    else
                       ld_begindatum := SYSDATE;
                    end if;
                 end if;
                 close c_rle_018;
                 ln_ras_id := rle_verwerk_ras( r_ras_031.id
                                             , ln_numrelatie
                                             , 'PR'
                                             , 'DA'
                                             , ld_begindatum
                                             , NULL
                                             , r_ras_031.ads_numadres
                                             , NULL
                                             , piv_locatieomschrijving
                                             , 'N'
                                             , 'N'
                                             , piv_landcode
                                             , piv_postcode
                                             , pin_huisnummer
                                             , piv_huisnummer_toevoeging
                                             , piv_straatnaam
                                             , piv_woonplaats
                                             ) ;
              ELSE
                 /* Adres updaten als soort adres al DA is.
                    DA adres ovoeren als alleen OA bestaat.
                    Cursor c_ras_031 bevat waarden OA adres
                    als er geen DA adres is.
                    Als begindatum moet "oude" datum worden
                    meegegeven.
                 */ /* */
                 ln_ras_id := rle_verwerk_ras( r_ras_031.id
                                             , ln_numrelatie
                                             , 'PR'
                                             , 'DA'
                                             , r_ras_031.datingang
                                             , NULL
                                             , r_ras_031.ads_numadres
                                             , NULL
                                             , piv_locatieomschrijving
                                             , 'N'
                                             , 'N'
                                             , piv_landcode
                                             , piv_postcode
                                             , pin_huisnummer
                                             , piv_huisnummer_toevoeging
                                             , piv_straatnaam
                                             , piv_woonplaats
                                             ) ;
              END IF ;

              pov_verwerkt := 'J' ;
           END IF ;
         END IF ;
      END IF;
   END IF ;

   /* Doorgave terugzetten */
   rle_indicatie.doorgeven_aan_gba := lv_oude_indicatie ;
EXCEPTION
 WHEN OTHERS THEN
     /* Reset indicatie */
   rle_indicatie.doorgeven_aan_gba := nvl( lv_oude_indicatie, 'J' ) ;

   /* uitvoer waarden */
   pov_verwerkt    := 'N' ;
   pov_foutmelding := sqlerrm ;
END RLE_GBA_WIJZIG_ADRES;
 
/