CREATE OR REPLACE PROCEDURE comp_rle.RLE_VERWERK_EX_PARTNERS
 (P_SCRIPT_NAAM IN varchar2
 ,P_SCRIPT_ID IN varchar
 ,P_BESTANDSNAAM IN varchar2
 ,P_AANTAL IN number
 )
 IS


cursor c_dln (b_bestandsnaam in varchar2
             ,b_aantal in number)
is
  select * from (select relatienummer
                       ,count(*) aantal
                 from rle_bron_ex_partners
                 where upper(bestandsnaam) = upper(b_bestandsnaam)
                   and ind_workflow_case_aangemaakt is null
                   and (relatienummer_ex is not null
                       or datum_verwerkt_gba_ex_geg is not null
                       or ind_gba_fout_dln = 'J')
                   and ind_verwerkt = 'N'
                 group by relatienummer
                 order by relatienummer)
                 where rownum <= b_aantal;

cursor c_bron (b_bestandsnaam in varchar2
              ,b_relatienummer in number)
is
  select *
  from rle_bron_ex_partners
  where upper(bestandsnaam) = upper(b_bestandsnaam)
    and relatienummer = b_relatienummer;

cursor c_bpl (b_bestandsnaam in varchar2
             ,b_relatienummer in number)
is
  select relatienummer
        ,count(*) aantal
  from rle_bron_ex_partners bron1
  where upper(bron1.bestandsnaam) = upper(b_bestandsnaam)
    and bron1.relatienummer = b_relatienummer
    and exists (select 1
                from rle_bron_ex_partners bron2
                where bron2.relatienummer = bron1.relatienummer
                  and bron1.bestandsnaam = bron2.bestandsnaam
                  and bron2.ind_verwerkt = 'N'
                  and bron2.ind_workflow_case_aangemaakt is null
                )
  group by relatienummer;

type t_fout_rec is record (bsn   varchar2(10)
                          ,dateinde_huwelijk date
                          ,fout varchar2(350));

type t_fout_tab is table of t_fout_rec index by pls_integer;

l_regelnr                   number := 0;
l_lijstnummer               number := 1;
l_volgnummer                stm_job_statussen.volgnummer%TYPE   := 0;
l_vorige_procedure          varchar2 (255);
l_huidige_procedure         varchar2 (255)                      := 'RLE_VERWERK_EX_PARTNERS';
l_sqlerrm                   varchar2 (255);
l_aantal                    number := 0;
l_aantal_bekend_rle         number := 0;
l_nog_te_verwerken          number := 0;
l_aantal_bsn                number := 0;
l_aantal_berichten          number := 0;
l_aantal_scheidingen        number := 0;
l_bsn_scheidingen           number := 0;
l_aantal_selectie           number := 0;
l_dln_fout                  number := 0;
l_ex_al_bekend              number := 0;
l_fout_verwerking           number := 0;
l_bsn                       varchar2(10);
l_dateinde                  date;
l_rkn_aanwezig              varchar2(1);
t_fout                      t_fout_tab;


e_fout_bestand              exception;
begin

  l_vorige_procedure        := stm_util.t_procedure;
  stm_util.t_procedure      := l_huidige_procedure;
  stm_util.t_programma_naam := 'RLE_VERWERK_EX_PARTNERS';
  stm_util.t_script_naam    := upper (p_script_naam);
  stm_util.t_script_id      := to_number (p_script_id);

  select count(*)
  into L_AANTAL
  from rle_bron_ex_partners
  where bestandsnaam = p_bestandsnaam;

  if l_aantal = 0
  then
    raise e_fout_bestand;
  end if;

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   rpad('-',100, '-')
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  'Aanmaken workflowcases tbv het vastleggen van ex-partners en/of relatiekoppelingen.'
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   rpad('-',100, '-')
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  'Start:'
  || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi')
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  ' '
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  rpad('-',100,'-')
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  ' '
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   'Selectie echtscheidingen bronbestand o.b.v. inputbestand: '||p_bestandsnaam);

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  ' '
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  rpad('=',100,'=')
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
  ' '
  );

  for r_dln in c_dln (b_bestandsnaam => p_bestandsnaam
                     ,b_aantal => p_aantal)
  loop
    begin

    stm_util.debug ('Bezig met deelnemer: '||r_dln.relatienummer);
    l_aantal_bsn := l_aantal_bsn + 1;
    l_aantal_selectie := l_aantal_selectie + r_dln.aantal;

    for r_bron in c_bron (b_bestandsnaam => p_bestandsnaam
                         ,b_relatienummer => r_dln.relatienummer)
    loop

      l_bsn_scheidingen := l_bsn_scheidingen + 1;
      l_dateinde := r_bron.huwelijk_datumeinde;
      l_bsn := r_bron.bsn;

      if r_bron.ind_gba_fout_dln = 'J'
      then
        l_dln_fout := l_dln_fout + 1;
      end if;

      if r_bron.relatienummer_ex is not null
      then
        l_ex_al_bekend := l_ex_al_bekend + 1;
      end if;

      if r_bron.relatienummer_ex is not null
      then

        rle_ex_partners.bestaat_rkn (p_rle_numrelatie_dln => r_bron.relatienummer
                                    ,p_rle_numrelatie_ex => r_bron.relatienummer_ex
                                    ,p_dat_begin => r_bron.huwelijk_datumbegin
                                    ,p_dat_einde => r_bron.huwelijk_datumeinde
                                    ,p_rkn_aanwezig => l_rkn_aanwezig
                                    );


        if l_rkn_aanwezig = 'J'
        then

          stm_util.debug ('relatiekoppeling gevonden met partner: '||r_bron.relatienummer_ex||' en einddatum: '||r_bron.huwelijk_datumeinde);

          update rle_bron_ex_partners
          set ind_verwerkt = 'J'
             ,ind_scheiding_alsnog_bekendrle = 'J'
             ,ind_workflow_case_aangemaakt = 'N'
          where intern_kenmerk = r_bron.intern_kenmerk;

          l_aantal_bekend_rle := l_aantal_bekend_rle + 1;

        else

          update rle_bron_ex_partners
          set ind_scheiding_alsnog_bekendrle = 'N'
          where intern_kenmerk = r_bron.intern_kenmerk;

        end if;

        else

          update rle_bron_ex_partners
          set ind_scheiding_alsnog_bekendrle = 'N'
          where intern_kenmerk = r_bron.intern_kenmerk;

      end if;
    end loop;

    for r_bpl in c_bpl (b_bestandsnaam => p_bestandsnaam
                       ,b_relatienummer => r_dln.relatienummer)
    loop
     stm_util.debug('bericht anmaken');
      rle_ex_partners.maak_bericht (p_bestandsnaam
                                   ,r_dln.relatienummer);

      update rle_bron_ex_partners
      set ind_workflow_case_aangemaakt = 'J'
      where relatienummer = r_bpl.relatienummer
        and upper(bestandsnaam) = upper(p_bestandsnaam)
        and ind_verwerkt = 'N'
        and ind_workflow_case_aangemaakt is null;

      l_aantal_scheidingen := l_aantal_scheidingen + r_bpl.aantal;
      l_aantal_berichten := l_aantal_berichten + 1;

    end loop;
    exception
    when others
      then

        l_fout_verwerking := l_fout_verwerking + 1;

        t_fout (l_fout_verwerking).bsn := l_bsn;
        t_fout (l_fout_verwerking).dateinde_huwelijk := l_dateinde;
        t_fout (l_fout_verwerking).fout := substr(sqlerrm,1,350);
    end;
  end loop;

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   'INPUT:'
  );

  stm_util.insert_lijsten
  (l_lijstnummer
  ,l_regelnr
  ,'Aantal echtscheidingen in bronbestand waarvan de GBA-gegevens van de ex zijn ontvangen en verwerkt: '||l_aantal_selectie
  );

  stm_util.insert_lijsten
  (l_lijstnummer
  ,l_regelnr
  ,'Aantal echtscheidingen in bronbestand waarvan de ex-en al bekend zijn in RLE: '||l_ex_al_bekend
  );

  stm_util.insert_lijsten
  (l_lijstnummer
  ,l_regelnr
  ,'Aantal echtscheidingen in bronbestand waarvan de deelnemers zijn gevonden in het GBA-bestand met fouten: '||l_dln_fout
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   ''
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   'OUTPUT:'
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   'Aantal echtscheidingen in bronbestand die al bekend zijn in RLE: '||l_aantal_bekend_rle
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   'Aantal echtscheidingen waarvoor workflow cases zijn aangemaakt: '||l_aantal_scheidingen
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   'Dit betreft '||l_aantal_berichten||' unieke workflow cases (1 per unieke deelnemer).'
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   ' '
  );

  select count(*)
  into l_nog_te_verwerken
  from rle_bron_ex_partners
  where upper(bestandsnaam) = upper(p_bestandsnaam)
    and ind_workflow_case_aangemaakt is null
    and (relatienummer_ex is not null
         or datum_verwerkt_gba_ex_geg is not null
         or ind_gba_fout_dln = 'J')
    and ind_verwerkt = 'N' ;

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   'Aantal echtscheidingen die niet zijn verwerkt i.v.m. het opgegeven maximum aantal workflow cases: '||l_nog_te_verwerken
  );

  stm_util.insert_lijsten
  (l_lijstnummer,
   l_regelnr,
   ''
  );

  if t_fout.count > 0
  then
    stm_util.insert_lijsten( l_lijstnummer
                           , l_regelnr
                           , 'De volgende echtscheidingen konden niet worden verwerkt vanwege een technische fout (aantal: '||l_fout_verwerking||').');

    for i in t_fout.first .. t_fout.last
    loop

      stm_util.insert_lijsten( l_lijstnummer
                             , l_regelnr
                             , 'Deelnemer BSN: '||t_fout(i).bsn||' _ Einddatum huwelijk: '||t_fout (i).dateinde_huwelijk||' _ Foutmelding: '
                             );

      stm_util.insert_lijsten( l_lijstnummer
                             , l_regelnr
                             , t_fout (i).fout);

    end loop;
  end if;

    stm_util.insert_lijsten( l_lijstnummer
                           , l_regelnr
                           , ''
                           );
    stm_util.insert_lijsten(1
                           ,l_regelnr
                           ,'Einde ' || stm_util.t_programma_naam
                           || ' op ' || to_char( sysdate, 'dd-mm-yyyy hh24:mi:ss')
                           );

  stm_util.insert_job_statussen(l_volgnummer, 'S', '0');
  stm_util.t_procedure := l_vorige_procedure;
exception
  when e_fout_bestand
  then
    rollback;
    stm_util.insert_job_statussen(l_volgnummer
                                 ,'S'
                                 ,'1'
                                   );
    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                 ' procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  'De opgegeven bestandsnaam ('||p_bestandsnaam||') is onbekend.');
    commit;

   when others
   then
     l_sqlerrm := substr (sqlerrm, 1, 180);
     l_huidige_procedure := substr (stm_util.t_procedure, 1, 180);
     rollback;
     stm_util.insert_job_statussen (l_volgnummer, 'S', '1');
     stm_util.insert_job_statussen (l_volgnummer, 'I', l_sqlerrm);
     commit;

end rle_verwerk_ex_partners;
 
/