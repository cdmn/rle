CREATE OR REPLACE PROCEDURE comp_rle.RLE_KVR_VERWERK
 (P_NUMRELATIE IN number
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN date
 ,P_CATEGORIE IN varchar2 := 'ALG'
 ,P_SRT_COM IN varchar2
 ,P_AUTHENTIEK IN varchar2 := 'N'
 )
 IS
  cursor c_kvr ( b_numrelatie         rle_kanaalvoorkeuren.rle_numrelatie%type
               , b_categorie          rle_communicatie_categorieen.code%type)
  is
select kvr.*, kvr.rowid
from   rle_communicatie_categorieen ccn
     , rle_communicatie_cat_kanalen cck
     , rle_kanaalvoorkeuren kvr
where  ccn.id = cck.ccn_id
and    cck.id = kvr.cck_id
and    kvr.rle_numrelatie = b_numrelatie
and    ccn.code = b_categorie
and    ccn.adm_code = nvl ( stm_context.administratie, 'ALG' )
and    nvl(kvr.dat_einde,sysdate) >= sysdate
order by kvr.dat_begin desc;


  cursor c_ccn ( b_categorie rle_communicatie_categorieen.code%type,
                               b_kanaal_voorkeur rle_communicatie_cat_kanalen.dwe_wrddom_srtcom%type )
  is
    select cck.id cck_id
    from rle_communicatie_categorieen ccn,
              rle_communicatie_cat_kanalen cck
    where  ccn.id = cck.ccn_id
    and    ccn.code = b_categorie
    and    ccn.adm_code = nvl ( stm_context.administratie, 'ALG' )
    and    cck.dwe_wrddom_srtcom = b_kanaal_voorkeur;

  r_kvr               c_kvr%rowtype;
  r_ccn               c_ccn%rowtype;
  authentiek          exception;
begin
  -- Haal laatste bekende voorkeur op voor de relatie/categorie
  open c_kvr ( p_numrelatie
             , p_categorie );

  fetch c_kvr
  into   r_kvr;

  if c_kvr%found
     and p_authentiek <> r_kvr.authentiek
     and r_kvr.authentiek = 'J'
  then
    -- Authentieke voorkeuren niet overschrijven
    raise authentiek;
  elsif c_kvr%found
  then
    -- Vorige voorkeur sluiten
    update rle_kanaalvoorkeuren
    set    dat_einde         =  case
                                when dat_begin > trunc ( p_dat_begin ) - 1
                                then
                                  dat_begin
                                else
                                  trunc ( p_dat_begin ) - 1
                                end
    where  rowid = r_kvr.rowid;
  end if;
  -- Voorkeur opvoeren
  open c_ccn(p_categorie,
             p_srt_com);
  fetch c_ccn into r_ccn;
  if c_ccn%notfound
  then
    close c_ccn;
    raise_application_error ( -20001, 'Kanaalvoorkeur '||p_srt_com||' bij Categorie '||p_categorie||' en administratie '||nvl ( stm_context.administratie, 'ALG' )||' niet toegestaan!' );
  end if;
  close c_ccn;

  begin
    insert into rle_kanaalvoorkeuren ( rle_numrelatie
                                     , dat_begin
                                     , cck_id
                                     , authentiek )
    values ( p_numrelatie
           , trunc ( p_dat_begin )
           , r_ccn.cck_id
           , p_authentiek );
    exception
    when DUP_VAL_ON_INDEX
    then
      -- Zelfde rij op zelfde dag weer actief maken
      update rle_kanaalvoorkeuren
      set    dat_einde = null
      where  dat_begin = trunc ( p_dat_begin )
      and    cck_id = r_ccn.cck_id
      and    rle_numrelatie = p_numrelatie;
  end;
  close c_kvr;
exception
  when authentiek
  then
    close c_kvr;

    raise_application_error ( -20001, 'De reeds bestaande voorkeur is authentiek en mag alleen door het portaal worden gewijzigd!' );
end;
 
/