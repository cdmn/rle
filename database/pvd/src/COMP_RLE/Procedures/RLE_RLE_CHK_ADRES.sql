CREATE OR REPLACE PROCEDURE comp_rle.RLE_RLE_CHK_ADRES
 (P_NUMRELATIE IN NUMBER
 ,P_RELATIE_ROL IN varchar2
 )
 IS
/******************************************************************************************************
  Naam:         RLE_RLE_CHK_ADRES
  Beschrijving: RAS Waarborgen minimum aantal adressen (BR ENT0011)

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  19-01-2006  NKU  Line 170, rollen Levensloop toegevoegd
  21-02-2008  PAS  Aanpassing ivm GBA aanpassing (Zorgketen cluster 1)
                   Indien er niet afgestemd mag worden met het GBA
                   bij rol 'PR' geen controle op DA adres maar op OA adres.
  15-09-2008 NKU Rol verzekeraar (VR) toegevoegd
  30-01-2009  XMB  Rollen WA, WB, WC en WD verwijderd.
  28-02-2012 JPG   Procesoptimalisatie NP. Bij ind opvoer gbav is geen OA adres verplicht
  15-07-2015 JCI   CHG 385543, Voor de volgende rollen geldt dat het adres niet verplicht is:
                   arbodienten (AD), Gevolmachtigden (GV), Tussenpersonen (TP) en Uitkeringsinstanties (UI).
 *******************************************************************************************************/
/* Ophalen rol */
CURSOR C_ROL_001
 (B_CODROL IN VARCHAR2
 )
 IS
/* C_ROL_001 */
SELECT	*
FROM	rle_rollen rol
WHERE	rol.codrol = b_codrol;
--
/* Ophalen van relatie-adres */
CURSOR C_RAS_003
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 ,B_SRTADRES IN VARCHAR2
 ,B_DATUMP IN DATE
 )
 IS
/* C_RAS_003 */
  SELECT ras.ads_numadres
  FROM	 RLE_RELATIE_ADRESSEN ras
  WHERE  ras.rle_numrelatie = b_numrelatie
  AND    NVL(ras.rol_codrol,'@') = NVL(b_codrol,'@')
  AND    ras.dwe_wrddom_srtadr = b_srtadres
  AND    ras.datingang <= b_datump
  AND    NVL(dateinde,TO_DATE('31-DEC-4712','DD-MON-YYYY')) >=
         NVL(b_datump,TO_DATE('31-DEC-4712','DD-MON-YYYY'));
--
-- Ophalen van relatie-adres
CURSOR C_RAS_004
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 ,B_DATUMP IN DATE
 )
 IS
  SELECT ras.ads_numadres
  FROM	 rle_relatie_adressen ras
  ,      rle_sadres_rollen ssl
  WHERE  ras.rle_numrelatie = b_numrelatie
  AND    ras.rol_codrol = b_codrol
  AND    ras.rol_codrol = ssl.codrol
  AND    ras.dwe_wrddom_srtadr = ssl.dwe_wrddom_sadres
  AND    ras.datingang <= b_datump
  AND    NVL(dateinde,TO_DATE('31-DEC-4712','DD-MON-YYYY')) >=
         NVL(b_datump,TO_DATE('31-DEC-4712','DD-MON-YYYY'));
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
SELECT numrelatie
,      datoverlijden
,      datbegin
,      dateinde
FROM	rle_relaties rle
WHERE	rle.numrelatie = b_numrelatie;
--
CURSOR C_RRL_001
 (B_NUMRELATIE IN NUMBER
 ,B_ROL_CODROL IN VARCHAR2
 )
 IS
/* C_RRL_001 */
SELECT  rrl.rle_numrelatie
       ,rrl.rol_codrol
FROM	rle_relatie_rollen rrl
WHERE   rrl.rle_numrelatie = b_numrelatie
AND     rrl.rol_codrol = b_rol_codrol;
-- Program Data
r_rle_001      c_rle_001%rowtype;
r_ras_003      c_ras_003%rowtype;
r_ras_004      c_ras_004%rowtype;
r_rol_001      c_rol_001%rowtype;
r_rrl_001      c_rrl_001%rowtype;
l_soort_adres  varchar2(30);
l_codrol       varchar2(2);
l_foutmelding  varchar2(100);
l_ras_notfound boolean;
l_ind_opvoer_gbav varchar2(1) := 'N'; -- JPG 28-02-2012
-- Sub-Program Units
-- PL/SQL Block
BEGIN
   IF p_relatie_rol in ('CP','AD','GV', 'TP', 'UI')
   THEN
      /* een contactpersoon heeft geen adres */
      NULL;
   ELSE
      /* Ophalen relatiegegevens */
      OPEN c_rle_001( p_numrelatie );
      FETCH c_rle_001 INTO r_rle_001;
      CLOSE c_rle_001;
      --
      IF p_relatie_rol IN ( 'PR', 'BG', 'WN')
      THEN
         IF (     p_relatie_rol = 'PR'
              AND r_rle_001.datoverlijden IS NULL
            )
         THEN
            /* Controles persoon: bij rol PR is DA of OA adres verplicht       */
            /* DA is verplicht indien er afgestemd wordt met het GBA           */
            /* OA is verplicht indien er niet afgestemd mag worden met het GBA */
            IF rle_chk_gba_afst( p_rle_numrelatie => r_rle_001.numrelatie
                               , p_persoonsnummer => NULL
                               ) = 'J'
            THEN
              l_codrol := 'DA';
              l_foutmelding := 'RLE-10304 #1persoon#2domicilie';
            ELSE
              l_codrol := 'OA';
              l_foutmelding := 'RLE-10304 #1persoon die niet mag worden afgestemd#2opgegeven';
              --
              -- JPG 28-02-2012
              --
              RLE_GBA_GET_IND_OPVOER_GBAV(PIN_PERSOONSNUMMER => NULL
                                         ,PIN_NUMRELATIE => r_rle_001.numrelatie
                                         ,POV_IND_OPVOER_GBAV => l_ind_opvoer_gbav);
            END IF;

            OPEN c_ras_003( r_rle_001.numrelatie
                          , p_relatie_rol
                          , l_codrol
                          , TRUNC( SYSDATE )
                          );
            FETCH c_ras_003 INTO r_ras_003;
            l_ras_notfound := c_ras_003%NOTFOUND;
            CLOSE c_ras_003;
            IF l_ras_notfound
            AND l_ind_opvoer_gbav = 'N' -- JPG 28-02-2012
            THEN
               stm_dbproc.raise_error( l_foutmelding );
            END IF ;
            --
            /* Zoeken of ook rol WN voorkomt, zoja dan is ook OA adres verplicht */
            OPEN c_rrl_001( r_rle_001.numrelatie
                          , 'WN');
            FETCH c_rrl_001 INTO r_rrl_001;
            IF c_rrl_001%FOUND
            THEN
               OPEN c_ras_003( r_rle_001.numrelatie
                             , p_relatie_rol
                             , 'OA'
                             , TRUNC( SYSDATE )
                             );
               FETCH c_ras_003 INTO r_ras_003;
               IF c_ras_003%NOTFOUND
               THEN
                  CLOSE c_ras_003;
                  CLOSE c_rrl_001;
                  stm_dbproc.raise_error( 'RLE-10304 #1persoon#2opgegeven'||r_rle_001.numrelatie );
			         ELSE
			            CLOSE c_ras_003;
               END IF;
            ELSE
			         CLOSE c_rrl_001;
			      END IF;
            --
         END IF;
      ELSIF p_relatie_rol = 'WG'
      THEN
          /* Controle werkgever: bij rol WG is SZ adres verplicht*/
          OPEN c_ras_003( r_rle_001.numrelatie
                         , p_relatie_rol
                         , 'SZ'
                         , NVL( r_rle_001.dateinde, SYSDATE )
                         );
          FETCH c_ras_003 INTO r_ras_003;
          IF c_ras_003%NOTFOUND
          THEN
             CLOSE c_ras_003;
             stm_dbproc.raise_error( 'RLE-10304 #1werkgever#2statutair zetel' );
		      ELSE
		         CLOSE c_ras_003;
          END IF ;
      ELSIF p_relatie_rol in ( 'VN', 'VZ' )
      THEN
	     --
         /* JWB 14-10-2004 Leuk die tabel RLE_SADRES_ROLLEN. Maar gebruik hem ook voortaan */
         /* voor de WAZ rollen doen we dat wel. Alle andere rollen zouden ook op die manier moeten, ooit */
         /* EZW 25-10-2004 OK, maar gebruik hem dan wel goed. Nieuwe cursor ras004 gemaakt. */
	 /* NKU  19-01-2006 Rollen van Levensloop toegevoegd WA, WB, WC en WD */
         OPEN c_ras_004( r_rle_001.numrelatie
                        , p_relatie_rol
                        , NVL( r_rle_001.dateinde, SYSDATE )
                        );
         FETCH c_ras_004 INTO r_ras_004;
         IF c_ras_004%NOTFOUND
         THEN
            CLOSE c_ras_004;
            --
            OPEN c_rol_001( p_relatie_rol );
            FETCH c_rol_001 INTO r_rol_001;
            CLOSE c_rol_001;
            --
            stm_dbproc.raise_error( 'RLE-10304 #1' || LOWER( r_rol_001.omsrol ));
		     ELSE
		        CLOSE c_ras_004;
         END IF;
      ELSIF p_relatie_rol in ('AK', 'CR','BW','GE','HC','BA','BO','VK','FO')
	    THEN
	     --
         /* PWA 19-11-2004 Aparte tak gemaakt voor overige relaties,
		                   omdat anders komplete relaties niet beéindigd kunnen worden.
						   Close v. alle cursoren in een ELSE-tak gezet i.p.v. achter de END IF */
         OPEN c_ras_003( r_rle_001.numrelatie
                        , p_relatie_rol
                        /* JWB: NVL op soort_adres toegevoegd) */
                        , 'FA'
                        , NVL( r_rle_001.dateinde, SYSDATE )
                        );
         FETCH c_ras_003 INTO r_ras_003;
         IF c_ras_003%NOTFOUND
         THEN
            CLOSE c_ras_003;
            --
            OPEN c_rol_001( p_relatie_rol );
            FETCH c_rol_001 INTO r_rol_001;
            CLOSE c_rol_001;
            --
		     ELSE
		        CLOSE c_ras_003;
         END IF;

      ELSIF p_relatie_rol in ('VR') -- Rol Verzekeraar!
	    THEN
	     --
         /* PWA 19-11-2004 Aparte tak gemaakt voor pensioenverzekeraars
		                   De pensioenverzekeraars worden in WOT (Waardeoverdrachten) geadministreerd.
						   */
         OPEN c_ras_003( r_rle_001.numrelatie
                        , p_relatie_rol
                        , 'CA'
                        , NVL( r_rle_001.dateinde, SYSDATE )
                        );
         FETCH c_ras_003 INTO r_ras_003;
         IF c_ras_003%NOTFOUND
         THEN
            CLOSE c_ras_003;
            --
            OPEN c_rol_001( p_relatie_rol );
            FETCH c_rol_001 INTO r_rol_001;
            CLOSE c_rol_001;
            --
		     ELSE
		        CLOSE c_ras_003;
         END IF;
      ELSE
         OPEN c_ras_003( r_rle_001.numrelatie
                        , p_relatie_rol
                        /* JWB: NVL op soort_adres toegevoegd) */
                        , 'FA'
                        , NVL( r_rle_001.dateinde, SYSDATE )
                        );
         FETCH c_ras_003 INTO r_ras_003;
         IF c_ras_003%NOTFOUND
         THEN
            CLOSE c_ras_003;
            --
            OPEN c_rol_001( p_relatie_rol );
            FETCH c_rol_001 INTO r_rol_001;
            CLOSE c_rol_001;
            --
            stm_dbproc.raise_error( 'RLE-10304 #1' || LOWER( r_rol_001.omsrol ) || '#2feitelijk' );
		     ELSE
		        CLOSE c_ras_003;
         END IF;
      END IF;
   END IF;
END Rle_Rle_Chk_Adres;
/