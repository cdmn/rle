CREATE OR REPLACE PROCEDURE comp_rle.RLE_RRL_CHK_DATSCHON
 (P_NEW_RRL_RLE_NUMRELATIE NUMBER
 ,P_NEW_RRL_ROL_CODROL VARCHAR2
 ,P_NEW_RRL_DATSCHONING DATE
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_RAS_012
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_RAS_012 */
select	*
from	rle_relatie_adressen ras
where	ras.rle_numrelatie = b_numrelatie
and	nvl(ras.rol_codrol,'@') = nvl(b_codrol,'@');
/* Ophalencommunicatienummer op rol */
CURSOR C_CNR_004
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_CNR_004 */
select	*
from	rle_communicatie_nummers cnr
where	cnr.rle_numrelatie = b_numrelatie
and	cnr.rol_codrol = b_codrol;
/* Ophalen van belangen per relatie */
CURSOR C_FNR_005
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_FNR_005 */
select	*
from	rle_financieel_nummers fnr
where	fnr.rle_numrelatie = b_numrelatie
and	fnr.rol_codrol = b_codrol;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* rle_rrl_chk_datschon */
  /* invullen/wijzigen van de schoningsdatum IS alleen toegestaan als de
  einddatum gevuld IS IN alle gerelateerde gebruikrelaties*/
/* RWI: gebruik_relaties wordt niet meer gebruikt
  FOR r_gre_002 IN c_gre_002(p_new_rrl_rle_numrelatie
  ,	p_new_rrl_rol_codrol) LOOP
  	IF	r_gre_002.dateinde IS NULL
  	OR r_gre_002.dateinde > p_new_rrl_datschoning
  	THEN
  		stm_dbproc.raise_error('RLE-000420',
  		'#1'||p_new_rrl_rle_numrelatie||
     		'#2 gebruik relaties',
  		'rle_rrl_chk_datschon');
  	END IF;
  END LOOP;
  */
  /* invullen/wijzigen van de schoningsdatum IS alleen toegestaan als de
einddatum
  van alle gerelateerde relatieadressen = NULL OF <= schoningsdatum van de
  relatie*/
  FOR r_ras_012 IN c_ras_012 (p_new_rrl_rle_numrelatie
  ,	p_new_rrl_rol_codrol) LOOP
  	IF	r_ras_012.dateinde IS NULL
  	OR	r_ras_012.dateinde > p_new_rrl_datschoning THEN
  		stm_dbproc.raise_error('RLE-000420',
  		'#1'||p_new_rrl_rle_numrelatie||
     		'#2 Relatie adressen',
  		'rle_rrl_chk_datschon');
  	END IF;
  END LOOP;
  /* invullen/wijzigen van de schoningsdatum is alleen toegestaan als de
einddatum
  van alle gerelateerde communicatienummers = NULL of <= schoningsdatum van de
  relatie */
  FOR r_cnr_004 IN c_cnr_004 (p_new_rrl_rle_numrelatie
  ,	p_new_rrl_rol_codrol) LOOP
  	IF	r_cnr_004.dateinde IS NULL
  	OR	r_cnr_004.dateinde > p_new_rrl_datschoning THEN
  		stm_dbproc.raise_error('RLE-000420',
  		'#1'||p_new_rrl_rle_numrelatie||
    		 '#2 Communicatie nummers',
  		'rle_rrl_chk_datschon');
  	END IF;
  END LOOP;
/* invullen/wijzigen van de schoningsdatum IS alleen toegestaan als de
 einddatum van alle gerelateerde financieelnummers = NULL of <= schoningsdatum
  van de relatie
*/
  FOR r_fnr_005 IN c_fnr_005 (p_new_rrl_rle_numrelatie
  ,	p_new_rrl_rol_codrol) LOOP
  	IF	r_fnr_005.dateinde IS NULL
  	OR	r_fnr_005.dateinde > p_new_rrl_datschoning THEN
  		stm_dbproc.raise_error('RLE-000420',
  		'#1'||p_new_rrl_rle_numrelatie||
  	   	'#2 Financieel nummers',
  		'rle_rrl_chk_datschon');
  	END IF;
  END LOOP;
END RLE_RRL_CHK_DATSCHON;

 
/