CREATE OR REPLACE PROCEDURE comp_rle.RLE_CRE_AVH_ZONDER_BEROEP
 (P_AVG_ID IN number
 ,P_DAT_BEGIN IN date
 ,P_DAT_EINDE IN date
 ,P_SUCCES OUT boolean
 ,P_ERRORCODE OUT varchar2
 )
 IS

l_errorcode varchar2(2000);
begin
  -- doe de verwerking o.b.v. ingekomen parameters
  rle_rie.kop_avg_avg(p_avg_id      => p_avg_id
		         ,p_dat_begin   => p_dat_begin
                     ,p_dat_einde   => p_dat_einde
                     ,p_code_beroep => null
                     ,p_error       => l_errorcode
                     );
  --
  if l_errorcode is null
  then
     p_succes := true;
  else
    p_errorcode := l_errorcode;
    p_succes := false;
  end if;

-- Stop alleen een when others exception handler in de listener en niet in de verwerkende procedure(s).
-- In geval van een onverwachte fout zal stm_bgproc.error_handler de backtrace afdrukken
-- en die ben je kwijt na een doorlopen exception.
-- Dat betekent verlies van nuttige informatie over de echte fout.
--exception
--  when others
END RLE_CRE_AVH_ZONDER_BEROEP;
 
/