CREATE OR REPLACE PROCEDURE comp_rle.RLE_OFE_CRE_RELATIE
 (P_NAAM IN VARCHAR2
 ,P_ZOEKNAAM IN VARCHAR2
 ,P_VOORLETTER IN VARCHAR2
 ,P_GEBOORTEDATUM IN DATE
 ,P_VOORVOEGSELS IN VARCHAR2
 ,P_GESLACHT IN VARCHAR2
 ,P_RELNUM IN OUT NUMBER
 )
 IS
-- PL/SQL Specification
cursor c_relnum
is
select rle_rle_seq1.nextval
from   dual
;
--
l_errmsg varchar2(2000);
--
-- PL/SQL Block
begin
  open c_relnum;
  fetch c_relnum into p_relnum;
  --
  insert into RLE_RELATIES
  (
    NUMRELATIE,
    INDGEWNAAM,
    INDINSTELLING,
    INDCONTROLE,
    CODORGANISATIE,
    NAMRELATIE,
    ZOEKNAAM,
    VOORLETTER,
    DATGEBOORTE,
    DWE_WRDDOM_VVGSL,
    DWE_WRDDOM_GESLACHT
  )
  values
  ( p_relnum
    , 'N'
    , 'N'
    , 'N'
    , 'A'
    , p_naam
    , p_zoeknaam
    , p_voorletter
    , p_geboortedatum
    , upper(replace(p_voorvoegsels, ' ', ''))
    , p_geslacht
  );
  --
  insert into RLE_RELATIE_ROLLEN
  (
    RLE_NUMRELATIE,
    ROL_CODROL,
    CODORGANISATIE
  )
  values
  (
    p_relnum
  , 'WZ'
  , 'A'
  );
  --
exception
  when others
  then
    l_errmsg := sqlerrm;
    raise_application_error(-20001, l_errmsg);
end;

 
/