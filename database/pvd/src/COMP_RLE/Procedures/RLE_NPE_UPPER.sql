CREATE OR REPLACE PROCEDURE comp_rle.RLE_NPE_UPPER
 IS
-- Sub-Program Unit Declarations
/* Totaal alle ned postcodes */
CURSOR C_NPE_006_C
 IS
/* 	C_PCOD06 */
select count(*)
from rle_ned_postcode 	npe
;
/* Alle ned postcodes */
CURSOR C_NPE_006
 IS
/* 	C_PCOD06 */
select npe.postcode
,		 npe.woonplaats
,		 rowid
from rle_ned_postcode 	npe
;
-- Program Data
L_AANTAL NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_NPE_UPPER */
stm_batch.init_batch('RLE_NPE_UPPER');
open c_npe_006_c;
fetch c_npe_006_c into l_aantal;
close c_npe_006_c;
STM_BATCH.LOG_TEVERWERKEN(l_aantal);
for r_npe_006 in c_npe_006 loop
  begin
    update rle_ned_postcode
    set woonplaats = LTRIM(RTRIM(upper(r_npe_006.woonplaats)))
    where rowid = r_npe_006.rowid;
  end;
end loop;
stm_batch.einde_batch('J');
exception
   when others then
     	  ROLLBACK;
        stm_batch.log_fout('rle_npe_upper');
        stm_batch.einde_batch('N');
END RLE_NPE_UPPER;

 
/