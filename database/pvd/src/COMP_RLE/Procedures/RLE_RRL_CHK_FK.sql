CREATE OR REPLACE PROCEDURE comp_rle.RLE_RRL_CHK_FK
 (P_RRL_RLE_NUMRELATIE IN NUMBER
 ,P_RRL_ROL_CODROL IN VARCHAR2
 ,P_LOCK_RECORD IN BOOLEAN := FALSE
 ,P_PASS_EXCEPTIONS IN BOOLEAN := FALSE
 )
 IS
-- Program Data
E_RESOURCE_BUSY EXCEPTION;
PRAGMA EXCEPTION_INIT(E_RESOURCE_BUSY, -54);
L_DUMMY NUMBER(1);
-- PL/SQL Block
BEGIN
if p_lock_record
    then
      select 1
      into   l_dummy
      from   rle_relatie_rollen rrol1
      where  rrol1.rle_numrelatie =
             p_rrl_rle_numrelatie
      and    rrol1.rol_codrol =
             p_rrl_rol_codrol
      for update of rol_codrol
      nowait
      ;
    else
      select 1
      into   l_dummy
      from   rle_relatie_rollen rrol1
      where  rrol1.rle_numrelatie =
             p_rrl_rle_numrelatie
      and    rrol1.rol_codrol =
             p_rrl_rol_codrol
      ;
    end if;
  exception
    when no_data_found
    then
      if p_pass_exceptions
      then
        raise no_data_found;
      else
        stm_dbproc.raise_error
       ('rle_rrl_ntex1'
        ,to_char(p_rrl_rle_numrelatie)
        ||'##'||p_rrl_rol_codrol
        );
      end if;
    when e_resource_busy
    then
      if p_pass_exceptions
      then
        raise e_resource_busy;
      else
        stm_dbproc.raise_error
        ('rle_rrl_rsbs1'
        ,to_char(p_rrl_rle_numrelatie)
        ||'##'||p_rrl_rol_codrol
        );
      end if;
END RLE_RRL_CHK_FK;

 
/