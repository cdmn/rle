CREATE OR REPLACE PROCEDURE comp_rle.RLE_CHK_WERKGEVER
 (P_NAAM_WERKGEVER IN VARCHAR2
 ,P_HANDELSNAAM IN VARCHAR2
 ,P_POSTCODE IN VARCHAR2
 ,P_HUISNUMMER IN NUMBER
 ,P_IND_AANWEZIG OUT VARCHAR
 ,P_RELATIENUMMER OUT NUMBER
 ,P_WERKGEVERSNUMMER OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_RLE_026
 (B_HANDELSNAAM IN VARCHAR2
 )
 IS
SELECT numrelatie
FROM rle_relaties
WHERE handelsnaam LIKE (b_handelsnaam||'%')
;
/* referenties die van dat systeem komen bepalen */
CURSOR C_REC_014
 (B_NUMRLE NUMBER
 ,B_CODROL VARCHAR2
 ,B_SYSTEEM VARCHAR2
 )
 IS
/* C_REC_014 */
select id, extern_relatie
from rle_relatie_externe_codes
where rle_numrelatie  = b_numrle
and rol_codrol        = upper(b_codrol)
and dwe_wrddom_extsys = upper(b_systeem);
/* Relatie adressen bij een numadres */
CURSOR C_RAS_018
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_RAS_018 */
select *
from	rle_relatie_adressen ras
where	ras.ads_numadres = b_numadres
;
CURSOR C_RLE_025
 (B_NAAM IN VARCHAR2
 )
 IS
SELECT numrelatie
FROM rle_relaties
WHERE zoeknaam LIKE UPPER(b_naam||'%')
;
CURSOR C_ADS_018
 (B_POSTCODE IN VARCHAR
 ,B_HUISNUMMER IN NUMBER
 )
 IS
select numadres
from rle_adressen
where postcode = b_postcode
and huisnummer =b_huisnummer
;
-- Program Data
L_HANDELSNAAM VARCHAR2(20);
R_RAS_018 C_RAS_018%ROWTYPE;
L_ID NUMBER(9);
L_WGR_NAAM VARCHAR2(20);
L_NUMADRES NUMBER(7);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_CHK_WERKGEVER */
/* Controle of een werkgever al voorkomt binnen de administratie van MN Services. */
/* Zoeken op postcode en huisnummer in ADRES naar het Nr adres. */
l_numadres := NULL;
p_relatienummer := NULL;
p_werkgeversnummer := NULL;
p_ind_aanwezig := 'N';
OPEN  c_ads_018(p_postcode, p_huisnummer);
FETCH c_ads_018 INTO l_numadres;
CLOSE c_ads_018;
/* Met dit Nr adres wordt IN RELATIE ADRES gezocht.*/
IF l_numadres IS NOT NULL
THEN
   OPEN  c_ras_018(l_numadres);
   FETCH c_ras_018 INTO r_ras_018;
   CLOSE c_ras_018;
END IF;
l_wgr_naam    := SUBSTR(p_naam_werkgever, 1, 10);
l_handelsnaam := SUBSTR(p_handelsnaam, 1, 10);
IF r_ras_018.id IS NOT NULL
THEN
   OPEN  c_rle_025(l_wgr_naam);
   FETCH c_rle_025 INTO p_relatienummer;
   CLOSE c_rle_025;
END IF;
IF p_relatienummer IS NULL
THEN
   OPEN  c_rle_026(l_handelsnaam);
   FETCH c_rle_026 INTO p_relatienummer;
   CLOSE c_rle_026;
END IF;
IF p_relatienummer IS NOT NULL
THEN
   OPEN  c_rec_014(p_relatienummer, 'WG', 'WGR');
   FETCH c_rec_014 INTO l_id, p_werkgeversnummer;
   CLOSE c_rec_014;
END IF;
IF p_relatienummer IS NOT  NULL
AND p_werkgeversnummer IS NOT NULL
THEN
   p_ind_aanwezig := 'J';
END IF;
END RLE_CHK_WERKGEVER;

 
/