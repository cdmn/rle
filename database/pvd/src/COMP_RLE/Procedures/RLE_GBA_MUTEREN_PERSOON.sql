CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_MUTEREN_PERSOON
 (PIN_NUMRELATIE IN NUMBER
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
--select	*
select numrelatie
,      codorganisatie
,      namrelatie
,      zoeknaam
,      indgewnaam
,      indinstelling
,      datschoning
,      dwe_wrddom_titel
,      dwe_wrddom_ttitel
,      dwe_wrddom_avgsl
,      dwe_wrddom_vvgsl
,      dwe_wrddom_brgst
,      dwe_wrddom_geslacht
,      dwe_wrddom_gewatitel
,      dwe_wrddom_rvorm
,      voornaam
,      voorletter
,      aanschrijfnaam
,      datgeboorte
,      code_gebdat_fictief
,      datoverlijden
,      datoprichting
,      datbegin
,      datcontrole
,      dateinde
,      indcontrole
,      regdat_overlbevest
,      datemigratie
,      naam_partner
,      vvgsl_partner
,      code_aanduiding_naamgebruik
,      handelsnaam
,      dat_creatie
,      creatie_door
,      dat_mutatie
,      mutatie_door
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Program Data
R_RLE_001 C_RLE_001%ROWTYPE;
LV_FOUTMELDING VARCHAR2(4000);
LV_RAISE_ERROR VARCHAR2(1) := 'N';
LV_ROL VARCHAR2(2);
LV_VOORVOEGSELS VARCHAR2(40);
LN_PERSOONSNUMMER NUMBER;
LV_VERWERKT VARCHAR2(1);
LN_SOFINUMMER NUMBER;
LV_GBA_STATUS VARCHAR2(2);
LN_ANUMMER NUMBER;
LV_GBA_AFNEMERS_IND VARCHAR2(1);
LV_VVGSL_PARTNER VARCHAR2( 100 );
LN_MAX_LENGTE NUMBER := 40;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
stm_util.debug('RLE_GBA_MUTEREN_PERSOON');
   /* Alleen doorgeven als het nodig is */
   IF rle_indicatie.doorgeven_aan_gba = 'N'
   THEN
      RETURN ;
   END IF ;
   /* Bepalen rol van de nieuwe relatie */
   rle_gba_bepaal_rol( pin_numrelatie
                     , lv_rol
                     ) ;
   /* Doorgeven hoeft alleen maar voor personen */
   IF lv_rol IS NULL
   THEN
      RETURN ;
   END IF ;
   /* Ophalen gegevens van relatie */
   OPEN c_rle_001( pin_numrelatie ) ;
   FETCH c_rle_001 INTO r_rle_001 ;
   CLOSE c_rle_001 ;
   ln_persoonsnummer := rle_m03_rec_extcod( 'PRS'
                                          , pin_numrelatie
                                          , 'PR'
                                          , SYSDATE
                                          ) ;
   ln_sofinummer := rle_m03_rec_extcod( 'SOFI'
                                      , pin_numrelatie
                                      , 'PR'
                                      , SYSDATE
                                      ) ;
   ln_anummer := rle_m03_rec_extcod( 'GBA'
                                   , pin_numrelatie
                                   , 'PR'
                                   , SYSDATE
                                   ) ;
   ibm.ibm( 'RFW_GET_WRD'
          , 'RLE'
          , 'VVL'
          , r_rle_001.dwe_wrddom_vvgsl
          , lv_raise_error
          , ln_max_lengte
          , lv_voorvoegsels
          ) ;
   ibm.ibm( 'RFW_GET_WRD'
          , 'RLE'
          , 'VVL'
          , r_rle_001.vvgsl_partner
          , lv_raise_error
          , ln_max_lengte
          , lv_vvgsl_partner
          ) ;
   /* Aanroep GBA dienst */
   ibm.ibm( 'GBA_MUTEREN_PERSOON'
          , 'RLE'
          , ln_persoonsnummer                -- PIN_PERSOONSNUMMER
          , ln_sofinummer                    -- PIN_SOFINUMMER
          , ln_anummer                       -- PIN_A_NUMMER
          , r_rle_001.namrelatie             -- PIV_NAAM
          , r_rle_001.voorletter             -- PIV_VOORLETTERS
          , lv_voorvoegsels                  -- PIV_VOORVOEGSELS
          , r_rle_001.datgeboorte            -- PID_GEBOORTEDATUM
          , r_rle_001.dwe_wrddom_geslacht    -- PIN_GESLACHT
          , r_rle_001.datoverlijden          -- PID_OVERLIJDENSDATUM
          , r_rle_001.regdat_overlbevest     -- PID_REGDAT_OVERLIJDEN
          , r_rle_001.datemigratie           -- PID_EMIGRATIEDATUM
          , r_rle_001.naam_partner           -- PIV_NAAM_PARTNER
          , lv_vvgsl_partner                 -- PIV_VVGSL_PARTNER
          , lv_verwerkt                      -- POV_VERWERKT
          , lv_foutmelding                   -- POV_FOUTMELDING
          , lv_gba_status                    -- POV_GBA_STATUS
          , lv_gba_afnemers_ind              -- POV_AFNEMERS_INDICATIE
          ) ;
   stm_util.debug ('pin_numrelatie: '||pin_numrelatie);
   stm_util.debug ('lv_gba_status: '||lv_gba_status);
   stm_util.debug ('lv_gba_afnemers_ind: '||lv_gba_afnemers_ind);
   IF NVL( lv_verwerkt, 'N' ) != 'J'
   THEN
      IF    INSTR( lv_foutmelding, 'ORA-20000: ' ) = 1
      THEN
         stm_dbproc.raise_error( SUBSTR( lv_foutmelding, 12 ) ) ;
      ELSIF INSTR( lv_foutmelding, 'ORA-' ) = 1
      THEN
         stm_dbproc.raise_error( 'RLE-10313 #1Muteren persoon #2' || SUBSTR( lv_foutmelding, 12 ) ) ;
      ELSE
         stm_dbproc.raise_error( 'RLE-10313 #1Muteren persoon #2' || lv_foutmelding ) ;
      END IF ;
   END IF ;
   rle_gba_wijzig_gba( pin_numrelatie
                     , lv_gba_status
                     , lv_gba_afnemers_ind
                     , NULL
                     ) ;
END RLE_GBA_MUTEREN_PERSOON;

 
/