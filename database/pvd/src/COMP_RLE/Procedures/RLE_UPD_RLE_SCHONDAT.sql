CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPD_RLE_SCHONDAT
 (P_NUMRELATIE rle_relaties.numrelatie%TYPE
 ,P_N_DATSCHONING rle_relaties.datschoning%TYPE
 )
 IS
BEGIN
/* RLE_UPD_RLE_SCHONDAT */
UPDATE	rle_relaties
SET	datschoning	=  p_n_datschoning
WHERE	numrelatie	=  p_numrelatie
AND	datschoning	<= p_n_datschoning;
END RLE_UPD_RLE_SCHONDAT;

 
/