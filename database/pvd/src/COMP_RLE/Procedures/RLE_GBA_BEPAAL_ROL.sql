CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_BEPAAL_ROL
 (PIN_NUMRELATIE IN NUMBER
 ,POV_ROL OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_RRL_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RRL_001 */
select	*
from	rle_relatie_rollen rrl
where	rrl.rle_numrelatie = b_numrelatie;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default returnwaarde
    */
   pov_rol := NULL ;
   FOR i_rrl_001 IN c_rrl_001( pin_numrelatie )
   LOOP
      /* De rol PR is de laagste prioriteit
      */
      IF    i_rrl_001.rol_codrol = 'PR'
      AND   (   pov_rol NOT IN ( 'BG', 'WN' )
            OR  pov_rol IS NULL
            )
      THEN
         pov_rol := 'PR' ;
      /* De rol BG (Begunstigde) gaat boven PR maar
         de rol DN (Deelnemer) is belangrijker
      */
      ELSIF i_rrl_001.rol_codrol = 'BG'
      AND   (   pov_rol NOT IN ( 'WN' )
            OR  pov_rol IS NULL
            )
      THEN
         pov_rol := 'BG' ;
      /* De rol DN (Deelnemer) gaat boven alles
      */
      ELSIF i_rrl_001.rol_codrol = 'WN'
      THEN
         pov_rol := 'WN' ;
      END IF ;
   END LOOP ;
END RLE_GBA_BEPAAL_ROL;

 
/