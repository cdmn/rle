CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_ADRES
 (P_NUMADRES IN NUMBER
 ,P_POSTCODE IN OUT VARCHAR2
 ,P_HUISNR IN OUT number
 ,P_TOEVNUM IN OUT VARCHAR2
 ,P_WOONPLAATS IN OUT varchar2
 ,P_STRAAT IN OUT varchar2
 ,P_LAND IN OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van adresvelden opbasis van id */
CURSOR C_ADS_011
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_ADS_011 */
select	*
from	rle_adressen ads
where	ads.numadres = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_WPS_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_WPS_001 */
select	wps.woonplaats
from	rle_woonplaatsen wps
,	rle_adressen ads
where	wps.woonplaatsid = ads.wps_woonplaatsid
and	numadres = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_STT_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_STT_001 */
select	stt.straatnaam
from	rle_straten stt
,	rle_adressen ads
where	stt.straatid  = ads.stt_straatid
and	ads.numadres  = b_numadres;
-- Program Data
R_ADS_011 C_ADS_011%ROWTYPE;
R_WPS_001 C_WPS_001%ROWTYPE;
R_STT_001 C_STT_001%ROWTYPE;
L_CODNL VARCHAR2(30);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_GET_ADRES */
open 	c_ads_011( p_numadres);
fetch	c_ads_011   into  r_ads_011;
close c_ads_011;
p_land := r_ads_011.lnd_codland;
p_huisnr := r_ads_011.huisnummer;
p_toevnum := r_ads_011.toevhuisnum;
p_postcode := r_ads_011.postcode;
-- bepaal landcode nederland
l_codnl := stm_algm_get.sysparm(NULL,'LAND',sysdate);
if p_land = l_codnl
then
   rle_get_wps_stt(r_ads_011.postcode
	 , r_ads_011.huisnummer
 	 , p_woonplaats
	 , p_straat);
else
   open c_wps_001( p_numadres);
   fetch c_wps_001 into r_wps_001;
   close c_wps_001;
   p_woonplaats := r_wps_001.woonplaats;
   open c_stt_001( p_numadres);
   fetch c_stt_001 into r_stt_001;
   close c_stt_001;
   p_straat :=  r_stt_001.straatnaam;
end if;
END RLE_GET_ADRES;

 
/