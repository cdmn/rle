CREATE OR REPLACE PROCEDURE comp_rle.RLE_STT_WIJZIG
 (P_WOONPLAATSID NUMBER
 ,P_STRAATNAAM VARCHAR2
 ,P_STRAATID_OUD NUMBER
 )
 IS
-- Sub-Program Unit Declarations
/* Andere straten met bep straatnaam bij een woonplaats */
CURSOR C_STT_005
 (B_WPS_ID IN VARCHAR2
 ,B_STRAAT IN VARCHAR2
 ,B_STRAATID NUMBER
 )
 IS
/* C_STT_005 */
select	stt.straatid
from	rle_straten stt
where	stt.wps_woonplaatsid = b_wps_id
and	stt.straatnaam = b_straat
and	stt.straatid <> b_straatid
;
/* Ophalen van adressen bij een straat */
CURSOR C_ADS_015
 (B_STRAATID IN NUMBER
 )
 IS
/*C_ADS_015*/
select ads.*, rowid from rle_adressen ads
where ads.stt_straatid = b_straatid
;
/* SELECTIE OP UK1 */
CURSOR C_ADS_016
 (B_CODLAND IN VARCHAR2
 ,B_WPS_ID IN NUMBER
 ,B_STT_ID IN NUMBER
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_016 */
select	ads.numadres
from	rle_adressen ads
where	ads.lnd_codland = b_codland
and	    ads.wps_woonplaatsid = b_wps_id
and	    ads.stt_straatid = b_stt_id
and	    ((ads.huisnummer is null
		  and b_huisnum is null)
	      or ads.huisnummer = b_huisnum)
and		((ads.toevhuisnum is null
		  and b_toevnum is null)
	      or ads.toevhuisnum = b_toevnum);
-- Program Data
R_STT_005 C_STT_005%ROWTYPE;
R_ADS_016 C_ADS_016%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Straatnaam wijzigen
       Eventueel bestaat er al een straat met de nieuwe
       naam bij dezelfde woonplaats. Dan adressen overhangen
       en straat verwijderen.
    */
    begin
       open c_stt_005(p_woonplaatsid
       				 ,p_straatnaam
       				 ,p_straatid_oud
       				 );
       fetch c_stt_005 into r_stt_005;
       if c_stt_005%NOTFOUND then
    		/* te wijzigen straatnaam bestaat nog niet in de woonplaats */
    		update rle_straten
    		set straatnaam = p_straatnaam
    		where straatid = p_straatid_oud;
       else
       	/* straat bestaat al bij dezelfde woonplaats.
       	   adressen laten verwijzen naar andere straat
    	  	   en vervolgens straat verwijderen
    	  	*/
    		for r_ads_015 in c_ads_015(p_straatid_oud)
    		loop
    				open c_ads_016(r_ads_015.lnd_codland
    					,	p_woonplaatsid
    					,	r_stt_005.straatid
    					,	r_ads_015.huisnummer
    					,	r_ads_015.toevhuisnum);
    				fetch c_ads_016 into r_ads_016;
    				if	c_ads_016%found then
          	   		/* adres bestaat al, deze weggooien nadat bijbehorende
          	   			relatie_adressen zijn gewijzigd naar bestaande
          	   			adres.
          	   		*/
          	   		update rle_relatie_adressen
          	   		set ads_numadres = r_ads_016.numadres
          	   		where ads_numadres = r_ads_015.numadres;
          	   		delete from rle_adressen
          	   		where numadres = r_ads_015.numadres;
    				else
    				   	update rle_adressen
    				   	set stt_straatid = r_stt_005.straatid
    		   			where rowid = r_ads_015.rowid;
    	   		end if;
    	   		close c_ads_016;
    		end loop;
       	delete from rle_straten
    		where straatid = p_straatid_oud;
       end if;
       close c_stt_005;
    end;
  /* Straatnaam wijzigen
     Eventueel bestaat er al een straat met de nieuwe
     naam bij dezelfde woonplaats. Dan adressen overhangen
     en straat verwijderen.
  */
  begin
     open c_stt_005(p_woonplaatsid
     				 ,p_straatnaam
     				 ,p_straatid_oud
     				 );
     fetch c_stt_005 into r_stt_005;
     if c_stt_005%NOTFOUND then
  		/* te wijzigen straatnaam bestaat nog niet in de woonplaats */
  		update rle_straten
  		set straatnaam = p_straatnaam
  		where straatid = p_straatid_oud;
     else
     	/* straat bestaat al bij dezelfde woonplaats.
     	   adressen laten verwijzen naar andere straat
  	  	   en vervolgens straat verwijderen
  	  	*/
  		for r_ads_015 in c_ads_015(p_straatid_oud)
  		loop
  				open c_ads_016(r_ads_015.lnd_codland
  					,	p_woonplaatsid
  					,	r_stt_005.straatid
  					,	r_ads_015.huisnummer
  					,	r_ads_015.toevhuisnum);
  				fetch c_ads_016 into r_ads_016;
  				if	c_ads_016%found then
        	   		/* adres bestaat al, deze weggooien nadat bijbehorende
        	   			relatie_adressen zijn gewijzigd naar bestaande
        	   			adres.
        	   		*/
        	   		update rle_relatie_adressen
        	   		set ads_numadres = r_ads_016.numadres
        	   		where ads_numadres = r_ads_015.numadres;
        	   		delete from rle_adressen
        	   		where numadres = r_ads_015.numadres;
  				else
  				   	update rle_adressen
  				   	set stt_straatid = r_stt_005.straatid
  		   			where rowid = r_ads_015.rowid;
  	   		end if;
  	   		close c_ads_016;
  		end loop;
     	delete from rle_straten
  		where straatid = p_straatid_oud;
     end if;
     close c_stt_005;
  end;
END RLE_STT_WIJZIG;

 
/