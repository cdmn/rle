CREATE OR REPLACE PROCEDURE comp_rle.RLE_M16_REC_VERWERK
 (P_CODERELATIE IN varchar2
 ,P_CODSYSTEEMCOMP IN varchar2
 ,P_CODROL IN varchar2
 ,P_NUMRELATIE IN number
 ,P_DATINGANG IN date
 ,P_DATEINDE IN date
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-externe_codes op rowid */
CURSOR C_REC_001
 (B_CODSYSTEEMCOMP IN VARCHAR2
 ,B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_REC_001 */
select	rec.*, rowid
from	rle_relatie_externe_codes rec
where	rec.dwe_wrddom_extsys = b_codsysteemcomp
and	rec.rle_numrelatie = b_numrelatie
and	(	(	rol_codrol is null
		and	b_codrol is null
		)
	or 	rol_codrol = b_codrol
	)
;
-- Program Data
L_CODROL VARCHAR2(3);
L_DATEINDE DATE;
L_DATINGANG_NW DATE;
L_DATEINDE_WIJZ VARCHAR2(1);
R_REC_001 C_REC_001%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_M16_REC_VERWERK */
l_codrol := p_codrol;
IF p_coderelatie IS NULL THEN
   RETURN;
END IF;
OPEN	c_rec_001(p_codsysteemcomp
	, p_numrelatie
	, l_codrol);
FETCH	c_rec_001 INTO r_rec_001;
IF  c_rec_001%notfound
THEN
    CLOSE c_rec_001;
    l_dateinde := p_dateinde;
    INSERT	INTO rle_relatie_externe_codes(rle_numrelatie
       ,	dwe_wrddom_extsys
       ,	extern_relatie
       ,	datingang
       ,	codorganisatie
       ,	rol_codrol
       ,	dateinde)
     VALUES	(p_numrelatie
          ,	p_codsysteemcomp
          ,	p_coderelatie
          ,	p_datingang
          ,	'A'
          ,	l_codrol
          ,	l_dateinde);
ELSE
       CLOSE	c_rec_001;
        IF	p_datingang IS NOT NULL
          AND	p_datingang < r_rec_001.datingang
        THEN
	l_datingang_nw := p_datingang;
        ELSE
	l_datingang_nw	:= NULL;
        END IF;
        IF	(p_dateinde IS NULL
        AND	r_rec_001.dateinde IS NOT NULL)
           OR	(
	(p_dateinde > r_rec_001.dateinde)
	OR
	(r_rec_001.dateinde IS NULL)
	)
        THEN
	l_dateinde_wijz   := 'J';
        ELSE
	l_dateinde_wijz   := 'N';
        END IF;
        IF	l_datingang_nw IS NOT NULL
        OR	l_dateinde_wijz  = 'J'
        OR      stm_dbproc.column_modified(r_rec_001.extern_relatie
			,p_coderelatie)
        THEN
	UPDATE	rle_relatie_externe_codes
	SET	datingang = NVL(l_datingang_nw,datingang)
                   ,                 extern_relatie = p_coderelatie
	,	dateinde  = p_dateinde
	WHERE	id  = r_rec_001.id;
         END IF;
END IF;
END RLE_M16_REC_VERWERK;

 
/