CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADS_CHK_WPS
 (P_NEW_ADS_WPS_WOONPLAATSID NUMBER
 ,P_NEW_ADS_LND_CODLAND VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* c_wnpl03 */
CURSOR C_WPS_003
 (B_WPS_ID IN NUMBER
 )
 IS
/* C_WPS_003 */
select wps.*
,		 rowid
from	rle_woonplaatsen wps
where	wps.woonplaatsid = b_wps_id;
-- Program Data
L_DUMMY NUMBER(1, 0) := 0;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ADS_CHK_WPS */
/*  Controles voor adres, woonplaats, als woonplaats id gevuld is dan lees
woonplaats met woonplaatsid code land moet gelijk zijn aan code land uit
woonplaats */
if	p_new_ads_wps_woonplaatsid is not NULL then
	for r_wps_003 in c_wps_003(p_new_ads_wps_woonplaatsid) loop
		l_dummy := 1;
		if	p_new_ads_lnd_codland <> r_wps_003.lnd_codland then
			stm_dbproc.raise_error('RLE-00437','#1'||
			p_new_ads_lnd_codland||'#2'||
			p_new_ads_wps_woonplaatsid
			,'rle_ads_chk_wnpl');
		end if;
	end loop;
	if 	l_dummy = 0 then
		stm_dbproc.raise_error('RLE-00436',
		'#1 '||p_new_ads_wps_woonplaatsid,
		'rle_ads_chk_wnpl');
	end if;
end if;
END RLE_ADS_CHK_WPS;

 
/