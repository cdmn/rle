CREATE OR REPLACE PROCEDURE comp_rle.RLE_WPS_WIJZIG
 (P_CODLAND VARCHAR2
 ,P_WOONPLAATS VARCHAR2
 ,P_WOONPLAATSID NUMBER
 ,P_ROWID ROWID
 )
 IS
-- Sub-Program Unit Declarations
/* SELECTIE OP UK1 */
CURSOR C_ADS_016
 (B_CODLAND IN VARCHAR2
 ,B_WPS_ID IN NUMBER
 ,B_STT_ID IN NUMBER
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_016 */
select	ads.numadres
from	rle_adressen ads
where	ads.lnd_codland = b_codland
and	    ads.wps_woonplaatsid = b_wps_id
and	    ads.stt_straatid = b_stt_id
and	    ((ads.huisnummer is null
		  and b_huisnum is null)
	      or ads.huisnummer = b_huisnum)
and		((ads.toevhuisnum is null
		  and b_toevnum is null)
	      or ads.toevhuisnum = b_toevnum);
/* Ophalen van adressen bij een woonplaats */
CURSOR C_ADS_007
 (B_WOONPLAATSID IN NUMBER
 )
 IS
/*C_ADS_007*/
select * from rle_adressen ads
where ads.wps_woonplaatsid = b_woonplaatsid;
/* Ophalen van straten bij een woonplaats */
CURSOR C_STT_004
 (B_WOONPLAATSID NUMBER
 )
 IS
/* C_STT_004 */
select *
from rle_straten stt
where stt.wps_woonplaatsid = b_woonplaatsid;
/* Andere straten met bep straatnaam bij een woonplaats */
CURSOR C_STT_005
 (B_WPS_ID IN VARCHAR2
 ,B_STRAAT IN VARCHAR2
 ,B_STRAATID NUMBER
 )
 IS
/* C_STT_005 */
select	stt.straatid
from	rle_straten stt
where	stt.wps_woonplaatsid = b_wps_id
and	stt.straatnaam = b_straat
and	stt.straatid <> b_straatid
;
/* Ophalen van woonplaatsen met woonplaatsnaam */
CURSOR C_WPS_007
 (B_CODLAND VARCHAR2
 ,B_WOONPLAATS VARCHAR2
 ,B_ROWID ROWID
 )
 IS
/* C_WPS_007 */
select * from rle_woonplaatsen wps
where  wps.lnd_codland = b_codland
  and  wps.woonplaats   = b_woonplaats
  and  rowid <> b_rowid
  ;
/* Ophalen van adressen bij een straat */
CURSOR C_ADS_015
 (B_STRAATID IN NUMBER
 )
 IS
/*C_ADS_015*/
select ads.*, rowid from rle_adressen ads
where ads.stt_straatid = b_straatid
;
-- Program Data
R_STT_005 C_STT_005%ROWTYPE;
R_ADS_016 C_ADS_016%ROWTYPE;
R_WPS_007 C_WPS_007%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
Begin
      /* checken of de woonplaats al bestaat */
  	open  c_wps_007(p_codland
  					,  p_woonplaats
  					,  p_rowid);
     fetch c_wps_007 into r_wps_007;
     if c_wps_007%found then
         /* Andere woonplaats bestaat al met nieuwe naam bij zelfde land
         	 Straten en relatieadressen omhangen naar bestaande.
         */
        /* Omhangen straten */
  	   for r_stt_004 in c_stt_004(p_woonplaatsid) loop
  			/* kijken of straat al aan ander woonplaats zit */
  		   open c_stt_005(r_wps_007.woonplaatsid
  		   				 ,r_stt_004.straatnaam
  		   				 ,r_stt_004.straatid
  		   				 );
  		   fetch c_stt_005 into r_stt_005;
  		   if c_stt_005%NOTFOUND then
  		   	/* straat bestaat niet bij andere woonplaats,
  		   	   dus overhangen
  		   	*/
  				update rle_straten
  				set wps_woonplaatsid = r_wps_007.woonplaatsid
  				where straatid = r_stt_004.straatid;
  		   else
  		   	/* straat bestaat al bij andere woonplaats.
  		   	   adressen laten verwijzen naar andere straat
  		   	   en vervolgens straat verwijderen
  		   	*/
  				for r_ads_015 in c_ads_015(r_stt_004.straatid)
  				loop
  					open c_ads_016(r_ads_015.lnd_codland
  					,	r_wps_007.woonplaatsid
  					,	r_stt_005.straatid
  					,	r_ads_015.huisnummer
  					,	r_ads_015.toevhuisnum);
  					fetch c_ads_016 into r_ads_016;
  					if	c_ads_016%found then
        	   		/* adres bestaat al, deze weggooien nadat bijbehorende
        	   			relatie_adressen zijn gewijzigd naar bestaande
        	   			adres.
        	   		*/
        	   		update rle_relatie_adressen
        	   		set ads_numadres = r_ads_016.numadres
        	   		where ads_numadres = r_ads_015.numadres;
        	   		delete from rle_adressen
        	   		where numadres = r_ads_015.numadres;
  					else
  				   	update rle_adressen
  				   	set stt_straatid = r_stt_005.straatid
  				   	,   wps_woonplaatsid = r_wps_007.woonplaatsid
  		   			where rowid = r_ads_015.rowid;
  		   		end if;
  		   		close c_ads_016;
  				end loop;
  		   	delete from rle_straten
  				where straatid = r_stt_004.straatid;
  		   end if;
  	      close c_stt_005;
  	   end loop;
  	   /* omhangen adressen naar andere woonplaats */
  	   for r_ads_007 in c_ads_007(p_woonplaatsid) loop
  			open c_ads_016(r_ads_007.lnd_codland
  					,	r_wps_007.woonplaatsid
  					,	r_ads_007.stt_straatid
  					,	r_ads_007.huisnummer
  					,	r_ads_007.toevhuisnum);
  			fetch c_ads_016 into r_ads_016;
  			if	c_ads_016%found then
    	   		/* adres bestaat al, oude weggooien nadat bijbehorende
    	   			relatie_adressen zijn gewijzigd naar bestaande
    	   			adres.
    	   		*/
    	   		update rle_relatie_adressen
    	   		set ads_numadres = r_ads_016.numadres
    	   		where ads_numadres = r_ads_007.numadres;
    	   		delete from rle_adressen
    	   		where numadres = r_ads_007.numadres;
  			else
  		   	update rle_adressen
              set wps_woonplaatsid = r_wps_007.woonplaatsid
              where numadres = r_ads_007.numadres;
  			end if;
     		close c_ads_016;
  	   end loop;
  	   /* Dubbele woonplaats verwijderen */
        delete from rle_woonplaatsen wps
        where wps.woonplaatsid = p_woonplaatsid;
     else
        /* woonplaatsnaam aanpassen, bestaat nog niet
           onder nieuwe naam
        */
      	update rle_woonplaatsen wps
      	   set wps.woonplaats  	  = p_woonplaats
           where rowid = p_rowid;
  	end if;
     close c_wps_007;
  end;
END RLE_WPS_WIJZIG;

 
/