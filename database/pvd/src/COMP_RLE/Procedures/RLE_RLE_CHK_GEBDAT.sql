CREATE OR REPLACE PROCEDURE comp_rle.RLE_RLE_CHK_GEBDAT
 (P_NEW_RLE_NUMRELATIE NUMBER
 ,P_NEW_RLE_DATGEBOORTE DATE
 )
 IS
CURSOR C_FNR_001
 (B_NUMRELATIE IN NUMBER
 )
IS
select	'x'
from	rle_financieel_nummers fnr
where	fnr.rle_numrelatie = b_numrelatie
and     fnr.datingang      < p_new_rle_datgeboorte;
r_fnr_001 c_fnr_001%rowtype;
--
CURSOR C_RAS_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
select	'x'
from	rle_relatie_adressen ras
where	ras.rle_numrelatie = b_numrelatie
and     ras.datingang < p_new_rle_datgeboorte;
r_ras_001 c_ras_001%rowtype;
--
CURSOR C_REC_003
 (B_NUMRELATIE IN NUMBER
 )
 IS
select	'x'
from	rle_relatie_externe_codes rec
where	rec.rle_numrelatie = b_numrelatie
and     rec.datingang < p_new_rle_datgeboorte;
r_rec_003 c_rec_003%rowtype;
--
BEGIN
/* RLE_RLE_CHK_GEBDAT */
/* controleren van geldigheidsdata rle_relatie_externe_codes */
open c_rec_003(p_new_rle_numrelatie);
fetch c_rec_003 into r_rec_003;
if c_rec_003%found
then
   close c_rec_003;
   stm_dbproc.raise_error('RLE-00407'
                         ,'#1 '||p_new_rle_numrelatie
			   ||'#2 Externe codes'
                         ,'rle_rle_chk_gebdat');
end if;
close c_rec_003;
/* controleren van geldigheidsdata financieel_nummers */
open c_fnr_001(p_new_rle_numrelatie);
fetch c_fnr_001 into r_fnr_001;
if c_fnr_001%found
then
   close c_fnr_001;
   stm_dbproc.raise_error('RLE-00407'
                         ,'#1 '||p_new_rle_numrelatie
                           ||'#2 Financiele nummers'
                         ,'rle_rle_chk_gebdat');
end if;
close c_fnr_001;
/* controleren van geldigheidsdata  rle_relatie_adressen */
open c_ras_001(p_new_rle_numrelatie);
if c_ras_001%found
then
   close c_ras_001;
   stm_dbproc.raise_error('RLE-00407'
                         ,'#1 '||p_new_rle_numrelatie
			   ||'#2 Adressen'
                         ,'rle_rle_chk_gebdat');
end if;
close c_ras_001;
END;

 
/