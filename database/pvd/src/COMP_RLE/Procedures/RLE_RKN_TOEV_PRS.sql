CREATE OR REPLACE PROCEDURE comp_rle.RLE_RKN_TOEV_PRS
 (PIN_PERSOONSNUMMER IN NUMBER
 ,PIN_PERSOONSNUMMER_VERWANT IN NUMBER
 ,PIV_SOORT_VERWANTSCHAP IN VARCHAR2
 ,PID_BEGINDATUM_SELECTIE IN DATE
 ,PID_BEGINDATUM IN DATE
 ,PID_EINDDATUM IN DATE
 ,PIV_INDICATIE_CORRECTIE IN VARCHAR2
 ,PIV_INDICATIE_VERWIJDEREN IN VARCHAR2
 ,POV_VERWERKT OUT VARCHAR2
 ,POV_FOUTMELDING OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_RKG_001
 (CPV_CODE_ROL_IN_KOPPELING VARCHAR2
 )
 IS
SELECT *
FROM   rle_rol_in_koppelingen  rkg
WHERE  rkg.code_rol_in_koppeling = cpv_code_rol_in_koppeling
;
CURSOR C_RKN_005
 (CPN_NUMRELATIE IN NUMBER
 ,CPN_NUMRELATIE_VOOR IN NUMBER
 ,CPD_BEGINDATUM IN DATE
 ,CPV_ROL_IN_KOPPELING IN VARCHAR2
 )
 IS
SELECT /* Lijst kan "nog" vrij worden uitgebreid */
       rkn.id
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkn.rkg_id = rkg.id
AND    rkn.rle_numrelatie        = cpn_numrelatie
AND    rkn.rle_numrelatie_voor   = cpn_numrelatie_voor
AND    TRUNC( rkn.dat_begin )    = TRUNC( cpd_begindatum )
AND    rkg.code_rol_in_koppeling = cpv_rol_in_koppeling
;
-- Program Data
R_RKG_001 C_RKG_001%ROWTYPE;
LN_NUMRELATIE NUMBER;
LN_NUMRELATIE_VOOR NUMBER;
R_RKN_005 C_RKN_005%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default uitvoer parameters */
   pov_verwerkt    := 'N' ;
   pov_foutmelding := NULL ;
   /* Bepalen relatienummers */
   rle_m06_rec_relnum( pin_persoonsnummer
                     , 'PRS'
                     , 'PR'
                     , SYSDATE
                     , ln_numrelatie
                     ) ;
   rle_m06_rec_relnum( pin_persoonsnummer_verwant
                     , 'PRS'
                     , 'PR'
                     , SYSDATE
                     , ln_numrelatie_voor
                     ) ;
   /* Bepalen rol in koppeling */
   OPEN c_rkg_001( piv_soort_verwantschap ) ;
   FETCH c_rkg_001 INTO r_rkg_001 ;
   CLOSE c_rkg_001 ;
   /* Controle invoer parameters */
   IF ln_numrelatie IS NULL
   THEN
      pov_foutmelding := 'Persoon niet gevonden.' ;
      RETURN ;
   END IF ;
   IF ln_numrelatie_voor IS NULL
   THEN
      pov_foutmelding := 'Verwant niet gevonden.' ;
      RETURN ;
   END IF ;
   IF piv_soort_verwantschap NOT IN ( 'H', 'K', 'O', 'S', 'G' )
   THEN
      pov_foutmelding := 'Soort niet H, K, O, S of G' ;
      RETURN ;
   ELSIF r_rkg_001.id IS NULL
   THEN
      pov_foutmelding := 'Soort niet gevonden.' ;
      RETURN ;
   END IF ;
   IF piv_indicatie_correctie NOT IN ( 'J', 'N' )
   THEN
      pov_foutmelding := 'Indicatie correctie niet J of N.' ;
      RETURN ;
   END IF ;
   IF piv_indicatie_verwijderen NOT IN ( 'J', 'N' )
   THEN
      pov_foutmelding := 'Indicatie verwijderen niet J of N.' ;
      RETURN ;
   END IF ;
   IF  piv_indicatie_correctie   = 'J'
   AND piv_indicatie_verwijderen = 'J'
   THEN
      pov_foutmelding := 'Indicatie verwijderen moet N zijn bij correctie.' ;
      RETURN ;
   END IF ;
   IF  (   piv_indicatie_correctie   = 'J'
       OR  piv_indicatie_verwijderen = 'J'
       )
   AND pid_begindatum_selectie IS NULL
   THEN
      pov_foutmelding := 'Begindatum selectie is niet gevuld.' ;
      RETURN ;
   END IF ;
   IF  piv_indicatie_verwijderen = 'N'
   AND pid_begindatum IS NULL
   THEN
      pov_foutmelding := 'Begindatum niet gevuld.' ;
      RETURN ;
   END IF ;
   /* Verwerk relatie koppeling */
   IF  (   piv_indicatie_correctie   = 'J'
       OR  piv_indicatie_verwijderen = 'J'
       )
   THEN
      OPEN c_rkn_005( ln_numrelatie
                    , ln_numrelatie_voor
                    , pid_begindatum_selectie
                    , piv_soort_verwantschap
                    ) ;
      FETCH c_rkn_005 INTO r_rkn_005 ;
      IF c_rkn_005%NOTFOUND
      THEN
         CLOSE c_rkn_005 ;
         pov_foutmelding := 'Verwantschap niet gevonden.' ;
         RETURN ;
      ELSE
         CLOSE c_rkn_005 ;
      END IF ;
      IF piv_indicatie_correctie = 'J'
      THEN
         /* UPDATE */
         UPDATE rle_relatie_koppelingen  rkn
         SET    rkn.dat_begin = pid_begindatum
         ,      rkn.dat_einde = pid_einddatum
         WHERE  rkn.id = r_rkn_005.id
         ;
         pov_verwerkt := 'J' ;
      ELSIF piv_indicatie_verwijderen = 'J'
      THEN
         /* DELETE */
         DELETE rle_relatie_koppelingen  rkn
         WHERE  rkn.id = r_rkn_005.id
         ;
         pov_verwerkt := 'J' ;
      END IF ;
   ELSE
      /* INSERT */
      INSERT INTO rle_relatie_koppelingen(
           id
         , rkg_id
         , rle_numrelatie
         , rle_numrelatie_voor
         , dat_begin
         , dat_einde
         ) VALUES(
           rle_rkn_seq1.nextval
         , r_rkg_001.id
         , ln_numrelatie
         , ln_numrelatie_voor
         , pid_begindatum
         , pid_einddatum
         ) ;
      pov_verwerkt := 'J' ;
   END IF ;
EXCEPTION
   WHEN OTHERS THEN
      /* Onverwachte fout */
      /* Uitvoer */
      pov_verwerkt    := 'N' ;
      pov_foutmelding := SQLERRM ;
      /* Cursor sluiten */
      IF c_rkg_001%ISOPEN
      THEN
         CLOSE c_rkg_001 ;
      END IF ;
      IF c_rkn_005%ISOPEN
      THEN
         CLOSE c_rkn_005 ;
      END IF ;
END RLE_RKN_TOEV_PRS;

 
/