CREATE OR REPLACE PROCEDURE comp_rle.RLE_SYSDATE_CHK
 (P_DATUM DATE
 ,P_DATUMOMS VARCHAR2
 )
 IS
BEGIN
/* RLE_SYSDATE_CHK */
if	p_datum is not NULL
and	p_datum > TRUNC(sysdate) then
	stm_dbproc.raise_error('RLE-00432'
	,	'#1'||p_datumoms
	,	'rle_sysdate_chk');
end if;
  /**
   ** toegevoegd RZR 31/5/02.
   **/
if	p_datum is not NULL
and	p_datum < to_date('01011875', 'DDMMYYYY') then
	stm_dbproc.raise_error('RLE-00373'
	,	NULL
	,	'rle_sysdate_chk');
end if;
END RLE_SYSDATE_CHK;

 
/