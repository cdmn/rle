CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADS_GET
 (P_NUMADRES IN NUMBER
 ,P_AANT_REGELS IN number
 ,P_AANT_POS IN number
 ,P_LAYOUTCODE IN varchar2
 ,P_O_MELDING IN OUT varchar2
 ,P_O_REGEL1 IN OUT varchar2
 ,P_O_REGEL2 IN OUT varchar2
 ,P_O_REGEL3 IN OUT varchar2
 ,P_O_REGEL4 IN OUT varchar2
 ,P_O_REGEL5 IN OUT varchar2
 ,P_O_REGEL6 IN OUT varchar2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_ADS_002
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_ADS_002 */
select ads.huisnummer
,      ads.toevhuisnum
,      ads.postcode
,      ads.lnd_codland
from   rle_adressen ads
where  ads.numadres = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_STT_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_STT_001 */
select	stt.straatnaam
from	rle_straten stt
,	rle_adressen ads
where	stt.straatid  = ads.stt_straatid
and	ads.numadres  = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_WPS_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_WPS_001 */
select	wps.woonplaats
from	rle_woonplaatsen wps
,	rle_adressen ads
where	wps.woonplaatsid = ads.wps_woonplaatsid
and	numadres = b_numadres;
/* Ophalen van postcode van bij een adres */
CURSOR C_ADS_001
 (B_NUMADRES NUMBER
 )
 IS
/* C_ADS_001 */
 select        ads.postcode
 , ads.lnd_codland
 from  rle_adressen ads
 where ads.numadres = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_LND_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_LND_001 */
select	lnd.codlayoutadres
,	lnd.codland
,	lnd.naam
from	rle_landen lnd
where	lnd.codland =
	(select	ads.lnd_codland
	from	rle_adressen ads
	where	ads.numadres = b_numadres);
-- Program Data
L_IND_GEVONDEN VARCHAR2(1);
L_LAYOUTCODE RLE_LANDEN.CODLAYOUTADRES%TYPE;
CN_MODULE CONSTANT VARCHAR2(30) := 'RLE_ADS_GET';
R_ADS_001 C_ADS_001%ROWTYPE;
L_VELD VARCHAR2(255);
L_LENGTE_VELD NUMBER(5, 0);
L_WOONPLAATS RLE_WOONPLAATSEN.WOONPLAATS%TYPE;
L_HUISNUMMER RLE_ADRESSEN.HUISNUMMER%TYPE;
L_TOEVHUISNUM RLE_ADRESSEN.TOEVHUISNUM%TYPE;
L_STRAAT RLE_STRATEN.STRAATNAAM%TYPE;
R_LND_001 C_LND_001%ROWTYPE;
R_ADS_002 C_ADS_002%ROWTYPE;
L_WAARDE VARCHAR2(1);
L_WAARDENR NUMBER;
L_POSITIE NUMBER;
L_REGEL NUMBER;
L_MAX_POSITIE NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ADS_GET */
DECLARE
TYPE tabel_type	IS TABLE OF VARCHAR2(2000) INDEX BY BINARY_INTEGER;
regel_table	tabel_type;
regel_index 	BINARY_INTEGER;
PROCEDURE  rle_ads_bepaal_adres(
  p_regel IN OUT NUMBER ,
  p_waarde IN OUT NUMBER ,
  p_numadrs IN NUMBER )
IS
BEGIN
--
  IF p_waarde = 1
  THEN
     OPEN  c_stt_001( p_numadres);
     FETCH c_stt_001 INTO l_straat;
     l_veld := l_straat;
     CLOSE c_stt_001;
  END IF;
--
  IF p_waarde = 2
  THEN
     OPEN c_ads_002( p_numadres);
     FETCH c_ads_002 INTO r_ads_002;
     l_veld := r_ads_002.huisnummer;
     CLOSE c_ads_002;
  END IF;
--
  IF p_waarde = 3
  THEN
     OPEN c_ads_002( p_numadres);
     FETCH c_ads_002 INTO r_ads_002;
     l_veld :=r_ads_002.toevhuisnum;
     CLOSE c_ads_002;
  END IF;
--
  IF p_waarde = 4
  THEN
     OPEN c_ads_002( p_numadres);
     FETCH c_ads_002 INTO r_ads_002;
     -- HVI 11-02-2004: Alleen bij NL spatie in Postcode van 6 posities (stond al in een oude versie):
     IF r_ads_002.lnd_codland = stm_algm_get.sysparm('KBD', 'LAND', SYSDATE)
     THEN
       l_veld := SUBSTR( r_ads_002.postcode, 1, 4)
              || ' '
              || SUBSTR( r_ads_002.postcode, 5, 2)
              || ' ';            -- osp extra spatie
     ELSE
       l_veld := r_ads_002.postcode;
     END IF;
     CLOSE c_ads_002;
  END IF;
--
/* PKG, 20-08-2003: Nav RRGG: waarom landnaam bij buitenlands adres
   achter woonplaats???? Landnaam altijd bij waarde 6 teruggeven
   nu dus als volgt:
*/
  IF p_waarde = 5
  THEN
     OPEN c_wps_001( p_numadres);
     FETCH c_wps_001 INTO l_woonplaats;
        l_veld := l_woonplaats;
     CLOSE c_wps_001;
  END IF;
--
  IF p_waarde = 6
  THEN
      OPEN c_lnd_001( p_numadres);
      FETCH c_lnd_001 INTO r_lnd_001;
         l_veld:= r_lnd_001.naam;
      CLOSE c_lnd_001;
  END IF;
--
/* HVI, 11-02-2004: Waardes boven de 6 werden niet getoond.
                    Hierbij is '7' gekoppeld aan RLE_LANDEN.CODLAND
                    indien dit gewijzigd wordt ook de inhoud van de
                    kolom RLE_LANDEN.CODLAYOUTADRES aanpassen!!!
*/
  IF p_waarde = 7
  THEN
      OPEN c_lnd_001( p_numadres);
      FETCH c_lnd_001 INTO r_lnd_001;
         l_veld:= r_lnd_001.codland;
      CLOSE c_lnd_001;
  END IF;
--
  IF p_waarde > 7
  THEN
      l_veld:= cn_module||': Voor waarde '||TO_CHAR(p_waarde)
               ||' is er nog geen adresveld gekoppeld';
  END IF;
  --
  l_lengte_veld := NVL( LENGTH( l_veld), 0);
  regel_table( p_regel) := regel_table( p_regel) || l_veld || ' ';
EXCEPTION
WHEN OTHERS
THEN
  p_o_melding := SQLERRM;
END;
--
BEGIN
  regel_table(1) := NULL;
  regel_table(2) := NULL;
  regel_table(3) := NULL;
  regel_table(4) := NULL;
  regel_table(5) := NULL;
  regel_table(6) := NULL;
  OPEN c_ads_001( p_numadres);
  FETCH c_ads_001 INTO r_ads_001;
  IF c_ads_001%found
  THEN
     l_ind_gevonden := 'J';
  ELSE
     l_ind_gevonden := 'N';
  END IF;
  CLOSE c_ads_001;
--
  IF l_ind_gevonden = 'J'
  THEN
     /* PKG, 20-08-2003 nav RRGG: elsif vervangen door else if aangepast
        r_lnd_001.codlayoutadres != 'NOTG' is namelijk tot nu toe
        altijd waar
     */
     OPEN c_lnd_001( p_numadres);
     FETCH c_lnd_001 INTO r_lnd_001;
     CLOSE c_lnd_001;
     IF p_layoutcode IS NOT NULL
     THEN
        l_layoutcode := p_layoutcode;
     ELSE
        IF r_lnd_001.codlayoutadres != 'NOTG'
        THEN
          l_layoutcode := r_lnd_001.codlayoutadres;
        ELSE
          l_layoutcode := '123#45#6#7#8#9';
        END IF;
     END IF;
     l_regel    := 1;
     l_positie  := 1;
     l_max_positie := NVL( LENGTH( l_layoutcode), 0);
--
     WHILE l_regel   <= p_aant_regels
       AND l_regel   <= 6
       AND l_positie <= l_max_positie
       LOOP
         l_waarde := SUBSTR( l_layoutcode, l_positie, 1);
         IF l_waarde ='#'
         THEN
            l_regel := l_regel + 1;
         ELSIF l_waarde IN ('1', '2', '3', '4', '5', '6', '7', '8', '9')
         THEN
            l_waardenr := TO_NUMBER( l_waarde);
            rle_ads_bepaal_adres( l_regel, l_waardenr, p_numadres);
         ELSE
            NULL;
         END IF;
         l_positie :=l_positie+1;
       END LOOP;
--
     p_o_regel1 := RTRIM(LTRIM(SUBSTR(regel_table(1),1,p_aant_pos)));
     p_o_regel2 := RTRIM(LTRIM(SUBSTR(regel_table(2),1,p_aant_pos)));
     p_o_regel3 := RTRIM(LTRIM(SUBSTR(regel_table(3),1,p_aant_pos)));
     p_o_regel4 := RTRIM(LTRIM(SUBSTR(regel_table(4),1,p_aant_pos)));
     p_o_regel5 := RTRIM(LTRIM(SUBSTR(regel_table(5),1,p_aant_pos)));
     p_o_regel6 := RTRIM(LTRIM(SUBSTR(regel_table(6),1,p_aant_pos)));
  ELSE
     p_o_regel1 := NULL;
     p_o_regel2 := NULL;
     p_o_regel3 := NULL;
     p_o_regel4 := NULL;
     p_o_regel5 := NULL;
     p_o_regel6 := NULL;
  END IF;
END;
END RLE_ADS_GET;

 
/