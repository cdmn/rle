CREATE OR REPLACE PROCEDURE comp_rle.RLE_CHK_RLE_INSTDAT
 (P_NUMRELATIE NUMBER
 ,P_DATINGANG DATE
 ,P_TABEL VARCHAR2
 )
 IS
-- PL/SQL Specification
/* C_RLE_001     Ophalen van relaties op relatienummer
*/
CURSOR C_RLE_001 (B_NUMRELATIE IN NUMBER
                 )
IS
SELECT indinstelling
,      dateinde
FROM   rle_relaties rle
WHERE  rle.numrelatie = b_numrelatie
;
-- PL/SQL Block
BEGIN
/* RLE_CHK_RLE_INSTDAT */
/* Bij instellingen  mag de begindatum niet na datum einde bestaan liggen */
for r_rle_001 in c_rle_001(p_numrelatie) loop
	if	r_rle_001.indinstelling = 'J'
	and	STM_ALGM_CHECK.ALGM_DATCOMP(p_datingang
		,	'>', r_rle_001.dateinde) = 'J' then
		stm_dbproc.raise_error('RLE-00386'
		,	'#1'|| p_tabel
		,	'rle_chk_rle_insdat');
	end if;
end loop;
END RLE_CHK_RLE_INSTDAT;

 
/