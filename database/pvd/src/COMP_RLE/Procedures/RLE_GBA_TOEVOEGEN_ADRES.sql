CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_TOEVOEGEN_ADRES
 (PIN_RAS_ID IN NUMBER
 )
 IS
  /******************************************************************************************************
  Naam:         RLE_GBA_TOEVOEGEN_ADRES
  Beschrijving: Toevoegen adressen van personen

  Datum       Wie  Wat
  ----------  ---  --------------------------------------------------------------
  ??-??-????  ???  Creatie
  19-02-2008  PAS  GBA-aanpassing Zorgketen 2008
                   IBM Aanroep naar GBA_MUTEREN_ADRES is ook nodig voor personen die niet
                   met het GBA afgestemd mogen worden. Middels deze IBM aanroep(en) wordt
                   namelijk (ook) geregeld dat in de tabel per_aanmeldingen het adres wordt bijgewerkt.
                   Opmerking: Personen die niet mogen worden afgestemd hebben minimaal een OA adres
                              en in elk geval geen DA adres.
  12-12-2001  XSW  Controle GBA-indicatie uitgezet
  ******************************************************************************************************/
-- Sub-Program Unit Declarations
CURSOR C_RAS_030
 (CPN_RAS_ID IN NUMBER
 )
 IS
SELECT ras.rle_numrelatie
,      ras.rol_codrol
,      ras.dwe_wrddom_srtadr
,      stt.straatnaam
,      ads.huisnummer
,      wps.woonplaats
,      ads.toevhuisnum
,      ads.postcode
,      ras.locatie
,      ras.provincie
,      lnd.codland
,      lnd.naam
FROM   rle_relatie_adressen  ras
,      rle_adressen          ads
,      rle_straten           stt
,      rle_woonplaatsen      wps
,      rle_landen            lnd
WHERE  ras.id               = cpn_ras_id
AND    ras.ads_numadres     = ads.numadres
AND    ads.stt_straatid     = stt.straatid (+)
AND    stt.wps_woonplaatsid = wps.woonplaatsid (+)
AND    ads.lnd_codland      = lnd.codland
;
-- Program Data
LV_VERWERKT VARCHAR2(1);
LN_PERSOONSNUMMER NUMBER;
R_RAS_030 C_RAS_030%ROWTYPE;
LV_FOUTMELDING VARCHAR2(4000);
BEGIN
/* Alleen doorgeven als het nodig is */
   IF rle_indicatie.doorgeven_aan_gba = 'N'
   THEN
      RETURN ;
   END IF ;
   /* Opzoeken adres gegevens */
   OPEN c_ras_030( pin_ras_id ) ;
   FETCH c_ras_030 INTO r_ras_030 ;
   CLOSE c_ras_030 ;
   /* Bepalen persoonsnummer */
   ln_persoonsnummer := rle_m03_rec_extcod( 'PRS'
                                          , r_ras_030.rle_numrelatie
                                          , 'PR'
                                          , SYSDATE
                                          ) ;
   /* Extra controle. Doorgeven moet alleen voor
      Domicilie adressen van personen.
   */
   IF (
        (     r_ras_030.rol_codrol        = 'PR'
          AND r_ras_030.dwe_wrddom_srtadr = 'DA'
        )
      OR
        /* 19-02-2007 PAS GBA-aanpassing Zorgketen 2008  */
        (     r_ras_030.rol_codrol        = 'PR'
          AND r_ras_030.dwe_wrddom_srtadr = 'OA'
          AND rle_chk_gba_afst( p_rle_numrelatie => r_ras_030.rle_numrelatie
                              , p_persoonsnummer => NULL
                              ) = 'N'
        )
      )
   THEN
     /* Aanroepen GBA dienst */
     ibm.ibm( 'GBA_TOEVOEGEN_ADRES'
            , 'RLE'
            , ln_persoonsnummer        -- PIN_PERSOONSNUMMER IN NUMBER
            , r_ras_030.straatnaam     -- PIV_STRAATNAAM
            , r_ras_030.huisnummer     -- PIN_HUISNUMMER
            , r_ras_030.woonplaats     -- PIV_WOONPLAATS
            , r_ras_030.toevhuisnum    -- PIV_TOEVOEGING
            , r_ras_030.postcode       -- PIV_POSTCODE
            , r_ras_030.locatie        -- PIV_LOCATIEOMSCHRIJVING
            , r_ras_030.codland        -- PIV_COD_LAND
            , r_ras_030.naam           -- PIV_OMS_LAND
            , r_ras_030.provincie      -- PIV_PROVINCIE
            , lv_verwerkt              -- POV_VERWERKT
            , lv_foutmelding           -- POV_FOUTMELDING
            ) ;
      /* xsw 12-12-2011 Fout in muteren adres leidt tot fout; incident245561
     IF NVL( lv_verwerkt, 'N' ) != 'J'
     THEN
        IF    INSTR( lv_foutmelding, 'ORA-20000: ' ) = 1
        THEN
           stm_dbproc.raise_error( SUBSTR( lv_foutmelding, 12 ) ) ;
        ELSIF INSTR( lv_foutmelding, 'ORA-' ) = 1
        THEN
           stm_dbproc.raise_error( 'RLE-10313 #1Toevoegen adres #2' || SUBSTR( lv_foutmelding, 12 ) ) ;
        ELSE
           stm_dbproc.raise_error( 'RLE-10313 #1Toevoegen adres #2' || lv_foutmelding ) ;
        END IF ;
     END IF ;
     */
   END IF;
END RLE_GBA_TOEVOEGEN_ADRES;
 
/