CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADS_NIEUW
 (P_STT_ID IN OUT NUMBER
 ,P_WPS_ID IN OUT NUMBER
 ,P_CODLAND IN OUT VARCHAR2
 ,P_STRAAT IN OUT VARCHAR2
 ,P_WOONPL IN OUT VARCHAR2
 ,P_O_NUMADRES IN OUT NUMBER
 ,P_POSTCOD IN OUT VARCHAR2
 ,P_HUISNUM IN OUT NUMBER
 ,P_TOEVNUM IN OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Bepalen adres-ID o.b.v. land, woonplaats en straat (ADRS04) */
CURSOR C_ADS_006
 (B_CODLAND IN VARCHAR2
 ,B_WPS_ID IN NUMBER
 ,B_STT_ID IN NUMBER
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_006 */
select *	/*ads.numadres*/
from	rle_adressen ads
where	ads.lnd_codland  = b_codland
and	ads.wps_woonplaatsid = b_wps_id
and	ads.stt_straatid = b_stt_id
and	((ads.huisnummer is null
	  and b_huisnum is null)
	or ads.huisnummer = b_huisnum)
and	((ads.toevhuisnum is null
     and b_toevnum is null)
     or ads.toevhuisnum = b_toevnum);
-- Program Data
R_ADS_006 RLE_ADRESSEN%ROWTYPE;
L_NUMADS NUMBER(7, 0);
L_LOKATIE VARCHAR2(6);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ADS_NIEUW */
   l_lokatie := '000010';
   if p_woonpl is null
   then
     l_lokatie := '000020';
     raise_application_error(-20001,'RLE-10360 Woonplaats is leeg, rle_ads_nieuw');
   end if;
   if p_wps_id is NULL
   then
      l_lokatie := '000030';
      p_wps_id := rle_wps_seq1_next_id;
      l_lokatie := '000040';
      insert into rle_woonplaatsen(woonplaatsid
      ,lnd_codland
      ,woonplaats)
      values
      (p_wps_id
      ,p_codland
      ,p_woonpl);
   end if;
   if p_stt_id  is null
   then
      l_lokatie := '000050';
      p_stt_id := rle_stt_seq1_next_id;
      l_lokatie := '000060';
      insert into rle_straten(straatid
      ,wps_woonplaatsid
      ,straatnaam)
      values
      (p_stt_id
      ,p_wps_id
      ,p_straat);
   end if;
   /* Aangezien de postcode leeg kan zijn wordt het adres nogmaals
   geselecteerd op de onderkende unieke sleutel. Als het adres gevonden
   wordt, volgt als in de nieuw aangeleverde gegevens de postcode
   gevuld is een wijziging van het adres. Wordt ook hier geen adres
   gevonden dan wordt het adres toegevoegd */
   l_lokatie := '000070';
   open c_ads_006(p_codland
    , p_wps_id
    , p_stt_id
    , p_huisnum
    , p_toevnum);
    fetch c_ads_006 into r_ads_006;
    if c_ads_006%found then
	   l_lokatie := '000080';
        update rle_adressen ads
        set ads.postcode = p_postcod
        where ads.numadres = r_ads_006.numadres;
    else
        -- Een aantal controles wordt niet uitgevoerd
        -- Hiervoor wordt een global gevuld
        rle_npe_pck.g_ind_npe_mutatie  := 'J';
		l_lokatie := '000090';
        l_numads := rle_ads_seq1_next_id;
		l_lokatie := '000110';
        insert into rle_adressen(numadres
          ,lnd_codland
          ,stt_straatid
          ,wps_woonplaatsid
          ,huisnummer
          ,postcode
          ,toevhuisnum)
        values
        (l_numads
         ,p_codland
         ,p_stt_id
         ,p_wps_id
         ,p_huisnum
         ,p_postcod
         ,p_toevnum);
        -- Global terugzetten
        rle_npe_pck.g_ind_npe_mutatie  := 'N';
    end if;
    l_lokatie := '000120';
    close	c_ads_006;
    p_o_numadres := l_numads;
EXCEPTION
when others then
    -- Global terugzetten
    rle_npe_pck.g_ind_npe_mutatie  := 'N';
  --  raise;
    stm_dbproc.raise_error('rle_ads_nieuw : '||NVL(l_lokatie,'leeg')||' '||SQLERRM);
END RLE_ADS_NIEUW;

 
/