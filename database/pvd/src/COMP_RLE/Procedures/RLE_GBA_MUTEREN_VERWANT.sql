CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_MUTEREN_VERWANT
 (PIN_RKN_ID IN NUMBER
 )
 IS
-- Sub-Program Unit Declarations
/* Zoeken relatiekoppeling met ID */
CURSOR C_RKN_004
 (CPN_RKN_ID IN NUMBER
 )
 IS
SELECT rkn.id
,      rkn.rkg_id
,      rkn.rle_numrelatie
,      rkn.rle_numrelatie_voor
,      rkn.dat_begin
,      rkn.dat_einde
,      rkg.code_rol_in_koppeling
,      rkg.omschrijving
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkn.id     = cpn_rkn_id
AND    rkn.rkg_id = rkg.id
;
-- Program Data
LV_ROL VARCHAR2(2);
LV_CODE_VERWANTSCHAP_1 VARCHAR2(1);
LV_FOUTMELDING VARCHAR2(4000);
LV_CODE_VERWANTSCHAP_2 VARCHAR2(1);
R_RKN_004 C_RKN_004%ROWTYPE;
LV_VERWERKT VARCHAR2(1);
LN_PERSOONSNUMMER_1 NUMBER;
LN_PERSOONSNUMMER_2 NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Alleen doorgeven als het nodig is */
   IF rle_indicatie.doorgeven_aan_gba = 'N'
   THEN
      RETURN ;
   END IF ;
   STM_UTIL.DEBUG('RLE_GBA_MUTEREN_VERWANT');
   /* Ophalen gegevens van relatie koppeling */
   OPEN c_rkn_004( pin_rkn_id ) ;
   FETCH c_rkn_004 INTO r_rkn_004 ;
   CLOSE c_rkn_004 ;
   /* De relaties staan in RLE net andersom.
      In de relatie koppeling is het
      <RKN.RLE_NUMRELATIE> is <RKG.OMSCHRIJVING> <RKN.RLE_NUMRELATIE_VOOR>
      Bijvoorbeeld:
      relatie 1 is OUDER VAN relatie 2
      Echter in GBA staat de verwant in PIN_PERSOONSNUMMER.
      --------------------
      Nota: Om te controleren welke wijziging doorgegeven moet worden aan
            het GBA moet worden gekeken welke van de twee relaties de rol
            DN heeft. Voor de relatie (of beide) die de rol DN heeft zal
            de verwantschap in het GBA zijn opgenomen.
   */ /* */
   ln_persoonsnummer_1 := rle_m03_rec_extcod( 'PRS'
                                            , r_rkn_004.rle_numrelatie
                                            , 'PR'
                                            , SYSDATE
                                            ) ;
   ln_persoonsnummer_2 := rle_m03_rec_extcod( 'PRS'
                                            , r_rkn_004.rle_numrelatie_voor
                                            , 'PR'
                                            , SYSDATE
                                            ) ;
   IF    r_rkn_004.code_rol_in_koppeling = 'O'
   THEN
      /* Gerichte verwantschap */
      lv_code_verwantschap_1 := 'O' ;
      lv_code_verwantschap_2 := 'K' ;
   ELSIF r_rkn_004.code_rol_in_koppeling = 'K'
   THEN
      /* Gerichte verwantschap */
      lv_code_verwantschap_1 := 'K' ;
      lv_code_verwantschap_2 := 'O' ;
   ELSIF r_rkn_004.code_rol_in_koppeling IN ( 'S', 'H', 'G' )
   THEN
      lv_code_verwantschap_1 := r_rkn_004.code_rol_in_koppeling ;
      lv_code_verwantschap_2 := r_rkn_004.code_rol_in_koppeling ;
   ELSE
      lv_code_verwantschap_1 := NULL ;
      lv_code_verwantschap_2 := NULL ;
   END IF ;
   /* Extra controle: alleen verwantschappen doorgeven */
   IF  lv_code_verwantschap_1 IS NULL
   AND lv_code_verwantschap_2 IS NULL
   THEN
      RETURN ;
   END IF ;
   /* Extra controle: error wanneer persoonsnummers niet bekend zijn. */
   IF ln_persoonsnummer_1 IS NULL
   OR ln_persoonsnummer_2 IS NULL
   THEN
      stm_dbproc.raise_error( 'RLE-10313 #1Muteren verwantschap#2De persoonsnummers konden niet worden gevonden.' ) ;
   END IF ;
   /* bepaal of update voor pin_numrelatie moet worden uitgevoerd */
   rle_gba_bepaal_rol( r_rkn_004.rle_numrelatie
                     , lv_rol
                     ) ;
   IF lv_rol IN ( 'WN', 'PR' )
   THEN
      /* Aanroep GBA dienst */
      STM_UTIL.DEBUG('EERSTE AANROEP GBA_MUTEREN_VERWANTSCHAP');
      ibm.ibm( 'GBA_MUTEREN_VERWANTSCHAP'
             , 'RLE'
             , ln_persoonsnummer_2      -- PIN_PERSOONSNUMMER
             , ln_persoonsnummer_1      -- PIN_PERSOONSNUMMER_DEELNEMER
             , r_rkn_004.dat_begin      -- PID_INGANGSDATUM
             , lv_code_verwantschap_1   -- PIV_CODE_VERWANTSCHAPSOORT
             , r_rkn_004.dat_einde      -- PID_EINDDATUM
             , lv_verwerkt              -- POV_VERWERKT
             , lv_foutmelding           -- POV_FOUTMELDING
             ) ;
      IF NVL( lv_verwerkt, 'N' ) != 'J'
      THEN
         IF    INSTR( lv_foutmelding, 'ORA-20000: ' ) = 1
         THEN
            stm_dbproc.raise_error( SUBSTR( lv_foutmelding, 12 ) ) ;
         ELSIF INSTR( lv_foutmelding, 'ORA-' ) = 1
         THEN
            stm_dbproc.raise_error( 'RLE-10313 #1Muteren verwantschap #2' || SUBSTR( lv_foutmelding, 12 ) ) ;
         ELSE
            stm_dbproc.raise_error( 'RLE-10313 #1Muteren verwantschap #2' || lv_foutmelding ) ;
         END IF ;
      END IF ;
   END IF ;
   /* bepaal of update voor pin_numrelatie_voor moet worden uitgevoerd */
   rle_gba_bepaal_rol( r_rkn_004.rle_numrelatie_voor
                     , lv_rol
                     ) ;
   /* tweede aanroep aanmaken verwant IN GBA IS overbodig
   IF lv_rol IN ( 'WN', 'PR' )
   THEN
      -- Aanroep GBA dienst
      STM_UTIL.DEBUG('TWEEDE AANROEP GBA_MUTEREN_VERWANTSCHAP');
      ibm.ibm( 'GBA_MUTEREN_VERWANTSCHAP'
             , 'RLE'
             , ln_persoonsnummer_1      -- PIN_PERSOONSNUMMER
             , ln_persoonsnummer_2      -- PIN_PERSOONSNUMMER_DEELNEMER
             , r_rkn_004.dat_begin      -- PID_INGANGSDATUM
             , lv_code_verwantschap_2   -- PIV_CODE_VERWANTSCHAPSOORT
             , r_rkn_004.dat_einde      -- PID_EINDDATUM
             , lv_verwerkt              -- POV_VERWERKT
             , lv_foutmelding           -- POV_FOUTMELDING
             ) ;
      IF NVL( lv_verwerkt, 'N' ) != 'J'
      THEN
         IF    INSTR( lv_foutmelding, 'ORA-20000: ' ) = 1
         THEN
            stm_dbproc.raise_error( SUBSTR( lv_foutmelding, 12 ) ) ;
         ELSIF INSTR( lv_foutmelding, 'ORA-' ) = 1
         THEN
            stm_dbproc.raise_error( 'RLE-10313 #1Muteren verwantschap #2' || SUBSTR( lv_foutmelding, 12 ) ) ;
         ELSE
            stm_dbproc.raise_error( 'RLE-10313 #1Muteren verwantschap #2' || lv_foutmelding ) ;
         END IF ;
      END IF ;
   END IF ;
   */
END RLE_GBA_MUTEREN_VERWANT;

 
/