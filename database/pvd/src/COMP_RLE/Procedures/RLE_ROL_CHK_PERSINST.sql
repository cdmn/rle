CREATE OR REPLACE PROCEDURE comp_rle.RLE_ROL_CHK_PERSINST
 (P_NEW_ROL_CODROL IN rle_rollen.codrol%TYPE
 ,P_OLD_ROL_CODPINST IN VARCHAR2
 ,P_NEW_ROL_CODPINST IN VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_RLE_003
 (B_NEW_ROL_CODROL IN VARCHAR2
 )
 IS
/* C_RLE_003 */
select	rle.*
from	rle_relaties rle
,	rle_relatie_rollen rrl
where	rrl.rol_codrol = b_new_rol_codrol
and	rrl.rle_numrelatie = rle.numrelatie;
-- Program Data
R_RLE_003 C_RLE_003%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ROL_CHK_PERSINST */
/* Bij wijzigen van codepersoon-instelling mag de
   nieuwe waarde enkel 'B' zijn tenzij nog geen enkele
   conflicterende relatierol bestaat */
IF	p_old_rol_codpinst <> p_new_rol_codpinst
	AND	p_new_rol_codpinst <> 'B' THEN
        IF	(p_old_rol_codpinst = 'P' OR p_old_rol_codpinst = 'B')
                AND	p_new_rol_codpinst = 'I' THEN
		for	r_rle_003 in c_rle_003(p_new_rol_codrol) loop
			IF	r_rle_003.indinstelling = 'N' THEN
				stm_dbproc.raise_error('RLE-00417',
				'#1'||p_new_rol_codrol,
				'rle_rol_chk_persinst');
                        END IF;
		END LOOP;
	ELSIF
		(p_old_rol_codpinst = 'I'
	OR	p_old_rol_codpinst = 'B')
	AND	p_new_rol_codpinst = 'P' THEN
	for	r_rle_003 in c_rle_003(p_new_rol_codrol) loop
		IF	r_rle_003.indinstelling = 'J' THEN
			stm_dbproc.raise_error('RLE-00417',
			'#1'||p_new_rol_codrol,
			'rle_rol_chk_persinst');
		END IF;
	END LOOP;
	END IF;
END IF;
END RLE_ROL_CHK_PERSINST;

 
/