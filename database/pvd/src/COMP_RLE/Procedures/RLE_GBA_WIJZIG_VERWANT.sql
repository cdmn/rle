CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_WIJZIG_VERWANT
 (PIN_PERSOONSNUMMER IN NUMBER
 ,PIN_PERSOONSNUMMER_DEELNEMER IN NUMBER
 ,PIV_CODE_SOORT_VERWANTSCHAP IN VARCHAR2
 ,PID_DAT_BEGIN IN DATE
 ,pid_code_dat_begin_fictief in varchar2
 ,PID_DAT_EINDE IN DATE
 ,pid_code_dat_einde_fictief in varchar2
 ,POV_VERWERKT OUT VARCHAR2
 ,POV_FOUTMELDING OUT VARCHAR2
 ,PIV_OFMW_MUTATIE IN BOOLEAN := false

 )
 IS
-- Sub-Program Unit Declarations
  cursor c_rkg_001 (b_code_rol_in_koppeling varchar2)
  is
  select *
  from   rle_rol_in_koppelingen rkg
  where  rkg.code_rol_in_koppeling = b_code_rol_in_koppeling
  ;

  cursor c_rkn_005 ( b_numrelatie in number
                   , b_numrelatie_voor in number
                   , b_begindatum in date
                   , b_code_rol_in_koppeling in varchar2
                   )
  is
  select /* lijst kan "nog" vrij worden uitgebreid */
  rkn.id
  from  rle_relatie_koppelingen rkn
  ,     rle_rol_in_koppelingen rkg
  where rkn.rkg_id = rkg.id
  and   rkn.rle_numrelatie = b_numrelatie
  and   rkn.rle_numrelatie_voor = b_numrelatie_voor
  and   trunc(rkn.dat_begin) = trunc(b_begindatum)
  and   rkg.code_rol_in_koppeling = b_code_rol_in_koppeling
  ;

  l_numrelatie_1       rle_relaties.numrelatie%type;
  l_numrelatie_2       rle_relaties.numrelatie%type;
  l_rol_in_koppeling_2 varchar2(4);
  r_rkn_005            c_rkn_005%rowtype;
  l_rol_in_koppeling_1 varchar2(4);
  l_oude_indicatie     varchar2(2);
  r_rkg_001            c_rkg_001%rowtype;


begin
  /* Default uitvoer parameters */
  pov_verwerkt    := 'N' ;
  pov_foutmelding := null ;

  /* Garanderen dat doorgave op "N" staat. */
  l_oude_indicatie := rle_indicatie.doorgeven_aan_gba ;
  if not piv_ofmw_mutatie  -- xjb 5-5-2014, wel/niet doorgeven aan comp_gba nalv mutaties via PartnerschapService op de middleware
  then
    rle_indicatie.doorgeven_aan_gba := 'N' ;
  else
    rle_indicatie.doorgeven_aan_gba := 'J' ;
  end if;

  /* Bepalen relatienummers */
  rle_m06_rec_relnum( p_relext => pin_persoonsnummer
                    , p_systeemcomp => 'PRS'
                    , p_codrol => 'PR'
                    , p_datparm => sysdate
                    , p_o_relnum => l_numrelatie_1
                    ) ;

  rle_m06_rec_relnum( p_relext => pin_persoonsnummer_deelnemer
                    , p_systeemcomp => 'PRS'
                    , p_codrol => 'PR'
                    , p_datparm => sysdate
                    , p_o_relnum => l_numrelatie_2
                    ) ;

  if piv_code_soort_verwantschap = 'O'
  then
    l_rol_in_koppeling_1 := 'O' ;
    l_rol_in_koppeling_2 := 'K' ;
  elsif piv_code_soort_verwantschap = 'K'
  then
    l_rol_in_koppeling_1 := 'K' ;
    l_rol_in_koppeling_2 := 'O' ;
  elsif piv_code_soort_verwantschap in ('S', 'G', 'H')
  then
    l_rol_in_koppeling_1 := piv_code_soort_verwantschap ;
    l_rol_in_koppeling_2 := piv_code_soort_verwantschap ;
  else
    l_rol_in_koppeling_1 := null ;
    l_rol_in_koppeling_2 := null ;
  end if;

  /* extra controle */
  if   l_rol_in_koppeling_1 is null
    or l_rol_in_koppeling_2 is null
    or l_numrelatie_1 is null
    or l_numrelatie_2 is null
  then
    raise no_data_found;
  end if;

  /* update eerste relatie koppeling */
  open c_rkn_005( b_numrelatie => l_numrelatie_1
                , b_numrelatie_voor => l_numrelatie_2
                , b_begindatum => pid_dat_begin
                , b_code_rol_in_koppeling => l_rol_in_koppeling_1
                ) ;
  fetch c_rkn_005 into r_rkn_005 ;
  if c_rkn_005%notfound
  then
    close c_rkn_005 ;

    open c_rkg_001(b_code_rol_in_koppeling => l_rol_in_koppeling_1);
    fetch c_rkg_001 into r_rkg_001;
    close c_rkg_001;

    if r_rkg_001.id is null
    then
      pov_verwerkt := 'N' ;
      pov_foutmelding := 'Soort relatiekoppeling kan niet worden bepaald.' ;

      return ;
    end if ;

    insert into rle_relatie_koppelingen
      ( id
      , rkg_id
      , rle_numrelatie
      , rle_numrelatie_voor
      , dat_begin
      , dat_einde
      , code_dat_begin_fictief
      , code_dat_einde_fictief
      )
    values
      ( rle_rkn_seq1.nextval
      , r_rkg_001.id
      , l_numrelatie_1
      , l_numrelatie_2
      , pid_dat_begin
      , pid_dat_einde
      , pid_code_dat_begin_fictief
      , pid_code_dat_einde_fictief
      );
    pov_verwerkt := 'J' ;
  else
    close c_rkn_005 ;

    update rle_relatie_koppelingen rkn
    set    rkn.dat_einde = pid_dat_einde
    ,      rkn.code_dat_einde_fictief = pid_code_dat_einde_fictief
    where  rkn.id = r_rkn_005.id
    ;

    pov_verwerkt := 'J' ;
  end if ;

  /* Update tweede relatie koppeling */
  open c_rkn_005( b_numrelatie => l_numrelatie_2
                , b_numrelatie_voor => l_numrelatie_1
                , b_begindatum => pid_dat_begin
                , b_code_rol_in_koppeling => l_rol_in_koppeling_2
                ) ;
  fetch c_rkn_005 into r_rkn_005 ;
  if c_rkn_005%notfound
  then
    close c_rkn_005 ;

    pov_verwerkt := 'N' ;
    pov_foutmelding := 'Tweede relatie koppeling kan niet gevonden worden.' ;
  else
    close c_rkn_005 ;

    update rle_relatie_koppelingen  rkn
    set    rkn.dat_einde = pid_dat_einde
    ,      rkn.code_dat_einde_fictief = pid_code_dat_einde_fictief
    where  rkn.id = r_rkn_005.id
    ;

    /* pov_verwerkt staat al op "J" als beide
       koppelingen worden gevonden.
       Anders is de eerste relatie koppeling
       niet gevonden en is het toch fout.
    */
  end if;

  /* Doorgave terugzetten */
  rle_indicatie.doorgeven_aan_gba := l_oude_indicatie;
exception
  when others
  then
    /* Reset indicatie */
    rle_indicatie.doorgeven_aan_gba := 'J';

    /* Uitvoerwaarden */
    pov_verwerkt    := 'N';
    pov_foutmelding := sqlerrm;

   /* Sluiten cursors */
   if c_rkn_005%isopen
   then
      close c_rkn_005;
   end if;

end rle_gba_wijzig_verwant;
/