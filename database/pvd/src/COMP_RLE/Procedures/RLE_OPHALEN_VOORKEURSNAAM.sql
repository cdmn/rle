CREATE OR REPLACE PROCEDURE comp_rle.RLE_OPHALEN_VOORKEURSNAAM
 (P_RELATIENUMMER IN VARCHAR2
 ,P_VOORKEURSNAAM OUT VARCHAR2
 ,P_VOORVOEGSELS OUT VARCHAR2
 ,P_VOORLETTERS OUT VARCHAR2
 ,P_VOLLEDIGE_NAAM OUT VARCHAR2
 ,P_GEBOORTENAAM_TBV_VZMFAX OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
cursor c_rle(b_relatienummer number)
    is
    select v.voorkeursnaam
    ,      v.naam
    ,      v.voorkeursvoorvoegsels
    ,      v.voorvoegsels
    ,      v.voorletters
    ,      v.volledige_naam1
    from   rle_v_personen v
    where  relatienummer = b_relatienummer
    ;
    r_rle c_rle%rowtype;
-- PL/SQL Block
BEGIN
open c_rle(p_relatienummer);
    fetch c_rle into r_rle;
    if c_rle%found
    then
        p_voorkeursnaam := r_rle.voorkeursnaam;
        p_voorvoegsels := r_rle.voorkeursvoorvoegsels;
        p_voorletters := r_rle.voorletters;
        p_volledige_naam := r_rle.volledige_naam1;
        if p_voorkeursnaam <> r_rle.naam
        then
            p_geboortenaam_tbv_vzmfax := ltrim(r_rle.voorvoegsels||' '||r_rle.naam);
        else
            p_geboortenaam_tbv_vzmfax := null;
        end if;
    end if;
END RLE_OPHALEN_VOORKEURSNAAM;

 
/