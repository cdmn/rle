CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADMIN_GEGEVENS
 (P_EXTERN_NR IN VARCHAR2
 ,P_NUMRELATIE OUT NUMBER
 ,P_NAMRELATIE OUT VARCHAR2
 ,P_STRAAT OUT varchar2
 ,P_HUISNR OUT number
 ,P_TOEVNUM OUT varchar2
 ,P_POSTCODE OUT varchar2
 ,P_WOONPLAATS OUT varchar2
 ,P_LAND OUT varchar2
 ,P_NUMCOMMUNICATIE OUT varchar2
 ,P_NAAM_CONTACTPERS OUT varchar2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen contactpersoon */
CURSOR C_CPN_001
 (B_RAS_ID IN number
 )
 IS
/* C_CPN_001 */
select  rle_numrelatie
from     rle_contactpersonen
where  ras_id = b_ras_id
;
/* Ophalen correspondentie- of feitelijk adres voor een relatie */
CURSOR C_RAS_032
 (B_NUMRELATIE IN number
 ,B_SOORT_ADRES IN VARCHAR2
 )
 IS
/* C_RAS_032 */
select ads_numadres
,            id
from    rle_relatie_adressen
where rle_numrelatie = b_numrelatie
and      dwe_wrddom_srtadr = b_soort_adres
AND   DATINGANG <= SYSDATE                 -- emh 23-02-2004 call 92881
AND   NVL(DATEINDE,SYSDATE) >= SYSDATE     -- emh 23-02-2004 call 92881
;
/* Ophalen relatienummer bij reco */
CURSOR C_REC_013
 (B_CODROL VARCHAR2
 ,B_CODEXTERN VARCHAR2
 )
 IS
/* C_REC_013 */
select *
from rle_relatie_externe_codes rec
where rec.rol_codrol = b_codrol
and rec.extern_relatie = b_codextern
order by rec.datingang desc;
-- Program Data
L_CODROL VARCHAR2(20) := 'AK';
L_O_OPMAAKNAAM VARCHAR2(120);
L_O_MELDING VARCHAR2(40);
L_COD_OPMAAK VARCHAR2(20) := '2';
L_SOORT_ADRES VARCHAR2(30) := 'CA';
L_DUMMY VARCHAR2(100);
R_REC_013 C_REC_013%ROWTYPE;
L_NUMADRES NUMBER;
L_RAS_ID NUMBER;
L_NUMRELATIE_CPN NUMBER;
L_AANTPOS NUMBER := 40;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* ophalen relatienummer bij het externe nummer */
FOR r_rec_013 IN c_rec_013(l_codrol, p_extern_nr) LOOP
       p_numrelatie := r_rec_013.rle_numrelatie;
END LOOP;
IF p_numrelatie IS NOT NULL
THEN
   /* ophalen naam */
   p_namrelatie := rle_get_nam(p_numrelatie);
   /* ophalen adresnummer */
  OPEN  c_ras_032 (p_numrelatie
                  ,l_soort_adres);
  FETCH c_ras_032 INTO   l_numadres
                		, l_ras_id;
     IF c_ras_032%NOTFOUND
     THEN
        CLOSE c_ras_032;
        L_SOORT_ADRES := 'FA';
        OPEN  c_ras_032 (p_numrelatie
                        ,l_soort_adres);
        FETCH c_ras_032 INTO   l_numadres
                	         , l_ras_id;
        CLOSE c_ras_032;
    END IF;
END IF;
/* adresgegevens ophalen via adresnummer */
IF l_numadres IS NOT NULL
THEN
   rle_get_adres(l_numadres
	,p_postcode
   	,p_huisnr
	,p_toevnum
	,p_woonplaats
	,p_straat
	,p_land
 	);
END IF;
/* ophalen relatienummer contactpersoon */
OPEN  c_cpn_001 (l_ras_id);
FETCH c_cpn_001 INTO l_numrelatie_cpn;
CLOSE c_cpn_001;
/* ophalen naam contactpersoon */
IF l_numrelatie_cpn IS NOT NULL
THEN
   /* ophalen naam contactpersoon */
   rle_m11_rle_naw(l_aantpos
		 , l_numrelatie_cpn
 		 , l_o_opmaaknaam
		 , l_o_melding
		 , l_cod_opmaak
 				 );
   p_naam_contactpers := l_o_opmaaknaam;
   rle_m30_comnum(L_NUMRELATIE_CPN
	, 'CP'
 	, 'TEL'
 	, SYSDATE
 	, p_numcommunicatie
 	, l_dummy
 	, l_dummy
 	);
END IF;
/* er is geen tel nr bij de contact prs dus geef die van het AK */
IF P_NUMCOMMUNICATIE IS NULL THEN
   /* ophalen telnr administratiekantoor */
   rle_m30_comnum(P_NUMRELATIE
	, L_CODROL
 	, 'TEL'
 	, SYSDATE
 	, p_numcommunicatie
 	, l_dummy
 	, l_dummy
 	);
END IF;
END RLE_ADMIN_GEGEVENS;

 
/