CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPD_RELATIE
 (P_RELATIENUMMER IN NUMBER
 ,P_ACHTERVOEGSEL IN VARCHAR2
 ,P_BURGERLIJKE_STAAT IN VARCHAR2
 ,P_CODE_FICTIEVE_GEBDAT IN VARCHAR2
 ,P_CODE_NAAMGEBRUIK IN VARCHAR2
 ,P_DATUM_EMIGRATIE IN DATE
 ,P_GEBOORTEDATUM IN DATE
 ,P_DATUM_OPRICHTING IN DATE
 ,P_DATUM_OVERLIJDEN IN DATE
 ,P_GESLACHT IN VARCHAR2
 ,P_GEWENSTE_AANSCHRIJFTITEL IN VARCHAR2
 ,P_HANDELSNAAM IN VARCHAR2
 ,P_NAAM IN VARCHAR2
 ,P_TITEL IN VARCHAR2
 ,P_TUSSENTITEL IN VARCHAR2
 ,P_VOORLETTERS IN VARCHAR2
 ,P_VOORNAAM IN VARCHAR2
 ,P_VOORVOEGSELS IN VARCHAR2
 ,P_VOORVOEGSELS_PARTNER IN VARCHAR2
 )
 IS
cursor c_rle
    ( b_relatienummer number
    )
  is
  select *
  from   rle_relaties
  where  numrelatie = b_relatienummer
  ;
  --
  r_rle c_rle%rowtype;
  --
  cursor c_rrl
    ( b_relatienummer number
    )
  is
  select *
  from   rle_relatie_rollen
  where  rle_numrelatie = b_relatienummer
  ;
  --
  r_rrl c_rrl%rowtype;
  r_rrl2 c_rrl%rowtype;
  r_rrl3 c_rrl%rowtype;
  --
  cursor c_dom
    ( b_domeincode varchar2
    , b_waarde varchar2
    )
  is
  select code
  from   domeinwaarden
  where  waarde = b_waarde
  ;
  --
  l_dummy varchar2(100);
  l_achtervoegsel varchar2(100);
  l_burgerlijke_staat varchar2(100);
  l_gewenste_aanschrijftitel varchar2(100);
  l_titel varchar2(100);
  l_tussentitel varchar2(100);
  l_voorvoegsels varchar2(100);
  l_voorvoegsels_partner varchar2(100);
  l_indinstelling varchar2(1);
begin
  open c_rle(p_relatienummer);
  fetch c_rle into r_rle;
  if c_rle%notfound
  then
    close c_rle;
    raise_application_error ( -20001, 'RLE-00618' );
  end if;
  close c_rle;
  --
  open c_rrl(p_relatienummer);
  fetch c_rrl into r_rrl;
  if c_rrl%found
  then
    fetch c_rrl into r_rrl2;
    if c_rrl%found
    then
      fetch c_rrl into r_rrl3;
    end if;
  end if;
  close c_rrl;
  --
  if p_achtervoegsel is not null
  then
    open c_dom ( 'AVL', p_achtervoegsel );
    fetch c_dom into l_achtervoegsel;
    if c_dom%notfound
    then
      raise_application_error ( -20001, 'RLE-00619' );
    end if;
    close c_dom;
  end if;
  --
  if p_burgerlijke_staat is not null
  then
    open c_dom ( 'BST', p_burgerlijke_staat );
    fetch c_dom into l_burgerlijke_staat;
    if c_dom%notfound
    then
      raise_application_error ( -20001, 'RLE-00620' );
    end if;
    close c_dom;
  end if;
  --
  if p_code_naamgebruik is not null
    and p_code_naamgebruik not in ('E', 'P', 'V', 'N')
  then
    raise_application_error ( -20001, 'RLE-00621' );
  end if;
  --
  if p_geslacht is not null
    and p_geslacht not in ('O', 'M', 'V')
  then
    raise_application_error ( -20001, 'RLE-00622' );
  end if;
  --
  if p_gewenste_aanschrijftitel is not null
  then
    open c_dom ( 'TIL', p_gewenste_aanschrijftitel );
    fetch c_dom into l_gewenste_aanschrijftitel;
    if c_dom%notfound
    then
      raise_application_error ( -20001, 'RLE-00623' );
    end if;
    close c_dom;
  end if;
  --
  if p_titel is not null
  then
    open c_dom ( 'TIL', p_titel );
    fetch c_dom into l_titel;
    if c_dom%notfound
    then
      raise_application_error ( -20001, 'RLE-00624' );
    end if;
    close c_dom;
  end if;
  --
  if p_tussentitel is not null
  then
    open c_dom ( 'TTL', p_tussentitel );
    fetch c_dom into l_tussentitel;
    if c_dom%notfound
    then
      raise_application_error ( -20001, 'RLE-00625' );
    end if;
    close c_dom;
  end if;
  --
  if p_voorvoegsels is not null
  then
    open c_dom ( 'VVL', p_voorvoegsels );
    fetch c_dom into l_voorvoegsels;
    if c_dom%notfound
    then
      raise_application_error ( -20001, 'RLE-00626' );
    end if;
    close c_dom;
  end if;
  --
  if p_voorvoegsels_partner is not null
  then
    open c_dom ( 'VVL', p_voorvoegsels_partner );
    fetch c_dom into l_voorvoegsels_partner;
    if c_dom%notfound
    then
      raise_application_error ( -20001, 'RLE-00627' );
    end if;
    close c_dom;
  end if;
  --
  delete rle_relatie_rollen
  where rle_numrelatie = r_rle.numrelatie;
  --
  delete rle_relaties
  where  numrelatie = r_rle.numrelatie;
  --
  if p_geboortedatum is null and p_voorletters is null
  then
    l_indinstelling := 'J';
  else
    l_indinstelling := 'N';
  end if;
  insert into rle_relaties
    ( NUMRELATIE
    , CODORGANISATIE
    , NAMRELATIE
    , ZOEKNAAM
    , INDGEWNAAM
    , INDINSTELLING
    , DATSCHONING
    , DWE_WRDDOM_TITEL
    , DWE_WRDDOM_TTITEL
    , DWE_WRDDOM_AVGSL
    , DWE_WRDDOM_VVGSL
    , DWE_WRDDOM_BRGST
    , DWE_WRDDOM_GESLACHT
    , DWE_WRDDOM_GEWATITEL
    , DWE_WRDDOM_RVORM
    , VOORNAAM
    , VOORLETTER
    , AANSCHRIJFNAAM
    , DATGEBOORTE
    , CODE_GEBDAT_FICTIEF
    , DATOVERLIJDEN
    , DATOPRICHTING
    , DATBEGIN
    , DATCONTROLE
    , DATEINDE
    , INDCONTROLE
    , REGDAT_OVERLBEVEST
    , DATEMIGRATIE
    , NAAM_PARTNER
    , VVGSL_PARTNER
    , CODE_AANDUIDING_NAAMGEBRUIK
    , HANDELSNAAM
    )
  values
    ( r_rle.NUMRELATIE
    , r_rle.CODORGANISATIE
    , p_naam
    , r_rle.ZOEKNAAM
    , r_rle.INDGEWNAAM
    , l_indinstelling
    , r_rle.DATSCHONING
    , l_titel
    , l_tussentitel
    , l_achtervoegsel
    , l_voorvoegsels
    , l_burgerlijke_staat
    , p_geslacht
    , l_gewenste_aanschrijftitel
    , r_rle.DWE_WRDDOM_RVORM
    , p_voornaam
    , p_voorletters
    , r_rle.AANSCHRIJFNAAM
    , p_geboortedatum
    , p_code_fictieve_gebdat
    , p_datum_overlijden
    , p_datum_oprichting
    , r_rle.DATBEGIN
    , r_rle.DATCONTROLE
    , r_rle.DATEINDE
    , r_rle.INDCONTROLE
    , r_rle.REGDAT_OVERLBEVEST
    , p_datum_emigratie
    , r_rle.NAAM_PARTNER
    , r_rle.VVGSL_PARTNER
    , p_code_naamgebruik
    , p_handelsnaam
  );
  --
  if r_rrl.rol_codrol is not null
  then
    insert into rle_relatie_rollen
      ( RLE_NUMRELATIE
      , ROL_CODROL
      , CODORGANISATIE
      , CODPRESENTATIE
      , DATSCHONING
      )
    values
      ( r_rrl.RLE_NUMRELATIE
      , r_rrl.ROL_CODROL
      , r_rrl.CODORGANISATIE
      , r_rrl.CODPRESENTATIE
      , r_rrl.DATSCHONING
      );
  end if;
  --
  if r_rrl2.rol_codrol is not null
  then
    insert into rle_relatie_rollen
      ( RLE_NUMRELATIE
      , ROL_CODROL
      , CODORGANISATIE
      , CODPRESENTATIE
      , DATSCHONING
      )
    values
      ( r_rrl2.RLE_NUMRELATIE
      , r_rrl2.ROL_CODROL
      , r_rrl2.CODORGANISATIE
      , r_rrl2.CODPRESENTATIE
      , r_rrl2.DATSCHONING
      );
  end if;
  --
  if r_rrl3.rol_codrol is not null
  then
    insert into rle_relatie_rollen
      ( RLE_NUMRELATIE
      , ROL_CODROL
      , CODORGANISATIE
      , CODPRESENTATIE
      , DATSCHONING
      )
    values
      ( r_rrl3.RLE_NUMRELATIE
      , r_rrl3.ROL_CODROL
      , r_rrl3.CODORGANISATIE
      , r_rrl3.CODPRESENTATIE
      , r_rrl3.DATSCHONING
      );
  end if;
end;

 
/