CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_GET_STATUS
 (PIN_PERSOONSNUMMER IN NUMBER
 ,PIN_NUMRELATIE IN NUMBER
 ,POV_GBA_STATUS OUT VARCHAR2
 ,POV_GBA_AFNEMERS_INDICATIE OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Zoeken afstemming gba met NUMRELATIE */
CURSOR C_ASA_001
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT *
FROM   rle_afstemmingen_gba  asa
WHERE  asa.rle_numrelatie = cpn_numrelatie
;
-- Program Data
R_ASA_001 C_ASA_001%ROWTYPE;
OTHERS EXCEPTION;
LN_NUMRELATIE NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default uitvoerwaarden */
   pov_gba_status             := NULL ;
   pov_gba_afnemers_indicatie := NULL ;
   /* Bepaal relatienummer */
   IF pin_numrelatie IS NOT NULL
   THEN
      ln_numrelatie := pin_numrelatie ;
   ELSE
      rle_m06_rec_relnum( pin_persoonsnummer
                        , 'PRS'
                        , 'PR'
                        , SYSDATE
                        , ln_numrelatie
                        ) ;
   END IF ;
   /* Controle */
   IF ln_numrelatie IS NULL
   THEN
      RETURN ;
   END IF ;
   /* Zoek afstemmingsgegevens */
   OPEN c_asa_001( ln_numrelatie ) ;
   FETCH c_asa_001 INTO r_asa_001 ;
   IF c_asa_001%FOUND
   THEN
      pov_gba_status             := r_asa_001.code_gba_status ;
      pov_gba_afnemers_indicatie := r_asa_001.afnemers_indicatie ;
   END IF ;
   CLOSE c_asa_001 ;
EXCEPTION
 WHEN OTHERS THEN
     /* Default waarden */
   pov_gba_status             := NULL ;
   pov_gba_afnemers_indicatie := NULL ;
END RLE_GBA_GET_STATUS;

 
/