CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_FNR_AI_SEPA
 (P_NUMRELATIE IN NUMBER
 ,P_PEILDATUM IN DATE
 ,P_NUMREKENING_AI OUT VARCHAR2
 ,P_BIC_AI OUT VARCHAR2
 ,P_IBAN_AI OUT VARCHAR2
 ,P_BET_WIJZE_AI OUT VARCHAR2
 ,P_NUMREKENING_AG OUT VARCHAR2
 ,P_BIC_AG OUT VARCHAR2
 ,P_IBAN_AG OUT VARCHAR2
 ,P_BET_WIJZE_AG OUT VARCHAR2
 )
 IS

-- PL/SQL Specification
cursor c_fnr_ai (b_numrelatie in number
             ,b_peildatum  in date
             )
is
select fnr.*
from  rle_financieel_nummers fnr
where fnr.datingang <= b_peildatum
and   nvl(fnr.dateinde,b_peildatum) >= b_peildatum
and   fnr.rle_numrelatie = b_numrelatie
and   fnr.codwijzebetaling in ('AI', 'AS')
order by datingang desc;
--
cursor c_fnr_ag (b_numrelatie in number
             ,b_peildatum  in date
             )
is
select fnr.*
from  rle_financieel_nummers fnr
where fnr.datingang <= b_peildatum
and   nvl(fnr.dateinde,b_peildatum) >= b_peildatum
and   fnr.rle_numrelatie = b_numrelatie
and   fnr.codwijzebetaling in ('AG', 'SB')
order by datingang desc;
--
cursor c_fnr (b_numrelatie in number
             ,b_peildatum  in date
             )
is
select fnr.*
from  rle_financieel_nummers fnr
where fnr.datingang <= b_peildatum
and   nvl(fnr.dateinde,b_peildatum) >= b_peildatum
and   fnr.rle_numrelatie = b_numrelatie
and   fnr.codwijzebetaling is null
order by datingang desc;
r_fnr_ai      c_fnr_ai%rowtype;
r_fnr_ag     c_fnr_ag%rowtype;
r_fnr            c_fnr%rowtype;
-- PL/SQL Block
BEGIN
    open c_fnr_ai(p_numrelatie
                 ,p_peildatum
                 );
    fetch c_fnr_ai into r_fnr_ai;
    IF c_fnr_ai%found
    THEN
       P_NUMREKENING_AI := r_fnr_ai.numrekening;
       P_BIC_AI := r_fnr_ai.BIC;
       P_IBAN_AI := r_fnr_ai.IBAN;
       P_BET_WIJZE_AI := r_fnr_ai.codwijzebetaling;
    ELSE
       P_NUMREKENING_AI := null;
       P_BIC_AI := null;
       P_IBAN_AI := null;
       P_BET_WIJZE_AI := null;
    END IF;
    close c_fnr_ai;
    --
    open c_fnr_ag(p_numrelatie
                 ,p_peildatum
                 );
    fetch c_fnr_ag into r_fnr_ag;
    IF c_fnr_ag%found
    THEN
       P_NUMREKENING_AG := r_fnr_ag.numrekening;
       P_BIC_AG := r_fnr_ag.BIC;
       P_IBAN_AG := r_fnr_ag.IBAN;
       P_BET_WIJZE_AG := r_fnr_ag.codwijzebetaling;
    ELSE
       P_NUMREKENING_AG := null;
       P_BIC_AG := null;
       P_IBAN_AG := null;
       P_BET_WIJZE_AG := null;
    END IF;
    close c_fnr_ag;
    --
    IF p_numrekening_ag is null
       and p_bic_ag is null
       and p_iban_ag is null
    then
       open c_fnr(p_numrelatie
                 ,p_peildatum
                 );
       fetch c_fnr into r_fnr;
       IF c_fnr%found
       THEN
          P_NUMREKENING_AG := r_fnr.numrekening;
          P_BIC_AG := r_fnr.BIC;
          P_IBAN_AG := r_fnr.IBAN;
          P_BET_WIJZE_AG := r_fnr.codwijzebetaling;
       END IF;
    end if;
END;
 
/