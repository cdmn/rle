CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_PERSGEGEVENS
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_NAMRELATIE OUT RLE_RELATIES.NAMRELATIE%TYPE
 ,P_INDINSTELLING OUT RLE_RELATIES.INDINSTELLING%TYPE
 ,P_COD_GESLACHT OUT RLE_RELATIES.DWE_WRDDOM_GESLACHT%TYPE
 ,P_OMS_GESLACHT OUT VARCHAR2
 ,P_COD_VVGSL OUT RLE_RELATIES.DWE_WRDDOM_VVGSL%TYPE
 ,P_OMS_VVGSL OUT VARCHAR2
 ,P_VOORLETTER OUT RLE_RELATIES.VOORLETTER%TYPE
 ,P_VOORNAAM OUT RLE_RELATIES.VOORNAAM%TYPE
 ,P_DATGEBOORTE OUT RLE_RELATIES.DATGEBOORTE%TYPE
 ,P_COD_DATGEB_FICTIEF OUT RLE_RELATIES.CODE_GEBDAT_FICTIEF%TYPE
 ,P_OMS_DATGEB_FICTIEF OUT VARCHAR2
 ,P_INDCONTROLE OUT RLE_RELATIES.INDCONTROLE%TYPE
 ,P_DATCONTROLE OUT RLE_RELATIES.DATCONTROLE%TYPE
 ,P_COD_NAAMGEBRUIK OUT RLE_RELATIES.CODE_AANDUIDING_NAAMGEBRUIK%TYPE
 ,P_OMS_NAAMGEBRUIK OUT VARCHAR2
 ,P_COD_AANSCHRIJFTITEL OUT RLE_RELATIES.DWE_WRDDOM_GEWATITEL%TYPE
 ,P_OMS_AANSCHRIJFTITEL OUT VARCHAR2
 ,P_DATOVERLIJDEN OUT DATE
 ,P_REGDATUM_OVERLBEVEST OUT DATE
 ,P_DATEMIGRATIE OUT DATE
 ,P_DAT_CREATIE OUT DATE
 ,P_CREATIE_DOOR OUT VARCHAR2
 ,P_SOFINUMMER OUT VARCHAR2
 ,P_NAAM_PARTNER OUT VARCHAR2
 ,P_VOORVOEGSEL_PARTNER OUT VARCHAR2
 ,P_PERSOONSNR IN VARCHAR2
 ,P_IND_PORTAL IN VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_028
 (B_NUMRELATIE IN NUMBER
 ,B_PERSOONSNR IN VARCHAR2
 )
 IS
/* C_RLE_028 */
SELECT /*+ INDEX(sof RLE_REC_RLE_FK1) */  rel.*
,       sof.extern_relatie sofi_nummer
FROM    rle_relatie_externe_codes sof
,       rle_relaties              rel
WHERE   rel.numrelatie		   IN
(	SELECT  prs.rle_numrelatie
      FROM	  rle_relatie_externe_codes prs
	WHERE	  prs.extern_relatie    = b_persoonsnr
	AND     prs.dwe_wrddom_extsys = 'PRS'
	AND     prs.rol_codrol      = 'PR'
	AND     prs.datingang     <= TRUNC(SYSDATE)
	AND     NVL(prs.dateinde,TRUNC(SYSDATE))
            	              >= TRUNC(SYSDATE)
      UNION
      SELECT  b_numrelatie
      FROM    dual
)
AND     sof.dwe_wrddom_extsys(+) = 'SOFI'
AND     sof.rol_codrol(+)        = 'PR'
AND     sof.datingang(+)        <= TRUNC(SYSDATE)
AND     NVL(sof.dateinde(+),TRUNC(SYSDATE))
                                >= TRUNC(SYSDATE)
AND     sof.rle_numrelatie(+)    = rel.numrelatie;
r_rle_028 c_rle_028%rowtype;
-- Program Data
L_NUMRELATIE NUMBER;
L_IND_IBM VARCHAR2(1) := 'N';
L_LENGTE_IBM NUMBER := 100;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
OPEN c_rle_028( p_numrelatie, p_persoonsnr);
   FETCH c_rle_028 INTO r_rle_028;
   IF  c_rle_028%FOUND
   AND r_rle_028.indinstelling = 'N'   -- Alleen personen
   THEN
      p_namrelatie           := r_rle_028.namrelatie                  ;
      p_indinstelling        := r_rle_028.indinstelling               ;
      p_cod_geslacht         := r_rle_028.dwe_wrddom_geslacht         ;
      p_cod_vvgsl            := r_rle_028.dwe_wrddom_vvgsl            ;
      p_voorletter           := r_rle_028.voorletter                  ;
      p_voornaam             := r_rle_028.voornaam                    ;
      p_datgeboorte          := r_rle_028.datgeboorte                 ;
      p_cod_datgeb_fictief   := r_rle_028.code_gebdat_fictief         ;
      p_indcontrole          := r_rle_028.indcontrole                 ;
      p_datcontrole          := r_rle_028.datcontrole                 ;
      p_cod_naamgebruik      := r_rle_028.code_aanduiding_naamgebruik ;
      p_cod_aanschrijftitel  := r_rle_028.dwe_wrddom_gewatitel        ;
      p_datoverlijden	     := r_rle_028.datoverlijden		   ;
      p_regdatum_overlbevest := r_rle_028.regdat_overlbevest ;
      p_datemigratie         := r_rle_028.datemigratie ;
      p_dat_creatie          := r_rle_028.dat_creatie ;
      p_creatie_door         := r_rle_028.creatie_door ;
	p_naam_partner	     := r_rle_028.naam_partner;
	p_voorvoegsel_partner  := r_rle_028.vvgsl_partner;
	p_sofinummer	     := r_rle_028.sofi_nummer;
      IF p_ind_portal = 'N'
      THEN
        ibm.ibm( 'RFW_GET_WRD'
             , 'RLE'
             , 'GES'
             , p_cod_geslacht
             , l_ind_ibm
             , l_lengte_ibm
             , p_oms_geslacht
             ) ;
        ibm.ibm( 'RFW_GET_WRD'
             , 'RLE'
             , 'FGD'
             , p_cod_datgeb_fictief
             , l_ind_ibm
             , l_lengte_ibm
             , p_oms_datgeb_fictief
             ) ;
        ibm.ibm( 'RFW_GET_WRD'
             , 'RLE'
             , 'ANG'
             , p_cod_naamgebruik
             , l_ind_ibm
             , l_lengte_ibm
             , p_oms_naamgebruik
             ) ;
        ibm.ibm( 'RFW_GET_WRD'
             , 'RLE'
             , 'GWT'
             , p_cod_aanschrijftitel
             , l_ind_ibm
             , l_lengte_ibm
             , p_oms_aanschrijftitel
             ) ;
      END IF;
      IF p_cod_vvgsl IS NOT NULL THEN
        IF p_ind_portal = 'N'
        THEN
          ibm.ibm( 'RFW_GET_WRD'
               , 'RLE'
               , 'VVL'
               , p_cod_vvgsl
               , l_ind_ibm
               , l_lengte_ibm
               , p_oms_vvgsl
               ) ;
        ELSE
          ibmportal.rfw_get_wrd ( 'VVL'
               , p_cod_vvgsl
               , l_ind_ibm
               , l_lengte_ibm
               , p_oms_vvgsl
               ) ;
        END IF;
      ELSE
        p_oms_vvgsl         := NULL;
      END IF;
   ELSE
     p_namrelatie           := NULL ;
     p_indinstelling        := NULL ;
     p_cod_geslacht         := NULL ;
     p_oms_geslacht         := NULL ;
     p_cod_vvgsl            := NULL ;
     p_oms_vvgsl            := NULL ;
     p_voorletter           := NULL ;
     p_voornaam             := NULL ;
     p_datgeboorte          := NULL ;
     p_cod_datgeb_fictief   := NULL ;
     p_oms_datgeb_fictief   := NULL ;
     p_indcontrole          := NULL ;
     p_datcontrole          := NULL ;
     p_cod_naamgebruik      := NULL ;
     p_oms_naamgebruik      := NULL ;
     p_cod_aanschrijftitel  := NULL ;
     p_oms_aanschrijftitel  := NULL ;
     p_datoverlijden        := NULL ;
     p_regdatum_overlbevest := NULL ;
     p_datemigratie         := NULL ;
     p_dat_creatie          := NULL ;
     p_creatie_door         := NULL ;
     p_sofinummer           := NULL ;
     p_naam_partner         := NULL ;
     p_voorvoegsel_partner  := NULL ;
   END IF ;
   CLOSE c_rle_028;
END RLE_GET_PERSGEGEVENS;

 
/