CREATE OR REPLACE PROCEDURE comp_rle.RLE_STANDAARD_ADRES
 (P_OPDRACHTGEVER IN NUMBER
 ,P_PRODUCT IN VARCHAR2
 ,P_RLE_NUMRELATIE IN NUMBER
 ,P_CODROL IN VARCHAR2
 ,P_PEILDATUM IN DATE
 ,P_CODE_SOORT_ADRES OUT VARCHAR2
 ,P_POSTCODE OUT VARCHAR2
 ,P_HUISNR OUT NUMBER
 ,P_HUISNR_TOEV OUT VARCHAR2
 ,P_WOONPLAATS OUT VARCHAR2
 ,P_STRAAT OUT VARCHAR2
 ,P_CODLAND OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
--
-- H de Bruin 10-12-2002
-- zo gemaakt dat de adrestypen in rle_sadres_rollen worden afgelopen om een
-- adres te bepalen. Het was zo dat alleen maar gekeken werd naar het eerste type in rij.
-- Indien dat type adres niet aanwezig was (maar dus wel een ander type )werd niets geretourneerd.
cursor c_rol_aantal (b_rle_numrelatie in number
                    ,b_codrol         in varchar2)
is
select count(*)
from   rle_sadres_rollen  sar
,      rle_relatie_rollen rrl
where  sar.codrol = rrl.rol_codrol
and    rrl.rle_numrelatie = b_rle_numrelatie
and    rrl.rol_codrol = b_codrol
;
--
cursor c_rol(b_rle_numrelatie in number
            ,b_codrol         in varchar2)
is
select rrl.rol_codrol
,      rrl.rle_numrelatie
,      sar.dwe_wrddom_sadres     code_soort_adres
from   rle_sadres_rollen  sar
,      rle_relatie_rollen rrl
where  sar.codrol = rrl.rol_codrol
and    rrl.rle_numrelatie = b_rle_numrelatie
and    rrl.rol_codrol = b_codrol
order by sar.volgnr;
--
r_rol c_rol%rowtype;
--
cursor c_ads (b_codrol            in varchar2
             ,b_numrelatie        in number
             ,b_dwe_wrddom_srtadr in varchar2
             ,b_peildatum         in date)
is
select ads.postcode
,      ads.huisnummer
,      ads.toevhuisnum
,      ads.wps_woonplaatsid
,      ads.lnd_codland
,      ads.stt_straatid
from   rle_relatie_adressen ras
,      rle_adressen         ads
where  ads.numadres       = ras.ads_numadres
and    ras.rol_codrol     = b_codrol
and    ras.rle_numrelatie = b_numrelatie
and    ras.dwe_wrddom_srtadr = b_dwe_wrddom_srtadr
and   (ras.DATEINDE is null or ras.DATEINDE >= nvl(b_peildatum,sysdate))
and    ras.datingang =
 (select max(ras1.datingang)
  from   rle_relatie_adressen ras1
  where  ras1.rol_codrol        = ras.rol_codrol
  and    ras1.rle_numrelatie    = ras.rle_numrelatie
  and    ras1.dwe_wrddom_srtadr = ras.dwe_wrddom_srtadr
  and    ras1.datingang <= nvl(b_peildatum,sysdate));
r_ads c_ads%rowtype;
--
cursor c_stt(b_codland         varchar2
            ,b_woonplaatsid    number
            ,b_straatid        number)
is
select lnd.codland
,          lnd.naam
,      wps.woonplaats
,      stt.straatnaam
from   rle_landen lnd
,      rle_woonplaatsen wps
,      rle_straten stt
where wps.lnd_codland  = lnd.codland
and   wps.woonplaatsid = stt.wps_woonplaatsid
and   stt.straatid     = b_straatid
and   wps.woonplaatsid = b_woonplaatsid
and   lnd.codland      = b_codland;
--
r_stt                    c_stt%rowtype;
l_aantal_s_adres         number;
l_teller                 number;
geen_default             exception;
l_peildatum              date;
l_opdrachtgever          number(10);
l_gegeven_type           varchar2(10):= 'ADRES';
l_leveringsovereenkomst  varchar2(10):= 'GBA';
l_gebruik_toegestaan     varchar2(5);
--
-- PL/SQL Block
BEGIN
  l_peildatum     := sysdate;
  l_opdrachtgever := p_opdrachtgever;
  stm_util.debug('module rle_standaard_adres');
-- bepalen aantal soorten adres voor deze relatie met deze rol
  open c_rol_aantal (p_rle_numrelatie
                    ,p_codrol);
  fetch c_rol_aantal into l_aantal_s_adres;
  stm_util.debug(' l_aantal_s_adres '||l_aantal_s_adres);
  close c_rol_aantal;
-- ophalen soort adres
  open c_rol( p_rle_numrelatie
             ,p_codrol);
  for l_teller in 1..l_aantal_s_adres
  loop
    fetch c_rol into r_rol;
    if c_rol%notfound then
	  stm_util.debug(' geen rol ');
      close c_rol;
      raise geen_default;
    end if;
--  ophalen adres van het gevonden soort
    /* Controle ivm RGG bij DA code.
       In sommige gevallen -afhankelijk v/h product en opdr.gever-
       komt dus niet het DA-adres terug, maar het OA adres.
       OSP 16-06-2003
      */
    if r_rol.code_soort_adres = 'DA'
    then
       ibm.ibm ('CHK_GEGEVENSGEBRUIK'
              , 'RLE'
              , p_product
              , l_opdrachtgever
              , l_gegeven_type
              , l_leveringsovereenkomst
              , l_peildatum
              , l_gebruik_toegestaan);
          --
          if l_gebruik_toegestaan = 'N'
          then
             r_rol.code_soort_adres := 'OA';
          end if;
    end if;
    --
    open  c_ads (p_codrol
               , p_rle_numrelatie
               , r_rol.code_soort_adres
               , p_peildatum);
    fetch c_ads into r_ads;
	if c_ads%found
	then
	  -- adres is gevonden, loop mag worden verlaten
      close c_ads;
	  exit;
	else
      close c_ads;
    end if;
  end loop
  ;
  close c_rol;
--  aanvullen naw ophalen obv gevonden adres
  open  c_stt(r_ads.lnd_codland, r_ads.wps_woonplaatsid, r_ads.stt_straatid);
  fetch c_stt into r_stt;
	if c_stt%found
    then
	  	/* Indien straat,woonplaats of land onbekend zijn van het DA adres dan CA ophalen
       	*/
	  if r_rol.code_soort_adres = 'DA'
      AND UPPER(r_stt.woonplaats) !='ONBEKEND'
      AND UPPER(r_stt.straatnaam) !='ONBEKEND'
      AND UPPER(r_stt.naam      ) !='ONBEKEND'
	  then
         P_CODE_SOORT_ADRES :=  r_rol.code_soort_adres;
         P_POSTCODE         :=  r_ads.postcode;
         P_HUISNR           :=  r_ads.huisnummer;
         P_HUISNR_TOEV      :=  r_ads.toevhuisnum;
         P_WOONPLAATS       :=  r_stt.woonplaats;
         P_STRAAT           :=  r_stt.straatnaam;
         P_CODLAND          :=  r_stt.codland;
         stm_util.debug('P_CODE_SOORT_ADRES = '||P_CODE_SOORT_ADRES);
         stm_util.debug('P_POSTCODE = '||P_POSTCODE);
         stm_util.debug('P_HUISNR = '||to_char(P_HUISNR));
         stm_util.debug('P_HUISNR_TOEV = '||P_HUISNR_TOEV);
         stm_util.debug('P_WOONPLAATS = '||P_WOONPLAATS);
         stm_util.debug('P_STRAAT = '||P_STRAAT);
         stm_util.debug('P_CODLAND = '||P_CODLAND);
         stm_util.debug('Einde module rle_standaard_adres');
	  elsif r_rol.code_soort_adres = 'DA'
      AND (UPPER(r_stt.woonplaats) ='ONBEKEND'
      OR  UPPER(r_stt.straatnaam) ='ONBEKEND'
      OR  UPPER(r_stt.naam      ) ='ONBEKEND'
	  or  r_stt.woonplaats is null
      OR  r_stt.straatnaam is null
      OR  r_stt.naam       is null
	  or c_stt%notfound
	  )
	  then
	  --close c_stt;
         open  c_ads (p_codrol
                     , p_rle_numrelatie
                     ,'CA'
                     , p_peildatum);
         fetch c_ads into r_ads;
	     if c_ads%found
         THEN
		  close c_stt;
		   open  c_stt(r_ads.lnd_codland, r_ads.wps_woonplaatsid, r_ads.stt_straatid);
           fetch c_stt into r_stt;
            P_CODE_SOORT_ADRES := 'CA';--r_rol.code_soort_adres;
            P_POSTCODE         :=  r_ads.postcode;
            P_HUISNR           :=  r_ads.huisnummer;
            P_HUISNR_TOEV      :=  r_ads.toevhuisnum;
            P_WOONPLAATS       :=  r_stt.woonplaats;
            P_STRAAT           :=  r_stt.straatnaam;
            P_CODLAND          :=  r_stt.codland;
          close c_ads;
		  close c_stt;
	    else
          close c_ads;
    	end if; --haal CA op
   else
   open  c_ads (p_codrol
              , p_rle_numrelatie
              , r_rol.code_soort_adres
              , p_peildatum);
   fetch c_ads into r_ads;
      if c_ads%found
	  -- adres is gevonden, loop mag worden verlaten
      THEN
         P_CODE_SOORT_ADRES :=  r_rol.code_soort_adres;
         P_POSTCODE         :=  r_ads.postcode;
         P_HUISNR           :=  r_ads.huisnummer;
         P_HUISNR_TOEV      :=  r_ads.toevhuisnum;
         P_WOONPLAATS       :=  r_stt.woonplaats;
         P_STRAAT           :=  r_stt.straatnaam;
         P_CODLAND          :=  r_stt.codland;
         close c_ads;
       end if;--ads
     end if;-- haal DA op
   end if;

EXCEPTION
  when geen_default then
    raise_application_error(-20000, 'Geen default adresbepaling mogelijk bij rol: '||p_codrol);
END RLE_STANDAARD_ADRES;
 
/