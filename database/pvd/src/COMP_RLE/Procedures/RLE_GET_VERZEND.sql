CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_VERZEND
 (P_OPDRACHTGEVER IN NUMBER
 ,P_PRODUCT IN VARCHAR2
 ,P_NUMRELATIE IN NUMBER
 ,P_POSTSOORT IN VARCHAR2
 ,P_CODROL IN VARCHAR2
 ,P_PEILDATUM IN DATE
 ,P_NUMRELATIE_BETREFT OUT NUMBER
 ,P_NAAM_BETREFT OUT VARCHAR2
 ,P_NUMRELATIE_HOORT_BIJ OUT NUMBER
 ,P_NAAM_HOORT_BIJ OUT VARCHAR2
 ,P_CODE_SOORT_ADRES OUT VARCHAR2
 ,P_POSTCODE OUT VARCHAR2
 ,P_HUISNR OUT NUMBER
 ,P_HUISNR_TOEV OUT VARCHAR2
 ,P_WOONPLAATS OUT VARCHAR2
 ,P_STRAAT OUT VARCHAR2
 ,P_LANDNAAM OUT VARCHAR2
 )
 IS
  cursor c_rle ( b_numrelatie pls_integer )
  is
  select rle.namrelatie
  from   rle_relaties rle
  where  rle.numrelatie = b_numrelatie;

  cursor c_pat ( b_postsoort in varchar2 )
  is
    select adressoort
         , ind_dummy_adres_negeren
         , max_geldigheid
    from   rle_postsoort_adressoorten
    where  postsoort = b_postsoort
    order  by prioriteit;

  cursor c_vbg( b_numrelatie        pls_integer
              , b_code_soort_post   varchar2
              , b_codrol            varchar2
              , b_code_soort_adres  varchar2 )
  is
    select vbg.rrl_rol_codrol
         , vbg.rrl_rle_numrelatie
         , vbg.rrl_rol_codrol_hoort_bij
         , vbg.rrl_rle_numrelatie_hoort_bij
         , vbg.rrl_rol_codrol_betreft
         , vbg.rrl_rle_numrelatie_betreft
         , vbg.code_soort_adres
    from   rle_verzendbestemmingen vbg
    where  vbg.rrl_rle_numrelatie = b_numrelatie
    and    vbg.code_soort_post    = b_code_soort_post
    and    vbg.rrl_rol_codrol     = b_codrol
    and  (  vbg.code_soort_adres  = b_code_soort_adres or b_code_soort_adres is null)
     ;

  r_vbg                       c_vbg%rowtype;
  r_rle                       c_rle%rowtype;
  l_numrelatie_cpn            pls_integer;
  l_verzendnaam_cpn           varchar2(200);
  l_melding                   varchar2 ( 120 );
  l_codopmaak                 varchar2 ( 1 ) := '2';   --type opmaak
  t_dummy                     varchar2 ( 255 );
  t_dummy1                    varchar2 ( 255 );
  t_dummy2                    varchar2 ( 255 );
  t_dummy3                    varchar2 ( 255 );
  t_dummy4                    varchar2 ( 255 );
  t_dummyd                    date;
  t_woonplaats                varchar2 ( 255 );
  t_peildatum                 date;
  l_soort_adres               varchar2 ( 100 );
  l_ind_dummy_adres_negeren   varchar2 ( 1 );
  l_adres_gevonden            boolean;
  l_landcode		              varchar2(3);
  l_vbg_notfound              boolean := false;
  l_max_geldigheid           integer;
  l_ingangsdatum_adres  date;

--
  function haal_adres_op
  return boolean
  is
    l_peildatum                             date := sysdate;
    lc_gegevenstype          constant       varchar2 ( 10 ) := 'ADRES';
    lc_leveringsovereenkomst constant       varchar2 ( 10 ) := 'GBA';
    l_gebruik_toegestaan                    varchar2 ( 5 );
    l_opdrachtgever                         number := p_opdrachtgever;
  begin
    stm_util.debug('haal_adres_op, begin');

    if l_soort_adres = 'DA'
    then
      chk_geggebruik
      ( p_product       => p_product
      , p_opdrachtgever => p_opdrachtgever
      , p_gegevenstype  => lc_gegevenstype
      , p_levov_code    => lc_leveringsovereenkomst
      , p_peildatum     => l_peildatum
      , p_ind_gebruik   => l_gebruik_toegestaan
      );

      if l_gebruik_toegestaan = 'N' then
        l_soort_adres := 'OA';
      end if;
    end if;

    open  c_vbg ( b_numrelatie        => p_numrelatie
                , b_code_soort_post   => p_postsoort
                , b_codrol            => p_codrol
                , b_code_soort_adres  => l_soort_adres );
    fetch c_vbg into r_vbg;
    close c_vbg;

    stm_util.debug ('**** Controleer verzendbestemming is voor '||p_numrelatie||'/'||p_postsoort||'/'||p_codrol||'/'||l_soort_adres);
    stm_util.debug ( 'rrl_rol_codrol '||r_vbg.rrl_rol_codrol );
    stm_util.debug ( 'rrl rle numrelatie '||to_char ( r_vbg.rrl_rle_numrelatie ) );
    stm_util.debug ( 'rrl_rol_codrol_hoort_bij '||r_vbg.rrl_rol_codrol_hoort_bij );
    stm_util.debug ( 'rrl_rle_numrelatie_hoort_bij'||to_char ( r_vbg.rrl_rle_numrelatie_hoort_bij ) );
    stm_util.debug ( 'rrl_rol_codrol_betreft '||r_vbg.rrl_rol_codrol_betreft );
    stm_util.debug ( 'rrl_rle_numrelatie_betreft '||to_char ( r_vbg.rrl_rle_numrelatie_betreft ) );
    stm_util.debug ( 'code_soort_adres' ||r_vbg.code_soort_adres );

    if  r_vbg.rrl_rol_codrol_hoort_bij = 'CP' then
      l_numrelatie_cpn  := r_vbg.rrl_rle_numrelatie_hoort_bij;
      l_verzendnaam_cpn := rle_get_verzendnaam ( p_numrelatie => r_vbg.rrl_rle_numrelatie_hoort_bij );
      stm_util.debug('l_verzendnaam = '||l_verzendnaam_cpn);
    end if;


    stm_util.debug ('**** EINDE Controleer of er een verzendbestemming is voor '||p_numrelatie||'/'||p_postsoort||'/'||p_codrol||'/'||l_soort_adres);


    stm_util.debug('haal_adres_op, aanroep rle_get_adresvelden met l_soort_adres = '||l_soort_adres);
    rle_get_adrsvelden ( p_opdrachtgever  => p_opdrachtgever
                       , p_produkt        => p_product
                       , p_numrelatie     => p_numrelatie
                       , p_codrol         => p_codrol
                       , p_srtadres       => l_soort_adres
                       , p_datump         => t_peildatum
                       , p_postcode       => p_postcode
                       , p_huisnr         => p_huisnr
                       , p_toevnum        => p_huisnr_toev
                       , p_woonplaats     => p_woonplaats
                       , p_straat         => p_straat
                       , p_codland        => l_landcode
                       , p_locatie        => t_dummy
                       , p_ind_woonboot   => t_dummy1
                       , p_ind_woonwagen  => t_dummy2
                       , p_landnaam       => t_dummy3
                       , p_begindatum     => l_ingangsdatum_adres
                       , p_einddatum      => t_dummyd
                       );
    stm_util.debug('haal_adres_op, einde');
    stm_util.debug('t_dummy = '||t_dummy);
    stm_util.debug('t_dummy1 = '||t_dummy1);
    stm_util.debug('t_dummy2 = '||t_dummy2);
    stm_util.debug('t_dummy3 = '||t_dummy3);
    stm_util.debug('t_dummy4 = '||t_dummy4);

    if p_woonplaats is null then
      return false;
    else
      return true;
    end if;
  end;

  --
  procedure haal_adres_op_oude_wijze
  is
  begin
    stm_Util.debug('haal_adres_op_oude_wijze, begin');
    t_peildatum := nvl ( p_peildatum, sysdate );
    stm_util.debug (    'P_NUMRELATIE : ' || to_char ( p_numrelatie ) );
    stm_util.debug (    'P_POSTSOORT : '  || p_postsoort );
    stm_util.debug (    'P_CODROL : '     || p_codrol );
    stm_util.debug (    'T_PEILDATUM : '  || to_char ( t_peildatum, 'dd-mm-yyyy' ) );

    open  c_vbg ( p_numrelatie, p_postsoort, p_codrol, null );
    fetch c_vbg into r_vbg;
    l_vbg_notfound := c_vbg%notfound;
    close c_vbg;

    stm_util.debug ( r_vbg.rrl_rol_codrol );
    stm_util.debug ( to_char ( r_vbg.rrl_rle_numrelatie ) );
    stm_util.debug ( r_vbg.rrl_rol_codrol_hoort_bij );
    stm_util.debug ( to_char ( r_vbg.rrl_rle_numrelatie_hoort_bij ) );
    stm_util.debug ( r_vbg.rrl_rol_codrol_betreft );
    stm_util.debug ( to_char ( r_vbg.rrl_rle_numrelatie_betreft ) );
    stm_util.debug ( r_vbg.code_soort_adres );

    if l_vbg_notfound then
      stm_util.debug ( 'geen verzendbestemming gevondend' );

      stm_util.debug ( 'Standaard adres' );
      -- Controle ivm RGG zit in de aangeroepen module.
      -- In sommige gevallen -afhankelijk v/h product en opdr.gever-
      -- komt dus niet het DA-adres terug, maar het OA adres.
      -- OSP 16-06-2003

      rle_standaard_adres ( p_opdrachtgever     => p_opdrachtgever
                          , p_product           => p_product
                          , p_rle_numrelatie    => p_numrelatie
                          , p_codrol            => p_codrol
                          , p_peildatum         => t_peildatum
                          , p_code_soort_adres  => p_code_soort_adres
                          , p_postcode          => p_postcode
                          , p_huisnr            => p_huisnr
                          , p_huisnr_toev       => p_huisnr_toev
                          , p_woonplaats        => p_woonplaats
                          , p_straat            => p_straat
                          , p_codland           => l_landcode
                          );

      open  c_rle( b_numrelatie => p_numrelatie );
      fetch c_rle into r_rle;
      close c_rle;

      p_numrelatie_betreft    := p_numrelatie;
      p_naam_betreft          := r_rle.namrelatie;
      p_numrelatie_hoort_bij  := null;
      p_naam_hoort_bij        := null;
    else
      stm_util.debug ( 'Niet standaard adres' );

      p_code_soort_adres := r_vbg.code_soort_adres;

      if( r_vbg.rrl_rol_codrol     is not null and
          r_vbg.rrl_rle_numrelatie is not null ) and
        ( r_vbg.rrl_rol_codrol_betreft     is null and
          r_vbg.rrl_rle_numrelatie_betreft is null ) and
        ( r_vbg.rrl_rol_codrol_hoort_bij      is null and
          r_vbg.rrl_rle_numrelatie_hoort_bij  is null )
      then  --werkgever

        stm_util.debug ( 'werkgever' );
        open  c_rle ( b_numrelatie => r_vbg.rrl_rle_numrelatie );
        fetch c_rle into r_rle;
        close c_rle;

        p_numrelatie_betreft    := r_vbg.rrl_rle_numrelatie;
        p_naam_betreft          := r_rle.namrelatie;
        p_numrelatie_hoort_bij  := null;
        p_naam_hoort_bij        := null;
        -- Controle ivm RGG zit in de aangeroepen module.
        -- In sommige gevallen -afhankelijk v/h product en opdr.gever-
        -- komt dus niet het DA-adres terug, maar het OA adres.
        -- OSP 16-06-2003

        rle_get_adrsvelden ( p_opdrachtgever  => p_opdrachtgever
                           , p_produkt        => p_product
                           , p_numrelatie     => r_vbg.rrl_rle_numrelatie
                           , p_codrol         => r_vbg.rrl_rol_codrol
                           , p_srtadres       => r_vbg.code_soort_adres
                           , p_datump         => t_peildatum
                           , p_postcode       => p_postcode
                           , p_huisnr         => p_huisnr
                           , p_toevnum        => p_huisnr_toev
                           , p_woonplaats     => p_woonplaats
                           , p_straat         => p_straat
                           , p_codland        => l_landcode
                           , p_locatie        => t_dummy
                           , p_ind_woonboot   => t_dummy
                           , p_ind_woonwagen  => t_dummy
                           , p_landnaam       => t_dummy
                           , p_begindatum     => t_dummyd
                           , p_einddatum      => t_dummyd
                           );

      elsif ( r_vbg.rrl_rol_codrol      is not null and
              r_vbg.rrl_rle_numrelatie  is not null ) and
            ( r_vbg.rrl_rol_codrol_betreft      is null and
              r_vbg.rrl_rle_numrelatie_betreft  is null ) and
            ( r_vbg.rrl_rol_codrol_hoort_bij      is not null and
              r_vbg.rrl_rle_numrelatie_hoort_bij  is not null )
      then  -- Contactpersoon bij werkgever
        stm_util.debug ( 'Contactpersoon bij werkgever' );
        open  c_rle ( b_numrelatie => r_vbg.rrl_rle_numrelatie );
        fetch c_rle into r_rle;
        close c_rle;

        p_numrelatie_betreft  := r_vbg.rrl_rle_numrelatie;
        p_naam_betreft        := r_rle.namrelatie;
        stm_util.debug ( 'voor rle_get_adrsvelden' );
        stm_util.debug ( 'Input parameter r_vbg.rrl_rle_numrelatie: '|| to_char( r_vbg.rrl_rle_numrelatie ));
        stm_util.debug ( 'Input parameter r_vbg.rrl_rol_codrol: '|| r_vbg.rrl_rol_codrol);
        stm_util.debug ( 'Input parameter p_code_soort_adres: '|| p_code_soort_adres);
        stm_util.debug ( 'Input parameter t_peildatum: '|| to_char( t_peildatum, 'dd-mm-yyyy' ));

        -- Controle ivm RGG zit in de aangeroepen module.
        -- In sommige gevallen -afhankelijk v/h product en opdr.gever-
        -- komt dus niet het DA-adres terug, maar het OA adres.
        -- OSP 16-06-2003

        rle_get_adrsvelden ( p_opdrachtgever  => p_opdrachtgever
                           , p_produkt        => p_product
                           , p_numrelatie     => r_vbg.rrl_rle_numrelatie
                           , p_codrol         => r_vbg.rrl_rol_codrol
                           , p_srtadres       => r_vbg.code_soort_adres
                           , p_datump         => t_peildatum
                           , p_postcode       => p_postcode
                           , p_huisnr         => p_huisnr
                           , p_toevnum        => p_huisnr_toev
                           , p_woonplaats     => p_woonplaats
                           , p_straat         => p_straat
                           , p_codland        => l_landcode
                           , p_locatie        => t_dummy
                           , p_ind_woonboot   => t_dummy
                           , p_ind_woonwagen  => t_dummy
                           , p_landnaam       => t_dummy
                           , p_begindatum     => t_dummyd
                           , p_einddatum      => t_dummyd
                           );

        rle_m11_rle_naw ( p_aantpos       => 40
                        , p_relnum        => r_vbg.rrl_rle_numrelatie_hoort_bij
                        , p_o_opmaaknaam  => p_naam_hoort_bij
                        , p_o_melding     => l_melding
                        , p_cod_opmaak    => l_codopmaak
                        );
        p_numrelatie_hoort_bij := r_vbg.rrl_rle_numrelatie_hoort_bij;
      elsif ( r_vbg.rrl_rol_codrol_betreft      is not null and
              r_vbg.rrl_rle_numrelatie_betreft  is not null ) and
            ( r_vbg.rrl_rol_codrol_hoort_bij      is null and
              r_vbg.rrl_rle_numrelatie_hoort_bij  is null )
      then -- administratiekantoor
        stm_util.debug ( 'administratiekantoor' );
        -- Controle ivm RGG zit in de aangeroepen module.
        -- In sommige gevallen -afhankelijk v/h product en opdr.gever-
        -- komt dus niet het DA-adres terug, maar het OA adres.
        -- OSP 16-06-2003

        rle_get_adrsvelden ( p_opdrachtgever  => p_opdrachtgever
                           , p_produkt        => p_product
                           , p_numrelatie     => r_vbg.rrl_rle_numrelatie_betreft
                           , p_codrol         => r_vbg.rrl_rol_codrol_betreft
                           , p_srtadres       => r_vbg.code_soort_adres
                           , p_datump         => t_peildatum
                           , p_postcode       => p_postcode
                           , p_huisnr         => p_huisnr
                           , p_toevnum        => p_huisnr_toev
                           , p_woonplaats     => p_woonplaats
                           , p_straat         => p_straat
                           , p_codland        => l_landcode
                           , p_locatie        => t_dummy
                           , p_ind_woonboot   => t_dummy
                           , p_ind_woonwagen  => t_dummy
                           , p_landnaam       => t_dummy
                           , p_begindatum     => t_dummyd
                           , p_einddatum      => t_dummyd
                           );
        p_numrelatie_hoort_bij  := null;
        p_naam_hoort_bij        := null;

        open  c_rle ( b_numrelatie => r_vbg.rrl_rle_numrelatie_betreft );
        fetch c_rle into r_rle;
        close c_rle;

        p_numrelatie_betreft    := r_vbg.rrl_rle_numrelatie_betreft;
        p_naam_betreft          := r_rle.namrelatie;
        p_numrelatie_hoort_bij  := null;
        p_naam_hoort_bij        := null;
      elsif ( r_vbg.rrl_rol_codrol_betreft      is not null and
              r_vbg.rrl_rle_numrelatie_betreft  is not null ) and
            ( r_vbg.rrl_rol_codrol_hoort_bij      is not null and
              r_vbg.rrl_rle_numrelatie_hoort_bij  is not null )
      then -- Contactpersoon bij administratiekantoor
        stm_util.debug ( 'Contactpersoon bij administratiekantoor' );
        open  c_rle( b_numrelatie => r_vbg.rrl_rle_numrelatie_betreft );
        fetch c_rle into r_rle;
        close c_rle;

        p_numrelatie_betreft  := r_vbg.rrl_rle_numrelatie_betreft;
        p_naam_betreft        := r_rle.namrelatie;
        -- Controle ivm RGG zit in de aangeroepen module.
        -- In sommige gevallen -afhankelijk v/h product en opdr.gever-
        -- komt dus niet het DA-adres terug, maar het OA adres.
        -- OSP 16-06-2003

        rle_get_adrsvelden ( p_opdrachtgever  => p_opdrachtgever
                           , p_produkt        => p_product
                           , p_numrelatie     => r_vbg.rrl_rle_numrelatie_betreft
                           , p_codrol         => r_vbg.rrl_rol_codrol_betreft
                           , p_srtadres       => r_vbg.code_soort_adres
                           , p_datump         => t_peildatum
                           , p_postcode       => p_postcode
                           , p_huisnr         => p_huisnr
                           , p_toevnum        => p_huisnr_toev
                           , p_woonplaats     => p_woonplaats
                           , p_straat         => p_straat
                           , p_codland        => l_landcode
                           , p_locatie        => t_dummy
                           , p_ind_woonboot   => t_dummy
                           , p_ind_woonwagen  => t_dummy
                           , p_landnaam       => t_dummy
                           , p_begindatum     => t_dummyd
                           , p_einddatum      => t_dummyd
                           );
        rle_m11_rle_naw ( p_aantpos       => 40
                        , p_relnum        => r_vbg.rrl_rle_numrelatie_hoort_bij
                        , p_o_opmaaknaam  => p_naam_hoort_bij
                        , p_o_melding     => l_melding
                        , p_cod_opmaak    => l_codopmaak
                        );
        p_numrelatie_hoort_bij := r_vbg.rrl_rle_numrelatie_hoort_bij;
      end if;

      if  p_woonplaats is null then
          -- Als toch geen adres terug wordt gegeven, dan standaard
        stm_util.debug ( 'Standaard adres als adres leeg blijkt' );
        stm_util.debug ( 'P_CODROL = ' || p_codrol );
        stm_util.debug ( 'T_PEILDATUM = ' || to_char ( t_peildatum, 'dd-mm-yyyy' ));
        -- Controle ivm RGG zit in de aangeroepen module.
        -- In sommige gevallen -afhankelijk v/h product en opdr.gever-
        -- komt dus niet het DA-adres terug, maar het OA adres.
        -- OSP 16-06-2003

        rle_standaard_adres ( p_opdrachtgever     => p_opdrachtgever
                            , p_product           => p_product
                            , p_rle_numrelatie    => p_numrelatie
                            , p_codrol            => p_codrol
                            , p_peildatum         => t_peildatum
                            , p_code_soort_adres  => p_code_soort_adres
                            , p_postcode          => p_postcode
                            , p_huisnr            => p_huisnr
                            , p_huisnr_toev       => p_huisnr_toev
                            , p_woonplaats        => p_woonplaats
                            , p_straat            => p_straat
                            , p_codland           => l_landcode
                            );

        stm_util.debug ( 'P_WOONPLAATS = '|| p_woonplaats );
        open  c_rle ( b_numrelatie => p_numrelatie );
        fetch c_rle into r_rle;
        close c_rle;

        p_numrelatie_betreft    := p_numrelatie;
        p_naam_betreft          := r_rle.namrelatie;
        p_numrelatie_hoort_bij  := null;
        p_naam_hoort_bij        := null;
      end if;
    end if;
    stm_util.debug('haal_adres_op_oude_wijze, einde');
  end;
  --
BEGIN
  --
  stm_util.debug('RLE_GET_VERZEND, BEGIN');
  open  c_pat ( b_postsoort => p_postsoort );
  fetch c_pat into l_soort_adres, l_ind_dummy_adres_negeren, l_max_geldigheid;

  if c_pat%found
  then
    l_adres_gevonden := false;
    --
    while c_pat%found
      and not l_adres_gevonden
    loop

      if l_ind_dummy_adres_negeren = 'J'
      then
        --
        l_adres_gevonden := haal_adres_op;

       -- PDS: aanpassing FO RLE0938 04-12-2012
        -- controle of adres nog geldig is indien max_geldigheid
        -- gevuld in rle_postsoort_adressoorten

         if ( l_max_geldigheid is not null
              and add_months (sysdate, - (l_max_geldigheid * 12)) > l_ingangsdatum_adres)
         then

         -- het adres is niet meer  geldig: variabelen resetten

          p_postcode            := null;
          p_huisnr                := null;
          p_huisnr_toev        := null;
          p_woonplaats        := null;
          p_straat                := null;
          p_landnaam          := null;
          l_landcode            := null;
          l_soort_adres        := null;
          l_adres_gevonden  := false;


        -- Als het adres nog geldig is : controleren of opgehaalde adres "ONBEKEND" is
        -- Zo ja -out variabelen resetten en doorzoeken

        elsif (  ( p_postcode is null  or p_huisnr is null ) and
              ( p_straat is null    or p_huisnr is null or p_woonplaats is null ) and
              ( l_landcode is null  or l_landcode = 'NL' )
           ) or
           (  ( l_landcode is not null and l_landcode <> 'NL' ) and
              ( p_straat is null or p_woonplaats is null )
           ) or
           (  lower( p_straat || ' ' || to_char( p_huisnr )) like '%postbus%5210%' or
              ( lower( p_straat ) like '%burg%elsen%l%n%' and p_huisnr in( 325, 329 ))
           ) or
           ( lower( p_straat ) like '%onbekend%' or lower( p_woonplaats ) like '%onbekend%' ) or
           ( lower( l_landcode ) = 'onb' ) or
           ( lower( p_straat ) = 'onb' or lower( p_woonplaats ) = 'onb' ) or
           rle_lnd_chk( l_landcode ) = 'N'
        then
          p_postcode              := null;
          p_huisnr                := null;
          p_huisnr_toev           := null;
          p_woonplaats            := null;
          p_straat                := null;
          p_landnaam              := null;
          l_landcode              := null;
          l_soort_adres           := null;
          l_adres_gevonden        := false;
        end if;

      else
        l_adres_gevonden := haal_adres_op;
      end if;  -- einde ind_dummy_adres_negeren = 'J'

      if not l_adres_gevonden
      then
        fetch c_pat into  l_soort_adres, l_ind_dummy_adres_negeren, l_max_geldigheid;
      end if;
    end loop;
    if l_adres_gevonden
    then
      p_code_soort_adres := l_soort_adres;
    --
    -- Onderstaande code toegevoegd ivm compatibiliteit met de aanroep
    -- van deze procedure.
      stm_util.debug('voor open c_rle met p_numrelatie = '||p_numrelatie);
      open  c_rle ( p_numrelatie );
      fetch c_rle into r_rle;
      close c_rle;
      p_numrelatie_betreft    := p_numrelatie;
      p_naam_betreft          := r_rle.namrelatie;
      p_numrelatie_hoort_bij  := l_numrelatie_cpn; -- null;
      p_naam_hoort_bij        := l_verzendnaam_cpn ; --null;
    -- Einde compatibiliteitscode.
    --
    end if;
  else
    STM_UTIL.DEBUG('Er is geen post_soort gevonden voor '||p_postsoort||' , voor aanroep haal_adres_op_oude_wijze');
    haal_adres_op_oude_wijze;
  end if;
  close c_pat;
  p_landnaam := rle_lnd_getoms (l_landcode);
  stm_util.debug('p_landnaam : '||p_landnaam);
END RLE_GET_VERZEND;
 
/