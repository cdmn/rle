CREATE OR REPLACE procedure comp_rle.rle_gba_nieuw_verwant
  (pin_aanmeldingsnummer in number
  ,piv_status_gba in varchar2
  ,piv_afnemers_indicatie in varchar2
  ,pin_sofinummer in number
  ,pin_anummer in number
  ,piv_code_rol in varchar2
  ,piv_naam in varchar2
  ,piv_voorletters in varchar2
  ,piv_voorvoegsels in varchar2
  ,pid_geboortedatum in date
  ,piv_geslacht in varchar2
  ,piv_straatnaam in varchar2
  ,pin_huisnummer in number
  ,piv_toevoeging in varchar2
  ,piv_postcode in varchar2
  ,piv_woonplaats in varchar2
  ,piv_locatieomschrijving in varchar2
  ,piv_landcode in varchar2
  ,pin_persoonsnummer_deelnemer in number
  ,pid_ingangsdatum in date
  ,piv_verwantschapsoort in varchar2
  ,pid_einddatum in date
  ,piv_naam_partner in varchar2
  ,piv_vvgsl_partner in varchar2
  ,pov_verwerkt out varchar2
  ,pov_foutmelding out varchar2
  ,pon_persoonsnummer out number
  )
is

/* -------------------------------------------------------------------------------------------------------
** Wijzigingshistorie
** Datum        Auteur     Reden
** ??-??-????   ???        Creatie
** 03-01-2017   VER        MNGBA-1142, wijziging doorgifte parameter voorvoegsels bij aanroep RLE_RLE_TOEV
**                         Domeincheck op geldig voorvoegsel gebeurt aldaar.
*/

  cursor c_rkg_001 (cpv_code_rol_in_koppeling varchar2)
  is
  select *
  from   rle_rol_in_koppelingen  rkg
  where  rkg.code_rol_in_koppeling = cpv_code_rol_in_koppeling
  ;

  ln_error varchar2(1000);
  r_rkg_001 c_rkg_001%rowtype;
  ln_dummy number;
  ln_numrelatie number;
  ln_numrelatie_deelnemer number;
  ln_code_aanduiding_naamgebruik varchar2(30);

begin
  pov_verwerkt       := 'N' ;
  pov_foutmelding    := null ;
  pon_persoonsnummer := null ;

  /* De volgende wijzigingen niet doorgeven aan gba */
  rle_indicatie.doorgeven_aan_gba := 'N' ;

  /* Als naam partner gevuld Code naamgebruik op 'V')*/
  if piv_naam_partner is not null
  then
     ln_code_aanduiding_naamgebruik := 'V';
  else
     ln_code_aanduiding_naamgebruik := null;
  end if;

  /* Opvoeren persoon als een verwant */
  ln_numrelatie := null;

  rle_rle_toev( p_numrelatie => ln_numrelatie
              , p_namrelatie => piv_naam
              , p_rol_codrol => 'BG'
              , p_indinstelling => 'N'
              , p_wrddom_geslacht => piv_geslacht
              , p_wrddom_vvgsl => piv_voorvoegsels
              , p_voorletter => piv_voorletters
              , p_voornaam => null
              , p_handelsnaam => null
              , p_datbegin => null
              , p_dateinde => null
              , p_datoprichting => null
              , p_datgeboorte => pid_geboortedatum
              , p_code_gebdat_fictief => null
              , p_datoverlijden => null
              , p_datregoverlijdenbevestiging => null
              , p_code_aanduiding_naamgebruik => ln_code_aanduiding_naamgebruik
              , p_naam_partner => piv_naam_partner
              , p_vvgsl_partner => piv_vvgsl_partner
              , p_datemigratie => null
              , p_wrddom_gewatitel => null
              , p_ind_vervallen => 'N'
              , p_ind_commit => 'N'
              , p_ind_succes => pov_verwerkt
              , p_melding => pov_foutmelding
              , p_externe_code => pon_persoonsnummer
              ) ;

  if pov_verwerkt = 'N'
  then
     pon_persoonsnummer := null ;
     /* foutmelding is al gevuld */
     return ;
  end if ;

  if ln_numrelatie is null
  then
    pov_verwerkt := 'N' ;
    pov_foutmelding := 'Relatienummer kan niet worden bepaald bij opvoeren of wijzigen relatie.' ;
    return ;
  end if ;

  /* Opvoeren A-Nummer */
  rle_m16_rec_verwerk( p_coderelatie =>  pin_anummer
                     , p_codsysteemcomp => 'GBA'
                     , p_codrol => 'PR'
                     , p_numrelatie => ln_numrelatie
                     , p_datingang => sysdate
                     , p_dateinde => null
                     ) ;

  /* Opvoeren Sofinummer */
  rle_m16_rec_verwerk( p_coderelatie => pin_sofinummer
                     , p_codsysteemcomp => 'SOFI'
                     , p_codrol => 'PR'
                     , p_numrelatie => ln_numrelatie
                     , p_datingang => sysdate
                     , p_dateinde => null
                     ) ;

  /* Opvoeren Aanmeldingsnummer, Status en Afnemersindicatie */
  rle_gba_wijzig_gba( pin_numrelatie => ln_numrelatie
                    , piv_gba_status => piv_status_gba
                    , piv_gba_afnemers_ind => piv_afnemers_indicatie
                    , pin_gba_aanmeldingsnummer => pin_aanmeldingsnummer
                    ) ;

  begin
    rle_m06_rec_relnum( p_relext => pin_persoonsnummer_deelnemer
                      , p_systeemcomp => 'PRS'
                      , p_codrol => 'PR'
                      , p_datparm => sysdate
                      , p_o_relnum => ln_numrelatie_deelnemer
                      ) ;
  exception
    when others
    then
      ln_numrelatie_deelnemer := null ;
  end ;

  if ln_numrelatie_deelnemer is null
  then
    pov_verwerkt := 'N' ;
    pov_foutmelding := 'Relatienummer van de deelnemer kan niet worden gevonden (persoonsnummer ' || pin_persoonsnummer_deelnemer || ').' ;
    pon_persoonsnummer := null ;
    return ;
  end if ;

  /* Opvoeren administraties van de deelnmer bij de verwant (administratiekoppeling) */
  rle_rie.kop_rkn_rie( p_rle_numrelatie_dlnmr => ln_numrelatie_deelnemer
                     , p_rle_numrelatie_vrwnt => ln_numrelatie
                     , p_error => ln_error
                     );

  if ln_error is not null
  then
    pov_verwerkt := 'N' ;
    pov_foutmelding := ln_error ;
    stm_util.debug( 'COMP_RLE.RLE_GBA_NIEUW_VERWANT: ' || pov_foutmelding ) ;
    return ;
  end if ;

  /* Opvoeren adres */
  ln_dummy := rle_verwerk_ras( pin_ras_id => null
                             , pin_ras_rle_numrelatie => ln_numrelatie
                             , piv_ras_rol_codrol => 'PR'
                             , piv_ras_dwe_wrddom_srtadr => 'DA'
                             , pid_ras_datingang => sysdate
                             , pid_ras_dateinde => null
                             , pin_ras_ads_numadres => null
                             , piv_ras_provincie => null
                             , piv_ras_locatie => piv_locatieomschrijving
                             , piv_ras_ind_woonboot => 'N'
                             , piv_ras_ind_woonwagen => 'J'
                             , piv_ads_codland => piv_landcode
                             , piv_ads_postcode => piv_postcode
                             , pin_ads_huisnummer => pin_huisnummer
                             , piv_ads_toevhuisnum => piv_toevoeging
                             , piv_ads_straatnaam => piv_straatnaam
                             , piv_ads_woonplaats => piv_woonplaats
                             ) ;

  /* Opvoeren verwantschap (relatiekoppeling) */
  open c_rkg_001(piv_verwantschapsoort);
  fetch c_rkg_001 into r_rkg_001;
  close c_rkg_001 ;

  insert into rle_relatie_koppelingen
    ( id
    , rkg_id
    , rle_numrelatie
    , rle_numrelatie_voor
    , dat_begin
    , dat_einde
    )
  values
    ( rle_rkn_seq1.nextval
    , r_rkg_001.id
    , ln_numrelatie_deelnemer
    , ln_numrelatie
    , pid_ingangsdatum
    , pid_einddatum
    );

   /* Uitvoer waarden */
   pov_verwerkt := 'J';

exception
  when others
  then
    /* Indicatie terugzetten */
    rle_indicatie.doorgeven_aan_gba := 'J';

    /* Default uitvoer */
    pov_verwerkt := 'N';
    pon_persoonsnummer := null;

    if pov_foutmelding is null
    then
      pov_foutmelding    := nvl( sqlerrm, 'Onbekend probleem.' ) ;
    end if ;

    /* Cursors sluiten */
    if c_rkg_001%isopen
    then
      close c_rkg_001 ;
    end if ;
end rle_gba_nieuw_verwant;
/