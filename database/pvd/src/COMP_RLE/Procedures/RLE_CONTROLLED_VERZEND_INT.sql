CREATE OR REPLACE PROCEDURE comp_rle.RLE_CONTROLLED_VERZEND_INT
 (P_RELATIENUMMER IN number
 ,P_REGELING IN varchar2 := null
 ,P_CODROL IN VARCHAR2 := null
 ,P_POSTSOORT IN VARCHAR2 := null
 ,P_DATUM_GEBEURTENIS IN date := sysdate
 ,P_FOUTMELDING OUT varchar2
 ,P_NAAM2 OUT varchar2
 ,P_NAAM3 OUT varchar2
 ,P_POSTCODE OUT varchar2
 ,P_HUISNUMMER OUT number
 ,P_HUISNR_TOEV OUT varchar2
 ,P_WOONPLAATS OUT varchar2
 ,P_STRAAT OUT varchar2
 ,P_LANDNAAM OUT varchar2
 ,P_SOORT_ADRES OUT VARCHAR2
 )
 IS

--
l_opdrachtgever  number        := null;
l_productcode    varchar2(100) := null;
l_rol            varchar2(100) := null;
l_postsoort      varchar2(100) := null;
l_peildatum      date          := null;
l_relatienummer2 number        := null;
l_relatienummer3 number        := null;
l_naam2          varchar2(100) := null;
l_naam3          varchar2(100) := null;
l_soort_adres    varchar2(100) := null;
l_postcode       varchar2(100) := null;
l_huisnummer     number        := null;
l_HUISNR_TOEV    varchar2(100) := null;
L_WOONPLAATS     varchar2(100) := null;
L_STRAAT         varchar2(100) := null;
L_LANDCODE       varchar2(100) := null;
l_landnaam       varchar2(100) := null;
--
l_adm_code       varchar2(200);
begin
  stm_util.debug( '*** rle_controlled_verzendadres *** ' );

  -- Bepaal de rol
  if p_codrol is not null
  then
    l_rol := p_codrol;
  else
    -- Parameter Persoon Rol bestaat
    l_rol := 'PR';
  end if;

  --
  -- Vullen administratie adhv regeling
  --
  IF p_regeling IS NOT NULL
  THEN
    l_adm_code := pdt_rgg.geef_adm_van_rgg (p_rgg_code => p_regeling)(1).adm_code;
    IF l_adm_code IS NULL
    THEN
      p_foutmelding := 'Regeling onbekend';
      return;
    ELSE
      l_productcode   := l_adm_code;
      l_opdrachtgever := NULL;
    END IF;
  ELSE
    -- Indien regeling leeg is dummy waarde voor opdrachtgever/product om adres <> DA te forceren
    l_opdrachtgever := 1233211;
    l_productcode   := 'XzyXa';
  END IF;

  stm_util.debug('Aanroep rle_get_verzend');
  stm_util.debug( 'l_opdrachtgever:' || to_char( l_opdrachtgever ));
  stm_util.debug( 'l_productcode:' || l_productcode );
  stm_util.debug( 'l_rol:' || l_rol );

  rle_get_verzend( l_opdrachtgever
                 , l_productcode
                 , p_relatienummer
                 , p_postsoort
                 , l_rol
                 , l_peildatum   -- LEEG
                 , l_relatienummer2   -- bijv Administratiekantoor
                 , l_naam2
                 , l_relatienummer3   -- Contactpersoon
                 , l_naam3
                 , l_soort_adres
                 , l_postcode
                 , l_huisnummer
                 , l_huisnr_toev
                 , l_woonplaats
                 , l_straat
                 , l_landnaam
                 );
  stm_util.debug( 'uit RLE_GET_VERZEND' );
  l_landcode := rle_get_landcode( l_landnaam );
  --
  stm_util.debug( 'l_relatienummer2:' || to_char( l_relatienummer2 ));
  stm_util.debug( 'l_naam2:' || l_naam2 );
  stm_util.debug( 'l_relatienummer3:' || to_char( l_relatienummer3 ));
  stm_util.debug( 'l_naam3:' || l_naam3 );
  stm_util.debug( 'l_soort_adres:' || l_soort_adres );
  stm_util.debug( 'l_postcode:' || l_postcode );
  stm_util.debug( 'l_huisnummer:' || to_char( l_huisnummer ));
  stm_util.debug( 'l_HUISNR_TOEV:' || l_huisnr_toev );
  stm_util.debug( 'L_WOONPLAATS:' || l_woonplaats );
  stm_util.debug( 'L_STRAAT:' || l_straat );
  stm_util.debug( 'L_LANDCODE:' || l_landcode );
  stm_util.debug( 'L_LANDNAAM:' || l_landnaam );

  -- check Onbekend Adres
  if    (     (    l_postcode is null
                or l_huisnummer is null )
          and (    l_straat is null
                or l_huisnummer is null
                or l_woonplaats is null )
          and (    l_landcode is null
                or l_landcode = 'NL' )   -- MVL, 29-03-2007
                                      )
     or (     (     l_landcode is not null
                and l_landcode <> 'NL' )
          and (    l_straat is null
                or l_woonplaats is null ))
     or (    lower( l_straat || ' ' || to_char( l_huisnummer )) like '%postbus%5210%'
          or (     lower( l_straat ) like '%burg%elsen%l%n%'
               and l_huisnummer in( 325, 329 )))
     or (    lower( l_straat ) like '%onbekend%'
          or lower( l_woonplaats ) like '%onbekend%' )
     or ( lower( l_landcode ) = 'onb' )
     or  (    lower( l_straat ) = 'onb'  -- jku adressen komen uit de conversie
          or lower( l_woonplaats ) = 'onb' )  -- 04-03-2011
  then
    p_foutmelding := 'Adres onbekend';
    return;
  end if;

  if rle_lnd_chk( l_landcode ) = 'N'
  then
    p_foutmelding := 'Adres onbekend';
    return;
  end if;

  p_foutmelding := null;
  p_naam2 := l_naam2;
  p_naam3 := l_naam3;
  p_postcode := l_postcode;
  p_huisnummer := l_huisnummer;
  p_huisnr_toev := l_huisnr_toev;
  p_woonplaats := l_woonplaats;
  p_straat := l_straat;
  p_landnaam := l_landnaam;
  p_soort_adres := l_soort_adres;
END RLE_CONTROLLED_VERZEND_INT;
 
/