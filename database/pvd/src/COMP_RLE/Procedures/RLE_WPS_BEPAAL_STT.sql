CREATE OR REPLACE PROCEDURE comp_rle.RLE_WPS_BEPAAL_STT
 (P_STT_ID IN OUT VARCHAR2
 ,P_WPS_ID IN OUT NUMBER
 ,P_IND_NWADRS IN OUT VARCHAR2
 ,P_CODLAND IN OUT VARCHAR2
 ,P_STRAAT IN OUT VARCHAR2
 ,P_WOONPL IN OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* c_stra02 */
CURSOR C_STT_008
 (B_WPS_ID IN NUMBER
 ,B_STRAAT IN VARCHAR2
 )
 IS
/* C_STT_008 */
select	STRAATID
from	rle_straten stt
where	stt.wps_woonplaatsid = b_wps_id
and	stt.straatnaam = b_straat;
/* c_wnpl02 */
CURSOR C_WPS_002
 (B_CODLAND IN VARCHAR2
 ,B_WOONPL IN VARCHAR2
 )
 IS
/* C_WPS_002 */
SELECT	wps.woonplaatsid
FROM	rle_woonplaatsen wps
WHERE	wps.lnd_codland = b_codland
AND	UPPER( wps.woonplaats ) = UPPER( b_woonpl );
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_WPS_BEPAAL_STT */
p_wps_id               := null;
p_stt_id               := null;
p_ind_nwadrs            := 'N';
open c_wps_002(p_codland,p_woonpl);
fetch c_wps_002
into p_wps_id;
if c_wps_002%NOTFOUND then
   close c_wps_002;
    p_ind_nwadrs        := 'J';
else
    close c_wps_002;
    open c_stt_008(p_wps_id,p_straat);
    fetch c_stt_008
    into p_stt_id;
    if c_stt_008%NOTFOUND then
       close c_stt_008;
       p_ind_nwadrs     := 'J';
    else
       close c_stt_008;
    end if;
end if;
END RLE_WPS_BEPAAL_STT;

 
/