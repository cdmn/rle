CREATE OR REPLACE PROCEDURE comp_rle.RLE_OPVOEREN_CONTACTPERSOON
 (P_NAAM IN VARCHAR2
 ,P_CODROL IN VARCHAR2
 ,P_VOORLETTER IN VARCHAR2
 ,P_VOORVOEGSELS IN VARCHAR2
 ,P_GESLACHT IN VARCHAR2
 ,P_GEBOORTEDATUM DATE
 ,P_NAAM_PARTNER IN VARCHAR2
 ,P_VVGSL_PARTNER IN VARCHAR2
 ,P_NAAMGEBRUIK IN VARCHAR2
 ,P_CODE_FUNCTIE IN VARCHAR2
 ,P_CODE_SOORT_CONTACTPERS IN VARCHAR2
 ,P_NUMREL_CONTACTPERS_VAN IN NUMBER
 ,P_CODROL_CONTACTPERS_VAN IN VARCHAR2
 ,P_SOORT_ADRES_CONTACTPERS_VAN IN VARCHAR2
 ,P_DATBEGIN_RAS_CONTACTPERS_VAN IN DATE
 ,P_NUMADRES_CONTACTPERS_VAN IN NUMBER
 ,P_SUCCES OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
--
 CURSOR c_relnum
 IS
 SELECT rle_rle_seq1.NEXTVAL
 FROM   dual
 ;
--
 CURSOR c_ras(b_numrel_contactpers_van IN NUMBER
                                 ,b_codrol_contactpers_van IN VARCHAR2
								 ,b_soort_adres_contactpers_van IN VARCHAR2
								 ,b_datbegin_ras_contactpers_van IN DATE
								 ,b_numadres_contactpers_van IN NUMBER
								 )
 IS
 SELECT ras.ID
 FROM RLE_RELATIE_ADRESSEN ras
 WHERE ras.RLE_NUMRELATIE = b_numrel_contactpers_van
 AND ras.ROL_CODROL = b_codrol_contactpers_van
 AND ras.DWE_WRDDOM_SRTADR = b_soort_adres_contactpers_van
 AND ras.DATINGANG = b_datbegin_ras_contactpers_van
 AND ras.ADS_NUMADRES = b_numadres_contactpers_van
  ;
 --
 l_numrel_contactpers NUMBER := NULL;
 l_ras_id NUMBER := NULL;
 l_errmsg VARCHAR2(2000) := NULL;
-- PL/SQL Block
BEGIN
  p_succes := NULL;
  --
  OPEN c_relnum;
  FETCH c_relnum INTO l_numrel_contactpers;
  CLOSE c_relnum;
  --
  INSERT INTO RLE_RELATIES
  (
    NUMRELATIE,
    INDGEWNAAM,
    INDINSTELLING,
    INDCONTROLE,
    CODORGANISATIE,
    NAMRELATIE,
    ZOEKNAAM,
    VOORLETTER,
    DATGEBOORTE,
    DWE_WRDDOM_VVGSL,
    DWE_WRDDOM_GESLACHT,
	NAAM_PARTNER,
	VVGSL_PARTNER,
	CODE_AANDUIDING_NAAMGEBRUIK
  )
  VALUES
  ( l_numrel_contactpers
    , 'N'
    , 'N'
    , 'N'
    , 'A'
    , p_naam
    , UPPER(SUBSTR(p_naam,1, 10)) --p_zoeknaam
    , p_voorletter
    , p_geboortedatum
    , UPPER(REPLACE(p_voorvoegsels, ' ', ''))
    , p_geslacht
	, p_naam_partner
	, p_vvgsl_partner
	, p_naamgebruik
  );
  --
  INSERT INTO RLE_RELATIE_ROLLEN
  (
    RLE_NUMRELATIE,
    ROL_CODROL,
    CODORGANISATIE
  )
  VALUES
  (
    l_numrel_contactpers
  , p_codrol
  , 'A'
  );
  --
  OPEN c_ras(p_numrel_contactpers_van
                           ,p_codrol_contactpers_van
                           ,p_soort_adres_contactpers_van
						   ,p_datbegin_ras_contactpers_van
                           ,p_numadres_contactpers_van);
   FETCH c_ras INTO l_ras_id;
   CLOSE c_ras;
  --
  INSERT INTO RLE_CONTACTPERSONEN
  (
    RAS_ID,
	RLE_NUMRELATIE,
	CODE_FUNCTIE,
	CODE_SOORT_CONTACTPERSOON
  )
  VALUES
  (
    l_ras_id
  , l_numrel_contactpers
  , p_code_functie
  , p_code_soort_contactpers
  );
---
  p_succes := 'J';
EXCEPTION
  WHEN OTHERS
  THEN
    p_succes := 'N';
    l_errmsg := SQLERRM;
	stm_dbproc.raise_error('Rle_Opvoeren_Contactpersoon: '||l_errmsg);
END;

 
/