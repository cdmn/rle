CREATE OR REPLACE PROCEDURE comp_rle.RLE_OPVOEREN_RELATIE
 (P_NAAM IN VARCHAR2
 ,P_CODROL IN VARCHAR2
 ,P_VOORLETTER IN VARCHAR2
 ,P_GEBOORTEDATUM IN DATE
 ,P_VOORVOEGSELS IN VARCHAR2
 ,P_GESLACHT IN VARCHAR2
 ,P_NAAM_PARTNER IN VARCHAR2
 ,P_VVGSL_PARTNER IN VARCHAR2
 ,P_NAAMGEBRUIK IN VARCHAR2
 ,P_POSTCODE IN VARCHAR2
 ,P_HUISNUM IN NUMBER
 ,P_TOEVNUM IN VARCHAR2
 ,P_WOONPL IN VARCHAR2
 ,P_STRAAT IN VARCHAR2
 ,P_LOCATIE IN VARCHAR2
 ,P_IND_WOONWAGEN IN VARCHAR2
 ,P_IND_WOONBOOT IN VARCHAR2
 ,P_PROVINCIE IN VARCHAR2
 ,P_CODE_LAND IN VARCHAR2
 ,P_SRT_ADRES IN VARCHAR2
 ,P_BEGINDATUM_ADRES IN DATE
 ,P_EINDDATUM_ADRES IN DATE
 ,P_INDINSTELLING IN VARCHAR2
 ,P_RELNUM IN OUT NUMBER
 ,P_NUMADRES IN OUT NUMBER
 ,P_SUCCES OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
CURSOR c_relnum
IS
SELECT rle_rle_seq1.NEXTVAL
FROM   dual
;
--
l_code_land VARCHAR2(3) := p_code_land;
l_postcode VARCHAR2(6):= p_postcode;
l_huisnummer NUMBER := p_huisnum;
l_toevoeging_huisnummer VARCHAR2(100):= p_toevnum;
l_straat VARCHAR2(200):= p_straat;
l_woonplaats VARCHAR2(100) := p_woonpl;
l_errmsg VARCHAR2(2000);
-- PL/SQL Block
BEGIN
  p_succes := NULL;
  OPEN c_relnum;
  FETCH c_relnum INTO p_relnum;
  CLOSE c_relnum;
  --
  INSERT INTO RLE_RELATIES
  (
    NUMRELATIE,
    INDGEWNAAM,
    INDINSTELLING,
    INDCONTROLE,
    CODORGANISATIE,
    NAMRELATIE,
    ZOEKNAAM,
    VOORLETTER,
    DATGEBOORTE,
    DWE_WRDDOM_VVGSL,
    DWE_WRDDOM_GESLACHT,
	NAAM_PARTNER,
	VVGSL_PARTNER,
	CODE_AANDUIDING_NAAMGEBRUIK
  )
  VALUES
  ( p_relnum
    , 'N'
    , P_INDINSTELLING
    , 'N'
    , 'A'
    , p_naam
    , UPPER(SUBSTR(p_naam,1, 30)) --p_zoeknaam
    , p_voorletter
    , p_geboortedatum
    , UPPER(REPLACE(p_voorvoegsels, ' ', ''))
    , p_geslacht
	, p_naam_partner
	, p_vvgsl_partner
	, p_naamgebruik
  );
  --
  INSERT INTO RLE_RELATIE_ROLLEN
  (
    RLE_NUMRELATIE,
    ROL_CODROL,
    CODORGANISATIE
  )
  VALUES
  (
    p_relnum
  , p_codrol
  , 'A'
  );
  --
  if l_woonplaats is not null
  then
    Rle_Rle_Ads_Toev (p_relnum
                    , p_codrol
                    , p_srt_adres
                    , p_begindatum_adres
                    , l_code_land
                    , l_postcode
                    , l_huisnummer
                    , l_toevoeging_huisnummer
                    , l_woonplaats
                    , l_straat
                    , p_einddatum_adres
                    , p_locatie
                    , p_numadres
                    , p_ind_woonwagen
                    , p_ind_woonboot
                    , p_provincie
                    );
  end if;
  --
  p_succes := 'J';
EXCEPTION
  WHEN OTHERS
  THEN
    p_succes := 'N';
    l_errmsg := SQLERRM;
	stm_dbproc.raise_error('Rle_Opvoeren_Relatie : '||l_errmsg);
END;

 
/