CREATE OR REPLACE PROCEDURE comp_rle.RLE_AFKEUREN_PERSOON
 (P_PERSOONSNUMMER IN varchar2
 ,P_IND_VERWERKT OUT varchar2
 )
 IS
-- Program Data
L_NUMRELATIE NUMBER(9);
-- PL/SQL Block
BEGIN
p_ind_verwerkt := 'N';
--
-- Ophalen relatienummen
--
   rle_m06_rec_relnum(p_persoonsnummer
                      , 'PRS'
                      , 'PR'
                      , SYSDATE
                      , l_numrelatie
                      ) ;
   IF L_numrelatie is not NULL
      then
--  Updaten status
--
      UPDATE RLE_AFSTEMMINGEN_GBA
       set  CODE_GBA_STATUS  = 'AF'
       where RLE_NUMRELATIE = l_NUMRELATIE	;
--
       p_ind_verwerkt := 'J';
   ELSE
       p_ind_verwerkt := 'N';
   END IF;
--
END RLE_AFKEUREN_PERSOON;

 
/