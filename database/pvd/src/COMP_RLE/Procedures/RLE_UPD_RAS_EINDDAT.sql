CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPD_RAS_EINDDAT
 (P_NUMRELATIE rle_relaties.numrelatie%TYPE
 ,P_CODROL rle_rollen.codrol%TYPE
 ,P_O_DATSCHONING rle_relaties.datschoning%TYPE
 ,P_N_DATSCHONING rle_relaties.datschoning%TYPE
 )
 IS
BEGIN
/* RLE_UPD_RAS_EINDDAT */
IF	p_o_datschoning is null THEN
	UPDATE	rle_relatie_adressen
	SET	dateinde		= p_n_datschoning
	WHERE	rle_numrelatie	= p_numrelatie
	AND	NVL(rol_codrol,'@') = NVL(p_codrol,'@')
	AND	dateinde  is null;
ELSE
	UPDATE	rle_relatie_adressen
	SET	dateinde		= p_n_datschoning
	WHERE	rle_numrelatie	= p_numrelatie
	AND	NVL(rol_codrol,'@') = NVL(p_codrol,'@')
	AND	(dateinde		= p_o_datschoning
		OR	dateinde is null);
END IF;
END RLE_UPD_RAS_EINDDAT;

 
/