CREATE OR REPLACE PROCEDURE comp_rle.RLE_EXP_OVERZ_BRON
 (P_SCRIPT_NAAM IN VARCHAR2
 ,P_SCRIPT_ID IN NUMBER
 ,P_BESTANDSNAAM IN VARCHAR2
 )
 IS

cursor c_rec ( b_bestandsnaam in varchar2)
is
 select regeling
       , relatienummer           --deelnemer
       , persoonsnummer
       , bsn
       , a_nummer
       , voorletters||' '||voorvoegsels||' '||naam  naam
       , geslacht
       , geboortedatum
       , overlijdensdatum
       , relatienummer_ex    --bron ex-partner
       , persoonsnummer_ex
       , bsn_ex
       , a_nummer_ex
       , voorletters_ex||' '||voorvoegsels_ex ||' '||naam_ex  naam_ex
       , geslacht_ex
       , geboortedatum_ex
       , overlijdensdatum_ex
       , straatnaam_ex
       , huisnummer_ex
       , toevoeging_ex||' '||huisletter_ex toevoeging_ex
       , postcode_ex
       , gemeentedeel_ex
       , (select landnaam_hfdl from landen where landkode = landcode_ex) landcode_ex
       , huwelijk_datumbegin
       , huwelijk_datumeinde
       , substr(foutmelding, 1, 75)  foutmelding
       , gba_ex_bsn
       , gba_ex_anr
       , gba_ex_voorletters||' '||gba_ex_voorvoegsel||' '||gba_ex_naam gba_ex_naam
       , gba_ex_geslacht
       , gba_ex_datgeboorte
       , gba_ex_datoverlijden
       , gba_ex_straat
       , gba_ex_huisnr
       , gba_ex_huisletter||' '||gba_ex_huisnrtoev  gba_ex_toevoeging
       , gba_ex_postcode
       , gba_ex_gemeentedeel
       , (select landnaam_hfdl from landen where landkode = gba_ex_landcode) gba_ex_landcode
       , ind_scheiding_al_bekend_rle
       , to_char(datum_opgenomen_gbabatch_dln, 'dd-mm-yyyy hh24:mi:ss') datum_opgenomen_gbabatch_dln
       , to_char(datum_verwerkt_gba_dln_geg, 'dd-mm-yyyy hh24:mi:ss') datum_verwerkt_gba_dln_geg
       , ind_gba_fout_dln
       , ind_ex_al_bekend_rle
       , to_char(datum_opgenomen_gbabatch_ex, 'dd-mm-yyyy hh24:mi:ss') datum_opgenomen_gbabatch_ex
       , to_char(datum_verwerkt_gba_ex_geg, 'dd-mm-yyyy hh24:mi:ss') datum_verwerkt_gba_ex_geg
       , ind_gba_fout_ex
       , ind_scheiding_alsnog_bekendrle
       , ind_ex_aangemaakt_rle
       , ind_rel_koppeling_aangemaakt
       , ind_rel_koppeling_gewijzigd
       , ind_workflow_case_aangemaakt
       , ind_opgenomen_in_werkbak
       , ind_verwerkt
       , intern_kenmerk
       , datum_aanlevering
       , bestandsnaam
  from   rle_bron_ex_partners
  where  bestandsnaam = b_bestandsnaam
;

cursor c_ind (b_bestandsnaam in varchar2)
is
  with
  a as
   (select count(*) aantal
    from rle_bron_ex_partners
    where bestandsnaam = b_bestandsnaam)
  ,b as
   (select count(*) ind_verwerkt
    from rle_bron_ex_partners
    where ind_verwerkt = 'J'
    and bestandsnaam = b_bestandsnaam)
  ,c as
   (select count(*) wacht_huw
    from rle_bron_ex_partners
    where datum_opgenomen_gbabatch_dln is not null
      and datum_verwerkt_gba_dln_geg is null
    and bestandsnaam = b_bestandsnaam)
  ,d as
   (select count(*) wacht_controle
    from rle_bron_ex_partners
    where datum_verwerkt_gba_dln_geg is not null
     and ind_ex_al_bekend_rle is null
     and ind_gba_fout_dln = 'N'
     and bestandsnaam = b_bestandsnaam)
  ,e as
   (select count(*) wacht_verwerking_ex
    from rle_bron_ex_partners
    where datum_opgenomen_gbabatch_ex is not null
      and datum_verwerkt_gba_ex_geg is null
      and bestandsnaam = b_bestandsnaam)
  ,f as
   (select count(*) wacht_registratie
    from rle_bron_ex_partners
    where ind_workflow_case_aangemaakt is null
     and (relatienummer_ex is not null
        or datum_verwerkt_gba_ex_geg is not null
        or ind_gba_fout_dln = 'J')
     and ind_verwerkt = 'N'
     and bestandsnaam = b_bestandsnaam)
  ,g as
   (select count(*) handm_verwerk
    from rle_bron_ex_partners
    where ind_verwerkt = 'N'
      and ind_opgenomen_in_werkbak = 'J'
      and bestandsnaam = b_bestandsnaam)
  ,h as
   (select count(distinct bsn) workflow
    from rle_bron_ex_partners
    where ind_verwerkt = 'N'
      and ind_opgenomen_in_werkbak = 'J'
      and bestandsnaam = b_bestandsnaam)
  select *
  from a
      ,b
      ,c
      ,d
      ,e
      ,f
      ,g
      ,h
;
cn_module              constant varchar2(25):='RLE_EXP_OVERZ_BRON';
--
l_volgnummer           stm_job_statussen.volgnummer%type := 0;
l_lijstnummer          number    := 1;
l_regelnummer          number    := 0;
--
l_database             varchar2(10);
fh_stoplijst           utl_file.file_type;
l_bestandsnaam         varchar2(255);

l_aantal               number := 0;
l_verwerkt             number := 0;
l_wacht_huw            number := 0;
l_wacht_controle       number := 0;
l_wacht_verw_ex        number := 0;
l_wacht_registratie    number := 0;
l_handm_verwerk        number := 0;
l_teller_workflow      number := 0;
l_aantal_naam          number := 0;

e_fout_bestand         exception;
begin

  stm_util.t_procedure       := cn_module;
  stm_util.t_script_naam     := p_script_naam;
  stm_util.t_script_id       := p_script_id;
  stm_util.t_programma_naam  := 'RLE_EXP_OVERZ_BRON';

  select case
           when regexp_substr ( global_name
                             , '[^\.]*'
                              ) = 'PPVD'
           then null
           else regexp_substr ( global_name
                              , '[^\.]*'
                              ) || '_'
         end
  into   l_database
  from   global_name;

  stm_util.debug ( 'Logbestand wordt aangemaakt' );

  -- Schrijven logbestand
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer, rpad ('***** LOGVERSLAG RLE_EXP_OVERZ_BRON*****', 70)
               || ' START: ' || to_char ( sysdate, 'dd-mm-yyyy hh24:mi:ss' ));

  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  ' ' );

  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,     rpad('-',100, '-'));

  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  ' ' );
  --
  execute immediate 'alter session set nls_date_format="dd-mm-yyyy"';
  --
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Parameter Bestandsnaam is: '||p_bestandsnaam );

    stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  ' ' );

  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer, rpad('-',100, '-'));

  select count(*)
  into l_aantal_naam
  from rle_bron_ex_partners
  where bestandsnaam = p_bestandsnaam;

  if l_aantal_naam = 0
  then
    raise e_fout_bestand;
  end if;

  -- bestandsnaam formeren
  l_bestandsnaam := 'RLEPRC68_OVERZICHT_'||p_bestandsnaam||'_'|| to_char(sysdate, 'yyyymmdd')|| '.csv';

  -- openen van de filehandler
  fh_stoplijst := utl_file.fopen ( 'RLE_OUTBOX'
                                  , l_bestandsnaam
                                  , 'W'
                                  , 5000);


  utl_file.put_line ( fh_stoplijst
                    , 'REGELING;RELATIENUMMER;PERSOONSNUMMER;BSN;A_NUMMER;NAAM;GESLACHT;GEBOORTEDATUM;OVERLIJDENSDATUM;RELATIENUMMER EX;PERSOONSNUMMER EX;'||
                      'BSN EX;A-NUMMER EX;NAAM EX;GESLACHT EX;GEBOORTEDATUM EX;OVERLIJDEN EX;STRAAT EX;HUISNUMMER EX;HUISNUMMERTOEVOEGING EX;'||
                      'POSTCODE EX;WOONPLAATS EX;LAND EX;BEGINDATUM HUWELIJK;EINDDATUM HUWELIJK;FOUTMELDING;GBA EX BSN;GBA EX A-NUMMER;GBA EX NAAM;GBA EX GESLACHT;'||
                      'GBA_EX GEBOORTEDATUM;GBA EX OVERLIJDENSDATUM;GBA EX STRAAT;GBA EX HUISNUMMER;GBA EX HUISNUMMERTOEVOEGING;GBA EX POSTCODE;GBA EX WOONPLAATS;'||
                      'GBA EX LANDNAAM;IND SCHEIDING AL BEKEND RLE;DATUM OPGENOMEN GBA BATCH DLN;DATUM VERWERKT GBA DLN GEG;IND GBA FOUT DLN;IND EX AL BEKEND RLE;DATUM OPGENOMEN GBA BATCH EX;'||
                      'DATUM VERWERKT GBA EX GEG;IND GBA FOUT EX;IND SCHEIDING ALSNOG BEKEND RLE;IND EX AANGEMAAKT RLE;IND RELATIEKOPPELING AANGEMAAKT;IND RELATIEKOPPELING GEWIJZIGD;'||
                      'IND WORKFLOWCASE AANGEMAAKT;IND OPGENOMEN IN WERKBAK;IND VERWERKT;INTERN KENMERK;DATUM AANLEVERING;BESTANDSNAAM'
                    );

  for r_rec in c_rec  (p_bestandsnaam )
  loop

    utl_file.put_line (fh_stoplijst
                      ,r_rec.regeling
                       ||';'||r_rec.relatienummer
                       ||';'||r_rec.persoonsnummer
                       ||';'||r_rec.bsn
                       ||';'||r_rec.a_nummer
                       ||';'||r_rec.naam
                       ||';'||r_rec.geslacht
                       ||';'||r_rec.geboortedatum
                       ||';'||r_rec.overlijdensdatum
                       ||';'||r_rec.bsn_ex
                       ||';'||r_rec.a_nummer_ex
                       ||';'||r_rec.relatienummer_ex
                       ||';'||r_rec.persoonsnummer_ex
                       ||';'||r_rec.naam_ex
                       ||';'||r_rec.geslacht_ex
                       ||';'||r_rec.geboortedatum_ex
                       ||';'||r_rec.overlijdensdatum_ex
                       ||';'||r_rec.straatnaam_ex
                       ||';'||r_rec.huisnummer_ex
                       ||';'||r_rec.toevoeging_ex
                       ||';'||r_rec.postcode_ex
                       ||';'||r_rec.gemeentedeel_ex
                       ||';'||r_rec.landcode_ex
                       ||';'||r_rec.huwelijk_datumbegin
                       ||';'||r_rec.huwelijk_datumeinde
                       ||';'||r_rec.foutmelding
                       ||';'||r_rec.gba_ex_anr
                       ||';'||r_rec.gba_ex_bsn
                       ||';'||r_rec.gba_ex_naam
                       ||';'||r_rec.gba_ex_geslacht
                       ||';'||r_rec.gba_ex_datgeboorte
                       ||';'||r_rec.gba_ex_datoverlijden
                       ||';'||r_rec.gba_ex_straat
                       ||';'||r_rec.gba_ex_huisnr
                       ||';'||r_rec.gba_ex_toevoeging
                       ||';'||r_rec.gba_ex_postcode
                       ||';'||r_rec.gba_ex_gemeentedeel
                       ||';'||r_rec.gba_ex_landcode
                       ||';'||r_rec.ind_scheiding_al_bekend_rle
                       ||';'||r_rec.datum_opgenomen_gbabatch_dln
                       ||';'||r_rec.datum_verwerkt_gba_dln_geg
                       ||';'||r_rec.ind_gba_fout_dln
                       ||';'||r_rec.ind_ex_al_bekend_rle
                       ||';'||r_rec.datum_opgenomen_gbabatch_ex
                       ||';'||r_rec.datum_verwerkt_gba_ex_geg
                       ||';'||r_rec.ind_gba_fout_ex
                       ||';'||r_rec.ind_scheiding_alsnog_bekendrle
                       ||';'||r_rec.ind_ex_aangemaakt_rle
                       ||';'||r_rec.ind_rel_koppeling_aangemaakt
                       ||';'||r_rec.ind_rel_koppeling_gewijzigd
                       ||';'||r_rec.ind_workflow_case_aangemaakt
                       ||';'||r_rec.ind_opgenomen_in_werkbak
                       ||';'||r_rec.ind_verwerkt
                       ||';'||r_rec.intern_kenmerk
                       ||';'||r_rec.datum_aanlevering
                       ||';'||r_rec.bestandsnaam
                       );

  end loop;


  open c_ind (p_bestandsnaam);
  fetch c_ind into l_aantal, l_verwerkt, l_wacht_huw, l_wacht_controle, l_wacht_verw_ex, l_wacht_registratie, l_handm_verwerk ,l_teller_workflow  ;
  close c_ind;

  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal echtscheidingen in bronbestand: '||to_char(l_aantal));
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal echtscheidingen volledig verwerkt: '||to_char(l_verwerkt));
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal echtscheidingen die wachten op verwerking huwelijkse gegevens (GBA): '||to_char(l_wacht_huw));
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal echtscheidingen die wachten op controle ex: '||to_char(l_wacht_controle));
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal echtscheidingen die wachten op verwerking gegevens ex (GBA): '||to_char(l_wacht_verw_ex));
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal echtscheidingen die wachten op registratie in RLE: '||to_char(l_wacht_registratie));
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal echtscheidingen die wachten op handmatige verwerking in de werkbak: '||to_char(l_handm_verwerk));
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer,  'Aantal workflowcases die wachten op handmatige verwerking in de werkbak: '||to_char(l_teller_workflow)); --te doen
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer, ' ');

  utl_file.fclose_all;

  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer, '********** EINDE VERSLAG CREATIE OVERZICHT BRONTABEL EX_PARNTERS  **********');
  stm_util.insert_lijsten ( l_lijstnummer, l_regelnummer, 'Eindtijd: ' || to_char ( sysdate, 'dd-mm-yyyy hh24:mi:ss' ));
  stm_util.debug ('Einde logverslag');
  --
  stm_util.insert_job_statussen ( l_volgnummer, 'S', '0' );
  --
  commit;
  --
exception
  when e_fout_bestand
  then
    rollback;
    stm_util.insert_job_statussen(l_volgnummer
                                 ,'S'
                                 ,'1'
                                   );
    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                 ' procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  'De opgegeven bestandsnaam ('||p_bestandsnaam||') is onbekend.');
    commit;
  when others
  then
    stm_util.t_foutmelding := sqlerrm;
    stm_util.debug('Foutmelding  : ' || stm_util.t_foutmelding);
    stm_util.debug('In programma : ' || stm_util.t_programma_naam);
    stm_util.debug('In procedure : ' || stm_util.t_procedure);

    stm_util.insert_job_statussen(l_volgnummer, 'S', '1');
    stm_util.insert_job_statussen(l_volgnummer,
                                  'I',
                                  stm_util.t_programma_naam ||
                                  ' procedure : ' || stm_util.t_procedure);
    --
    if stm_util.t_foutmelding is not null then
      stm_util.insert_job_statussen(l_volgnummer,
                                    'I',
                                    stm_util.t_foutmelding);
    end if;
    --
    if stm_util.t_tekst is not null then
      stm_util.insert_job_statussen(l_volgnummer, 'I', stm_util.t_tekst);
    end if;
    --
    rollback;
    --
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'S'
                                  , '1'
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , stm_util.t_programma_naam || '.' || stm_util.t_procedure
                                  );
    stm_util.insert_job_statussen ( l_volgnummer
                                  , 'I'
                                  , substr(sqlerrm,1,250)
                                  );
END RLE_EXP_OVERZ_BRON;
 
/