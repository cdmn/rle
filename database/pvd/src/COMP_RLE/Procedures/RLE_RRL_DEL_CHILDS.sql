CREATE OR REPLACE PROCEDURE comp_rle.RLE_RRL_DEL_CHILDS
 (P_NUMRELATIE rle_relaties.numrelatie%TYPE
 ,P_CODROL rle_rollen.codrol%TYPE
 )
 IS
BEGIN
/* rle_rrle_del_childs */
DELETE	FROM rle_communicatie_nummers
WHERE	rle_numrelatie = p_numrelatie
AND	rol_codrol = p_codrol;
DELETE	FROM rle_relatie_adressen
WHERE	rle_numrelatie = p_numrelatie
AND	rol_codrol = p_codrol;
DELETE	FROM rle_financieel_nummers
WHERE	rle_numrelatie = p_numrelatie
AND	rol_codrol        = p_codrol;
END RLE_RRL_DEL_CHILDS;

 
/