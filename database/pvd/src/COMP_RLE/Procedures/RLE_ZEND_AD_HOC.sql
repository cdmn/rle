CREATE OR REPLACE PROCEDURE comp_rle.RLE_ZEND_AD_HOC
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_IND_AANGEMAAKT IN VARCHAR2
 )
 IS
cursor  c_asa( b_rle_numrelatie rle_afstemmingen_gba.rle_numrelatie%type )
is
select  'x'
from    rle_afstemmingen_gba asa
where   asa.rle_numrelatie     = b_rle_numrelatie
and     asa.afnemers_indicatie = 'J'
;
cursor c_prs(b_numrelatie rle_v_personen.numrelatie%type)
is
select prs.persoonsnummer
,      prs.sofinummer
,      prs.namrelatie
,      prs.voorvoegsels
,      prs.geslacht
,      prs.geboortedatum
from   rle_v_personen prs
where  prs.numrelatie = b_numrelatie
;
cursor c_vdnp (b_rle_numrelatie ret_v_deelnemerschappen.rle_numrelatie%type)
is
select 'x'
from   ret_v_deelnemerschappen vdnp
where  vdnp.rle_numrelatie = b_rle_numrelatie
;
l_deelnemerschap        boolean;
l_aantal_pensioen       number := 0;
l_adm_tab               wsf_adm.tab_adm;
l_kodegvw               wgr_v_werkzaamheden.code_groep_werkzhd%type;
l_kodesgw               wgr_v_werkzaamheden.code_subgroep_werkzhd%type;
l_code_functiecategorie cvc_v.code_functiecategorie%type;
r_adm                   rle_administraties%rowtype;
l_anummer               number(15);
l_namrelatie            rle_v_personen.namrelatie%TYPE;
l_postcode              varchar2(15);
l_huisnummer            number(10);
l_toevhuisnum           varchar2(20);
l_woonplaats            varchar2(50);
l_straatnaam            varchar2(50);
l_voorvoegsels          rle_v_personen.voorvoegsels%TYPE;
l_gemeentenummer        number(4);
l_datgeboorte           rle_v_personen.datgeboorte%TYPE;
l_geslacht              rle_v_personen.geslacht%TYPE;
l_sofinummer            rle_v_personen.sofinummer%TYPE;
l_persoonsnummer        rle_v_personen.persoonsnummer%TYPE;
l_afstemming_gba        boolean := false;
lv_dummy                varchar2(2000);
ld_dummy                date;
ln_dummy                number(10);
l_appl_code             VARCHAR2(3):= 'RLE';
l_produkt               VARCHAR2(10);
l_opdrachtgever         NUMBER(9);
r_prs                   c_prs%rowtype;
l_error                 varchar2(1000);
c_proc_name constant    varchar2(25) := 'RLE_RIE.ZEND_AD_HOC';
--
--EGIN
begin
   -- Bepalen GBA afstemming
   open c_asa( p_rle_numrelatie ) ;
   fetch c_asa into lv_dummy;
   l_afstemming_gba := c_asa%found;
   close c_asa;
   --
   if l_afstemming_gba
   then
      l_aantal_pensioen := rle_tel_pensioen(p_rle_numrelatie);
      --
      if  l_aantal_pensioen <= 1
      then
      begin
         --Bepalen of sprake is van een deelnemerschap
         open c_vdnp(p_rle_numrelatie);
         fetch c_vdnp into lv_dummy;
         l_deelnemerschap := c_vdnp%found;
         close c_vdnp;
         --
         if l_deelnemerschap
         or p_ind_aangemaakt = 'J'
         then
            --Ophalen GBA gegevens
            open c_prs( p_rle_numrelatie ) ;
            fetch c_prs INTO r_prs;
            close c_prs ;
            --Vorige arbeidsverhouding had geen pensioenproduct dus Ad-hoc bericht naar GBA.
            l_opdrachtgever := TO_NUMBER(stm_algm_get.sysparm(l_appl_code
                                                             , 'OPDR_GEVER'
                                                             , sysdate));
            --
            l_produkt := stm_algm_get.sysparm(l_appl_code
                                             , 'PRODUKT'
                                             , sysdate);
            --
            rle_get_adrsvelden ( l_opdrachtgever
                               , l_produkt
                               , p_rle_numrelatie
                               , 'PR'
                               , 'DA'
                               , sysdate
                               , l_postcode
                               , l_huisnummer
                               , l_toevhuisnum
                               , l_woonplaats
                               , l_straatnaam
                               , lv_dummy
                               , lv_dummy
                               , lv_dummy
                               , lv_dummy
                               , lv_dummy
                               , ld_dummy
                               , ld_dummy
                               ) ;
            --
            l_gemeentenummer:= per_lib.bepaal_gemeentenummer('N'
                                                            , null
                                                            , l_postcode);
            --
            l_anummer := rle_m03_rec_extcod('GBA'
                                            ,p_rle_numrelatie
                                            , 'PR'
                                            , sysdate);
            --
           gba_lib.schrijven_hq01_bericht( r_prs.persoonsnummer
                                          ,l_gemeentenummer
                                          ,l_anummer
                                          ,l_straatnaam
                                          ,l_huisnummer
                                          ,l_postcode
                                          ,r_prs.namrelatie
                                          ,r_prs.voorvoegsels
                                          ,r_prs.geboortedatum
                                          ,r_prs.geslacht
                                          ,'Koppeling pensioen. Bijwerken verwanten.'
                                          ,9
                                          ,r_prs.sofinummer);
         end if;
         exception
         when others then
           if sqlcode between -20999 and -20000
            then
               null;
           else
               raise;
            end if;
         end;
         --
      end if;
     --
   end if;
   --
exception
when others
then
   l_error := c_proc_name||' rle numrelatie : '||p_rle_numrelatie||' fout: '||sqlerrm;
   raise_application_error(-20000, l_error);
END RLE_ZEND_AD_HOC;
 
/