CREATE OR REPLACE PROCEDURE comp_rle.RLE_NPE_CHK_AANW
 (P_LND_CODLAND VARCHAR2
 ,P_HUISNUMMER NUMBER
 ,P_POSTCODE VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_LND_002
 (B_CODLAND IN VARCHAR2
 )
 IS
/* C_LND_002 */
select	*
from	rle_landen lnd
where	lnd.codland = b_codland;
/* Ophalen straat en wnpl van postcode */
CURSOR C_NPE_001
 (B_PCODE IN VARCHAR2
 ,B_HUISNR IN NUMBER
 )
 IS
/*	C_PCOD01 */
/* 	codreeks 0 is oneven huisnr¿s
	codreeks 1 is even huisnr¿s
	codreeks 2 en 3 zijn woonboten/wagens geen huisnr contrle.
*/
SELECT npe.woonplaats
,      npe.straatnaam
FROM	rle_ned_postcode npe
WHERE	npe.postcode = b_pcode
AND	(	(	npe.numscheidingvan <= b_huisnr
		AND	npe.numscheidingtm >= b_huisnr
		AND	npe.codreeks <> MOD(b_huisnr,2)
		)
	OR	(	npe.numscheidingvan <= 0
		AND	npe.numscheidingtm >= 0
		AND	npe.codreeks IN (2,3)
		AND	NOT EXISTS	(	SELECT	1
						FROM	rle_ned_postcode	npe2
						WHERE	npe2.postcode = npe.postcode
						AND	npe2.numscheidingvan <= b_huisnr
						AND	npe2.numscheidingtm >= b_huisnr
						AND	npe2.codreeks <> MOD(b_huisnr,2)
					)
		)
	)
;
-- Program Data
R_NPE_001 C_NPE_001%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_NPE_CHK_AANW */
/* Postcode controle als controletabel = Ned. postcode. Lees in tabel met
codeland. Als controletabel is'Nederlandse postcode, dan moet  voorkomen:
postcode en scheidingsnrvan <= huisnummer <= scheidingsnrt/m tevens vindt er een
controle plaats of huisnummer even of oneven is*/
for r_lnd_002 in c_lnd_002(p_lnd_codland) loop
	if	r_lnd_002.controletabel = 'NLP' then
		open	c_npe_001(p_postcode
		,	p_huisnummer);
		fetch	c_npe_001 into r_npe_001;
		if	c_npe_001%notfound then
			stm_dbproc.raise_error('RLE-000391',null,'rle_npe_chk_aanw');
		end if;
		close	c_npe_001;
	end if;
end loop;
END RLE_NPE_CHK_AANW;

 
/