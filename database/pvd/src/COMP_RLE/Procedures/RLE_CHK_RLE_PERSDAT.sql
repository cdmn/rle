CREATE OR REPLACE PROCEDURE comp_rle.RLE_CHK_RLE_PERSDAT
 (P_NUMRELATIE IN NUMBER
 ,P_DATINGANG IN DATE
 ,P_TABEL IN VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
select	*
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_CHK_RLE_PERSDAT */
/* Bij personen mag de begindatum niet na overlijdensdatum en niet voor de
geboortedatum liggen */
for r_rle_001 in c_rle_001(p_numrelatie) loop
	if	r_rle_001.indinstelling = 'N' then
		if	STM_ALGM_CHECK.ALGM_DATCOMP(p_datingang
			,	'>', r_rle_001.datoverlijden) = 'J'
		or	p_datingang < r_rle_001.datgeboorte then
			stm_dbproc.raise_error('RLE-00387'
			,	'#1'||p_tabel
			,	'rle_chk_rle_persdat');
		end if;
	end if;
end loop;
END RLE_CHK_RLE_PERSDAT;

 
/