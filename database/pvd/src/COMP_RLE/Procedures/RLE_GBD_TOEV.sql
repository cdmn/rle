CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBD_TOEV
 (PIN_NR_RELATIE IN NUMBER
 ,PIN_NR_PERSOON IN NUMBER
 ,PID_DAT_BEGIN IN DATE
 ,POV_VERWERKT OUT VARCHAR2
 ,POV_FOUTMELDING OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_GBD_001
 (CPN_NUMRELATIE IN NUMBER
 ,CPD_PEILDATUM IN DATE
 )
 IS
SELECT *
FROM   rle_gemoedsbezwaardheden  gbd
WHERE  gbd.rle_numrelatie = cpn_numrelatie
AND    gbd.dat_begin <= cpd_peildatum
AND    (  gbd.dat_einde >= TRUNC( cpd_peildatum )
       OR gbd.dat_einde IS NULL
       )
;
-- Program Data
LN_NUMRELATIE NUMBER;
R_GBD_001 C_GBD_001%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default uitvoer */
   pov_verwerkt := 'N' ;
   pov_foutmelding := NULL ;
   /* Controle invoerparameters */
   IF  pin_nr_relatie IS NULL
   AND pin_nr_persoon IS NULL
   THEN
      pov_foutmelding := 'Relatienummer of persoonsnummer is verplicht.' ;
      RETURN ;
   END IF ;
   IF pid_dat_begin IS NULL
   THEN
      pov_foutmelding := 'Ingangsdatum gemoedsbzwaardheid is verplicht.' ;
      RETURN ;
   END IF ;
   /* Bepalen relatienummer */
   IF pin_nr_relatie IS NOT NULL
   THEN
      ln_numrelatie := pin_nr_relatie ;
   ELSE
      rle_m06_rec_relnum( pin_nr_persoon
                        , 'PRS'
                        , 'PR'
                        , SYSDATE
                        , ln_numrelatie
                        ) ;
   END IF ;
   IF ln_numrelatie IS NULL
   THEN
      pov_foutmelding := 'Relatie kan niet worden gevonden.' ;
      RETURN ;
   END IF ;
   /* Bepalen gemoedsbezwaardheid */
   OPEN c_gbd_001( ln_numrelatie
                 , pid_dat_begin
                 ) ;
   FETCH c_gbd_001 INTO r_gbd_001 ;
   IF c_gbd_001%FOUND
   THEN
      CLOSE c_gbd_001 ;
      pov_foutmelding := 'Gemoedsbezwaardheid is reeds vastgelegd.' ;
      RETURN ;
   ELSE
      CLOSE c_gbd_001 ;
   END IF ;
   /* Registreren gemoedsbezwaardheid */
   INSERT INTO rle_gemoedsbezwaardheden(
        rle_numrelatie
      , dat_begin
      , dat_einde
      ) VALUES(
        ln_numrelatie
      , pid_dat_begin
      , NULL
      ) ;
   /* Verwerking ok */
   pov_verwerkt := 'J' ;
EXCEPTION
   WHEN OTHERS THEN
      /* Uitvoer */
      pov_verwerkt := 'N' ;
      pov_foutmelding := 'Gemoedsbezwaardheid is niet verwerkt. Onbekende fout.' ;
END RLE_GBD_TOEV;

 
/