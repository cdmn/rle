CREATE OR REPLACE PROCEDURE comp_rle.RLE_OPVOEREN_REL_EXTERNE_CODE
 (P_RELNUM IN NUMBER
 ,P_DWE_WRDDOM_EXTSYS IN VARCHAR2
 ,P_DATINGANG IN DATE
 ,P_CODROL IN VARCHAR2
 ,P_EXTERN_RELATIE IN VARCHAR2
 ,P_SUCCES OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
l_errmsg VARCHAR2(2000);
-- PL/SQL Block
BEGIN
  p_succes := NULL;
  INSERT INTO RLE_RELATIE_EXTERNE_CODES(
              ID
            , rle_numrelatie
            , dwe_wrddom_extsys
            , DATINGANG
            , CODORGANISATIE
            , ROL_CODROL
            , EXTERN_RELATIE
            )
			VALUES(
              rle_rec_seq1.NEXTVAL
            , p_relnum
            , p_dwe_wrddom_extsys
            , p_datingang
            , 'A'
            , p_codrol
            , p_extern_relatie
            );
---
  p_succes := 'J';
EXCEPTION
  WHEN OTHERS
  THEN
    p_succes := 'N';
    l_errmsg := SQLERRM;
	stm_dbproc.raise_error('Rle_Opvoeren_Rel_Externe_Code: '||l_errmsg);
END;

 
/