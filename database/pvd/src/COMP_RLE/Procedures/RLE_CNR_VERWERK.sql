CREATE OR REPLACE PROCEDURE comp_rle.RLE_CNR_VERWERK
 (P_NUMRELATIE IN NUMBER
 ,P_CODROL IN VARCHAR2
 ,P_SRT_COM IN VARCHAR2
 ,P_PEILDATUM IN DATE
 ,P_NUMCOMMUNICATIE IN VARCHAR2
 ,P_AUTHENTIEK IN VARCHAR2 := 'N'
 ,P_RAS_ID NUMBER := null
 )
 IS
  cursor c_cnr (
    b_numrelatie     rle_communicatie_nummers.rle_numrelatie%type
  , b_codrol           rle_communicatie_nummers.rol_codrol%type
  , b_srt_com         rle_communicatie_nummers.dwe_wrddom_srtcom%type
  , b_peildatum      date
  , b_adm_code     rle_communicatie_nummers.adm_code%type
  , b_ras_id           rle_communicatie_nummers.ras_id%type)
  is
    select cnr.datingang
         , cnr.numcommunicatie
         , cnr.authentiek
         , cnr.rowid
    from   rle_communicatie_nummers cnr
    where  cnr.rle_numrelatie = b_numrelatie
    and    cnr.dwe_wrddom_srtcom = b_srt_com
    and    nvl ( cnr.rol_codrol, nvl ( b_codrol, 'leeg' ) ) = nvl ( b_codrol, 'leeg' )
    and    nvl ( b_peildatum, trunc ( sysdate ) ) between cnr.datingang
                                                      and nvl ( cnr.dateinde, nvl ( b_peildatum, trunc ( sysdate ) ) )
    and    cnr.adm_code = b_adm_code
    and    (cnr.ras_id = b_ras_id or b_ras_id is null) -- xjb, #DPL 28-2-2013
    order by cnr.datingang desc;

  r_cnr               c_cnr%rowtype;
  authentiek          exception;
  l_administratie  rle_communicatie_nummers.adm_code%type;
begin
  l_administratie  := nvl ( stm_context.administratie, 'ALG' );

  open c_cnr ( p_numrelatie
             , p_codrol
             , p_srt_com
             , p_peildatum
             , l_administratie
             , p_ras_id );

  fetch c_cnr
  into   r_cnr;

  if c_cnr%notfound
  then
    close c_cnr;

    if p_numcommunicatie is not null
    then
      insert into rle_communicatie_nummers ( numcommunicatie
                                           , rle_numrelatie
                                           , dwe_wrddom_srtcom
                                           , datingang
                                           , indinhown
                                           , rol_codrol
                                           , authentiek
                                           , adm_code
                                           , ras_id )
      values ( p_numcommunicatie
             , p_numrelatie
             , p_srt_com
             , nvl ( trunc ( p_peildatum ), trunc ( sysdate ) )
             , 'N'
             , p_codrol
             , p_authentiek
             , l_administratie
             , p_ras_id );
      -- oude rij wordt afgesloten in after statement trigger.
    end if;
  else
    /* c_cnr%FOUND */
    close c_cnr;

    if p_authentiek <> r_cnr.authentiek
       and r_cnr.authentiek = 'J'
    then
      -- Authentieke nummers niet overschrijven
      raise authentiek;
    end if;

    if trunc(nvl(p_peildatum,sysdate)) = r_cnr.datingang
    then
      if p_numcommunicatie <> r_cnr.numcommunicatie
      then
        -- communicatienummer met zelfde begindatum wijzigen
        update rle_communicatie_nummers
        set    numcommunicatie = p_numcommunicatie,
               authentiek = p_authentiek
        where  rowid = r_cnr.rowid;
      elsif p_numcommunicatie = r_cnr.numcommunicatie
            and p_numcommunicatie is not null
            and p_authentiek <> r_cnr.authentiek
      then
        -- authentiek bijwerken
        update rle_communicatie_nummers
        set    authentiek        = p_authentiek
        where  rowid = r_cnr.rowid;
      elsif p_numcommunicatie is null
      then
        -- communicatienummer met zelfde begindatum verwijderen
        delete rle_communicatie_nummers
        where  rowid = r_cnr.rowid;
      end if;
    elsif p_numcommunicatie <> r_cnr.numcommunicatie
    then
      -- rij aanmaken met nieuw communicatienummer
      -- oude rij wordt afgesloten in after statement trigger
      insert into rle_communicatie_nummers ( numcommunicatie
                                           , rle_numrelatie
                                           , dwe_wrddom_srtcom
                                           , datingang
                                           , indinhown
                                           , rol_codrol
                                           , authentiek
                                           , adm_code
                                           , ras_id )
      values ( p_numcommunicatie
             , p_numrelatie
             , p_srt_com
             , nvl ( trunc ( p_peildatum ), trunc ( sysdate ) )
             , 'N'
             , p_codrol
             , p_authentiek
             , l_administratie
             , p_ras_id );
    elsif p_numcommunicatie is null
    then
      -- oude rij afsluiten
      update rle_communicatie_nummers
      set    dateinde          = nvl ( trunc ( p_peildatum ), trunc ( sysdate ) ) - 1
      where  rowid = r_cnr.rowid;
    end if;
  end if;
exception
  when authentiek
  then
    raise_application_error ( -20001, 'Het reeds bestaande communicatienummer is authentiek en mag alleen door het portaal worden gewijzigd!' );
end;
 
/