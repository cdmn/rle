CREATE OR REPLACE PROCEDURE comp_rle.RLE_RLE_CHILDPER
 (P_NUMRELATIE IN NUMBER
 ,P_DATINGANG IN OUT DATE
 ,P_DATEINDE  IN OUT DATE
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
select	*
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_RLE_CHILDPER
 * geldigheidscheck van de child tov. parent RELATIES.
 * als de ingangsdatum van de child voor die van de parent ligt, wordt
 * deze automatisch aangepast.
 */
  /* In de datumcontrole kan de rle_datbegin of de rle_datgeboorte de NULL
     waarde bevatten. De controle moet dan toch uitgevoerd worden met een lage
     datum. Hierbij wordt derhalve 01-01-1100 gebruikt als vergelijkingsdatum.
 */
for r_rle_001 in c_rle_001(p_numrelatie)
loop  /* een loop is hier niet echt nodig want er kan er toch maar een gevonden worden */
   if r_rle_001.indinstelling = 'J'
   then
      if p_datingang < r_rle_001.datbegin
      then
         stm_util.debug('RLE_RLE_CHILDPER: ingangsdatum childrecord aangepast aan begindatum relatie');
         p_datingang := r_rle_001.datbegin;
      end if;
      STM_ALGM_CHECK.ALGM_DATGLDH(nvl(r_rle_001.datbegin,to_date('01-01-1100','DD-MM-YYYY'))
  			,	r_rle_001.dateinde
  			,	p_datingang
  			,	p_dateinde
  			,	'Relaties');
   else
      if p_datingang < r_rle_001.datgeboorte
      then
         stm_util.debug('RLE_RLE_CHILDPER: ingangsdatum childrecord aangepast aan geboortedatum');
         p_datingang := r_rle_001.datgeboorte;
      end if;
      STM_ALGM_CHECK.ALGM_DATGLDH(nvl(r_rle_001.datgeboorte,to_date('01-01-1100','DD-MM-YYYY'))
  			,	r_rle_001.datoverlijden
  			,	p_datingang
  			,	p_dateinde
  			,	'Relaties');
   end if;
end loop;
END RLE_RLE_CHILDPER;

 
/