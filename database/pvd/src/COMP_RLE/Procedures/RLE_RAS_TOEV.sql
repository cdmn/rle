CREATE OR REPLACE procedure comp_rle.rle_ras_toev
 (p_numrelatie in number
 ,p_codrol in varchar2
 ,p_srt_adres in varchar2
 ,p_locatie in varchar2
 ,p_begindatum in date
 ,p_einddatum in date
 ,p_numadres in number
 ,p_ind_vervallen in varchar2 := 'N'
 ,p_ind_commit in varchar2 := 'N'
 ,p_ind_woonwagen in varchar2 := 'N'
 ,p_ind_woonboot in varchar2 := 'N'
 ,p_provincie in varchar2
 )
 is

  /*
   Wijzigingshistorie
   Datum        Atueur             Reden
   -----------------------------------------------------
   ??-??-????  ???                 Creatie
   30-06-2016   Elise Versteeg     MNGBA-1090, haal het actuele adres ipv adres per p_ingangsdatum
  */


  /* ophalen van huidig geldige relatie-adres */
  cursor c_ras_020 ( b_numrelatie in number
                   , b_codrol in varchar2
                   , b_srt_adres in varchar2
                   )
  is
  select rowid
  ,      ras.*
  from   rle_relatie_adressen ras
  where   ras.rle_numrelatie = b_numrelatie
  and     nvl(ras.rol_codrol,'@') = nvl(b_codrol,'@')
  and    ras.dwe_wrddom_srtadr = nvl(b_srt_adres,'@')
  and    ras.dateinde is null
  ;
  --
  cursor c_ras_da ( b_numrelatie in number
                  , b_codrol in varchar2
                  , b_srt_adres in varchar2
                  )
  is
  select rowid
  ,      ras.*
  from   rle_relatie_adressen ras
  where   ras.rle_numrelatie = b_numrelatie
  and     nvl(ras.rol_codrol,'@') = nvl(b_codrol,'@')
  and    ras.dwe_wrddom_srtadr = nvl(b_srt_adres,'@')
  ;

  r_ras_020  c_ras_020%rowtype;
  r_ras_da  c_ras_da%rowtype;
  l_persoonsnummer  number(9);
  l_begindatum  date;
  l_gba_status  varchar2(4);
  l_afnemersindicatie  varchar2(2);
  l_lokatie  varchar2(6);
begin
  l_lokatie := '000010';
  stm_util.debug('lokatie : '||l_lokatie);

  if upper(p_ind_vervallen) = 'J'
  then
    l_lokatie := '000020';
    stm_util.debug('lokatie : '||l_lokatie);

    delete
    from   rle_relatie_adressen
    where  rle_numrelatie = p_numrelatie
    and    ads_numadres = p_numadres
    ;

    l_lokatie := '000030';
    stm_util.debug('lokatie : '||l_lokatie);
  else
    /* kijken of relatieadres al voorkomt
       indien niet, dan toevoegen */
    if p_begindatum is null
    then
      l_begindatum := trunc(sysdate);
    else
      l_begindatum := trunc(p_begindatum);
    end if;
    --
    l_lokatie := '000040';
    stm_util.debug('lokatie : '||l_lokatie);
    open c_ras_020( b_numrelatie => p_numrelatie
                  , b_codrol => p_codrol
                  , b_srt_adres => p_srt_adres
                  );
    fetch c_ras_020 into r_ras_020;
    if c_ras_020%notfound
    then
      l_lokatie := '000050';
      stm_util.debug('lokatie : '||l_lokatie);

      insert into rle_relatie_adressen
        ( datingang
        , rle_numrelatie
        , dwe_wrddom_srtadr
        , ads_numadres
        , codorganisatie
        , indinhown
        , rol_codrol
        , dateinde
        , locatie
        , ind_woonboot
        , ind_woonwagen
        , provincie
        )
      values
        ( l_begindatum
        , p_numrelatie
        , p_srt_adres
        , p_numadres
        , 'A'
        , 'N'
        , p_codrol
        , p_einddatum
        , p_locatie
        , p_ind_woonboot
        , p_ind_woonwagen
        , p_provincie
        );
    else
      --update alleen bij dezelfde begindatum,numrelatie
      --srt_adres,codrol, en een gewijzigd p_numadres.
      l_lokatie := '000060';
      stm_util.debug('lokatie : '||l_lokatie);

      if     r_ras_020.ads_numadres <> p_numadres
      then
        if r_ras_020.datingang = l_begindatum
        then
          update rle_relatie_adressen
          set    ads_numadres = p_numadres
          ,      datingang = l_begindatum
          ,      dateinde = p_einddatum
          ,      ind_woonboot = p_ind_woonboot
          ,      ind_woonwagen = p_ind_woonwagen
          ,      locatie = p_locatie
          where  rowid = r_ras_020.rowid
          ;
        else
          insert into rle_relatie_adressen
            ( datingang
            , rle_numrelatie
            , dwe_wrddom_srtadr
            , ads_numadres
            , codorganisatie
            , indinhown
            , rol_codrol
            , dateinde
            , locatie
            , ind_woonboot
            , ind_woonwagen
            , provincie
            )
          values
            ( l_begindatum
            , p_numrelatie
            , p_srt_adres
            , p_numadres
            , 'A'
            , 'N'
            , p_codrol
            , p_einddatum
            , p_locatie
            , p_ind_woonboot
            , p_ind_woonwagen
            , p_provincie
            );
        end if;
      end if;
    end if;
    close c_ras_020;
    --
    if p_srt_adres = 'OA'
    then
      l_lokatie := '000070';
      stm_util.debug('lokatie : '||l_lokatie);
      open c_ras_da ( b_numrelatie => p_numrelatie
                    , b_codrol => p_codrol
                    , b_srt_adres => 'DA'
                    );
      fetch c_ras_da into r_ras_da;
      if c_ras_da%notfound
      then
        l_lokatie := '000080';
        stm_util.debug('lokatie : '||l_lokatie);
        insert
        into rle_relatie_adressen
          ( datingang
          , rle_numrelatie
          , dwe_wrddom_srtadr
          , ads_numadres
          , codorganisatie
          , indinhown
          , rol_codrol
          , dateinde
          , locatie
          , ind_woonboot
          , ind_woonwagen
          , provincie
          )
        values
          ( l_begindatum
          , p_numrelatie
          , 'DA'
          , p_numadres
          , 'A'
          , 'N'
          , p_codrol
          , p_einddatum
          , p_locatie
          , p_ind_woonboot
          , p_ind_woonwagen
          , p_provincie
          );
      else
        l_lokatie := '000090';
        stm_util.debug('lokatie : '||l_lokatie);
        rle_gba_get_status( pin_persoonsnummer => l_persoonsnummer
                          , pin_numrelatie => p_numrelatie
                          , pov_gba_status => l_gba_status
                          , pov_gba_afnemers_indicatie => l_afnemersindicatie
                          );
         --
         stm_util.debug('l_gba_status = '||l_gba_status);
         stm_util.debug('l_afnemersindicatie = '||l_afnemersindicatie);
         if     nvl(l_afnemersindicatie,'N') = 'N'
            and l_gba_status not in ('IA','TB','AA')
         then
           l_lokatie := '000110';
           stm_util.debug('lokatie : '||l_lokatie);
           insert into rle_relatie_adressen
             ( datingang
             , rle_numrelatie
             , dwe_wrddom_srtadr
             , ads_numadres
             , codorganisatie
             , indinhown
             , rol_codrol
             , dateinde
             , locatie
             , ind_woonboot
             , ind_woonwagen
             , provincie
             )
           values
             ( l_begindatum
             , p_numrelatie
             , 'DA'
             , p_numadres
             , 'A'
             , 'N'
             , p_codrol
             , p_einddatum
             , p_locatie
             , p_ind_woonboot
             , p_ind_woonwagen
             , p_provincie
             );
         end if;
       end if;
       close c_ras_da;
     end if;
     --
     if p_ind_commit = 'J'
     then
       l_lokatie := '000120';
       stm_util.debug('lokatie : '||l_lokatie);
       commit;
     end if;
  end if;
exception
  when others
  then
    stm_dbproc.raise_error('RLE_RAS_TOEV : '||nvl(l_lokatie,'leeg')||' '||sqlerrm);
end rle_ras_toev;
/