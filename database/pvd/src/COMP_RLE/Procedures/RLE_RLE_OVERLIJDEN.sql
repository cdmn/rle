CREATE OR REPLACE PROCEDURE comp_rle.RLE_RLE_OVERLIJDEN
 (P_NUMRELATIE IN NUMBER
 ,P_DATOVERLIJDEN IN DATE
 ,P_DATOVERLIJDEN_OLD IN date
 )
 IS

L_PRD_VOLGNUMMER VARCHAR2(20);
L_PRD_DAT_EINDE DATE;
L_SOORT_PERIODE VARCHAR2(50);
L_PRD_DAT_BEGIN DATE;
L_AVG_DAT_EINDE DATE;
L_AVG_DAT_BEGIN DATE;
L_WERKGEVER VARCHAR2(50);
L_DATOVERLIJDEN_OLD DATE;
L_PRD_VERWERKT VARCHAR2(10);
L_PRD_MELDING VARCHAR2(500);
L_DUM_DAT DATE;
L_DUM_CHAR VARCHAR2(50);
L_MELDING VARCHAR2(500);
L_INDICATIEFOUT VARCHAR2(5);
L_VERWERKT VARCHAR2(5);
L_REDENEINDE VARCHAR2(10);
L_NULL_DAT DATE;
L_NULL VARCHAR2(2);
L_AVG_ID VARCHAR2(30);
L_DATOVERLIJDEN DATE;
L_WERKNEMER VARCHAR2(50);
  --
  l_ind_event varchar2(100);
  --
  cursor c_prd (b_persoon prd_perioden.persoon%type)
  is
    select *
    from   prd_perioden
    where  persoon    = b_persoon
    ;
  --
begin
  /* RLE_RLE_overlijden */
  l_datoverlijden_old := p_datoverlijden_old;
  l_datoverlijden     := p_datoverlijden;
  l_redeneinde        := 'OVL';
  --
  -- Bepalen persoonnummer
  l_werknemer     := rle_m03_rec_extcod( 'PRS', p_numrelatie, 'PR', sysdate) ;
  --
  IF l_werknemer IS NULL THEN
    stm_dbproc.raise_error('RLE-00615','#1'||p_numrelatie||'#2PRS'||'#3PR '||
                                       '#4'||TO_CHAR(sysdate, 'DD-MM-YYYY'),'RLE_RLE_BRU');
  END IF;
  --
  -- Alle arbeidsverhoudingen en toekomstige arbeidsverhoudingen tov datum overlijden
  -- 21-09-2011 JKU verwijderd ivm RGG aanpassingen / geen melding richting de AVH meer.
  --
  -- Alle aktieve perioden en toekomstige perioden tov overlijdensdatum
  for r_prd in c_prd (b_persoon  => l_werknemer)
  loop
    --
    if r_prd.begindatum > l_datoverlijden
    then
      stm_dbproc.raise_error('RLE-00617','#1Periodes ('||'soort periode='||r_prd.soort_periode
                                                       ||' met datum BEGIN='||TO_CHAR(r_prd.begindatum, 'DD-MM-YYYY')||')'||
                                         '#2'||TO_CHAR(l_datoverlijden, 'DD-MM-YYYY'),'RLE_RLE_BRU');
    end if;
    --
    prd_prd_einde
    (P_PERSOON           => l_werknemer
    ,P_ARBEIDSVERHOUDING => null
    ,P_IND_AFHANKELIJK   => null
    ,P_EINDDATUM         => l_datoverlijden  --p_peildatum
    ,P_REDEN_EINDE       => 'OVL' -- 21-09-2011 JKU ivm RGG en Marvall INC 235704
    );
    --
  end loop;
  --
  -- Event voor afleiden OVT zit in trigger RLE_RLE_AUR (vanwege afhankelijkheid met geboortedatum)
  --
end rle_rle_overlijden;
 
/