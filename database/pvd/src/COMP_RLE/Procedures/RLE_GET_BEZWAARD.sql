CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_BEZWAARD
 (PIN_NUMRELATIE IN NUMBER
 ,PID_PEILDATUM IN DATE
 ,POV_IND_BEZWAARD OUT VARCHAR2
 ,POD_BEGINDATUM OUT DATE
 ,POD_EINDDATUM OUT DATE
 ,POV_CREATIE_DOOR OUT VARCHAR2
 ,POD_DAT_CREATIE OUT DATE
 )
 IS

CURSOR C_GBD_001
 (CPN_NUMRELATIE IN NUMBER
 ,CPD_PEILDATUM IN DATE
 )
 IS
SELECT *
FROM   rle_gemoedsbezwaardheden  gbd
WHERE  gbd.rle_numrelatie = cpn_numrelatie
AND    gbd.dat_begin <= cpd_peildatum
AND    (  gbd.dat_einde >= TRUNC( cpd_peildatum )
       OR gbd.dat_einde IS NULL
       )
;
CURSOR C_LEEG
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT min(gbd.dat_begin) dat_begin
,      CASE when max(nvl(gbd.dat_einde,to_date('01-01-3040','dd-mm-yyyy'))) = to_date('01-01-3040','dd-mm-yyyy')
       then NULL
	   ELSE max(gbd.dat_einde)
	   END  dat_einde
,      CASE when max(gbd.CREATIE_DOOR) like 'OPS%'
       THEN min(gbd.CREATIE_DOOR)
	   ELSE max(gbd.CREATIE_DOOR)
	   END  CREATIE_DOOR
,      CASE when max(gbd.DAT_CREATIE) like 'OPS%'
       THEN min(gbd.DAT_CREATIE)
	   ELSE max(gbd.DAT_CREATIE)
	   END  DAT_CREATIE
FROM   rle_gemoedsbezwaardheden  gbd
WHERE  gbd.rle_numrelatie = cpn_numrelatie
group  by gbd.RLE_NUMRELATIE;
--
R_LEEG C_LEEG%ROWTYPE;
R_GBD_001 C_GBD_001%ROWTYPE;
BEGIN
  if pid_peildatum is null then
   --
   OPEN  c_leeg( pin_numrelatie	) ;
   FETCH c_leeg INTO r_leeg;
   IF c_leeg%FOUND
   THEN
      pov_ind_bezwaard	:= 'J' ;
      pod_begindatum	:= r_leeg.dat_begin;
      pod_einddatum     := r_leeg.dat_einde;
      pov_creatie_door	:= r_leeg.creatie_door;
      pod_dat_creatie	:= r_leeg.dat_creatie;
   ELSE
      pov_ind_bezwaard := 'N' ;
   END IF ;
   CLOSE c_leeg;
   --
  else
   --
   -- ongewijzigd voor alle idioten die dat willen ??
   --
   OPEN  c_gbd_001( pin_numrelatie
                	,pid_peildatum
                	) ;
   FETCH c_gbd_001 INTO r_gbd_001;
   IF c_gbd_001%FOUND
   THEN
      pov_ind_bezwaard	:= 'J' ;
      pod_begindatum	:= r_gbd_001.dat_begin;
      pod_einddatum	:= r_gbd_001.dat_einde;
      pov_creatie_door	:= r_gbd_001.creatie_door;
      pod_dat_creatie	:= r_gbd_001.dat_creatie;
   ELSE
      pov_ind_bezwaard := 'N' ;
   END IF ;
   CLOSE c_gbd_001 ;
  end if;
END;
 
/