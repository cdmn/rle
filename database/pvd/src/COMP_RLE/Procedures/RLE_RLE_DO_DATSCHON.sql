CREATE OR REPLACE PROCEDURE comp_rle.RLE_RLE_DO_DATSCHON
 (P_RLE_NUMRELATIE NUMBER
 ,P_RLE_DATSCHONING DATE
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van alle communicatienummers */
CURSOR C_CNR_006
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_CNR_006 */
select	*
from	rle_communicatie_nummers cnr
where	cnr.rle_numrelatie = b_numrelatie;
/* Ophalen van relatie-adres */
CURSOR C_FNR_008
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_FNR_008 */
select	*
from	rle_financieel_nummers fnr
where	fnr.rle_numrelatie = b_numrelatie;
/* Ophalen van relatie-adres */
CURSOR C_RAS_016
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RAS_016 */
select	*
from	rle_relatie_adressen ras
where	ras.rle_numrelatie = b_numrelatie;
CURSOR C_RRL_007
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RRL_007 */
select	*
from	rle_relatie_rollen rrl
where	rrl.rle_numrelatie = b_numrelatie;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_RLE_DO_DATSCHON */
  /* Schonen rollen bij de relatie
  schoningsdatum van alle gerelateerde relatierol := schoningsdatum van de
  relatie */
  for r_rrl_007 in c_rrl_007(p_rle_numrelatie) loop
      if (r_rrl_007.datschoning is null
         ) then
  	   update rle_relatie_rollen rrl
  	      set rrl.datschoning = p_rle_datschoning
       where rle_numrelatie = r_rrl_007.rle_numrelatie
       and   rol_codrol     = r_rrl_007.rol_codrol;
      end if;
  end loop;
  /*
  Schonen van van alle gerelateerde relatieadressen
  Einddatum van alle gerelateerde relatieadressen := schoningsdatum
  van de relatie
  */
  for r_ras_016 in c_ras_016(p_rle_numrelatie) loop
      if (r_ras_016.dateinde is null
      and p_rle_datschoning >=  r_ras_016.datingang) then
  		update rle_relatie_adressen ras
      	set ras.dateinde =  p_rle_datschoning
       	where id = r_ras_016.id;
      end if;
  end loop;
  /*
  Schonen van alle gerelateerde communicatienummers
  */
  for r_cnr_006 in c_cnr_006(p_rle_numrelatie) loop
      if (r_cnr_006.dateinde is null
      and p_rle_datschoning >=  r_cnr_006.datingang) then
  	    update rle_communicatie_nummers cnr
  		   set cnr.dateinde =  p_rle_datschoning
       	where id = r_cnr_006.id;
      end if;
  end loop;
  /*
  Schonen van alle gerelateerde financieelnummers
  */
  for r_fnr_008 in c_fnr_008(p_rle_numrelatie) loop
      if (r_fnr_008.dateinde is null
      and p_rle_datschoning >=  r_fnr_008.datingang) then
  	    update rle_financieel_nummers fnr
  		   set fnr.dateinde =  p_rle_datschoning
        where rle_numrelatie = r_fnr_008.rle_numrelatie
        and   numrekening     = r_fnr_008.numrekening;
      end if;
  end loop;
END RLE_RLE_DO_DATSCHON;

 
/