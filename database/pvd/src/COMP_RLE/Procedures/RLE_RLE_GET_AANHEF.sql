CREATE OR REPLACE PROCEDURE comp_rle.RLE_RLE_GET_AANHEF
 (P_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,P_AANHEF OUT VARCHAR2
 ,P_AANSP_TITEL OUT VARCHAR2
 ,P_AANSP_TITEL_KLEIN OUT VARCHAR2
 )
 IS
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
--select	*
select namrelatie
,      indinstelling
,      dwe_wrddom_vvgsl
,      dwe_wrddom_geslacht
,      dwe_wrddom_gewatitel
,      naam_partner
,      vvgsl_partner
,      code_aanduiding_naamgebruik
,      datoverlijden
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;
-- Program Data
l_opmaaknaam VARCHAR2(500);
l_aanhef varchar2(100);
L_PARTNER_FNAAM VARCHAR2(500);
L_PARTNER_VOORVOEGSEL VARCHAR2(40);
L_EIGEN_VOORVOEGSEL VARCHAR2(40);
R_RLE_001 C_RLE_001%ROWTYPE;
L_IND_IBM VARCHAR2(1) := 'N';
L_EIGEN_NAAM VARCHAR2(184);
L_LENGTE_IBM NUMBER := 40;
L_EIGEN_FNAAM VARCHAR2(500);
L_PARTNER_NAAM VARCHAR2(184);
l_aansp_titel  varchar2(100);
-- Sub-Program Unit Declarations
/* Gebruikt in RLE_M11_GET_NAW om de voorletters van een naam op te maken */
-- PL/SQL Block
BEGIN
  l_opmaaknaam := NULL ;
  -- Ophalen eigen naam
  OPEN c_rle_001( P_NUMRELATIE ) ;
  FETCH c_rle_001 INTO r_rle_001 ;
  IF c_rle_001%NOTFOUND
  THEN
      CLOSE c_rle_001 ;
      RETURN;
  END IF ;
  close c_rle_001;
-- eerst de aanspreekwijze bepalen

   l_aanhef := 'heer, mevrouw ' ;
   l_aansp_titel := l_aanhef;
  IF r_rle_001.indinstelling =  'N'
  THEN
    l_aanhef := NULL ;
    IF r_rle_001.dwe_wrddom_gewatitel IS NOT NULL -- is er een code voor gewenste aanspreektitel
    THEN
            RFE_RFW_GET_WRD
                ( 'GWT'
               , r_rle_001.dwe_wrddom_gewatitel
               , l_ind_ibm
               , l_lengte_ibm
               , l_aanhef
              ) ;
       l_aansp_titel := l_aanhef;
    ELSIF r_rle_001.dwe_wrddom_geslacht = 'M'
    THEN
      l_aanhef := 'heer' ;
      l_aansp_titel := 'De heer';
    ELSIF r_rle_001.dwe_wrddom_geslacht = 'V'
    THEN
      l_aanhef := 'mevrouw' ;
      l_aansp_titel := 'Mevrouw';
    END IF ;
--  dan de opgemaakte naam bepalen tbv AANHEF
      -- Zoek voorvoegsel
    IF r_rle_001.dwe_wrddom_vvgsl IS NOT NULL
    THEN
        RFE_RFW_GET_WRD
                ( 'VVL'
                , r_rle_001.dwe_wrddom_vvgsl
                , l_ind_ibm
                , l_lengte_ibm
                , l_eigen_voorvoegsel
                ) ;
         --
         -- RTJ 26-11-2003 Call 91115 Eerste letter voorvoegsel moet hoofdletter worden
         --
         l_eigen_voorvoegsel := UPPER(substr(l_eigen_voorvoegsel,1,1))||substr(l_eigen_voorvoegsel,2);
         -- EINDE RTJ
    END IF ;
    IF r_rle_001.code_aanduiding_naamgebruik in ('P','V','N')
    THEN
       IF r_rle_001.vvgsl_partner IS NOT NULL
       THEN
            RFE_RFW_GET_WRD
                   ( 'VVL'
                   , r_rle_001.vvgsl_partner
                   , l_ind_ibm
                   , l_lengte_ibm
                   , l_partner_voorvoegsel
                   ) ;
         --
         -- RTJ 02-12-2003 nav callnr 91690
         --
         IF l_partner_voorvoegsel IS NULL
         THEN
            l_partner_voorvoegsel := r_rle_001.vvgsl_partner;
         END IF;
         -- EINDE RTJ
         --
         -- RTJ 26-11-2003 Call 91115 Eerste letter voorvoegsel moet hoofdletter worden
         --
         l_partner_voorvoegsel := UPPER(substr(l_partner_voorvoegsel,1,1))||substr(l_partner_voorvoegsel,2);
         -- EINDE RTJ
       END IF ;
    END IF ;
    l_eigen_fnaam := ltrim(rtrim(rtrim(l_eigen_voorvoegsel)||' '||r_rle_001.namrelatie));
    l_partner_fnaam := ltrim(rtrim(rtrim(l_partner_voorvoegsel)||' '||r_rle_001.naam_partner));
    IF r_rle_001.code_aanduiding_naamgebruik in ( 'P', 'V')
    THEN
       l_opmaaknaam := l_partner_fnaam;
    ELSIF NVL(r_rle_001.code_aanduiding_naamgebruik, 'E') in ('E', 'N')  -- JPG 21-10-2009
    THEN
       l_opmaaknaam := l_eigen_fnaam;
    END IF;
  END IF;
 /* 02-10-2006 NKU aanhef aanpassen igv overlijden */
    IF r_rle_001.datoverlijden is not null
   THEN
        p_aanhef := 'heer, mevrouw ' ;
  ELSE
       p_aanhef := l_aanhef || ' ' || l_opmaaknaam;
  END IF;
  p_aansp_titel := l_aansp_titel;
  p_aansp_titel_klein := lower(l_aansp_titel);

  --
END RLE_RLE_GET_AANHEF;
 
/