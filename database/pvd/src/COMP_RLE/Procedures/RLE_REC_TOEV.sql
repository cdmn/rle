CREATE OR REPLACE PROCEDURE comp_rle.RLE_REC_TOEV
 (P_ID IN OUT NUMBER
 ,P_RLE_NUMRELATIE IN NUMBER
 ,P_WRDDOM_EXTSYS IN VARCHAR2
 ,P_DATINGANG IN DATE
 ,P_CODORGANISATIE IN VARCHAR2
 ,P_ROL_CODROL IN VARCHAR2
 ,P_EXTERN_RELATIE IN VARCHAR2
 ,P_DATEINDE IN DATE
 ,P_NEPPARAMETER IN VARCHAR2
 ,P_IND_GEVERIFIEERD IN VARCHAR2
 ,P_IND_VERVALLEN IN VARCHAR2 := 'N'
 ,P_IND_COMMIT IN VARCHAR2 := 'N'
 ,P_IND_VERWERKT OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-externe_codes op rowid */
CURSOR C_REC_001
 (B_CODSYSTEEMCOMP IN VARCHAR2
 ,B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_REC_001 */
select	rec.*, rowid
from	rle_relatie_externe_codes rec
where	rec.dwe_wrddom_extsys = b_codsysteemcomp
and	rec.rle_numrelatie = b_numrelatie
and	(	(	rol_codrol is null
		and	b_codrol is null
		)
	or 	rol_codrol = b_codrol
	)
;
-- Program Data
R_REC_001 C_REC_001%ROWTYPE;
OTHERS EXCEPTION;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
p_ind_verwerkt := 'N' ;
 IF UPPER(p_ind_vervallen) = 'J'
 THEN
         DELETE FROM rle_relatie_externe_codes
         WHERE rle_numrelatie = p_rle_numrelatie
         AND extern_relatie = p_extern_relatie
         AND rol_codrol = p_rol_codrol
         AND dwe_wrddom_extsys = p_wrddom_extsys;
 ELSE
      OPEN c_rec_001( p_wrddom_extsys
		, p_rle_numrelatie
		, p_rol_codrol);
      FETCH c_rec_001 INTO r_rec_001;
      IF c_rec_001%notfound
      THEN
	CLOSE c_rec_001;
                  IF p_id IS NULL THEN
   	    p_id	 := rle_rec_seq1_next_id;
                  END IF;
	INSERT INTO rle_relatie_externe_codes
	( id
	, rle_numrelatie
	, dwe_wrddom_extsys
	, datingang
	, codorganisatie
	, rol_codrol
	, extern_relatie
	, dateinde
	, ind_geverifieerd
	)
	VALUES
	( p_id
	, p_rle_numrelatie
	, p_wrddom_extsys
	, p_datingang
	, p_codorganisatie
	, p_rol_codrol
	, p_extern_relatie
	, p_dateinde
	, p_ind_geverifieerd
	);
         ELSE
	UPDATE rle_relatie_externe_codes
	SET rle_numrelatie = p_rle_numrelatie
	, dwe_wrddom_extsys = p_wrddom_extsys
	, datingang = p_datingang
	, codorganisatie = p_codorganisatie
	, rol_codrol = p_rol_codrol
	, extern_relatie = p_extern_relatie
	, dateinde = p_dateinde
	, ind_geverifieerd = p_ind_geverifieerd
	WHERE ROWID = r_rec_001.rowid;
	CLOSE c_rec_001;
        END IF;
END IF;
IF p_ind_commit = 'J'
THEN
	COMMIT;
END IF;
p_ind_verwerkt := 'J' ;
EXCEPTION
 WHEN OTHERS THEN
  p_ind_verwerkt := 'N' ;
END RLE_REC_TOEV;

 
/