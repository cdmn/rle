CREATE OR REPLACE PROCEDURE comp_rle.RLE_ROL_CHK_GRPROL
 (P_CODROL IN VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen rol */
CURSOR C_ROL_003
 (B_CODROL IN VARCHAR2
 ,B_INDGROEPSROL IN VARCHAR2 := 'N'
 )
 IS
/* C_ROL_003 */
select	*
from	rle_rollen rol
where	rol.codrol = b_codrol
and	rol.indgroepsrol = b_indgroepsrol;
-- Program Data
R_ROL_003 C_ROL_003%ROWTYPE;
L_IND_FOUND VARCHAR2(1) := 'N';
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ROL_CHK_GRPROL */
open  c_rol_003(p_codrol, 'J');
fetch c_rol_003 into r_rol_003;
if c_rol_003%found then
  l_ind_found := 'J';
else
  l_ind_found := 'N';
end if;
close c_rol_003;
if l_ind_found = 'N' then
  stm_dbproc.raise_error('RLE-000416'
                       ,'#1'||p_codrol
                       ,'rle_rol_chk_grprol');
end if;
END RLE_ROL_CHK_GRPROL;

 
/