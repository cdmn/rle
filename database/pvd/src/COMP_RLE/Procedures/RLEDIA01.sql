CREATE OR REPLACE PROCEDURE comp_rle.RLEDIA01
 (P_SCRIPT_NAAM VARCHAR2
 ,P_SCRIPT_ID VARCHAR2
 )
 IS
   CURSOR c_rle_straatnaam
   IS
      SELECT rle.straatnaam straatnaam
           , rle.straatid straatid
           , rle.dat_mutatie
           , rle.mutatie_door
      FROM   rle_straten rle;

   CURSOR c_rle_woonplaatsen
   IS
      SELECT rle.woonplaats woonplaats
           , rle.woonplaatsid woonplaatsid
           , rle.dat_mutatie
           , rle.mutatie_door
      FROM   rle_woonplaatsen rle;

   --
   CURSOR c_rle_namen
   IS
      SELECT rle.namrelatie
           , rle.naam_partner
           , rle.numrelatie
           , rle.dat_mutatie
           , rle.mutatie_door
           , rrl.extern_relatie prsnr
      FROM   rle_relaties rle
           , rle_relatie_externe_codes rrl
      WHERE  rle.numrelatie = rrl.rle_numrelatie
      AND    rrl.dwe_wrddom_extsys = 'PRS'
      AND    rrl.rol_codrol = 'PR';

   --
   CURSOR c_gebruik_str_wpl_id(
      b_str_wpl_id   NUMBER
   )
   IS
      SELECT   rrl.rle_numrelatie rel_nr
             , rrl.extern_relatie ext_nr
             , rrl.dwe_wrddom_extsys rol_nr
      FROM     rle_relatie_adressen ras
             , rle_adressen ads
             , rle_relatie_externe_codes rrl
      WHERE    (    ads.stt_straatid = b_str_wpl_id
                 OR ads.wps_woonplaatsid = b_str_wpl_id )
      AND      ras.ads_numadres = ads.numadres
      AND      ras.rle_numrelatie = rrl.rle_numrelatie
      AND      SYSDATE BETWEEN ras.datingang AND ras.dateinde
      GROUP BY rrl.rle_numrelatie
             , rrl.extern_relatie
             , rrl.dwe_wrddom_extsys
      ORDER BY rrl.rle_numrelatie;

   r_rle_namen          c_rle_namen%ROWTYPE;
   r_rle_straatnaam     c_rle_straatnaam%ROWTYPE;
   r_rle_woonplaatsen   c_rle_woonplaatsen%ROWTYPE;
--
   t_db_name            VARCHAR2( 100 );
   t_locatie            VARCHAR2( 50 );
   t_lijstnummer        stm_lijsten.lijstnummer%TYPE   := 1;
   t_regelnummer        stm_lijsten.regelnummer%TYPE   := 0;
   t_volgnummer         NUMBER( 7 )                    := 0;
   t_sqlerrm            VARCHAR2( 255 );
   t_tel                NUMBER;
   t_teller             NUMBER;
--
BEGIN
   -- stm_util.set_debug_on;
   t_locatie := 'Controle rle_adressen';
   stm_util.t_programma_naam := 'RLEDIA01';
   stm_util.t_script_naam := UPPER( p_script_naam );
   stm_util.t_script_id := TO_NUMBER( p_script_id );
   --
   -- functionaliteit voor diacrieten / afwijkende tekens
   --
   --
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , 'Controle op vermoedelijke onjuiste tekens in de namen van STRATEN'
                          );
   --
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , ' '
                          );

   --
   FOR r_rle_straatnaam IN c_rle_straatnaam
   LOOP
      t_tel := 0;
      t_teller := LENGTH( r_rle_straatnaam.straatnaam );

      FOR i IN 1 .. t_teller
      LOOP
         IF    ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) < 32
            OR ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) BETWEEN 36 AND 37
            OR ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) BETWEEN 60 AND 64
            OR ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) BETWEEN 93 AND 95
            OR ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) BETWEEN 123 AND 137
            OR ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) BETWEEN 145 AND 153
            OR ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) BETWEEN 161 AND 191
            OR ASCII( SUBSTR( r_rle_straatnaam.straatnaam
                            , i
                            , 1
                            )) IN( 33, 91, 139, 155, 215, 247 )
         THEN
            t_tel := 1;
         END IF;

         EXIT WHEN t_tel = 1;
      END LOOP;

      IF t_tel = 1
      THEN
         --
         -- geef alle relatienummers waar deze str/wpl aan gekoppeld zijn
         --
         FOR r_gebruik_str_wpl_id IN c_gebruik_str_wpl_id( r_rle_straatnaam.straatid )
         LOOP
            IF c_gebruik_str_wpl_id%rowcount = 1
            THEN
               stm_util.insert_lijsten( t_lijstnummer
                                      , t_regelnummer
                                      , ' onjuist straatnaam met id : ' || r_rle_straatnaam.straatid || ' naam : ' || r_rle_straatnaam.straatnaam || ' dat mutatie : ' || r_rle_straatnaam.dat_mutatie || ' door : ' || r_rle_straatnaam.mutatie_door
                                      );
               stm_util.insert_lijsten( t_lijstnummer
                                      , t_regelnummer
                                      , ' nu volgt gebruik van deze straat in adressen van bepaalde relaties : '
                                      );
            END IF;

            stm_util.insert_lijsten( t_lijstnummer
                                   , t_regelnummer
                                   , ' rel nr : ' || r_gebruik_str_wpl_id.rel_nr || ' ext nr : ' || r_gebruik_str_wpl_id.ext_nr || ' rol ext nr : ' || r_gebruik_str_wpl_id.rol_nr
                                   );
         END LOOP;
      END IF;
   END LOOP;

   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , ' '
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , 'Controle op vermoedelijke onjuiste tekens in de namen van WOONPLAATSEN'
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , ' '
                          );

   --
   FOR r_rle_woonplaatsen IN c_rle_woonplaatsen
   LOOP
      t_tel := 0;
      t_teller := LENGTH( r_rle_woonplaatsen.woonplaats );

      FOR i IN 1 .. t_teller
      LOOP
         IF    ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) < 32
            OR ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) BETWEEN 36 AND 37
            OR ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) BETWEEN 60 AND 64
            OR ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) BETWEEN 93 AND 95
            OR ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) BETWEEN 123 AND 137
            OR ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) BETWEEN 145 AND 153
            OR ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) BETWEEN 161 AND 191
            OR ASCII( SUBSTR( r_rle_woonplaatsen.woonplaats
                            , i
                            , 1
                            )) IN( 33, 91, 139, 155, 215, 247 )
         THEN
            t_tel := 1;
         END IF;

         EXIT WHEN t_tel = 1;
      END LOOP;

      IF t_tel = 1
      THEN
         --
         -- geef alle relatienummers waar deze str/wpl aan gekoppeld zijn
         --
         FOR r_gebruik_str_wpl_id IN c_gebruik_str_wpl_id( r_rle_woonplaatsen.woonplaatsid )
         LOOP
            IF c_gebruik_str_wpl_id%rowcount = 1
            THEN
               stm_util.insert_lijsten( t_lijstnummer
                                      , t_regelnummer
                                      , ' onjuist woonplaats met id : ' || r_rle_woonplaatsen.woonplaatsid || ' naam : ' || r_rle_woonplaatsen.woonplaats || ' dat mutatie : ' || r_rle_woonplaatsen.dat_mutatie || ' door : '
                                        || r_rle_woonplaatsen.mutatie_door
                                      );
               stm_util.insert_lijsten( t_lijstnummer
                                      , t_regelnummer
                                      , ' nu volgt gebruik van deze woonplaats in adressen van bepaalde relaties : '
                                      );
            END IF;

            stm_util.insert_lijsten( t_lijstnummer
                                   , t_regelnummer
                                   , ' rel nr : ' || r_gebruik_str_wpl_id.rel_nr || ' ext nr : ' || r_gebruik_str_wpl_id.ext_nr || ' rol ext nr : ' || r_gebruik_str_wpl_id.rol_nr
                                   );
         END LOOP;
      END IF;
   END LOOP;

   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , ' '
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , 'Controle op vermoedelijke onjuiste tekens in de namen van Namen van RELATIES '
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , ' '
                          );

   FOR r_rle_namen IN c_rle_namen
   LOOP
      t_tel := 0;
      t_teller := LENGTH( r_rle_namen.namrelatie );

      FOR i IN 1 .. t_teller
      LOOP
         IF    ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) < 32
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) BETWEEN 33 AND 38
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) BETWEEN 40 AND 43
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) BETWEEN 47 AND 64
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) BETWEEN 91 AND 96
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) BETWEEN 123 AND 137
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) BETWEEN 145 AND 153
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) BETWEEN 160 AND 191
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) IN( 139, 155, 215, 247 )
            OR ASCII( SUBSTR( r_rle_namen.namrelatie
                            , i
                            , 1
                            )) > 255
         THEN
            t_tel := 1;
         END IF;

         EXIT WHEN t_tel = 1;
      END LOOP;

      IF     r_rle_namen.naam_partner IS NOT NULL
         AND t_tel = 0
      THEN
         t_teller := LENGTH( r_rle_namen.naam_partner );

         FOR i IN 1 .. t_teller
         LOOP
            IF    ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) < 32
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) BETWEEN 33 AND 38
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) BETWEEN 40 AND 43
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) BETWEEN 47 AND 64
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) BETWEEN 91 AND 96
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) BETWEEN 123 AND 137
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) BETWEEN 145 AND 153
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) BETWEEN 160 AND 191
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) IN( 139, 155, 215, 247 )
               OR ASCII( SUBSTR( r_rle_namen.naam_partner
                               , i
                               , 1
                               )) > 255
            THEN
               t_tel := 1;
            END IF;

            EXIT WHEN t_tel = 1;
         END LOOP;
      END IF;

      IF t_tel = 1
      THEN
         stm_util.insert_lijsten( t_lijstnummer
                                , t_regelnummer
                                ,    ' onjuist naam bij relnr : '
                                  || r_rle_namen.numrelatie
                                  || ' prsnr : '
                                  || r_rle_namen.prsnr
                                  || ' naam : '
                                  || r_rle_namen.namrelatie
                                  || ' partner : '
                                  || r_rle_namen.naam_partner
                                  || ' dat mutatie : '
                                  || r_rle_namen.dat_mutatie
                                  || ' door : '
                                  || r_rle_namen.mutatie_door
                                );
      END IF;
   END LOOP;

   --
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , ' '
                          );
   stm_util.insert_lijsten( t_lijstnummer
                          , t_regelnummer
                          , 'Einde verwerking RLEDIA01 ' || TO_CHAR( SYSDATE
                                                                  , 'dd-mm-yyyy hh24:mi:ss'
                                                                  )
                          );
   stm_util.insert_job_statussen( t_volgnummer
                                , 'S'
                                , '0'
                                );
   COMMIT;
EXCEPTION   -- hoofd
   WHEN OTHERS
   THEN
      ROLLBACK;
      stm_util.insert_job_statussen( t_volgnummer
                                   , 'S'
                                   , '1'
                                   );
      stm_util.insert_job_statussen( t_volgnummer
                                   , 'I'
                                   , SUBSTR( SQLERRM
                                           , 1
                                           , 255
                                           )
                                   );
      stm_util.insert_job_statussen( t_volgnummer
                                   , 'I'
                                   , t_locatie
                                   );
      COMMIT;
END RLEDIA01;
 
/