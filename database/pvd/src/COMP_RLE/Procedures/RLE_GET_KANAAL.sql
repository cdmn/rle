CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_KANAAL
 (P_NUMRELATIE IN RLE_KANAALVOORKEUREN.RLE_NUMRELATIE%TYPE
 ,P_PEILDATUM IN DATE
 ,P_KANAALVOORKEUR IN OUT VARCHAR2
 ,P_DATBEGIN OUT DATE
 ,P_DATEINDE OUT DATE
 )
 IS

L_IND_IBM VARCHAR2(1) := 'N';
L_LENGTE_IBM NUMBER := 40;
/*
Wijzigingshistorie
==============
24-11-2011   XCW            Alleen categorie algemeen ophalen

*/

/* Ophalen kanaalvoorkeuren van een relatie adhv NUMRELATIE */
CURSOR C_KVR_001
 (B_NUMRELATIE IN RLE_KANAALVOORKEUREN.RLE_NUMRELATIE%TYPE
 ,B_PEILDATUM IN date
 )
 IS
/* Cursor C_KVR_001
   2003-03-05 HVI: Wijziging
                   Cursor niet meer als "Sub Program Unit" vanwege
                   specifieke SELECT ipv "SELECT *"
*/
SELECT cck.dwe_wrddom_srtcom code_soort_kanaalvoorkeur  --kvr.code_soort_kanaalvoorkeur
     , kvr.dat_begin
     , kvr.dat_einde
FROM   rle_kanaalvoorkeuren  kvr
  ,      rle_communicatie_cat_kanalen cck
  ,      rle_communicatie_categorieen ccn
WHERE  kvr.rle_numrelatie = b_numrelatie
AND        kvr.dat_begin <= NVL(b_peildatum, SYSDATE)
AND        NVL(kvr.dat_einde, SYSDATE) >=
                NVL(b_peildatum , SYSDATE)
  and    kvr.cck_id = cck.id
  and    cck.ccn_id = ccn.id
  and    ccn.adm_code = 'ALG'
  and    ccn.code = 'ALG'
;
R_KVR_001   C_KVR_001%ROWTYPE;
BEGIN
  p_kanaalvoorkeur := NULL ;
  p_datbegin       := NULL ;
  p_dateinde       := NULL ;

  OPEN c_kvr_001( p_numrelatie,p_peildatum ) ;
  FETCH c_kvr_001 INTO r_kvr_001 ;
  IF c_kvr_001%FOUND
  THEN
       /* Ophalen referentie waarde voor kanaalvoorkeur */
        ibm.ibm( 'RFW_GET_WRD'
                , 'RLE'
                , 'KVR'
                , r_kvr_001.code_soort_kanaalvoorkeur
                , l_ind_ibm
                , l_lengte_ibm
                , p_kanaalvoorkeur
                ) ;
         p_datbegin := r_kvr_001.dat_begin ;
         p_dateinde := r_kvr_001.dat_einde ;
         CLOSE c_kvr_001;
  ELSE
         /* Geen rij gevonden */
         CLOSE c_kvr_001 ;
  END IF ;
END RLE_GET_KANAAL;
 
/