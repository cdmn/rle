CREATE OR REPLACE PROCEDURE comp_rle.RLE_M10_ADRES
 (P_OPDRACHTGEVER IN NUMBER
 ,P_PRODUKT IN varchar2
 ,P_NUMRELATIE IN NUMBER
 ,P_CODROL IN VARCHAR2
 ,P_SRTADRES IN varchar2
 ,P_DATUMP IN date
 ,P_IND_OPM IN varchar2
 ,P_AANT_REGELS IN number
 ,P_AANT_POS IN number
 ,P_LAYOUTCODE IN varchar2
 ,P_O_MELDING IN OUT varchar2
 ,P_O_REGEL1 IN OUT varchar2
 ,P_O_REGEL2 IN OUT varchar2
 ,P_O_REGEL3 IN OUT varchar2
 ,P_O_REGEL4 IN OUT varchar2
 ,P_O_REGEL5 IN OUT varchar2
 ,P_O_REGEL6 IN OUT varchar2
 )
 IS
/* Ophalen van relatie-adres */
CURSOR C_RAS_003
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 ,B_SRTADRES IN VARCHAR2
 ,B_DATUMP IN DATE
 )
 IS
  select ras.ads_numadres
  from   rle_relatie_adressen ras
  where  ras.rle_numrelatie = b_numrelatie
  and    nvl(ras.rol_codrol,'@') = nvl(b_codrol,'@')
  and    ras.dwe_wrddom_srtadr = b_srtadres
  and    ras.datingang <= b_datump
  and    nvl(dateinde,to_date('31-DEC-4712','DD-MON-YYYY')) >=
         nvl(b_datump,to_date('31-DEC-4712','DD-MON-YYYY'));
--
/* Ophalen van relatie-adres */
CURSOR C_RAS_004
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 ,B_DATUMP IN DATE
 )
 IS
select  ras.ads_numadres
,       nvl(sar.volgnr,99) volgnr
,       ras.dwe_wrddom_srtadr
from    rle_relatie_adressen ras
,       rle_sadres_rollen sar
where   ras.rle_numrelatie = b_numrelatie
and     nvl(ras.rol_codrol,'@') = nvl(b_codrol,'@')
and     ras.dwe_wrddom_srtadr = sar.dwe_wrddom_sadres(+)
and     ras.rol_codrol = sar.codrol(+)
and     ras.datingang <=b_datump
and     nvl(dateinde,to_date('31-DEC-4712','DD-MON-YYYY')) >=
        nvl(b_datump,to_date('31-DEC-4712','DD-MON-YYYY'))
order by        2;
--
/* Ophalen van relatie-adres */
CURSOR C_RAS_005
 (B_NUMRELATIE IN NUMBER
 ,B_DATUMP IN DATE
 )
 IS
select  ras.ads_numadres
,       ras.dwe_wrddom_srtadr
from    rle_relatie_adressen ras
where   ras.rle_numrelatie = b_numrelatie
and     ras.datingang <= b_datump
  and   nvl(dateinde,to_date('31-DEC-4712','DD-MON-YYYY')) >=
        nvl(b_datump,to_date('31-DEC-4712','DD-MON-YYYY'));
--
-- Program Data
L_NUMADRES               NUMBER;
L_PLAATS                 NUMBER;
R_RAS_003                C_RAS_003%ROWTYPE;
R_RAS_004                C_RAS_004%ROWTYPE;
R_RAS_005                C_RAS_005%ROWTYPE;
L_WAARDE                 VARCHAR2(1);
L_POSITIE                NUMBER;
L_MAX_POSITIE            NUMBER;
L_LAYOUTCODE             VARCHAR2(20);
L_IND_GEVONDEN           VARCHAR2(1);
l_gebruik_toegestaan     varchar2(2) := 'J';
l_soort_adres            varchar2(4);
l_peildatum              date;
l_opdrachtgever          number(10);
l_gegeven_type           varchar2(10):= 'ADRES';
l_leveringsovereenkomst  varchar2(10):= 'GBA';
-- Sub-Program Unit Declarations
BEGIN
  /* RLE_M10_ADRES */
  l_ind_gevonden:='N';
  --
   /* Aanpassing IVM RRGG: bij opvragen Domicilie adres
    eerst controleren of opdrachtgever/product wel geautoriseerd zijn
    */
   l_soort_adres   := p_srtadres;
   l_peildatum     := nvl (p_datump, sysdate);  -- OSP 19-08-2003
   l_opdrachtgever := p_opdrachtgever;
   --
   if p_srtadres is not null
   then
      if p_srtadres = 'DA'
      then
        ibm.ibm('CHK_GEGEVENSGEBRUIK'
               ,'RLE'
               ,p_produkt
               ,l_opdrachtgever
               ,l_gegeven_type
               ,l_leveringsovereenkomst
               ,l_peildatum
               ,l_gebruik_toegestaan);
      end if;
      --
      if l_gebruik_toegestaan = 'N'
      then
         p_o_melding := 'Opdrachtgever '||p_opdrachtgever||
                        ' met produkt '||p_produkt||
                        ' niet geautoriseerd.';
      else
          open   c_ras_003(p_numrelatie
           ,       p_codrol
           ,       p_srtadres
           ,       l_peildatum);
          fetch   c_ras_003 into r_ras_003;
          if      c_ras_003%FOUND
          then
                  l_numadres := r_ras_003.ads_numadres;
                  l_ind_gevonden := 'J';
                  close   c_ras_003;
          else
                  close   c_ras_003;
                  if p_codrol is not null
                  then
                     p_o_melding := 'Bij  rol '||p_codrol||
                                    ' is geen '||p_srtadres||
                                    ' adres gevonden.';
                  else
                      p_o_melding := 'Er is geen '||p_srtadres||
                                     ' adres gevonden (zonder rol).';
                  end if;
          end if;
      end if;
   end if; -- p_srt_adres is not null
   --
   if p_srtadres is null
   then
      if p_codrol is not null
      then
          l_plaats := 4;
          open    c_ras_004(p_numrelatie
          ,       p_codrol
          ,       l_peildatum);
          fetch   c_ras_004 into r_ras_004;
          if c_ras_004%FOUND
          then
              l_plaats := 6;
              l_numadres := r_ras_004.ads_numadres;
              l_ind_gevonden := 'J';
              l_soort_adres := r_ras_004.dwe_wrddom_srtadr;
          else
              open    c_ras_005(p_numrelatie
                               ,l_peildatum);
              fetch   c_ras_005 into r_ras_005;
              if      c_ras_005%FOUND
              then
                      l_numadres := r_ras_005.ads_numadres;
                      l_soort_adres := r_ras_005.dwe_wrddom_srtadr;
                      l_ind_gevonden := 'J';
              else
                      p_o_melding := 'Bij deze rol: '
                                   ||p_codrol||' is geen relatieadres gevonden';
              end if;
              close c_ras_005;
          end if;
          close c_ras_004;
      else
          open    c_ras_005(p_numrelatie
                           ,l_peildatum);
          fetch   c_ras_005 into r_ras_005;
          if      c_ras_005%FOUND
          then
                  l_numadres := r_ras_005.ads_numadres;
                  l_soort_adres := r_ras_005.dwe_wrddom_srtadr;
                  l_ind_gevonden := 'J';
          else
                  p_o_melding := 'Bij deze relatie '
                          ||p_numrelatie||' is geen relatieadres gevonden';
          end if;
          close   c_ras_005;
      end if;
      --
     /* Aanpassing IVM RRGG: Als het gevonden adres een DA adres is,
       controleren of opdrachtgever/product wel geautoriseerd zijn.
       bij geen autorisatie het 'OA' adres ophalen
      */
      if l_ind_gevonden ='J'
      and l_soort_adres = 'DA'
      then
         ibm.ibm('CHK_GEGEVENSGEBRUIK'
                ,'RLE'
                ,p_produkt
                ,l_opdrachtgever
                ,l_gegeven_type
                ,l_leveringsovereenkomst
                ,l_peildatum
                ,l_gebruik_toegestaan);
          --
          if l_gebruik_toegestaan = 'N'
          then
             l_soort_adres := 'OA';
             --
             open   c_ras_003(p_numrelatie
              ,       p_codrol
              ,       l_soort_adres
              ,       l_peildatum);
             fetch   c_ras_003 into r_ras_003;
             if      c_ras_003%FOUND
             then
                     l_numadres := r_ras_003.ads_numadres;
                     l_ind_gevonden := 'J';
                     close   c_ras_003;
             else
                     close   c_ras_003;
                     l_ind_gevonden :='N';
                     p_o_melding := 'Bij  relatie '||p_numrelatie||
                                    ' is geen '||l_soort_adres||
                                    ' adres gevonden.';
             end if;
          end if;
      end if;
   end if; -- p_srt_adres is null
  --
  if      l_ind_gevonden ='J'
  and     p_ind_opm = 'J' then
          rle_ads_get(l_numadres
          ,       p_aant_regels
          ,       p_aant_pos
          ,       p_layoutcode
          ,       p_o_melding
          ,       p_o_regel1
          ,       p_o_regel2
          ,       p_o_regel3
          ,       p_o_regel4
          ,       p_o_regel5
          ,       p_o_regel6);
  end if;
  --
END RLE_M10_ADRES;

 
/