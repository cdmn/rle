CREATE OR REPLACE procedure comp_rle.rle_mut_col_contract
 (
  p_wgr_werkgnr in varchar2
, p_adm_code in varchar2
, p_code_functiecategorie in varchar2
, p_dat_begin in date
, p_dat_einde in date
, p_succes out boolean
, p_errorcode out varchar2
)
is
l_errorcode varchar2(2000);
begin
   rle_rie.kop_rwg_cvc(p_wgr_werkgnr => p_wgr_werkgnr
                      ,p_adm_code    => p_adm_code
		      ,p_func_cat    => p_code_functiecategorie
		      ,p_dat_begin   => p_dat_begin
		      ,p_dat_einde   => p_dat_einde
		      ,p_error       => l_errorcode
		      );
  if l_errorcode is null
  then
     p_succes := true;
  else
    p_errorcode := l_errorcode;
    p_succes := false;
  end if;
end rle_mut_col_contract;
 
/