CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPDATE_SYN_STATUS
 (P_NUMRELATIE IN number
 ,P_RLE_INDICATIE IN varchar2 := null
 ,P_RLE_CODE_IND IN varchar2 := null
 ,P_GBA_INDICATIE IN varchar2 := null
 ,P_REDEN IN varchar2 := null
 ,P_GROEP IN varchar2 := null
 ,P_LAATSTE_CONTROLE IN date := null
 )
 IS

/*
Procedure:    RLE_UPDATE_SYN_STATUS
Omschrijving: Procedure voor het bijwerken van RLE_SYNCHRONISATIE_STATUSSEN

Wanneer     Wie     Reden
--------------------------------------------------------------------------------------
13-12-2013  XCW     Creatie procedure
18-02-2014  WHR     Bug parameter gewenste indicatie
03-02-2014  WHR     Groep toevoegen
24-02-2014  WHR     Reden leeg kunnnen maken ivm de RLEPRC71
14-03-2014  WHR     P_RLE_CODE_IND toevoegen
23-04-2014  AZM     Bij het van updaten van rle_synchronisatie_statussen de redenen aan elkaar plakken gescheiden door een komma
*/
begin
  --
  update rle_synchronisatie_statussen
  set    rle_gewenste_indicatie = nvl  ( p_rle_indicatie,   rle_gewenste_indicatie )
       , rle_code_gewenste_ind  = nvl  ( p_rle_code_ind,    rle_code_gewenste_ind )
       , rle_datum_indicatie    = nvl2 ( p_rle_indicatie,   sysdate, rle_datum_indicatie )
       , gba_afnemers_indicatie = nvl  ( p_gba_indicatie, gba_afnemers_indicatie )
       , gba_datum_indicatie    = nvl2 ( p_gba_indicatie, sysdate, gba_datum_indicatie )
       , reden                  = case when p_reden = '-'
                                       then null
                                       --else nvl ( substr ( p_reden , 1, 1000 ) , reden ) AZM 23-4-14 deze regel vervangen door de regel hieronder ivm FO-wijziging
                                       when reden like '%'||p_reden||'%'
                                       then reden
                                       else substr (nvl2(reden, reden||','||p_reden, p_reden), 1, 1000 )
                                  end
       , datum_reden            = case when p_reden = '-'
                                       then sysdate
                                       when reden like '%'||p_reden||'%'
                                       then datum_reden
                                       else nvl2 ( p_reden, sysdate, datum_reden )
                                  end
       , groep                  = nvl( substr( p_groep , 1, 4 ) , groep )
       , datum_groep            = nvl2 ( p_groep, sysdate, datum_groep )
       , datum_laatste_controle = nvl  (p_laatste_controle, datum_laatste_controle )
  where  rle_numrelatie         = p_numrelatie;
  --
  if sql%rowcount = 0
  then
    insert into rle_synchronisatie_statussen ( rle_numrelatie
                                             , rle_gewenste_indicatie
                                             , rle_code_gewenste_ind
                                             , rle_datum_indicatie
                                             , gba_afnemers_indicatie
                                             , gba_datum_indicatie
                                             , reden
                                             , datum_reden
                                             , groep
                                             , datum_groep
                                             , datum_laatste_controle )
    values ( p_numrelatie
           , p_rle_indicatie
           , p_rle_code_ind
           , nvl2 ( p_rle_indicatie, sysdate, null )
           , p_gba_indicatie
           , nvl2 ( p_gba_indicatie, sysdate, null )
           , case when p_reden = '-'
                  then null
                  else nvl  ( substr ( p_reden , 1, 1000 ) , null )
             end
           , case when p_reden = '-'
                  then sysdate
                  else nvl2 ( p_reden, sysdate, null )
             end
           , substr( p_groep, 1, 4 )
           , nvl2 ( p_groep, sysdate, null )
           , p_laatste_controle );
  end if;
  --
end rle_update_syn_status;
 
/