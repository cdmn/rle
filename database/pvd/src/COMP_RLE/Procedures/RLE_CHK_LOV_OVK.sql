CREATE OR REPLACE PROCEDURE comp_rle.RLE_CHK_LOV_OVK
 (P_RLE_NUMRELATIE IN NUMBER
 ,P_ADM_CODE IN VARCHAR2
 ,P_ROL_CODROL IN VARCHAR2
 )
 IS
/****************************************************************************************************************
Wijzigingshistorie:

Wanneer	       Wie                  Wat
-------------        ------------------  -------
30-01-2009    JTS   	              Procedure wordt aangeroepen door trigger RLE_RIE_BRD. Er mag geen record
                                                   worden weggegooid als er een levensloopovereenkomst is vastgelegd in
                                                   PRT_LL_OVEREENKOMSTEN.
09-02-2009    XMB                 Bovenstaande is ook van toepassing als de relatie een werkgever is.
****************************************************************************************************************/
--declaration
cursor c_lot_prs (b_rle_numrelatie in number
                 ,b_adm_code       in varchar2
                 )
is
select '1'
from   PRT_LL_OVEREENKOMSTEN  lot
,      PRT_PRODUCTEN          prt
where  PRT_PRODUCTCODE     = PRT.PRODUCTCODE
and    LOT.RELATIENR_PRS   = b_rle_numrelatie
and    PRT.ADM_CODE        = b_adm_code;
--
r_lot_prs   c_lot_prs%rowtype;
--
cursor c_lot_wgr (b_rle_numrelatie in number
                 ,b_adm_code       in varchar2
                 )
is
select '1'
from   PRT_LL_OVEREENKOMSTEN  lot
,      PRT_PRODUCTEN          prt
where  PRT_PRODUCTCODE     = PRT.PRODUCTCODE
and    LOT.RELATIENR_WGR   = b_rle_numrelatie
and    PRT.ADM_CODE        = b_adm_code;
--
r_lot_wgr   c_lot_wgr%rowtype;
--
BEGIN
--
if p_rol_codrol = 'PR'
then
  open c_lot_prs( p_rle_numrelatie
                , p_adm_code
                );
  fetch c_lot_prs into  r_lot_prs;
  if c_lot_prs%found
  then
    close c_lot_prs;
    stm_dbproc.raise_error('RLE-10399',
	 '#1'||' '||p_rle_numrelatie,'rle_chk_lov_ovk');
  end if;
  close c_lot_prs;
elsif p_rol_codrol = 'WG'
then
  open c_lot_wgr( p_rle_numrelatie
                , p_adm_code
                );
  fetch c_lot_wgr into  r_lot_wgr;
  if c_lot_wgr%found
  then
    close c_lot_wgr;
    stm_dbproc.raise_error('RLE-10399',
	 '#1'||' '||p_rle_numrelatie,'rle_chk_lov_ovk');
  end if;
  close c_lot_wgr;
end if;
--
END RLE_CHK_LOV_OVK;
 
/