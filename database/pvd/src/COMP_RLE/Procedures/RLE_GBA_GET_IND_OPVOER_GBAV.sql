CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_GET_IND_OPVOER_GBAV
 (PIN_PERSOONSNUMMER IN NUMBER
 ,PIN_NUMRELATIE IN NUMBER
 ,POV_IND_OPVOER_GBAV OUT VARCHAR2
 )
 IS

-- Sub-Program Unit Declarations
/* Zoeken afstemming gba met NUMRELATIE */
CURSOR C_ASA_001
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT *
FROM   rle_afstemmingen_gba  asa
WHERE  asa.rle_numrelatie = cpn_numrelatie
;
-- Program Data
R_ASA_001 C_ASA_001%ROWTYPE;
OTHERS EXCEPTION;
LN_NUMRELATIE NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default uitvoerwaarden */
   pov_ind_opvoer_gbav        := NULL ;
   /* Bepaal relatienummer */
   IF pin_numrelatie IS NOT NULL
   THEN
      ln_numrelatie := pin_numrelatie ;
   ELSE
      rle_m06_rec_relnum( pin_persoonsnummer
                        , 'PRS'
                        , 'PR'
                        , SYSDATE
                        , ln_numrelatie
                        ) ;
   END IF ;
   /* Controle */
   IF ln_numrelatie IS NULL
   THEN
      RETURN ;
   END IF ;
   /* Zoek afstemmingsgegevens */
   OPEN c_asa_001( ln_numrelatie ) ;
   FETCH c_asa_001 INTO r_asa_001 ;
   IF c_asa_001%FOUND
   THEN
      pov_ind_opvoer_gbav        := r_asa_001.ind_opvoer_gbav ;
   END IF ;
   CLOSE c_asa_001 ;
EXCEPTION
 WHEN OTHERS THEN
     /* Default waarden */
   pov_ind_opvoer_gbav             := NULL ;
END RLE_GBA_GET_IND_OPVOER_GBAV;
 
/