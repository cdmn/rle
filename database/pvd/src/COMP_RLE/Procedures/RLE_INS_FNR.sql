CREATE OR REPLACE procedure comp_rle.rle_ins_fnr
 (p_coda_elmcode in varchar2
 ,p_coda_iban in varchar2
 )
 is

--r_fnr       c_fnr%rowtype;
l_type_rle  varchar2 ( 1 );
l_pwr_nr    varchar2 ( 100 );
l_rlnr      number;
l_teller    number := 0;
begin
  l_type_rle := substr ( p_coda_elmcode
                       , 1
                       , 1
                       );
  --
  l_pwr_nr         := substr ( p_coda_elmcode, 2 );
  begin
    case l_type_rle
      when 'W'
      then
        l_rlnr           :=
          to_number ( rle_lib.get_relatienr_werkgever ( p_werkgevernummer => lpad ( trim ( l_pwr_nr )
                                                                                  , 6
                                                                                  , '0' ) ) );
      when 'P'
      then
        l_rlnr           := rle_lib.get_relatienr_persoon ( to_number ( l_pwr_nr ) );
      when 'R'
      then
        l_rlnr           := l_pwr_nr;
      else
        null;
    end case;
  exception
    when others
    then
      l_rlnr           := 0;
  end;
  insert into rle_financieel_nummers
   (rle_numrelatie
  ,numrekening
  ,lnd_codland
  ,dwe_wrddom_srtrek
  ,datingang
  ,codorganisatie
  ,tenaamstelling
  ,coddoelrek
  ,codwijzebetaling
  ,adm_code
  ,bic
  ,iban)
  values
  (l_rlnr
  ,null
  ,'NL'
  ,'AI'
  ,sysdate --to_date ('01011900', 'ddmmyyyy')
  ,'A'
  ,''
  ,'ALGRN'
  ,'AS'
  ,'ALG'
  ,null
  ,p_coda_iban)
  ;
end;
 
/