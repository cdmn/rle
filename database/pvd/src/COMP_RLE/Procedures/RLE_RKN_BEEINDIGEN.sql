CREATE OR REPLACE PROCEDURE comp_rle.RLE_RKN_BEEINDIGEN
 (PIN_NUMRELATIE IN RLE_RELATIES.NUMRELATIE%TYPE
 ,PIV_ROL_RELATIE IN RLE_ROLLEN.CODROL%TYPE
 ,PIV_ROL_GEKOPPELDE IN RLE_ROLLEN.CODROL%TYPE
 ,PID_EINDDATUM IN DATE
 )
 IS
BEGIN
   UPDATE rle_relatie_koppelingen  rkn
   SET    rkn.dat_einde = pid_einddatum
   WHERE  rkn.id IN ( SELECT sel_rkn.id
                      FROM   rle_relatie_koppelingen  sel_rkn
                      ,      rle_rol_in_koppelingen   sel_rkg
                      WHERE  sel_rkn.rkg_id                 = sel_rkg.id
                      AND    sel_rkn.rle_numrelatie         = pin_numrelatie
                      AND    sel_rkg.rol_codrol_met         = piv_rol_gekoppelde
                      AND    sel_rkg.rol_codrol_beperkt_tot = NVL( piv_rol_relatie, sel_rkg.rol_codrol_beperkt_tot )
                    ) ;
END ;

 
/