CREATE OR REPLACE PROCEDURE comp_rle.RLE_M06_REC_RELNUM
 (P_RELEXT IN varchar2
 ,P_SYSTEEMCOMP IN varchar2
 ,P_CODROL IN varchar2
 ,P_DATPARM IN date
 ,P_O_RELNUM IN OUT number
 )
 IS
-- Program Data
L_CODROL VARCHAR2(3);
L_EXTERNE_CODE VARCHAR2(20);
L_WGRNR NUMBER(20, 0);
-- PL/SQL Specification
/* Ophalen van relatie-externe_codes */
CURSOR C_REC
 (B_RELEXT IN VARCHAR2
 ,B_SYSTEEMCOMP IN VARCHAR2
 ,B_CODROL IN VARCHAR2
 ,B_DATPARM IN DATE
 )
 IS
/* C_REC */
select  rec.rle_numrelatie
from    rle_relatie_externe_codes rec
where   rec.extern_relatie       = b_relext
and     rec.dwe_wrddom_extsys   = nvl(b_systeemcomp,rec.dwe_wrddom_extsys)
and     rec.rol_codrol          = nvl(b_codrol,rec.rol_codrol )
and     trunc(rec.datingang)    <= trunc(nvl(b_datparm,rec.datingang))
and     (trunc(rec.dateinde)    >= trunc(nvl(b_datparm,rec.datingang))
        or rec.dateinde         is null)
order by nvl(rec.rol_codrol, 'zzz')
;
r_rec  c_rec%ROWTYPE;
-- PL/SQL Block
BEGIN
/* RLE_M06_REC_RELNUM */
l_codrol := p_codrol;
if p_systeemcomp = 'WGR'
or l_codrol = 'WG'
then
   l_wgrnr := to_number(p_relext);
   l_externe_code := ltrim(to_char(l_wgrnr,'099999'));
elsif p_systeemcomp = 'SOFI'
then /* sofinr wordt met voorloopnullen opgeslagen, maar wordt niet altijd zo ingevoerd */
   l_externe_code := ltrim(to_char(to_number(p_relext),'099999999'));
else
   l_externe_code := p_relext;
end if;
stm_util.debug('RLE_M06_REC_RELNUM: zoek externe code: '||l_externe_code||' syscomp: '||p_systeemcomp);
OPEN c_rec(l_externe_code
,	p_systeemcomp
,	l_codrol
,	p_datparm);
FETCH c_rec INTO r_rec;
p_o_relnum := NVL(r_rec.rle_numrelatie,p_o_relnum);
IF	c_rec%notfound THEN
	p_o_relnum := NULL;
END IF;
CLOSE c_rec;
END RLE_M06_REC_RELNUM;

 
/