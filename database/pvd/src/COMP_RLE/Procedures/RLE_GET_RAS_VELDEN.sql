CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_RAS_VELDEN
 (PIN_RAS_ID IN NUMBER
 ,PON_NUMRELATIE OUT NUMBER
 ,POV_CODROL OUT VARCHAR2
 ,POV_SRTADR OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen radr via radr_id */
CURSOR C_RAS_019
 (B_RAS_ID rle_relatie_adressen.id%type
 )
 IS
/* C_RAS_019 */
select *
from rle_relatie_adressen ras
where ras.id = b_ras_id;
-- Program Data
R_RAS_019 C_RAS_019%ROWTYPE;
OTHERS EXCEPTION;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Ophalen ras gegevens */
   OPEN c_ras_019( pin_ras_id ) ;
   FETCH c_ras_019 INTO r_ras_019 ;
   IF c_ras_019%NOTFOUND
   THEN
      RAISE NO_DATA_FOUND ;
   END IF ;
   CLOSE c_ras_019 ;
   pon_numrelatie := r_ras_019.rle_numrelatie ;
   pov_codrol     := r_ras_019.rol_codrol ;
   pov_srtadr     := r_ras_019.dwe_wrddom_srtadr ;
EXCEPTION
 WHEN OTHERS THEN
     pon_numrelatie := NULL ;
   pov_codrol     := NULL ;
   pov_srtadr     := NULL ;
   IF c_ras_019%ISOPEN
   THEN
      CLOSE c_ras_019 ;
   END IF ;
END RLE_GET_RAS_VELDEN;

 
/