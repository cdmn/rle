CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_GET_AANMELDING
 (PIN_PERSOONSNUMMER IN NUMBER
 ,PIN_NUMRELATIE IN NUMBER
 ,PON_AANMELDINGSNUMMER OUT NUMBER
 )
 IS
-- Sub-Program Unit Declarations
/* Zoeken aanmelding met NUMRELATIE */
CURSOR C_AMG_001
 (CPN_NUMRELATIE IN NUMBER
 )
 IS
SELECT *
FROM   rle_aanmeldingen  amg
WHERE  amg.rle_numrelatie = cpn_numrelatie
;
-- Program Data
LN_NUMRELATIE NUMBER;
R_AMG_001 C_AMG_001%ROWTYPE;
OTHERS EXCEPTION;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default uitvoerwaarden */
   pon_aanmeldingsnummer := NULL ;
   /* Bepaal relatienummer */
   IF pin_numrelatie IS NOT NULL
   THEN
      ln_numrelatie := pin_numrelatie ;
   ELSE
      rle_m06_rec_relnum( pin_persoonsnummer
                        , 'PRS'
                        , 'PR'
                        , SYSDATE
                        , ln_numrelatie
                        ) ;
   END IF ;
   /* Controle */
   IF ln_numrelatie IS NULL
   THEN
      RETURN ;
   END IF ;
   /* Zoek afstemmingsgegevens */
   OPEN c_amg_001( ln_numrelatie ) ;
   FETCH c_amg_001 INTO r_amg_001 ;
   IF c_amg_001%FOUND
   THEN
      pon_aanmeldingsnummer := r_amg_001.aanmeldingsnummer ;
   END IF ;
   CLOSE c_amg_001 ;
EXCEPTION
 WHEN OTHERS THEN
     /* Default waarden */
   pon_aanmeldingsnummer := NULL ;
END RLE_GBA_GET_AANMELDING;

 
/