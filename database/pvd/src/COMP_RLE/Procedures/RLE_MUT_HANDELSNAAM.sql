CREATE OR REPLACE PROCEDURE comp_rle.RLE_MUT_HANDELSNAAM
 (P_NUMRELATIE IN number
 ,P_HANDELSNAAM IN varchar2
 )
 IS
begin
  update rle_relaties
  set handelsnaam = p_handelsnaam
  where  numrelatie = p_numrelatie;
END RLE_MUT_HANDELSNAAM;
 
/