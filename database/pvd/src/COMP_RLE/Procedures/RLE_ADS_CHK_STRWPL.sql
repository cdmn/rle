CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADS_CHK_STRWPL
 (P_NEW_ADS_WPS_WOONPLAATSID NUMBER
 ,P_NEW_ADS_STT_STRAATID NUMBER
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_STT_003
 (B_STRAATID IN NUMBER
 )
 IS
/* C_STT_003 */
select	*
from	rle_straten stt
where	stt.straatid = b_straatid;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ADS_CHK_STRWPL */
/* Controles voor adres, woonplaats */
/* als straatid gevuld is dan lees straat met straatid */
/* woonplaatsid moet gelijk zijn aan woonplaats id uit straat */
if	p_new_ads_stt_straatid is not NULL then
  	for r_stt_003 in c_stt_003(p_new_ads_stt_straatid) loop
		if	p_new_ads_wps_woonplaatsid <>
				r_stt_003.wps_woonplaatsid then
	  		stm_dbproc.raise_error('RLE-00435', null, 'rle_ads_chk_strwpl');
		end if;
	end loop;
end if;
END RLE_ADS_CHK_STRWPL;

 
/