CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_FNR_TAG
 (P_NUMRELATIE IN number
 ,P_CMPCODE IN varchar2
 ,P_IND_R IN VARCHAR2
 ,P_FNR_TAB OUT bbs_rle_lib.t_fnr_tab
 )
 IS

  -- xcw 05-07-2013: voor administratie waarvoor de relatie wel een rol heeft maar geen tag
  -- willen we toch het financiele nummer doorgeven maar dan met tag = 0
  -- ukv 16-09-2014: verkeerd record werd opgehaald datingang desc gemaakt.
  -- ukv 13-10-2014: Default bank was een nested if, een niveau hoger geplaatst.
  -- ukv 14-10-2014: De default bank mag alleen gezet worden voor een lopende bankrekening.

  cursor c_fnr( b_numrelatie   number
              , b_ind_r_nummer varchar2) is
    select distinct *
      from (with fnr_adm as (select distinct fnr.id
                                            ,fnr.numrekening
                                            ,fnr.bic
                                            ,fnr.iban
                                            ,fnr.datingang
                                            ,fnr.dateinde
                                            ,fnr.codwijzebetaling
                                            ,ria.adm_code ria_adm_code
                               from rle_financieel_nummers   fnr
                                   ,rle_relatierollen_in_adm ria
                              where ria.rle_numrelatie = fnr.rle_numrelatie
                                and fnr.rle_numrelatie = b_numrelatie
                                and (fnr.dateinde is null or
                                     trunc(fnr.dateinde) > to_date('30062013', 'ddmmyyyy')))
             select fnr.id
                   ,fnr.numrekening
                   ,fnr.bic
                   ,fnr.iban
                   ,fnr.datingang
                   ,fnr.dateinde
                   ,fnr.ria_adm_code adm_code
                   ,nvl(fnc.tag, 0) tag
                   ,fnr.codwijzebetaling
               from fnr_adm        fnr
                   ,rle_finnr_coda fnc
              where fnc.fnr_id(+) = fnr.id
                and fnc.adm_code(+) = fnr.ria_adm_code
                and fnc.ind_r_nummer(+) = b_ind_r_nummer
             union
             select fnr.id
                   ,fnr.numrekening
                   ,fnr.bic
                   ,fnr.iban
                   ,fnr.datingang
                   ,fnr.dateinde
                   ,fnc.adm_code adm_code
                   ,nvl(fnc.tag, 0) tag
                   ,fnr.codwijzebetaling
               from rle_financieel_nummers fnr
                   ,rle_finnr_coda         fnc
              where fnr.id = fnc.fnr_id(+)
                and fnr.rle_numrelatie = b_numrelatie
                and (fnr.dateinde is null or
                    trunc(fnr.dateinde) > to_date('30062013', 'ddmmyyyy') or
                    fnc.tag is not null)
                and fnc.ind_r_nummer(+) = b_ind_r_nummer
              )
              order by datingang desc
                      ,tag       desc nulls last
                      ,iban
                      ,adm_code;


  type fnc_rec is record(
    adm_code    rle_finnr_coda.adm_code%type,
    iban        rle_financieel_nummers.iban%type,
    numrekening rle_financieel_nummers.numrekening%type);

  type fnc_tabtype is table of fnc_rec index by pls_integer;

  l_fnr_tab          bbs_rle_lib.t_fnr_tab;
  t_adm              stm_coda_pv.adm_tabtype;
  t_fnr_aanwezig_tab fnc_tabtype;
  l_teller           number := 0;
  lb_def_bank        boolean := false;
  l_vroegste_date       date;
  l_vroegste_teller     number := 1;
  lb_fnr_aanwezig    boolean := false;
begin
  t_adm := stm_coda_pv.get_pv_adm_code(p_gegevens_soort => 'DEBITEUR'
                                      ,p_bedrijfscode   => p_cmpcode
                                      ,p_code_rol       => null);
  --
  for i in t_adm.first .. t_adm.last loop
    -- de adminstraties van de rle (behorend bij de CODA cmpcode)
    stm_util.debug('t_adm(i).adm_code : ' || t_adm(i).adm_code);

    for v_fnr in c_fnr(b_numrelatie   => p_numrelatie
                      ,b_ind_r_nummer => p_ind_r)
    loop

      --stm_util.debug('fnr.id : '                || v_fnr.id);
      --stm_util.debug('fnr.numrekening : '       || v_fnr.numrekening);
      --stm_util.debug('fnr.bic : '               || v_fnr.bic);
      --stm_util.debug('fnr.iban : '              || v_fnr.iban);
      --stm_util.debug('fnr.datingang : '         || v_fnr.datingang);
      --stm_util.debug('fnr.dateinde : '          || v_fnr.dateinde);
      --stm_util.debug('fnc.adm_code adm_code : ' || v_fnr.adm_code);
      --stm_util.debug('nvl(fnc.tag, 0) tag : '   || v_fnr.tag);
      --stm_util.debug('fnr.codwijzebetaling : '  || v_fnr.codwijzebetaling);

      if t_adm(i).adm_code = nvl(v_fnr.adm_code, t_adm(i).adm_code) then

        if t_fnr_aanwezig_tab is not null and t_fnr_aanwezig_tab.count > 0 then

          for j in t_fnr_aanwezig_tab.first .. t_fnr_aanwezig_tab.last loop
            -- we gaan de fnr numnmers richting CODA bijhouden, want...

            if nvl(t_fnr_aanwezig_tab(j).iban, t_fnr_aanwezig_tab(j).numrekening) =
               nvl(v_fnr.iban, v_fnr.numrekening)
            then
              lb_fnr_aanwezig := true;
              exit; -- deze gaan we niet meer dan 1 keer retourneren...
            else
              lb_fnr_aanwezig := false;
            end if;
          end loop;
        end if;
        --
        if not lb_fnr_aanwezig -- Dit nummer zit nog niet in de PL/SQL tabel
        then
          --stm_util.debug('fnc.adm_code adm_code : ' || v_fnr.adm_code);
          -- dus die kan erbij
          l_teller := l_teller + 1;
          t_fnr_aanwezig_tab(l_teller).adm_code := v_fnr.adm_code;
          t_fnr_aanwezig_tab(l_teller).iban := v_fnr.iban;
          t_fnr_aanwezig_tab(l_teller).numrekening := v_fnr.numrekening;
          l_fnr_tab(l_teller).numrekening := v_fnr.numrekening;
          l_fnr_tab(l_teller).bic := v_fnr.bic;
          l_fnr_tab(l_teller).iban := v_fnr.iban;
          -- betaalwijze gaan we gebruiken voor default bank binnen CODA
          l_fnr_tab(l_teller).bet_wijze := '0';
          l_fnr_tab(l_teller).datingang := v_fnr.datingang;
          l_fnr_tab(l_teller).dateinde := v_fnr.dateinde;
          l_fnr_tab(l_teller).cmpcode := p_cmpcode;
          --stm_util.debug('l_fnr_tab(l_teller).tag : '   || v_fnr.tag);
          l_fnr_tab(l_teller).tag := v_fnr.tag;

          if  v_fnr.codwijzebetaling in ('AG', 'SB')
          and v_fnr.dateinde is null
          then
             -- dit wordt sowieso de default bank
             l_fnr_tab(l_teller).bet_wijze := '1';
             lb_def_bank := true;
          end if;

          -- bepaal de vroegste ingangsdat. Deze wordt default bank indien geen SB
          if (l_vroegste_date is null or
              trunc(l_vroegste_date) > trunc(v_fnr.datingang)) and not
             lb_def_bank and v_fnr.dateinde is null
          then
            -- gegadigde voor default bank binnen CODA
            l_vroegste_date   := trunc(v_fnr.datingang);
            l_vroegste_teller := l_teller;
          end if;


        end if;
      end if;
    end loop;
  end loop;

  if not lb_def_bank
  then
  -- Geen AG of SB, we moeten de default bank nog zetten.
  -- l_vroegste_teller is collection subscript, welke hierboven is bepaald.
    l_fnr_tab(l_vroegste_teller).bet_wijze := '1';
  end if;

  p_fnr_tab := l_fnr_tab;
end;
 
/