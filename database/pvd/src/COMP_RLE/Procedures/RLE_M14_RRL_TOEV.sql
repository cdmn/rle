CREATE OR REPLACE PROCEDURE comp_rle.RLE_M14_RRL_TOEV
 (P_NUMRELATIE IN number
 ,P_CODROL IN varchar2
 ,P_CODSYSTEEMCOMP IN varchar2
 ,P_CODERELATIE IN varchar2
 ,P_MELDING IN OUT varchar2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen rol */
CURSOR C_ROL_001
 (B_CODROL IN VARCHAR2
 )
 IS
/* C_ROL_001 */
select	*
from	rle_rollen rol
where	rol.codrol = b_codrol;
CURSOR C_RRL_002
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_RRL_002 */
select	*
from	rle_relatie_rollen rrl
where	rrl.rle_numrelatie   = b_numrelatie
and	rrl.rol_codrol = b_codrol;
-- Program Data
ONGELDIG_ROL EXCEPTION;
L_IND_FOUND VARCHAR2(1) := 'N';
R_RRL_002 C_RRL_002%ROWTYPE;
R_ROL_001 C_ROL_001%ROWTYPE;
ONGELDIG_SYSTEEMCOMP EXCEPTION;
L_MELDING VARCHAR2(20);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_M14_RRL_TOEV */
p_melding := NULL;
-- deel 1 : kontroleren of opgegeven systeemkomponent overeenkomt
--          met de komponent behorende bij de rol
OPEN	c_rol_001(p_codrol);
FETCH	c_rol_001 INTO r_rol_001;
IF	c_rol_001%notfound THEN
	CLOSE	c_rol_001;
                 stm_dbproc.raise_error('RLE-00502'
                       ,'ROL '||'#1'|| p_codrol||' onbekend'
                       ,'RLE_M24_RRL_TOEV');
ELSE
  CLOSE	c_rol_001;
 -- l_ind_found := 'N';
/*
  << RolSysAutorisatie >>
  FOR r_rae_001 IN c_rae_001(p_codrol) LOOP
    IF p_codsysteemcomp = r_rae_001.dmn_wrd THEN
      l_ind_found := 'J';
      EXIT RolSysAutorisatie;
    END IF;
  END LOOP RolSysAutorisatie;
  IF l_ind_found = 'N' THEN
    stm_dbproc.raise_error('RLE-00502'
                         ,'Systeemcomponent' || '#1'||' '|| p_codsysteemcomp
                         ||'#2'||' '||p_codrol
  	       ,'RLE_M14_RRL_TOEV');
  END IF;
*/
END IF;
-- deel 2 : toevoegen of wijzigen relatierol
OPEN c_rrl_002(p_numrelatie
             ,p_codrol);
FETCH c_rrl_002 INTO r_rrl_002;
IF c_rrl_002%notfound THEN
       CLOSE	c_rrl_002;
       INSERT INTO rle_relatie_rollen(rle_numrelatie
         ,rol_codrol
         ,codpresentatie
         ,codorganisatie
         ,datschoning)
        VALUES(p_numrelatie
        ,p_codrol
        ,NULL
        ,'A'
        ,NULL);
ELSE
	CLOSE	c_rrl_002;
END IF;
-- deel 3 eventueel van een occurence in rle_gebruik_relaties
/*
if p_coderelatie IS NOT NULL THEN
  rle_m01_gebruik_rle(p_numrelatie
                     ,p_codrol
                     ,p_codsysteemcomp
                     ,p_coderelatie
                     ,'N'
                     ,l_melding);
END IF;
*/
END RLE_M14_RRL_TOEV;

 
/