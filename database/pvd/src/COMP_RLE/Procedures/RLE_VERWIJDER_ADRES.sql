CREATE OR REPLACE PROCEDURE comp_rle.RLE_VERWIJDER_ADRES
 (P_RELATIENUMMER IN rle_relaties.numrelatie%TYPE
 ,P_SOORT_ADRES IN VARCHAR2
 ,P_LANDCODE IN VARCHAR2
 )
 IS

CURSOR c_ads (b_relatienummer in number
             ,b_soort_adres in varchar2
             ,b_landcode in varchar2) is
select ras.id
from rle_relatie_adressen ras
,    rle_adressen ads
where ras.ads_numadres = ads.numadres
and ras.rle_numrelatie = b_relatienummer
and ras.dwe_wrddom_srtadr = b_soort_adres
and ads.lnd_codland = b_landcode;
BEGIN
  FOR r_ads in c_ads (p_relatienummer
                     ,p_soort_adres
                     ,p_landcode)
  LOOP
    DELETE
    FROM  RLE_RELATIE_ADRESSEN RAS
    WHERE RAS.ID = r_ads.id;
  END LOOP;
END RLE_VERWIJDER_ADRES;
 
/