CREATE OR REPLACE PROCEDURE comp_rle.RLE_RKN_TOEV
 (P_RELATIENUMMER IN RLE_RELATIE_KOPPELINGEN.RLE_NUMRELATIE%TYPE
 ,P_RELATIENUMMER_VOOR IN RLE_RELATIE_KOPPELINGEN.RLE_NUMRELATIE_VOOR%TYPE
 ,P_CODE_ROL IN RLE_ROL_IN_KOPPELINGEN.CODE_ROL_IN_KOPPELING%TYPE
 ,P_BEGINDATUM IN DATE := SYSDATE
 ,P_IND_AFSLUITEN IN VARCHAR2 := 'N'
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_RKG_001
 (CPV_CODE_ROL_IN_KOPPELING VARCHAR2
 )
 IS
SELECT *
FROM   rle_rol_in_koppelingen  rkg
WHERE  rkg.code_rol_in_koppeling = cpv_code_rol_in_koppeling
;
CURSOR C_RKN_005
 (CPN_NUMRELATIE IN NUMBER
 ,CPN_NUMRELATIE_VOOR IN NUMBER
 ,CPD_BEGINDATUM IN DATE
 ,CPV_ROL_IN_KOPPELING IN VARCHAR2
 )
 IS
SELECT /* Lijst kan "nog" vrij worden uitgebreid */
       rkn.id
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkn.rkg_id = rkg.id
AND    rkn.rle_numrelatie        = cpn_numrelatie
AND    rkn.rle_numrelatie_voor   = cpn_numrelatie_voor
AND    TRUNC( rkn.dat_begin )    = TRUNC( cpd_begindatum )
AND    rkg.code_rol_in_koppeling = cpv_rol_in_koppeling
;
/* Ophalen alle actieve relatie koppelingen */
CURSOR C_RKN_013
 (B_NUMRELATIE IN RLE_RELATIE_KOPPELINGEN.RLE_NUMRELATIE%TYPE
 ,B_NUMRELATIE_VOOR IN RLE_RELATIE_KOPPELINGEN.RLE_NUMRELATIE_VOOR%TYPE
 ,B_PEILDATUM IN DATE
 ,B_ROL_IN_KOPPELING IN RLE_ROL_IN_KOPPELINGEN.CODE_ROL_IN_KOPPELING%TYPE
 )
 IS
SELECT rkn.id, rkn.dat_einde
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkg.code_rol_in_koppeling = b_rol_in_koppeling
AND    rkn.rkg_id = rkg.id
AND    rkn.rle_numrelatie        = b_numrelatie
AND    rkn.rle_numrelatie_voor   = b_numrelatie_voor
AND    rkn.dat_begin             < b_peildatum
AND    (rkn.dat_einde IS NULL OR
        rkn.dat_einde > b_peildatum)
ORDER BY rkn.dat_begin ASC
FOR UPDATE OF rkn.dat_einde
;
/* Ophalen alle actieve relatie koppelingen bij een 'Relatienummer voor' */
CURSOR C_RKN_014
 (B_NUMRELATIE IN RLE_RELATIE_KOPPELINGEN.RLE_NUMRELATIE%TYPE
 ,B_PEILDATUM IN DATE
 ,B_ROL_IN_KOPPELING IN RLE_ROL_IN_KOPPELINGEN.CODE_ROL_IN_KOPPELING%TYPE
 )
 IS
SELECT rkn.id, rkn.dat_einde
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkg.code_rol_in_koppeling = b_rol_in_koppeling
AND    rkn.rkg_id = rkg.id
AND    rkn.rle_numrelatie = b_numrelatie
AND    rkn.dat_begin < b_peildatum
AND    (rkn.dat_einde IS NULL OR
        rkn.dat_einde > b_peildatum)
ORDER BY rkn.dat_begin ASC
FOR UPDATE OF rkn.dat_einde
;
-- Program Data
L_FOUND BOOLEAN := FALSE;
L_RKG_ID RLE_ROL_IN_KOPPELINGEN.ID%TYPE;
L_BEGINDATUM DATE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
l_begindatum := TRUNC (p_begindatum);
--
-- Controle code rol in koppeling
--
FOR r_rkg_001 IN c_rkg_001 (p_code_rol)
LOOP
  l_rkg_id := r_rkg_001.id;
  l_found := TRUE;
END LOOP;
IF NOT l_found
THEN
  RAISE no_data_found;
END IF;
--
-- Bestaat de koppeling reeds?
--
l_found := FALSE;
FOR r_rkn_005 IN c_rkn_005 ( p_relatienummer
                           , p_relatienummer_voor
                           , l_begindatum
                           , p_code_rol)
LOOP
  l_found := TRUE;
END LOOP;
--
-- Niet gevonden, aanmaken nieuw voorkomen.
--
IF NOT l_found
THEN
  --
  -- Afsluiten vorig voorkomens ...
  --
  FOR r_rkn_013 IN c_rkn_013 ( p_relatienummer
                             , p_relatienummer_voor
                             , l_begindatum
                             , p_code_rol)
  LOOP
    UPDATE rle_relatie_koppelingen
    SET dat_einde = l_begindatum - 1
    WHERE CURRENT OF c_rkn_013
    ;
  END LOOP;
  --
  IF p_ind_afsluiten = 'J'
  THEN
    FOR r_rkn_014 IN c_rkn_014 ( p_relatienummer
                               , l_begindatum
                               , p_code_rol)
    LOOP
      UPDATE rle_relatie_koppelingen
      SET dat_einde = l_begindatum - 1
      WHERE CURRENT OF c_rkn_014
      ;
    END LOOP;
  END IF;
  --
  -- Aanmaken nieuw voorkomen
  --
  INSERT INTO rle_relatie_koppelingen
  ( ID
  , RKG_ID
  , RLE_NUMRELATIE
  , RLE_NUMRELATIE_VOOR
  , DAT_BEGIN
  ) VALUES (
    rle_rkn_seq1.NEXTVAL
  , l_rkg_id
  , p_relatienummer
  , p_relatienummer_voor
  , l_begindatum
  );
END IF;
END RLE_RKN_TOEV;

 
/