CREATE OR REPLACE PROCEDURE comp_rle.RLE_REC_CHK_NR_RBD
 (P_NR_RBD IN VARCHAR2
 )
 IS
BEGIN
if instr( p_nr_rbd, 'L') <> nvl( length( p_nr_rbd), 0) - 2  OR
   nvl( length( translate( p_nr_rbd, 'L0123456789', 'L')), 0) <> 1
then
   stm_dbproc.raise_error('RLE-00543');
end if;
END RLE_REC_CHK_NR_RBD;

 
/