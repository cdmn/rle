CREATE OR REPLACE PROCEDURE comp_rle.RLE_M15_ADS_VERWERK
 (P_CODLAND IN OUT varchar2
 ,P_POSTCOD IN OUT varchar2
 ,P_HUISNUM IN OUT number
 ,P_TOEVNUM IN OUT varchar2
 ,P_WOONPL IN OUT varchar2
 ,P_STRAAT IN OUT varchar2
 ,P_O_NUMADRES IN OUT number
 )
 IS
-- Sub-Program Unit Declarations
/* Bepalen adres o.b.v. landcode en postcode */
CURSOR C_ADS_003
 (B_CODLAND IN VARCHAR2
 ,B_POSTCOD IN VARCHAR2
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_003 */
select 	ads.numadres
from 	rle_adressen ads
where 	ads.lnd_codland = b_codland
and    (upper(ads.toevhuisnum) = upper(b_toevnum)
        or (ads.toevhuisnum is null
            and b_toevnum is null))
and    (ads.huisnummer = b_huisnum
         or( b_huisnum is null)
             and ads.huisnummer is null)
and     ads.postcode = b_postcod;
--
/* Bepalen adres-ID o.b.v. land, woonplaats en straat */
CURSOR C_ADS_004
 (B_CODLAND IN VARCHAR2
 ,B_WPS_ID IN NUMBER
 ,B_STT_ID IN NUMBER
 ,B_POSTCOD IN VARCHAR2
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_004 */
SELECT	ads.numadres
FROM	rle_adressen ads
WHERE	ads.lnd_codland = b_codland
AND     (UPPER( ads.toevhuisnum ) = UPPER( b_toevnum )
         or (ads.toevhuisnum IS NULL
             AND b_toevnum IS NULL)
         )
AND     (ads.huisnummer = b_huisnum
         or (ads.huisnummer IS NULL
             AND b_huisnum IS NULL)
         )
AND     ( ads.postcode  = UPPER( b_postcod )
          or (ads.postcode IS NULL
              AND b_postcod IS NULL)
         )
AND	    ads.stt_straatid = b_stt_id
AND         ads.wps_woonplaatsid = b_wps_id
;
-- Program Data
L_STT_ID NUMBER(7, 0);
L_IND_NWADRS VARCHAR2(1);
L_CODNL VARCHAR2(20);
L_NUMADRS NUMBER(7, 0);
L_WOONPL VARCHAR2(40) := null;
L_STRAAT VARCHAR2(40) := null;
L_WPS_ID NUMBER(5, 0);
L_LOKATIE VARCHAR2(6);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
stm_util.debug('Start RLE_M15_ADS_VERWERK');
/* RLE_M15_ADS_VERWERK */
-- bepaal landcode nederland
l_lokatie := '000010';
 stm_util.debug('lokatie : '||l_lokatie);
l_codnl := stm_algm_get.sysparm(NULL,'LAND',sysdate);
-- Als landcode Nederland en postcode aanwezig, zoek dan
-- het numadres op via de c_ads_003
if  p_codland = l_codnl
and p_postcod is not null
then
  l_lokatie := '000020';
   stm_util.debug('lokatie : '||l_lokatie);
  open c_ads_003(p_codland
	,	 p_postcod
	,	 p_huisnum
	,	 p_toevnum);
  fetch c_ads_003 into l_numadrs;
  if c_ads_003%found
  then
    -- Verwerking klaar, numadres gevonden
	l_lokatie := '000030';
	 stm_util.debug('lokatie : '||l_lokatie);
    close c_ads_003;
    p_o_numadres := l_numadrs;
  else
    -- numadres is nog niet aanwezig
	l_lokatie := '000040';
	 stm_util.debug('lokatie : '||l_lokatie);
    close c_ads_003;
    -- via postcode + huisnr straat en woonplaats ophalen
	l_lokatie := '000050';
	 stm_util.debug('lokatie : '||l_lokatie);
    rle_get_wps_stt( p_postcod
                   , p_huisnum
                   , l_woonpl
                   , l_straat
                   );
    l_lokatie := '000060';
	 stm_util.debug('lokatie : '||l_lokatie);
    rle_wps_bepaal_stt( l_stt_id
                      , l_wps_id
                      , l_ind_nwadrs
                      , p_codland
                      , l_straat
                      , l_woonpl
                      );
    l_lokatie := '000070';
	 stm_util.debug('lokatie : '||l_lokatie);
    rle_ads_nieuw( l_stt_id
                 , l_wps_id
                 , p_codland
                 , l_straat
                 , l_woonpl
                 , p_o_numadres
                 , p_postcod
                 , p_huisnum
                 , p_toevnum
                 );
  end if;
else
  l_lokatie := '000080';
   stm_util.debug('lokatie : '||l_lokatie);
  -- Verwerking van buitenlands adres
  if p_codland <> l_codnl
  then
    l_woonpl := upper(p_woonpl);
    l_straat := initcap(p_straat);
  else
    l_woonpl := p_woonpl;
    l_straat := p_straat;
  end if;
  l_lokatie := '000090';
   stm_util.debug('lokatie : '||l_lokatie);
  rle_wps_bepaal_stt( l_stt_id
                    , l_wps_id
                    , l_ind_nwadrs
                    , p_codland
                    , l_straat
                    , l_woonpl
                    );
  if l_ind_nwadrs = 'N'
  then
    l_lokatie := '000100';
	 stm_util.debug('lokatie : '||l_lokatie);
    open c_ads_004( p_codland
                  , l_wps_id
                  , l_stt_id
                  , p_postcod
                  , p_huisnum
                  , p_toevnum
                  );
    fetch c_ads_004 into l_numadrs;
    if c_ads_004%found
    then
      -- numadres voor buitenlands adres gevonden. Verwerking klaar
      close c_ads_004;
      p_o_numadres := l_numadrs;
    else
      -- Nieuw buitenlands adres toevoegen
      close c_ads_004;
	  l_lokatie := '000110';
	   stm_util.debug('lokatie : '||l_lokatie);
      rle_ads_nieuw( l_stt_id
                   , l_wps_id
                   , p_codland
                   , l_straat
                   , l_woonpl
                   , p_o_numadres
                   , p_postcod
                   , p_huisnum
                   , p_toevnum
                   );
    end if;
  else
    -- Nieuw buitenlands adres toevoegen
	l_lokatie := '000120';
	 stm_util.debug('lokatie : '||l_lokatie);
    rle_ads_nieuw( l_stt_id
                 , l_wps_id
                 , p_codland
                 , l_straat
                 , l_woonpl
                 , p_o_numadres
                 , p_postcod
                 , p_huisnum
                 , p_toevnum
                 );
  end if;
  -- als alleen postcode en huisnummer meegegeven
  -- dan vullen retourwaarde woonplaats en straatnaam
  if p_woonpl is null
  then
    p_woonpl := l_woonpl;
  end if;
  if p_straat is null
  then
    p_straat := l_straat;
  end if;
end if;
EXCEPTION
WHEN OTHERS THEN
	 stm_dbproc.raise_error('rle_m15_ads_verwerk : '||NVL(l_lokatie,'leeg')||' '||SQLERRM);
END RLE_M15_ADS_VERWERK;
 
/