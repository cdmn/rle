CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_RKG
 (P_RKN_ID IN RLE_RELATIE_KOPPELINGEN.ID%TYPE
 ,P_CODE_ROL_IN_KOPPELING OUT RLE_ROL_IN_KOPPELINGEN.CODE_ROL_IN_KOPPELING%TYPE
 ,P_OMSCHRIJVING OUT RLE_ROL_IN_KOPPELINGEN.OMSCHRIJVING%TYPE
 ,P_ROL_CODROL_BEPERKT_TOT OUT RLE_ROL_IN_KOPPELINGEN.ROL_CODROL_MET%TYPE
 ,P_ROL_CODROL_MET OUT RLE_ROL_IN_KOPPELINGEN.ROL_CODROL_MET%TYPE
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_RKG_002
 (B_RKN_ID RLE_RELATIE_KOPPELINGEN.ID%TYPE
 )
 IS
SELECT	rkg.code_rol_in_koppeling
,      	rkg.omschrijving
,      	rkg.rol_codrol_beperkt_tot
,      	rkg.rol_codrol_met
FROM	rle_relatie_koppelingen rkn
,    	rle_rol_in_koppelingen rkg
WHERE	rkn.rkg_id 	= rkg.id
AND	 rkn.id 		= b_rkn_id
;
CURSOR C_RKG_002_JN
 (B_RKN_ID RLE_RELATIE_KOPPELINGEN.ID%TYPE
 )
 IS
SELECT	rkg2.code_rol_in_koppeling
,      	rkg2.omschrijving
,      	rkg2.rol_codrol_beperkt_tot
,      	rkg2.rol_codrol_met
FROM	rle_relatie_koppelingen_jn rkn2
,    	rle_rol_in_koppelingen rkg2
WHERE	rkn2.rkg_id 	= rkg2.id
AND	 rkn2.id 		= b_rkn_id
;
-- Program Data
R_RKG_002_JN  C_RKG_002_JN%ROWTYPE;
R_RKG_002     C_RKG_002%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
OPEN  c_rkg_002( p_rkn_id ) ;
   FETCH c_rkg_002 INTO r_rkg_002 ;
   IF c_rkg_002%FOUND
   THEN
       p_code_rol_in_koppeling  := r_rkg_002.code_rol_in_koppeling;
       p_omschrijving           := r_rkg_002.omschrijving;
       p_rol_codrol_beperkt_tot := r_rkg_002.rol_codrol_beperkt_tot;
       p_rol_codrol_met         := r_rkg_002.rol_codrol_met;
   ELSE
      OPEN  c_rkg_002_jn( p_rkn_id ) ;
      FETCH c_rkg_002_jn INTO r_rkg_002_jn ;
      IF c_rkg_002_jn%FOUND
      THEN
         p_code_rol_in_koppeling  := r_rkg_002_jn.code_rol_in_koppeling;
         p_omschrijving           := r_rkg_002_jn.omschrijving;
         p_rol_codrol_beperkt_tot := r_rkg_002_jn.rol_codrol_beperkt_tot;
         p_rol_codrol_met         := r_rkg_002_jn.rol_codrol_met;
      END IF;
      CLOSE c_rkg_002_jn;
   END IF ;
   CLOSE c_rkg_002 ;
EXCEPTION
   WHEN OTHERS
   THEN
      p_code_rol_in_koppeling	:= NULL;
      p_omschrijving		:= NULL;
      p_rol_codrol_beperkt_tot	:= NULL;
      p_rol_codrol_met		:= NULL;
      /* Sluiten cursor */
      IF c_rkg_002%ISOPEN
      THEN
         CLOSE c_rkg_002 ;
      END IF ;
      IF c_rkg_002_jn%ISOPEN
      THEN
         CLOSE c_rkg_002_jn ;
      END IF ;
END RLE_GET_RKG;

 
/