CREATE OR REPLACE PROCEDURE comp_rle.RLE_M31_NPE_MUTATIE
 (P_AKTIE VARCHAR2
 ,P_POSTCODEOUD VARCHAR2
 ,P_REEKSOUD NUMBER
 ,P_SCHEIDINGVANOUD NUMBER
 ,P_SCHEIDINGTMOUD NUMBER
 ,P_POSTCODE VARCHAR2
 ,P_CODREEKS NUMBER
 ,P_SCHEIDINGVAN NUMBER
 ,P_SCHEIDINGTM NUMBER
 ,P_WOONPLAATS VARCHAR2
 ,P_STRAATNAAM VARCHAR2
 ,P_WPS_OUD VARCHAR2
 ,P_STT_OUD VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Bepalen adres o.b.v. landcode en postcode */
CURSOR C_ADS_003
 (B_CODLAND IN VARCHAR2
 ,B_POSTCOD IN VARCHAR2
 ,B_HUISNUM IN NUMBER
 ,B_TOEVNUM IN VARCHAR2
 )
 IS
/* C_ADS_003 */
select 	ads.numadres
from 	rle_adressen ads
where 	ads.lnd_codland = b_codland
and   	(ads.postcode = b_postcod
		and	((ads.huisnummer is null
	        and b_huisnum is null)
	    or	ads.huisnummer = b_huisnum)
and     (( ads.toevhuisnum is null
		and b_toevnum is null)
	    or 	ads.toevhuisnum = b_toevnum));
/* Ophalen van adressen bij een postcode en huisnummer range */
CURSOR C_ADS_008
 (B_POSTCODE VARCHAR2
 ,B_NUMVAN NUMBER
 ,B_NUMTOT NUMBER
 )
 IS
/*C_ADS_008*/
select * from rle_adressen ads
where ads.postcode = b_postcode
and   ads.huisnummer >= b_numvan
and   ads.huisnummer <= b_numtot;
/* Bepalen adres-ID o.b.v. land, woonplaats en straa (c_adrs04) */
CURSOR C_ADS_009
 (B_CODLAND IN VARCHAR2
 ,B_WPS_ID IN NUMBER
 ,B_STT_ID IN NUMBER
 ,B_HUISNUMVAN IN NUMBER
 ,B_HUISNUMTM NUMBER
 )
 IS
/* C_ADS_009 */
select *
from	rle_adressen ads
where	ads.lnd_codland  = b_codland
and	ads.wps_woonplaatsid = b_wps_id
and	ads.stt_straatid = b_stt_id
and	ads.huisnummer <= b_huisnumtm
and ads.huisnummer >= b_huisnumvan;
/* c_adrs04 */
CURSOR C_ADS_010
 (B_CODLAND IN VARCHAR2
 ,B_POSTCOD IN VARCHAR2
 ,B_HUISNUMVAN IN NUMBER
 ,B_HUISNUMTOT IN NUMBER
 )
 IS
/* C_ADS_010 */
select	*
from	rle_adressen ads
where	ads.lnd_codland = b_codland
and	    ads.postcode     = b_postcod
and	     ads.huisnummer >=  b_huisnumvan
and	     ads.huisnummer <=  b_huisnumtot;
/* Ophalen van adressen bij een straat */
CURSOR C_ADS_015
 (B_STRAATID IN NUMBER
 )
 IS
/*C_ADS_015*/
select ads.*, rowid from rle_adressen ads
where ads.stt_straatid = b_straatid
;
/* Relatie adressen bij een numadres */
CURSOR C_RAS_018
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_RAS_018 */
select *
from	rle_relatie_adressen ras
where	ras.ads_numadres = b_numadres
;
/* c_stra02 */
CURSOR C_STT_002
 (B_WPS_ID IN NUMBER
 ,B_STRAAT IN VARCHAR2
 )
 IS
/* C_STT_002 */
SELECT	*
FROM	rle_straten stt
WHERE	stt.wps_woonplaatsid = b_wps_id
AND	UPPER( stt.straatnaam ) = UPPER( b_straat ) ;
/* Ophalen van woonplaatsen met woonplaatsnaam */
CURSOR C_WPS_004
 (B_CODLAND VARCHAR2
 ,B_WOONPLAATS VARCHAR2
 )
 IS
/* C_WPS_004 */
select * from rle_woonplaatsen wps
where  wps.lnd_codland = b_codland
  and  wps.woonplaats   = b_woonplaats;
/* c_wnpl02 */
CURSOR C_WPS_008
 (B_CODLAND IN VARCHAR2
 ,B_WOONPL IN VARCHAR2
 )
 IS
/* C_WPS_008 */
select	wps.woonplaatsid
,			rowid
from	rle_woonplaatsen wps
where	wps.lnd_codland = b_codland
and	wps.woonplaats = b_woonpl
;
-- Program Data
L_INDWOONPLAATS_NWE VARCHAR2(1);
L_INDSTRAAT_NWE VARCHAR2(1);
L_REEKSEVEN VARCHAR2(1);
L_INDHUISNUMEVEN VARCHAR2(1);
R_ADS_015 C_ADS_015%ROWTYPE;
L_REMIND NUMBER;
L_MELDING VARCHAR2(132);
L_WOONPLAATSID NUMBER;
L_STRAATID NUMBER;
R_WPS_004 C_WPS_004%ROWTYPE;
R_RAS_018 C_RAS_018%ROWTYPE;
R_STT_002 C_STT_002%ROWTYPE;
R_WPS_008 C_WPS_008%ROWTYPE;
L_NUMADRES NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
-- RLE_M31_NPE_MUTATIE
--
DECLARE
PROCEDURE CTRL_WPS_STT
( p_wps VARCHAR2
, p_stt VARCHAR2
, p_ind_toevoeg VARCHAR2
)
IS
BEGIN
  --
  -- Controle woonplaats al aanwezig?
  --
  l_woonplaatsid := NULL;
  l_straatid := NULL;
--
  l_indwoonplaats_nwe := 'N';
  OPEN c_wps_004( 'NL', p_wps);
  FETCH c_wps_004 INTO r_wps_004 ;
  IF c_wps_004%found THEN
     l_woonplaatsid := r_wps_004.woonplaatsid;
     CLOSE c_wps_004;
  ELSE
     CLOSE c_wps_004;
--
     IF p_ind_toevoeg = 'J'
     THEN
        INSERT
        INTO   rle_woonplaatsen
               ( lnd_codland
               , woonplaats)
        VALUES ( 'NL'
               , p_wps);
        l_indwoonplaats_nwe := 'J';
        --
        -- Na insert opnieuw de woonpaats ophalen voor id
        --
        OPEN c_wps_004( 'NL', p_wps);
        FETCH c_wps_004 INTO r_wps_004;
        l_woonplaatsid := r_wps_004.woonplaatsid;
        CLOSE c_wps_004;
     END IF;
  END IF;
  --
  -- Controle straatnaam al aanwezig?
  --
  l_indstraat_nwe := 'N';
  OPEN c_stt_002( l_woonplaatsid, p_stt);
  FETCH c_stt_002 INTO r_stt_002;
  IF c_stt_002%found
  THEN
     l_straatid := r_stt_002.straatid;
     CLOSE c_stt_002;
  ELSE
     CLOSE c_stt_002;
     IF p_ind_toevoeg = 'J'
     THEN
        INSERT
        INTO   rle_straten
               ( wps_woonplaatsid
               , straatnaam)
        VALUES ( l_woonplaatsid
               , p_stt);
        l_indstraat_nwe := 'J';
        --
        -- Na insert opnieuw de straat ophalen voor id
        --
        OPEN c_stt_002( l_woonplaatsid, p_stt);
        FETCH c_stt_002 INTO r_stt_002 ;
        l_straatid := r_stt_002.straatid;
        CLOSE c_stt_002;
     END IF;
  END IF;
END;
--
--
-- ******** HOOFDROUTINE **********
--
BEGIN
-- Test indien reeks van toepassing is
l_reekseven := NULL;
IF NVL( p_reeksoud, p_codreeks) = 0
THEN
   l_reekseven := 'N';
ELSIF NVL( p_reeksoud, p_codreeks) = 1
THEN
   l_reekseven := 'J';
END IF;
--
-- Postcode range wordt verwijderd
--
IF p_aktie = 'D'
THEN
   --
   -- Opschonen van alle adressen
   --
   FOR r_ads_008 IN c_ads_008( p_postcodeoud
                             , p_scheidingvanoud
                             , p_scheidingtmoud
                             )
   LOOP
     OPEN c_ras_018( r_ads_008.numadres);
     FETCH c_ras_018 INTO r_ras_018;
     IF c_ras_018%found
     THEN
        l_melding := 'RLE_M31_NPE_MUTATIE; '
                  || 'verwijderde postcode ('
                  || p_postcodeoud || '/'
                  || p_scheidingvanoud || '/'
                  || p_scheidingtmoud
                  || ') in gebruik bij relatienr: '
                  || TO_CHAR( r_ras_018.rle_numrelatie);
        stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                           , 'WAARSCHUW'
                           , l_melding);
     END IF;
     CLOSE c_ras_018;
     IF l_reekseven IS NOT NULL
     THEN
        l_remind := MOD( r_ads_008.huisnummer, 2);
        IF l_remind > 0
        THEN
           l_indhuisnumeven := 'N';
        ELSE
           l_indhuisnumeven := 'J';
        END IF;
        --
        -- Alleen updaten indien de reeksen matchen
        --
        IF l_indhuisnumeven = l_reekseven
        THEN
           UPDATE rle_adressen ads
           SET    ads.postcode = NULL
           WHERE  ads.numadres = r_ads_008.numadres;
        END IF;
     END IF;
   END LOOP;
--
ELSIF p_aktie = 'I'
THEN
   --
   -- Postcode wordt toegevoegd
   --
   Ctrl_wps_stt( p_woonplaats, p_straatnaam, 'J');
   --
   -- Indien woonplaats en adres al aanwezig, kan het zijn dat
   -- er al adressen zijn zonder de postcode. Deze worden bijgewerkt
   --
   IF l_indstraat_nwe = 'N'     AND
      l_indwoonplaats_nwe = 'N'
   THEN
      FOR r_ads_009 IN c_ads_009( 'NL'
                                , l_woonplaatsid
                                , l_straatid
                                , p_scheidingvan
                                , p_scheidingtm
                                )
      LOOP
        IF l_reekseven IS NOT NULL
        THEN
           l_remind := MOD( r_ads_009.huisnummer, 2);
           IF l_remind > 0
           THEN
              l_indhuisnumeven := 'N';
           ELSE
              l_indhuisnumeven := 'J';
           END IF;
           --
           -- Alleen updaten indien de reeksen matchen
           --
           IF l_indhuisnumeven = l_reekseven
           THEN
              --
              -- Voor het vullen van de postcode eerst controleren of dit adres niet
              -- al bestaat MET postcode ivm BR ENT0001
              --
              l_numadres := NULL;
              --
              OPEN C_ADS_003(r_ads_009.lnd_codland
                            ,p_postcode
                            ,r_ads_009.huisnummer
                            ,r_ads_009.toevhuisnum
                            );
              FETCH C_ADS_003 INTO l_numadres;
              CLOSE C_ADS_003;
              --
              IF l_numadres IS NULL
              THEN
                 -- Niet gevonden: Dus probleemloos postcode vullen.
                 UPDATE rle_adressen ads
                 SET    ads.postcode = p_postcode
                 WHERE  ads.numadres = r_ads_009.numadres;
              ELSE
                 -- Wel gevonden: Verwijder te updaten record na de relaties naar dit record
                 --               te hebben omgelegd naar het gevonden record (alleen als
                 --               gevonden record ongelijk is aan het over te houden record!)
                 IF l_numadres <> r_ads_009.numadres
                 THEN
                    UPDATE rle_relatie_adressen ras
                    SET    ras.ads_numadres = l_numadres
                   WHERE  ras.ads_numadres = r_ads_009.numadres;
                   --
                   DELETE rle_adressen ads
                   WHERE  ads.numadres = r_ads_009.numadres;
                   --
                 END IF;
              END IF;
           END IF;
        END IF;
      END LOOP;
   END IF;
--
ELSIF p_aktie = 'M'
THEN
--
-- Postcode range wordt gewijzigd.
--
-- Nieuwe woonplaats en straat zijn nu aanwezig.
-- Als oude verschilt van nieuwe, en er is geen postcode reeks
-- gegeven, dan id's van de oude ophalen en wijzigen met de nieuwe.
-- Als wel postcode reeks gegeven, dan alleen adressen binnen die
-- reeks naar nieuwe postcode en/of woonplaats en/of straat.
--
   IF RTRIM( LTRIM( SUBSTR( p_postcodeoud, 5, 2))) IS NULL
   THEN
      OPEN c_wps_008( 'NL', p_wps_oud);
      FETCH c_wps_008 INTO r_wps_008;
      IF c_wps_008%found
      THEN
         IF p_wps_oud <> p_woonplaats
         THEN
            --
            -- Woonplaats wijziging
            --
            rle_wps_wijzig( 'NL'
                          , p_woonplaats
                          , r_wps_008.woonplaatsid
                          , r_wps_008.rowid
                          );
--
            CLOSE c_wps_008;
            --
            -- Nieuwe woonplaatsid ophalen
            --
            OPEN c_wps_008( 'NL', p_woonplaats);
            FETCH c_wps_008 INTO r_wps_008;
         END IF;
--
         IF p_stt_oud <> p_straatnaam
         THEN
            --
            -- Straatnaam wijziging, zoeken met nieuwe wps_id
            -- aangezien rle_wps_wijzig ook de straten omhangt.
            --
            OPEN c_stt_002( r_wps_008.woonplaatsid, p_stt_oud);
            FETCH c_stt_002 INTO r_stt_002;
            IF c_stt_002%found
            THEN
               rle_stt_wijzig( r_wps_008.woonplaatsid
                             , p_straatnaam
                             , r_stt_002.straatid
                             );
            END IF;
            CLOSE c_stt_002;
         END IF;
      END IF;
      CLOSE c_wps_008;
   ELSE
      --
      -- Ophalen van adressen met de oude postcode
      --
      Ctrl_wps_stt( p_woonplaats, p_straatnaam, 'J');
      FOR r_ads_010 IN c_ads_010( 'NL'
                                , p_postcodeoud
                                , p_scheidingvanoud
                                , p_scheidingtmoud
                                )
      LOOP
        IF l_reekseven IS NOT NULL
        THEN
           l_remind := MOD( r_ads_010.huisnummer, 2);
           IF l_remind > 0
           THEN
              l_indhuisnumeven := 'N';
              ELSE
              l_indhuisnumeven := 'J';
           END IF;
           --
           -- Alleen updaten indien de reeksen matchen
           --
           IF l_indhuisnumeven = l_reekseven
           THEN
              UPDATE rle_adressen
              SET    postcode = p_postcode
                   , stt_straatid = l_straatid
                   , wps_woonplaatsid = l_woonplaatsid
              WHERE  numadres = r_ads_010.numadres;
              --
              -- Als het huisnummer van het adres niet meer voorkomt in de
              -- nieuwe, door de PTT, aangeleverde reeks dit loggen.
              -- Adres en Relatieadressen moeten bezien worden.
              --
              IF r_ads_010.huisnummer < p_scheidingvan  OR
                 r_ads_010.huisnummer > p_scheidingtm
              THEN
                 l_melding := 'RLE_M31_NPE_MUTATIE; '
                           || 'postcodereeks(' || p_postcodeoud || '/'
                           || p_scheidingvanoud || '/' || p_scheidingtmoud
                           || ')wijzigt naar(' || p_postcode || '/'
                           || p_scheidingvan || '/' || p_scheidingtm
                           || '). Adres ' || r_ads_010.postcode || '/'
                           || TO_CHAR(r_ads_010.huisnummer) || 'aanwezig';
                 stm_batch.log_regel( 'RLE_NPE_PCK.MUTATIE'
                                    , 'WAARSCHUW'
                                    , l_melding);
              END IF;
           END IF;
        END IF;
      END LOOP;
   END IF;
END IF;
END;
END RLE_M31_NPE_MUTATIE;

 
/