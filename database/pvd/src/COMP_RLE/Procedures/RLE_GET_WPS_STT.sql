CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_WPS_STT
 (P_PCODE IN VARCHAR2
 ,P_HUISNR IN NUMBER
 ,P_WOONPLAATS OUT VARCHAR2
 ,P_STRAAT OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen straat en wnpl van postcode */
CURSOR C_NPE_001
 (B_PCODE IN VARCHAR2
 ,B_HUISNR IN NUMBER
 )
 IS
/*	C_PCOD01 */
/* 	codreeks 0 is oneven huisnr¿s
	codreeks 1 is even huisnr¿s
	codreeks 2 en 3 zijn woonboten/wagens geen huisnr contrle.
*/
SELECT npe.woonplaats
,      npe.straatnaam
FROM	rle_ned_postcode npe
WHERE	npe.postcode = b_pcode
AND	(	(	npe.numscheidingvan <= b_huisnr
		AND	npe.numscheidingtm >= b_huisnr
		AND	npe.codreeks <> MOD(b_huisnr,2)
		)
	OR	(	npe.numscheidingvan <= 0
		AND	npe.numscheidingtm >= 0
		AND	npe.codreeks IN (2,3)
		AND	NOT EXISTS	(	SELECT	1
						FROM	rle_ned_postcode	npe2
						WHERE	npe2.postcode = npe.postcode
						AND	npe2.numscheidingvan <= b_huisnr
						AND	npe2.numscheidingtm >= b_huisnr
						AND	npe2.codreeks <> MOD(b_huisnr,2)
					)
		)
	)
;
-- Program Data
R_NPE_001 C_NPE_001%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default uitvoer */
   p_woonplaats := NULL ;
   p_straat     := NULL ;
   /* Opzoeken psotcode en huisnr */
   OPEN  c_npe_001( p_pcode, p_huisnr ) ;
   FETCH c_npe_001 INTO r_npe_001 ;
   IF c_npe_001%FOUND
   THEN
	p_woonplaats := r_npe_001.woonplaats ;
	p_straat     := r_npe_001.straatnaam ;
   END IF ;
   CLOSE c_npe_001 ;
EXCEPTION
   WHEN OTHERS THEN
      /* Uitvoer */
      p_woonplaats := NULL ;
      p_straat     := NULL ;
      /* Sluiten cursor */
      IF c_npe_001%ISOPEN
      THEN
         CLOSE c_npe_001 ;
      END IF ;
END RLE_GET_WPS_STT;

 
/