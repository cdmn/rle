CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_RKG_OMGEKEERD
 (PIV_CODE_ROL_IN_KOPPELING IN VARCHAR2
 ,POV_CODE_ROL_IN_KOPPELING OUT VARCHAR2
 )
 IS
/* ---------------------------------------------------------------------------------------------
  ** Functie: Bepaal "omgekeerde" code_rol_in_koppeling voor relatiekoppelingen
  **
  ** ---------------------------------------------------------------------------------------------
  ** Wijzigingen
  ** Datum       Wie  Wijzigingen
  ** ==========  ===  ============================================================================
  ** 04-08-2015  JCI  chg 374612, OVGN en AFKV toegevoegd
  ** ---------------------------------------------------------------------------------------------
  */
BEGIN
   /*** GERICHTE KOPPELINGEN ***/
   IF    piv_code_rol_in_koppeling = 'K'    THEN pov_code_rol_in_koppeling := 'O'    ;
   ELSIF piv_code_rol_in_koppeling = 'O'    THEN pov_code_rol_in_koppeling := 'K'    ;

   ELSIF piv_code_rol_in_koppeling = 'HOLD' THEN pov_code_rol_in_koppeling := 'WERK' ;
   ELSIF piv_code_rol_in_koppeling = 'WERK' THEN pov_code_rol_in_koppeling := 'HOLD' ;

   ELSIF piv_code_rol_in_koppeling = 'ROPV' THEN pov_code_rol_in_koppeling := 'RVGR' ;
   ELSIF piv_code_rol_in_koppeling = 'RVGR' THEN pov_code_rol_in_koppeling := 'ROPV' ;

   ELSIF piv_code_rol_in_koppeling = 'FIL'  THEN pov_code_rol_in_koppeling := 'HFDV' ;
   ELSIF piv_code_rol_in_koppeling = 'HFDV' THEN pov_code_rol_in_koppeling := 'FIL'  ;

   ELSIF piv_code_rol_in_koppeling = 'AFSP' THEN pov_code_rol_in_koppeling := 'AFSN' ;
   ELSIF piv_code_rol_in_koppeling = 'AFSN' THEN pov_code_rol_in_koppeling := 'AFSP' ;

   ELSIF piv_code_rol_in_koppeling = 'SAMV' THEN pov_code_rol_in_koppeling := 'FUST' ;
   ELSIF piv_code_rol_in_koppeling = 'FUST' THEN pov_code_rol_in_koppeling := 'SAMV' ;

   ELSIF piv_code_rol_in_koppeling = 'OVND' THEN pov_code_rol_in_koppeling := 'OVNV' ;
   ELSIF piv_code_rol_in_koppeling = 'OVNV' THEN pov_code_rol_in_koppeling := 'OVND' ;

   ELSIF piv_code_rol_in_koppeling = 'BW'   THEN pov_code_rol_in_koppeling := 'WB'   ;
   ELSIF piv_code_rol_in_koppeling = 'WB'   THEN pov_code_rol_in_koppeling := 'BW'   ;

   ELSIF piv_code_rol_in_koppeling = 'VW'   THEN pov_code_rol_in_koppeling := 'WV'   ;
   ELSIF piv_code_rol_in_koppeling = 'WV'   THEN pov_code_rol_in_koppeling := 'VW'   ;

   ELSIF piv_code_rol_in_koppeling = 'AV'   THEN pov_code_rol_in_koppeling := 'VA'   ;
   ELSIF piv_code_rol_in_koppeling = 'VA'   THEN pov_code_rol_in_koppeling := 'AV'   ;

   ELSIF piv_code_rol_in_koppeling = 'FW'   THEN pov_code_rol_in_koppeling := 'WV'   ;
   ELSIF piv_code_rol_in_koppeling = 'WF'   THEN pov_code_rol_in_koppeling := 'VW'   ;

   ELSIF piv_code_rol_in_koppeling = 'CA'   THEN pov_code_rol_in_koppeling := 'KC'   ;
   ELSIF piv_code_rol_in_koppeling = 'KC'   THEN pov_code_rol_in_koppeling := 'CA'   ;

   ELSIF piv_code_rol_in_koppeling = 'CW'   THEN pov_code_rol_in_koppeling := 'WC'   ;
   ELSIF piv_code_rol_in_koppeling = 'WC'   THEN pov_code_rol_in_koppeling := 'CW'   ;

   ELSIF piv_code_rol_in_koppeling = 'WE'   THEN pov_code_rol_in_koppeling := 'EW'   ;
   ELSIF piv_code_rol_in_koppeling = 'EW'   THEN pov_code_rol_in_koppeling := 'WE'   ;

   ELSIF piv_code_rol_in_koppeling = 'CRWG' THEN pov_code_rol_in_koppeling := 'WGCR' ;
   ELSIF piv_code_rol_in_koppeling = 'WGCR' THEN pov_code_rol_in_koppeling := 'CRWG' ;

   ELSIF piv_code_rol_in_koppeling = 'BWWG' THEN pov_code_rol_in_koppeling := 'WGBW' ;
   ELSIF piv_code_rol_in_koppeling = 'WGBW' THEN pov_code_rol_in_koppeling := 'BWWG' ;

   ELSIF piv_code_rol_in_koppeling = 'CRCP' THEN pov_code_rol_in_koppeling := 'CPCR' ;
   ELSIF piv_code_rol_in_koppeling = 'CPCR' THEN pov_code_rol_in_koppeling := 'CRCP' ;

   ELSIF piv_code_rol_in_koppeling = 'BWCP' THEN pov_code_rol_in_koppeling := 'CPBW' ;
   ELSIF piv_code_rol_in_koppeling = 'CPBW' THEN pov_code_rol_in_koppeling := 'BWCP' ;

   ELSIF piv_code_rol_in_koppeling = 'BOCP' THEN pov_code_rol_in_koppeling := 'CPBO' ;
   ELSIF piv_code_rol_in_koppeling = 'CPBO' THEN pov_code_rol_in_koppeling := 'BOCP' ;

   ELSIF piv_code_rol_in_koppeling = 'MTT'  THEN pov_code_rol_in_koppeling := 'DTT'  ;
   ELSIF piv_code_rol_in_koppeling = 'DTT'  THEN pov_code_rol_in_koppeling := 'MTT'  ;

   ELSIF piv_code_rol_in_koppeling = 'OVGN'  THEN pov_code_rol_in_koppeling := 'AFKV';
   ELSIF piv_code_rol_in_koppeling = 'AFKV'  THEN pov_code_rol_in_koppeling := 'OVGN';
   /* ONGERICHTE KOPPELINGEN */
   ELSIF piv_code_rol_in_koppeling = 'H'    THEN pov_code_rol_in_koppeling := 'H'    ;
   ELSIF piv_code_rol_in_koppeling = 'G'    THEN pov_code_rol_in_koppeling := 'G'    ;
   ELSIF piv_code_rol_in_koppeling = 'S'    THEN pov_code_rol_in_koppeling := 'S'    ;
   ELSIF piv_code_rol_in_koppeling = 'DUBB' THEN pov_code_rol_in_koppeling := 'DUBB' ;
   ELSIF piv_code_rol_in_koppeling = 'ZUS'  THEN pov_code_rol_in_koppeling := 'ZUS'  ;
   ELSIF piv_code_rol_in_koppeling = 'BEZM' THEN pov_code_rol_in_koppeling := 'BEZM' ;

   /* Enkelvoudige koppelingen:
      RN
      HP
   */

   /* ONBEKEND */
   ELSE
      pov_code_rol_in_koppeling := NULL ;
   END IF ;
END RLE_GET_RKG_OMGEKEERD;
/