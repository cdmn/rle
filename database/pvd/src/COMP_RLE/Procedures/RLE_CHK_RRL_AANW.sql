CREATE OR REPLACE PROCEDURE comp_rle.RLE_CHK_RRL_AANW
 (P_RLE_NUMRELATIE NUMBER
 ,P_ROL_CODROL VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_RRL_002
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 )
 IS
/* C_RRL_002 */
select	*
from	rle_relatie_rollen rrl
where	rrl.rle_numrelatie   = b_numrelatie
and	rrl.rol_codrol = b_codrol;
-- Program Data
L_INDGEVONDEN VARCHAR2(1) := 'N';
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_CHK_RRL_AANW */
/* controle op voorkomen in rle_relatie_rollen op coderol*/
l_indgevonden := 'N';
IF	p_rol_codrol is not NULL then
	for r_rrl_002 in c_rrl_002(p_rle_numrelatie
	,	p_rol_codrol) loop
		l_indgevonden := 'J';
	end loop;
end if;
if	l_indgevonden = 'N' then
	stm_dbproc.raise_error('RLE-00389', '', 'rle_chk_rrl_aanw');
end if;
END RLE_CHK_RRL_AANW;

 
/