CREATE OR REPLACE procedure comp_rle.rle_rle_ads_toev
 (p_numrelatie in number
 ,p_codrol in varchar2
 ,p_srt_adres in varchar2
 ,p_begindatum in date
 ,p_codland in out varchar2
 ,p_postcod in out varchar2
 ,p_huisnum in out number
 ,p_toevnum in out varchar2
 ,p_woonpl in out varchar2
 ,p_straat in out varchar2
 ,p_einddatum in date
 ,p_locatie in varchar2
 ,p_numadres in out number
 ,p_ind_woonwagen in varchar2 := 'N'
 ,p_ind_woonboot in varchar2 := 'N'
 ,p_provincie in varchar2
 )
 is
  ---------------------------------------------------------------------------------------
  -- Wijzigingshistorie
  -- Datum         Auteur            Reden
  -- ------------------------------------------------------------------------------------
  -- ??-??-????    ???               Creatie
  -- 30-06-2016    Elise Versteeg    MNGBA-1090 + achterstallig onderhoud wegwerken
  ---------------------------------------------------------------------------------------

  -- program data
  l_lokatie varchar2(6);
  l_numadres number(7,0);
begin
  /* RLE_RLE_ADS_TOEV */
  stm_util.debug('Start RLE_RLE_ADS_TOEV');

  if p_numadres is null
  then
    l_lokatie := '000010';

    rle_m15_ads_verwerk( p_codland => p_codland
                       , p_postcod => p_postcod
                       , p_huisnum => p_huisnum
                       , p_toevnum => p_toevnum
                       , p_woonpl => p_woonpl
                       , p_straat => p_straat
                       , p_o_numadres => p_numadres
                       );
    l_lokatie := '000020';
  end if;
  l_lokatie := '000030';
  l_numadres := p_numadres;

  if l_numadres is null
  then
    l_lokatie := '000040';
    stm_dbproc.raise_error('RLE-00555', null,'rle_rle_ads_toev');
  end if;
  l_lokatie := '000050';
  rle_ras_toev( p_numrelatie => p_numrelatie
              , p_codrol => p_codrol
              , p_srt_adres => p_srt_adres
              , p_locatie => p_locatie
              , p_begindatum => p_begindatum
              , p_einddatum => p_einddatum
              , p_numadres => l_numadres
              , p_ind_vervallen => 'N'
              , p_ind_commit => 'N'
              , p_ind_woonwagen => p_ind_woonwagen
              , p_ind_woonboot => p_ind_woonboot
              , p_provincie => p_provincie
              );
  l_lokatie := '000060';
exception
  when others
  then
    stm_dbproc.raise_error('RLE_RLE_ADS_TOEV : '||nvl(l_lokatie,'leeg')||' '||sqlerrm);
end rle_rle_ads_toev;
/