CREATE OR REPLACE PROCEDURE comp_rle.RLEADS01
 (P_SCRIPT_NAAM VARCHAR2
 ,P_SCRIPT_ID VARCHAR2
 )
 IS
/* Datafix: rle_ads_fix_pkwpl
   Doel: Opschonen RLE_ADRESSEN, RLE_WOONPLAATSEN en RLE_STRATEN locaties met
         een verkeerde wpl en str aanduiding welke zijn aangemaakt tijdens de conversie.
   Proces1 Via dit datafix worden via de postcode en huisnummer uit RLE_ADRES de juiste woonplaats
          en straat erbij gezocht uit het PTT tabel. Deze worden vergeleken met de
          actuele straat- en woonplaats- naam in RLE_STRATEN en RLE_WOONPLAATSEN. indien de
          waardens niet overeenkomen dan moet RLE_ADRES worden aangepast. Daarvoor
          wordt eerst vastgesteld of de correcte woonplaats en straat reeds voorkomen in
          RLE_WOONPLAATS en RLE_STRAAT. Zoniet dan wordt de woonplaats en straat
          aangemaakt. Vervolgens wordt RLE_ADRES gemuteerd met een verwijzing naar
          de correcte woonplaats en straatnaam.
   Proces2 Opschonen onjuiste Woonplaatsen en straten. Er vindt een selectie plaats
          van alle RLE_WOONPLAATSEN die niet voorkomen in de PTT_NED_POSTKODE tabel.
          Deze woonplaatsen en daarbij behorende straten worden vervolgens verwijderd
          uit RLE_WOONPLAATSEN en RLE_STRATEN. Dit zal niet lukken indien deze in gebruik
          zijn. Deze gevallen hebben geen postcode waardoor ze tijdens proces1 niet zijn verwerkt.
   Proces3 Opschonen van straten die niet voorkomen in de PTT_NED_POSTKODE tabel.
          De betreffende STRATEN worden verwijderd, dit zal niet lukken indien deze
          in gebruik zijn. Dit zal op het overzicht te zien zijn.
   Proces4 Opschonen van dubbele adressen. Hoewel er een controle in de triggers zit op het
          ontstaan van dubbele adressen komen deze toch voor in de database. Hierop lopen
          enkele batches fout. Deze dubbele adressen moeten dus opschoond worden.
    Tijd: ongeveer 15 min
    Herstart: NVT. Commit pas aan het einde. Kan zonder aanpassing opnieuw gedraaid worden
    DATUM: 09-02-2002
*/

cursor c_adres (b_numadres number , b_wnpl number , b_straat number, b_huisnummer number, b_toevhuisnum varchar2)
is
select
a.numadres
from    rle_adressen a
,       rle_adressen b
where a.POSTCODE=b.POSTCODE
and   a.HUISNUMMER=b.HUISNUMMER
and   nvl(a.TOEVHUISNUM,1)=nvl(b.TOEVHUISNUM,1)
and   a.LND_CODLAND=b.LND_CODLAND
and   b.WPS_WOONPLAATSID=b_wnpl
and   b.STT_STRAATID=b_straat
and   b.huisnummer = b_huisnummer
and   nvl(b.toevhuisnum,'1') = nvl(b_toevhuisnum,'1')
and   a.NUMADRES <> b_numadres
;
/* selectie binnenlandse locaties uit RLE_ADRESSEN met een postcode*/
cursor c_ads
is
select ads.stt_straatid
,      ads.wps_woonplaatsid
,      ads.postcode
,      ads.huisnummer
,      ads.toevhuisnum
,      ads.numadres
,      wpl.woonplaats
,      str.straatnaam
from rle_woonplaatsen wpl
,    rle_straten str
,    rle_adressen ads
where wpl.woonplaatsid = ads.wps_woonplaatsid
and   str.straatid     = ads.stt_straatid
and   ads.postcode is not null
and   ads.lnd_codland = 'NL'
;
/* selectie id van de correcte woonplaats */
cursor c_wpl (b_woonplaats varchar2)
is
select woonplaatsid
from  rle_woonplaatsen wpl
where lnd_codland = 'NL'
and   woonplaats = b_woonplaats
;
/* select id van de correcte straat*/
cursor c_str (b_woonplaatsid number
             ,b_straatnaam   varchar2)
is
select straatid
from   rle_straten str
where  str.straatnaam = b_straatnaam
and    str.wps_woonplaatsid = b_woonplaatsid
;

/* selecteer onjuiste woonplaatsen die niet bestaan in ptt_ned_postcode*/
cursor c_wpl_001
is
select upper(wps.woonplaats) woonplaats
from rle_woonplaatsen wps
where wps.lnd_codland = 'NL'
minus
select distinct (npe.woonplaats)
from rle_ned_postcode npe
;

/* selecteer straten (met hun wnpl) die niet voorkomen in ptt_ned_postcode */
cursor c_wpl_011
is
select wps.woonplaats woonplaats
,      stt.straatnaam straatnaam
from   rle_straten      stt
,      rle_woonplaatsen wps
where  stt.wps_woonplaatsid = wps.woonplaatsid
and    wps.lnd_codland = 'NL'
minus
select npe.woonplaats woonplaats
,      npe.straatnaam straatnaam
from   rle_ned_postcode npe
group  by npe.woonplaats,npe.straatnaam
;

/* selecteer id behorend bij onjuiste woonplaats */
cursor c_wpl_002 (b_woonplaats varchar2)
is
select wps.woonplaatsid
from   rle_woonplaatsen wps
where  wps.woonplaats = b_woonplaats
and    wps.lnd_codland = 'NL'
;
/* selecteer onjuiste straten behorend bij c_wpl_001 */
cursor c_str_001 (b_woonplaatsid number)
is
select stt.straatid
,      stt.straatnaam
,      stt.wps_woonplaatsid
from rle_straten stt
where stt.wps_woonplaatsid = b_woonplaatsid
;
r_ads c_ads%rowtype;
r_adres c_adres%rowtype;
r_wpl c_wpl%rowtype;
r_str c_str%rowtype;
r_wpl_001 c_wpl_001%rowtype;
r_wpl_011 c_wpl_011%rowtype;
r_wpl_002 c_wpl_002%rowtype;
r_str_001 c_str_001%rowtype;

t_npe_woonplaats RLE_NED_POSTCODE.woonplaats%type;
t_npe_straat RLE_NED_POSTCODE.straatnaam%type;
t_db_name varchar2(100);
t_locatie varchar2(50);
t_lijstnummer              stm_lijsten.lijstnummer%TYPE:= 1;
t_regelnummer              stm_lijsten.regelnummer%TYPE:= 0;
t_volgnummer               number(7) := 0;
t_sqlerrm                  varchar2(255);


/* cursor voor dubbele adressen */
cursor c_dub
is
  select a.numadres a_numadres
       , b.numadres b_numadres
       , a.postcode
       , a.huisnummer
       , a.toevhuisnum
       , a.lnd_codland
       , decode(a.postcode,'2280HE', 'MN',null)                                   mn
       , decode(a.WPS_WOONPLAATSID,b.WPS_WOONPLAATSID,null,'WPL ongelijk')        wpl
       , decode(a.STT_STRAATID,b.STT_STRAATID,null,'Straat ongelijk')             str
  from    rle_adressen a
  ,       rle_adressen b
  where a.POSTCODE=b.POSTCODE
  and   a.HUISNUMMER=b.HUISNUMMER
  and   nvl(a.TOEVHUISNUM,'xXx')=nvl(b.TOEVHUISNUM,'xXx')
  and   a.LND_CODLAND=b.LND_CODLAND
  and   a.WPS_WOONPLAATSID=b.WPS_WOONPLAATSID
  and   a.STT_STRAATID=b.STT_STRAATID
  and   a.NUMADRES <> b.numadres
  and   a.postcode <> '2280HE' ;    -- MN zelf niet meenemen


/* Hoofdverwerking */
BEGIN
  -- stm_util.set_debug_on;
  t_locatie := 'Herstel rle_adressen';
  stm_util.t_programma_naam := 'RLEADS01';
  stm_util.t_script_naam    := UPPER(p_script_naam);
  stm_util.t_script_id      := TO_NUMBER(p_script_id);
  SELECT value
  INTO t_db_name
  FROM v$parameter
  WHERE name = 'db_name'
  ;
  stm_util.insert_lijsten(t_lijstnummer
                           ,t_regelnummer,
                   'datafix herstellen rle_adressen op database '||t_db_name||' '||
                   TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
  stm_util.insert_lijsten(t_lijstnummer
                         ,t_regelnummer,
                         'mut numads'||CHR(09)
                         ||'postc'||CHR(09)
                         ||'huisnr'  ||CHR(09)
                         ||'stt_id'||CHR(09)
                         ||'wpl_id'||CHR(09)
                         ||RPAD('straatnaam',40)
                         ||RPAD('woonplaats',40)
                         );
   /* PROCES1 OPSCHONEN ONJUISTE POSTC HUISNUMMER MET WPL EN STRAAT COMBINATIE */
  OPEN c_ads;
  FETCH c_ads INTO r_ads;
  WHILE c_ads%found
  LOOP
    BEGIN
      /* bepaal wpl en straat uit PTT tabel via postc huisnummer */
      rle_get_wps_stt(r_ads.postcode
                     ,r_ads.huisnummer
                     ,t_npe_woonplaats
                     ,t_npe_straat);
      /* muteren van rle_adressen indien niet aansluiten met ptt_ned */
      IF t_npe_woonplaats <> r_ads.woonplaats
      OR t_npe_straat     <> r_ads.straatnaam
      THEN
         stm_util.insert_lijsten(t_lijstnummer
                                ,t_regelnummer,
                                'oud '||r_ads.numadres||CHR(09)
                                ||r_ads.postcode||CHR(09)
                                ||r_ads.huisnummer||CHR(09)
                                ||r_ads.stt_straatid||CHR(09)
                                ||r_ads.wps_woonplaatsid||CHR(09)
                                ||RPAD(r_ads.straatnaam,40)
                                ||RPAD(r_ads.woonplaats,40)
                                );
         /* ophalen nieuwe id van woonplaatsnaam behorend bij t_npe_wpl */
         r_wpl.woonplaatsid := NULL;
         r_str.straatid     := NULL;

         OPEN c_wpl(t_npe_woonplaats);
         FETCH c_wpl INTO r_wpl;
         CLOSE c_wpl;
         /* aanmaken nieuwe wpl afkomstig van ptt indien deze niet bestaat */
         IF r_wpl.woonplaatsid IS NULL
         THEN
            INSERT INTO rle_woonplaatsen
            (lnd_codland
            ,woonplaats
            )
            VALUES
            ('NL'
            ,t_npe_woonplaats
            )returning woonplaatsid INTO r_wpl.woonplaatsid;
         END IF;
         /* ophalen nieuwe id van straat */
         OPEN c_str(r_wpl.woonplaatsid
                   ,t_npe_straat
                   );
         FETCH c_str INTO r_str;
         CLOSE c_str;
         /* aanmaken nieuwe str afkomstig van ptt behorend bij (evt) nieuwe wpl*/
         IF r_str.straatid IS NULL
         THEN
            INSERT INTO rle_straten
            (wps_woonplaatsid
            ,straatnaam
            )
            VALUES
            (r_wpl.woonplaatsid
            ,t_npe_straat
            )returning straatid INTO r_str.straatid
            ;
         END IF;
         /* verkeerde rle_adres muteren met nieuwe wpl en str */
	   --
	   -- eerst kijken of het adres reeds bekend is zo nee update
	   -- zo ja hang de relatie adressen om en verwijder het oude adres
	   --
	   r_adres.numadres := NULL;
	   OPEN C_ADRES ( r_ads.numadres
                        , r_wpl.woonplaatsid
			, r_str.straatid
                        , r_ads.huisnummer
                        , r_ads.toevhuisnum
                        );
	   FETCH c_adres INTO r_adres;
	   IF c_adres%found THEN
	      UPDATE rle_relatie_adressen
		SET    ads_numadres=r_adres.numadres
		WHERE  ads_numadres=r_ads.numadres;
		--
		DELETE FROM rle_adressen
		WHERE  numadres=r_ads.numadres;
	   ELSE
           UPDATE rle_adressen ads
           SET    ads.stt_straatid =      r_str.straatid
           ,      ads.wps_woonplaatsid =  r_wpl.woonplaatsid
           WHERE ads.numadres = r_ads.numadres
           ;
	   END IF;
	   CLOSE c_adres;
         stm_util.insert_lijsten(t_lijstnummer
                                ,t_regelnummer,
                                'new '||r_ads.numadres||CHR(09)
                                ||r_ads.postcode||CHR(09)
                                ||r_ads.huisnummer||CHR(09)
                                ||r_str.straatid||CHR(09)
                                ||r_wpl.woonplaatsid||CHR(09)
                                ||RPAD(t_npe_straat,40)
                                ||t_npe_woonplaats);
      END IF;

    FETCH c_ads INTO r_ads;
    EXCEPTION -- binnen de loop
    WHEN OTHERS
    THEN
       stm_util.insert_lijsten(t_lijstnummer
                              ,t_regelnummer,
                              SQLERRM);
       stm_util.insert_lijsten(t_lijstnummer
                              ,t_regelnummer
                              ,r_ads.postcode||CHR(09)
                              ||r_ads.huisnummer||CHR(09)
                              ||r_ads.stt_straatid||CHR(09)
                              ||r_ads.wps_woonplaatsid||CHR(09)
                              ||r_ads.numadres);
         /* Cursoren sluiten */
         IF c_wpl%ISOPEN
         THEN
            CLOSE c_wpl;
         END IF ;

         IF c_str%ISOPEN
         THEN
            CLOSE c_str ;
         END IF ;

         IF c_adres%ISOPEN
         THEN
            CLOSE c_adres ;
         END IF ;
         /* Desondanks Ga door met de volgende */
         FETCH c_ads INTO r_ads;
    END;
  END LOOP; /* PROCES1 */


  /* PROCES2 OPSCHONEN ONJUISTE WOONPLAATSEN EN STRATEN */
  stm_util.insert_lijsten(t_lijstnummer
                         ,t_regelnummer,
  'Opschonen woonplaatsen en bijhorende straten niet bestaand in PTT_NED_POSTCODE');
  t_locatie := 'Verwijder rle_woonplaatsen en rle_straten';
  OPEN c_wpl_001;
  FETCH c_wpl_001 INTO r_wpl_001;
  /* selectie niet bestaande woonplaatsen */
  WHILE c_wpl_001%found
  LOOP
     BEGIN
         /* ophalen id niet bestaande woonplaats */
         OPEN c_wpl_002(r_wpl_001.woonplaats);
         FETCH c_wpl_002 INTO r_wpl_002;
         CLOSE c_wpl_002;
         stm_util.insert_lijsten(t_lijstnummer
                                ,t_regelnummer,
                                r_wpl_002.woonplaatsid||CHR(09)
                                ||r_wpl_001.woonplaats
                                );
         r_str_001 := NULL;
         /* ophalen en verwijderen straten bij niet bestaand wpl */
         OPEN c_str_001(r_wpl_002.woonplaatsid);
         FETCH c_str_001 INTO r_str_001;
         WHILE c_str_001%found
         LOOP
            stm_util.insert_lijsten(t_lijstnummer
                                   ,t_regelnummer,
                                    r_wpl_002.woonplaatsid||CHR(09)
                                   ||r_str_001.straatid||CHR(09)
                                   ||r_str_001.straatnaam
                                   );
            DELETE FROM rle_straten str
            WHERE str.wps_woonplaatsid = r_wpl_002.woonplaatsid
            AND   str.straatid         = r_str_001.straatid
            ;
            FETCH c_str_001 INTO r_str_001;
         END LOOP;
         CLOSE c_str_001;
         /* verwijder woonplaats */
         DELETE FROM rle_woonplaatsen
         WHERE  woonplaatsid = r_wpl_002.woonplaatsid
         ;
      FETCH c_wpl_001 INTO r_wpl_001;
      EXCEPTION -- verwijder loop
      WHEN OTHERS
      THEN
         stm_util.insert_lijsten(t_lijstnummer
                           ,t_regelnummer,
                       'fout tijdens verw wpl: '||r_wpl_002.woonplaatsid||CHR(09)
                       ||RPAD(r_wpl_001.woonplaats,40)||RPAD(r_str_001.straatnaam,40));
         stm_util.insert_lijsten(t_lijstnummer
                           ,t_regelnummer,
                           SQLERRM);
         /* Cursoren sluiten */
         IF c_wpl_002%ISOPEN
         THEN
            CLOSE c_wpl_002 ;
         END IF ;

         IF c_str_001%ISOPEN
         THEN
            CLOSE c_str_001 ;
         END IF ;
         /* Desondanks Ga door met de volgende */
         FETCH c_wpl_001 INTO r_wpl_001;
      END;
  END LOOP; /* proces2 */

/* PROCES3 OPSCHONEN ONJUISTE STRATEN */
  stm_util.insert_lijsten(t_lijstnummer
                         ,t_regelnummer,
  'Opschonen straten niet bestaand in PTT_NED_POSTCODE');
  t_locatie := 'Verwijder rle_straten';
  OPEN c_wpl_011;
  FETCH c_wpl_011 INTO r_wpl_011;
  /* selectie niet bestaande straten bij hun woonplaatsen */
  WHILE c_wpl_011%found
  LOOP
      BEGIN
         /* ophalen id niet bestaande woonplaats */
	   r_wpl_002.woonplaatsid := NULL;
		 --
         OPEN c_wpl_002(r_wpl_011.woonplaats);
         FETCH c_wpl_002 INTO r_wpl_002;
         CLOSE c_wpl_002;
		 --
         stm_util.insert_lijsten(t_lijstnummer
                                ,t_regelnummer,
					     'Verwijder straat : '
                                   ||r_wpl_002.woonplaatsid||CHR(09)
                                   ||r_wpl_011.woonplaats||CHR(09)
                                   ||r_wpl_011.straatnaam
                                   );
         DELETE FROM rle_straten str
         WHERE  str.wps_woonplaatsid = r_wpl_002.woonplaatsid
         AND    str.straatnaam       = r_wpl_011.straatnaam
         ;
      --
      FETCH c_wpl_011 INTO r_wpl_011;
      EXCEPTION -- verwijder loop
      WHEN OTHERS
      THEN
         stm_util.insert_lijsten(t_lijstnummer
                           ,t_regelnummer,
                       'fout tijdens verw str: '||r_wpl_002.woonplaatsid||CHR(09)
                       ||RPAD(r_wpl_011.woonplaats,40)||RPAD(r_wpl_011.straatnaam,40));
         stm_util.insert_lijsten(t_lijstnummer
                           ,t_regelnummer,
                           SQLERRM);
         /* Cursoren sluiten */
         IF c_wpl_002%ISOPEN
         THEN
            CLOSE c_wpl_002 ;
         END IF ;
         /* Desondanks Ga door met de volgende */
         FETCH c_wpl_011 INTO r_wpl_011;
      END;
  END LOOP; /* proces3 */

  /* PROCES4 OPSCHONEN DUBBELE ADRESSEN*/
  stm_util.insert_lijsten(t_lijstnummer
                         ,t_regelnummer,
                         'Opschonen dubbele adressen');
  t_locatie := 'Ontdubbelen adressen';
  for r_dub in c_dub
  loop
     begin
       --
       --stm_util.insert_lijsten(t_lijstnummer
       --                       ,t_regelnummer
       --                       ,'Ontdubbelen adres:' || r_dub.a_numadres || '<-'
       --                        || r_dub.b_numadres || ', postcode/huisnummer ' || r_dub.postcode
       --                        || '-' || r_dub.huisnummer
       --                        || r_dub.toevhuisnum );
       --
       -- bijwerken rle_relatie adressen
       update rle_relatie_adressen
       set    ads_numadres = r_dub.a_numadres
       where  ads_numadres = r_dub.b_numadres
       and    exists (select 1
                      from   rle_adressen
                      where  numadres = r_dub.a_numadres);
       --
       if sql%rowcount > 0 then
         -- Alleen maar op lijst indien werkelijk is uitgevoerd. Omdat iedere combinatie 2x in query zit.
         stm_util.insert_lijsten(t_lijstnummer
                                ,t_regelnummer
                                ,'Ontdubbelen adres:' || r_dub.a_numadres || '<-'
                                  || r_dub.b_numadres || ', postcode/huisnummer ' || r_dub.postcode
                                  || '-' || r_dub.huisnummer
                                  || r_dub.toevhuisnum );
       end if;
       --
       --stm_util.insert_lijsten(t_lijstnummer
       --                       ,t_regelnummer
       --                       ,'Aantal relatie adressen omgehangen:' || sql%rowcount);
       --
       delete from rle_adressen ads
       where  numadres = r_dub.b_numadres
       and    not exists (select 1
                          from   rle_relatie_adressen ras
                          where  ras.ads_numadres = ads.numadres);
       --
       --stm_util.insert_lijsten(t_lijstnummer
       --                       ,t_regelnummer
       --                       ,'Aantal adressen verwijderd:' || sql%rowcount);
       --
     exception
     when others
     then
        stm_util.insert_lijsten(t_lijstnummer
                               ,t_regelnummer,
                       'fout tijdens ontdubbelen adres: ' || r_dub.a_numadres ||
                       '<-' || r_dub.b_numadres || ', postcode ' || r_dub.postcode);
        stm_util.insert_lijsten(t_lijstnummer
                               ,t_regelnummer
                               ,SQLERRM);
     end;
  end loop;  /* dubbele adressen */
  --
  --rollback
  stm_util.insert_lijsten(t_lijstnummer
                         ,t_regelnummer
                         ,'Einde verwerking RLEADS01'||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
  stm_util.insert_job_statussen(t_volgnummer,'S','0');
  COMMIT;
  EXCEPTION -- hoofd
  WHEN OTHERS
  THEN
     ROLLBACK;
     stm_util.insert_job_statussen(t_volgnummer,'S','1');
     stm_util.insert_job_statussen(t_volgnummer,'I',SUBSTR(SQLERRM,1,255));
     stm_util.insert_job_statussen(t_volgnummer,'I',t_locatie);
     COMMIT;
END RLEADS01;
 
/