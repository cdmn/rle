CREATE OR REPLACE PROCEDURE comp_rle.RLE_CONVERSIE_ALTER_PRS_SEQ
 IS
-- PL/SQL Specification
ln_current_number  NUMBER ;
   ln_highest_number  NUMBER ;
-- PL/SQL Block
BEGIN
   select nvl( max( to_number( rec.extern_relatie ) ),  0 ) + 1
   into   ln_highest_number
   from   rle_relatie_externe_codes  rec
   where  rec.dwe_wrddom_extsys = 'PRS'
   ;
   select last_number
   into   ln_current_number
   from   user_sequences
   where  sequence_name = 'RLE_PERSOONSNUMMER_SEQ1'
   ;
   IF ln_current_number != ln_highest_number
   THEN
      execute immediate
	  'alter sequence comp_rle.rle_persoonsnummer_seq1
	  increment by ' || to_char( ln_highest_number - ln_current_number )
	  ;
      select rle_persoonsnummer_seq1.nextval
	  into   ln_current_number
	  from   dual
	  ;
      execute immediate
      'alter sequence comp_rle.rle_persoonsnummer_seq1
	  increment by 1'
	  ;
   END IF ;
END ;

 
/