CREATE OR REPLACE PROCEDURE comp_rle.RLE_INSERT_RIE
 (P_ADM_CODE IN VARCHAR2
 ,P_RLE_NUMRELATIE IN NUMBER
 ,P_ROL_CODROL IN VARCHAR2
 ,P_IND_AANGEMAAKT OUT VARCHAR2
 ,P_ERROR OUT VARCHAR2
 ,P_GBA_SYNC_INDIEN_AL_AANWEZIG IN VARCHAR2 := 'J'
 )
 IS
/*
Wanneer     Wie     Reden
---------------------------------------------
??          ??      Creatie
18-05-2008  XMK     Procedure totaal herzien voor project RLE 2008.
13-08-2008  XMK     Dup_val_on_index toegevoegd.
18-11-2008  XMK     Sequence wgr_wgr_seq2 wordt voortaan gebruikt.
10-01-2013  XCW     SEPA II: DB ook geldige rol
14-05-2013  XFZ     SEPA II: VN voor BOVEMIJ toegevoegd
*/

  cursor c_adm ( b_adm_code rle_administraties.code%type )
  is
    select adm.ind_afstemmen_gba_tgstn, adm.type
    from   rle_administraties adm
    where  adm.code = b_adm_code;

  cursor c_rrl ( b_rle_numrelatie rle_relatie_rollen.rle_numrelatie%type, b_rol_codrol rle_relatie_rollen.rol_codrol%type )
  is
    select 'x'
    from   rle_relatie_rollen rrl
    where  rrl.rle_numrelatie = b_rle_numrelatie
    and    rrl.rol_codrol = b_rol_codrol;

  cursor c_wgr
  is
    select wgr_s2.nextval
    from   dual;

  cursor c_prs
  is
    select rle_persoonsnummer_seq1.nextval
    from   dual;

  l_externe_relatie   rle_relatie_externe_codes.extern_relatie%type;
  l_dwe_wrddom_extsys rle_relatie_externe_codes.dwe_wrddom_extsys%type;
  l_ind_aangemaakt    varchar2 ( 1 ) := 'N';
  l_adm_type          rle_administraties.type%type;
  l_ind_afstemmen_gba_tgstn rle_administraties.ind_afstemmen_gba_tgstn%type;
  l_dummy             varchar2 ( 1 );
  l_error             varchar2 ( 2000 );
  c_proc_name constant varchar2 ( 20 ) := 'RLE_INSERT_RIE';
begin
  --
  open c_adm ( p_adm_code );

  fetch c_adm
  into   l_ind_afstemmen_gba_tgstn, l_adm_type;

  if c_adm%notfound
  then
    close c_adm;

    stm_dbproc.raise_error ( 'RLE-10381 Code administratie (' || p_adm_code || ') is onbekend' );
  end if;

  close c_adm;

  --
  if p_adm_code <> 'ALG'
     and p_rol_codrol not in ('PR', 'WG', 'DB')
  then
    stm_dbproc.raise_error ( 'RLE-10379 Opgegeven rol (' || p_rol_codrol || ') moet PR, WG of DB zijn' );
  elsif p_rol_codrol in ('PR', 'WG', 'DB', 'VN')
  then
    open c_rrl ( p_rle_numrelatie, p_rol_codrol );

    fetch c_rrl
    into   l_dummy;

    if c_rrl%notfound
    then
      rle_opvoeren_relatierol ( p_codrol         => p_rol_codrol
                              , p_relnum         => p_rle_numrelatie
                              , p_succes         => l_ind_aangemaakt );
    end if;

    close c_rrl;

    --
    -- Indien een nieuwe RELATIEROL wordt vastgelegd dan wordt tevens een Persoonsnummer
    -- of Werkgeversnummer uitgegeven.
    --
    if l_ind_aangemaakt = 'J'
       and p_rol_codrol not in ('DB', 'VN')
    then
      -- Bepalen nieuw persoonsnummer of nieuw werkgevernummer
      if p_rol_codrol = 'WG'
      then
        l_dwe_wrddom_extsys := 'WGR';

        open c_wgr;

        fetch c_wgr
        into   l_externe_relatie;

        close c_wgr;
      elsif p_rol_codrol = 'PR'
      then
        l_dwe_wrddom_extsys := 'PRS';

        open c_prs;

        fetch c_prs
        into   l_externe_relatie;

        close c_prs;
      end if;

      --
      rle_opvoeren_rel_externe_code ( p_relnum         => p_rle_numrelatie
                                    , p_dwe_wrddom_extsys => l_dwe_wrddom_extsys
                                    , p_datingang      => trunc ( sysdate )
                                    , p_codrol         => p_rol_codrol
                                    , p_extern_relatie => l_externe_relatie
                                    , p_succes         => l_dummy );
    end if;
  --
  end if;

  --
  begin
    --
    insert into rle_relatierollen_in_adm ( adm_code
                                         , rol_codrol
                                         , rle_numrelatie )
    values ( p_adm_code
           , p_rol_codrol
           , p_rle_numrelatie );

    --
    p_ind_aangemaakt := 'J';
  --
  exception
    when dup_val_on_index
    then
      p_ind_aangemaakt := 'N';
  end;

  --
  -- Als GBA afstemming mag voor de administratie dan doen we dat.
  --
  if l_ind_afstemmen_gba_tgstn = 'J'
     and ( p_ind_aangemaakt = 'J'
          or p_gba_sync_indien_al_aanwezig = 'J' )
  then
    rle_gba_afstemmen ( p_rle_numrelatie => p_rle_numrelatie );
  end if;

  --
  --Bepalen of misschien een ad-hoc bericht moet worden verzonden.
  --
  if l_adm_type = 'P'
     and l_ind_afstemmen_gba_tgstn = 'J'
     and p_rol_codrol = 'PR'
     and ( p_ind_aangemaakt = 'J'
          or p_gba_sync_indien_al_aanwezig = 'J' )
  then
    rle_zend_ad_hoc ( p_rle_numrelatie => p_rle_numrelatie, p_ind_aangemaakt => p_ind_aangemaakt );
  end if;
exception
  when others
  then
    p_ind_aangemaakt := 'N';
    p_error          := c_proc_name || ': ' || sqlerrm;
end;
 
/