CREATE OR REPLACE PROCEDURE comp_rle.RLE_RKN_WG_AV
 (P_WERKGEVERSNR IN VARCHAR2
 ,P_ADMINKANTOORNR IN VARCHAR2
 ,P_BEGINDATUM IN DATE
 ,P_IND_VERWERKT OUT VARCHAR2
 ,P_MELDING OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_RKG_001
 (CPV_CODE_ROL_IN_KOPPELING VARCHAR2
 )
 IS
SELECT *
FROM   rle_rol_in_koppelingen  rkg
WHERE  rkg.code_rol_in_koppeling = cpv_code_rol_in_koppeling
;
CURSOR C_RKN_005
 (CPN_NUMRELATIE IN NUMBER
 ,CPN_NUMRELATIE_VOOR IN NUMBER
 ,CPD_BEGINDATUM IN DATE
 ,CPV_ROL_IN_KOPPELING IN VARCHAR2
 )
 IS
SELECT /* Lijst kan "nog" vrij worden uitgebreid */
       rkn.id
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkn.rkg_id = rkg.id
AND    rkn.rle_numrelatie        = cpn_numrelatie
AND    rkn.rle_numrelatie_voor   = cpn_numrelatie_voor
AND    TRUNC( rkn.dat_begin )    = TRUNC( cpd_begindatum )
AND    rkg.code_rol_in_koppeling = cpv_rol_in_koppeling
;
-- Program Data
R_RKN_005 C_RKN_005%ROWTYPE;
L_NUMRELATIE_AK NUMBER;
R_RKG_001 C_RKG_001%ROWTYPE;
L_NUMRELATIE_WGR NUMBER;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* Default uitvoer */
   p_ind_verwerkt := 'N'  ;
   p_melding      := NULL ;
   /* Bepalen relatienummer werkgever */
   rle_m06_rec_relnum( p_werkgeversnr
                     , 'WGR'
                     , 'WG'
                     , SYSDATE
                     , l_numrelatie_wgr
                     ) ;
   IF l_numrelatie_wgr IS NULL
   THEN
      p_ind_verwerkt := 'N' ;
      p_melding      := 'Werkgevernummer is onbekend.' ;
      RETURN ;
   END IF ;
   /* Bepalen relatienummer administratiekantoor */
   rle_m06_rec_relnum( p_adminkantoornr
                     , 'AKR'
                     , 'AK'
                     , SYSDATE
                     , l_numrelatie_ak
                     ) ;
   IF l_numrelatie_ak IS NULL
   THEN
      p_ind_verwerkt := 'N' ;
      p_melding      := 'Administratiekantoornummer is onbekend.' ;
      RETURN ;
   END IF ;
   /* Bepalen soort koppeling */
   OPEN  c_rkg_001( 'AV' ) ;
   FETCH c_rkg_001 INTO r_rkg_001 ;
   CLOSE c_rkg_001 ;
   IF r_rkg_001.id IS NULL
   THEN
      p_ind_verwerkt := 'N' ;
      p_melding      := 'Type koppeling kan niet gevonden worden.' ;
      RETURN ;
   END IF ;
   /* Bepalen of deze koppeling al bestaat */
   OPEN  c_rkn_005( l_numrelatie_ak
                  , l_numrelatie_wgr
                  , p_begindatum
                  , 'AV'
                  ) ;
   FETCH c_rkn_005 INTO r_rkn_005 ;
   IF c_rkn_005%FOUND
   THEN
      CLOSE c_rkn_005 ;
      /* Koppeling bestaat al. Niets meer doen. */
      p_ind_verwerkt := 'J' ;
      p_melding      := 'Relatie koppeling bestond reeds.' ;
   ELSE
      CLOSE c_rkn_005 ;
      /* Relatiekoppeling bestaat nog niet.
         Vorige afsluiten en nieuwe opvoeren
      */
      UPDATE rle_relatie_koppelingen  rkn
      SET    rkn.dat_einde           = TRUNC( p_begindatum ) - 1
      WHERE  rkn.rkg_id              = r_rkg_001.id
      AND    rkn.rle_numrelatie_voor = l_numrelatie_wgr
      AND    rkn.dat_einde           IS NULL
      AND    rkn.dat_begin           < p_begindatum
      ;
      INSERT INTO rle_relatie_koppelingen(
           ID
         , RKG_ID
         , RLE_NUMRELATIE
         , RLE_NUMRELATIE_VOOR
         , DAT_BEGIN
         ) VALUES(
           rle_rkn_seq1.nextval
         , r_rkg_001.id
         , l_numrelatie_ak
         , l_numrelatie_wgr
         , p_begindatum
         ) ;
      p_ind_verwerkt := 'J';
   END IF ;
END RLE_RKN_WG_AV;

 
/