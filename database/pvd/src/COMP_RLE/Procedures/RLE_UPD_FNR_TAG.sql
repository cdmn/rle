CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPD_FNR_TAG
 (P_CODA_ELMCODE IN VARCHAR2
 ,P_CODA_CMPCODE IN VARCHAR2
 ,P_CODA_SWIFT IN VARCHAR2
 ,P_CODA_IBAN IN VARCHAR2
 ,P_CODA_TAG IN VARCHAR2
 )
 IS
  cursor c_fnr ( b_bic         varchar2
               , b_iban        varchar2
               , b_numrelatie  number
               )
  is
    select id
    from   rle_financieel_nummers fnr
    where iban = b_iban
    and    rle_numrelatie = b_numrelatie
   order by nvl(fnr.dateinde,sysdate + 1000) desc
        ,   decode(codwijzebetaling, 'AS', 1,2)
    ;
  -- l_variablen
  r_fnr       c_fnr%rowtype;
  l_type_rle  varchar2 ( 1 );
  l_pwr_nr    varchar2 ( 100 );
  l_rlnr      number;
  t_adm       stm_coda_pv.adm_tabtype;
  l_teller    number := 0;
function iff_codrol_exists (p_relnr number, p_adm varchar2 )
return boolean
is
  cursor c1
  is
  select  null
  from    rle_relatierollen_in_adm
  where  rle_numrelatie = p_relnr
  and     adm_code = p_adm;
  l_return   boolean := false;
begin
  for v1 in c1
  loop
     l_return := true;
  end loop;
  return l_return;
end;
begin
  l_type_rle := substr ( p_coda_elmcode
                       , 1
                       , 1
                       );
  --
  l_pwr_nr         := substr ( p_coda_elmcode, 2 );
  --
  begin
    case l_type_rle
      when 'W'
      then
        l_rlnr           :=
          to_number ( rle_lib.get_relatienr_werkgever ( p_werkgevernummer => lpad ( trim ( l_pwr_nr )
                                                                                  , 6
                                                                                  , '0' ) ) );
      when 'P'
      then
        l_rlnr           := rle_lib.get_relatienr_persoon ( to_number ( l_pwr_nr ) );
      when 'R'
      then
        l_rlnr           := l_pwr_nr;
      else
        null;
    end case;
  exception
    when others
    then
      l_rlnr           := 0;
  end;
 --
  open c_fnr ( p_coda_swift
             , p_coda_iban
             , l_rlnr );

  fetch c_fnr
  into  r_fnr;

  if c_fnr%notfound
  then
    null;
  else
    begin
      t_adm := stm_coda_pv.get_pv_adm_code ( 'DEBITEUR'
                                           , p_coda_cmpcode
                                           , null
                                           );

      if t_adm is not null
         and t_adm.count > 0
      then
        for i in t_adm.first .. t_adm.last
        loop
          if iff_codrol_exists (l_rlnr,  t_adm ( i ).adm_code)
             or l_type_rle = 'R'
          then
             begin
               update rle_finnr_coda
               set tag = p_coda_tag
               where  fnr_id = r_fnr.id
               and    adm_code = t_adm ( i ).adm_code
               and   ind_r_nummer = decode (l_type_rle, 'R', 'J', 'N')
               and   tag <> p_coda_tag;
               /*
               insert into rle_finnr_coda
                      ( fnr_id, adm_code, tag, ind_r_nummer)
               values ( r_fnr.id
                      , t_adm ( i ).adm_code
                      , p_coda_tag
                      , decode (l_type_rle, 'R', 'J', 'N') );

             exception
             when dup_val_on_index -- al eerder in een PRC01 run
             then
               null;
             */
             end;
          end if;
        end loop;
      end if;
    end;
  end if;
end;
 
/