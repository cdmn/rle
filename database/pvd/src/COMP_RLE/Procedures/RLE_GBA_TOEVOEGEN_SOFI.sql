CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_TOEVOEGEN_SOFI
 (PIN_NUMRELATIE IN NUMBER
 )
 IS
-- Program Data
LN_PERSOONSNUMMER NUMBER;
LV_VERWERKT VARCHAR2(1);
LV_ROL VARCHAR2(2);
LV_FOUTMELDING VARCHAR2(4000);
LN_SOFINUMMER NUMBER;
-- PL/SQL Block
BEGIN
/* Alleen doorgeven als het nodig is */
   IF rle_indicatie.doorgeven_aan_gba = 'N'
   THEN
      RETURN ;
   END IF ;
   /* Bepalen rol van de nieuwe relatie */
   rle_gba_bepaal_rol( pin_numrelatie
                     , lv_rol
                     ) ;
   /* Doorgeven hoeft alleen maar voor personen */
   IF lv_rol IS NULL
   THEN
      RETURN ;
   END IF ;
   ln_persoonsnummer := rle_m03_rec_extcod( 'PRS'
                                          , pin_numrelatie
                                          , 'PR'
                                          , SYSDATE
                                          ) ;
   ln_sofinummer := rle_m03_rec_extcod( 'SOFI'
                                      , pin_numrelatie
                                      , 'PR'
                                      , SYSDATE
                                      ) ;
   /* Aanroep GBA dienst */
   ibm.ibm( 'GBA_TOEVOEGEN_SOFINUMMER'
          , 'RLE'
          , ln_persoonsnummer                -- PIN_PERSOONSNUMMER
          , ln_sofinummer                    -- PIN_SOFINUMMER
          , lv_verwerkt                      -- POV_VERWERKT
          , lv_foutmelding                   -- POV_FOUTMELDING
          ) ;
   IF NVL( lv_verwerkt, 'N' ) != 'J'
   THEN
      IF    INSTR( lv_foutmelding, 'ORA-20000: ' ) = 1
      THEN
         stm_dbproc.raise_error( SUBSTR( lv_foutmelding, 12 ) ) ;
      ELSIF INSTR( lv_foutmelding, 'ORA-' ) = 1
      THEN
         stm_dbproc.raise_error( 'RLE-10313 #1Toevoegen sofinummer #2' || SUBSTR( lv_foutmelding, 12 ) ) ;
      ELSE
         stm_dbproc.raise_error( 'RLE-10313 #1Toevoegen sofinummer #2' || lv_foutmelding ) ;
      END IF ;
   END IF ;
END RLE_GBA_TOEVOEGEN_SOFI;

 
/