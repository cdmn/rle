CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_FNR_AI
 (P_NUMRELATIE IN NUMBER
 ,P_PEILDATUM IN DATE
 ,P_NUMREKENING_AI OUT VARCHAR2
 ,P_NUMREKENING_AG OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
cursor c_fnr (b_numrelatie in number
             ,b_peildatum  in date
             ,b_doelrek     in varchar2
             )
is
select  fnr.numrekening
       ,fnr.dwe_wrddom_srtrek
from  rle_financieel_nummers fnr
where fnr.datingang <= b_peildatum
and   nvl(fnr.dateinde,b_peildatum) >= b_peildatum
and   fnr.coddoelrek = b_doelrek
and   fnr.rle_numrelatie = b_numrelatie
order by datingang desc;
--
l_doelrek  rle_financieel_nummers.coddoelrek%type;
r_fnr      c_fnr%rowtype;
--
-- PL/SQL Block
BEGIN
    --
    /* 1. Eerst Incassonummer ophalen */
    l_doelrek := 'AIC';
    --
    open c_fnr(p_numrelatie
              ,p_peildatum
              ,l_doelrek
              );
    fetch c_fnr into r_fnr;
    IF c_fnr%found
    THEN
       P_NUMREKENING_AI := r_fnr.numrekening;
    ELSE
       P_NUMREKENING_AI := null;
    END IF;
    close c_fnr;
    --
    /* 2. Algemeen rekeningnummer ophalen */
    l_doelrek := 'ALGRN';
    --
    open c_fnr(p_numrelatie
              ,p_peildatum
              ,l_doelrek
              );
    fetch c_fnr into r_fnr;
    IF c_fnr%found
    THEN
       P_NUMREKENING_AG := r_fnr.numrekening;
    ELSE
       P_NUMREKENING_AG := null;
    END IF;
    close c_fnr;
    --
END;

 
/