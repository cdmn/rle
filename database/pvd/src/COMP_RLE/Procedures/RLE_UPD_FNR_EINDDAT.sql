CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPD_FNR_EINDDAT
 (P_NUMRELATIE rle_relaties.numrelatie%TYPE
 ,P_CODROL rle_rollen.codrol%TYPE
 ,P_O_DATSCHONING rle_financieel_nummers.dateinde%TYPE
 ,P_N_DATSCHONING rle_financieel_nummers.dateinde%TYPE
 )
 IS
BEGIN
/* RLE_UPD_FNR_EINDDAT */
IF	p_o_datschoning is null THEN
	UPDATE	rle_financieel_nummers
	SET	dateinde		= p_n_datschoning
	WHERE	rle_numrelatie	= p_numrelatie
	AND	NVL(rol_codrol,'@') = NVL(p_codrol,'@')
	AND	dateinde		is  null;
ELSE
	UPDATE	rle_financieel_nummers
	SET	dateinde		= p_n_datschoning
	WHERE	rle_numrelatie	= p_numrelatie
	AND	NVL(rol_codrol,'@') = NVL(p_codrol,'@')
	AND	(dateinde		= p_o_datschoning
                or 	dateinde is null);
END IF;
END RLE_UPD_FNR_EINDDAT;

 
/