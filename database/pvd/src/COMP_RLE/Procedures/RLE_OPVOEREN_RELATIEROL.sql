CREATE OR REPLACE PROCEDURE comp_rle.RLE_OPVOEREN_RELATIEROL
 (P_CODROL IN VARCHAR2
 ,P_RELNUM IN NUMBER
 ,P_SUCCES OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
CURSOR c_rrl(b_codrol IN VARCHAR2
                             ,b_relnum IN NUMBER)
IS
SELECT '1'
FROM RLE_RELATIE_ROLLEN rrl
WHERE rrl.ROL_CODROL = b_codrol
AND rrl.RLE_NUMRELATIE = b_relnum
;
l_dummy VARCHAR2(1);
l_errmsg VARCHAR2(2000);
e_relatierol_bestaat_al EXCEPTION;
-- PL/SQL Block
BEGIN
  p_succes := NULL;
  OPEN	c_rrl(p_codrol
                          ,p_relnum);
  FETCH	c_rrl INTO l_dummy;
  IF c_rrl%FOUND THEN
	CLOSE	c_rrl;
	RAISE e_relatierol_bestaat_al;
  ELSE
     CLOSE	c_rrl;
     INSERT INTO RLE_RELATIE_ROLLEN
    (
      RLE_NUMRELATIE,
      ROL_CODROL,
      CODORGANISATIE
    )
    VALUES
    (
      p_relnum
    , P_CODROL
    , 'A'
    );
   ---
   p_succes := 'J';
 END IF;
EXCEPTION
  WHEN e_relatierol_bestaat_al
  THEN
    p_succes := 'N';
  WHEN OTHERS
  THEN
    p_succes := 'N';
    l_errmsg := SQLERRM;
	stm_dbproc.raise_error('Rle_Opvoeren_Relatierol: '||l_errmsg);
END;

 
/