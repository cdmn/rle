CREATE OR REPLACE PROCEDURE comp_rle.RLE_AQ_MUT_WGR_WERKZAAMHEDEN
 IS

  l_consumer_name VARCHAR2 ( 27 ) := 'RLE_MUT_WGR_WERKZAAMHEDEN';
  l_message_handle RAW ( 16 );
  l_event_name VARCHAR2 ( 23 );
  l_params VARCHAR2 ( 4000 );
  l_params_save VARCHAR2 ( 4000 );
  l_succes BOOLEAN;
  l_errorcode VARCHAR2 ( 2000 );
  l_looping BOOLEAN := TRUE;
  l_soort_mutatie VARCHAR2 ( 100 );
  l_dummy_var VARCHAR2(100);
  l_wgr_id  VARCHAR2(100);
  l_dat_begin VARCHAR2(100);
  l_dat_einde VARCHAR2(100);
  l_code_groep_werkzhd VARCHAR2(100);
  l_code_subgroep_werkzhd VARCHAR2(100);
  l_wgr_nr  NUMBER;
  l_otherinfo VARCHAR2( 250 )
    := 'Advanced Queue listener comp_rle.rle_aq_mut_wgr_werkzaamheden is gestopt. Deze moet opnieuw worden gestart zodra het fout gegane bericht verwerkt kan worden.';
BEGIN
  WHILE l_looping
  LOOP
    -- Lezen bericht voor abonnementhouder ret_aq_overlijden_relatie
    -- Indien geen bericht klaar staat, en ook geen stop_queue bericht
    -- dan wordt gewacht op het eerstvolgende bericht.
    stm_aq.dequeue( p_consumer_name  => l_consumer_name
                  , p_dequeue_mode   => 'REMOVE'
                  , p_navigation     => 'FIRST_MESSAGE'
                  , p_message_handle => l_message_handle
                  , p_event_name     => l_event_name
                  , p_params         => l_params
                  , p_succes         => l_succes
                  , p_errorcode      => l_errorcode
                  );
    IF l_succes
    THEN
      l_params_save := l_params;
      --
      IF l_event_name IN ('STOP_QUEUE', stm_aq.stop_event('RLE','MUT WGR WERKZAAMHEDEN'))
      THEN
        l_looping := FALSE;
      ELSE

        -- Lees de parameters en roep de verwerkende module aan.
        l_soort_mutatie         := stm_get_word ( l_params ); --1
        l_dummy_var             := stm_get_word ( l_params ); --2
        l_dummy_var             := stm_get_word ( l_params ); --3
        l_dummy_var             := stm_get_word ( l_params ); --4
        l_dummy_var             := stm_get_word ( l_params ); --5
        l_dat_begin             := stm_get_word ( l_params ); --6
        l_dummy_var             := stm_get_word ( l_params ); --7
        l_wgr_id                := stm_get_word ( l_params ); --8
        l_dummy_var             := stm_get_word ( l_params ); --9
        l_code_groep_werkzhd    := stm_get_word ( l_params ); --10
	l_dummy_var             := stm_get_word ( l_params ); --11
	l_dat_einde             := stm_get_word ( l_params ); --12
	l_dummy_var             := stm_get_word ( l_params ); --13
	l_code_subgroep_werkzhd := stm_get_word ( l_params ); --14

         IF l_soort_mutatie in ('I', 'U')
         THEN
	    --
            -- nu ophalen werkgevernummer
            get_wgrnr( p_pin_wgr_id       => to_number(l_wgr_id)
                     , p_pon_nr_werkgever => l_wgr_nr
                      );
            --
	    rle_mut_wgr_werkzaamheden( p_wgr_werkgnr      => lpad(to_char(l_wgr_nr),6,'0')
	                             , p_kodegvw          => l_code_groep_werkzhd
		                     , p_kodesgw          => l_code_subgroep_werkzhd
				     , p_dat_begin        => to_date(l_dat_begin, 'dd-mm-yyyy hh24:mi:ss')
				     , p_dat_einde        => to_date(l_dat_einde, 'dd-mm-yyyy hh24:mi:ss')
                                     , p_succes           => l_succes
                                     , p_errorcode        => l_errorcode
                                     );
         END IF;
      END IF;
   END IF;
   --
   IF l_succes
   THEN
     COMMIT;
   ELSE
      -- Verwerking fout gegaan OF lezen queue fout gegaan.
      -- Doe een rollback en roep de standaard error handler aan
      -- voor achtergrondprocessen.
      -- Na een fout moet de listener altijd worden beeindigd!!
      ROLLBACK;
      stm_bgproc.error_handler ( p_type_verwerking                    => stm_bgproc.c_advanced_queuing
                               , p_procedure                          => 'RLE_AQ_MUT_WGR_WERKZAAMHEDEN'
                               , p_errormsg                           => l_errorcode
                               , p_params                             => l_params_save
                               , p_otherinfo                          => l_otherinfo
                               , p_raise_exception                    => FALSE
                               );
      l_looping := FALSE;
    END IF;
  END LOOP;
EXCEPTION
  WHEN OTHERS
  THEN
    ROLLBACK;
    stm_bgproc.error_handler ( p_type_verwerking                      => stm_bgproc.c_advanced_queuing
                             , p_procedure                            => 'RLE_AQ_MUT_WGR_WERKZAAMHEDEN'
                             , p_errormsg                             => SQLERRM
                             , p_params                               => l_params_save
                             , p_otherinfo                            => l_otherinfo
                             , p_raise_exception                      => FALSE
                             );
END RLE_AQ_MUT_WGR_WERKZAAMHEDEN;
 
/