CREATE OR REPLACE PROCEDURE comp_rle.RLE_ADS_CHK_PKWPL
 (P_PCODE IN VARCHAR2
 ,P_HUISNR IN NUMBER
 ,P_STRAATID IN NUMBER
 ,P_WOONPLAATSID IN NUMBER
 )
 IS
-- Sub-Program Unit Declarations
CURSOR C_STT_003
 (B_STRAATID IN NUMBER
 )
 IS
/* C_STT_003 */
select	*
from	rle_straten stt
where	stt.straatid = b_straatid;
/* c_wnpl03 */
CURSOR C_WPS_003
 (B_WPS_ID IN NUMBER
 )
 IS
/* C_WPS_003 */
select wps.*
,		 rowid
from	rle_woonplaatsen wps
where	wps.woonplaatsid = b_wps_id;
-- Program Data
L_NPE_STRAAT VARCHAR2(30);
L_NPE_WOONPLAATS VARCHAR2(30);
L_STT_STRAAT VARCHAR2(30);
L_WPS_WOONPLAATS VARCHAR2(30);
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_ADS_CHK_PKWPL */
  /* Controles of de postcode/huisnummer in overeenstemming is met de
  straat/woonplaats. */
  rle_get_wps_stt(p_pcode
  ,	p_huisnr
  ,	l_npe_woonplaats
  ,	l_npe_straat);
  for r_stt_003 in c_stt_003(p_straatid) loop
  	l_stt_straat := r_stt_003.straatnaam;
  end loop;
  for r_wps_003 in c_wps_003(p_woonplaatsid) loop
  	l_wps_woonplaats := r_wps_003.woonplaats;
  end loop;
  if	upper(l_npe_straat) <> upper(l_stt_straat)
  or	upper(l_npe_woonplaats) <> upper(l_wps_woonplaats) then
  	stm_dbproc.raise_error('RLE-00460',null, 'rle_ads_chk_pkwpl');
  end if;
END RLE_ADS_CHK_PKWPL;

 
/