CREATE OR REPLACE procedure comp_rle.rle_cre_waardeoverdracht
 (
  p_relatienummer in varchar2
, p_rol_codrol in varchar2
, p_code_adm in varchar2
, p_succes out boolean
, p_errorcode out varchar2
)
is
l_errorcode varchar2(2000);
begin
  --
  rle_rie.kop_wod(p_rle_numrelatie  => p_relatienummer
		 ,p_adm_code        => p_code_adm
	         ,p_rol_codrol      => p_rol_codrol
		 ,p_error           => l_errorcode
		 );
  --
  if l_errorcode is null
  then
     p_succes := true;
  else
    p_errorcode := l_errorcode;
    p_succes := false;
  end if;
exception
  when others
  then
    p_errorcode := sqlerrm;
    p_succes := false;
end rle_cre_waardeoverdracht;
 
/