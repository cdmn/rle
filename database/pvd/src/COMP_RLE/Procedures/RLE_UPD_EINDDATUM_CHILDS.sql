CREATE OR REPLACE PROCEDURE comp_rle.RLE_UPD_EINDDATUM_CHILDS
 (P_NUMRELATIE IN NUMBER
 ,P_EINDDATUM_OUD IN DATE
 ,P_EINDDATUM_NEW IN DATE
 )
 IS
begin
 IF stm_dbproc.column_modified(p_einddatum_oud,p_einddatum_new)
 THEN
 if p_einddatum_new is not null
 and p_einddatum_new < nvl(p_einddatum_oud,p_einddatum_new+1)
 then
    update rle_communicatie_nummers
    set dateinde = trunc(p_einddatum_new)
    where rle_numrelatie = p_numrelatie
    and   nvl(dateinde,p_einddatum_new+1) > p_einddatum_new;
    update rle_financieel_nummers
    set dateinde = trunc(p_einddatum_new)
    where rle_numrelatie = p_numrelatie
    and   nvl(dateinde,p_einddatum_new+1) > p_einddatum_new;
    update rle_relatie_adressen
    set dateinde = trunc(p_einddatum_new)
    where rle_numrelatie = p_numrelatie
    and   nvl(dateinde,p_einddatum_new+1) > p_einddatum_new;
 else
    update rle_relatie_adressen
    set dateinde = trunc(p_einddatum_new)
    where rle_numrelatie = p_numrelatie
    and   dateinde = p_einddatum_oud;
    update rle_communicatie_nummers
    set dateinde = trunc(p_einddatum_new)
    where rle_numrelatie = p_numrelatie
    and   dateinde = p_einddatum_oud;
    update rle_financieel_nummers
    set dateinde = trunc(p_einddatum_new)
    where rle_numrelatie = p_numrelatie
    and   dateinde = p_einddatum_oud;
 end if;
 END IF /* einddatum modified */;
end;

 
/