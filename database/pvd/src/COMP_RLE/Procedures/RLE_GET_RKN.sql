CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_RKN
 (PIN_RKN_ID IN NUMBER
 ,PON_NUMRELATIE OUT NUMBER
 ,PON_NUMRELATIE_VOOR OUT NUMBER
 ,POD_DAT_BEGIN OUT DATE
 ,POD_DAT_EINDE OUT DATE
 ,POV_CODE_ROL_IN_KOPPELING OUT VARCHAR2
 ,POV_OMSCHRIJVING OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Zoeken relatiekoppeling met ID */
CURSOR C_RKN_004
 (CPN_RKN_ID IN NUMBER
 )
 IS
SELECT rkn.id
,      rkn.rkg_id
,      rkn.rle_numrelatie
,      rkn.rle_numrelatie_voor
,      rkn.dat_begin
,      rkn.dat_einde
,      rkg.code_rol_in_koppeling
,      rkg.omschrijving
FROM   rle_relatie_koppelingen  rkn
,      rle_rol_in_koppelingen   rkg
WHERE  rkn.id     = cpn_rkn_id
AND    rkn.rkg_id = rkg.id
;
-- Program Data
R_RKN_004 C_RKN_004%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* default waarden */
   pon_numrelatie            := NULL ;
   pon_numrelatie_voor       := NULL ;
   pod_dat_begin             := NULL ;
   pod_dat_einde             := NULL ;
   pov_code_rol_in_koppeling := NULL ;
   pov_omschrijving          := NULL ;
   OPEN c_rkn_004( pin_rkn_id ) ;
   FETCH c_rkn_004 INTO r_rkn_004 ;
   IF c_rkn_004%FOUND
   THEN
      pon_numrelatie            := r_rkn_004.rle_numrelatie ;
      pon_numrelatie_voor       := r_rkn_004.rle_numrelatie_voor ;
      pod_dat_begin             := r_rkn_004.dat_begin ;
      pod_dat_einde             := r_rkn_004.dat_einde ;
      pov_code_rol_in_koppeling := r_rkn_004.code_rol_in_koppeling ;
      pov_omschrijving          := r_rkn_004.omschrijving ;
   END IF ;
   CLOSE c_rkn_004 ;
EXCEPTION
   WHEN OTHERS THEN
      pon_numrelatie            := NULL ;
      pon_numrelatie_voor       := NULL ;
      pod_dat_begin             := NULL ;
      pod_dat_einde             := NULL ;
      pov_code_rol_in_koppeling := NULL ;
      pov_omschrijving          := NULL ;
      /* Sluiten cursor */
      IF c_rkn_004%ISOPEN
      THEN
         CLOSE c_rkn_004 ;
      END IF ;
END RLE_GET_RKN;

 
/