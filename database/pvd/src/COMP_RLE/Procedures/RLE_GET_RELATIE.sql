CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_RELATIE
 (P_NUMRELATIE IN NUMBER
 ,P_NAMRELATIE OUT VARCHAR2
 ,P_ZOEKNAAM OUT VARCHAR2
 ,P_INDGEWNAAM OUT VARCHAR2
 ,P_INDINSTELLING OUT VARCHAR2
 ,P_DATSCHONING OUT DATE
 ,P_DWE_WRDDOM_TITEL OUT VARCHAR2
 ,P_DWE_WRDDOM_TTITEL OUT VARCHAR2
 ,P_DWE_WRDDOM_AVGSL OUT VARCHAR2
 ,P_DWE_WRDDOM_VVGSL OUT VARCHAR2
 ,P_DWE_WRDDOM_BRGST OUT VARCHAR2
 ,P_DWE_WRDDOM_GESLACHT OUT VARCHAR2
 ,P_DWE_WRDDOM_GEWATITEL OUT VARCHAR2
 ,P_DWE_WRDDOM_RVORM OUT VARCHAR2
 ,P_VOORNAAM OUT VARCHAR2
 ,P_VOORLETTER OUT VARCHAR2
 ,P_AANSCHRIJFNAAM OUT VARCHAR2
 ,P_DATGEBOORTE OUT DATE
 ,P_CODE_GEBDAT_FICTIEF OUT VARCHAR2
 ,P_DATOVERLIJDEN OUT DATE
 ,P_DATOPRICHTING OUT DATE
 ,P_DATBEGIN OUT DATE
 ,P_DATCONTROLE OUT DATE
 ,P_DATEINDE OUT DATE
 ,P_INDCONTROLE OUT VARCHAR2
 ,P_REGDAT_OVERLBEVEST OUT DATE
 ,P_DATEMIGRATIE OUT DATE
 ,P_NAAM_PARTNER OUT VARCHAR2
 ,P_VVGSL_PARTNER OUT VARCHAR2
 ,P_CODE_AANDUIDING_NAAMGEBRUIK OUT VARCHAR2
 ,P_HANDELSNAAM OUT VARCHAR2
 ,P_CODORGANISATIE OUT VARCHAR2
 ,P_DAT_CREATIE OUT DATE
 ,P_CREATIE_DOOR OUT VARCHAR2
 )
 IS
-- PL/SQL Specification
cursor c_rle
is
select namrelatie, zoeknaam, indgewnaam, indinstelling, datschoning, dwe_wrddom_titel, dwe_wrddom_ttitel, dwe_wrddom_avgsl, dwe_wrddom_vvgsl, dwe_wrddom_brgst, dwe_wrddom_geslacht, dwe_wrddom_gewatitel, dwe_wrddom_rvorm, voornaam, voorletter, aanschrijfnaam, datgeboorte, code_gebdat_fictief, datoverlijden, datoprichting, datbegin, datcontrole, dateinde, indcontrole, regdat_overlbevest, datemigratie, naam_partner, vvgsl_partner, code_aanduiding_naamgebruik, handelsnaam, codorganisatie
,dat_creatie,creatie_door
from   RLE_RELATIES
where  numrelatie = p_numrelatie;
-- PL/SQL Block
begin
 open c_rle;
 fetch c_rle into  p_namrelatie
, p_zoeknaam
, p_indgewnaam
, p_indinstelling
, p_datschoning
, p_dwe_wrddom_titel
, p_dwe_wrddom_ttitel
, p_dwe_wrddom_avgsl
, p_dwe_wrddom_vvgsl
, p_dwe_wrddom_brgst
, p_dwe_wrddom_geslacht
, p_dwe_wrddom_gewatitel
, p_dwe_wrddom_rvorm
, p_voornaam
, p_voorletter
, p_aanschrijfnaam
, p_datgeboorte
, p_code_gebdat_fictief
, p_datoverlijden
, p_datoprichting
, p_datbegin
, p_datcontrole
, p_dateinde
, p_indcontrole
, p_regdat_overlbevest
, p_datemigratie
, p_naam_partner
, p_vvgsl_partner
, p_code_aanduiding_naamgebruik
, p_handelsnaam
, p_codorganisatie
, p_dat_creatie
, p_creatie_door;
close c_rle;
--
/* Vullen van gewenste aanschrijftitel met domeinwaarde,
zodat deze in brieven gebruikt kan worden
*/
if p_indinstelling = 'N'
and p_dwe_wrddom_gewatitel is null
then
   select decode(p_dwe_wrddom_geslacht,'M','DHR','V','MVR',p_dwe_wrddom_geslacht)
   into p_dwe_wrddom_gewatitel
   from dual;
end if;
--
end;

 
/