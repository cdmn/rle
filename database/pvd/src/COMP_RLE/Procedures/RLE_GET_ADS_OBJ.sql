CREATE OR REPLACE PROCEDURE comp_rle.RLE_GET_ADS_OBJ
 (P_NUMRELATIE IN NUMBER
 ,P_CODROL IN VARCHAR2
 ,P_SRTADRES IN varchar2
 ,P_DATUMP IN date
 ,P_POSTCODE IN OUT VARCHAR2
 ,P_HUISNR IN OUT number
 ,P_TOEVNUM IN OUT VARCHAR2
 ,P_WOONPLAATS IN OUT varchar2
 ,P_GEMEENTE IN OUT VARCHAR2
 ,P_STRAAT IN OUT varchar2
 ,P_IND_WOONBOOT IN OUT VARCHAR2
 ,P_IND_WOONWAGEN IN OUT VARCHAR2
 ,P_CODLAND IN OUT VARCHAR2
 ,P_NAAMLAND IN OUT VARCHAR2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van relatie-adres */
CURSOR C_WPS_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_WPS_001 */
select	wps.woonplaats
from	rle_woonplaatsen wps
,	rle_adressen ads
where	wps.woonplaatsid = ads.wps_woonplaatsid
and	numadres = b_numadres;
/* Ophalen van relatie-adres */
CURSOR C_STT_001
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_STT_001 */
select	stt.straatnaam
from	rle_straten stt
,	rle_adressen ads
where	stt.straatid  = ads.stt_straatid
and	ads.numadres  = b_numadres;
/* Ophalen van relatie-adres incl. ind woonboot/woonwagen */
CURSOR C_RAS_024
 (B_NUMRELATIE IN NUMBER
 ,B_CODROL IN VARCHAR2
 ,B_SRTADRES IN VARCHAR2
 ,B_DATUMP IN DATE
 )
 IS
/* C_RAS_024 */
select  ras.ads_numadres
,       ras.ind_woonboot
,       ras.ind_woonwagen
from    rle_relatie_adressen ras
where   ras.rle_numrelatie = b_numrelatie
and     (ras.rol_codrol is null or
         ras.rol_codrol = b_codrol)
and     ras.dwe_wrddom_srtadr = b_srtadres
and     nvl(b_datump,trunc(sysdate))
    between ras.datingang
    and     nvl(ras.dateinde
               ,nvl(b_datump,trunc(sysdate))
               )
order by ras.rle_numrelatie, ras.rol_codrol;
/* Ophalen van relatie-adres */
CURSOR C_ADS_002
 (B_NUMADRES IN NUMBER
 )
 IS
/* C_ADS_002 */
select ads.huisnummer
,      ads.toevhuisnum
,      ads.postcode
,      ads.lnd_codland
from   rle_adressen ads
where  ads.numadres = b_numadres;
/* Gemeente bij een postcode */
CURSOR C_NPE_007
 (B_POSTCODE IN VARCHAR2
 )
 IS
select gemeentenaam
from   rle_ned_postcode
where  postcode = b_postcode;
-- Program Data
R_STT_001 C_STT_001%ROWTYPE;
R_WPS_001 C_WPS_001%ROWTYPE;
R_RAS_024 C_RAS_024%ROWTYPE;
R_NPE_007 C_NPE_007%ROWTYPE;
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_GET_ADS_OBJ */
open	c_ras_024( p_numrelatie
               , p_codrol
               , p_srtadres
               , p_datump);
fetch	c_ras_024 into r_ras_024;
if c_ras_024%FOUND
then
  p_ind_woonboot   := r_ras_024.ind_woonboot;
  p_ind_woonwagen  := r_ras_024.ind_woonwagen;
  open  c_ads_002( r_ras_024.ads_numadres);
  fetch c_ads_002
  into  p_huisnr
      , p_toevnum
      , p_postcode
      , p_codland;
  close c_ads_002;
  p_naamland := rle_lnd_getoms(p_codland);
  if p_codland = 'NL'
  then
    -- Actuele postcode tabel raadplegen
    rle_get_wps_stt(p_postcode
                   , p_huisnr
                   , p_woonplaats
                   , p_straat);
    -- ophalen gemeentecode via postcode
    open  c_npe_007(p_postcode);
    fetch c_npe_007
    into  r_npe_007;
    close c_npe_007;
    p_gemeente := nvl(r_npe_007.gemeentenaam,'Onbekend');
  else
    open   c_stt_001(r_ras_024.ads_numadres);
    fetch  c_stt_001
    into   p_straat;
    close  c_stt_001;
    open   c_wps_001(r_ras_024.ads_numadres);
    fetch  c_wps_001
    into   p_woonplaats;
    close  c_wps_001;
  end if;
end if;
close	c_ras_024;
END RLE_GET_ADS_OBJ;

 
/