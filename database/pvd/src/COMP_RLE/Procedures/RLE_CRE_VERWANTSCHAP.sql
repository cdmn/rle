CREATE OR REPLACE procedure comp_rle.rle_cre_verwantschap
 (p_rle_numrelatie_dlnmr in varchar2
 ,p_rle_numrelatie_vrwnt in varchar2
 ,p_succes out boolean
 ,p_errorcode out varchar2
 )
 is
l_errorcode varchar2(2000);
begin
  -- doe de verwerking o.b.v. ingekomen parameters
  rle_rie.kop_rkn_rie(p_rle_numrelatie_dlnmr => p_rle_numrelatie_dlnmr
                     ,p_rle_numrelatie_vrwnt => p_rle_numrelatie_vrwnt
                     ,p_error                => l_errorcode
                     );
  --
  if l_errorcode is null
  then
     p_succes := true;
  else
    p_errorcode := l_errorcode;
    p_succes := false;
  end if;
exception
  when others
  then
    p_errorcode := sqlerrm;
    p_succes    := false;
end rle_cre_verwantschap;
 
/