CREATE OR REPLACE PROCEDURE comp_rle.RLE_M29_FINNUM
 (P_NUMRELATIE IN number
 ,P_CODROL IN varchar2
 ,P_SRTREK IN varchar2
 ,P_DATUM IN date
 ,P_NUMREKENING IN OUT number
 ,P_O_SRTREK IN OUT varchar2
 ,P_NAAM OUT VARCHAR2
 ,P_MELDING IN OUT varchar2
 )
 IS
-- Sub-Program Unit Declarations
/* Ophalen van financieel nummer op rol soort en peildatum */
CURSOR C_FNR_002
 (B_SELROL IN VARCHAR2
 ,B_SELSRT IN VARCHAR2
 ,B_NUMRELATIE IN number
 ,B_CODROL IN varchar2
 ,B_SRTREK IN varchar2
 ,B_DATUM IN DATE
 )
 IS
/* C_FNR_002 */
select	fnr.numrekening
,	fnr.dwe_wrddom_srtrek
,	fnr.tenaamstelling
from	rle_financieel_nummers fnr
where	fnr.rle_numrelatie = b_numrelatie
and	nvl(fnr.rol_codrol,'@') = decode(b_selrol,'J',b_codrol,'@')
and	fnr.dwe_wrddom_srtrek = decode(b_selsrt
	,	'J'
	,	b_srtrek
	,	fnr.dwe_wrddom_srtrek)
and	b_datum between fnr.datingang
                and     nvl(dateinde,b_datum)
order by fnr.datingang desc;
-- Program Data
L_GEVONDEN VARCHAR2(1) := 'N';
-- Sub-Program Units
-- PL/SQL Block
BEGIN
/* RLE_M29_FINNUM */
l_gevonden	:= 'N';
p_numrekening	:= null;
p_o_srtrek	:= null;
p_melding	:= null;
if p_codrol is not NULL then
    if p_srtrek is not NULL
    then
 	--  tak 1:  rol en soort gevuld
	for r_fnr_002 in c_fnr_002('J','J'
		,	p_numrelatie
		,	p_codrol
		,	p_srtrek
		,	p_datum) loop
		p_numrekening := r_fnr_002.numrekening;
		p_naam := r_fnr_002.tenaamstelling;
		p_o_srtrek := r_fnr_002.dwe_wrddom_srtrek;
		l_gevonden := 'J';
	end loop;
    else
	--  tak 2:  rol gevuld en soort niet gevuld
	for r_fnr_002 in c_fnr_002('J','N'
		,	p_numrelatie
		,	p_codrol
		,	p_srtrek
		,	p_datum) loop
		p_numrekening := r_fnr_002.numrekening;
		p_naam := r_fnr_002.tenaamstelling;
		p_o_srtrek := r_fnr_002.dwe_wrddom_srtrek;
		l_gevonden := 'J';
	end loop;
     end if;
end if;
if   l_gevonden = 'N' then
   -- dit kan in twee gevallen: geen selektie op rol of wel selektie op
   -- rol maar geen financieelnr gevonden met de specifieke rol; in dat
   -- geval zoeken naar algemeen financieelnr.
   if p_srtrek is not null
   then
	--  tak 3:  rol niet gevuld en soort gevuld
	for r_fnr_002 in c_fnr_002('N','J'
		,	p_numrelatie
		,	p_codrol
		,	p_srtrek
		,	p_datum) loop
		p_numrekening := r_fnr_002.numrekening;
		p_naam := r_fnr_002.tenaamstelling;
		p_o_srtrek := r_fnr_002.dwe_wrddom_srtrek;
		l_gevonden := 'J';
	end loop;
    else
	--  tak 4:  rol en soort niet gevuld
	for r_fnr_002 in c_fnr_002('N','N'
		,	p_numrelatie
		,	p_codrol
		,	p_srtrek
		,	p_datum) loop
		p_numrekening := r_fnr_002.numrekening;
		p_naam := r_fnr_002.tenaamstelling;
		p_o_srtrek := r_fnr_002.dwe_wrddom_srtrek;
		l_gevonden := 'J';
	end loop;
    end if;
end if;
if  l_gevonden = 'N' then
    p_melding := 'Geen financieel nummer gevonden';
end if;
END RLE_M29_FINNUM;

 
/