CREATE OR REPLACE PROCEDURE comp_rle.RLE_M11_RLE_NAW
 (P_AANTPOS IN number
 ,P_RELNUM IN number
 ,P_O_OPMAAKNAAM IN OUT varchar2
 ,P_O_MELDING IN OUT varchar2
 ,P_COD_OPMAAK IN VARCHAR2 := '1'
 )
 IS

/* Ophalen van relaties op relatienummer */
CURSOR C_RLE_001
 (B_NUMRELATIE IN NUMBER
 )
 IS
/* C_RLE_001 */
--select	*
select numrelatie
,      codorganisatie
,      namrelatie
,      zoeknaam
,      indgewnaam
,      indinstelling
,      datschoning
,      dwe_wrddom_titel
,      dwe_wrddom_ttitel
,      dwe_wrddom_avgsl
,      dwe_wrddom_vvgsl
,      dwe_wrddom_brgst
,      dwe_wrddom_geslacht
,      dwe_wrddom_gewatitel
,      dwe_wrddom_rvorm
,      voornaam
,      voorletter
,      aanschrijfnaam
,      datgeboorte
,      code_gebdat_fictief
,      datoverlijden
,      datoprichting
,      datbegin
,      datcontrole
,      dateinde
,      indcontrole
,      regdat_overlbevest
,      datemigratie
,      naam_partner
,      vvgsl_partner
,      code_aanduiding_naamgebruik
,      handelsnaam
,      dat_creatie
,      creatie_door
,      dat_mutatie
,      mutatie_door
from	rle_relaties rle
where	rle.numrelatie = b_numrelatie;

L_PARTNER_FNAAM VARCHAR2(500);
L_EIGEN_FNAAM VARCHAR2(500);
L_PARTNER_NAAM VARCHAR2(184);
L_PARTNER_VOORLETTERS VARCHAR2(20);
L_PARTNER_VOORVOEGSEL VARCHAR2(40);
L_LENGTE_IBM NUMBER := 40;
L_IND_IBM VARCHAR2(1) := 'N';
L_EIGEN_VOORVOEGSEL VARCHAR2(40);
L_EIGEN_VOORLETTERS VARCHAR2(20);
L_EIGEN_NAAM VARCHAR2(184);
R_RLE_001 C_RLE_001%ROWTYPE;
L_COD_OPMAAK VARCHAR2(20);

/* Opzoeken partner van NUMRELATIE met een koppeling 'G', 'S', of 'H'. */
CURSOR C_RLE_022
 (B_NUMRELATIE RLE_RELATIES.NUMRELATIE%TYPE
 )
 IS
/* C_RLE_022 */
SELECT *
FROM   rle_relaties rle
WHERE  EXISTS( SELECT NULL
               FROM   rle_relatie_koppelingen rkn
               ,      rle_rol_in_koppelingen  rkg
               WHERE  rkn.rle_numrelatie        =  rle.numrelatie
               AND    rkg.id                    =  rkn.rkg_id
               AND    rkg.code_rol_in_koppeling IN ( 'S', 'H', 'P' )
             )
;

/* Gebruikt in RLE_M11_GET_NAW om de voorletters van een naam op te maken */
FUNCTION FNC_BEPAAL_VOORLETTERS
 (PIV_VOORLETTERS IN VARCHAR2
 )
 RETURN VARCHAR2;
/*******************************************************************************\
     RLE_M11_RLE_NAW - Ophalen opgemaakte naam

     Invoerparameters:
       p_relnum
       p_aantpos
       p_cod_opmaak

     Uitvoerparameters:
       p_o_opmaaknaam
       p_o_melding

     Werking:
       Zoek de naam, voorletters, voorvoegsel en code_gebruik_naam van de relatie
       op. Indien nodig (code_gebruik = "N", "P" of "V") zoek de naam, voorletters,
       voorvoegsel op van de relatie die een koppeling met code "H"uwlijk, "S"amen-
       wonend of "G"eregistreerd partnerschap heeft met de opgegeven relatie.

     Maak de namen als volgt op:
       p_cod_opmaak = 1 ---> Naam + ", "' + voorvoegsel + ", " + voorletters
       p_cod_opmaak = 2 ---> Voorletters + " " + voorvoegsel + " " + naam
       I.v.m. Geslachtsaanduiding:
       p_cod_opmaak = 8 ---> 'Dhr. ' of 'Mevr. ' + p_cod_opmaak = 1
       p_cod_opmaak = 9 ---> 'Dhr. ' of 'Mevr. ' + p_cod_opmaak = 2
       p_cod_opmaak = 0 ---> 'M' of 'V' + p_cod_opmaak = 1

     Maak de totale naam als volgt op:
       code_gebruik = "E" ---> Eigen naam
       code_gebruik = "P" ---> Partner naam
       code_gebruik = "V" ---> Partner naam + " - " + eigen naam
       code_gebruik = "N" ---> Eigen naam + " - " + partner naam


   \*******************************************************************************/


/* Gebruikt in RLE_M11_GET_NAW om de voorletters van een naam op te maken */
FUNCTION FNC_BEPAAL_VOORLETTERS
 (PIV_VOORLETTERS IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
BEGIN
      IF INSTR( piv_voorletters, '.' ) = 0  AND
         INSTR( piv_voorletters, ' ' ) = 0
      THEN
         -- Geen puntjes en spaties. Dus zelf puntjes toevoegen tussen alle letters.
         RETURN( LTRIM( SUBSTR( piv_voorletters,  1, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  2, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  3, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  4, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  5, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  6, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  7, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  8, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters,  9, 1 ) || '.', '.' ) ||
                 LTRIM( SUBSTR( piv_voorletters, 10, 1 ) || '.', '.' )
               ) ;
      ELSIF INSTR( piv_voorletters, '.' ) =  0  AND
            INSTR( piv_voorletters, ' ' ) != 0
      THEN
         -- Geen puntjes, wel spaties. Dus spaties vervangen door puntjes.
         RETURN( REPLACE( piv_voorletters, ' ', '.' ) ) ;
      ELSE
         -- Puntjes en spaties aanwezig. Gewoon zo laten.
         RETURN( piv_voorletters ) ;
      END IF ;
      RETURN( NULL ) ;
END;
BEGIN

   -- Schoonmaken uitvoerparameters
   p_o_opmaaknaam := NULL ;
   p_o_melding    := NULL ;

   -- Bepaal opmaak
   l_cod_opmaak := NVL( p_cod_opmaak, stm_algm_get.sysparm( NULL, 'VLGN', SYSDATE ) ) ;

   IF l_cod_opmaak IS NULL
   THEN
      stm_dbproc.raise_error( 'RLE-00501'
                            , 'onbekende naamvolgcode'
                            , 'rle_m11_rle_naw'
                            ) ;
      RETURN ;
   END IF ;

   -- Ophalen eigen naam
   OPEN c_rle_001( p_relnum ) ;
   FETCH c_rle_001 INTO r_rle_001 ;

   IF c_rle_001%NOTFOUND
   THEN
      CLOSE c_rle_001 ;
      p_o_melding := 'Relatienummer onbekend' ;
      RETURN ;
   END IF ;

   CLOSE c_rle_001 ;

   IF r_rle_001.indinstelling = 'J'
   THEN
      -- xcw: substring toegevoegd
      p_o_opmaaknaam := substr(r_rle_001.namrelatie,1,nvl(p_aantpos,50));
   ELSE
      -- formatteer voorletters (puntgescheiden haafdletters)
      l_eigen_voorletters := fnc_bepaal_voorletters( r_rle_001.voorletter ) ;

      -- Zoek voorvoegsel
      IF r_rle_001.dwe_wrddom_vvgsl IS NOT NULL
      THEN
         /* xcw: ibm weg
         ibm.ibm( 'RFW_GET_WRD'
                , 'RLE'
                , 'VVL'
                , r_rle_001.dwe_wrddom_vvgsl
                , l_ind_ibm
                , l_lengte_ibm
                , l_eigen_voorvoegsel
                ) ;  */
        rfe_rfw_get_wrd (p_ref_code => 'VVL'
                        ,p_waarde_kort => r_rle_001.dwe_wrddom_vvgsl
                        ,p_ind_check => l_ind_ibm
                        ,p_lengte => l_lengte_ibm
                        ,p_waarde => l_eigen_voorvoegsel
                        );
      END IF ;
      IF r_rle_001.code_aanduiding_naamgebruik in ('P','V','N')
      THEN
         l_partner_voorletters := NULL ;
         IF r_rle_001.vvgsl_partner IS NOT NULL
         THEN
            /* xcw: ibm weg
            ibm.ibm( 'RFW_GET_WRD'
                   , 'RLE'
                   , 'VVL'
                   , r_rle_001.vvgsl_partner
                   , l_ind_ibm
                   , l_lengte_ibm
                   , l_partner_voorvoegsel
                   ) ; */
             rfe_rfw_get_wrd (p_ref_code => 'VVL'
                             ,p_waarde_kort => r_rle_001.vvgsl_partner
                             ,p_ind_check => l_ind_ibm
                             ,p_lengte => l_lengte_ibm
                             ,p_waarde => l_partner_voorvoegsel
                             );
            --
            -- RTJ 26-11-2003 nav callnr 91690
            --
            IF l_partner_voorvoegsel IS NULL
            THEN
               l_partner_voorvoegsel := r_rle_001.vvgsl_partner;
            END IF;
            -- EINDE RTJ
         END IF ;
      END IF ;
      IF l_cod_opmaak in ('1', '8', '0')
      THEN
         l_eigen_fnaam := ltrim(rtrim(rtrim(r_rle_001.namrelatie)||' '||l_eigen_voorvoegsel));
         l_partner_fnaam := ltrim(rtrim(rtrim(r_rle_001.naam_partner)||' '||l_partner_voorvoegsel));
      ELSIF l_cod_opmaak in ('2', '9')
      THEN
         l_eigen_fnaam := ltrim(rtrim(rtrim(l_eigen_voorvoegsel)||' '||r_rle_001.namrelatie));
         l_partner_fnaam := ltrim(rtrim(rtrim(l_partner_voorvoegsel)||' '||r_rle_001.naam_partner));
      END IF;
      IF r_rle_001.code_aanduiding_naamgebruik = 'P'
      THEN
         p_o_opmaaknaam := l_partner_fnaam;
      ELSIF r_rle_001.code_aanduiding_naamgebruik = 'E'
      THEN
         p_o_opmaaknaam := l_eigen_fnaam;
      ELSIF r_rle_001.code_aanduiding_naamgebruik = 'V'
      THEN
         p_o_opmaaknaam := l_partner_fnaam||'-'||l_eigen_fnaam;
      ELSIF r_rle_001.code_aanduiding_naamgebruik = 'N'
      THEN
         p_o_opmaaknaam := l_eigen_fnaam||'-'||l_partner_fnaam;
      END IF;
      IF l_cod_opmaak in ('1', '8', '0')
      THEN
         p_o_opmaaknaam := p_o_opmaaknaam||', '||l_eigen_voorletters;
      ELSIF l_cod_opmaak in ('2', '9')
      THEN
         p_o_opmaaknaam := rtrim(l_eigen_voorletters)||' '||p_o_opmaaknaam;
      END IF;
      STM_UTIL.DEBUG('De nu (r 238) opgebouwde naam is: '
                    ||p_o_opmaaknaam
                    );
      -- Tbv geslachtaanduiding zijn nog 3 cod_opmaak's verzonnen: 0, 8 en 9.
      IF l_cod_opmaak in ('8', '9')
        THEN
        IF r_rle_001.dwe_wrddom_geslacht = 'V'
        THEN
          p_o_opmaaknaam := 'Mevr. '||p_o_opmaaknaam;
        ELSIF r_rle_001.dwe_wrddom_geslacht = 'M'
        THEN
          p_o_opmaaknaam := 'Dhr. '||p_o_opmaaknaam;
        END IF;
      ELSIF l_cod_opmaak = '0'
      THEN
        p_o_opmaaknaam := SUBSTR( NVL(SUBSTR(r_rle_001.dwe_wrddom_geslacht,1 ,1),'?')
                                ||p_o_opmaaknaam
                                , 1, 100);
      END IF;
      STM_UTIL.DEBUG('De nu (r 266) opgebouwde naam is: '
                    ||p_o_opmaaknaam
                    );
   END IF;
END;
 
/