CREATE OR REPLACE procedure comp_rle.rle_mut_werknemer_beroepen
 (p_avg_id in number
 ,p_dat_begin in date
 ,p_dat_einde in date
 ,p_code_beroep in varchar2
 ,p_succes out boolean
 ,p_errorcode out varchar2
 )
 is
l_errorcode varchar2(2000);
begin
  -- doe de verwerking o.b.v. ingekomen parameters
  rle_rie.kop_avg_avg(p_avg_id      => p_avg_id
		     ,p_dat_begin   => p_dat_begin
		     ,p_dat_einde   => p_dat_einde
                     ,p_code_beroep => p_code_beroep
                     ,p_error       => l_errorcode
                     );
  --
  if l_errorcode is null
  then
     p_succes := true;
  else
    p_errorcode := l_errorcode;
    p_succes := false;
  end if;
exception
  when others
  then
    p_errorcode := sqlerrm;
    p_succes    := false;
end rle_mut_werknemer_beroepen;
 
/