CREATE OR REPLACE PROCEDURE comp_rle.RLE_GBA_VERWIJDER_VERWANT
 (P_RKN_ID IN NUMBER
 ,P_NUMRELATIE IN NUMBER
 ,P_NUMRELATIE_VOOR IN NUMBER
 ,P_DAT_BEGIN IN DATE
 ,P_ROL IN NUMBER
 )
 IS
-- PL/SQL Specification
type rec_verwantschap is record (persoon1 number,persoon2 number,dat_begin date,rol varchar2(10));
r_rkn rec_verwantschap;
-- PL/SQL Block
begin
 select rec.extern_relatie   persoon1
,      rec1.extern_relatie  persoon2
,      p_dat_begin
,      rkg.code_rol_in_koppeling rol
into r_rkn
from rle_relatie_externe_codes rec1
,    rle_relatie_externe_codes rec
,    rle_rol_in_koppelingen    rkg
--,    rle_relatie_koppelingen   rkn
where /*rkn.id = p_rkn_id
and */  rec.rle_numrelatie  = p_numrelatie
and   rec.dwe_wrddom_extsys = 'PRS'
and   rec1.rle_numrelatie = p_numrelatie_voor
and   rec1.dwe_wrddom_extsys = 'PRS'
and   rkg.id = p_rol
;
stm_util.debug('synchroniseren met component GBA');
 gba_verwijder_verwant(r_rkn.persoon1
                      ,r_rkn.persoon2
                      ,r_rkn.dat_begin
                      ,r_rkn.rol);
exception
 when no_data_found
 then
    raise_application_error(-20000,'Geen persoonsnummer gevonden bij relatie. Kan verwantschap niet verwijderen in component GBA');
end;

 
/