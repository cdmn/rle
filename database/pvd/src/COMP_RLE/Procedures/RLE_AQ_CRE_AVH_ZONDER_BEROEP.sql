CREATE OR REPLACE PROCEDURE comp_rle.RLE_AQ_CRE_AVH_ZONDER_BEROEP
 IS
  l_consumer_name varchar2 ( 27 ) := 'RLE_CRE_AVH_ZONDER_BEROEP';
  l_message_handle raw ( 16 );
  l_event_name varchar2 ( 23 );
  l_params varchar2 ( 4000 );
  l_params_save varchar2 ( 4000 );
  l_succes boolean;
  l_errorcode varchar2 ( 2000 );
  l_looping boolean := true;
  l_publisher varchar2 ( 100 );
  l_soort_mutatie VARCHAR2 ( 100 );
  l_dummy_var VARCHAR2(100);
  l_datum_event VARCHAR2(100);
  l_avg_id VARCHAR2(100);
  l_dat_begin VARCHAR2(100);
  l_dat_einde VARCHAR2(100);
  l_code_beroep  VARCHAR2(100);
  l_otherinfo varchar2( 250 )
    := 'Advanced Queue listener comp_rle.rle_aq_cre_avh_zonder_beroep is gestopt. Deze moet opnieuw worden gestart zodra het fout gegane bericht verwerkt kan worden.';
begin
  while l_looping
  loop
    -- Lezen bericht voor abonnementhouder ret_aq_overlijden_relatie
    -- Indien geen bericht klaar staat, en ook geen stop_queue bericht
    -- dan wordt gewacht op het eerstvolgende bericht.
    stm_aq.dequeue( p_consumer_name  => l_consumer_name
                  , p_dequeue_mode   => 'REMOVE'
                  , p_navigation     => 'FIRST_MESSAGE'
                  , p_message_handle => l_message_handle
                  , p_event_name     => l_event_name
                  , p_params         => l_params
                  , p_succes         => l_succes
                  , p_errorcode      => l_errorcode
                  );

    if l_succes
    then
       l_params_save := l_params;

       -- Lees de parameters en roep de verwerkende module aan.
        l_soort_mutatie := stm_get_word ( l_params ); --1
        l_datum_event   := stm_get_word ( l_params ); --2
        l_dummy_var     := stm_get_word ( l_params ); --3
        l_dummy_var     := stm_get_word ( l_params ); --4
        l_dummy_var     := stm_get_word ( l_params ); --5
        l_avg_id        := stm_get_word ( l_params ); --6
        l_dummy_var     := stm_get_word ( l_params ); --7
        l_dat_begin     := stm_get_word ( l_params ); --8
        l_dummy_var     := stm_get_word ( l_params ); --9
        l_code_beroep   := stm_get_word ( l_params ); --10
        l_dummy_var     := stm_get_word ( l_params ); --11
        l_dat_einde     := stm_get_word ( l_params ); --12
        l_dummy_var     := stm_get_word ( l_params ); --13
        l_dummy_var     := stm_get_word ( l_params ); --14
        l_dummy_var     := stm_get_word ( l_params ); --15
        l_dummy_var     := stm_get_word ( l_params ); --16
        l_dummy_var     := stm_get_word ( l_params ); --17
        l_dummy_var     := stm_get_word ( l_params ); --18
        l_dummy_var     := stm_get_word ( l_params ); --19
        l_dummy_var     := stm_get_word ( l_params ); --20
        l_dummy_var     := stm_get_word ( l_params ); --21
        l_dummy_var     := stm_get_word ( l_params ); --22
        --
        IF l_soort_mutatie = 'I'
        THEN
	   --
           rle_cre_avh_zonder_beroep( p_avg_id             => to_number(l_avg_id)
				                    , p_dat_begin          => to_date(l_dat_begin, 'dd-mm-yyyy hh24:mi:ss')
		   		                    , p_dat_einde          => to_date(l_dat_einde, 'dd-mm-yyyy hh24:mi:ss')
	                                , p_succes             => l_succes
                                    , p_errorcode          => l_errorcode
                                    );
       end if;
       --
    end if;
    --
    if l_succes
    then
      commit;
    else
      -- Verwerking fout gegaan OF lezen queue fout gegaan.
      -- Doe een rollback en roep de standaard error handler aan
      -- voor achtergrondprocessen.
      -- Na een fout moet de listener altijd worden beeindigd!!
      rollback;
      stm_bgproc.error_handler ( p_type_verwerking                    => stm_bgproc.c_advanced_queuing
                               , p_procedure                          => 'RLE_AQ_CRE_AVH_ZONDER_BEROEP'
                               , p_errormsg                           => l_errorcode
                               , p_params                             => l_params_save
                               , p_otherinfo                          => l_otherinfo
                               , p_raise_exception                    => false
                               );
      l_looping := false;
    end if;
  end loop;
exception
  --
  -- Stop alleen een exception handler in de listener en niet in de verwerkende procedure(s).
  -- In geval van een onverwachte fout zal stm_bgproc.error_handler de backtrace afdrukken
  -- en die ben je kwijt na een doorlopen exception.
  -- Dat betekent verlies van nuttige informatie over de echte fout.
  when others
  then
    rollback;
    stm_bgproc.error_handler ( p_type_verwerking                      => stm_bgproc.c_advanced_queuing
                             , p_procedure                            => 'RLE_AQ_CRE_AVH_ZONDER_BEROEP'
                             , p_errormsg                             => sqlerrm
                             , p_params                               => l_params_save
                             , p_otherinfo                            => l_otherinfo
                             , p_raise_exception                      => false
                             );
END RLE_AQ_CRE_AVH_ZONDER_BEROEP;
 
/