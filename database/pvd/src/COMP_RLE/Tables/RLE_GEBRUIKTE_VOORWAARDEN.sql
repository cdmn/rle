CREATE TABLE comp_rle.rle_gebruikte_voorwaarden (
  cde_id NUMBER(10) NOT NULL,
  sve_id NUMBER(10) NOT NULL,
  regelnummer NUMBER(*,0) NOT NULL,
  var1_waarde VARCHAR2(4000 BYTE),
  var2_waarde VARCHAR2(4000 BYTE),
  var3_waarde VARCHAR2(4000 BYTE),
  var4_waarde VARCHAR2(4000 BYTE),
  var5_waarde VARCHAR2(4000 BYTE),
  ind_geblokkeerd VARCHAR2(1 BYTE) NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  CONSTRAINT rle_gve_pk PRIMARY KEY (cde_id,sve_id,regelnummer),
  CONSTRAINT rle_gve_cde_fk FOREIGN KEY (cde_id) REFERENCES comp_rle.rle_campagne_definities ("ID"),
  CONSTRAINT rle_gve_sve_fk FOREIGN KEY (sve_id) REFERENCES comp_rle.rle_standaard_voorwaarden ("ID")
);