CREATE TABLE comp_rle.rle_communicatie_nummers (
  "ID" NUMBER(9) NOT NULL,
  numcommunicatie VARCHAR2(50 BYTE) NOT NULL,
  rle_numrelatie NUMBER(9) NOT NULL,
  dwe_wrddom_srtcom VARCHAR2(30 BYTE) NOT NULL CONSTRAINT rle_cnr_ck5 CHECK (/* QMS$ENFORCE_UPPER_CASE_DWE_WRDDOM_SRTCOM */
DWE_WRDDOM_SRTCOM = UPPER(DWE_WRDDOM_SRTCOM)),
  codorganisatie VARCHAR2(4 BYTE) NOT NULL CONSTRAINT rle_cnr_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODORGANISATIE */
CODORGANISATIE = UPPER(CODORGANISATIE)),
  datingang DATE NOT NULL,
  indinhown VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL CONSTRAINT rle_cnr_ck4 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDINHOWN */
INDINHOWN = UPPER(INDINHOWN)),
  dateinde DATE,
  rol_codrol VARCHAR2(2 BYTE) CONSTRAINT rle_cnr_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_ROL_CODROL */
ROL_CODROL = UPPER(ROL_CODROL)),
  ras_id NUMBER(9),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  adm_code VARCHAR2(12 BYTE) DEFAULT 'ALG' NOT NULL,
  authentiek VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL,
  CONSTRAINT rle_cnr_ck1 CHECK (dateinde is null
OR
(
dateinde IS NOT NULL
AND datingang IS NOT NULL
AND TRUNC(dateinde) >= TRUNC(datingang)
)),
  CONSTRAINT rle_cnr_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_cnr_uk1 UNIQUE (rle_numrelatie,adm_code,dwe_wrddom_srtcom,numcommunicatie,datingang,ras_id),
  CONSTRAINT rle_cnr_ras_fk1 FOREIGN KEY (ras_id) REFERENCES comp_rle.rle_relatie_adressen ("ID"),
  CONSTRAINT rle_cnr_rie_fk1 FOREIGN KEY (rle_numrelatie,rol_codrol,adm_code) REFERENCES comp_rle.rle_relatierollen_in_adm (rle_numrelatie,rol_codrol,adm_code),
  CONSTRAINT rle_cnr_rle_fk1 FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie),
  CONSTRAINT rle_cnr_rol_fk1 FOREIGN KEY (rol_codrol) REFERENCES comp_rle.rle_rollen (codrol)
);
COMMENT ON TABLE comp_rle.rle_communicatie_nummers IS 'Communicatienummers';