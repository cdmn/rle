CREATE TABLE comp_rle.rle_ned_postcode (
  postcode VARCHAR2(15 BYTE) NOT NULL CONSTRAINT rle_npe_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_POSTCODE */
POSTCODE = UPPER(POSTCODE)),
  codreeks NUMBER(1) NOT NULL,
  numscheidingvan NUMBER(5) NOT NULL,
  numscheidingtm NUMBER(5) NOT NULL,
  codorganisatie VARCHAR2(4 BYTE) NOT NULL CONSTRAINT rle_npe_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODORGANISATIE */
CODORGANISATIE = UPPER(CODORGANISATIE)),
  indpostchand VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL CONSTRAINT rle_npe_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDPOSTCHAND */
INDPOSTCHAND = UPPER(INDPOSTCHAND)),
  woonplaats VARCHAR2(30 BYTE),
  straatnaam VARCHAR2(30 BYTE),
  gemeentenaam VARCHAR2(30 BYTE),
  gemeentenummer NUMBER(4),
  codprovincie VARCHAR2(3 BYTE) CONSTRAINT rle_npe_ck4 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODPROVINCIE */
CODPROVINCIE = UPPER(CODPROVINCIE)),
  codcebuco NUMBER(3),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_npe_pk PRIMARY KEY (postcode,codreeks,numscheidingvan,numscheidingtm)
);
COMMENT ON TABLE comp_rle.rle_ned_postcode IS 'Nederlandse postcodes';