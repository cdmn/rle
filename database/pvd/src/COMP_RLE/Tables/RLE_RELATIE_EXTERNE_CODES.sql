CREATE TABLE comp_rle.rle_relatie_externe_codes (
  dat_mutatie DATE NOT NULL,
  "ID" NUMBER(9) NOT NULL,
  rle_numrelatie NUMBER(9) NOT NULL,
  dwe_wrddom_extsys VARCHAR2(30 BYTE) NOT NULL,
  datingang DATE NOT NULL,
  codorganisatie VARCHAR2(4 BYTE) NOT NULL CONSTRAINT rle_rec_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODORGANISATIE */
CODORGANISATIE = UPPER(CODORGANISATIE)),
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  rol_codrol VARCHAR2(2 BYTE) CONSTRAINT rle_rec_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_ROL_CODROL */
ROL_CODROL = UPPER(ROL_CODROL)),
  extern_relatie VARCHAR2(20 BYTE),
  ind_geverifieerd VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL,
  dateinde DATE,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_rec_ck1 CHECK (dateinde is null
OR
(
dateinde IS NOT NULL
AND datingang IS NOT NULL
AND TRUNC(dateinde) >= TRUNC(datingang)
)),
  CONSTRAINT rle_rec_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_rec_rle_fk1 FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie),
  CONSTRAINT rle_rec_rol_fk1 FOREIGN KEY (rol_codrol) REFERENCES comp_rle.rle_rollen (codrol)
);
COMMENT ON TABLE comp_rle.rle_relatie_externe_codes IS 'Externe codes van de Relatie';