CREATE TABLE comp_rle.rle_gbav_fout (
  intern_kenmerk NUMBER,
  resultaat VARCHAR2(2000 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline CHARACTERSET AL32UTF8 STRING SIZES ARE IN CHARACTERS STRING SIZES ARE IN CHARACTERS
          BADFILE RLE_INBOX:'rle_gbav_fout.bad'
          DISCARDFILE RLE_INBOX:'rle_gbav_fout.dsc'
          LOGFILE RLE_INBOX:'rle_gbav_fout.log'
          fields terminated by ';'
            missing field values are null
)
LOCATION ('gbav_fout'))
REJECT LIMIT UNLIMITED;