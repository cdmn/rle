CREATE TABLE comp_rle.rle_gba_huwelijken_tnt (
  "ID" NUMBER NOT NULL,
  bsn_persoon VARCHAR2(20 BYTE) NOT NULL,
  a_nummer VARCHAR2(20 BYTE),
  bsn VARCHAR2(20 BYTE),
  voornamen VARCHAR2(200 BYTE),
  voorvoegsels_geslachtsnaam VARCHAR2(200 BYTE),
  geslachtsnaam VARCHAR2(200 BYTE),
  geboortedatum VARCHAR2(200 BYTE),
  datum_sluiting VARCHAR2(200 BYTE),
  datum_ontbinding VARCHAR2(200 BYTE),
  reden_ontbinding VARCHAR2(200 BYTE),
  opmerkingen VARCHAR2(1000 BYTE),
  dat_creatie DATE NOT NULL,
  CONSTRAINT rle_tnt_pk PRIMARY KEY ("ID")
);