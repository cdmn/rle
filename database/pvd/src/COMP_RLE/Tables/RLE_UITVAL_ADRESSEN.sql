CREATE TABLE comp_rle.rle_uitval_adressen (
  persoonsnummer VARCHAR2(9 BYTE),
  werkgevernummer VARCHAR2(9 BYTE),
  straatnaam VARCHAR2(100 BYTE),
  postcode VARCHAR2(15 BYTE),
  huisnummer NUMBER(10),
  huisnr_toev VARCHAR2(15 BYTE),
  woonplaats VARCHAR2(40 BYTE),
  ind_buitenland VARCHAR2(2 BYTE) DEFAULT 'N',
  soort_adres VARCHAR2(240 BYTE),
  landcode_bui VARCHAR2(5 BYTE),
  aktnummer VARCHAR2(6 BYTE)
);