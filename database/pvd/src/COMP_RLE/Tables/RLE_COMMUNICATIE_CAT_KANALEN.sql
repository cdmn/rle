CREATE TABLE comp_rle.rle_communicatie_cat_kanalen (
  "ID" NUMBER NOT NULL,
  ccn_id NUMBER NOT NULL,
  dwe_wrddom_srtcom VARCHAR2(40 BYTE) NOT NULL,
  datum_begin DATE NOT NULL,
  datum_einde DATE,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  ind_default VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL,
  CONSTRAINT rle_cck_ck1 CHECK (datum_einde is null
OR
(
datum_einde IS NOT NULL
AND datum_begin IS NOT NULL
AND TRUNC(datum_einde) >= TRUNC(datum_begin)
)),
  CONSTRAINT rle_cck_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_cck_uk1 UNIQUE (ccn_id,dwe_wrddom_srtcom,datum_begin),
  CONSTRAINT rle_cck_ccn_fk1 FOREIGN KEY (ccn_id) REFERENCES comp_rle.rle_communicatie_categorieen ("ID")
);