CREATE TABLE comp_rle.rle_relatie_koppelingen_jn (
  jn_user VARCHAR2(30 BYTE) NOT NULL,
  jn_date_time DATE NOT NULL,
  jn_operation VARCHAR2(3 BYTE) NOT NULL,
  "ID" NUMBER(9) NOT NULL,
  rkg_id NUMBER(9),
  rle_numrelatie NUMBER(9),
  rle_numrelatie_voor NUMBER(9),
  dat_begin DATE,
  code_dat_begin_fictief VARCHAR2(1 BYTE),
  dat_einde DATE,
  code_dat_einde_fictief VARCHAR2(1 BYTE),
  creatie_door VARCHAR2(30 BYTE),
  dat_creatie DATE,
  mutatie_door VARCHAR2(30 BYTE),
  dat_mutatie DATE
);