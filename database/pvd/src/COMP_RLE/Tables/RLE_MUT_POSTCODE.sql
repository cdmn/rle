CREATE TABLE comp_rle.rle_mut_postcode (
  pbn_id NUMBER(9) NOT NULL,
  mutsrt VARCHAR2(1 BYTE) NOT NULL,
  ind_mut_postcode_dl1 NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_postcode_dl2 NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_reekscod NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_numscheidingvan NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_numscheidingtm NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_woonplaats_ptt NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_woonplaats_nen NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_straatnaam_ptt NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_straatnaam_nen NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_straatnaam_off NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_gemeentenaam NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_gemeentenummer NUMBER(1),
  ind_mut_codprov NUMBER(1) DEFAULT 0 NOT NULL,
  ind_mut_codcebuco NUMBER(1) DEFAULT 0 NOT NULL,
  postcode_oud VARCHAR2(15 BYTE) CONSTRAINT rle_mpe_ck10 CHECK (/* QMS$ENFORCE_UPPER_CASE_POSTCODE_OUD */
POSTCODE_OUD = UPPER(POSTCODE_OUD)),
  reekscod_oud VARCHAR2(1 BYTE) CONSTRAINT rle_mpe_ck11 CHECK (/* QMS$ENFORCE_UPPER_CASE_REEKSCOD_OUD */
REEKSCOD_OUD = UPPER(REEKSCOD_OUD)),
  numscheidingvan_oud NUMBER(5),
  numscheidingtm_oud NUMBER(5),
  woonplaats_ptt_oud VARCHAR2(18 BYTE),
  woonplaats_nen_oud VARCHAR2(24 BYTE),
  straatnaam_ptt_oud VARCHAR2(17 BYTE),
  straatnaam_nen_oud VARCHAR2(24 BYTE),
  straatnaam_off_oud VARCHAR2(43 BYTE),
  gemeentenaam_oud VARCHAR2(24 BYTE),
  gemeentenummer_oud NUMBER(4),
  codprov_oud VARCHAR2(1 BYTE) CONSTRAINT rle_mpe_ck12 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODPROV_OUD */
CODPROV_OUD = UPPER(CODPROV_OUD)),
  codcebuco_oud NUMBER(3),
  postcode_nw VARCHAR2(15 BYTE) CONSTRAINT rle_mpe_ck13 CHECK (/* QMS$ENFORCE_UPPER_CASE_POSTCODE_NW */
POSTCODE_NW = UPPER(POSTCODE_NW)),
  reekscod_nw VARCHAR2(1 BYTE) CONSTRAINT rle_mpe_ck19 CHECK (/* QMS$ENFORCE_UPPER_CASE_REEKSCOD_NW */
REEKSCOD_NW = UPPER(REEKSCOD_NW)),
  numscheidingvan_nw NUMBER(5),
  numscheidingtm_nw NUMBER(5),
  woonplaats_ptt_nw VARCHAR2(18 BYTE),
  woonplaats_nen_nw VARCHAR2(24 BYTE),
  straatnaam_ptt_nw VARCHAR2(17 BYTE),
  straatnaam_nen_nw VARCHAR2(24 BYTE),
  straatnaam_off_nw VARCHAR2(43 BYTE),
  gemeentenaam_nw VARCHAR2(24 BYTE),
  gemeentenummer_nw NUMBER(4),
  codprov_nw VARCHAR2(1 BYTE) CONSTRAINT rle_mpe_ck14 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODPROV_NW */
CODPROV_NW = UPPER(CODPROV_NW)),
  codcebuco_nw NUMBER(3),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  volgnr NUMBER(4),
  CONSTRAINT rle_mpe_pbn_fk1 FOREIGN KEY (pbn_id) REFERENCES comp_rle.rle_pcod_batchrun ("ID") ON DELETE CASCADE
);
COMMENT ON TABLE comp_rle.rle_mut_postcode IS 'Mutatietape Postcode';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.mutsrt IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_postcode_dl1 IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_postcode_dl2 IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_reekscod IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_numscheidingvan IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_numscheidingtm IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_woonplaats_ptt IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_woonplaats_nen IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_straatnaam_ptt IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_straatnaam_nen IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_straatnaam_off IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_gemeentenaam IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_codprov IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.ind_mut_codcebuco IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.postcode_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.reekscod_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.numscheidingvan_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.numscheidingtm_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.woonplaats_ptt_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.woonplaats_nen_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.straatnaam_ptt_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.straatnaam_nen_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.straatnaam_off_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.gemeentenaam_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.codprov_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.codcebuco_oud IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.postcode_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.reekscod_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.numscheidingvan_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.numscheidingtm_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.woonplaats_ptt_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.woonplaats_nen_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.straatnaam_ptt_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.straatnaam_nen_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.straatnaam_off_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.gemeentenaam_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.codprov_nw IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_mut_postcode.codcebuco_nw IS '- Column already exists';