CREATE GLOBAL TEMPORARY TABLE comp_rle.rle_tmp_machtigingen (
  werkgever VARCHAR2(6 BYTE) NOT NULL,
  gemachtigd_bedrijf VARCHAR2(6 BYTE) NOT NULL,
  rle_dat_begin DATE,
  mnonline VARCHAR2(3 BYTE) NOT NULL
)
ON COMMIT DELETE ROWS;