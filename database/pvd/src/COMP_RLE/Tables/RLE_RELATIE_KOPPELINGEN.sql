CREATE TABLE comp_rle.rle_relatie_koppelingen (
  "ID" NUMBER(9) NOT NULL,
  rkg_id NUMBER(9) NOT NULL,
  rle_numrelatie NUMBER(9) NOT NULL,
  rle_numrelatie_voor NUMBER(9) NOT NULL,
  dat_begin DATE NOT NULL,
  code_dat_begin_fictief VARCHAR2(1 BYTE),
  dat_einde DATE,
  code_dat_einde_fictief VARCHAR2(1 BYTE),
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  CONSTRAINT rle_rkn_ck1 CHECK (dat_einde is null
OR
(
dat_einde IS NOT NULL
AND dat_begin IS NOT NULL
AND TRUNC(dat_einde) >= TRUNC(dat_begin)
)),
  CONSTRAINT rle_rkn_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_rkn_uk1 UNIQUE (rle_numrelatie,rle_numrelatie_voor,dat_begin,rkg_id),
  CONSTRAINT rle_rkn_rkg_fk1 FOREIGN KEY (rkg_id) REFERENCES comp_rle.rle_rol_in_koppelingen ("ID"),
  CONSTRAINT rle_rkn_rle_fk1 FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie),
  CONSTRAINT rle_rkn_rle_fk2 FOREIGN KEY (rle_numrelatie_voor) REFERENCES comp_rle.rle_relaties (numrelatie)
);