CREATE TABLE comp_rle.rle_ref_codes (
  rv_domain VARCHAR2(100 BYTE) NOT NULL,
  rv_low_value VARCHAR2(240 BYTE) NOT NULL,
  rv_high_value VARCHAR2(240 BYTE),
  rv_abbreviation VARCHAR2(240 BYTE),
  rv_meaning VARCHAR2(240 BYTE)
);