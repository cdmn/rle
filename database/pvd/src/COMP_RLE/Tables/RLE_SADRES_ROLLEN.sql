CREATE TABLE comp_rle.rle_sadres_rollen (
  codrol VARCHAR2(2 BYTE) NOT NULL CONSTRAINT rle_sar_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODROL */
CODROL = UPPER(CODROL)),
  volgnr NUMBER(2) NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  dwe_wrddom_sadres VARCHAR2(30 BYTE),
  dat_mutatie DATE NOT NULL,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_sar_pk PRIMARY KEY (codrol,volgnr),
  CONSTRAINT rle_sar_uk1 UNIQUE (codrol,dwe_wrddom_sadres),
  CONSTRAINT rle_sar_rol_fk1 FOREIGN KEY (codrol) REFERENCES comp_rle.rle_rollen (codrol) ON DELETE CASCADE
);
COMMENT ON TABLE comp_rle.rle_sadres_rollen IS 'Soort adressen bij een Rol';