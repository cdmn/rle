CREATE GLOBAL TEMPORARY TABLE comp_rle.rle_tmp_actieve_wgr (
  werkgevernummer VARCHAR2(6 BYTE) NOT NULL,
  code_functiecategorie VARCHAR2(2 BYTE) NOT NULL,
  dataanv DATE NOT NULL,
  dateind DATE
)
ON COMMIT DELETE ROWS;