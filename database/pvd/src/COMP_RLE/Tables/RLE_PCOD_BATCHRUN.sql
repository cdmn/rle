CREATE TABLE comp_rle.rle_pcod_batchrun (
  "ID" NUMBER(9) NOT NULL,
  draaimaand VARCHAR2(7 BYTE) DEFAULT TO_CHAR( SYSDATE, 'YYYY-MM') NOT NULL,
  subtype VARCHAR2(1 BYTE) DEFAULT 'M' NOT NULL,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_pbn_pk PRIMARY KEY ("ID")
);
COMMENT ON TABLE comp_rle.rle_pcod_batchrun IS 'RLE_PCOD_BATCHRUN';