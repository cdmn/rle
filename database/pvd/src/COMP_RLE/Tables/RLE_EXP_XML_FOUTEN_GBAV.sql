CREATE TABLE comp_rle.rle_exp_xml_fouten_gbav (
  filename VARCHAR2(50 BYTE),
  insertdate DATE,
  "ID" VARCHAR2(20 BYTE) NOT NULL,
  bsn VARCHAR2(10 BYTE),
  datum_verwerkt DATE,
  foutmelding VARCHAR2(4000 BYTE)
);