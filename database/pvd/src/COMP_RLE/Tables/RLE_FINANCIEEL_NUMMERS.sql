CREATE TABLE comp_rle.rle_financieel_nummers (
  dat_mutatie DATE NOT NULL,
  rle_numrelatie NUMBER(9) NOT NULL,
  numrekening VARCHAR2(34 BYTE) NOT NULL,
  lnd_codland VARCHAR2(3 BYTE) DEFAULT 'NL' NOT NULL CONSTRAINT rle_fnr_ck5 CHECK (/* QMS$ENFORCE_UPPER_CASE_LND_CODLAND */
LND_CODLAND = UPPER(LND_CODLAND)),
  dwe_wrddom_srtrek VARCHAR2(30 BYTE) NOT NULL CONSTRAINT rle_fnr_ck6 CHECK (/* QMS$ENFORCE_UPPER_CASE_DWE_WRDDOM_SRTREK */
DWE_WRDDOM_SRTREK = UPPER(DWE_WRDDOM_SRTREK)),
  datingang DATE NOT NULL,
  indinhown VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL CONSTRAINT rle_fnr_ck7 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDINHOWN */
INDINHOWN = UPPER(INDINHOWN)),
  dateinde DATE,
  indfiat VARCHAR2(1 BYTE) DEFAULT 'J' NOT NULL CONSTRAINT rle_fnr_ck4 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDFIAT */
INDFIAT = UPPER(INDFIAT)),
  rol_codrol VARCHAR2(2 BYTE) CONSTRAINT rle_fnr_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_ROL_CODROL */
ROL_CODROL = UPPER(ROL_CODROL)),
  codorganisatie VARCHAR2(4 BYTE) NOT NULL CONSTRAINT rle_fnr_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODORGANISATIE */
CODORGANISATIE = UPPER(CODORGANISATIE)),
  tenaamstelling VARCHAR2(80 BYTE),
  coddoelrek VARCHAR2(10 BYTE) NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  codwijzebetaling VARCHAR2(2 BYTE),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  adm_code VARCHAR2(12 BYTE) DEFAULT 'ALG' NOT NULL,
  bic VARCHAR2(11 BYTE),
  iban VARCHAR2(34 BYTE),
  "ID" NUMBER NOT NULL,
  CONSTRAINT rle_fnr_ck1 CHECK (dateinde is null
OR
(
dateinde IS NOT NULL
AND datingang IS NOT NULL
AND TRUNC(dateinde) >= TRUNC(datingang)
)),
  CONSTRAINT rle_fnr_ck10 CHECK ((BIC is not null
and IBAN is not null) or (BIC is null and IBAN is null)),
  CONSTRAINT rle_fnr_ck9 CHECK (codwijzebetaling != 'AS' or (BIC is not null and IBAN is not null)),
  CONSTRAINT rle_fnr_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_fnr_uk1 UNIQUE (rle_numrelatie,iban,numrekening,datingang,adm_code,dwe_wrddom_srtrek,coddoelrek),
  CONSTRAINT rle_fnr_uk2 UNIQUE (datingang,numrekening,rle_numrelatie,dwe_wrddom_srtrek,coddoelrek),
  CONSTRAINT rle_fnr_lnd_fk1 FOREIGN KEY (lnd_codland) REFERENCES comp_rle.rle_landen (codland),
  CONSTRAINT rle_fnr_rie_fk1 FOREIGN KEY (rle_numrelatie,rol_codrol,adm_code) REFERENCES comp_rle.rle_relatierollen_in_adm (rle_numrelatie,rol_codrol,adm_code),
  CONSTRAINT rle_fnr_rle_fk1 FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie),
  CONSTRAINT rle_fnr_rol_fk1 FOREIGN KEY (rol_codrol) REFERENCES comp_rle.rle_rollen (codrol)
);
COMMENT ON TABLE comp_rle.rle_financieel_nummers IS 'Rekeningen';