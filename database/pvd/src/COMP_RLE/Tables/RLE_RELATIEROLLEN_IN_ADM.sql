CREATE TABLE comp_rle.rle_relatierollen_in_adm (
  adm_code VARCHAR2(12 BYTE) NOT NULL,
  rol_codrol VARCHAR2(2 BYTE) NOT NULL,
  rle_numrelatie NUMBER(9) NOT NULL,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_rie_pk PRIMARY KEY (rle_numrelatie,rol_codrol,adm_code),
  CONSTRAINT rle_rie_adm_fk FOREIGN KEY (adm_code) REFERENCES comp_rle.rle_administraties (code),
  CONSTRAINT rle_rie_rrl_fk FOREIGN KEY (rle_numrelatie,rol_codrol) REFERENCES comp_rle.rle_relatie_rollen (rle_numrelatie,rol_codrol)
);