CREATE TABLE comp_rle.rle_gbav_meer (
  intern_kenmerk NUMBER,
  anummer VARCHAR2(10 BYTE),
  bsn VARCHAR2(10 BYTE),
  voornamen VARCHAR2(500 BYTE),
  voorvoegsel VARCHAR2(20 BYTE),
  naam VARCHAR2(2000 BYTE),
  geboortedatum VARCHAR2(15 BYTE),
  geslacht VARCHAR2(15 BYTE),
  naamgebruik VARCHAR2(15 BYTE),
  overlijdensdatum VARCHAR2(15 BYTE),
  gemeentenummer VARCHAR2(15 BYTE),
  gemeentedeel VARCHAR2(200 BYTE),
  straatnaam VARCHAR2(200 BYTE),
  huisnummer VARCHAR2(15 BYTE),
  huisletter VARCHAR2(15 BYTE),
  toevoeging VARCHAR2(15 BYTE),
  postcode VARCHAR2(15 BYTE),
  landcode VARCHAR2(15 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline CHARACTERSET AL32UTF8 STRING SIZES ARE IN CHARACTERS STRING SIZES ARE IN CHARACTERS
          BADFILE RLE_INBOX:'rle_gbav_meer.bad'
          DISCARDFILE RLE_INBOX:'rle_gbav_meer.dsc'
          LOGFILE RLE_INBOX:'rle_gbav_meer.log'
          fields terminated by ';'
             missing field values are null
)
LOCATION ('gbav_meer'))
REJECT LIMIT UNLIMITED;