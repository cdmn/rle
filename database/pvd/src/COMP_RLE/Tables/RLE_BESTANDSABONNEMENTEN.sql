CREATE TABLE comp_rle.rle_bestandsabonnementen (
  bsd_naam VARCHAR2(30 BYTE) NOT NULL,
  code_abonnee VARCHAR2(30 BYTE) NOT NULL,
  dat_begin DATE NOT NULL,
  dat_einde DATE,
  CONSTRAINT rle_bst_pk PRIMARY KEY (dat_begin,code_abonnee,bsd_naam),
  CONSTRAINT rle_bst_bsd_fk FOREIGN KEY (bsd_naam) REFERENCES comp_rle.rle_bestanden (naam)
);