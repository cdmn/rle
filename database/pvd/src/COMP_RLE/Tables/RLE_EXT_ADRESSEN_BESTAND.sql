CREATE TABLE comp_rle.rle_ext_adressen_bestand (
  rol_codrol VARCHAR2(2 BYTE),
  soort_adres VARCHAR2(30 BYTE),
  dat_selectie VARCHAR2(19 BYTE),
  mn_numrelatie VARCHAR2(9 BYTE),
  mn_persoonsnummer VARCHAR2(8 BYTE),
  mn_voorletters VARCHAR2(10 BYTE),
  mn_tussenvoegsel VARCHAR2(30 BYTE),
  mn_achternaam VARCHAR2(120 BYTE),
  mn_straat VARCHAR2(40 BYTE),
  mn_huisnr VARCHAR2(10 BYTE),
  mn_huisnr_toevoeging VARCHAR2(15 BYTE),
  mn_postcode VARCHAR2(15 BYTE),
  mn_woonplaats VARCHAR2(40 BYTE),
  uitspraakcode VARCHAR2(3 BYTE),
  overeenkomst_in_perc VARCHAR2(3 BYTE),
  naam VARCHAR2(60 BYTE),
  adres VARCHAR2(37 BYTE),
  postcode VARCHAR2(7 BYTE),
  woonplaats VARCHAR2(24 BYTE),
  titels VARCHAR2(25 BYTE),
  voorletters VARCHAR2(20 BYTE),
  voorvoegsels VARCHAR2(15 BYTE),
  achternaam VARCHAR2(30 BYTE),
  achtervoegsels VARCHAR2(15 BYTE),
  aanhef VARCHAR2(45 BYTE),
  straatnaam VARCHAR2(24 BYTE),
  huisnr VARCHAR2(5 BYTE),
  huisnr_toevoeging VARCHAR2(6 BYTE),
  netnummer VARCHAR2(5 BYTE),
  abonneenummer VARCHAR2(7 BYTE),
  verhuiscode VARCHAR2(1 BYTE),
  verhuisdatum VARCHAR2(8 BYTE),
  datum_nazenden_tm VARCHAR2(8 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline
      badfile 'rle_ext_adressen_bestand.bad'
      discardfile 'rle_ext_adressen_bestand.dis'
      logfile 'rle_ext_adressen_bestand.log'
	  fields terminated by ";" optionally enclosed by '"'
      missing field values are null
)
LOCATION ('rle_ext_adressen_bestand.csv'));