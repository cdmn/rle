CREATE TABLE comp_rle.rle_toegestane_rollen (
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  rol_codgroep VARCHAR2(2 BYTE) NOT NULL CONSTRAINT rle_trl_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_ROL_CODGROEP */
ROL_CODGROEP = UPPER(ROL_CODGROEP)),
  rol_codtoegestaan VARCHAR2(2 BYTE) NOT NULL CONSTRAINT rle_trl_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_ROL_CODTOEGESTAAN */
ROL_CODTOEGESTAAN = UPPER(ROL_CODTOEGESTAAN)),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_trl_pk PRIMARY KEY (rol_codgroep,rol_codtoegestaan),
  CONSTRAINT rle_trl_rol_fk1 FOREIGN KEY (rol_codgroep) REFERENCES comp_rle.rle_rollen (codrol),
  CONSTRAINT rle_trl_rol_fk2 FOREIGN KEY (rol_codtoegestaan) REFERENCES comp_rle.rle_rollen (codrol) ON DELETE CASCADE
);
COMMENT ON TABLE comp_rle.rle_toegestane_rollen IS 'Toegestane rollen voor Groepsrol';