CREATE TABLE comp_rle.rle_tech_data (
  "ID" NUMBER NOT NULL,
  modulenaam VARCHAR2(50 BYTE),
  begindatum DATE,
  einddatum DATE,
  script_id NUMBER,
  PRIMARY KEY ("ID")
);
COMMENT ON TABLE comp_rle.rle_tech_data IS 'Registreert technische gegevens t.b.v. screen relaties';