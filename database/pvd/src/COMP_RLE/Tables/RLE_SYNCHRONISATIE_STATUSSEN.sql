CREATE TABLE comp_rle.rle_synchronisatie_statussen (
  rle_numrelatie NUMBER(9) NOT NULL,
  rle_gewenste_indicatie VARCHAR2(1 BYTE),
  rle_datum_indicatie DATE,
  gba_afnemers_indicatie VARCHAR2(1 BYTE),
  gba_datum_indicatie DATE,
  reden VARCHAR2(1000 BYTE),
  datum_reden DATE,
  datum_laatste_controle DATE,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  groep VARCHAR2(3 BYTE),
  datum_groep DATE,
  rle_code_gewenste_ind VARCHAR2(10 BYTE),
  CONSTRAINT rle_scs_pk PRIMARY KEY (rle_numrelatie),
  CONSTRAINT rle_scs_rle_fk FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie)
);