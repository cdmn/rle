CREATE TABLE comp_rle.rle_adressen (
  numadres NUMBER(10) NOT NULL,
  lnd_codland VARCHAR2(3 BYTE) NOT NULL CONSTRAINT rle_ads_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_LND_CODLAND */
LND_CODLAND = UPPER(LND_CODLAND)),
  codorganisatie VARCHAR2(4 BYTE) NOT NULL CONSTRAINT rle_ads_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODORGANISATIE */
CODORGANISATIE = UPPER(CODORGANISATIE)),
  stt_straatid NUMBER(7),
  wps_woonplaatsid NUMBER(5),
  huisnummer NUMBER(10),
  postcode VARCHAR2(15 BYTE) CONSTRAINT rle_ads_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_POSTCODE */
POSTCODE = UPPER(POSTCODE)),
  toevhuisnum VARCHAR2(15 BYTE),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_ads_pk PRIMARY KEY (numadres),
  CONSTRAINT rle_ads_lnd_fk1 FOREIGN KEY (lnd_codland) REFERENCES comp_rle.rle_landen (codland),
  CONSTRAINT rle_ads_stt_fk1 FOREIGN KEY (stt_straatid) REFERENCES comp_rle.rle_straten (straatid),
  CONSTRAINT rle_ads_wps_fk1 FOREIGN KEY (wps_woonplaatsid) REFERENCES comp_rle.rle_woonplaatsen (woonplaatsid)
);
COMMENT ON TABLE comp_rle.rle_adressen IS 'Adressen';