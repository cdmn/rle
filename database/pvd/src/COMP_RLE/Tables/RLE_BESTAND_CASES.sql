CREATE TABLE comp_rle.rle_bestand_cases (
  persoonsnummer NUMBER(20),
  documentsoort VARCHAR2(20 BYTE),
  opmerking VARCHAR2(500 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline skip 1
      badfile 'rle_bestand_cases.bad'
      discardfile 'rle_bestand_cases.dis'
      logfile 'rle_bestand_cases.log'
    fields terminated by ";"
      missing field values are null
)
LOCATION ('rle_bestand_cases.csv'));