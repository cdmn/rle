CREATE TABLE comp_rle.rle_communicatie_categorieen (
  "ID" NUMBER NOT NULL,
  code VARCHAR2(10 BYTE) NOT NULL,
  omschrijving VARCHAR2(100 BYTE) NOT NULL,
  datum_begin DATE NOT NULL,
  datum_einde DATE,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  adm_code VARCHAR2(12 BYTE) DEFAULT 'ALG' NOT NULL,
  CONSTRAINT rle_ccn_ck1 CHECK (datum_einde is null
OR
(
datum_einde IS NOT NULL
AND datum_begin IS NOT NULL
AND TRUNC(datum_einde) >= TRUNC(datum_begin)
)),
  CONSTRAINT rle_ccn_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_ccn_uk1 UNIQUE (adm_code,code),
  CONSTRAINT rle_ccn_uk2 UNIQUE (omschrijving)
);