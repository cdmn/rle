CREATE TABLE comp_rle.verrijkt_schaduw_bestand (
  bsn VARCHAR2(30 BYTE),
  prtnr_a_nr VARCHAR2(30 BYTE),
  prtnr_bsn VARCHAR2(30 BYTE),
  prtnr_voornaam VARCHAR2(300 BYTE),
  prtnr_adelijk_titel VARCHAR2(300 BYTE),
  prtnr_voorv_geslachts_naam VARCHAR2(30 BYTE),
  prtnr_geslachts_naam VARCHAR2(300 BYTE),
  prtnr_geboorte_datum DATE,
  prtnr_geboorte_datum_txt VARCHAR2(30 BYTE),
  prtnr_datum_huwelijk DATE,
  prtnr_datum_huwelijk_txt VARCHAR2(30 BYTE),
  prtnr_datum_ontb_huwelijk DATE,
  prtnr_datum_ontb_huwelijk_txt VARCHAR2(30 BYTE),
  prtnr_reden_ontb_huwelijk VARCHAR2(30 BYTE),
  mn_persoonsnummer VARCHAR2(20 BYTE),
  mn_relatienummer NUMBER(9),
  mn_geboorte_datum DATE,
  mn_datum_ltste_huwelijk DATE,
  mn_overl_dat_ltste_partner DATE,
  mn_ind_deelnemer_pmt VARCHAR2(1 BYTE),
  mn_ind_deelnemer_pme VARCHAR2(1 BYTE),
  mn_ind_deelnemer_kvd VARCHAR2(1 BYTE),
  mn_1e_avg_datum_pmt DATE,
  mn_1e_avg_datum_pme DATE,
  mn_1e_avg_datum_kvd DATE,
  mn_overlijden_datum DATE,
  mn_id_abp NUMBER,
  mn_afkoopbedrag NUMBER(10,2),
  mn_recht_1975 NUMBER(10,2),
  mn_geslacht VARCHAR2(240 BYTE),
  mn_prtnr_persoonsnummer VARCHAR2(20 BYTE),
  mn_prtnr_relatienummer NUMBER(9),
  mn_prtnr_verm_relnummer NUMBER(9),
  mn_prtnr_basis_relnummer VARCHAR2(20 BYTE),
  mn_prtnr_datum_huwelijk DATE,
  mn_prtnr_datum_ontb_huwelijk DATE,
  mn_prtnr_overlijden_datum DATE,
  record_verrijkt VARCHAR2(1 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline CHARACTERSET AL32UTF8 STRING SIZES ARE IN CHARACTERS
         READSIZE 1048576
          BADFILE RLE_INBOX:'bron_schaduw_tabel.bad'
          DISCARDFILE RLE_INBOX:'bron_schaduw_tabel.dsc'
          LOGFILE RLE_INBOX:'bron_schaduw_tabel.log'
          fields terminated by ';'
          MISSING FIELD VALUES ARE NULL
)
LOCATION ('BRON_SCHADUW_TABEL.csv'))
REJECT LIMIT UNLIMITED;