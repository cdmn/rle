CREATE TABLE comp_rle.rle_ext_adr_subsets (
  "ID" NUMBER(9) NOT NULL,
  subsetnaam VARCHAR2(20 BYTE) NOT NULL,
  rol_codrol VARCHAR2(2 BYTE) NOT NULL,
  soort_adres VARCHAR2(30 BYTE) NOT NULL,
  dat_selectie DATE,
  dat_ontvangen DATE,
  CONSTRAINT rle_eat_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_eat_uk1 UNIQUE (subsetnaam,rol_codrol,soort_adres),
  CONSTRAINT rle_eat_uk2 UNIQUE (rol_codrol,soort_adres,dat_selectie)
);
COMMENT ON TABLE comp_rle.rle_ext_adr_subsets IS 'Subsets t.b.v. het extern afstemmen van adressen';