CREATE TABLE comp_rle.rle_relatie_externe_codes_jn (
  jn_user VARCHAR2(30 BYTE) NOT NULL,
  jn_date_time DATE NOT NULL,
  jn_operation VARCHAR2(3 BYTE) NOT NULL,
  "ID" NUMBER(9) NOT NULL,
  rle_numrelatie NUMBER(9),
  dwe_wrddom_extsys VARCHAR2(30 BYTE),
  datingang DATE,
  codorganisatie VARCHAR2(4 BYTE),
  rol_codrol VARCHAR2(2 BYTE),
  extern_relatie VARCHAR2(20 BYTE),
  ind_geverifieerd VARCHAR2(1 BYTE),
  dateinde DATE,
  dat_creatie DATE,
  creatie_door VARCHAR2(30 BYTE),
  dat_mutatie DATE,
  mutatie_door VARCHAR2(30 BYTE)
);
COMMENT ON TABLE comp_rle.rle_relatie_externe_codes_jn IS 'Journal table for table RLE_RELATIE_EXTERNE_CODES';