CREATE TABLE comp_rle.rle_ext_kanaalvoorkeuren (
  persoonsnummer VARCHAR2(200 BYTE),
  relatienummer VARCHAR2(200 BYTE),
  regeling VARCHAR2(200 BYTE),
  communicatie_categorie VARCHAR2(200 BYTE),
  kanaal_voorkeur VARCHAR2(200 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline
         skip 1
         badfile rle_outbox:'ret_ext_upo_aanvragen.bad'
         logfile rle_outbox:'ret_ext_upo_aanvragen.log'
         discardfile rle_outbox:'ret_ext_upo_aanvragen.dsc'
         fields terminated by ';'
         lrtrim
         missing field values are null
(
  PERSOONSNUMMER,
  RELATIENUMMER,
  REGELING,
  COMMUNICATIE_CATEGORIE,
  KANAAL_VOORKEUR
)
)
LOCATION ('rle_ext_kanaalvoorkeuren.csv'))
REJECT LIMIT UNLIMITED;