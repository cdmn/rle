CREATE TABLE comp_rle.rle_gemoedsbezwaardheden (
  rle_numrelatie NUMBER(9) NOT NULL,
  dat_begin DATE NOT NULL,
  dat_einde DATE,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  CONSTRAINT rle_gbd_ck1 CHECK (dat_einde is null
OR (dat_einde IS NOT NULL
AND dat_begin IS NOT NULL
AND TRUNC(dat_einde) >= TRUNC(dat_begin)
)),
  CONSTRAINT rle_gbd_pk PRIMARY KEY (rle_numrelatie,dat_begin),
  CONSTRAINT rle_gbd_rle_fk FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie)
);