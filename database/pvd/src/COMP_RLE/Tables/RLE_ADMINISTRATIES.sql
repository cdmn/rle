CREATE TABLE comp_rle.rle_administraties (
  code VARCHAR2(12 BYTE) NOT NULL,
  omschrijving VARCHAR2(50 BYTE),
  ind_afstemmen_gba_tgstn VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  "TYPE" VARCHAR2(1 BYTE) DEFAULT 'A' NOT NULL,
  CONSTRAINT rle_ate_pk PRIMARY KEY (code)
);