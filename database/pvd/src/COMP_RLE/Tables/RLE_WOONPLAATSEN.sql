CREATE TABLE comp_rle.rle_woonplaatsen (
  woonplaatsid NUMBER(5) NOT NULL,
  lnd_codland VARCHAR2(3 BYTE) NOT NULL CONSTRAINT rle_wps_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_LND_CODLAND */
LND_CODLAND = UPPER(LND_CODLAND)),
  woonplaats VARCHAR2(40 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_wps_pk PRIMARY KEY (woonplaatsid),
  CONSTRAINT rle_wps_uk1 UNIQUE (lnd_codland,woonplaats),
  CONSTRAINT rle_wps_lnd_fk1 FOREIGN KEY (lnd_codland) REFERENCES comp_rle.rle_landen (codland)
);
COMMENT ON TABLE comp_rle.rle_woonplaatsen IS 'Woonplaatsen';