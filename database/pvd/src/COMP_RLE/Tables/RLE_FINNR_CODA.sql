CREATE TABLE comp_rle.rle_finnr_coda (
  fnr_id NUMBER NOT NULL,
  adm_code VARCHAR2(240 BYTE) NOT NULL,
  "TAG" NUMBER NOT NULL,
  ind_r_nummer VARCHAR2(3 BYTE) DEFAULT 'N' NOT NULL CONSTRAINT rle_fnc_ck1 CHECK (ind_r_nummer in ('J', 'N')),
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  CONSTRAINT rle_fnc_pk PRIMARY KEY (fnr_id,adm_code,ind_r_nummer),
  CONSTRAINT rle_fnc_uk1 UNIQUE (fnr_id,adm_code,ind_r_nummer,"TAG"),
  CONSTRAINT rle_fnc_adm_fk1 FOREIGN KEY (adm_code) REFERENCES comp_rle.rle_administraties (code),
  CONSTRAINT rle_fnc_fnr_fk1 FOREIGN KEY (fnr_id) REFERENCES comp_rle.rle_financieel_nummers ("ID")
);