CREATE TABLE comp_rle.rle_relatie_rollen (
  rle_numrelatie NUMBER(9) NOT NULL,
  rol_codrol VARCHAR2(2 BYTE) NOT NULL CONSTRAINT rle_rrl_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_ROL_CODROL */
ROL_CODROL = UPPER(ROL_CODROL)),
  codorganisatie VARCHAR2(4 BYTE) NOT NULL CONSTRAINT rle_rrl_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODORGANISATIE */
CODORGANISATIE = UPPER(CODORGANISATIE)),
  codpresentatie VARCHAR2(15 BYTE) CONSTRAINT rle_rrl_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODPRESENTATIE */
CODPRESENTATIE = UPPER(CODPRESENTATIE)),
  datschoning DATE,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_rrl_pk PRIMARY KEY (rle_numrelatie,rol_codrol),
  CONSTRAINT rle_rrl_rle_fk1 FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie),
  CONSTRAINT rle_rrl_rol_fk1 FOREIGN KEY (rol_codrol) REFERENCES comp_rle.rle_rollen (codrol)
);
COMMENT ON TABLE comp_rle.rle_relatie_rollen IS 'Rollen van de Relatie';