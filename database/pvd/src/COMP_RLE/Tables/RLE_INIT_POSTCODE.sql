CREATE TABLE comp_rle.rle_init_postcode (
  postcode VARCHAR2(15 BYTE) CONSTRAINT rle_ipe_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_POSTCODE */
POSTCODE = UPPER(POSTCODE)),
  reekscod NUMBER(1),
  numscheidingvan NUMBER(5),
  numscheidingtm NUMBER(5),
  woonplaats_ptt VARCHAR2(18 BYTE),
  woonplaats_nen VARCHAR2(24 BYTE),
  straatnaam_ptt VARCHAR2(17 BYTE),
  straatnaam_nen VARCHAR2(24 BYTE),
  straatnaam_off VARCHAR2(43 BYTE),
  gemeentenaam VARCHAR2(24 BYTE),
  gemeentenummer NUMBER(4),
  codprov VARCHAR2(1 BYTE) CONSTRAINT rle_ipe_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODPROV */
CODPROV = UPPER(CODPROV)),
  codcebuco NUMBER(3),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL
);
COMMENT ON TABLE comp_rle.rle_init_postcode IS 'InitiÙle Postcodetape';
COMMENT ON COLUMN comp_rle.rle_init_postcode.postcode IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.reekscod IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.numscheidingvan IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.numscheidingtm IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.woonplaats_ptt IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.woonplaats_nen IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.straatnaam_ptt IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.straatnaam_nen IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.straatnaam_off IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.gemeentenaam IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.codprov IS '- Column already exists';
COMMENT ON COLUMN comp_rle.rle_init_postcode.codcebuco IS '- Column already exists';