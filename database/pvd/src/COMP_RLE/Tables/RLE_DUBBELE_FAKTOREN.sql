CREATE TABLE comp_rle.rle_dubbele_faktoren (
  proces VARCHAR2(10 BYTE) NOT NULL,
  rubriek VARCHAR2(20 BYTE) NOT NULL,
  faktor_lengte NUMBER(3),
  faktor_inhoud NUMBER(3),
  faktor_volgorde NUMBER(3),
  faktor_leeg NUMBER(3),
  faktor_zwaarte NUMBER(3),
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_dfr_pk PRIMARY KEY (proces,rubriek)
);
COMMENT ON TABLE comp_rle.rle_dubbele_faktoren IS 'Dubbele faktoren';