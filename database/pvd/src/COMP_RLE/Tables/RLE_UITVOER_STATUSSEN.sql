CREATE TABLE comp_rle.rle_uitvoer_statussen (
  bsd_naam VARCHAR2(30 BYTE) NOT NULL,
  runnummer NUMBER(12) NOT NULL,
  vanaf_datum DATE NOT NULL,
  tot_datum DATE NOT NULL,
  starttijd_uitvoer DATE NOT NULL,
  bestandsnaam VARCHAR2(50 BYTE) NOT NULL,
  db_directory_uitvoer VARCHAR2(50 BYTE) NOT NULL,
  eindtijd_uitvoer DATE,
  CONSTRAINT rle_uss_pk PRIMARY KEY (bsd_naam,runnummer),
  CONSTRAINT rle_uss_bsd_fk FOREIGN KEY (bsd_naam) REFERENCES comp_rle.rle_bestanden (naam)
);