CREATE TABLE comp_rle.rle_nationaliteiten_jn (
  rle_numrelatie NUMBER(9),
  rle_numrelatie_new NUMBER(9),
  code_nationaliteit VARCHAR2(4 BYTE),
  code_nationaliteit_new VARCHAR2(4 BYTE),
  dat_begin DATE,
  dat_begin_new DATE,
  dat_einde DATE,
  dat_einde_new DATE,
  jn_user VARCHAR2(30 BYTE) NOT NULL,
  jn_date_time DATE NOT NULL,
  jn_operation VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE,
  creatie_door VARCHAR2(30 BYTE),
  dat_mutatie DATE,
  mutatie_door VARCHAR2(30 BYTE)
);