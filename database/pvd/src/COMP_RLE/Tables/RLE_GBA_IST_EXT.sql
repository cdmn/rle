CREATE TABLE comp_rle.rle_gba_ist_ext (
  a_nummer VARCHAR2(30 BYTE),
  bsn VARCHAR2(30 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline CHARACTERSET AL32UTF8 STRING SIZES ARE IN CHARACTERS
    READSIZE 1048576
     BADFILE RLE_INBOX:'rle_gba_ist_ext.bad'
     DISCARDFILE RLE_INBOX:'rle_gba_ist_ext.dsc'
     LOGFILE RLE_INBOX:'rle_gba_ist_ext.log'
     fields terminated by ';'
     MISSING FIELD VALUES ARE NULL
)
LOCATION ('RLE_GBA_IST_EXT.csv'))
REJECT LIMIT UNLIMITED;