CREATE TABLE comp_rle.rle_nationaliteiten (
  "ID" NUMBER(9) NOT NULL,
  rle_numrelatie NUMBER(9) NOT NULL,
  code_nationaliteit VARCHAR2(4 BYTE) NOT NULL,
  dat_begin DATE NOT NULL,
  dat_einde DATE,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT nat_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_nat_uk1 UNIQUE (rle_numrelatie,dat_begin,code_nationaliteit)
);