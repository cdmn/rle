CREATE TABLE comp_rle.rle_bestanden (
  naam VARCHAR2(30 BYTE) NOT NULL,
  bsd_groep VARCHAR2(10 BYTE) NOT NULL,
  bsd_type VARCHAR2(10 BYTE) NOT NULL,
  db_directory VARCHAR2(50 BYTE) NOT NULL,
  formaat_naam VARCHAR2(200 BYTE) NOT NULL,
  CONSTRAINT rle_bsd_pk PRIMARY KEY (naam)
);