CREATE TABLE comp_rle.rle_thermometer (
  bsn VARCHAR2(15 BYTE),
  persoonsnummer VARCHAR2(15 BYTE),
  relatienummer NUMBER,
  rubrieknummer VARCHAR2(300 BYTE),
  CONSTRAINT rle_rtr_rle_fk1 FOREIGN KEY (relatienummer) REFERENCES comp_rle.rle_relaties (numrelatie)
);