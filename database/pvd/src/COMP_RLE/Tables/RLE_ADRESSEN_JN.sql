CREATE TABLE comp_rle.rle_adressen_jn (
  jn_user VARCHAR2(30 BYTE) NOT NULL,
  jn_date_time DATE NOT NULL,
  jn_operation VARCHAR2(3 BYTE) NOT NULL,
  numadres NUMBER(7) NOT NULL,
  lnd_codland VARCHAR2(3 BYTE),
  codorganisatie VARCHAR2(4 BYTE),
  stt_straatid NUMBER(7),
  wps_woonplaatsid NUMBER(5),
  huisnummer NUMBER(10),
  postcode VARCHAR2(15 BYTE),
  toevhuisnum VARCHAR2(15 BYTE),
  dat_creatie DATE,
  creatie_door VARCHAR2(30 BYTE),
  dat_mutatie DATE,
  mutatie_door VARCHAR2(30 BYTE)
);
COMMENT ON TABLE comp_rle.rle_adressen_jn IS 'Journal table for table RLE_ADRESSEN';