CREATE TABLE comp_rle.rle_straten (
  straatid NUMBER(7) NOT NULL,
  wps_woonplaatsid NUMBER(5) NOT NULL,
  straatnaam VARCHAR2(40 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  CONSTRAINT rle_stt_pk PRIMARY KEY (straatid),
  CONSTRAINT rle_stt_uk1 UNIQUE (wps_woonplaatsid,straatnaam),
  CONSTRAINT rle_stt_wps_fk1 FOREIGN KEY (wps_woonplaatsid) REFERENCES comp_rle.rle_woonplaatsen (woonplaatsid)
);
COMMENT ON TABLE comp_rle.rle_straten IS 'Straten';