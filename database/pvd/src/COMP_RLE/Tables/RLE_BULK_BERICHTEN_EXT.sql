CREATE TABLE comp_rle.rle_bulk_berichten_ext (
  persoonsnummer VARCHAR2(20 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
RECORDS delimited by newline skip 1
     READSIZE 1048576
     BADFILE RLE_INBOX:'rle_bulk_berichten_ext.bad'
     DISCARDFILE RLE_INBOX:'rle_bulk_berichten_ext.dsc'
     LOGFILE RLE_INBOX:'rle_bulk_berichten_ext.log'
     fields terminated by ';'
     MISSING FIELD VALUES ARE NULL
)
LOCATION ('BULK_BERICHTEN.csv'))
REJECT LIMIT UNLIMITED;