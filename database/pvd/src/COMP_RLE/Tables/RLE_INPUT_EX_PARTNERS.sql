CREATE TABLE comp_rle.rle_input_ex_partners (
  regeling VARCHAR2(5 BYTE),
  bsn_dln VARCHAR2(9 BYTE),
  bsn_ex VARCHAR2(9 BYTE),
  a_nummer_ex VARCHAR2(30 BYTE),
  naam_ex VARCHAR2(120 BYTE),
  naamgebruik_ex VARCHAR2(1 BYTE),
  voorvoegsels_ex VARCHAR2(30 BYTE),
  voornamen_ex VARCHAR2(100 BYTE),
  voorletters_ex VARCHAR2(30 BYTE),
  geslacht_ex VARCHAR2(1 BYTE),
  geboortedatum_ex VARCHAR2(10 BYTE),
  overlijdensdatum_ex VARCHAR2(10 BYTE),
  landcode_ex VARCHAR2(5 BYTE),
  straatnaam_ex VARCHAR2(30 BYTE),
  huisnummer_ex VARCHAR2(10 BYTE),
  huisletter_ex VARCHAR2(10 BYTE),
  toevoeging_ex VARCHAR2(10 BYTE),
  postcode_ex VARCHAR2(10 BYTE),
  gemeentenummer_ex VARCHAR2(10 BYTE),
  gemeentedeel_ex VARCHAR2(30 BYTE),
  huwelijk_datumbegin VARCHAR2(10 BYTE),
  huwelijk_datumeinde VARCHAR2(10 BYTE),
  reden_einde_huwelijk VARCHAR2(1 BYTE),
  foutmelding VARCHAR2(4000 BYTE),
  bestandsnaam VARCHAR2(50 BYTE),
  creatie_door VARCHAR2(20 BYTE),
  dat_creatie DATE,
  mutatie_door VARCHAR2(20 BYTE),
  dat_mutatie DATE
);