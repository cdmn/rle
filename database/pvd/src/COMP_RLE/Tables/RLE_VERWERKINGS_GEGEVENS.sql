CREATE TABLE comp_rle.rle_verwerkings_gegevens (
  module_naam VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  relatie_koppelingen_id NUMBER(9),
  CONSTRAINT rle_vgn_pk PRIMARY KEY (module_naam)
);