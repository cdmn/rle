CREATE TABLE comp_rle.rle_syn_van_gba (
  anummer VARCHAR2(300 BYTE),
  bsn VARCHAR2(300 BYTE),
  rubrieknummer VARCHAR2(300 BYTE),
  ebrpwaarde VARCHAR2(1000 BYTE),
  bestandwaarde VARCHAR2(1000 BYTE)
)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY RLE_INBOX
ACCESS PARAMETERS (
records delimited by newline skip 1 CHARACTERSET AL32UTF8 STRING SIZES ARE IN CHARACTERS
         READSIZE 1048576
          BADFILE RLE_INBOX:'afwijkingen.bad'
          DISCARDFILE RLE_INBOX:'afwijkingen.dsc'
          LOGFILE RLE_INBOX:'afwijkingen.log'
          fields terminated by ';'
          MISSING FIELD VALUES ARE NULL
)
LOCATION ('afwijkingen.csv'))
REJECT LIMIT UNLIMITED;