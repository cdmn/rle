CREATE TABLE comp_rle.rle_postsoort_adressoorten (
  "ID" NUMBER(8) NOT NULL,
  postsoort VARCHAR2(30 BYTE) NOT NULL,
  adressoort VARCHAR2(2 BYTE) NOT NULL,
  prioriteit NUMBER NOT NULL,
  ind_dummy_adres_negeren VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL,
  max_geldigheid NUMBER(*,0),
  CONSTRAINT rle_pat_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_pat_uk1 UNIQUE (adressoort,postsoort,prioriteit)
);