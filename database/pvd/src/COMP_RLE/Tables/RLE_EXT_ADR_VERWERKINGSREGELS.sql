CREATE TABLE comp_rle.rle_ext_adr_verwerkingsregels (
  uitspraakcode VARCHAR2(3 BYTE) NOT NULL,
  verhuiscode VARCHAR2(3 BYTE),
  score_vanaf NUMBER NOT NULL,
  score_tm NUMBER NOT NULL,
  resultaat VARCHAR2(30 BYTE) NOT NULL,
  ind_chk_naamsgelijkheid VARCHAR2(3 BYTE) DEFAULT 'N',
  ind_chk_huisnr_toev VARCHAR2(3 BYTE) DEFAULT 'N',
  ind_chk_verhuisdatum VARCHAR2(3 BYTE) DEFAULT 'N',
  ind_chk_dubbele_naam VARCHAR2(3 BYTE) DEFAULT 'N'
);