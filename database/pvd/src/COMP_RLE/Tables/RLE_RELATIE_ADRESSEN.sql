CREATE TABLE comp_rle.rle_relatie_adressen (
  provincie VARCHAR2(20 BYTE),
  rle_numrelatie NUMBER(9) NOT NULL,
  rol_codrol VARCHAR2(2 BYTE) CONSTRAINT rle_ras_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_ROL_CODROL */
ROL_CODROL = UPPER(ROL_CODROL)),
  datingang DATE NOT NULL,
  dateinde DATE,
  numkamer NUMBER(5),
  dwe_wrddom_srtadr VARCHAR2(30 BYTE) NOT NULL,
  locatie VARCHAR2(100 BYTE),
  ind_woonboot VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL,
  ind_woonwagen VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL,
  "ID" NUMBER(9) NOT NULL,
  codorganisatie VARCHAR2(4 BYTE) NOT NULL CONSTRAINT rle_ras_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODORGANISATIE */
CODORGANISATIE = UPPER(CODORGANISATIE)),
  ads_numadres NUMBER(10) NOT NULL,
  indinhown VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL CONSTRAINT rle_ras_ck4 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDINHOWN */
INDINHOWN = UPPER(INDINHOWN)),
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  dat_mutatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  adm_code VARCHAR2(12 BYTE) DEFAULT 'ALG' NOT NULL,
  CONSTRAINT rle_ras_ck1 CHECK (dateinde is null
OR
(
dateinde IS NOT NULL
AND datingang IS NOT NULL
AND TRUNC(dateinde) >= TRUNC(datingang)
)),
  CONSTRAINT rle_ras_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_ras_uk1 UNIQUE (rle_numrelatie,rol_codrol,adm_code,dwe_wrddom_srtadr,datingang,ads_numadres),
  CONSTRAINT rle_ras_ads_fk1 FOREIGN KEY (ads_numadres) REFERENCES comp_rle.rle_adressen (numadres),
  CONSTRAINT rle_ras_rie_fk1 FOREIGN KEY (rle_numrelatie,rol_codrol,adm_code) REFERENCES comp_rle.rle_relatierollen_in_adm (rle_numrelatie,rol_codrol,adm_code),
  CONSTRAINT rle_ras_rle_fk1 FOREIGN KEY (rle_numrelatie) REFERENCES comp_rle.rle_relaties (numrelatie),
  CONSTRAINT rle_ras_rol_fk1 FOREIGN KEY (rol_codrol) REFERENCES comp_rle.rle_rollen (codrol)
);
COMMENT ON TABLE comp_rle.rle_relatie_adressen IS 'Adressen van de Relatie';