CREATE TABLE comp_rle.rle_tmp_werkgever (
  werkgevernummer VARCHAR2(6 BYTE) NOT NULL,
  code_functiecategorie VARCHAR2(2 BYTE) NOT NULL,
  dataanv DATE NOT NULL,
  dateind DATE,
  init_datum DATE
);