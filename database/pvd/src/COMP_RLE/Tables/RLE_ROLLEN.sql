CREATE TABLE comp_rle.rle_rollen (
  codrol VARCHAR2(2 BYTE) NOT NULL CONSTRAINT rle_rol_ck1 CHECK (/* QMS$ENFORCE_UPPER_CASE_CODROL */
CODROL = UPPER(CODROL)),
  omsrol VARCHAR2(30 BYTE) NOT NULL,
  indwijzigbaar VARCHAR2(1 BYTE) DEFAULT 'J' NOT NULL CONSTRAINT rle_rol_ck2 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDWIJZIGBAAR */
INDWIJZIGBAAR = UPPER(INDWIJZIGBAAR)),
  indschonen VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL CONSTRAINT rle_rol_ck3 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDSCHONEN */
INDSCHONEN = UPPER(INDSCHONEN)),
  indgroepsrol VARCHAR2(1 BYTE) DEFAULT 'N' NOT NULL CONSTRAINT rle_rol_ck4 CHECK (/* QMS$ENFORCE_UPPER_CASE_INDGROEPSROL */
INDGROEPSROL = UPPER(INDGROEPSROL)),
  rle_vrol_plus_rol_codrol VARCHAR2(2 BYTE) DEFAULT 'P' NOT NULL,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  prioriteit NUMBER(3) NOT NULL,
  dat_mutatie DATE NOT NULL,
  CONSTRAINT rle_rol_pk PRIMARY KEY (codrol),
  CONSTRAINT rle_rol_uk1 UNIQUE (prioriteit),
  CONSTRAINT rle_rol_uk2 UNIQUE (omsrol)
);
COMMENT ON TABLE comp_rle.rle_rollen IS 'Rollen';