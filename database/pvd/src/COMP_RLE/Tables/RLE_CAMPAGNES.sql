CREATE TABLE comp_rle.rle_campagnes (
  "ID" NUMBER(10) NOT NULL,
  cde_id NUMBER(10) NOT NULL,
  aanmaakmoment DATE NOT NULL,
  status VARCHAR2(3 BYTE) NOT NULL,
  briefcode VARCHAR2(30 BYTE),
  technische_procesnaam VARCHAR2(10 BYTE),
  werkbak VARCHAR2(230 BYTE),
  code_soort_adres VARCHAR2(30 BYTE),
  gebruikt_sql_statement CLOB,
  aanmaakmoment_brieven TIMESTAMP,
  creatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_creatie DATE NOT NULL,
  mutatie_door VARCHAR2(30 BYTE) NOT NULL,
  dat_mutatie DATE NOT NULL,
  CONSTRAINT rle_cpe_pk PRIMARY KEY ("ID"),
  CONSTRAINT rle_cpe_cde_fk FOREIGN KEY (cde_id) REFERENCES comp_rle.rle_campagne_definities ("ID")
);