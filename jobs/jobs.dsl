/* JOB DSL configuration script for application/domain RLE */
job_defaults {
  jenkins_folder = 'Build'
  application = 'RLE'
}

jobs {

  RLE_DATAFIX {
    type = 'DATAFIX'
    branch = 'database/pvd/datafixes'
  }

  RLE_DB_PVD {
    type = 'DB'
    branch = 'database/pvd'
    sonar_project='sonar-project.properties'
    sonar_properties='sonar.projectBaseDir=src/COMP_RLE'
  }

  RLE_DESHTML {
    type = 'DESHTML'
    branch = 'documentatie/deshtml'
  }

  RLE_FRM {
    type = 'FRM'
    branch = 'frm'
  }

  RLE_JCS {
    type = 'JCS'
    branch = 'jcs'
  }

  RLE_SCRIPTS {
    type = 'SCRIPTS'
    branch = 'scripts'
  }

  RLE_CMP {
    type = 'CMP'
    branch = 'componenten'
    depth = 0
  }

  RLE_RUN_ALL_BLD {
    type = 'BLDALL'
    soort = 'BLD'
  }

  RLE_RUN_ALL_CMP {
    type = 'BLDALL'
    soort = 'CMP'
  }

  RLE_RELEASE_BRANCH {
    type = 'BRANCH'
    depth = 0
    branch = 'RLE'
    mds = 'N'
  }
}
