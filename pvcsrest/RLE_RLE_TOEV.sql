delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RLE_TOEV';
delete from stm_moduleparameters
    where mde_module = 'RLE_RLE_TOEV';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RLE_TOEV';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RLE_TOEV';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RLE_TOEV';
DELETE FROM stm_modules
    WHERE module = 'RLE_RLE_TOEV';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RLE_TOEV',1,'RLE',1
    ,'RLE0001: Toevoegen relatie','P'
    ,'RLE_RLE_TOEV',''
    ,'A',''
    ,40);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A24','RLE_RLE_TOEV',1,'J'
    ,'P_IND_SUCCES','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A25','RLE_RLE_TOEV',1,'J'
    ,'P_MELDING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A26','RLE_RLE_TOEV',1,'J'
    ,'P_EXTERNE_CODE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A23','RLE_RLE_TOEV',1,'J'
    ,'P_IND_COMMIT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_RLE_TOEV',1,'J'
    ,'P_NUMRELATIE','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_RLE_TOEV',1,'J'
    ,'P_NAMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_RLE_TOEV',1,'J'
    ,'P_ROL_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_RLE_TOEV',1,'J'
    ,'P_INDINSTELLING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_RLE_TOEV',1,'J'
    ,'P_WRDDOM_GESLACHT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_RLE_TOEV',1,'J'
    ,'P_WRDDOM_VVGSL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_RLE_TOEV',1,'J'
    ,'P_VOORLETTER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_RLE_TOEV',1,'J'
    ,'P_VOORNAAM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_RLE_TOEV',1,'J'
    ,'P_HANDELSNAAM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D10','RLE_RLE_TOEV',1,'J'
    ,'P_DATBEGIN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D11','RLE_RLE_TOEV',1,'J'
    ,'P_DATEINDE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D12','RLE_RLE_TOEV',1,'J'
    ,'P_DATOPRICHTING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D13','RLE_RLE_TOEV',1,'J'
    ,'P_DATGEBOORTE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_RLE_TOEV',1,'J'
    ,'P_CODE_GEBDAT_FICTIEF','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D15','RLE_RLE_TOEV',1,'J'
    ,'P_DATOVERLIJDEN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D16','RLE_RLE_TOEV',1,'J'
    ,'P_DATREGOVERLIJDENBEVESTIGING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A17','RLE_RLE_TOEV',1,'J'
    ,'P_CODE_AANDUIDING_NAAMGEBRUIK','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A18','RLE_RLE_TOEV',1,'J'
    ,'P_NAAM_PARTNER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A19','RLE_RLE_TOEV',1,'J'
    ,'P_VVGSL_PARTNER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D20','RLE_RLE_TOEV',1,'J'
    ,'P_DATEMIGRATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A21','RLE_RLE_TOEV',1,'J'
    ,'P_WRDDOM_GEWATITEL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A22','RLE_RLE_TOEV',1,'J'
    ,'P_IND_VERVALLEN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A27','RLE_RLE_TOEV',1,'N'
    ,'P_CODE_LAND','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A28','RLE_RLE_TOEV',1,'N'
    ,'P_SRT_ADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A29','RLE_RLE_TOEV',1,'N'
    ,'P_POSTCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N30','RLE_RLE_TOEV',1,'N'
    ,'P_HUISNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A31','RLE_RLE_TOEV',1,'N'
    ,'P_TOEVOEGING_HUISNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A32','RLE_RLE_TOEV',1,'N'
    ,'P_WOONPLAATS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A33','RLE_RLE_TOEV',1,'N'
    ,'P_STRAAT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D34','RLE_RLE_TOEV',1,'N'
    ,'P_INGANGSDATUM_ADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D35','RLE_RLE_TOEV',1,'N'
    ,'p_EINDDATUM_ADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N36','RLE_RLE_TOEV',1,'N'
    ,'P_ADRESNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A37','RLE_RLE_TOEV',1,'N'
    ,'P_LOCATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A38','RLE_RLE_TOEV',1,'N'
    ,'P_IND_WOONBOOT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A39','RLE_RLE_TOEV',1,'N'
    ,'P_IND_WOONWAGEN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A40','RLE_RLE_TOEV',1,'N'
    ,'P_PROVINCIE','INP');
