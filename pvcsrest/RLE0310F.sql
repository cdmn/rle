delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0310F';
delete from stm_moduleparameters
    where mde_module = 'RLE0310F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0310F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0310F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0310F';
DELETE FROM stm_modules
    WHERE module = 'RLE0310F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0310F',1,'RLE',1
    ,'Onderhouden gemoedsbezwaardheden','S'
    ,'RLE0310F',''
    ,'A','Onderhouden gemoedsbezwaardheden'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0310F',1,'J'
    ,'Relatienr','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0310F',1,'NR_PERSOON',
        ':RLE.LKP_PERSOONSNUMMER');
