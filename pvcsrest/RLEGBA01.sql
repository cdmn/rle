REM ****************************************************************************
REM Naam      : RLEGBA01.sql 
REM Doel      : Vervaardigen van een ASCII bestand (csv) met verschillen 
REM             tussen de gba (tnt) relatie koppelingen en die van rle
REM FO        : RLE1020
REM
REM Parameters: %1  script naam   ( wordt door JCS bepaald )
REM             %2  script id     ( wordt door JCS bepaald )
REM
REM Wijzigingshistorie:
REM Versie Naam                   Datum      Wijziging
REM ------ ---------------------- ---------- ---------------------------------------
REM 1.0    H. Kulker              22-04-2014 Creatie 
REM 1.1    H. Kulker              30-04-2014 Eerste 3 tech/afleidbare velden uit csv gehaald
REM ****************************************************************************

whenever sqlerror exit failure
whenever oserror  exit failure

SET linesize 10000
SET define '&'
SET define on
SET pagesize 0
SET heading off
SET feedback off
SET pause off
SET verify off
SET trimspool on
SET termout off


variable var_context_naam varchar2(10)

begin
  :var_context_naam := stm_util.t_context;
end;
/

var b_script_naam             varchar2(20);
var b_script_id               varchar2(20);

exec :b_script_naam             := '&1'
exec :b_script_id               := '&2'

SET termout off

REM - Ophalen instance naam

define l_instance = ''

column l1 noprint new_value l_instance

SELECT   name   || '_'                        L1
FROM     V$DATABASE
WHERE    name != 'PPVD'
/

REM - Bepalen bestandsnaam
REM - De bestandsnaam begint altijd met de scriptnaam, %1
REM   en eindigt (voor de extensie) met het job_id, %2

define l_file_naam = ''

column l2 new_value l_file_naam

SELECT                  '&l_instance'       -- INSTANCE NAAM
                     || '&1'                -- SCRIPTNAAM		 
         || '_'      || '&2'                -- JOB_ID
         || '.CSV'
                                            L2
FROM       DUAL
/

alter session set nls_numeric_characters=',.';
alter session set optimizer_index_cost_adj=100;

-- Op unix
SPOOL $TMP/RLE/outbox/\&l_file_naam
-- Op windows
--SPOOL &l_file_naam

REM - Aanmaken van de rapportage

select
'"'||'bsn'
--||'";"'||'id_tnt'
--||'";"'||'id_rkn'
--||'";"'||'ind_match'
||'";"'||'persoonsnummer'
||'";"'||'relatienummer'
||'";"'||'overlijdensdatum'
||'";"'||'deelnemerschappen'
||'";"'||'primaire_relatierol'
||'";"'||'persoonsnummer_partner_rle'
||'";"'||'relatienummer_partner_rle'
||'";"'||'overlijdensdatum_partner_rle'
||'";"'||'ind_datum_huwelijk_afwijkend'
||'";"'||'ind_datum_ontbinding_afwijkend'
||'";"'||'ind_partner_afwijkend'
||'";"'||'datum_huwelijk_rle'
||'";"'||'datum_huwelijk_gba'
||'";"'||'datum_ontbinding_rle'
||'";"'||'datum_ontbinding_gba'
||'";"'||'reden_ontbinding_gba'
||'";"'||'bsn_partner_rle'
||'";"'||'bsn_partner_gba'
||'";"'||'geboortedatum_partner_rle'
||'";"'||'geboortedatum_partner_gba'
||'";"'||'a_nr_partner_rle'
||'";"'||'a_nr_partner_gba'
||'";"'||'naam_partner_rle'
||'";"'||'naam_partner_gba'||'"'
from dual
;

select 
    '"'||tnt.bsn                                
--||'";"'||tnt.id_tnt                             
--||'";"'||tnt.id_rkn                             
--||'";"'||tnt.ind_match                          
||'";"'||tnt.persoonsnummer                     
||'";"'||tnt.relatienummer                      
||'";"'||tnt.overlijdensdatum                   
||'";"'||tnt.deelnemerschappen                  
||'";"'||tnt.primaire_relatierol                
||'";"'||tnt.persoonsnummer_partner_rle         
||'";"'||tnt.relatienummer_partner_rle          
||'";"'||tnt.overlijdensdatum_partner_rle       
||'";"'||case when tnt.ind_datum_huwelijk_afwijkend    = '1' then 'N' when tnt.ind_datum_huwelijk_afwijkend  = '0' then 'J' else '' end
||'";"'||case when tnt.ind_datum_ontbinding_afwijkend  = '1' then 'N' when tnt.ind_datum_ontbinding_afwijkend = '0' then 'J' else '' end
||'";"'||case when tnt.ind_partner_afwijkend           = '1' then 'N' when tnt.ind_partner_afwijkend = '0' then 'J' else '' end
||'";"'||tnt.datum_huwelijk_rle                 
||'";"'||tnt.datum_huwelijk_gba                 
||'";"'||tnt.datum_ontbinding_rle                
||'";"'||tnt.datum_ontbinding_gba                
||'";"'||tnt.reden_ontbinding_gba                
||'";"'||tnt.bsn_partner_rle                    
||'";"'||tnt.bsn_partner_gba                    
||'";"'||tnt.geboortedatum_partner_rle          
||'";"'||tnt.geboortedatum_partner_gba          
||'";"'||tnt.a_nr_partner_rle                   
||'";"'||tnt.a_nr_partner_gba                   
||'";"'||tnt.naam_partner_rle                   
||'";"'||tnt.naam_partner_gba                   ||'"'
from rle_gba_tnt_verschillen tnt
where tnt.ind_match <> 'J'
;

prompt
prompt

REM - Parameters

SELECT 'Instance naam                : ' || replace('&l_instance','_')
FROM DUAL;
SELECT 'Script naam                  : ' || '&1'
FROM DUAL;
SELECT 'Script id                    : ' || '&2'
FROM DUAL;

SPOOL OFF

REM - job_statussen bijwerken

INSERT INTO stm_job_statussen
(   script_naam
,   script_id
,   programma_naam
,   volgnummer
,   uitvoer_soort
,   uitvoer_waarde
,   datum_tijd_mutatie
,   gebruiker
,   context_naam
)
VALUES
(   UPPER('&1')
,   &2
,   'RLEGBA01'
,   1
,   'S'
,   '0'
,   SYSDATE
,   USER
,   :var_context_naam
);

COMMIT;


