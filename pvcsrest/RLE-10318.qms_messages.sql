DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-10318'
/

DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-10318'
/

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10318', 'Deze contactpersoon hoort niet bij de relatie of bij een ander adres.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10318', 'DUT'
            , 'Deze contactpersoon hoort niet bij de relatie of bij een ander adres.'
            , '');

