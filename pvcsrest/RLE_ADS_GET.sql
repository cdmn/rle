delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_ADS_GET';
delete from stm_moduleparameters
    where mde_module = 'RLE_ADS_GET';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_ADS_GET';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_ADS_GET';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_ADS_GET';
DELETE FROM stm_modules
    WHERE module = 'RLE_ADS_GET';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_ADS_GET',1,'RLE',1
    ,'Ophalen adres via ADS','P'
    ,'RLE_ADS_GET',''
    ,'A',''
    ,11);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_ADS_GET',1,'J'
    ,'P_NUMADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_ADS_GET',1,'J'
    ,'P_AANT_REGELS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_ADS_GET',1,'J'
    ,'P_AANT_POS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_ADS_GET',1,'J'
    ,'P_LAYOUTCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_ADS_GET',1,'J'
    ,'P_O_MELDING','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_ADS_GET',1,'J'
    ,'P_O_REGEL1','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_ADS_GET',1,'J'
    ,'P_O_REGEL2','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_ADS_GET',1,'J'
    ,'P_O_REGEL3','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_ADS_GET',1,'J'
    ,'P_O_REGEL4','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_ADS_GET',1,'J'
    ,'P_O_REGEL5','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_ADS_GET',1,'J'
    ,'P_O_REGEL6','MOD');
