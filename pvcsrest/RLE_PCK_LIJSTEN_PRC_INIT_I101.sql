delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I101';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I101';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I101';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I101';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I101';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_INIT_I101';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_INIT_I101',1,'RLE',1
    ,'Ophalen relatierollen (s)','P'
    ,'RLE_PCK_LIJSTEN.PRC_INIT_I101',''
    ,'A',''
    ,1);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_PCK_LIJSTEN.PRC_INIT_I101',1,'J'
    ,'P_NUMRELATIE','INP');
