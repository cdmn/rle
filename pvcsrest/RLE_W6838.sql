/* Naam......: RLE_W6838.sql
** Release...: W6838
** Datum.....: 06-01-2011 09:27:24
** Notes.....: Gegenereerd door ItsTools Versie 4.0.0.35
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W6838', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema voor RLE.rle_prsm_ind01.ind
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_mv_personen_1.vw
@@ RLE.rle_mv_personen_1.vw
prompt RLE.rle_prsm1_ind01.ind
@@ RLE.rle_prsm1_ind01.ind
prompt RLE.rle_prsm1_ind02.ind
@@ RLE.rle_prsm1_ind02.ind
prompt RLE.rle_prsm1_ind03.ind
@@ RLE.rle_prsm1_ind03.ind
prompt RLE.rle_prsm1_ind04.ind
@@ RLE.rle_prsm1_ind04.ind
prompt RLE.rle_prsm1_ind05.ind
@@ RLE.rle_prsm1_ind05.ind
prompt RLE.rle_mv_personen_2.vw
@@ RLE.rle_mv_personen_2.vw
prompt RLE.rle_prsm2_ind01.ind
@@ RLE.rle_prsm2_ind01.ind
prompt RLE.rle_prsm2_ind02.ind
@@ RLE.rle_prsm2_ind02.ind
prompt RLE.rle_prsm2_ind03.ind
@@ RLE.rle_prsm2_ind03.ind
prompt RLE.rle_prsm2_ind04.ind
@@ RLE.rle_prsm2_ind04.ind
prompt RLE.rle_prsm2_ind05.ind
@@ RLE.rle_prsm2_ind05.ind
prompt RLE.rle_mv_personen.syn
@@ RLE.rle_mv_personen.syn
prompt RLE.rle_mvr_prsm.pks
@@ RLE.rle_mvr_prsm.pks
prompt RLE.rle_mvr_prsm.pkb
@@ RLE.rle_mvr_prsm.pkb

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W6838 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W6838 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
