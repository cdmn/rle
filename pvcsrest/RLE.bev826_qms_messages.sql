DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-00348'
/
DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-00348'
/
INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind
                                   , suppr_always_ind, constraint_name)
     VALUES ( 'RLE-00348'
            , 'Waarde voor reeks bestaat niet'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00348', 'DUT'
            , 'Waarde voor reeks bestaat niet'
            , '');

DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-00350'
/
DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-00350'
/
INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind
                                   , suppr_always_ind, constraint_name)
     VALUES ( 'RLE-00350'
            , 'PTT postcodes mogen niet gewijzigd worden'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00350', 'DUT'
            , 'PTT postcodes mogen niet gewijzigd worden'
            , '');