REM ****************************************************************************
REM Naam      : STM.cre_directory.sql
REM Doel      : Definieren Oracle directory RLE_INBOX
REM -           Uitgangspunt is dat overeenkomstige directory RLE_OUTBOX reeds
REM -           bestaat.
REM -
REM -           Script moet worden gedraaid onder een schema met privilege voor
REM -           CREATE DIRECTORY.
REM -
REM Wijzigingshistorie:
REM Versie Naam                   Datum      Wijziging
REM ------ ---------------------- ---------- -----------------------------------
REM 1.0    E. Schillemans         02-03-2009 Creatie
REM ****************************************************************************

define pad=''

column pad noprint new_value pad

select replace(directory_path,'/outbox','/inbox') pad from all_directories
where  directory_name = 'RLE_OUTBOX';


create directory RLE_INBOX as '&pad';
grant read, write on  directory RLE_INBOX to COMP_RLE;
