delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_GEBDAT';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_GEBDAT';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_GEBDAT';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_GEBDAT';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_GEBDAT';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_GEBDAT';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_GEBDAT',1,'RLE',1
    ,'ophalen geboortedatum','F'
    ,'RLE_GET_GEBDAT',''
    ,'A',''
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GET_GEBDAT',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D1','RLE_GET_GEBDAT',1,'J'
    ,'returnvalue','OUT');
