delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0010F';
delete from stm_moduleparameters
    where mde_module = 'RLE0010F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0010F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0010F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0010F';
DELETE FROM stm_modules
    WHERE module = 'RLE0010F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0010F',1,'RLE',1
    ,'Zoeken relatie','S'
    ,'RLE0010F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('INTERFACE','RLE0010F',1,'N'
    ,'Interfacenaam','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('KNOPVERDER','RLE0010F',1,'N'
    ,'Knop verder uit','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('KNOPNIEUW','RLE0010F',1,'N'
    ,'Knop nieuw uit','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_VEST','RLE0010F',1,'N'
    ,'Vestiging','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NAN','RLE0010F',1,'N'
    ,'niet algemeen zoeken','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_SYS','RLE0010F',1,'N'
    ,'Systeem','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_KLANT','RLE0010F',1,'N'
    ,'Klantnummer','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_SOFI','RLE0010F',1,'N'
    ,'Sofinummer','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('EX_B_D','RLE0010F',1,'N'
    ,'EX_B_D','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_WGR','RLE0010F',1,'N'
    ,'Werkgever','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('EXT_NUMMER','RLE0010F',1,'N'
    ,'Extern nummer','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_PERSOON','RLE0010F',1,'N'
    ,'Persoonsnummer','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0010F',1,'J'
    ,'Relatienr','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0010F',1,'J'
    ,'Rol','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NULL',
        ':QMS$CTRL1.CGNBT_CHAR_FIELD152');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NR_ADMINKA',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'EXT_NUMMER',
        ':RLE.CGNBT_NAMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NR_PERSOON',
        ':RLE.CGNBT_PERSOONSNR');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NR_REL_RVM',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NR_REL_VTG',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NR_SOFI',
        ':rle.cgnbt_namrelatie');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NR_WGR',
        ':rle.cgnbt_namrelatie');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0010F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
