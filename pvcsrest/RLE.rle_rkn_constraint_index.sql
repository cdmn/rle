-- Scriptnaam: RLE_RKN_CONSTRAINT_INDEX.sql
-- Datum     : 08-12-2016
-- Auteur    : Elise Versteeg
-- Doel      : Opnieuw aanmaken van constraints en indexen zoals aanwezig op PPVD  voorafgaande aan installatie van RLE W11024



-- Eerst de statistics van de tabel aanmaken
analyze table COMP_RLE.RLE_RELATIE_KOPPELINGEN compute statistics;


-- Create/Recreate indexes 
create index COMP_RLE.RKN_RLE_FK_I on COMP_RLE.RLE_RELATIE_KOPPELINGEN (RLE_NUMRELATIE)
  tablespace RLE_C
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 44648K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index COMP_RLE.RKN_RLE_VOOR_FK_I on COMP_RLE.RLE_RELATIE_KOPPELINGEN (RLE_NUMRELATIE_VOOR)
  tablespace RLE_C
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 44648K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index COMP_RLE.RLE_RKN_IND1 on COMP_RLE.RLE_RELATIE_KOPPELINGEN (RLE_NUMRELATIE, RKG_ID, DAT_BEGIN, DAT_EINDE)
  tablespace RLE_C
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index COMP_RLE.RLE_RKN_RKG_FK_I on COMP_RLE.RLE_RELATIE_KOPPELINGEN (RKG_ID)
  tablespace RLE_C
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 44648K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table COMP_RLE.RLE_RELATIE_KOPPELINGEN
  add constraint RLE_RKN_PK primary key (ID)
  using index 
  tablespace RLE_C
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 44648K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table COMP_RLE.RLE_RELATIE_KOPPELINGEN
  add constraint RLE_RKN_UK1 unique (RLE_NUMRELATIE, RLE_NUMRELATIE_VOOR, DAT_BEGIN, RKG_ID)
  using index 
  tablespace RLE_C
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 107024K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table COMP_RLE.RLE_RELATIE_KOPPELINGEN
  add constraint RLE_RKN_RKG_FK1 foreign key (RKG_ID)
  references COMP_RLE.RLE_ROL_IN_KOPPELINGEN (ID);
alter table COMP_RLE.RLE_RELATIE_KOPPELINGEN
  add constraint RLE_RKN_RLE_FK1 foreign key (RLE_NUMRELATIE)
  references COMP_RLE.RLE_RELATIES (NUMRELATIE);
alter table COMP_RLE.RLE_RELATIE_KOPPELINGEN
  add constraint RLE_RKN_RLE_FK2 foreign key (RLE_NUMRELATIE_VOOR)
  references COMP_RLE.RLE_RELATIES (NUMRELATIE);
-- Create/Recreate check constraints 
alter table COMP_RLE.RLE_RELATIE_KOPPELINGEN
  add constraint RLE_RKN_CK1
  check (dat_einde is null
OR
(
dat_einde IS NOT NULL
AND dat_begin IS NOT NULL
AND TRUNC(dat_einde) >= TRUNC(dat_begin)
));


--toegevoegd op verzoek van ZJH (vanuit W11184) omdat creatie daar fout gegaan is
WHENEVER SQLERROR CONTINUE
DROP INDEX COMP_RLE.RLE_RKN_IND2;


CREATE INDEX COMP_RLE.RLE_RKN_IND2 ON COMP_RLE.RLE_RELATIE_KOPPELINGEN
(RKG_ID, DAT_BEGIN, DAT_EINDE)
TABLESPACE RLE_C
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           );


-- Grant/Revoke object privileges 
grant select on COMP_RLE.RLE_RELATIE_KOPPELINGEN to APP_PWG;
grant select on COMP_RLE.RLE_RELATIE_KOPPELINGEN to COMP_AVG with grant option;
grant select on COMP_RLE.RLE_RELATIE_KOPPELINGEN to COMP_QRT with grant option;
grant select on COMP_RLE.RLE_RELATIE_KOPPELINGEN to DBL_VANUIT_BI_ALG;
grant select on COMP_RLE.RLE_RELATIE_KOPPELINGEN to DBL_VANUIT_DWH_COMP_QRT;
grant select on COMP_RLE.RLE_RELATIE_KOPPELINGEN to INK_TO_AVG with grant option;
grant select on COMP_RLE.RLE_RELATIE_KOPPELINGEN to ROL_FACIVV01;