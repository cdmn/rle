set pages 5000
set trimspool on
set trimout on
set lines 5000
set verify off
set feedback off

column maximum_value new_value current_max_val
column increment_value new_value next_seq_val
column s1 noprint new_value current_seq_val

prompt
prompt
prompt ###########################################################################
prompt ##  sequence RLE_PERSOONSNUMMER_SEQ1 voor tabel COMP_GBA.PERSONEN.PERSOONSNUMMER
prompt ###########################################################################
select max(PERSOONSNUMMER) maximum_value from COMP_GBA.PERSONEN;
select RLE_PERSOONSNUMMER_SEQ1.nextval s1 from dual;
select case when ( &current_max_val - &current_seq_val ) > 1  then &current_max_val - &current_seq_val + 1  else 1 end increment_value from dual;
alter sequence COMP_RLE.RLE_PERSOONSNUMMER_SEQ1 increment by &next_seq_val;
select RLE_PERSOONSNUMMER_SEQ1.nextval s1 from dual;
alter sequence COMP_RLE.RLE_PERSOONSNUMMER_SEQ1 increment by 1;
select RLE_PERSOONSNUMMER_SEQ1.nextval new_sequence_value from dual;

