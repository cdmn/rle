prompt uitzetten trigger RLE_FNR_BRU en rle_fnr_as
  alter trigger rle_fnr_bru disable;
  alter trigger rle_fnr_as  disable;
--  
update rle_financieel_nummers
set bic = 'DEUTNL2A' 
where bic = 'DEUTNL2N'
;
-- aanzetten trigger RLE_FNR_BRU
  alter trigger rle_fnr_bru enable;
  alter trigger rle_fnr_as enable;

delete from cg_ref_codes 
where rv_domain = 'WIJZE VAN BETALING'
and (rv_low_value = 'AI' or rv_low_value ='AG')
;

