DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-00340'
/
DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-00340'
/
INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind
                                   , suppr_always_ind, constraint_name)
     VALUES ( 'RLE-00340'
            , 'Relatie <p1> <p2> heeft geen geldig adres van het soort <p3> '
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00340', 'DUT'
            , 'Relatie <p1> <p2> heeft geen geldig adres van het soort <p3> '
            , '');

DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-00342'
/
DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-00342'
/
INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind
                                   , suppr_always_ind, constraint_name)
     VALUES ( 'RLE-00342'
            , 'Er bestaat geen geldige relatiekoppeling tussen de relaties <p1> <p2> en <p3> <p4> '
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00342', 'DUT'
            , 'Er bestaat geen geldige relatiekoppeling tussen de relaties <p1> <p2> en <p3> <p4> '
            , '');

DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-00344'
/
DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-00344'
/
INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind
                                   , suppr_always_ind, constraint_name)
     VALUES ( 'RLE-00344'
            , 'Relatie <p1> <p2> is niet als contactpersoon bekend op het <p3> adres van relatie <p4> <p5> '
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00344', 'DUT'
            , 'Relatie <p1> <p2> is niet als contactpersoon bekend op het <p3> adres van relatie <p4> <p5> '
            , '');

DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-00346'
/
DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-00346'
/
INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind
                                   , suppr_always_ind, constraint_name)
     VALUES ( 'RLE-00346'
            , 'Combinatie van relaties niet mogelijk'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00346', 'DUT'
            , 'Combinatie van relaties niet mogelijk'
            , '');