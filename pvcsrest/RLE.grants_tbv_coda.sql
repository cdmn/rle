-- Scriptnaam: RLE.grants_tbv_coda.sql
-- Auteur    : Elise Versteeg
-- Doel      : Toekennen van execute rechten op RLE-SEPA items aan de user van de databaselink richting CODA database


grant execute on RLE_GET_FNR_AI_SEPA to DBL_VANUIT_CODA_CODAFIN;
grant execute on RLE_GET_FINNUM_SEPA to DBL_VANUIT_CODA_CODAFIN;
grant execute on RLE_M29_FINNUM_SEPA to DBL_VANUIT_CODA_CODAFIN;



