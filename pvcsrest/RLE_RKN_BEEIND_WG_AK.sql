delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RKN_BEEIND_WG_AK';
delete from stm_moduleparameters
    where mde_module = 'RLE_RKN_BEEIND_WG_AK';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RKN_BEEIND_WG_AK';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RKN_BEEIND_WG_AK';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RKN_BEEIND_WG_AK';
DELETE FROM stm_modules
    WHERE module = 'RLE_RKN_BEEIND_WG_AK';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RKN_BEEIND_WG_AK',1,'RLE',1
    ,'Beeindigen relatiekoppeling werkgever-administratiekantoor','P'
    ,'RLE_RKN_BEEIND_WG_AK',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_RKN_BEEIND_WG_AK',1,'J'
    ,'piv_num_wgr','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_RKN_BEEIND_WG_AK',1,'J'
    ,'piv_num_akr','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D3','RLE_RKN_BEEIND_WG_AK',1,'J'
    ,'pid_einddatum','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_RKN_BEEIND_WG_AK',1,'J'
    ,'pov_ind_verwerkt','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_RKN_BEEIND_WG_AK',1,'J'
    ,'pov_melding','OUT');
