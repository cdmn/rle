prompt ONDH_RELATIE
set define off
set heading off
set pagesize 0
set linesize 1000
set trimspool on
set echo off
set termout off
set feedback off
spool tempgif.sql
prompt prompt terugzetten gif
select 'insert into stm_gebruik_interfaces
 (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
 values
 ((select id from stm_interfaces where codzoek = ''ONDH_RELATIE''),1,'''||mde_module||''',1);'
 FROM stm_gebruik_interfaces g
 ,    stm_interfaces i
 WHERE g.ifv_ife_id = i.id and codzoek = 'ONDH_RELATIE';
spool off
spool tempdgb.sql
prompt prompt terugzetten dgb
select 'insert into aut_dienstengebruik
 (ife_id,fot_korte_naam)
 values
 ((select id from stm_interfaces where codzoek = ''ONDH_RELATIE''),'''||g.fot_korte_naam||''');'
 FROM aut_dienstengebruik g
 ,    stm_interfaces i
 WHERE g.ife_id = i.id and codzoek = 'ONDH_RELATIE';
spool off
SET termout ON
SET feedback ON
DELETE FROM  stm_gebruik_interfaces
    WHERE ifv_ife_id = (SELECT id FROM stm_interfaces WHERE codzoek = 'ONDH_RELATIE');
DELETE FROM aut_dienstengebruik
    WHERE ife_id = (SELECT id FROM stm_interfaces
         WHERE codzoek = 'ONDH_RELATIE');
DELETE FROM stm_wrdparameters
    WHERE mif_ifv_ife_id = (SELECT id FROM stm_interfaces
         WHERE codzoek = 'ONDH_RELATIE');
delete from stm_moduleinterfaces
    where ifv_ife_id = (select id from stm_interfaces
         where codzoek = 'ONDH_RELATIE');
delete from stm_abonnementen
    where ife_id = (select id from stm_interfaces
         where codzoek = 'ONDH_RELATIE');
delete from stm_interface_parameters
 where ifv_ife_id = (select id from stm_interfaces
         where codzoek = 'ONDH_RELATIE');
delete from stm_interface_versies
    where ife_id = (select id from stm_interfaces
         where codzoek = 'ONDH_RELATIE');
delete from stm_interfaces
    where codzoek = 'ONDH_RELATIE';
PROMPT interface
INSERT into stm_interfaces
    (ave_name,ave_numversie,codzoek,windowtitel,subtype,chk_null)
    values
    ('RLE','1',
    'ONDH_RELATIE','Onderhouden persoon (via venster)',
    'S','');
prompt interface versies
INSERT into stm_interface_versies
    (ife_id,numversie,codstatus)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_RELATIE')
    ,1,'A');
PROMPT module
prompt moduleinterface
INSERT into stm_moduleinterfaces
    (ifv_ife_id,ifv_numversie,volgnum,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_RELATIE')
    ,1,1
    ,'RLE0061F'
    ,1);
prompt wrdparameters
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_RELATIE'),1
    ,1,'NR_RELATIE'
    ,'RLE0061F',1,'NR_RELATIE'
    ,'','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_RELATIE'),1
    ,1,'SW_PRS'
    ,'RLE0061F',1,'NR_PERSOON'
    ,'','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_RELATIE'),1
    ,1,'CODE_ROL'
    ,'RLE0061F',1,'CODE_ROL'
    ,'','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_RELATIE'),1
    ,1,'SOFI_VERPL'
    ,'RLE0061F',1,'SOFI_VERPL'
    ,'','',NULL);
@tempgif
@tempdgb
set define on
