delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_NAW';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_NAW';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_NAW';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_NAW';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_NAW';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_NAW';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_NAW',1,'RLE',1
    ,'Ophalen naam relatie','P'
    ,'RLE_GET_NAW',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_NAW',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_NAW',1,'J'
    ,'P_NAAM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_NAW',1,'J'
    ,'P_VOORLETTERS','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_NAW',1,'J'
    ,'P_VOORVOEGSELS','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GET_NAW',1,'J'
    ,'P_RVORM','MOD');
