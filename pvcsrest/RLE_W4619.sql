/* Naam......: RLE_W4619.sql
** Release...: W4619
** Datum.....: 29-08-2008 15:37:47
** Notes.....: Gegenereerd door ITStools Versie 3.14
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4619', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema voor grants
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_QRT');
alter user COMP_QRT identified by COMP_QRT;
connect COMP_QRT/COMP_QRT@&&DBN
alter user COMP_QRT identified by values '&&_password';
column password clear
undefine _password
grant insert, update, delete, select on qrt_documentgegevens to comp_rle;

/* Switchen naar het juiste schema voor RLE.rle_aangemaakte_groepen.tab
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_aangemaakte_groepen.tab
@@ RLE.rle_aangemaakte_groepen.tab
prompt RLE.rle_gebruikte_voorwaarden.tab
@@ RLE.rle_gebruikte_voorwaarden.tab
prompt RLE.rle_groepsdefinities.tab
@@ RLE.rle_groepsdefinities.tab
prompt RLE.rle_relaties_in_groepen.tab
@@ RLE.rle_relaties_in_groepen.tab
prompt RLE.rle_standaard_voorwaarden.tab
@@ RLE.rle_standaard_voorwaarden.tab
prompt RLE.rle_agp_gde_fk.ind
@@ RLE.rle_agp_gde_fk.ind
prompt RLE.rle_gvw_gde_fk.ind
@@ RLE.rle_gvw_gde_fk.ind
prompt RLE.rle_gvw_sve_fk.ind
@@ RLE.rle_gvw_sve_fk.ind
prompt RLE.rle_rig_agp_fk.ind
@@ RLE.rle_rig_agp_fk.ind
prompt RLE.rle_rig_rle_fk.ind
@@ RLE.rle_rig_rle_fk.ind
prompt RLE.rle_agp_pk.con
@@ RLE.rle_agp_pk.con
prompt RLE.rle_gde_pk.con
@@ RLE.rle_gde_pk.con
prompt RLE.rle_gvw_pk.con
@@ RLE.rle_gvw_pk.con
prompt RLE.rle_rig_pk.con
@@ RLE.rle_rig_pk.con
prompt RLE.rle_sve_pk.con
@@ RLE.rle_sve_pk.con
prompt RLE.rig_agp_fk.con
@@ RLE.rig_agp_fk.con
prompt RLE.rig_rle_fk.con
@@ RLE.rig_rle_fk.con
prompt RLE.rle_agp_gde_fk.con
@@ RLE.rle_agp_gde_fk.con
prompt RLE.rle_gvw_gde_fk.con
@@ RLE.rle_gvw_gde_fk.con
prompt RLE.rle_gvw_sve_fk.con
@@ RLE.rle_gvw_sve_fk.con
prompt RLE.rle_sve_uk.con
@@ RLE.rle_sve_uk.con
prompt RLE.rle_agp_seq.sqs
@@ RLE.rle_agp_seq.sqs
prompt RLE.rle_gde_seq.sqs
@@ RLE.rle_gde_seq.sqs
prompt RLE.rle_rig_seq.sqs
@@ RLE.rle_rig_seq.sqs
prompt RLE.rle_sve_seq.sqs
@@ RLE.rle_sve_seq.sqs
set define off
prompt RLE.rle_cmp_brf.pks
@@ RLE.rle_cmp_brf.pks
set define on
set define off
prompt RLE.rle_cmp_sel.pks
@@ RLE.rle_cmp_sel.pks
set define on
set define off
prompt RLE.rle_cmp_brf.pkb
@@ RLE.rle_cmp_brf.pkb
set define on
set define off
prompt RLE.rle_cmp_sel.pkb
@@ RLE.rle_cmp_sel.pkb
set define on
prompt RLE.rle_agp_briu.trg
@@ RLE.rle_agp_briu.trg
prompt RLE.rle_gde_briu.trg
@@ RLE.rle_gde_briu.trg
prompt RLE.rle_gvw_briu.trg
@@ RLE.rle_gvw_briu.trg
prompt RLE.rle_rig_briu.trg
@@ RLE.rle_rig_briu.trg
prompt RLE.rle_sve_briu.trg
@@ RLE.rle_sve_briu.trg

/* Switchen naar het juiste schema voor RLE0040F.rol
*/
set define on
connect /@&&DBN

prompt RLE0040F.rol
@@ RLE0040F.rol
prompt RLE0041F.rol
@@ RLE0041F.rol
prompt RLE0042F.rol
@@ RLE0042F.rol

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4619 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W4619 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
