DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10410';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10410';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10410', 'Einddatum nationaliteit ligt voor begindatum nationaliteit', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Einddatum ligt voor begindatum', NULL, 'RLE-10410');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10411';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10411';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10411', 'Deze relatie is geen persoon', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Deze relatie is geen persoon', NULL, 'RLE-10411');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10412';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10412';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10412', 'Begindatum nieuwe periode ligt voor begindatum vorige periode', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Begindatum nieuwe periode ligt voor begindatum vorige periode', NULL, 'RLE-10412');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10413';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10413';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10413', 'Het personeelsnummer mag alleen uit cijfers bestaan.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Het personeelsnummer mag alleen uit cijfers bestaan.', NULL, 'RLE-10413');


