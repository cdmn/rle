create table comp_rle.rle_tech_data ( id          number primary key      
                                    , modulenaam  varchar2(50)  
                                    , begindatum  date
                                    , einddatum   date
                                    , script_id      number
                                    );
                                    
comment on table comp_rle.rle_tech_data is 'Registreert technische gegevens t.b.v. screen relaties';

create public synonym rle_tech_data for comp_rle.rle_tech_data;
