delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_ADRES';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_ADRES';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_ADRES';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_ADRES';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_ADRES';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_ADRES';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_ADRES',1,'RLE',1
    ,'ophalen adres','P'
    ,'RLE_GET_ADRES',''
    ,'A',''
    ,7);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_ADRES',1,'J'
    ,'P_NUMADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_ADRES',1,'J'
    ,'P_POSTCODE','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_GET_ADRES',1,'J'
    ,'P_HUISNR','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_ADRES',1,'J'
    ,'P_TOEVNUM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GET_ADRES',1,'J'
    ,'P_WOONPLAATS','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GET_ADRES',1,'J'
    ,'P_STRAAT','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GET_ADRES',1,'J'
    ,'P_LAND','MOD');
