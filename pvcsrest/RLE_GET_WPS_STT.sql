delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_WPS_STT';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_WPS_STT';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_WPS_STT';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_WPS_STT';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_WPS_STT';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_WPS_STT';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_WPS_STT',1,'RLE',1
    ,'RLE0902: Ophalen straat, woonplaats bij postcode, huisnr','P'
    ,'RLE_GET_WPS_STT',''
    ,'A',''
    ,4);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_GET_WPS_STT',1,'J'
    ,'P_PCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GET_WPS_STT',1,'J'
    ,'P_HUISNR','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_WPS_STT',1,'J'
    ,'P_WOONPLAATS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_WPS_STT',1,'J'
    ,'P_STRAAT','OUT');
