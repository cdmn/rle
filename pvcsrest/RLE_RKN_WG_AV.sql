delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RKN_WG_AV';
delete from stm_moduleparameters
    where mde_module = 'RLE_RKN_WG_AV';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RKN_WG_AV';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RKN_WG_AV';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RKN_WG_AV';
DELETE FROM stm_modules
    WHERE module = 'RLE_RKN_WG_AV';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RKN_WG_AV',1,'RLE',1
    ,'Toevoegen relatiekoppeling tussen werkgever en adminkantoor.','P'
    ,'RLE_RKN_WG_AV',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_RKN_WG_AV',1,'J'
    ,'P_WERKGEVERSNR','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_RKN_WG_AV',1,'J'
    ,'P_ADMINKANTOORNR','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D3','RLE_RKN_WG_AV',1,'J'
    ,'P_BEGINDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_RKN_WG_AV',1,'J'
    ,'P_IND_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_RKN_WG_AV',1,'N'
    ,'P_MELDING','OUT');
