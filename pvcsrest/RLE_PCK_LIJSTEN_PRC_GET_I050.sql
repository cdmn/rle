delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I050';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I050';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I050';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I050';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I050';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_GET_I050';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_GET_I050',1,'RLE',1
    ,'Ophalen contactpersonen (rij)','P'
    ,'RLE_PCK_LIJSTEN.PRC_GET_I050',''
    ,'A',''
    ,10);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_NUMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_OMS_FUNCTIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_OPGEMAAKTE_NAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_TELEFOONNUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_FAXNUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_EMAILADRES','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_COD_FUNCTIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_COD_GESLACHT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_OMS_GESLACHT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_PCK_LIJSTEN.PRC_GET_I050',1,'J'
    ,'P_MOBIELNUMMER','OUT');
