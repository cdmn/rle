SET lines         200
SET pages         80
SET pause         OFF
SET serveroutput  ON SIZE 1000000
SET trimspool     ON
SET termout      ON
SET feedback     ON
SET verify       OFF

COL CODROL FORMAT A8
COL OMSROL FORMAT A25
COL DWE_WRDDOM_SADRES FORMAT A20

PROMPT
Prompt opvoeren relatie rollen Levensloop
PROMPT

INSERT INTO RLE_ROLLEN 
( CODROL
, OMSROL
, INDWIJZIGBAAR
, INDSCHONEN
, INDGROEPSROL
, RLE_VROL_PLUS_ROL_CODROL
, PRIORITEIT
 ) VALUES 
 ( 'WA'
 , 'Werknemer Metalektro'
 , 'J'
 , 'N'
 , 'N'
 , 'P'
 , 90
 ); 
 --
  INSERT INTO RLE_ROLLEN 
( CODROL
, OMSROL
, INDWIJZIGBAAR
, INDSCHONEN
, INDGROEPSROL
, RLE_VROL_PLUS_ROL_CODROL
, PRIORITEIT
 ) VALUES 
 ( 'WB'
 , 'Werkgever Metalektro'
 , 'J'
 , 'N'
 , 'N'
 , 'P'
 , 91
 ); 
 --
 INSERT INTO RLE_ROLLEN 
( CODROL
, OMSROL
, INDWIJZIGBAAR
, INDSCHONEN
, INDGROEPSROL
, RLE_VROL_PLUS_ROL_CODROL
, PRIORITEIT
 ) VALUES 
 ( 'WC'
 , 'Werknemer MN Levensloop'
 , 'J'
 , 'N'
 , 'N'
 , 'P'
 , 92
 ); 
 --
 INSERT INTO RLE_ROLLEN 
( CODROL
, OMSROL
, INDWIJZIGBAAR
, INDSCHONEN
, INDGROEPSROL
, RLE_VROL_PLUS_ROL_CODROL
, PRIORITEIT
 ) VALUES 
 ( 'WD'
 , 'Werkgever MN Levensloop'
 , 'J'
 , 'N'
 , 'N'
 , 'P'
 , 93
 ); 
 --
 SELECT CODROL
 ,      OMSROL
 FROM   RLE_ROLLEN
 WHERE  CODROL IN ('WA', 'WB', 'WC', 'WD')
 ;
 
 PROMPT
 PROMPT Opvoeren soort adresrollen Levensloop
 prompt
 
 INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WA'
 , 1
 , 'OA'
 )
 ;
 --
 INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WB'
 , 1
 , 'FA'
 ); 
 --
 INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WB'
 , 2
 , 'CA'
 ); 
 --
 INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WB'
 , 3
 , 'SZ'
 ); 
 --
 INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WC'
 , 1
 , 'OA'
 ); 
 --
  INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WD'
 , 1
 , 'FA'
 ); 
 --
 INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WD'
 , 2
 , 'CA'
 ); 
 --
 INSERT INTO RLE_SADRES_ROLLEN 
 ( CODROL
 , VOLGNR
 , DWE_WRDDOM_SADRES
 ) VALUES 
 ( 'WD'
 , 3
 , 'SZ'
 ); 
 --
 select codrol
 ,      DWE_WRDDOM_SADRES
 from   RLE_SADRES_ROLLEN 
 where  CODROL IN ('WA', 'WB', 'WC', 'WD')
 ;
 PROMPT
 PROMPT EINDE: Opvoeren relatie rollen en soort adresrollen Levensloop
 PROMPT
 
 

 
