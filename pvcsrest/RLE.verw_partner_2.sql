WHENEVER SQLERROR CONTINUE
WHENEVER OSERROR  CONTINUE

alter table comp_rle.RLE_RELATIE_KOPPELINGEN disable all triggers;

update RLE_RELATIE_KOPPELINGEN
set rkg_id = (select id from RLE_ROL_IN_KOPPELINGEN where CODE_ROL_IN_KOPPELING = 'H')
where rkg_id = (select id from RLE_ROL_IN_KOPPELINGEN where CODE_ROL_IN_KOPPELING = 'G');

delete
from RLE_ROL_IN_KOPPELINGEN
where id=(select id from RLE_ROL_IN_KOPPELINGEN where CODE_ROL_IN_KOPPELING = 'G');

alter table comp_rle.RLE_RELATIE_KOPPELINGEN enable all triggers;

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

