delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0145F';
delete from stm_moduleparameters
    where mde_module = 'RLE0145F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0145F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0145F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0145F';
DELETE FROM stm_modules
    WHERE module = 'RLE0145F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0145F',1,'RLE',1
    ,'Selecteren land','S'
    ,'RLE0145F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_LAND','RLE0145F',1,'N'
    ,'Landcode','OUT');
