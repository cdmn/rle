/* Naam......: RLE_W7576.sql
** Release...: W7576
** Datum.....: 08-12-2011 11:19:25
** Notes.....: Gegenereerd door ItsTools Versie 4.0.2.2
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W7576', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste schema voor RLE.rle_brieven_kvk.tab
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_brieven_kvk.tab
@@ RLE.rle_brieven_kvk.tab
prompt RLE.rle_bkk_ind1.ind
@@ RLE.rle_bkk_ind1.ind
set define off
prompt RLE.rle_aanmaken_br_opvr_kvk.pks
@@ RLE.rle_aanmaken_br_opvr_kvk.pks
set define on
set define off
prompt RLE.rle_aanmaken_br_opvr_kvk.pkb
@@ RLE.rle_aanmaken_br_opvr_kvk.pkb
set define on

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W7576 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W7576 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
