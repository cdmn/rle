delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_M03_REC_EXTCOD';
delete from stm_moduleparameters
    where mde_module = 'RLE_M03_REC_EXTCOD';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_M03_REC_EXTCOD';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_M03_REC_EXTCOD';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_M03_REC_EXTCOD';
DELETE FROM stm_modules
    WHERE module = 'RLE_M03_REC_EXTCOD';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_M03_REC_EXTCOD',1,'RLE',1
    ,'.','F'
    ,'RLE_M03_REC_EXTCOD',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_M03_REC_EXTCOD',1,'J'
    ,'P_DATPARM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_M03_REC_EXTCOD',1,'J'
    ,'RETURNVALUE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_M03_REC_EXTCOD',1,'J'
    ,'P_SYSTEEMCOMP','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_M03_REC_EXTCOD',1,'J'
    ,'P_RELNUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_M03_REC_EXTCOD',1,'J'
    ,'P_CODROL','INP');
