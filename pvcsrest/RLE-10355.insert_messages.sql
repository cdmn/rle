INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10355', 'De periode van deze administratiekantoor, curator of bewindvoerder overlapt met een andere relatiekoppeling voor deze werkgever.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10355', 'DUT'
            , 'De periode van deze administratiekantoor, curator of bewindvoerder overlapt met een andere relatiekoppeling voor deze werkgever.'
            , '');

