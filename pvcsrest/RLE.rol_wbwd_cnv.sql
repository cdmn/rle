------------------------------------------------------------------------------
-- Programma : RLE.rol_wbwd_cnv.sql
-- Functie   : 
-- Auteur    : XMB
-- Datum     : 26-01-2009
-- Doel      : Vervangen van WB, WD rollen door WG.
--             tbv Levensloop.
-- Aangepast : 04-06-2009 Lijst voor en lijst na toegevoegd.
--
----------------------------------------------------------------------------
set serveroutput on size 1000000
set pagesize 2000
SET linesize 10000
SET heading off
SET feedback off
SET pause off
SET verify off
SET trimspool on
SET colsep ';'

prompt ***********************************************************************************
prompt ***                        START                                      ***
prompt ***********************************************************************************

delete from lijsten
where       script_naam = 'ROL_WBWD_CNV'
and         programma_naam = 'ROL_WBWD_CNV'
/

col RUN_DAT new_value RUN_DAT noprint
select to_char (sysdate, 'DDMMYYYYHH24MISS') run_dat
from   dual;

spool ROL_WBWD_CNV_&&RUN_DAT

prompt
prompt
prompt De beginsituatie....
prompt ***********************************************************************************
prompt
prompt

select 'Relatienummer;Werkgeversnummer;Rol;Naam;Handelsnaam;Kvk nummer'
from dual
/

select distinct to_char(rle.NUMRELATIE) Relatienummer
,      ( select rec.extern_relatie
         from   rle_relatie_externe_codes rec
         where  rle.numrelatie = rec.rle_numrelatie
         and    rec.rol_codrol = 'WG'
         and    rec.DWE_WRDDOM_EXTSYS = 'WGR'
       ) Werkgeversnummer
,      rrl.rol_codrol Code_rol
,      rle.NAMRELATIE Naam
,      rle.HANDELSNAAM Handelsnaam
,      ( select rec.extern_relatie
         from   rle_relatie_externe_codes rec
         where  rle.numrelatie = rec.rle_numrelatie
         and    rec.rol_codrol = 'WG'
         and    rec.DWE_WRDDOM_EXTSYS = 'KVK'
       ) Kvk_nummer          
from   rle_relaties rle
,      rle_relatie_rollen rrl
where  rle.NUMRELATIE = rrl.RLE_NUMRELATIE
and    rrl.rol_codrol in ('WB','WD')
order by rle.NUMRELATIE
/


declare
  procedure p_werkgevers
  is
    --
    TYPE t_rrl_rec IS RECORD
      (   numrelatie     rle_relaties.numrelatie%type
      ,   bedrijfsnaam   rle_relaties.namrelatie%type
      ,   melding        varchar2(4000)
      );
    TYPE t_rrl_tab IS TABLE OF t_rrl_rec INDEX BY BINARY_INTEGER;
    t_rrl                         t_rrl_tab;
    --
    cursor c_rle  (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select rle.numrelatie     
    ,      rle.namrelatie     bedrijfsnaam
    ,      rle.handelsnaam
    from   rle_relaties rle
    where  rle.numrelatie = b_numrelatie;
    r_rle                         c_rle%rowtype;
    --
    cursor c_rrl
    is
    select rrl.rle_numrelatie
    ,      rrl.rol_codrol
    from   rle_relatie_rollen rrl
    where  rrl.ROL_CODROL in ('WB','WD');
    --
    r_rrl                         c_rrl%rowtype;
    --
    cursor c_rrl2 (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select rrl.rle_numrelatie
    from   rle_relatie_rollen rrl
    where  rrl.rle_numrelatie = b_numrelatie
    and    rrl.rol_codrol = 'WG';
    --
    r_rrl2                        c_rrl2%rowtype;
    --  
    cursor c_rie_alg (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatierollen_in_adm rie
    where  rie.rle_numrelatie = b_numrelatie
    and    rie.rol_codrol = 'WG'
    and    rie.adm_code   = 'ALG';
    --
    r_rie_alg                     c_rie_alg%rowtype;
    --
    cursor c_rie_ll (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatierollen_in_adm rie
    where  rie.rle_numrelatie = b_numrelatie
    and    rie.rol_codrol = 'WG'
    and    rie.adm_code in ('MELL','MNLL','MTLL');
    --
    r_rie_ll                      c_rie_ll%rowtype;
    --
    cursor c_ras (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatie_adressen ras
    where  ras.rle_numrelatie = b_numrelatie
    and    ras.ROL_CODROL in ('WB','WD')
    and    ras.dateinde is null
    and    exists
      (select ras2.rle_numrelatie
       from   rle_relatie_adressen ras2
       where  ras2.rle_numrelatie    = ras.rle_numrelatie
       and    ras2.ROL_CODROL        = 'WG'
       and    ras2.DWE_WRDDOM_SRTADR = ras.DWE_WRDDOM_SRTADR
       and    ras2.ads_numadres      != ras.ads_numadres
       and    ras2.dateinde is null
      );
    --
    r_ras                         c_ras%rowtype;
    --  
    --Cursor voor update records met alleen een WB/WD rol, dateinde is not null..
    cursor c_ras21 (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatie_adressen ras
    where  ras.rle_numrelatie = b_numrelatie
    and    ras.ROL_CODROL in ('WB','WD')
    and    ras.dateinde is not null
    and    ras.rle_numrelatie not in
     (select ras2.rle_numrelatie
      from   rle_relatie_adressen ras2
      where  ras2.ROL_CODROL = 'WG'
      and    ras2.DWE_WRDDOM_SRTADR = ras.DWE_WRDDOM_SRTADR
     )
    for update of ras.rol_codrol;
    --
    --Cursor voor update records met alleen een WB/WD rol, dateinde is null.
    cursor c_ras22 (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatie_adressen ras
    where  ras.rle_numrelatie = b_numrelatie
    and    ras.ROL_CODROL in ('WB','WD')
    and    ras.dateinde is null
    and    ras.rle_numrelatie not in
     (select ras2.rle_numrelatie
      from   rle_relatie_adressen ras2
      where  ras2.ROL_CODROL = 'WG'
      and    ras2.dateinde is null
      and    ras2.DWE_WRDDOM_SRTADR = ras.DWE_WRDDOM_SRTADR
     )
    for update of ras.rol_codrol;
    --
    --update datingang van WG records met zelfde ads_numadres.
    cursor c_ras3 (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatie_adressen ras
    where  ras.ROL_CODROL = 'WG'
    and    ras.rle_numrelatie    = b_numrelatie
    and    exists
     (select ras2.*
      from   rle_relatie_adressen ras2
      where  ras2.ROL_CODROL in ('WB','WD')
      and    ras2.DWE_WRDDOM_SRTADR = ras.DWE_WRDDOM_SRTADR
      and    ras2.rle_numrelatie    = ras.rle_numrelatie
      and    ras2.ads_numadres      = ras.ads_numadres
      and    ras2.datingang         < ras.DATINGANG
     )
    for update of ras.datingang;
    --
    --Cursor voor update records met alleen een WB/WD rol.
    cursor c_rec (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatie_externe_codes rec
    where  rec.rle_numrelatie = b_numrelatie
    and    rec.ROL_CODROL in ('WB','WD')
    and    rec.rle_numrelatie not in
      (select rec2.rle_numrelatie
       from   rle_relatie_externe_codes rec2
       where  rec2.ROL_CODROL = 'WG'
       and    rec2.DWE_WRDDOM_EXTSYS = rec.DWE_WRDDOM_EXTSYS
      )
    for update of rec.rol_codrol;
    --
    --update datingang van WG records.
    cursor c_rec2 (b_numrelatie in rle_relaties.numrelatie%type)
    is
    select *
    from   rle_relatie_externe_codes rec
    where  rec.rle_numrelatie = b_numrelatie
    and    rec.ROL_CODROL = 'WG'
    and    exists
      (select 1
       from   rle_relatie_externe_codes rec2
       where  rec2.ROL_CODROL        in ( 'WB', 'WD')
       and    rec2.DWE_WRDDOM_EXTSYS = rec.DWE_WRDDOM_EXTSYS
       and    rec2.rle_numrelatie    = rec.rle_numrelatie
       and    rec2.datingang         < rec.DATINGANG
      )
    for update of rec.datingang;
    --
    l_numrelatie                  number (10);
    l_error                       varchar2 (32767);
    l_teller                      number default 0;
    l_teller_correct              number default 0;
    l_teller_fout                 number default 0;
    l_adm_code                    rle_administraties.code%type;
    l_doorgaan                    varchar2 (1) default 'J';
    l_lijstnummer                 number := 1;
    l_regelnummer                 number := 0;
    l_error_ex                    varchar2 (32767);
  begin
    stm_util.t_procedure       := 'ROL_WBWD_CNV';
    stm_util.t_script_naam     := 'ROL_WBWD_CNV';
    stm_util.t_script_id       := 1;
    stm_util.t_programma_naam  := 'ROL_WBWD_CNV';
    
    -- initialiseren tabtype
    t_rrl.delete;
    
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Muteren werkgevers Levensloop'
                                                   || chr (10)
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           => ''
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           => ''
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Datum : '
                                                   || to_char (sysdate, 'DD-MM-YYYY')
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Tijd  : '
                                                   || to_char (sysdate, 'HH24:MI')
                            );
    --
    execute immediate 'alter trigger comp_rle.rle_ras_as disable';
    execute immediate 'alter trigger comp_rle.rle_ras_brd disable';
    execute immediate 'alter trigger comp_rle.rle_ras_bru disable';
    --                                
    for r_rrl in c_rrl
    loop
        savepoint muteren_relatie;
        
        l_teller                    :=   l_teller + 1;
        -- in de tabtype gaan we meldingen bijhouden
        l_numrelatie                := r_rrl.rle_numrelatie;
        t_rrl (l_teller).numrelatie := r_rrl.rle_numrelatie;
        l_error                     := null;
        l_error_ex                  := null;
        l_doorgaan                  := 'J';
                
        begin
          --
          open  c_rle (l_numrelatie);
          fetch c_rle into r_rle;
          --
          t_rrl (l_teller).bedrijfsnaam := r_rle.bedrijfsnaam;
          if length (r_rle.handelsnaam) > 35
          then
            l_doorgaan                 := 'N';
            l_error                    := 'Bedrijfsnaam groter dan 35. Bedrijfsnaam: '|| r_rle.bedrijfsnaam||' ,Relatie: '
                                            || to_char(l_numrelatie);
          end if;
          close c_rle;
          --                 
        exception
            when others
              then
              l_doorgaan                 := 'N';
              l_error                    := 'Fout opgetreden in C_RLE, Relatie: '
                                            || to_char(l_numrelatie) || chr (10) || sqlerrm;
        end;
        --
        --STAP INSERT
        if l_doorgaan = 'J'
        then
          begin
            --
            open  c_rrl2 (l_numrelatie);
            fetch c_rrl2 into r_rrl2;
            --
            if c_rrl2%notfound 
            then
            rle_rrl.ins_rrl
              ( l_numrelatie
              , 'WG'
              );
            end if;
            close c_rrl2;
            --                 
          exception
              when others
              then
                if c_rrl2%isopen
                then
                  close c_rrl2;
                end if;
                l_doorgaan                 := 'N';
                l_error                    := 'Toevoegen van relaties in RRL mislukt. Rol: WG, Relatie: '
                                            || to_char(l_numrelatie)  || chr (10) || sqlerrm;
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            open  c_rie_alg (l_numrelatie);
            fetch c_rie_alg into r_rie_alg;
            --
            if c_rie_alg%notfound 
            then
              insert into rle_relatierollen_in_adm( adm_code
                                                  , rol_codrol
                                                  , rle_numrelatie
                                                  )
              values ( 'ALG'
                     , 'WG'
                     , l_numrelatie
                     );
              --
            end if;
            close c_rie_alg;
            --            
          exception
            when others
            then
              if c_rie_alg%isopen
              then
                close c_rie_alg;
              end if;
              l_error                    :=    'Toevoegen van relaties in RIE mislukt. Rol: WG, Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            open  c_rie_ll (l_numrelatie);
            fetch c_rie_ll into r_rie_ll;
            --
            if c_rie_ll%notfound 
            then
              l_adm_code := case when r_rrl.rol_codrol = 'WB' then 'MELL'
                                 else 'MNLL'
                            end;
              insert into rle_relatierollen_in_adm( adm_code
                                                  , rol_codrol
                                                  , rle_numrelatie
                                                  )
              values ( l_adm_code
                     , 'WG'
                     , l_numrelatie
                     );
              --
            end if;
            close c_rie_ll;
            --            
          exception
            when others
            then
              if c_rie_ll%isopen
              then
                close c_rie_ll;
              end if;
              l_error                    :=    'Toevoegen van relaties in RIE mislukt. Rol: WG, Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            open  c_ras (l_numrelatie);
            fetch c_ras into r_ras;
            --
            if c_ras%found 
            then
              l_error                    :=    'verschillende aktieve adressen gevonden. Relatie: '
                                               || to_char(l_numrelatie);
              l_doorgaan                 := 'N';
              --
            end if;
            close c_ras;
            --            
          exception
            when others
            then
              if c_ras%isopen
              then
                close c_ras;
              end if;
              l_error                    :=    'Fout opgetreden in C_RAS. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        --STAP UPDATE
        if l_doorgaan = 'J'
        then
          begin
            --
            for r_ras21 in c_ras21 (l_numrelatie)
            loop
              --
              update rle_relatie_adressen ras
              set    ras.ROL_CODROL = 'WG'
              where current of c_ras21;
              --
            end loop;
            --            
          exception
            when others
            then
              if c_ras21%isopen
              then
                close c_ras21;
              end if;
              l_error                    :=    'Update van rol in RAS mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            for r_ras22 in c_ras22 (l_numrelatie)
            loop
              --
              update rle_relatie_adressen ras
              set    ras.ROL_CODROL = 'WG'
              where current of c_ras22;
              --
            end loop;
            --
          exception
            when others
            then
              if c_ras22%isopen
              then
                close c_ras22;
              end if;
              l_error                    :=    'Update van rol in RAS mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            for r_ras3 in c_ras3 (l_numrelatie)
            loop
              --
              update rle_relatie_adressen ras
              set    ras.DATINGANG =
               ( select ras.datingang
                 from   rle_relatie_adressen ras
                 where  ras.ROL_CODROL in ('WB','WD')
                 and    ras.rle_numrelatie    = r_ras3.rle_numrelatie
                 and    ras.ads_numadres      = r_ras3.ads_numadres
                 and    ras.DWE_WRDDOM_SRTADR = r_ras3.DWE_WRDDOM_SRTADR
                 and    ras.datingang         < r_ras3.DATINGANG
               )
              where current of c_ras3;
              --
            end loop;
            --            
          exception
            when others
            then
              if c_ras3%isopen
              then
                close c_ras3;
              end if;
              l_error                    :=    'Update van datingang in RAS mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            for r_rec in c_rec (l_numrelatie)
            loop
              --
              update rle_relatie_externe_codes rec
              set    rec.ROL_CODROL = 'WG'
              where current of c_rec;
              --
            end loop;
            --            
          exception
            when others
            then
              if c_rec%isopen
              then
                close c_rec;
              end if;
              l_error                    :=    'Update van rol in REC mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            for r_rec2 in c_rec2 (l_numrelatie)
            loop
              --
              update rle_relatie_externe_codes rec
              set    rec.DATINGANG =
               ( select rec.datingang
                 from   rle_relatie_externe_codes rec
                 where  rec.ROL_CODROL in ('WB','WD')
                 and    rec.rle_numrelatie    = r_rec2.rle_numrelatie
                 and    rec.DWE_WRDDOM_EXTSYS = r_rec2.DWE_WRDDOM_EXTSYS
                 and    rec.datingang         < r_rec2.DATINGANG
               )
              where current of c_ras3;
             --
            end loop;
            --            
          exception
            when others
            then
              if c_rec2%isopen
              then
                close c_rec2;
              end if;
              l_error                    :=    'Update van datingang in REC mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        --STAP DELETE
        if l_doorgaan = 'J'
        then
          begin
            --
            --Verwijderen van W* records uit RAS met zelfde numadres als WG records.
            delete
            from   rle_relatie_adressen ras
            where  ras.ROL_CODROL in ('WB','WD')
            and    ras.rle_numrelatie = l_numrelatie;
            --            
          exception
            when others
            then
              l_error                    :=    'Delete van records in RAS mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            --Verwijderen van W* record uit RIE.
            delete
            from   rle_relatierollen_in_adm rie
            where  rie.ROL_CODROL in ('WB','WD')
            and    rie.rle_numrelatie = l_numrelatie;
            --            
          exception
            when others
            then
              l_error                    :=    'Delete van record in RIE mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            --Verwijderen van W* records uit REC.
            delete
            from   rle_relatie_externe_codes rec
            where  rec.ROL_CODROL in ('WB','WD')
            and    rec.rle_numrelatie = l_numrelatie;
            --            
          exception
            when others
            then
              l_error                    :=    'Delete van record in REC mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        if l_doorgaan = 'J'
        then
          begin
            --
            --Verwijderen van W* record uit RRL.
            delete
            from   rle_relatie_rollen rrl
            where  rrl.ROL_CODROL in ('WB','WD')
            and    rrl.rle_numrelatie = l_numrelatie;
            --            
          exception
            when others
            then
              l_error                    :=    'Delete van record in RRL mislukt. Relatie: '
                                               || to_char(l_numrelatie)  || chr (10) || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;
        --
        --
        if l_doorgaan = 'N'
        then
          rollback to muteren_relatie;
          l_teller_fout              :=   l_teller_fout + 1;
          -- foutmelding registreren
          -- melding in tabtype stoppen zodat deze later in de tabel kan worden geschreven
          t_rrl (l_teller).melding   := substr (  l_error_ex
                                                || ' - '
                                                ||l_error
                                              , 1
                                              , 4000
                                               );
        else
          l_teller_correct           :=   l_teller_correct + 1;
          t_rrl (l_teller).melding   := substr (l_error_ex||l_error
                                              , 1
                                              , 4000
                                               );
        end if;                                                                             
    end loop;
    --
    if l_doorgaan = 'J'
    then
      begin
        --
        --Verwijderen van W* record uit RRL.
        delete 
        from   RLE_SADRES_ROLLEN
        where  CODROL in ('WA','WB','WC','WD');
        --            
      exception
        when others
        then
          l_error                    :=    'Delete van record in RRL mislukt. Relatie: '
                                           || to_char(l_numrelatie)  || chr (10) || sqlerrm;
          l_doorgaan                 := 'N';
      end;
    end if;
    --
    --
    if l_doorgaan = 'J'
    then
      begin
        --
        --Verwijderen van W* record uit ROL.
        delete 
        from   RLE_ROLLEN
        where  CODROL in ('WA','WB','WC','WD');
        --            
      exception
        when others
        then
          l_error                    :=    'Delete van record in RRL mislukt. Relatie: '
                                           || to_char(l_numrelatie)  || chr (10) || sqlerrm;
          l_doorgaan                 := 'N';
      end;
    end if;
    --
    --rollback;  
    --
    execute immediate 'alter trigger comp_rle.rle_ras_as enable';
    execute immediate 'alter trigger comp_rle.rle_ras_brd enable';
    execute immediate 'alter trigger comp_rle.rle_ras_bru enable';
    --   
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Opgetreden fouten: '
                                                     || l_teller_fout
                                                     || chr (10)
                              );

    if t_rrl.count > 0
    then
        for i in t_rrl.first .. t_rrl.last
        loop
          if t_rrl (i).melding is not null
          then
            stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                                   , il_regelnummer     => l_regelnummer
                                   , il_tekst           => substr (   t_rrl (i).bedrijfsnaam
                                                                   || ' '
                                                                   || t_rrl (i).melding
                                                                 , 1
                                                                 , 255
                                                                  )
                                    );
          end if;
        end loop;
    end if;

    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Aantal niet verwerkte relaties: '
                                                     || l_teller_fout
                                                     || chr (10)
                              );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Aantal verwerkte relaties : '
                                                     || l_teller_correct
                              );
   
  exception
    when others
    then
      raise_application_error (-20000, 'ERROR - '
                                || sqlerrm);
  end;

begin
  p_werkgevers;
end;
/

prompt
prompt
prompt De eindsituatie....
prompt ***********************************************************************************
prompt
prompt

select distinct to_char(rle.NUMRELATIE) Relatienummer
,      ( select rec.extern_relatie
         from   rle_relatie_externe_codes rec
         where  rle.numrelatie = rec.rle_numrelatie
         and    rec.rol_codrol = 'WG'
         and    rec.DWE_WRDDOM_EXTSYS = 'WGR'
       ) Werkgeversnummer
,      rrl.rol_codrol Code_rol
,      rle.NAMRELATIE Naam
,      rle.HANDELSNAAM Handelsnaam
,      ( select rec.extern_relatie
         from   rle_relatie_externe_codes rec
         where  rle.numrelatie = rec.rle_numrelatie
         and    rec.rol_codrol = 'WG'
         and    rec.DWE_WRDDOM_EXTSYS = 'KVK'
       ) Kvk_nummer          
from   rle_relaties rle
,      rle_relatie_rollen rrl
where  rle.NUMRELATIE = rrl.RLE_NUMRELATIE
--and    rrl.rol_codrol = 'WG'
and    exists
  (select 1
   from   rle_relatie_rollen rrl2
   where  rrl2.rle_NUMRELATIE = rle.NUMRELATIE
   and    rrl2.rol_codrol in ('WB','WD')
  )
order by rle.NUMRELATIE
/

prompt
prompt
prompt De inhoud van de lijstentabel tonen....
prompt ***********************************************************************************


select   tekst
from     lijsten
where    script_naam    = 'ROL_WBWD_CNV'
and      programma_naam = 'ROL_WBWD_CNV'
order by regelnummer
/
prompt
prompt ***********************************************************************************
prompt
prompt De inhoud van de lijstentabel verwijderen...
prompt
delete from lijsten
where       script_naam = 'ROL_WBWD_CNV'
and         programma_naam = 'ROL_WBWD_CNV'
/
prompt ***********************************************************************************
prompt ***                        EINDE ROL_WBWD_CNV                                   ***
prompt ***********************************************************************************
prompt

spool OFF
