/* Naam......: RLE_W2158_HERA.sql
** Release...: W2158
** Datum.....: Fri Mar  7 12:50:37 2003
** Notes.....: Aangemaakt met Des_tool_mn Versie 3.1.4.1.5.9d 


*/

set linesize   79
set pause     off


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma beeindigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er vanuit JCS wordt gestart.
   Hier wordt er vanuit gegaan dat vanuit JCS de user OPS$xJCS wordt gebruikt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(user,6),'JCS','exit','continue') P_EXIT
from dual;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C en RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    into dummy
    from dba_users
   where username = 'COMP_RLE';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE bestaat niet in deze database,
*          neem contact op met FIT-be-op !');
end;
/

/* Voor databases afgeleid van ZEUS, dus aangemaakt vanuit EROS
   moeten we het versie-nummer vullen in de table IMU_APPLICATIE_VERSIE
   Voor database afgeleid van HERA, dus aangemaakt vanuit DKRE
   moeten we het versie-nummer vullen in de tabel STM_VERSIE_WIJZIGINGEN
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W2158', sysdate );

commit;


/* Cross grants tussen de COMP_-users onderling deel 1
*/
REM start RLE_W2158_grants1.sql

/* Switchen naar het juiste schema
*/
connect /@&&DBN
set define on
set verify off
define _password = "SU_PASSWORD"
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_AUT');
alter user COMP_AUT identified by COMP_AUT;
connect COMP_AUT/COMP_AUT@&&DBN
alter user COMP_AUT identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT


REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
REM Hier de files zetten die vanuit deze directory aangeroepen moeten worden.

@@ RLE0010F_DIENSTENGEBRUIK.sql
@@ RLE0300F_DIENSTENGEBRUIK.sql

REM Tot zover de inhoud van de directory
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* Cross grants tussen de COMP_-users onderling deel 2
*/
connect /@&&DBN
REM start RLE_W2158_grants2.sql

/* creatie database-rollen
*/
connect /@&&DBN
REM start RLE.????.rol

/* RLE_REF_CODES vullen en check constraints plaatsen
*/
connect /@&&DBN
REM start RLE.????.dom




insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W2158 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W2158 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

