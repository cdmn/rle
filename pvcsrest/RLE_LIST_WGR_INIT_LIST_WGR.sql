delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_LIST_WGR.INIT_LIST_WGR';
delete from stm_moduleparameters
    where mde_module = 'RLE_LIST_WGR.INIT_LIST_WGR';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_LIST_WGR.INIT_LIST_WGR';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_LIST_WGR.INIT_LIST_WGR';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_LIST_WGR.INIT_LIST_WGR';
DELETE FROM stm_modules
    WHERE module = 'RLE_LIST_WGR.INIT_LIST_WGR';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_LIST_WGR.INIT_LIST_WGR',1,'RLE',1
    ,'Initieren lijst werkgevers ter controle bestaan','P'
    ,'RLE_LIST_WGR.INIT_LIST_WGR',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_LIST_WGR.INIT_LIST_WGR',1,'J'
    ,'P_NAAM_WERKGEVER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_LIST_WGR.INIT_LIST_WGR',1,'N'
    ,'P_HANDELSNAAM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_LIST_WGR.INIT_LIST_WGR',1,'J'
    ,'P_POSTCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N4','RLE_LIST_WGR.INIT_LIST_WGR',1,'J'
    ,'P_HUISNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_LIST_WGR.INIT_LIST_WGR',1,'N'
    ,'P_TOEVHUISNR','INP');
