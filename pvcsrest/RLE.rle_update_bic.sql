prompt uitzetten trigger rle_fnr_bru en rle_fnr_as
  alter trigger rle_fnr_bru disable;
  alter trigger rle_fnr_as  disable;
--  
update rle_financieel_nummers
set bic = 'DEUTNL2N' 
where bic = 'DEUTNL2A'
and numrekening not like '265%'
and substr (iban, 5,30) not like 'DEUT0265%'
;
prompt aanzetten trigger RLE_FNR_BRU
  alter trigger rle_fnr_bru enable;
  alter trigger rle_fnr_as enable;


