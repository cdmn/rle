delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_M14_RRL_TOEV';
delete from stm_moduleparameters
    where mde_module = 'RLE_M14_RRL_TOEV';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_M14_RRL_TOEV';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_M14_RRL_TOEV';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_M14_RRL_TOEV';
DELETE FROM stm_modules
    WHERE module = 'RLE_M14_RRL_TOEV';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_M14_RRL_TOEV',1,'RLE',1
    ,'Registreren rol bij relatie','P'
    ,'RLE_M14_RRL_TOEV',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_M14_RRL_TOEV',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_M14_RRL_TOEV',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_M14_RRL_TOEV',1,'J'
    ,'P_CODSYSTEEMCOMP','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_M14_RRL_TOEV',1,'J'
    ,'P_CODERELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_M14_RRL_TOEV',1,'J'
    ,'P_MELDING','MOD');
