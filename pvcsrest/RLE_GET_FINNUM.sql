delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_FINNUM';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_FINNUM';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_FINNUM';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_FINNUM';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_FINNUM';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_FINNUM';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_FINNUM',1,'RLE',1
    ,'Ophalen financieel nummer','F'
    ,'RLE_GET_FINNUM',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_FINNUM',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_FINNUM',1,'J'
    ,'P_SRTREK','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_GET_FINNUM',1,'J'
    ,'P_DATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_GET_FINNUM',1,'J'
    ,'RETURNVALUE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_FINNUM',1,'J'
    ,'P_NUMRELATIE','INP');
