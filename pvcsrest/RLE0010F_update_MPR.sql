/* RLE0010F_update_MPR.sql:
   Wijzigen verkeerd opgevoerde waarde van TYP (was 'MOD') in verband
   met het moeten kunnen aanroepen van RLE0010F zonder opgegeven NR_RELATIE
*/
UPDATE stm_moduleparameters
   SET typ           = 'OUT'
 WHERE mde_numversie = 1
   AND prm_cod       = 'NR_RELATIE'
   AND mde_module    = 'RLE0010F'
;

