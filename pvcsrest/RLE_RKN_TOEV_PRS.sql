delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RKN_TOEV_PRS';
delete from stm_moduleparameters
    where mde_module = 'RLE_RKN_TOEV_PRS';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RKN_TOEV_PRS';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RKN_TOEV_PRS';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RKN_TOEV_PRS';
DELETE FROM stm_modules
    WHERE module = 'RLE_RKN_TOEV_PRS';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RKN_TOEV_PRS',1,'RLE',1
    ,'RLE0953: Onderhouden verwantschappen (batch)','P'
    ,'RLE_RKN_TOEV_PRS',''
    ,'A',''
    ,10);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_RKN_TOEV_PRS',1,'J'
    ,'PIN_PERSOONSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_RKN_TOEV_PRS',1,'J'
    ,'PIN_PERSOONSNUMMER_VERWANT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_RKN_TOEV_PRS',1,'J'
    ,'PIV_SOORT_VERWANTSCHAP','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_RKN_TOEV_PRS',1,'J'
    ,'PID_BEGINDATUM_SELECTIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_RKN_TOEV_PRS',1,'J'
    ,'PID_BEGINDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D6','RLE_RKN_TOEV_PRS',1,'J'
    ,'PID_EINDDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_RKN_TOEV_PRS',1,'J'
    ,'PIV_INDICATIE_CORRECTIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_RKN_TOEV_PRS',1,'J'
    ,'PIV_INDICATIE_VERWIJDEREN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_RKN_TOEV_PRS',1,'J'
    ,'POV_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_RKN_TOEV_PRS',1,'J'
    ,'POV_FOUTMELDING','OUT');
