merge into rle_standaard_voorwaarden sve1 using ( select 'VERZET (WGR)' code ,q'#15.1) Werkgevers met verzet uitsluiten (1.39)#' naam ,q'#Definitie:
Uitsluiten van Werkgevers met geldig Verzet voor gegeven Verzender , Dekkingsoort , Communicatie kanaal.
Parameters:
1	Verzender (Tussenpersoon of RLE ADMINISTRATIE.Code)
2	Dekkingsoort (REG DEKKINGSOORT.Code) Hieruit wordt de product groep afgeleid middels REG DEKKING PARAMETERS
3	Communicatie kanaal#' beschrijving ,q'#select 1
from   rle_relatie_verzetten rvt
,      rle_relatie_externe_codes  rec
where  rvt.rle_numrelatie = rec.rle_numrelatie
and    rvt.rle_numrelatie = :NUMRELATIE
and    rec.rol_codrol = 'WG'
and    rec.dwe_wrddom_extsys = 'WGR'
and    rvt.verval_datum is null
and  ( rvt.adm_code       is null or upper(rvt.adm_code)      = upper(:VAR1) or (rvt.adm_code is not null and :VAR1 is null) )
and  ( rvt.tussenpersoon  is null or upper(rvt.tussenpersoon) = upper(:VAR1) or (rvt.tussenpersoon is not null and :VAR1 is null) )
and  ( rvt.verzet_kanaal  is null or upper(rvt.verzet_kanaal) = upper(:VAR2) or (rvt.verzet_kanaal is not null and :VAR2 is null) )
and  ( rvt.productgroep   is null 
    or rvt.productgroep = ( select min(dpr.waarde_karakter)
                            from   reg_dekking_parameters dpr 
                            where  dpr.naam       = 'VERZET_PRODUCTGRP'
                            and    trunc(sysdate) between dpr.ingangsdatum and nvl(dpr.einddatum,trunc(sysdate))
                            and    upper(dpr.dks_code)   = upper(:VAR3)
                          ) 
     or ( rvt.productgroep is not null 
          and ( select min(dpr.waarde_karakter)
                from   reg_dekking_parameters dpr 
                where  dpr.naam = 'VERZET_PRODUCTGRP'
                and    trunc(sysdate) between dpr.ingangsdatum and nvl(dpr.einddatum,trunc(sysdate))
                and    upper(dpr.dks_code) = upper(:VAR3)) is null
          )
      )
having count(*) = 0#' sqltext ,q'#Verzender (adm. of tussenpers.)#' var1_naam ,'C' var1_datatype ,q'#Communicatie kanaal#' var2_naam ,'C' var2_datatype ,q'#Dekkingsoort#' var3_naam ,'C' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$PAS' creatie_door ,to_date('20-11-2009 16:33:04','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('09-12-2009 15:05:00','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
