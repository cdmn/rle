/* Naam......: RLE_W4771.sql
** Release...: W4771
** Datum.....: 15-12-2009 11:14:39
** Notes.....: Gegenereerd door ITStools Versie 3.25
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4771', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema voor RLE.rle_campagne_definities.tab
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR CONTINUE

drop sequence rle_agp_seq;

drop sequence rle_gde_seq;

drop sequence rle_sve_seq;

drop sequence rle_rig_seq;

drop table rle_relaties_in_groepen;

drop table rle_aangemaakte_groepen;

drop table rle_gebruikte_voorwaarden;

drop table rle_standaard_voorwaarden;

drop table rle_groepsdefinities;

drop public synonym rle_agp_seq;

drop public synonym rle_gde_seq;

drop public synonym rle_sve_seq;

drop public synonym rle_rig_seq;

drop public synonym rle_relaties_in_groepen;

drop public synonym rle_aangemaakte_groepen;

drop public synonym rle_gebruikte_voorwaarden;

drop public synonym rle_standaard_voorwaarden;

drop public synonym rle_groepsdefinities;

WHENEVER SQLERROR EXIT

prompt RLE.rle_campagne_definities.tab
@@ RLE.rle_campagne_definities.tab
prompt RLE.rle_campagnes.tab
@@ RLE.rle_campagnes.tab
prompt RLE.rle_gebruikte_voorwaarden.tab
@@ RLE.rle_gebruikte_voorwaarden.tab
prompt RLE.rle_relaties_in_campagnes.tab
@@ RLE.rle_relaties_in_campagnes.tab
prompt RLE.rle_standaard_voorwaarden.tab
@@ RLE.rle_standaard_voorwaarden.tab
prompt RLE.rle_cpe_cde_fk.ind
@@ RLE.rle_cpe_cde_fk.ind
prompt RLE.rle_gve_cde_fk.ind
@@ RLE.rle_gve_cde_fk.ind
prompt RLE.rle_gve_sve_fk.ind
@@ RLE.rle_gve_sve_fk.ind
prompt RLE.rle_ric_cpe_fk.ind
@@ RLE.rle_ric_cpe_fk.ind
prompt RLE.rle_ric_rle_fk.ind
@@ RLE.rle_ric_rle_fk.ind
prompt RLE.rle_cde_pk.con
@@ RLE.rle_cde_pk.con
prompt RLE.rle_cpe_pk.con
@@ RLE.rle_cpe_pk.con
prompt RLE.rle_gve_pk.con
@@ RLE.rle_gve_pk.con
prompt RLE.rle_ric_pk.con
@@ RLE.rle_ric_pk.con
prompt RLE.rle_sve_pk.con
@@ RLE.rle_sve_pk.con
prompt RLE.rle_cde_uk.con
@@ RLE.rle_cde_uk.con
prompt RLE.rle_sve_uk.con
@@ RLE.rle_sve_uk.con
prompt RLE.rle_cpe_cde_fk.con
@@ RLE.rle_cpe_cde_fk.con
prompt RLE.rle_gve_cde_fk.con
@@ RLE.rle_gve_cde_fk.con
prompt RLE.rle_gve_sve_fk.con
@@ RLE.rle_gve_sve_fk.con
prompt RLE.rle_ric_cpe_fk.con
@@ RLE.rle_ric_cpe_fk.con
prompt RLE.rle_ric_rle_fk.con
@@ RLE.rle_ric_rle_fk.con
prompt RLE.rle_v_campagne_statistiek.vw
@@ RLE.rle_v_campagne_statistiek.vw
prompt RLE.rle_cde_seq.sqs
@@ RLE.rle_cde_seq.sqs
prompt RLE.rle_cpe_seq.sqs
@@ RLE.rle_cpe_seq.sqs
prompt RLE.rle_ric_seq.sqs
@@ RLE.rle_ric_seq.sqs
prompt RLE.rle_sve_seq.sqs
@@ RLE.rle_sve_seq.sqs
set define off
prompt RLE.rle_cmp_brf.pks
@@ RLE.rle_cmp_brf.pks
set define on
set define off
prompt RLE.rle_cmp_sel.pks
@@ RLE.rle_cmp_sel.pks
set define on
set define off
prompt RLE.rle_cmp_brf.pkb
@@ RLE.rle_cmp_brf.pkb
set define on
set define off
prompt RLE.rle_cmp_sel.pkb
@@ RLE.rle_cmp_sel.pkb
set define on
prompt RLE.rle_cde_briu.trg
@@ RLE.rle_cde_briu.trg
prompt RLE.rle_cpe_briu.trg
@@ RLE.rle_cpe_briu.trg
prompt RLE.rle_gve_briu.trg
@@ RLE.rle_gve_briu.trg
prompt RLE.rle_ric_briu.trg
@@ RLE.rle_ric_briu.trg
prompt RLE.rle_sve_briu.trg
@@ RLE.rle_sve_briu.trg
prompt RLE.vullen_rle_sve_aantal_wnr_max_(wgr).sql
@@ RLE.vullen_rle_sve_aantal_wnr_max_(wgr).sql
prompt RLE.vullen_rle_sve_aantal_wnr_min_(wgr).sql
@@ RLE.vullen_rle_sve_aantal_wnr_min_(wgr).sql
prompt RLE.vullen_rle_sve_aantal_wnr_range_(wgr).sql
@@ RLE.vullen_rle_sve_aantal_wnr_range_(wgr).sql
prompt RLE.vullen_rle_sve_beperking_selectie_op_persoonsnummer.sql
@@ RLE.vullen_rle_sve_beperking_selectie_op_persoonsnummer.sql
prompt RLE.vullen_rle_sve_beperking_selectie_op_relatienummer.sql
@@ RLE.vullen_rle_sve_beperking_selectie_op_relatienummer.sql
prompt RLE.vullen_rle_sve_beperking_selectie_op_werkgevernummer.sql
@@ RLE.vullen_rle_sve_beperking_selectie_op_werkgevernummer.sql
prompt RLE.vullen_rle_sve_beroep_(prs).sql
@@ RLE.vullen_rle_sve_beroep_(prs).sql
prompt RLE.vullen_rle_sve_collectief_opgezegd_(wgr).sql
@@ RLE.vullen_rle_sve_collectief_opgezegd_(wgr).sql
prompt RLE.vullen_rle_sve_dekking_actief_(prs).sql
@@ RLE.vullen_rle_sve_dekking_actief_(prs).sql
prompt RLE.vullen_rle_sve_dekking_actief_(wgr).sql
@@ RLE.vullen_rle_sve_dekking_actief_(wgr).sql
prompt RLE.vullen_rle_sve_dekking_niet_actief_(prs).sql
@@ RLE.vullen_rle_sve_dekking_niet_actief_(prs).sql
prompt RLE.vullen_rle_sve_dekking_niet_actief_(wgr).sql
@@ RLE.vullen_rle_sve_dekking_niet_actief_(wgr).sql
prompt RLE.vullen_rle_sve_geb.datum_max_(prs).sql
@@ RLE.vullen_rle_sve_geb.datum_max_(prs).sql
prompt RLE.vullen_rle_sve_geb.datum_min_(prs).sql
@@ RLE.vullen_rle_sve_geb.datum_min_(prs).sql
prompt RLE.vullen_rle_sve_geb.datum_range_(prs).sql
@@ RLE.vullen_rle_sve_geb.datum_range_(prs).sql
prompt RLE.vullen_rle_sve_groep_van_werkzaamheden_(wgr).sql
@@ RLE.vullen_rle_sve_groep_van_werkzaamheden_(wgr).sql
prompt RLE.vullen_rle_sve_loonsom_max_(wgr).sql
@@ RLE.vullen_rle_sve_loonsom_max_(wgr).sql
prompt RLE.vullen_rle_sve_loonsom_min_(wgr).sql
@@ RLE.vullen_rle_sve_loonsom_min_(wgr).sql
prompt RLE.vullen_rle_sve_loonsom_range_(wgr).sql
@@ RLE.vullen_rle_sve_loonsom_range_(wgr).sql
prompt RLE.vullen_rle_sve_niet_verzekeringspakket_(wgr).sql
@@ RLE.vullen_rle_sve_niet_verzekeringspakket_(wgr).sql
prompt RLE.vullen_rle_sve_personen_met_arbeidsverhouding_(prs).sql
@@ RLE.vullen_rle_sve_personen_met_arbeidsverhouding_(prs).sql
prompt RLE.vullen_rle_sve_personen_zonder_arbeidsverhouding_(prs).sql
@@ RLE.vullen_rle_sve_personen_zonder_arbeidsverhouding_(prs).sql
prompt RLE.vullen_rle_sve_salaris_max_(prs).sql
@@ RLE.vullen_rle_sve_salaris_max_(prs).sql
prompt RLE.vullen_rle_sve_salaris_min_(prs).sql
@@ RLE.vullen_rle_sve_salaris_min_(prs).sql
prompt RLE.vullen_rle_sve_salaris_range_(prs).sql
@@ RLE.vullen_rle_sve_salaris_range_(prs).sql
prompt RLE.vullen_rle_sve_uitsluiting_aanwezig_(prs).sql
@@ RLE.vullen_rle_sve_uitsluiting_aanwezig_(prs).sql
prompt RLE.vullen_rle_sve_verzekeringspakket_(prs).sql
@@ RLE.vullen_rle_sve_verzekeringspakket_(prs).sql
prompt RLE.vullen_rle_sve_verzekeringspakket_(wgr).sql
@@ RLE.vullen_rle_sve_verzekeringspakket_(wgr).sql
prompt RLE.vullen_rle_sve_verzet_(prs).sql
@@ RLE.vullen_rle_sve_verzet_(prs).sql
prompt RLE.vullen_rle_sve_verzet_(wgr).sql
@@ RLE.vullen_rle_sve_verzet_(wgr).sql
prompt RLE.vullen_rle_sve_werkgeverstatus_(wgr).sql
@@ RLE.vullen_rle_sve_werkgeverstatus_(wgr).sql
prompt RLE.vullen_rle_sve_verzekeringspakket_niet_actief_(prs).sql
@@ RLE.vullen_rle_sve_verzekeringspakket_niet_actief_(prs).sql

/* Switchen naar het juiste schema voor RLE.datatype.dom
*/
set define on
connect /@&&DBN

prompt RLE0043F.rol
@@ RLE0043F.rol
prompt RLE.datatype.dom
@@ RLE.datatype.dom

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4771 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W4771 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
