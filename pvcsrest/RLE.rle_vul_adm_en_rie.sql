insert into rle_administraties(code,omschrijving, ind_afstemmen_gba_tgstn )
values('ALG','Administratie voor algemeen beschikbare gegevens', 'N')
/
insert into rle_administraties(code,omschrijving, ind_afstemmen_gba_tgstn )
values('PMT','Administratie voor algemeen beschikbare gegevens', 'J')
/
insert into rle_administraties(code,omschrijving, ind_afstemmen_gba_tgstn )
values('NVSCHADE','Administratie voor algemeen beschikbare gegevens', 'J')
/
insert into rle_administraties(code,omschrijving, ind_afstemmen_gba_tgstn )
values('BOVEMIJ','Administratie voor algemeen beschikbare gegevens', 'N')
/
insert into rle_administraties(code,omschrijving, ind_afstemmen_gba_tgstn )
values('ZILVKR','Administratie voor algemeen beschikbare gegevens', 'N')
/
insert into rle_administraties(code,omschrijving, ind_afstemmen_gba_tgstn )
values('INTPOL','Administratie voor algemeen beschikbare gegevens', 'N')
/
insert into rle_relatierollen_in_adm
(            adm_code, rol_codrol, rle_numrelatie)
select distinct 'ALG', rol_codrol, rle_numrelatie
from rle_relatie_rollen
/
commit;
