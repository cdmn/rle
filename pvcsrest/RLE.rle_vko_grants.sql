grant select on rle_relatie_externe_codes to comp_vko with grant option;

grant select on rle_relaties to comp_vko with grant option;

grant select on rle_v_personen to comp_vko with grant option;

grant execute on FNC_BEPAAL_VOORLETTERS to comp_vko with grant option;

grant execute on rle_get_voorkeursnaam to comp_vko with grant option;
