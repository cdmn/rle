DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10418';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10418';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10418', 'Ongeldige soort', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Alleen I (individu) of O (overige) toegestaan bij soort.', NULL, 'RLE-10418');


