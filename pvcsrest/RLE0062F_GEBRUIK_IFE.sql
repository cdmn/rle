delete from stm_gebruik_interfaces where mde_module = 'RLE0062F';
insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='GBA_AANMAKEN_ADHOC')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='GBA_AANMAKEN_PLAATSING')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='GBA_AANMAKEN_VERWIJDER')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_REKENING')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_ADRES')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_COMMUNICATIENUMMER')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_KANAALVOORKEUR')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SELECTEREN_PERSOON')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_VERWANTSCHAPPEN')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SELECTEREN_BEWINDVOERDER')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_GEMOEDSBEZWAARDHEDEN')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_VERZENDBESTEMMING')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SEL_REF_FGD')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SEL_REF_GES')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SEL_REF_GWT')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SEL_REF_VVL')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SEL_REF_BST')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SEL_REF_ANG')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='SEL_OPU_RLE_DTY')
,1
,'RLE0062F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_RELATIE_SPECIAAL')
    ,1
    ,'Q34WERKB'
    ,1);

commit;
