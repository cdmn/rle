/*
PKG, 06-11-2013:
Wegens discrepanties tussen RLE en CODA in het aantal TAGS voor NVS-U/UIWT
loopt CODAPRC01 steeds verkeerd.
Daarom in RLE alle records voor NVS-U verwijderen, en vanuit CODA opnieuw laten opvoeren
*/
set serveroutput on size 100000
set linesize 180
set trimspool on

Prompt Start script RLE_del_fnr_coda_nvsu.sql

Prompt
Prompt Schonen van RLE FINNR_TAGS voor Administratie NVS-U
prompt

prompt Vooraf Tellen van aantal records per administratie in RLE_FINNR_CODA
select substr(fnc.adm_code,1,30) administratie
,count(fnc.fnr_id) aantal_recs 
from   rle_finnr_coda fnc
group by adm_code ;

prompt Verwijderen van alle records van NVS-U uit RLE_FINNR_CODA
delete from rle_finnr_coda fnc
where fnc.adm_code = 'NVS-U';

prompt Vooraf Tellen van aantal records per administratie in RLE_FINNR_CODA
select substr(fnc.adm_code,1,30) administratie
,count(fnc.fnr_id) aantal_recs 
from   rle_finnr_coda fnc
group by adm_code ;

prompt Einde script RLE_del_fnr_coda_nvsu.sql

