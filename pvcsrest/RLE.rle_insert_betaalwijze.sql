-- 021-072013
-- xsw
-- terugzetten betaalwijze AI en AG (oplossing in scherm rle0080F; ind_vervallen is null toegevoegd aan lov)
insert into cg_ref_codes ( rv_low_value
                         , rv_domain
                         , rv_abbreviation
                         , rv_meaning
                         , ind_vervallen
                         )
             values      ( 'AI'
                         , 'WIJZE VAN BETALING'
                         , 'WBG'
                         , 'Automatische incasso'
                         , 'J'
                         );
                         
insert into cg_ref_codes ( rv_low_value
                         , rv_domain
                         , rv_abbreviation
                         , rv_meaning
                         , ind_vervallen
                         )
             values      ( 'AG'
                         , 'WIJZE VAN BETALING'
                         , 'WBG'
                         , 'Accept giro.'
                         , 'J'
                         );                         

