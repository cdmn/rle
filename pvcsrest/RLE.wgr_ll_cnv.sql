------------------------------------------------------------------------------
-- Programma : RLE.wgr_ll_cnv.sql
-- Functie   : 
-- Auteur    : XMB
-- Datum     : 22-01-2009
-- Doel      : Opvoeren van Metalektro werkgevers in component WGR
--             tbv Levensloop.
-- Aangepast : 04-06-2009 IBM.PUBLISH_EVENT ( 'MUT RLE ADRESSEN'....)
--             toegevoegd tbv synchronisatie.
-- Aangepast : 07-07-2009 MNLL en MTLL worden ook geconverteerd.
----------------------------------------------------------------------------
set serveroutput on size 1000000
set pagesize 2000
SET linesize 10000
SET heading off
SET feedback off
SET pause off
SET verify off
SET trimspool on
--SET termout off
--SET colsep ';'
prompt ***********************************************************************************
prompt ***                        START                                      ***
prompt ***********************************************************************************


col RUN_DAT new_value RUN_DAT noprint
select to_char (sysdate, 'DDMMYYYYHH24MISS') run_dat
from   dual;

spool wgr_ll_cnv_&&RUN_DAT

prompt
prompt
prompt Levensloop werkgevers die ontbreken in de component WGR....
prompt ***********************************************************************************
prompt
prompt

select 'Relatienummer;Werkgeversnummer;Naam'
from dual
/

select   to_char(rle.numrelatie) || ';' ||
         rle.namrelatie || ';' ||
         (select ree.extern_relatie
           from   rle_relatie_externe_codes ree
           where  rle.numrelatie = ree.rle_numrelatie
           and    ree.rol_codrol = 'WG'
           and    ree.dwe_wrddom_extsys = 'WGR' 
         ) nr_werkgever
from   rle_relaties             rle
where  rle.numrelatie in
      (select rie.rle_numrelatie
       from   rle_relatierollen_in_adm rie
       where  rie.adm_code in ( 'MELL', 'MNLL','MTLL')
       and    rie.rol_codrol = 'WG'
      )
and   rle.numrelatie not in
     (select wgr.nr_relatie
      from   wgr_werkgevers wgr
     )
/

declare
  procedure p_upd_wgr
  is
    --
    cursor c_iln
    is
    select   rle.numrelatie
           , rle.namrelatie   bedrijfsnaam
           , rle.handelsnaam
           , length (rle.handelsnaam) breedte_hnaam
           , (select ree.extern_relatie
                    from   rle_relatie_externe_codes ree
                    where  rle.numrelatie = ree.rle_numrelatie
                    and    ree.rol_codrol = 'WG'
                    and    ree.dwe_wrddom_extsys = 'WGR' 
                    and    rownum < 2) nr_werkgever
           , rle.DWE_WRDDOM_RVORM rechtsvorm
           , trunc(nvl (rle.datbegin, rle.dat_creatie)) begindatum
           , null straatnaam
           , null huisnummer
           , null postcode
           , null melding
    from   rle_relaties             rle
    where  rle.numrelatie in
      (select rie.rle_numrelatie
       from   rle_relatierollen_in_adm rie
       where  rie.adm_code in ( 'MELL', 'MNLL','MTLL')
       and    rie.rol_codrol = 'WG'
      )
    and    rle.numrelatie not in
     (select wgr.nr_relatie
      from   wgr_werkgevers wgr
     );
    --
    cursor c_rie ( b_numrelatie in number)
    is
    select rie.adm_code
    from   rle_relatierollen_in_adm rie
    where  rie.adm_code in ('MTLL','MELL','MNLL')
    and    rie.rle_numrelatie = b_numrelatie;
    --
    cursor c_rie_totaal
    is
    select   count(distinct(rle.numrelatie))
    from   rle_relaties             rle
    where  rle.numrelatie in
      (select rie.rle_numrelatie
       from   rle_relatierollen_in_adm rie
       where  rie.adm_code in ( 'MELL', 'MNLL','MTLL')
       and    rie.rol_codrol = 'WG'
      )
    and    rle.numrelatie not in
     (select wgr.nr_relatie
      from   wgr_werkgevers wgr
     );
    --
    cursor c_ads (b_numrelatie rle_relaties.numrelatie%type)
    is  
    select     ads.ads_numadres
    from       rle_relatie_adressen  ads
    where      ads.DWE_WRDDOM_SRTADR = 'SZ'
    and        ads.rol_codrol        = 'WG'
    and        ads.dateinde          is null
    and        ads.rle_numrelatie = b_numrelatie;
      
    cursor c_wss (
      b_relatienummer             in       number
    )
    is
      select wss.dat_begin
           , wss.wgr_id
           , wss.code_status
      from   wgr_werkgever_statussen wss
           , wgr_werkgevers wgr
      where  wss.wgr_id = wgr.id
      and    wss.dat_einde is null
      and    wgr.nr_relatie = b_relatienummer;

    r_wss                         c_wss%rowtype;
    --
    cursor c_gvw (b_naam gvw.naam%type)
    is
    select kodegvw
    from   gvw gvw
    where  gvw.naam = b_naam;
    --
    type ilntabtype is table of c_iln%rowtype
      index by binary_integer;

    t_iln                         ilntabtype;
    r_iln                         c_iln%rowtype;
    
    l_begindatum                  date;
    l_ras_begindatum              date;
    l_ras_provincie               rle_relatie_adressen.provincie%type;
    l_ras_id                      rle_relatie_adressen.id%type;
    l_ras_numkamer                rle_relatie_adressen.numkamer%type;
    l_ras_locatie                 rle_relatie_adressen.locatie%type;
    l_ras_ind_woonboot            rle_relatie_adressen.ind_woonboot%type;
    l_ras_ind_woonwagen           rle_relatie_adressen.ind_woonwagen%type;
    l_ras_dat_creatie             rle_relatie_adressen.dat_creatie%type;
    l_ras_creatie_door            rle_relatie_adressen.creatie_door%type;
    l_ras_dat_mutatie             rle_relatie_adressen.dat_mutatie%type;
    l_ras_mutatie_door            rle_relatie_adressen.mutatie_door%type;
    l_ads_lnd_codland             rle_adressen.lnd_codland%type;
    l_relatienummer               number (10);
    l_verwerkt                    varchar2 (1);
    l_error                       varchar2 (32767);
    l_teller                      number default 0;
    l_teller_correct              number default 0;
    l_teller_fout                 number default 0;
    l_numadres                    rle_adressen.numadres%type;
    l_woonplaats                  rle_woonplaatsen.woonplaats%type;
    l_straatnaam                  rle_straten.straatnaam%type;
    l_huisnummer                  rle_adressen.huisnummer%type;
    l_postcode                    rle_adressen.postcode%type;
    l_toevhuisnum                 rle_adressen.toevhuisnum%type;
    l_land                        rle_adressen.lnd_codland%type;
    l_bedrijfsnaam                rle_relaties.namrelatie%type;
    l_handelsnaam                 rle_relaties.handelsnaam%type;
    l_nr_werkgever                wgr_werkgevers.nr_relatie%type;
    l_adm_code                    rle_relatierollen_in_adm.adm_code%type;
    l_code_hoofdgr_bedrtak        wgr_bedrijfstakken.code_hoofdgr_bedrtak%type;
    l_rechtsvorm                  rle_relaties.DWE_WRDDOM_RVORM%type;
    l_aantal_totaal               number;
    l_doorgaan                    varchar2 (1) default 'J';
    l_lijstnummer                 number := 1;
    l_regelnummer                 number := 0;
    l_breedte_hnaam               number default 0;
    l_code_groep_werkzhd          varchar2 (20);
    l_error_ex                    varchar2 (32767);
    --
    cn_kodebdt_mell             constant    varchar2(5)   := 'MI';      -- Code hoofdgroep bedrijfstak voor MELL
    cn_kodebdt_mnll             constant    varchar2(5)   := 'IS';      -- Code hoofdgroep bedrijfstak voor MNLL
    cn_kodebdt_mtll             constant    varchar2(5)   := 'IS';      -- Code hoofdgroep bedrijfstak voor MTLL
    cn_kodegvw_mell             constant    varchar2(100) := 'Werkzaamheden Metalektro';        -- Code groep van werkzaamheden voor MELL
    cn_kodegvw_mnll             constant    varchar2(100) := 'Onbepaald';                       -- Code groep van werkzaamheden voor MNLL
    cn_kodegvw_mtll             constant    varchar2(100) := 'Werkzaamheden gelieerd aan de Metaal en Techniek'; -- Code groep van werkzaamheden voor MTLL
    cn_code_inschrijving        constant    varchar2(5)   := 'WGR';     -- Code aanleiding inschrijving
    --
  begin
    stm_util.t_procedure       := 'WGR_LL_CNV';
    stm_util.t_script_naam     := 'WGR_LL_CNV';
    stm_util.t_script_id       := 1;
    stm_util.t_programma_naam  := 'WGR_LL_CNV';
    
    -- initialiseren tabtype
    t_iln.delete;
    --
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Verwerken bestaande Werkgevers Metalektro'
                                                   || chr (10)
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           => ''
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           => ''
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Datum : '
                                                   || to_char (sysdate, 'DD-MM-YYYY')
                            );
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    'Tijd  : '
                                                   || to_char (sysdate, 'HH24:MI')
                            );
    --
    open  c_rie_totaal;
    fetch c_rie_totaal into l_aantal_totaal;
    close c_rie_totaal;
    --                        
    for r_iln in c_iln
    loop
        savepoint opvoeren_relatie;
        
        l_teller                   :=   l_teller + 1;
        -- in de tabtype gaan we meldingen bijhouden
        t_iln (l_teller).numrelatie   := r_iln.numrelatie;
        t_iln (l_teller).bedrijfsnaam := r_iln.bedrijfsnaam;
      
        -- De run voert de werkgevers en adressen op en produceert een kort verwerkingsverslag.
        -- eerst resetten van de variabelen
        l_relatienummer            := r_iln.numrelatie;
        l_nr_werkgever             := null;
        l_verwerkt                 := null;
        l_error                    := null;
        l_error_ex                 := null;
        l_woonplaats               := null;
        l_straatnaam               := null;
        l_doorgaan                 := 'J';
         
        l_bedrijfsnaam             := r_iln.bedrijfsnaam;
        l_handelsnaam              := r_iln.handelsnaam;
        --l_nr_werkgever             := r_iln.nr_werkgever;
        l_breedte_hnaam            := r_iln.breedte_hnaam;
        l_begindatum               := r_iln.begindatum;
        l_rechtsvorm               := r_iln.rechtsvorm;
        l_ras_begindatum           := null;
        --
        open  c_rie (l_relatienummer);
        fetch c_rie into l_adm_code;
        close c_rie;
        --
        l_code_hoofdgr_bedrtak     := case l_adm_code
                                        when 'MELL' then cn_kodebdt_mell 
                                        when 'MNLL' then cn_kodebdt_mnll
                                        when 'MTLL' then cn_kodebdt_mtll 
                                        else null
                                      end;
        --
        -- bepalen code groep van werkzaamheden
        case when l_adm_code = 'MELL'
             then
               open  c_gvw( cn_kodegvw_mell );
               fetch c_gvw into l_code_groep_werkzhd;
               if    c_gvw%notfound then
                 close c_gvw;
                 stm_dbproc.raise_error('ERROR - tijdens bepalen code groep van werkzaamheden');
               end if;
               close c_gvw;
             when l_adm_code = 'MNLL'
             then
               open  c_gvw( cn_kodegvw_mnll );
               fetch c_gvw into l_code_groep_werkzhd;
               if    c_gvw%notfound then
                 close c_gvw;
                 stm_dbproc.raise_error('ERROR - tijdens bepalen code groep van werkzaamheden');
               end if;
               close c_gvw;
             when l_adm_code = 'MTLL'
             then
               open  c_gvw( cn_kodegvw_mtll );
               fetch c_gvw into l_code_groep_werkzhd;
               if    c_gvw%notfound then
                 close c_gvw;
                 stm_dbproc.raise_error('ERROR - tijdens bepalen code groep van werkzaamheden');
               end if;
               close c_gvw;
             else
               l_code_groep_werkzhd := null;
        end case;
        --
        begin
            --
            open  c_ads( l_relatienummer);
            fetch c_ads into l_numadres;
            close c_ads;
            --           
            RLE_GET_ADRES
             (P_NUMADRES    => l_numadres
             ,P_POSTCODE    => l_postcode
             ,P_HUISNR      => l_huisnummer
             ,P_TOEVNUM     => l_toevhuisnum
             ,P_WOONPLAATS  => l_woonplaats
             ,P_STRAAT      => l_straatnaam
             ,P_LAND        => l_land
             );     
           --
           select ras.datingang
           ,      ras.provincie 
           ,      ras.id
           ,      ras.numkamer
           ,      ras.locatie
           ,      ras.ind_woonboot
           ,      ras.ind_woonwagen
           ,      ras.dat_creatie
           ,      ras.creatie_door
           ,      ras.dat_mutatie
           ,      ras.mutatie_door
           ,      ads.lnd_codland
           into   l_ras_begindatum
           ,      l_ras_provincie 
           ,      l_ras_id
           ,      l_ras_numkamer
           ,      l_ras_locatie
           ,      l_ras_ind_woonboot
           ,      l_ras_ind_woonwagen
           ,      l_ras_dat_creatie
           ,      l_ras_creatie_door
           ,      l_ras_dat_mutatie
           ,      l_ras_mutatie_door
           ,      l_ads_lnd_codland
           from   rle_relatie_adressen ras
           ,      rle_adressen         ads
           where  ras.rle_numrelatie = l_relatienummer
           and    ras.ads_numadres   = l_numadres
           and    ras.ads_numadres   = ads.numadres
           and    ras.DWE_WRDDOM_SRTADR = 'SZ'
           and    ras.rol_codrol        = 'WG'
           and    ras.dateinde          is null;
           --
           if l_ads_lnd_codland != 'NL'
           then -- XPT 15-07-2009 buitenlandse adressen niet aanbieden met postcode
             l_postcode   := null;
           end if;
           --
           IBM.PUBLISH_EVENT
             ( 'MUT RLE ADRESSEN'
             , 'I'
             , to_char(l_ras_begindatum)
             , NULL, l_ras_provincie 
             , NULL, l_ras_id
             , NULL, to_char(l_ras_begindatum)
             , NULL, l_relatienummer
             , NULL, 'SZ'
             , NULL, l_numadres
             , NULL, 'A'
             , NULL, 'N'
             , NULL, 'WG'
             , NULL, l_ras_numkamer
             , NULL, NULL
             , NULL, l_ras_locatie
             , NULL, l_ras_ind_woonboot
             , NULL, l_ras_ind_woonwagen
             , NULL, to_char(l_ras_dat_creatie)
             , NULL, l_ras_creatie_door
             , NULL, to_char(l_ras_dat_mutatie)
             , NULL, l_ras_mutatie_door
             );            
        exception
            when others
            then
              l_doorgaan                 := 'N';
              l_error                    := 'Relatie met nummer '
                                            || r_iln.numrelatie
                                            || ' niet gevonden in RLE_ADRESSEN';
        end;
      
        if l_doorgaan = 'J'
        then
          if l_breedte_hnaam > 35
          then
            l_error                    :=    'Bedrijfsnaam > 35 Relatienummer: '
                                          || l_relatienummer  ||' '
                                          || sqlerrm;
            l_doorgaan                 := 'N';
          end if;
        end if;
        
        if l_begindatum > l_ras_begindatum
        then
          l_begindatum := l_ras_begindatum;
        end if;
    
        if l_doorgaan = 'J'
        then
          begin
            --
            if l_ads_lnd_codland != 'NL'
            then -- XPT 14-07-2009 buitenlandse adressen niet aanbieden aan wgr_reg_wgr
              l_straatnaam := null;
              l_huisnummer := null;
              l_postcode   := null;
            else
              l_postcode := upper (l_postcode);
            end if;
            --
            comp_wgr.wgr_reg_wgr (piv_statutaire_naam         => r_iln.bedrijfsnaam
                                , piv_handelsnaam             => l_handelsnaam
                                , pid_datum_begin             => l_begindatum
                                , piv_code_inschrijving       => cn_code_inschrijving
                                , pin_nr_relatie              => l_relatienummer
                                , pon_nr_werkgever            => l_nr_werkgever
                                , pov_verwerkt                => l_verwerkt
                                , pov_foutmelding             => l_error
                                , pid_datum_einde             => null
                                , piv_code_rechtsvorm         => l_rechtsvorm
                                , p_srt_adres                 => 'SZ'
                                , p_straat                    => l_straatnaam
                                , p_huisnummer                => l_huisnummer
                                , p_toevoeging_huisnummer     => null
                                , p_postcode                  => l_postcode
                                , p_woonplaats                => null
                                , p_provincie                 => null
                                , p_locatie                   => null
                                , p_ind_woonboot              => null
                                , p_ind_woonwagen             => null
                                 );
            if l_verwerkt = 'N'
            then
              l_doorgaan                 := 'N';
              l_error                    := ' wgr_reg_wgr 1 - '
                                            || chr (10)
                                            || l_error;
            end if;
          exception
            when others
            then
              l_error                    := ' wgr_reg_wgr 2 - '
                                            || chr (10)
                                            || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;

        if l_doorgaan = 'J'
        then
          declare
            l_wgr_id number;
          begin
            --
            select wgr.id into l_wgr_id
            from   wgr_werkgevers wgr
            where  wgr.nr_relatie = l_relatienummer;
            
            insert into wgr_bedrijfstakken btk
                        (btk.dat_begin
                       , btk.wgr_id
                       , btk.code_hoofdgr_bedrtak
                        )
            select l_begindatum
                 , wgr.id
                 , l_code_hoofdgr_bedrtak
            from   wgr_werkgevers wgr
            where  wgr.nr_relatie = l_relatienummer;
          exception
            when others
            then
              l_error                    :=    'Toevoegen bedrijfstak mislukt relatie '
                                            || l_relatienummer  ||' '
                                            || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;

        -- toevoegen groep van werkzaamheden l_code_groep_werkzhd
        if l_doorgaan = 'J'
        then
          begin
            -- bestaat de groep van werkzaamheden dan niets doen
            -- bestaat de groep nog niet dan aanmaken
            insert into wgr_werkzaamheden wzd
                        (wzd.dat_begin
                       , wzd.wgr_id
                       , wzd.code_groep_werkzhd
                        )
            select l_begindatum
                 , wgr.id
                 , l_code_groep_werkzhd
            from   wgr_werkgevers wgr
            where  wgr.nr_relatie = l_relatienummer
            and    not exists (select 1
                               from   wgr_werkzaamheden wzdd
                               where  wzdd.wgr_id = wgr.id
                               and    wzdd.dat_begin = l_begindatum
                              );
          exception
            when others
            then
              l_error                    :=    'Toevoegen groep van werkzaamheden mislukt relatie '
                                            || l_relatienummer  ||' '
                                            || sqlerrm;
              l_doorgaan                 := 'N';
          end;
        end if;

        if l_doorgaan = 'J'
        then
          begin
            -- ophalen status uit WERKGEVERSTATUS waar einddatum niet is gevuld (is maximaal 1)
            open  c_wss (b_relatienummer     => l_relatienummer);
            fetch c_wss
            into  r_wss;

            if c_wss%found
            then
              if l_adm_code = 'MTLL'
              then
                if r_wss.code_status != 'D'
                then
                  -- indien de status niet D definitief
                  update wgr_werkgever_statussen wss
                  set    wss.dat_begin = l_begindatum - 1
                  where  wss.wgr_id = r_wss.wgr_id
                  and    wss.dat_einde is null;
                  
                  wgr_wss_toev (p_wgr_id           => r_wss.wgr_id
                              , p_status           => 'D'
                              , p_reden_status     => null
                              , p_peildatum        => l_begindatum
                              , p_ind_commit       => 'N'
                              );
                end if;
              else
                update wgr_werkgever_statussen wss
                set    wss.dat_begin = l_begindatum
                where  wss.wgr_id = r_wss.wgr_id
                and    wss.dat_einde is null;
              end if;
            end if;

            if c_wss%isopen
            then
              close c_wss;
            end if;
          exception
            when others
            then
              l_error                    :=    'Definitief maken mislukt bij relatie '
                                            || l_relatienummer ||' '
                                            || sqlerrm;
              l_doorgaan                 := 'N';

              if c_wss%isopen
              then
                close c_wss;
              end if;
          end;
        end if;

        if l_doorgaan = 'N'
        then
          rollback to opvoeren_relatie;
          l_teller_fout              :=   l_teller_fout + 1;
          -- foutmelding registreren
          -- melding in tabtype stoppen zodat deze later in de tabel kan worden geschreven
          t_iln (l_teller).melding   := substr (  l_error_ex
                                                || ' - '
                                                ||l_error
                                              , 1
                                              , 4000
                                               );
        else
          l_teller_correct           :=   l_teller_correct + 1;
          t_iln (l_teller).melding   := substr (l_error_ex||l_error
                                              , 1
                                              , 4000
                                               );
        end if;                                                                             
    end loop;

    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                           , il_regelnummer     => l_regelnummer
                           , il_tekst           =>    ' Opgetreden fouten: '
                                                     || l_teller_fout
                                                     || chr (10)
                              );

    if t_iln.count > 0
    then
        for i in t_iln.first .. t_iln.last
        loop
          if t_iln (i).melding is not null
          then
            stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                                   , il_regelnummer     => l_regelnummer
                                   , il_tekst           => substr (   'Relatienr '
                                                                   || t_iln (i).numrelatie
                                                                   || ' '
                                                                   || t_iln (i).bedrijfsnaam
                                                                   || ' '
                                                                   || t_iln (i).melding
                                                                 , 1
                                                                 , 255
                                                                  )
                                    );
          end if;
        end loop;
    end if;
    --
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                             , il_regelnummer     => l_regelnummer
                             , il_tekst           =>    'Totaal aantal LL werkgevers: '
                                                     || l_aantal_totaal
                                                     || chr (10)
                              );
    --
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                             , il_regelnummer     => l_regelnummer
                             , il_tekst           =>    'Aantal werkgevers niet opgevoerd in WGR wegens fouten: '
                                                     || l_teller_fout
                                                     || chr (10)
                              );
    --
    stm_util.insert_lijsten (il_lijstnummer     => l_lijstnummer
                             , il_regelnummer     => l_regelnummer
                             , il_tekst           =>    'Aantal in WGR opgevoerde werkgevers : '
                                                     || l_teller_correct
                              );
    --
  exception
    when others
    then
      raise_application_error (-20000, 'ERROR - '
                                || sqlerrm);
  end;

begin
  p_upd_wgr;
end;
/


prompt
prompt
prompt Levensloop werkgevers die ontbreken in de component WGR....
prompt ***********************************************************************************
prompt
prompt

select 'Relatienummer;Werkgeversnummer;Naam'
from dual
/

select   to_char(rle.numrelatie) || ';' ||
         rle.namrelatie || ';' ||
         (select ree.extern_relatie
           from   rle_relatie_externe_codes ree
           where  rle.numrelatie = ree.rle_numrelatie
           and    ree.rol_codrol = 'WG'
           and    ree.dwe_wrddom_extsys = 'WGR' 
         ) nr_werkgever
from   rle_relaties             rle
where  rle.numrelatie in
      (select rie.rle_numrelatie
       from   rle_relatierollen_in_adm rie
       where  rie.adm_code in ( 'MELL', 'MNLL','MTLL')
       and    rie.rol_codrol = 'WG'
      )
and   rle.numrelatie not in
     (select wgr.nr_relatie
      from   wgr_werkgevers wgr
     )
/

prompt
prompt
prompt De inhoud van de lijstentabel tonen....
prompt ***********************************************************************************


select   tekst
from     lijsten
where    script_naam    = 'WGR_LL_CNV'
and      programma_naam = 'WGR_LL_CNV'
order by regelnummer
/
prompt
prompt ***********************************************************************************
prompt
prompt De inhoud van de lijstentabel verwijderen...
prompt
delete from lijsten
where       script_naam = 'WGR_LL_CNV'
and         programma_naam = 'WGR_LL_CNV'
/
prompt ***********************************************************************************
prompt ***                        EINDE WGR_LL_CNV                                     ***
prompt ***********************************************************************************
prompt

spool OFF
