delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_CHK_PERSOON';
delete from stm_moduleparameters
    where mde_module = 'RLE_CHK_PERSOON';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_CHK_PERSOON';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_CHK_PERSOON';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_CHK_PERSOON';
DELETE FROM stm_modules
    WHERE module = 'RLE_CHK_PERSOON';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_CHK_PERSOON',1,'RLE',1
    ,'Controleren bestaan persoon','P'
    ,'RLE_CHK_PERSOON',''
    ,'A',''
    ,20);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D12','RLE_CHK_PERSOON',1,'J'
    ,'POD_DATOVERLIJDEN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A13','RLE_CHK_PERSOON',1,'J'
    ,'POV_DA_STRAAT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N14','RLE_CHK_PERSOON',1,'J'
    ,'PON_DA_HUISNUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_CHK_PERSOON',1,'J'
    ,'POV_DA_TOEVHUISNUM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A16','RLE_CHK_PERSOON',1,'J'
    ,'POV_DA_POSTCODE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A17','RLE_CHK_PERSOON',1,'J'
    ,'POV_DA_PLAATS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_CHK_PERSOON',1,'J'
    ,'PIV_PERSOONSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A18','RLE_CHK_PERSOON',1,'J'
    ,'POV_DA_COD_LAND','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A19','RLE_CHK_PERSOON',1,'J'
    ,'POV_DA_OMS_LAND','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A20','RLE_CHK_PERSOON',1,'J'
    ,'POV_OPGEMAAKTE_NAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_CHK_PERSOON',1,'J'
    ,'PIV_SOFINUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_CHK_PERSOON',1,'J'
    ,'POV_IND_BESTAAT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_CHK_PERSOON',1,'J'
    ,'POV_PERSOONSNUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_CHK_PERSOON',1,'J'
    ,'POV_SOFINUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N6','RLE_CHK_PERSOON',1,'J'
    ,'PON_NUMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_CHK_PERSOON',1,'J'
    ,'POV_STATUS_GBA','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_CHK_PERSOON',1,'J'
    ,'POV_IND_AFNEMERS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D9','RLE_CHK_PERSOON',1,'J'
    ,'POD_DATGEBOORTE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_CHK_PERSOON',1,'J'
    ,'POV_COD_GESLACHT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_CHK_PERSOON',1,'J'
    ,'POV_OMS_GESLACHT','OUT');
