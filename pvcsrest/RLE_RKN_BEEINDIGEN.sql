delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RKN_BEEINDIGEN';
delete from stm_moduleparameters
    where mde_module = 'RLE_RKN_BEEINDIGEN';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RKN_BEEINDIGEN';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RKN_BEEINDIGEN';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RKN_BEEINDIGEN';
DELETE FROM stm_modules
    WHERE module = 'RLE_RKN_BEEINDIGEN';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RKN_BEEINDIGEN',1,'RLE',1
    ,'Beeindigen relatiekoppeling','P'
    ,'RLE_RKN_BEEINDIGEN',''
    ,'A',''
    ,4);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_RKN_BEEINDIGEN',1,'J'
    ,'PIN_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_RKN_BEEINDIGEN',1,'J'
    ,'PIV_ROL_RELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_RKN_BEEINDIGEN',1,'J'
    ,'PIV_ROL_GEKOPPELDE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_RKN_BEEINDIGEN',1,'J'
    ,'PID_EINDDATUM','INP');
