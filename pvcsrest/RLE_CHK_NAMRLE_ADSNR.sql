delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_CHK_NAMRLE_ADSNR';
delete from stm_moduleparameters
    where mde_module = 'RLE_CHK_NAMRLE_ADSNR';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_CHK_NAMRLE_ADSNR';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_CHK_NAMRLE_ADSNR';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_CHK_NAMRLE_ADSNR';
DELETE FROM stm_modules
    WHERE module = 'RLE_CHK_NAMRLE_ADSNR';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_CHK_NAMRLE_ADSNR',1,'RLE',1
    ,'Controle naam en adres uniek','F'
    ,'RLE_CHK_NAMRLE_ADSNR',''
    ,'A',''
    ,4);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_CHK_NAMRLE_ADSNR',1,'J'
    ,'P_NAMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_CHK_NAMRLE_ADSNR',1,'J'
    ,'P_NUMADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_CHK_NAMRLE_ADSNR',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_CHK_NAMRLE_ADSNR',1,'J'
    ,'RETURNVALUE','OUT');
