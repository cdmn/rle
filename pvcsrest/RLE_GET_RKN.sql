delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_RKN';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_RKN';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_RKN';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_RKN';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_RKN';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_RKN';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_RKN',1,'RLE',1
    ,'I_102: Ophalen bepaalde relatiekoppeling','P'
    ,'RLE_GET_RKN',''
    ,'A',''
    ,7);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_RKN',1,'J'
    ,'PIN_RKN_ID','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GET_RKN',1,'J'
    ,'PON_NUMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_GET_RKN',1,'J'
    ,'PON_NUMRELATIE_VOOR','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_GET_RKN',1,'J'
    ,'POD_DAT_BEGIN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_GET_RKN',1,'J'
    ,'POD_DAT_EINDE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GET_RKN',1,'J'
    ,'POV_CODE_ROL_IN_KOPPELING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GET_RKN',1,'J'
    ,'POV_OMSCHRIJVING','OUT');
