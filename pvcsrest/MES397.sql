delete from qms_message_text where msp_code = 'RLE-00397';
delete from qms_message_properties where code = 'RLE-00397';

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00397', 'Relatie komt mogelijk al voor.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00397', 'DUT'
            , 'Relatie komt mogelijk al voor.'
            , '');

