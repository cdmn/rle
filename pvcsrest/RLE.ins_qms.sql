DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10319';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10319';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10319', 'Het sofinummer is verplicht voor personen die 8 jaar of ouder zijn.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Het sofinummer is verplicht voor personen die 9 jaar of ouder zijn.', NULL, 'RLE-10319');