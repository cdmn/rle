delete from stm_wrdparameters
where mpr_mde_module = 'RLE0290F';

delete from stm_moduleinterfaces
where mde_module = 'RLE0290F';

prompt moduleinterface
INSERT into stm_moduleinterfaces
    (ifv_ife_id,ifv_numversie,volgnum,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_CONTACTPERSONEN')
    ,1,1
    ,'RLE0290F'
    ,1);
prompt wrdparameters
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_CONTACTPERSONEN'),1
    ,1,'RAS_ID'
    ,'RLE0290F',1,'RAS_ID'
    ,'','',NULL);