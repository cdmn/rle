/*************************************************************************
***              Uitdelen RLE grants aan COMP_AVG                      ***
*************************************************************************/

GRANT SELECT ON COMP_RLE.RLE_V_RELATIEROLLEN_IN_ADM_ZC TO COMP_AVG WITH GRANT OPTION;