delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0050F';
delete from stm_moduleparameters
    where mde_module = 'RLE0050F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0050F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0050F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0050F';
DELETE FROM stm_modules
    WHERE module = 'RLE0050F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0050F',1,'RLE',1
    ,'Onderhouden communicatienummer','S'
    ,'RLE0050F',''
    ,'A','Onderhouden communicatienummer'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0050F',1,'N'
    ,'Relatienr','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0050F',1,'N'
    ,'Rol','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('DAT_PEIL','RLE0050F',1,'N'
    ,'Peildatum','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0050F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0050F',1,'REF_SC',
        ':cnr.dwe_wrddom_srtcom');
