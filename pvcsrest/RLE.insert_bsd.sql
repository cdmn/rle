declare
   l_aantal number;
begin
   update rle_bestanden
   set formaat_naam = 'WGR_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.dat'
   where  naam = 'OTIB_WGR';

   if sql%rowcount = 0
   then
      insert into rle_bestanden
                  (naam
                  ,bsd_groep
                  ,bsd_type
                  ,db_directory
                  ,formaat_naam)
      values      ('OTIB_WGR', 'OTIB', 'WGR', 'RLE_OUTBOX', 'WGR_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.dat');
   end if;

   update rle_bestanden
   set formaat_naam = 'WNR_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.dat'
   where  naam = 'OTIB_WNR';

   if sql%rowcount = 0
   then
      insert into rle_bestanden
                  (naam
                  ,bsd_groep
                  ,bsd_type
                  ,db_directory
                  ,formaat_naam)
      values      ('OTIB_WNR', 'OTIB', 'WNR', 'RLE_OUTBOX', 'WNR_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.dat');
   end if;

   select count (1)
   into   l_aantal
   from   rle_bestandsabonnementen;

   if l_aantal = 0
   then
      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WGR'
                  ,'OTIB_L'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WGR'
                  ,'OTIB_K'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WGR'
                  ,'OTIB_E'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WGR'
                  ,'SKO'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WGR'
                  ,'OLC'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WGR'
                  ,'OFE'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WNR'
                  ,'OTIB_L'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WNR'
                  ,'OTIB_K'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WNR'
                  ,'OTIB_E'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WNR'
                  ,'SKO'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WNR'
                  ,'OLC'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);

      insert into rle_bestandsabonnementen
                  (bsd_naam
                  ,code_abonnee
                  ,dat_begin
                  ,dat_einde)
      values      ('OTIB_WNR'
                  ,'OFE'
                  ,to_date ('01/01/1992 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
                  ,null);
   end if;

   commit;
end;
/
