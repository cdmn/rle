delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RLE_EXI';
delete from stm_moduleparameters
    where mde_module = 'RLE_RLE_EXI';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RLE_EXI';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RLE_EXI';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RLE_EXI';
DELETE FROM stm_modules
    WHERE module = 'RLE_RLE_EXI';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RLE_EXI',1,'RLE',1
    ,'Controleren bestaan relatie','F'
    ,'RLE_RLE_EXI',''
    ,'A',''
    ,3);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D3','RLE_RLE_EXI',1,'J'
    ,'P_PEILDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_RLE_EXI',1,'J'
    ,'P_RELANUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_RLE_EXI',1,'J'
    ,'RETURNVALUE','OUT');
