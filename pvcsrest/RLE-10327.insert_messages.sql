INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10327', 'Een relatie mag geen koppeling met zichzelf hebben.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10327', 'DUT'
            , 'Een relatie mag geen koppeling met zichzelf hebben.'
            , '');

