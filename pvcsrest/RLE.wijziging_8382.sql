------------------------------------------------------------------------------
-- Auteur    : Karel Vlieg gebaseerd op code van Marc Bouwens D5132
-- Datum     : 14-02-2013
-- Doel      : Voor de ontvlechting van het CODA-ALG bedrijf dienen de 
--             debiteuren voor de FAC en CAV nota initieel worden aangeboden
--             onder de juiste administratie. Dit doen we dmv het event
--             'MUT RLE ROL IN ADMIN'
-- Incident  : 306671
------------------------------------------------------------------------------
set serveroutput on size 1000000
set pagesize  0
set trimspool on
set linesize 1000
spool RLE.wijziging_8382.lst

prompt ***********************************************************************************
prompt ***                        START RLE.wijziging_8382                             ***
prompt ***********************************************************************************
prompt *                                                                               ***
prompt *  Doel  : M.b.v. publicatie wordt er nieuwe administratie toegevoegd worden    ***
prompt *          voor de relaties met de rol 'WERKGEVER'                              ***
prompt ***********************************************************************************

declare

    -- per fonds alles relaties verzamelen met een lopende polis vanaf 01-01-2013
cursor c_wgr_rie
is
with aktieve_wgrs1
     as (select wgr.nr_werkgever, TO_NUMBER (wgr.nr_relatie) nr_relatie
           from wgr_werkgevers wgr
          where 1 = 1
                and exists
                       (select 1
                          from wgr_werkgever_statussen wss
                         where wgr.id = wss.wgr_id and wss.code_status != 'B'
                               and dat_begin =
                                      (select MAX (wss1.dat_begin)
                                         from wgr_werkgever_statussen wss1
                                        where wss1.wgr_id = wss.wgr_id))
                and exists
                       (select 1
                          from wgr_werkgever_statussen wss
                         where wgr.id = wss.wgr_id and wss.code_status = 'B')
                and wsf_sec.get_sec ('WGR' -- Alleen max uren ophalen wanneer de sector overeenkomt
                                   , SYSDATE
                                   , null
                                   , wgr.nr_werkgever
                                   , null) = 'MN'
                and dat_einde is null)
select distinct wgr.*, rie.adm_code, rie.rol_codrol
  from aktieve_wgrs1 wgr, rle_relatierollen_in_adm rie, fon
 where     rie.rle_numrelatie = TO_NUMBER (wgr.nr_relatie)
       and rie.rol_codrol = 'WG'
       and fon.adm_code = rie.adm_code
       and fon.kodebds = 'MN'
       and fon.adm_code <> 'ALG'
       and fon.dateind is null
       and fon.adm_code not in ('BOVEMIJ', 'AOV', 'KMU', 'ZILVKR', 'INTPOL')
union all
select distinct wgr.nr_werkgever
              , TO_NUMBER (wgr.nr_relatie) nr_relatie
              , rie.adm_code
              , rie.rol_codrol
  from wgr_werkgever_statussen wss
     , wgr_werkgevers wgr
     , rle_relatierollen_in_adm rie
 where     wgr.id = wss.wgr_id
       and rie.rle_numrelatie = TO_NUMBER (wgr.nr_relatie)
       and rie.rol_codrol = 'WG'
       and wgr.nr_relatie in
              (select rec.rle_numrelatie
                 from rle_relatie_externe_codes rec
                where rec.rol_codrol = 'WG' and rec.dwe_wrddom_extsys = 'WGR'
                      and rec.extern_relatie in
                            ( '002846', '004283', '010806', '018007', '020253', '023983', '026417', '028048', '028448', '039587'
                            , '039879', '040016', '049295', '051797', '054237', '056284', '057538', '062491', '064119', '065322'
                            , '068296', '073590', '075362', '075451', '076179', '078928', '079497', '083935', '100898', '104398'
                            , '106191', '106910', '107639', '107860', '108188', '108198', '108323', '108457', '108594', '109070'
                            , '109301', '109931', '110217', '112102', '113884', '114436', '114644', '114691', '114736', '115946'
                            , '116608', '118950', '120521', '120755', '120980', '122164', '122330', '122999', '123277', '123590'
                            , '124301', '124604', '124701', '125194', '125239', '125760', '126395', '127204', '127514', '128134'
                            , '129766', '129883', '130156', '130161', '130642', '130713', '131127', '131637', '131903', '132117'
                            , '132252', '132741', '133284', '134684', '134892', '194154', '194869', '195093', '195876', '196040'
                            , '196089', '196420', '197470', '197578', '198400', '198680', '198929', '199022', '199408', '200277'
                            , '200976', '201124', '201220', '201595'))
       and wss.nr_volg = (select MAX (wss1.nr_volg)
                            from wgr_werkgever_statussen wss1
                           where wss1.wgr_id = wss.wgr_id);

 
    l_errm            varchar2(2000);
    l_teller_wgr      number:=0;
    l_teller_fout_wgr number:=0;

  begin
    
    for r_wgr_rie in c_wgr_rie
    loop
 
        l_teller_wgr         := l_teller_wgr + 1;

        begin

		dbms_output.put_line('publicatie voor relatie: ' || r_wgr_rie.nr_relatie ||
                                                ' rol: ' || r_wgr_rie.rol_codrol ||
		                                        ' adm: ' ||  r_wgr_rie.adm_code);
		
    -- per 
    --
    /* publicatie van insert*/
    ibm.publish_event ('MUT RLE ROL IN ADMIN'
          ,'I'
          ,to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')
          ,null,r_wgr_rie.nr_relatie
          ,null,r_wgr_rie.rol_codrol
          ,null,r_wgr_rie.adm_code
          ,null,to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')
          ,null,user
          ,null,to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')
          ,null,user
          );

        exception
          when others
          then
            l_errm           :=
              substr ( sqlerrm
                     , 1
                     , 200 );

            l_teller_fout_wgr := l_teller_fout_wgr + 1;
      dbms_output.put_line(' WGR: fout gegaan bij ' || r_wgr_rie.nr_relatie || ' melding ' || l_errm); 
        end;

    end loop;
    
  dbms_output.put_line('aantal verwerkt voor wgr' || l_teller_wgr); 
  dbms_output.put_line(' Wgr : aantal fout  ' || l_teller_fout_wgr ); 
 
 end;
/


prompt *
prompt *
prompt *
prompt *
prompt *
prompt ***********************************************************************************
prompt ***                        EINDE RLE.wijziging_8382                             ***
prompt ***********************************************************************************
prompt
spool off
