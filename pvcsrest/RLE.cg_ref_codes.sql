insert into cg_ref_codes
( RV_LOW_VALUE     ,
  RV_HIGH_VALUE    ,
  RV_ABBREVIATION  ,
  RV_DOMAIN        ,
  RV_MEANING       ,
  RV_TYPE          ,
  IND_VERVALLEN    
  )
select 'EMAIL2'    ,
  null,
  'SC'  ,
  'Soort communicatie' ,
  '2e Emailadres'      ,
  'CG'          ,
  'N'
from  dual
where not exists (select 1  
                  from cg_ref_codes rfc2
                  where rfc2.RV_ABBREVIATION = 'SC' 
                  and   rfc2.rv_low_value =  'EMAIL2');

insert into cg_ref_codes
( RV_LOW_VALUE     ,
  RV_HIGH_VALUE    ,
  RV_ABBREVIATION  ,
  RV_DOMAIN        ,
  RV_MEANING       ,
  RV_TYPE          ,
  IND_VERVALLEN    
  )
select RV_LOW_VALUE,
  RV_HIGH_VALUE    ,
  'SC'  ,
  'Soort communicatie' ,
  RV_MEANING       ,
  RV_TYPE          ,
  IND_VERVALLEN   
from cg_ref_codes rfc
where RV_ABBREVIATION = 'KVR'
and   not exists (select 1  
                  from cg_ref_codes rfc2
                  where rfc2.RV_ABBREVIATION = 'SC' 
                  and   rfc2.rv_low_value =  rfc.rv_low_value);
				  
insert into cg_ref_codes
( RV_LOW_VALUE     ,
  RV_HIGH_VALUE    ,
  RV_ABBREVIATION  ,
  RV_DOMAIN        ,
  RV_MEANING       ,
  RV_TYPE          ,
  IND_VERVALLEN    
  )
select 'NIETGEWENST'    ,
  null,
  'SC'  ,
  'Soort communicatie' ,
  'Niet gewenst'      ,
  'CG'          ,
  'N'
from  dual
where not exists (select 1  
                  from cg_ref_codes rfc2
                  where rfc2.RV_ABBREVIATION = 'SC' 
                  and   rfc2.rv_low_value =  'NIETGEWENST');
				  
insert into cg_ref_codes
( RV_LOW_VALUE     ,
  RV_HIGH_VALUE    ,
  RV_ABBREVIATION  ,
  RV_DOMAIN        ,
  RV_MEANING       ,
  RV_TYPE          ,
  IND_VERVALLEN    
  )
select 'POST'    ,
  null,
  'SC'  ,
  'Soort communicatie' ,
  'Post'      ,
  'CG'          ,
  'N'
from  dual
where not exists (select 1  
                  from cg_ref_codes rfc2
                  where rfc2.RV_ABBREVIATION = 'SC' 
                  and   rfc2.rv_low_value =  'POST');
				  
				  
commit;
