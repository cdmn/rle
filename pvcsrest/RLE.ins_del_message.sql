delete from qms_message_text
    where msp_code = 'RLE-00617';
delete from qms_message_properties
    where code = 'RLE-00617';
INSERT INTO qms_message_properties
    (code, description, severity, logging_ind, suppr_wrng_ind, suppr_always_ind, constraint_name)
    VALUES
    ('RLE-00617','Er zijn <p1> na de nieuwe overlijdensdatum (<p2>).','E'
    ,'N','N'
    ,'N','');
INSERT into qms_message_text
    (language, text, help_text, msp_code)
    values
    ('DUT','Er zijn <p1> na de nieuwe overlijdensdatum (<p2>).',''
    ,'RLE-00617');
delete from qms_message_text
    where msp_code = 'RLE-00616';
delete from qms_message_properties
    where code = 'RLE-00616';
INSERT INTO qms_message_properties
    (code, description, severity, logging_ind, suppr_wrng_ind, suppr_always_ind, constraint_name)
    VALUES
    ('RLE-00616','Fout in <p1>: <p2>','E'
    ,'N','N'
    ,'N','');
INSERT into qms_message_text
    (language, text, help_text, msp_code)
    values
    ('DUT','Fout in <p1>: <p2>',''
    ,'RLE-00616');
delete from qms_message_text
    where msp_code = 'RLE-00615';
delete from qms_message_properties
    where code = 'RLE-00615';
INSERT INTO qms_message_properties
    (code, description, severity, logging_ind, suppr_wrng_ind, suppr_always_ind, constraint_name)
    VALUES
    ('RLE-00615','Geen externe code gevonden bij numrelatie <p1>, systeemcomponent <p2>, code rol <p2> op <p4>.','E'
    ,'N','N'
    ,'N','');
INSERT into qms_message_text
    (language, text, help_text, msp_code)
    values
    ('DUT','Geen externe code gevonden bij numrelatie <p1>, systeemcomponent <p2>, code rol <p2> op <p4>.',''
    ,'RLE-00615');
delete from qms_message_text
    where msp_code = 'RLE-00614';
delete from qms_message_properties
    where code = 'RLE-00614';
INSERT INTO qms_message_properties
    (code, description, severity, logging_ind, suppr_wrng_ind, suppr_always_ind, constraint_name)
    VALUES
    ('RLE-00614','<p1> niet verwerkt.','E'
    ,'N','N'
    ,'N','');
INSERT into qms_message_text
    (language, text, help_text, msp_code)
    values
    ('DUT','<p1> niet verwerkt.',''
    ,'RLE-00614');
delete from qms_message_text
    where msp_code = 'RLE-00613';
delete from qms_message_properties
    where code = 'RLE-00613';
INSERT INTO qms_message_properties
    (code, description, severity, logging_ind, suppr_wrng_ind, suppr_always_ind, constraint_name)
    VALUES
    ('RLE-00613','<p1> verwerkt.','I'
    ,'N','N'
    ,'N','');
INSERT into qms_message_text
    (language, text, help_text, msp_code)
    values
    ('DUT','<p1> verwerkt.',''
    ,'RLE-00613');
prompt Nog wel COMMIT uitvoeren !!!
