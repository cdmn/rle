INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10354', 'Voor deze persoon is geen a-nummer bekend, terwijl de afnemersindicatie ''Aan'' staat.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10354', 'DUT'
            , 'Voor deze persoon is geen a-nummer bekend, terwijl de afnemersindicatie ''Aan'' staat.'
            , '');

