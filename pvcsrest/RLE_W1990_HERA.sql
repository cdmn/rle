/* Naam......: RLE_W1990_HERA.sql
** Release...: W1990
** Datum.....: Fri Nov 15 11:22:15 2002
** Notes.....: Aangemaakt met Des_tool_mn Versie 3.1.4.1.5.9d


*/

set linesize   79
set pause     off


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma beeindigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er vanuit JCS wordt gestart.
   Hier wordt er vanuit gegaan dat vanuit JCS de user OPS$xJCS wordt gebruikt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(user,6),'JCS','exit','continue') P_EXIT
from dual;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C en RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    into dummy
    from dba_users
   where username = 'COMP_RLE';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE bestaat niet in deze database,
*          neem contact op met FIT-be-op !');
end;
/

/* Voor databases afgeleid van ZEUS, dus aangemaakt vanuit EROS
   moeten we het versie-nummer vullen in de table IMU_APPLICATIE_VERSIE
   Voor database afgeleid van HERA, dus aangemaakt vanuit DKRE
   moeten we het versie-nummer vullen in de tabel STM_VERSIE_WIJZIGINGEN
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W1990', sysdate );

commit;


/* Cross grants tussen de COMP_-users onderling deel 1
*/
REM start RLE_W1990_grants1.sql
-- 01-12-2002 MDH probl 1731
@@ RLE.insert_cds_utl_file.sql

/* Switchen naar het juiste schema
*/
connect /@&&DBN
set define on
set verify off
define _password = "SU_PASSWORD"
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

@@ RLE.rle_dubbele_faktoren.tab
@@ RLE.rle_dfr_pk.con
@@ RLE.rle_dfr_bri.trg
@@ RLE.rle_dfr_bru.trg

@@ RLE.rle_dubbel.pks
@@ RLE.rle_dubbel.pkb
@@ RLE.rle_rle_toev.prc
@@ RLE.rle_rle_bri.trg

drop table rle_verzendbestemmingen_jn;
drop table rle_verzendbestemmingen;
drop public synonym rle_verzendbestemmingen_jn;
drop public synonym rle_verzendbestemmingen;
@@ RLE.rle_verzendbestemmingen.tab
@@ RLE.rle_verzendbestemmingen_jn.tab
@@ RLE.rle_vbg_pk.con
@@ RLE.rle_vbg_uk1.con
@@ RLE.rle_vbg_rrl_fk3.con
@@ RLE.rle_vbg_rrl_fk1.con
@@ RLE.rle_vbg_rrl_fk2.con
@@ RLE.rle_vbg_bri.trg
@@ RLE.rle_vbg_bru.trg
@@ RLE.rle_vbg_bs.trg
@@ RLE.rle_vbg_ard.trg
@@ RLE.rle_vbg_ar.trg
@@ RLE.rle_vbg_as.trg
@@ RLE.rle_rrl_as.trg


@@ RLE.rle_asa_bru.trg
@@ RLE.rle_asa_bri.trg

@@ RLE.rle_rle_bri.trg
@@ RLE.rle_rle_bru.trg

@@ RLE.rle_fnr_brd.trg
@@ RLE.rle_fnr_bri.trg
@@ RLE.rle_fnr_bru.trg

-- 20-11-2002, VER, toevoeging agv P1646
@@ RLE.rle_ras_bri.trg
@@ RLE.rle_ras_bru.trg
@@ RLE.rle_ras_brd.trg
-- 20-11-2002, VER, einde toevoeging agv P1646

@@ RLE.rle_rec_bri.trg
@@ RLE.rle_rec_bru.trg
@@ RLE.rle_rec_brd.trg

@@ RLE.rle_rrl_bri.trg
@@ RLE.rle_rrl_brd.trg

@@ RLE.rle_rkn_brd.trg
@@ RLE.rle_rkn_bri.trg
@@ RLE.rle_rkn_bru.trg
@@ RLE.rle_rkn_as.trg

@@ RLE.rle_cnr_brd.trg
@@ RLE.rle_cnr_bri.trg
@@ RLE.rle_cnr_bru.trg
-- 15-11-2002, HVI, begin toevoeging agv P1586
@@ RLE.rle_cnr_as.trg
-- 15-11-2002, HVI, einde toevoeging agv P1586

@@ RLE.rle_gbd_bri.trg
@@ RLE.rle_gbd_bru.trg
@@ RLE.rle_gbd_brd.trg

@@ RLE.rle_admin_gegevens.prc
@@ RLE.rle_standaard_adres.prc
@@ RLE.rle_get_verzend.prc

-- 07-11-2002, VER, begin toevoeging agv P1545
@@ RLE.rle_rle_ck22.con
-- 07-11-2002, VER, einde toevoeging agv P1545

-- 15-11-2002, HVI, begin toevoeging agv P1564 en P1586
@@ RLE.rle_chk_rle_instdat.prc
@@ RLE.rle_m30_comnum.prc
-- 15-11-2002, HVI, einde toevoeging agv P1564 en P1586

-- TOEGEVOEGD AGV P1631
@@ RLE.rle_pck_lijsten.pks
@@ RLE.rle_pck_lijsten.pkb

-- 21-11-2002 JWB P1657
@@ RLE.rle_get_verzendnaam.fnc
-- 27-11-2002 AVB P1609
@@ RLE.insert_rle_sadres_rollen.sql

-- 02-12-2002 mdh P1731
@@ RLE.rle_npe_pck.pks
@@ RLE.rle_npe_pck.pkb

-- 03-12-2002 mdh p1750
@@ RLE.rle_mut_postcode.atb

@@ RLE.rle_gba_muteren_verwant.prc
@@ RLE.rle_gba_toevoegen_anummer.prc
@@ RLE.rle_gba_toevoegen_verwant.prc
@@ RLE.rle_rec_as.trg
@@ RLE.rle_rle_get_aanhef.prc

@@ RLE.rle_verwerk_ras.fnc

-- 12-12-2002 mdh p1845
@@ RLE.rle_rle_as.trg

/* Cross grants tussen de COMP_-users onderling deel 2
*/
connect /@&&DBN
REM start RLE_W1990_grants2.sql

/* creatie database-rollen
*/
connect /@&&DBN

@@ RLE.verw_partner_1.sql
@@ RLE.verw_partner_2.sql
@@ RLE.vul_stm_rle0061f.sql

@@ RLE.insert_geg_rle0340f.sql

@@ RLE0340F.rol
@@ RLE0270F.rol
@@ RLE0340F_DIENSTENGEBRUIK.sql
@@ GEBRUIK_SEL_OPU_PST.sql

-- 07-11-2002, VER, begin toevoeging agv P1545
@@ RLE-10359.qms_messages.sql
-- 07-11-2002, VER, einde toevoeging agv P1545

@@ RLE0410F.rol


/* RLE_REF_CODES vullen en check constraints plaatsen
*/
connect /@&&DBN

@@ RLE.bev395_qms_messages.sql
@@ RLE.insert_rle_sadres_rollen.sql
@@ RLE_GET_VERZEND.sql
@@ RLE.ophalen_adres_verzend.sql
@@ RLE.vul_stm_rle0340f.sql

@@ RLE_RLE_TOEV.sql
@@ RLE.registreren_administratie.sql
@@ RLE.registreren_begunstigde.sql
@@ RLE.registreren_deelnemer.sql
@@ RLE.registreren_persoon.sql
@@ RLE.registreren_werkgever.sql

@@ FACTOREN.sql
@@ MES397.sql

-- P1718
@@ RLE.update_par_rle_cnr_verwerk.sql

-- P1724
@@ RLE0110F.sql

-- P1728
@@ RLE.ondh_adres.sql

-- 21-11-2002 JWB P1657
start RLE.rle_get_verzendnaam.sql
start RLE.ophalen_verzendnaam.sql

-- P1763
@@ RLE.ins_gif_rle0110f.sql
@@ RLE.percdup.sql

-- P1779
@@ GEBRUIK_ONDH_CONTACTPERSONEN.sql



--
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W1990 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W1990 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

