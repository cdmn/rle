merge into rle_postsoort_adressoorten a using
 (select 20                                  id
  ,      'UPO'                              postsoort
  ,      'CA'                               adressoort
  ,      1                                  prioriteit
  from dual) b
on (    a.id = b.id 
and     a.postsoort = b.postsoort)
when not matched then 
insert (id, postsoort, adressoort, prioriteit)
values ( b.id, b.postsoort, b.adressoort, b.prioriteit )
when matched then
update set a.adressoort = b.adressoort
,          a.prioriteit = b.prioriteit
;

merge into rle_postsoort_adressoorten a using
 (select 21                                 id
  ,      'UPO'                              postsoort
  ,      'DA'                               adressoort
  ,      2                                  prioriteit
  from dual) b
on (    a.id = b.id 
and     a.postsoort = b.postsoort)
when not matched then 
insert (id, postsoort, adressoort, prioriteit)
values ( b.id, b.postsoort, b.adressoort, b.prioriteit )
when matched then
update set a.adressoort = b.adressoort
,          a.prioriteit = b.prioriteit
;

merge into rle_postsoort_adressoorten a using
 (select 22                                 id
  ,      'UPO'                              postsoort
  ,      'OA'                               adressoort
  ,      3                                  prioriteit
  from dual) b
on (    a.id = b.id 
and     a.postsoort = b.postsoort)
when not matched then 
insert (id, postsoort, adressoort, prioriteit)
values ( b.id, b.postsoort, b.adressoort, b.prioriteit )
when matched then
update set a.adressoort = b.adressoort
,          a.prioriteit = b.prioriteit
;
