delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0150F';
delete from stm_moduleparameters
    where mde_module = 'RLE0150F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0150F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0150F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0150F';
DELETE FROM stm_modules
    WHERE module = 'RLE0150F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0150F',1,'RLE',1
    ,'Onderhouden postcodes','S'
    ,'RLE0150F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_POST','RLE0150F',1,'N'
    ,'Postcode','OUT');
