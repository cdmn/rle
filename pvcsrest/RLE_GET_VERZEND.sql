delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_VERZEND';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_VERZEND';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_VERZEND';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_VERZEND';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_VERZEND';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_VERZEND';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_VERZEND',1,'RLE',1
    ,'Ophalen verzendbestemmingen voor een relatie','P'
    ,'RLE_GET_VERZEND',''
    ,'A',''
    ,15);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_GET_VERZEND',1,'N'
    ,'P_CODE_SOORT_ADRES','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_GET_VERZEND',1,'N'
    ,'P_POSTCODE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N11','RLE_GET_VERZEND',1,'N'
    ,'P_HUISNUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_GET_VERZEND',1,'N'
    ,'P_HUISNR_TOEV','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A13','RLE_GET_VERZEND',1,'N'
    ,'P_WOONPLAATS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_GET_VERZEND',1,'N'
    ,'P_STRAAT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_GET_VERZEND',1,'N'
    ,'P_CODLAND','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_VERZEND',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_VERZEND',1,'J'
    ,'P_POSTSOORT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_VERZEND',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_GET_VERZEND',1,'N'
    ,'P_PEILDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N5','RLE_GET_VERZEND',1,'N'
    ,'P_NUMRELATIE_BETREFT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GET_VERZEND',1,'N'
    ,'P_NAAM_BETREFT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N7','RLE_GET_VERZEND',1,'N'
    ,'P_NUMRELATIE_HOORT_BIJ','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GET_VERZEND',1,'N'
    ,'P_NAAM_HOORT_BIJ','OUT');
