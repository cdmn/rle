prompt GET_ADRES_ZAKELIJK
set define off
set heading off
set pagesize 0
set linesize 1000
set trimspool on
set echo off
set termout off
set feedback off
spool tempgif.sql
prompt prompt terugzetten gif
select 'insert into stm_gebruik_interfaces
 (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
 values
 ((select id from stm_interfaces where codzoek = ''GET_ADRES_ZAKELIJK''),1,'''||mde_module||''',1);'
 FROM stm_gebruik_interfaces g
 ,    stm_interfaces i
 WHERE g.ifv_ife_id = i.id and codzoek = 'GET_ADRES_ZAKELIJK';
spool off
spool tempdgb.sql
prompt prompt terugzetten dgb
select 'insert into aut_dienstengebruik
 (ife_id,fot_korte_naam)
 values
 ((select id from stm_interfaces where codzoek = ''GET_ADRES_ZAKELIJK''),'''||g.fot_korte_naam||''');'
 FROM aut_dienstengebruik g
 ,    stm_interfaces i
 WHERE g.ife_id = i.id and codzoek = 'GET_ADRES_ZAKELIJK';
spool off
SET termout ON
SET feedback ON
DELETE FROM  stm_gebruik_interfaces
    WHERE ifv_ife_id = (SELECT id FROM stm_interfaces WHERE codzoek = 'GET_ADRES_ZAKELIJK');
DELETE FROM aut_dienstengebruik
    WHERE ife_id = (SELECT id FROM stm_interfaces
         WHERE codzoek = 'GET_ADRES_ZAKELIJK');
DELETE FROM stm_wrdparameters
    WHERE mif_ifv_ife_id = (SELECT id FROM stm_interfaces
         WHERE codzoek = 'GET_ADRES_ZAKELIJK');
delete from stm_moduleinterfaces
    where ifv_ife_id = (select id from stm_interfaces
         where codzoek = 'GET_ADRES_ZAKELIJK');
delete from stm_abonnementen
    where ife_id = (select id from stm_interfaces
         where codzoek = 'GET_ADRES_ZAKELIJK');
delete from stm_interface_parameters
 where ifv_ife_id = (select id from stm_interfaces
         where codzoek = 'GET_ADRES_ZAKELIJK');
delete from stm_interface_versies
    where ife_id = (select id from stm_interfaces
         where codzoek = 'GET_ADRES_ZAKELIJK');
delete from stm_interfaces
    where codzoek = 'GET_ADRES_ZAKELIJK';
PROMPT interface
INSERT into stm_interfaces
    (ave_name,ave_numversie,codzoek,windowtitel,subtype,chk_null)
    values
    ('RLE','1',
    'GET_ADRES_ZAKELIJK','Ophalen zakenadres',
    'D','');
prompt interface versies
INSERT into stm_interface_versies
    (ife_id,numversie,codstatus)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK')
    ,1,'A');
PROMPT module
prompt moduleinterface
INSERT into stm_moduleinterfaces
    (ifv_ife_id,ifv_numversie,volgnum,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK')
    ,1,1
    ,'RLE_M10_ADRES'
    ,1);
prompt wrdparameters
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'N1'
    ,'RLE_M10_ADRES',1,''
    ,'2','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A10'
    ,'RLE_M10_ADRES',1,''
    ,'7','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A12'
    ,'RLE_M10_ADRES',1,''
    ,'9','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A11'
    ,'RLE_M10_ADRES',1,''
    ,'8','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A15'
    ,'RLE_M10_ADRES',1,''
    ,'12','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A14'
    ,'RLE_M10_ADRES',1,''
    ,'11','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A13'
    ,'RLE_M10_ADRES',1,''
    ,'10','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A2'
    ,'RLE_M10_ADRES',1,''
    ,'3','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A3'
    ,'RLE_M10_ADRES',1,''
    ,'&SZ','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'D4'
    ,'RLE_M10_ADRES',1,''
    ,'4','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A5'
    ,'RLE_M10_ADRES',1,''
    ,'&J','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'N6'
    ,'RLE_M10_ADRES',1,''
    ,'5','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'N7'
    ,'RLE_M10_ADRES',1,''
    ,'6','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A8'
    ,'RLE_M10_ADRES',1,''
    ,'&','',NULL);
INSERT into stm_wrdparameters
    (mif_ifv_ife_id,mif_ifv_numversie,mif_volgnum
    ,mpr_prm_cod,mpr_mde_module,mpr_mde_numversie
    ,ggn_cod,wrd,kae_loge_cod,kae_volgnum)
    values
    ((select id from stm_interfaces where codzoek = 'GET_ADRES_ZAKELIJK'),1
    ,1,'A9'
    ,'RLE_M10_ADRES',1,''
    ,'7','',NULL);
@tempgif
@tempdgb
set define on
