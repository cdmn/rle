delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GBA_GET_STATUS';
delete from stm_moduleparameters
    where mde_module = 'RLE_GBA_GET_STATUS';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GBA_GET_STATUS';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GBA_GET_STATUS';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GBA_GET_STATUS';
DELETE FROM stm_modules
    WHERE module = 'RLE_GBA_GET_STATUS';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GBA_GET_STATUS',1,'RLE',1
    ,'Ophalen GBA status van een persoon','P'
    ,'RLE_GBA_GET_STATUS',''
    ,'A',''
    ,4);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GBA_GET_STATUS',1,'J'
    ,'PIN_PERSOONSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GBA_GET_STATUS',1,'J'
    ,'PIN_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GBA_GET_STATUS',1,'J'
    ,'POV_GBA_STATUS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GBA_GET_STATUS',1,'J'
    ,'POV_GBA_AFNEMERS_INDICATIE','OUT');
