delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_M10_ADRES';
delete from stm_moduleparameters
    where mde_module = 'RLE_M10_ADRES';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_M10_ADRES';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_M10_ADRES';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_M10_ADRES';
DELETE FROM stm_modules
    WHERE module = 'RLE_M10_ADRES';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_M10_ADRES',1,'RLE',1
    ,'ophalen adres','P'
    ,'RLE_M10_ADRES',''
    ,'A',''
    ,15);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_M10_ADRES',1,'J'
    ,'P_O_REGEL5','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_M10_ADRES',1,'J'
    ,'P_O_REGEL6','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_M10_ADRES',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_M10_ADRES',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_M10_ADRES',1,'J'
    ,'P_SRTADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_M10_ADRES',1,'J'
    ,'P_DATUMP','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_M10_ADRES',1,'J'
    ,'P_IND_OPM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N6','RLE_M10_ADRES',1,'J'
    ,'P_AANT_REGELS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N7','RLE_M10_ADRES',1,'J'
    ,'P_AANT_POS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_M10_ADRES',1,'J'
    ,'P_LAYOUTCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_M10_ADRES',1,'J'
    ,'P_O_MELDING','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_M10_ADRES',1,'J'
    ,'P_O_REGEL1','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_M10_ADRES',1,'J'
    ,'P_O_REGEL2','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_M10_ADRES',1,'J'
    ,'P_O_REGEL3','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A13','RLE_M10_ADRES',1,'J'
    ,'P_O_REGEL4','MOD');
