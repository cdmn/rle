DELETE RLE_ref_codes
/


INSERT INTO RLE_ref_codes(
     RV_DOMAIN
   , RV_LOW_VALUE
   , RV_HIGH_VALUE
   , RV_ABBREVIATION
   , RV_MEANING
   ) VALUES(
     'STATUS AANGEMELDE PERSOON'
   , 'AA'
   , ''
   , 'Aangemeld'
   , 'Aangemeld'
   )
/


INSERT INTO RLE_ref_codes(
     RV_DOMAIN
   , RV_LOW_VALUE
   , RV_HIGH_VALUE
   , RV_ABBREVIATION
   , RV_MEANING
   ) VALUES(
     'STATUS AANGEMELDE PERSOON'
   , 'IA'
   , ''
   , 'In afstemming'
   , 'In afstemming'
   )
/


INSERT INTO RLE_ref_codes(
     RV_DOMAIN
   , RV_LOW_VALUE
   , RV_HIGH_VALUE
   , RV_ABBREVIATION
   , RV_MEANING
   ) VALUES(
     'STATUS AANGEMELDE PERSOON'
   , 'TB'
   , ''
   , 'Te beoordelen'
   , 'Te beoordelen'
   )
/


INSERT INTO RLE_ref_codes(
     RV_DOMAIN
   , RV_LOW_VALUE
   , RV_HIGH_VALUE
   , RV_ABBREVIATION
   , RV_MEANING
   ) VALUES(
     'STATUS AANGEMELDE PERSOON'
   , 'OV'
   , ''
   , 'Overgezet'
   , 'Overgezet'
   )
/


INSERT INTO RLE_ref_codes(
     RV_DOMAIN
   , RV_LOW_VALUE
   , RV_HIGH_VALUE
   , RV_ABBREVIATION
   , RV_MEANING
   ) VALUES(
     'STATUS AANGEMELDE PERSOON'
   , 'AF'
   , ''
   , 'Afgekeurd'
   , 'Afgekeurd'
   )
/


