DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-10356'
/

DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-10356'
/

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10356', 'Handelsnaam mag niet meer dan 35 posities bevatten i.v.m. registratie in RWG.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10356', 'DUT'
            , 'Handelsnaam mag niet meer dan 35 posities bevatten i.v.m. registratie in RWG.'
            , '');

