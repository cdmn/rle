delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0110F';
delete from stm_moduleparameters
    where mde_module = 'RLE0110F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0110F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0110F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0110F';
DELETE FROM stm_modules
    WHERE module = 'RLE0110F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0110F',1,'RLE',1
    ,'Onderhouden adressen','S'
    ,'RLE0110F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A_G_SRT_AD','RLE0110F',1,'N'
    ,'Alleen geg srt adres','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NUM_ADRES','RLE0110F',1,'N'
    ,'Adres','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('COD_ROL_R','RLE0110F',1,'N'
    ,'Code Rol Referentie','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0110F',1,'N'
    ,'Relatienr','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0110F',1,'N'
    ,'Rol','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('DAT_PEIL','RLE0110F',1,'N'
    ,'Peildatum','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SRT_ADRES','RLE0110F',1,'N'
    ,'Adressoort','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0110F',1,'RAS_ID',
        'RAS.ID');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0110F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0110F',1,'SRT_ADRES',
        ':RAS.DWE_WRDDOM_SRTDOM');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0110F',1,'NUM_ADRES',
        ':RAS.ADS_NUMADRES');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0110F',1,'REF_SAD',
        ':RAS.DWE_WRDDOM_SRTADR');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0110F',1,'CODE_ROL',
        ':RAS.ROL_CODROL');
