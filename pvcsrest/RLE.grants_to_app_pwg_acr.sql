/*************************************************************************
***              Uitdelen RLE grants aan APP_PWG_ACR                   ***
*************************************************************************/
prompt Uitdelen RLE grants aan APP_PWG_ACR

grant select on COMP_RLE.RLE_V_ADMINISTRATIEKANTOREN to APP_PWG_ACR;
grant select on COMP_RLE.RLE_V_WERKGEVERS_XL         to APP_PWG_ACR;
grant select on COMP_RLE.RLE_V_NED_POSTCODE          to APP_PWG_ACR;
grant select on COMP_RLE.RLE_V_WERKGEVERS_XL_PORTAL  to APP_PWG_ACR;


