-- Programma: deelnemergegevens.sql
-- Auteur   : XKV 
-- Datum    : 23-12-2009 
-- Doel     : Deelnemergegevens voor de controles
-- Marval   :
-- Incident : 
------------------------------------------------------------------------------------------------------

set heading off
SET FEEDBACK OFF

spool PME.deelnemergegevens.csv

select    adm.adm_code
       || ';'
       || prs.extern_relatie
       || ';'
       || sofi.extern_relatie
       || ';'
       || rle.namrelatie
       || ';'
       || fnc_bepaal_voorletters( rle.voorletter )
       || ';'
       || rle.datgeboorte
       || ';'
       || geslacht.waarde
       || ';'
       || rle.datoverlijden
       || ';'
       || asa.afnemers_indicatie
       || ';'
       || pens.extern_relatie
from   comp_rle.rle_relaties rle
     , comp_rle.rle_relatie_externe_codes prs
     , comp_rle.rle_relatie_externe_codes sofi
     , comp_rle.rle_relatie_externe_codes pens
     , domeinwaarden geslacht
     , rle_relatierollen_in_adm adm
     , rle_afstemmingen_gba asa
where  rle.numrelatie = prs.rle_numrelatie
and    prs.rol_codrol = 'PR'
and    prs.dwe_wrddom_extsys = 'PRS'
and    rle.numrelatie = sofi.rle_numrelatie(+)
and    sofi.rol_codrol(+) = 'PR'
and    sofi.dwe_wrddom_extsys(+) = 'SOFI'
and    rle.numrelatie = pens.rle_numrelatie(+)
and    pens.rol_codrol(+) = 'PR'
and    pens.dwe_wrddom_extsys(+) = 'PENS'
and    geslacht.domeincode = 'GES'
and    geslacht.code = rle.dwe_wrddom_geslacht
and    rle.numrelatie = adm.rle_numrelatie
and    rle.numrelatie = asa.rle_numrelatie(+)
and    adm.rol_codrol = 'PR'
and    adm.adm_code = 'PME'
;

spool off

spool BPFK.deelnemergegevens.csv

select    adm.adm_code
       || ';'
       || prs.extern_relatie
       || ';'
       || sofi.extern_relatie
       || ';'
       || rle.namrelatie
       || ';'
       || fnc_bepaal_voorletters( rle.voorletter )
       || ';'
       || rle.datgeboorte
       || ';'
       || geslacht.waarde
       || ';'
       || rle.datoverlijden
       || ';'
       || asa.afnemers_indicatie
       || ';'
       || pens.extern_relatie
from   comp_rle.rle_relaties rle
     , comp_rle.rle_relatie_externe_codes prs
     , comp_rle.rle_relatie_externe_codes sofi
     , comp_rle.rle_relatie_externe_codes pens
     , domeinwaarden geslacht
     , rle_relatierollen_in_adm adm
     , rle_afstemmingen_gba asa
where  rle.numrelatie = prs.rle_numrelatie
and    prs.rol_codrol = 'PR'
and    prs.dwe_wrddom_extsys = 'PRS'
and    rle.numrelatie = sofi.rle_numrelatie(+)
and    sofi.rol_codrol(+) = 'PR'
and    sofi.dwe_wrddom_extsys(+) = 'SOFI'
and    rle.numrelatie = pens.rle_numrelatie(+)
and    pens.rol_codrol(+) = 'PR'
and    pens.dwe_wrddom_extsys(+) = 'PENS'
and    geslacht.domeincode = 'GES'
and    geslacht.code = rle.dwe_wrddom_geslacht
and    rle.numrelatie = adm.rle_numrelatie
and    rle.numrelatie = asa.rle_numrelatie(+)
and    adm.rol_codrol = 'PR'
and    adm.adm_code = 'BPFK'
;

spool off

