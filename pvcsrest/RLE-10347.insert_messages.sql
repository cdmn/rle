INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10347', 'Indicaties en locatie vullen niet toegestaan bij postbus of antwoordnummer.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10347', 'DUT'
            , 'Indicaties en locatie vullen niet toegestaan bij postbus of antwoordnummer.'
            , 'Het is bij een adres met een postbus of antwoordnummer niet toegestaan de indicaties woonwagen of woonboot aan te vinken of om de locatie te vullen.');

