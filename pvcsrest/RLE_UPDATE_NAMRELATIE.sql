delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_UPDATE_NAMRELATIE';
delete from stm_moduleparameters
    where mde_module = 'RLE_UPDATE_NAMRELATIE';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_UPDATE_NAMRELATIE';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_UPDATE_NAMRELATIE';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_UPDATE_NAMRELATIE';
DELETE FROM stm_modules
    WHERE module = 'RLE_UPDATE_NAMRELATIE';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_UPDATE_NAMRELATIE',1,'RLE',1
    ,'Update statutaire naam','P'
    ,'RLE_UPDATE_NAMRELATIE',''
    ,'A',''
    ,3);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_UPDATE_NAMRELATIE',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_UPDATE_NAMRELATIE',1,'J'
    ,'P_NAMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_UPDATE_NAMRELATIE',1,'J'
    ,'P_SUCCES','OUT');
