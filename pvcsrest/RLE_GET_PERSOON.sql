delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_PERSOON';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_PERSOON';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_PERSOON';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_PERSOON';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_PERSOON';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_PERSOON';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_PERSOON',1,'RLE',1
    ,'Ophalen persoonsgegevens','P'
    ,'RLE_GET_PERSOON',''
    ,'A',''
    ,11);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_PERSOON',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_PERSOON',1,'J'
    ,'P_NAMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D3','RLE_GET_PERSOON',1,'J'
    ,'P_DATGEBOORTE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_PERSOON',1,'J'
    ,'P_VOORLETTER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GET_PERSOON',1,'J'
    ,'P_VOORVOEGSEL','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GET_PERSOON',1,'J'
    ,'P_STRAATNAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N7','RLE_GET_PERSOON',1,'J'
    ,'P_HUISNUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GET_PERSOON',1,'J'
    ,'P_TOEVHUISNUM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_GET_PERSOON',1,'J'
    ,'P_POSTCODE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_GET_PERSOON',1,'J'
    ,'P_WOONPLAATS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_GET_PERSOON',1,'J'
    ,'P_LAND','OUT');
