prompt toevoegen message RLE-10361

delete from qms_message_text
    where msp_code = 'RLE-10361';
delete from qms_message_properties
    where code = 'RLE-10361';
INSERT INTO qms_message_properties
    (code, description, severity, logging_ind, suppr_wrng_ind, suppr_always_ind, constraint_name)
    VALUES
    ('RLE-10361','Procent-teken gevonden op eerste positie, dit is niet toegestaan!','E'
    ,'N','N'
    ,'N','');
INSERT into qms_message_text
    (language, text, help_text, msp_code)
    values
    ('DUT','Procent-teken gevonden op eerste positie, dit is niet toegestaan!',''
    ,'RLE-10361');

COMMIT;
