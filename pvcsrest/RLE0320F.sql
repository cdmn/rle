delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0320F';
delete from stm_moduleparameters
    where mde_module = 'RLE0320F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0320F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0320F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0320F';
DELETE FROM stm_modules
    WHERE module = 'RLE0320F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0320F',1,'RLE',1
    ,'Selecteren soort adres per rol','S'
    ,'RLE0320F',''
    ,'A','Selecteren soort adres'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0320F',1,'J'
    ,'Rol','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SRT_ADRES','RLE0320F',1,'J'
    ,'Adressoort','OUT');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0320F',1,'SRT_ADRES',
        ':SAR.DWE_WRDDOM_SADRES');
