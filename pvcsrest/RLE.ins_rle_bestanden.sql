Insert into COMP_RLE.RLE_BESTANDEN
   (NAAM, 
    BSD_GROEP, BSD_TYPE, DB_DIRECTORY, FORMAAT_NAAM)
 Values
   ('OOM_WGR', 'OOM', 'WGR', 'RLE_OUTBOX', 'WGR_OOM_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.xml');
Insert into COMP_RLE.RLE_BESTANDEN
   (NAAM, 
    BSD_GROEP, BSD_TYPE, DB_DIRECTORY, FORMAAT_NAAM)
 Values
   ('OOM_WNR', 'OOM', 'WNR', 'RLE_OUTBOX', 'WNR_OOM_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.xml');
Insert into COMP_RLE.RLE_BESTANDEN
   (NAAM, 
    BSD_GROEP, BSD_TYPE, DB_DIRECTORY, FORMAAT_NAAM)
 Values
   ('OOC_WNR', 'OOC', 'WNR', 'RLE_OUTBOX', 'WNR_OOC_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.csv');
Insert into COMP_RLE.RLE_BESTANDEN
   (NAAM, 
    BSD_GROEP, BSD_TYPE, DB_DIRECTORY, FORMAAT_NAAM)
 Values
   ('OOC_WGR', 'OOC', 'WGR', 'RLE_OUTBOX', 'WGR_OOC_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.csv');
   
update rle_bestanden
set formaat_naam = 'WGR_OTIB_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.xml'
where naam = 'OTIB_WGR';
  
update rle_bestanden
set formaat_naam = 'WNR_OTIB_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.xml'
where naam = 'OTIB_WNR';
     
 commit;