delete from stm_wrdparameters
where mpr_mde_module = 'RLE0300F'
and   mpr_prm_cod    = 'CODE_ROL'
and   mif_ifv_ife_id = (select id from stm_interfaces 
                        where codzoek = 'ONDH_OVERIGE_ROLLEN');

delete from stm_moduleparameters
where mde_module = 'RLE0300F'
and   prm_cod    = 'CODE_ROL';

