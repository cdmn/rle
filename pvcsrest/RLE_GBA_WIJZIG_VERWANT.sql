delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GBA_WIJZIG_VERWANT';
delete from stm_moduleparameters
    where mde_module = 'RLE_GBA_WIJZIG_VERWANT';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GBA_WIJZIG_VERWANT';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GBA_WIJZIG_VERWANT';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GBA_WIJZIG_VERWANT';
DELETE FROM stm_modules
    WHERE module = 'RLE_GBA_WIJZIG_VERWANT';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GBA_WIJZIG_VERWANT',1,'RLE',1
    ,'I_088: Wijzigen verwantschap vanuit GBA (Alleen aan te roepen door GBA!!!)','P'
    ,'RLE_GBA_WIJZIG_VERWANT',''
    ,'A',''
    ,7);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GBA_WIJZIG_VERWANT',1,'J'
    ,'PIN_PERSOONSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GBA_WIJZIG_VERWANT',1,'J'
    ,'PIN_PERSOONSNUMMER_DEELNEMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D3','RLE_GBA_WIJZIG_VERWANT',1,'J'
    ,'PID_DAT_BEGIN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GBA_WIJZIG_VERWANT',1,'J'
    ,'PIV_CODE_SOORT_VERWANTSCHAP','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_GBA_WIJZIG_VERWANT',1,'J'
    ,'PID_DAT_EINDE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GBA_WIJZIG_VERWANT',1,'J'
    ,'POV_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GBA_WIJZIG_VERWANT',1,'J'
    ,'POV_FOUTMELDING','OUT');
