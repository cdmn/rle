/* Naam......: RLE_W4218.sql
** Release...: W4218
** Datum.....: 11-08-2007 08:39:04
** Notes.....: Gegenereerd door ITStools Versie 3.00
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;


WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4218', sysdate );

commit;


/* Switchen naar het juiste schema
*/
connect /@&&DBN
set define on
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

@@ RLE.rle_administraties.tab
@@ RLE.rle_relatierollen_in_adm.tab
@@ RLE.rle_communicatie_nummers.atb
@@ RLE.rle_financieel_nummers.atb
@@ RLE.rle_financieel_nummers_jn.atb
@@ RLE.rle_relatie_adressen.atb
@@ RLE.rle_relatie_adressen_jn.atb
@@ RLE.rle_insert_rie.prc
@@ RLE.rle_chk_gba_afst.fnc
@@ RLE.rle_ate_bri.trg
@@ RLE.rle_ate_bru.trg
@@ RLE.rle_cnr_bri.trg
@@ RLE.rle_fnr_bri.trg
@@ RLE.rle_ras_bri.trg
@@ RLE.rle_rie_bri.trg
@@ RLE.rle_rie_bru.trg
@@ RLE.rle_ras_as.trg
@@ RLE.rle_vul_adm_en_rie.sql
@@ RLE.rie_ate_fk_i.ind
@@ RLE.rie_rrl_fk_i.ind
@@ RLE.rle_ate_pk.con
@@ RLE.rle_rie_pk.con
@@ RLE.rle_cnr_rie_fk1.con
@@ RLE.rle_fnr_rie_fk1.con
@@ RLE.rle_ras_rie_fk1.con
@@ RLE.rle_rie_ate_fk.con
@@ RLE.rle_rie_rrl_fk.con
@@ RLE.rle_cnr_uk1.con
@@ RLE.rle_ras_uk1.con
@@ RLE.rle_fnr_pk.con
@@ RLE.rle_bijwerken_postsoort.sql

@@ RLE.rle_rle_chk_adres.prc
@@ RLE.rle_gba_wijzig_adres.prc
@@ RLE.rle_gba_toevoegen_adres.prc
@@ RLE.rle_gba_muteren_adres.prc
@@ RLE.rle_ras_as.trg


/* creatie database-rollen
*/
connect /@&&DBN
@@ RLE0061F.rol
@@ RLE0110F.rol

connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4218 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W4218 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on
