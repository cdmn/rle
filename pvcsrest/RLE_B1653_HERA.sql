/* Naam......: RLE_B1653_HERA.sql
** Release...: B1653
** Datum.....: Mon Mar 17 14:38:00 2003
** Notes.....: Aangemaakt met Des_tool_mn Versie 3.1.4.1.5.9d


*/

set linesize   79
set pause     off


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma beeindigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er vanuit JCS wordt gestart.
   Hier wordt er vanuit gegaan dat vanuit JCS de user OPS$xJCS wordt gebruikt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(user,6),'JCS','exit','continue') P_EXIT
from dual;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C en RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    into dummy
    from dba_users
   where username = 'COMP_RLE';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE bestaat niet in deze database,
*          neem contact op met FIT-be-op !');
end;
/

/* Voor databases afgeleid van ZEUS, dus aangemaakt vanuit EROS
   moeten we het versie-nummer vullen in de table IMU_APPLICATIE_VERSIE
   Voor database afgeleid van HERA, dus aangemaakt vanuit DKRE
   moeten we het versie-nummer vullen in de tabel STM_VERSIE_WIJZIGINGEN
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'B1653', sysdate );

commit;


/* Cross grants tussen de COMP_-users onderling deel 1
*/
REM start RLE_B1653_grants1.sql

/* Switchen naar het juiste schema is hier niet nodig (alleen STM-update)
*/
connect /@&&DBN

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT


REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
REM Hier de files zetten die vanuit deze directory aangeroepen moeten worden.

/*
   RLE0010F.sql wordt hier NIET aangeroepen, terwijl deze wel wordt opgeleverd!
   Het draaien van RLE0010F.sql betekend dat er nog een reeks andere scripts zou
   moeten draaien terwijl er slechts EEN tupel van de tabel STM_MODULEPARAMETRS
   hoeft te worden ge-UPDATE-d met behulp van RLE0010F_update_MPR.sql.
   Nieuwe versie van RLE0010F.sql is wel nodig bij deze oplevering.
*/

@@ RLE0010F_update_MPR.sql

REM Tot zover de inhoud van de directory
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* Cross grants tussen de COMP_-users onderling deel 2
*/
connect /@&&DBN
REM start RLE_B1653_grants2.sql

/* creatie database-rollen
*/
connect /@&&DBN
REM start RLE.????.rol

/* RLE_REF_CODES vullen en check constraints plaatsen
*/
connect /@&&DBN
REM start RLE.????.dom




insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'B1653 : Klaar', sysdate );

commit;

prompt Implementatie RLE release B1653 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

