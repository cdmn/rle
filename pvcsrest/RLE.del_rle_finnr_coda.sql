/*
YYW, 18-06-2014:
Opschonen "oude" tags in COMP_RLE: FINNR_CODA mbt rol in adm ZILVKR-U, BOVEMIJ-U en KMU-U
( Later events aanmaken voor werkgevers met de rol in admin  ZILVKR-U, BOVEMIJ-U en KMU-U 
  om Coda bij te werken.
 )
*/
set serveroutput on size 100000
set linesize 180
set trimspool on

Prompt Start script RLE.del_rle_finnr_coda.sql

Prompt
Prompt Schonen van RLE FINNR_TAGS voor Administratie ZILVKR-U, BOVEMIJ-U en KMU-U
prompt

prompt Vooraf Tellen van aantal records per administratie in RLE_FINNR_CODA
select substr(fnc.adm_code,1,30) administratie
,count(fnc.fnr_id) aantal_recs 
from   rle_finnr_coda fnc
group by adm_code
order by administratie;

prompt Verwijderen van alle records van de administratie uit RLE_FINNR_CODA
delete from rle_finnr_coda fnc
where fnc.adm_code in ('ZILVKR-U','BOVEMIJ-U','KMU-U');

prompt Achteraf Tellen van aantal records per administratie in RLE_FINNR_CODA
select substr(fnc.adm_code,1,30) administratie
,count(fnc.fnr_id) aantal_recs 
from   rle_finnr_coda fnc
group by adm_code
order by administratie;

prompt Einde script RLE.del_rle_finnr_coda.sql

