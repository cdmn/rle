delete from rle_dubbele_faktoren;

INSERT INTO RLE_DUBBELE_FAKTOREN ( PROCES, RUBRIEK, FAKTOR_LENGTE, FAKTOR_INHOUD,
FAKTOR_VOLGORDE, FAKTOR_LEEG, FAKTOR_ZWAARTE)
VALUES ( 
'PERSOON', 'NAAM', 25, 25, 50, NULL, 10); 
INSERT INTO RLE_DUBBELE_FAKTOREN ( PROCES, RUBRIEK, FAKTOR_LENGTE, FAKTOR_INHOUD,
FAKTOR_VOLGORDE, FAKTOR_LEEG, FAKTOR_ZWAARTE)
VALUES ( 
'PERSOON', 'GEBOORTEDATUM', 0, 15, 85, NULL, 5); 
INSERT INTO RLE_DUBBELE_FAKTOREN ( PROCES, RUBRIEK, FAKTOR_LENGTE, FAKTOR_INHOUD,
FAKTOR_VOLGORDE, FAKTOR_LEEG, FAKTOR_ZWAARTE)
VALUES ( 
'PERSOON', 'VOORLETTERS', 30, 30, 40, NULL, 2); 
INSERT INTO RLE_DUBBELE_FAKTOREN ( PROCES, RUBRIEK, FAKTOR_LENGTE, FAKTOR_INHOUD,
FAKTOR_VOLGORDE, FAKTOR_LEEG, FAKTOR_ZWAARTE)
VALUES ( 
'WERKGEVER', 'NAAM', 25, 25, 50, NULL, 5); 
INSERT INTO RLE_DUBBELE_FAKTOREN ( PROCES, RUBRIEK, FAKTOR_LENGTE, FAKTOR_INHOUD,
FAKTOR_VOLGORDE, FAKTOR_LEEG, FAKTOR_ZWAARTE)
VALUES ( 
'WEKRGEVER', 'POSTCODE', 0, 50, 50, NULL, 5); 
INSERT INTO RLE_DUBBELE_FAKTOREN ( PROCES, RUBRIEK, FAKTOR_LENGTE, FAKTOR_INHOUD,
FAKTOR_VOLGORDE, FAKTOR_LEEG, FAKTOR_ZWAARTE ) 
VALUES ( 
'WERKGEVER', 'HUISNUMMER', 10, 40, 50, NULL, 5); 
commit;
 
