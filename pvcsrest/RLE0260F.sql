delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0260F';
delete from stm_moduleparameters
    where mde_module = 'RLE0260F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0260F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0260F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0260F';
DELETE FROM stm_modules
    WHERE module = 'RLE0260F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0260F',1,'RLE',1
    ,'Onderhouden kanaalvoorkeur','S'
    ,'RLE0260F',''
    ,'A','Onderhouden kanaalvoorkeur'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0260F',1,'J'
    ,'Relatienr','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0260F',1,'REF_KVR',
        ':KVR.CODE_SOORT_KANAALVOORKEUR');
