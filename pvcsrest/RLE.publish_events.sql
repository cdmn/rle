set serveroutput on
set trimspool on

prompt Start script RLE.publish_events.sql
prompt
prompt Aanmaken van Relatierol in administratie
prompt De rol wordt aangemaakt voor de uitkeringsbedrijven van CODA: BOVEMIJ-U, KMU-U en ZILVKR-U
prompt

prompt
prompt Tellen van aantal relaties met de rol per administratie voor aanmaken events
prompt
select adm_code, count(rie.rle_numrelatie) aantal
from rle_relatierollen_in_adm rie
where rie.adm_code in ('BOVEMIJ-U','KMU-U','ZILVKR-U')
group by rie.adm_code
order by rie.adm_code;

prompt
prompt Voor alle relaties van de administratie in RLE opnieuw signaal klaarzetten voor CODA
prompt

declare
  cursor c_rie
  is
    select rie.*
    from rle_relatierollen_in_adm rie
    where rie.adm_code in ('ZILVKR-U','BOVEMIJ-U','KMU-U')
    order by adm_code;
--
  l_aantal_bestaande pls_integer := 0;
begin
--
  for r_rie in c_rie
  loop
    /* publicatie van insert*/
    ibm.publish_event ('MUT RLE ROL IN ADMIN'
                           ,'I'
                           ,to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')
                           ,null,r_rie.rle_numrelatie
                           ,null,r_rie.rol_codrol
                           ,null,r_rie.adm_code
                           ,null,to_char(r_rie.dat_creatie,'dd-mm-yyyy hh24:mi:ss')
                           ,null,r_rie.creatie_door
                           ,null,to_char(r_rie.dat_mutatie,'dd-mm-yyyy hh24:mi:ss')
                           ,null,r_rie.mutatie_door
                           );
    --
    l_aantal_bestaande := l_aantal_bestaande + 1;                       
    --
  end loop;
  --
  dbms_output.put_line('Bestaande relaties opnieuw klaargezet voor bestaande gevallen: '||l_aantal_bestaande);
 end; 
 /

set serveroutput on
set trimspool on

prompt
prompt Einde publiceren events voor bestaande toegewezen aanspraken
select adm_code, count(rie.rle_numrelatie) aantal
from rle_relatierollen_in_adm rie
where rie.adm_code in ('BOVEMIJ-U','KMU-U','ZILVKR-U')
group by rie.adm_code
order by rie.adm_code;

prompt
prompt Einde script RLE.publish_events.sql
 