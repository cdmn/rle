INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10337', 'De einddatum mag niet voor de begindatum liggen.'
            , 'E', 'N', 'N', 'N', 'RLE_KVR_CK1');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10337', 'DUT'
            , 'De einddatum mag niet voor de begindatum liggen.'
            , '');

