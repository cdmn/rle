/* Naam......: RLE_W1684_HERA.sql
** Release...: 1
** Datum.....: Thu May XX 2002


*/

set linesize   79
set pause     off


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma beeindigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er vanuit JCS wordt gestart.
   Hier wordt er vanuit gegaan dat vanuit JCS de user OPS$xJCS wordt gebruikt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(user,6),'JCS','exit','continue') P_EXIT
from dual;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Voor databases afgeleid van ZEUS, dus aangemaakt vanuit EROS
   moeten we het versie-nummer vullen in de table IMU_APPLICATIE_VERSIE
   Voor database afgeleid van HERA, dus aangemaakt vanuit DKRE
   moeten we het versie-nummer vullen in de tabel STM_VERSIE_WIJZIGINGEN
*/

insert into STM_VERSIE_WIJZIGINGEN
       values ( 'RLE', 'W1684', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* insert scripts
*/
connect /@&&DBN

@@ RLE_ref_codes.sql
@@ RLE.verwijder_messages.sql
@@ RLE.insert_messages.sql
@@ RLE.insert_rle_rollen.sql
@@ RLE.insert_rle_sadres_rollen.sql
@@ RLE.insert_rle_rol_in_koppelingen.sql
@@ RLE.update_rle_rol_in_koppelingen.sql
@@ RLEIFE.sql
@@ RLE-10332.verwijder_messages.sql
@@ RLE-10332.insert_messages.sql
@@ RLE-10333.verwijder_messages.sql
@@ RLE-10333.insert_messages.sql
@@ RLE-10334.verwijder_messages.sql
@@ RLE-10334.insert_messages.sql
@@ RLE-10318.qms_messages.sql
@@ RLE-10327.verwijder_messages.sql
@@ RLE-10327.insert_messages.sql
@@ RLE-10335.verwijder_messages.sql
@@ RLE-10335.insert_messages.sql
@@ RLE-10337.verwijder_messages.sql
@@ RLE-10337.insert_messages.sql
@@ RLE-10338.verwijder_messages.sql
@@ RLE-10338.insert_messages.sql
@@ RLE.ins_del_message.sql
@@ RLE-10339.ins_del_messages.sql
@@ RLE-10341.verwijder_messages.sql
@@ RLE-10341.insert_messages.sql
@@ RLE-10342.verwijder_messages.sql
@@ RLE-10342.insert_messages.sql
@@ RLE-10346.verwijder_messages.sql
@@ RLE-10346.insert_messages.sql
@@ RLE-10347.verwijder_messages.sql
@@ RLE-10347.insert_messages.sql
@@ RLE-10351.verwijder_messages.sql
@@ RLE-10352.verwijder_messages.sql
@@ RLE-10353.verwijder_messages.sql
@@ RLE-10354.verwijder_messages.sql
@@ RLE-10355.verwijder_messages.sql
@@ RLE-10351.insert_messages.sql
@@ RLE-10352.insert_messages.sql
@@ RLE-10353.insert_messages.sql
@@ RLE-10354.insert_messages.sql
@@ RLE-10355.insert_messages.sql

prompt Implementatie RLE release W1684 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

