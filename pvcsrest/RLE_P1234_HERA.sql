/* Naam......: RLE_P1234_HERA.sql
** Release...: 1
** Datum.....: Thu May XX 2002


*/

set linesize   79
set pause     off


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma beeindigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er vanuit JCS wordt gestart.
   Hier wordt er vanuit gegaan dat vanuit JCS de user OPS$xJCS wordt gebruikt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(user,6),'JCS','exit','continue') P_EXIT
from dual;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Voor databases afgeleid van ZEUS, dus aangemaakt vanuit EROS
   moeten we het versie-nummer vullen in de table IMU_APPLICATIE_VERSIE
   Voor database afgeleid van HERA, dus aangemaakt vanuit DKRE
   moeten we het versie-nummer vullen in de tabel STM_VERSIE_WIJZIGINGEN
*/

insert into STM_VERSIE_WIJZIGINGEN
       values ( 'RLE', 'P1234', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* insert scripts
*/
connect /@&&DBN

start RLE-10351.verwijder_messages.sql
start RLE-10352.verwijder_messages.sql
start RLE-10353.verwijder_messages.sql
start RLE-10354.verwijder_messages.sql
start RLE-10355.verwijder_messages.sql
start RLE-10351.insert_messages.sql
start RLE-10352.insert_messages.sql
start RLE-10353.insert_messages.sql
start RLE-10354.insert_messages.sql
start RLE-10355.insert_messages.sql
start RLE0290F.sql
start RLE_PCK_LIJSTEN_PRC_GET_I050.sql
start RLE_PCK_LIJSTEN_PRC_INIT_I050.sql
start RLE.get_contactpersonen_rij.sql
start RLE.get_contactpersonen_start.sql

WHENEVER SQLERROR CONTINUE
WHENEVER OSERROR  CONTINUE
start RLE.insert_rle_rol_in_koppelingen.sql
WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

prompt Implementatie RLE release P1234 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

