delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_RLE';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_RLE';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_RLE';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_RLE';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_RLE';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_RLE';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_RLE',1,'RLE',1
    ,'Ophalen relatie','P'
    ,'RLE_GET_RLE',''
    ,'A',''
    ,27);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A26','RLE_GET_RLE',1,'J'
    ,'P_HANDELSNAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D27','RLE_GET_RLE',1,'J'
    ,'P_DATBEGIN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_RLE',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_RLE',1,'J'
    ,'P_CODORGANISATIE','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_RLE',1,'J'
    ,'P_NAMRELATIE','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_RLE',1,'J'
    ,'P_INDGEWNAAM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GET_RLE',1,'J'
    ,'P_INDINSTELLING','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D6','RLE_GET_RLE',1,'J'
    ,'P_DATSCHONING','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GET_RLE',1,'J'
    ,'P_TITEL','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GET_RLE',1,'J'
    ,'P_TTITEL','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_GET_RLE',1,'J'
    ,'P_AVGSL','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_GET_RLE',1,'J'
    ,'P_VVGSL','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_GET_RLE',1,'J'
    ,'P_BRGST','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_GET_RLE',1,'J'
    ,'P_GESLACHT','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A13','RLE_GET_RLE',1,'J'
    ,'P_GEWATITEL','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_GET_RLE',1,'J'
    ,'P_VOORNAAM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_GET_RLE',1,'J'
    ,'P_RVORM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A16','RLE_GET_RLE',1,'J'
    ,'P_AANSCHRIJFNAAM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A17','RLE_GET_RLE',1,'J'
    ,'P_VOORLETTER','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A18','RLE_GET_RLE',1,'J'
    ,'P_DATGEBOORTE','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A19','RLE_GET_RLE',1,'J'
    ,'P_CODE_GEBDAT_FICTIEF','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A20','RLE_GET_RLE',1,'J'
    ,'P_DATOVERLIJDEN','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A21','RLE_GET_RLE',1,'J'
    ,'P_DATOPRICHTING','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A22','RLE_GET_RLE',1,'J'
    ,'P_DATEINDE','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A23','RLE_GET_RLE',1,'J'
    ,'P_NAAM_PARTNER','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A24','RLE_GET_RLE',1,'J'
    ,'P_VVGSL_PARTNER','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A25','RLE_GET_RLE',1,'J'
    ,'P_CODE_AANDUIDING_NGK','MOD');
