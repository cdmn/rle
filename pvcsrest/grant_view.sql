set pagesize   0
set linesize 220
set embedded  on
set trimspool on
set echo     off
set feedback off
set heading  off
set verify   off

col directory new_value _directory noprint
col verwijderen new_value _verwijderen noprint
col DBN new_value DBN noprint

set pause off

select decode(substr(userenv('terminal'),1,3)
,null,'/work/'
,'pts','/work/'
,'%TEMP%\') directory
,      decode(substr(userenv('terminal'),1,3)
,null,'rm -f'
,'pts','rm -f'
,'del') verwijderen
from dual;

select name DBN from v$database;

break on owner
column owner format A300 fold_after

prompt conn /@&&DBN

spool off

set pagesize  24
set linesize  79
set heading   on
set verify    on
set feedback  on
set pause    off
set echo     off
set embedded off

WHENEVER SQLERROR CONTINUE


connect /@&&DBN

set define on
define _password = "SU_PASSWORD"
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RFE');
alter user COMP_RFE identified by COMP_RFE;
connect COMP_RFE/COMP_RFE@&&DBN
alter user COMP_RFE identified by values '&&_password';
column password clear
undefine _password
grant select on rfe_v_domeinwaarden to comp_rle with grant option;

connect /@&&DBN

set define on
define _password = "SU_PASSWORD"
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password
grant select on RLE_V_RLE_OOK_ZONDER_ADR to ROL_RAADPLEEG_BOVEMIJ
     , ROL_R_MUT_BOVEMIJ;
set define on
