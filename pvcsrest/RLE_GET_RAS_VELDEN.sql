delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_RAS_VELDEN';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_RAS_VELDEN';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_RAS_VELDEN';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_RAS_VELDEN';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_RAS_VELDEN';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_RAS_VELDEN';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_RAS_VELDEN',1,'RLE',1
    ,'I_082: Ophalen relatie via adres','P'
    ,'RLE_GET_RAS_VELDEN',''
    ,'A',''
    ,4);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_RAS_VELDEN',1,'J'
    ,'PIN_RAS_ID','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GET_RAS_VELDEN',1,'J'
    ,'PON_NUMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_RAS_VELDEN',1,'J'
    ,'POV_CODROL','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_RAS_VELDEN',1,'J'
    ,'POV_STADR','OUT');
