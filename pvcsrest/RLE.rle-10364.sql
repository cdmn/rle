
DELETE qms_message_text mst
WHERE  mst.msp_code = 'RLE-10364'
/

DELETE qms_message_properties msp
WHERE  msp.code = 'RLE-10364'
/


INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND
                                   , SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME )
     VALUES ( 'RLE-10364'
            , '�f het huisnummer is ingevuld �f het postbusnummer.'
            , 'E', 'N', 'N', 'N', NULL); 

INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE )
     VALUES ( 'DUT'
            , '�f het huisnummer is ingevuld �f het postbusnummer.'
            , NULL, 'RLE-10364'); 
