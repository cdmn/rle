prompt Uitdelen RLE grants aan APP_KBD_PMT

grant select on RLE_V_WERKGEVERS_XL_PORTAL to app_kbd_pmt;
grant select on RLE_V_ADM_KANTOREN to app_kbd_pmt;
grant select on comp_rle.RLE_V_WERKGEVERS_PORTAL_LOOKUP to app_kbd_pmt;
grant select on comp_rle.RLE_V_PERSONEN_XL_PORTAL to app_kbd_pmt;
grant select on comp_rle.rle_v_relaties to app_kbd_pmt;
grant select on comp_rle.rle_relaties to app_kbd_pmt;
grant select on RLE_V_RELATIEKOPPELINGEN to app_kbd_pmt;
grant select on RLE_V_PERSONEN to app_kbd_pmt;
grant select on rle_v_administratiekantoren to app_kbd_pmt;
