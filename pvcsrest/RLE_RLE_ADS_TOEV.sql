delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RLE_ADS_TOEV';
delete from stm_moduleparameters
    where mde_module = 'RLE_RLE_ADS_TOEV';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RLE_ADS_TOEV';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RLE_ADS_TOEV';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RLE_ADS_TOEV';
DELETE FROM stm_modules
    WHERE module = 'RLE_RLE_ADS_TOEV';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RLE_ADS_TOEV',1,'RLE',1
    ,'Toevoegen relatie adres','P'
    ,'RLE_RLE_ADS_TOEV',''
    ,'A',''
    ,16);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_SRT_ADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_BEGINDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_CODLAND','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_POSTCOD','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N7','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_HUISNUM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_TOEVNUM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_WOONPL','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_STRAAT','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_LOCATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_IND_WOONWAGEN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_IND_WOONBOOT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D11','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_EINDDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N13','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_NUMADRES','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A16','RLE_RLE_ADS_TOEV',1,'J'
    ,'P_PROVINCIE','INP');
