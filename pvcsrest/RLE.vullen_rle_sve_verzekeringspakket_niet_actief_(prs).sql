merge into rle_standaard_voorwaarden sve1 using ( select 'VERZEKERINGSPAKKET NIET ACTIEF (PRS)' code ,q'#13.2) Personen zonder verzekeringspakketten#' naam ,q'#Personen zonder op gegeven peildatum een geldige Grondslag voor een geldige Polis AO die gebaseerd is op het VERZEKERINGSPAKKET met het opgegeven Pakketnummer.
Parameters:
1	Datum
2	Pakketnummer (REG VERZEKERINGSPAKKET.Vezekeringspakketnummer)#' beschrijving ,q'#select 1
from   rle_relatie_externe_codes  rec
where  rec.rle_numrelatie        = :NUMRELATIE
and    rec.dwe_wrddom_extsys     = 'PRS'
and    rec.rol_codrol            = 'PR'
and not exists (select 1
                from   aos_polissen_ao            pao
                ,      aos_verzekerden_ao         vao
                ,      aos_verzekerd_perioden_ao  veo
                where  vao.id                    = vao.id
                and    vao.pao_id                = pao.id
                and    pao.vzp_nummer            in ( :VAR1 )
                and    pao.fiatteur_polis        is not null
                and    pao.fiatteur_annulering   is null
                and    vao.prs_persoonsnummer    = rec.extern_relatie
                and    veo.vao_id                = vao.id
                and    veo.veo_type              = 'GAO'
                and    :VAR2                     between veo.ingangsdatum and nvl(veo.einddatum,:VAR2)
                and    veo.fiatmoment            is not null
                and    veo.vervalmoment          is null
               )#' sqltext ,q'#Verzekeringspakket (b.v. 40,41,42 )#' var1_naam ,'R' var1_datatype ,q'#Peildatum (DD-MM-YYYY)#' var2_naam ,'D' var2_datatype ,q'##' var3_naam ,'' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$XMK' creatie_door ,to_date('01-04-2010 10:33:15','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('20-04-2010 13:56:53','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
