REM ****************************************************************************
REM Naam      : RLE_RGG_GBA001.sql
REM Doel      : Vervaardigen van een ASCII bestand (csv) met gegevens van overleden deelnemers
REM           : tussen twee peildatums begin en eind. 
REM           
REM           : Input variabelen
REM Parameters: %1  script naam   ( wordt door JCS bepaald )
REM             %2  script id     ( wordt door JCS bepaald )
REM             %3  Peildatum_begin
REM             %4  Peildatum_einde
REM             %5  RGG_CODE
REM
REM Wijzigingshistorie:
REM Versie Naam                   Datum      Wijziging
REM ------ ---------------------- ---------- ---------------------------------------
REM 1.0    R.A. Pargas            27-09-2011 Creatie / 
REM                                           Null waardes worden als 'null' doorgegeven
REM                                           door de jcs bouwsteen.                                            
REM ****************************************************************************

REM whenever sqlerror exit failure
REM whenever oserror  exit failure

SET linesize 10000
SET define '&'
SET define on
SET pagesize 0
SET heading off
SET feedback off
SET pause off
SET verify off
SET trimspool on
SET termout off

variable var_context_naam varchar2(10)

begin
  :var_context_naam := stm_util.t_context;
end;
/


var b_script_naam             varchar2(20);
var b_script_id               varchar2(20);
var b_peildatum_begin         varchar2(10);
var b_peildatum_einde         varchar2(10);
var b_rgg_code                varchar2(10);


exec :b_script_naam         := '&1'
exec :b_script_id           := '&2'
exec :b_peildatum_begin     := '&3'
exec :b_peildatum_einde     := '&4'
exec :b_rgg_code            := '&5'


SET termout off

REM - Ophalen instance naam

define l_instance = ''

column l1 noprint new_value l_instance

SELECT   name   || '_'   L1
FROM     V$DATABASE
WHERE    name != 'PPVD'
/

REM - Bepalen bestandsnaam
REM - De bestandsnaam begint altijd met de scriptnaam, %1
REM   en eindigt (voor de extensie) met het job_id, %2

define l_file_naam = ''

column l2 new_value l_file_naam

prompt voor_de_select_filenaam

SELECT                  '&l_instance'       -- INSTANCE NAAM
                     || '&1'                -- SCRIPTNAAM
         || '_'      || :b_rgg_code
         || '_'      || :b_peildatum_begin
         || '_'      || :b_peildatum_einde
         || '_'      || '&2'                -- JOB_ID
         || '.CSV'
                                            L2
FROM       DUAL
/

prompt na_de_select_filenaam

alter session set nls_numeric_characters=',.';
alter session set optimizer_index_cost_adj=100;

-- Op unix
SPOOL $TMP/RLE/outbox/\&l_file_naam
-- Op windows

--SPOOL &l_file_naam


REM - Aanmaken van de rapportage
-- header van csv bestand.
select
'Persoonsnummer'            ||';'||
'Naam_Werknemer'            ||';'||
'GeboorteDatum'             ||';'||
'DatumOverlijden'           ||';'||
'DatumOverlBevestigd_GBA'   ||';'||
'DatumBegin_AVG'            ||';'||
'DatumEinde_AVG'            ||';'||
'MutatieDatum_AVG'          ||';'||
'Aant_dg_tussen_ovl_en_afm' ||';'||
'Werkgever_Nummer'          ||';'||
'Naam_Werkgever'            ||';'||
'Tel_Nummer'                ||';'||
'Naam_Contact_Persoon'      ||';'||
'Tel_Contact_Persoon'       ||';'||
'Email_Edres'               ||';'||
'Fax_Nummer'                ||';'||
'Naam_Accountmanager'       ||';'||
'Rgg_Code'
from dual
/
-- data van csv bestand.
with ovl as
 (select  /*+ full (avg) full(rts) */
           avg.id
         , rts.numrelatie relatienummer_prs
         ,  wot.rgg_code rgg_code
         , rts.regdat_overlbevest dat_overlbevestigd_gba
         , avg.rle_numrelatie_wg relatienummer_wgr
         , rts.datoverlijden DatumOverlijden
         , avg.dat_begin DatumBegin_AVG
         , avg.dat_einde DatumEinde_AVG
         , avg.dat_mutatie MutatieDatum_AVG
         , wot.rgg_code RGG_CODE2
        from   avg_arbeidsverhoudingen avg
                 , rle_relaties rts
                 , ovt_wgr_overeenkomsten wot
        where  rts.numrelatie = avg.rle_numrelatie_pr
        and    wot.rle_numrelatie = avg.rle_numrelatie_wg
        and    rts.datoverlijden between avg.dat_begin and nvl(avg.dat_einde,rts.datoverlijden)
        --and    avg.dat_einde is null
        and    rts.datoverlijden is not null
        and    rts.datoverlijden between to_date ( :b_peildatum_begin, 'dd_mm-yyyy' ) and to_date ( :b_peildatum_einde, 'dd_mm-yyyy' )
        and  (:b_rgg_code = wot.rgg_code or :b_rgg_code = 'null')
        and trunc(sysdate) between wot.datum_begin and nvl(wot.datum_einde, trunc(sysdate)) 
 )  select prs.persoonsnummer                                              || ';'||
         prs.volledige_naam3                                               ||';' ||                                      
         to_char(prs.geboortedatum, 'DD-MM-YYYY')                          ||';' ||                                      
         to_char(prs.datum_overlijden, 'DD-MM-YYYY')                       ||';' ||                                      
         to_char(ovl.Dat_overlbevestigd_gba, 'DD-MM-YYYY')                 ||';' ||                                   
         to_char(ovl.DatumBegin_AVG, 'DD-MM-YYYY')                         ||';' ||
         to_char(ovl.DatumEinde_AVG, 'DD-MM-YYYY')                         ||';' ||
         to_char(ovl.MutatieDatum_AVG, 'DD-MM-YYYY')                       ||';' ||
         case when ovl.DatumEinde_AVG is not null then
           (trunc(ovl.MutatieDatum_AVG) - trunc(ovl.Dat_overlbevestigd_gba)) ||';'
            else ';' 
         end ||  
         wgr.werkgevernr                                                   ||';' ||                                        
         wgr.naam_relatie                                                  ||';' ||                                        
         wgr.tel_nummer                                                    ||';' ||
         wgr.sz_naam_contactpersoon                                        ||';' ||                                   
         wgr.sz_telnr_contactpersoon                                       ||';' ||                                 
         wgr.email_adres                                                   ||';' ||
         wgr.fax_nummer                                                    ||';' ||       
         wgr.actmgr_naam                                                   ||';' ||                                              
         ovl.rgg_code2                                                     ||';'
    from ovl
      ,  rle_mv_personen prs
      ,  rle_v_werkgevers_xl wgr
    where ovl.relatienummer_prs = prs.relatienummer
    and     ovl.relatienummer_wgr = wgr.relatienr
    order by prs.datum_overlijden asc
/

prompt
prompt

REM - Parameters

SELECT 'Instance naam                : ' || replace('&l_instance','_')
FROM DUAL;
SELECT 'Script naam                  : ' || '&1'
FROM DUAL;
SELECT 'Script id                    : ' || '&2'
FROM DUAL;
SELECT 'Regeling                     : ' || nvl(:b_rgg_code, 'Alle_Regelingen')
FROM DUAL;
SELECT 'Peildatum begin              : ' || :b_peildatum_begin  
FROM DUAL;
SELECT 'Peildatum einde              : ' || :b_peildatum_einde
FROM DUAL;

SPOOL OFF

REM - job_statussen bijwerken

INSERT INTO stm_job_statussen
(   script_naam
,   script_id
,   programma_naam
,   volgnummer
,   uitvoer_soort
,   uitvoer_waarde
,   datum_tijd_mutatie
,   gebruiker
,   context_naam
)
VALUES
(   UPPER('&1')
,   &2
,   'RLE_RGG_GBA001'
,   1
,   'S'
,   '0'
,   SYSDATE
,   USER
,   :var_context_naam
);

COMMIT;