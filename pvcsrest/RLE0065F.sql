delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0065F';
delete from stm_moduleparameters
    where mde_module = 'RLE0065F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0065F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0065F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0065F';
DELETE FROM stm_modules
    WHERE module = 'RLE0065F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0065F',1,'RLE',1
    ,'Ondh relaties instelling','S'
    ,'RLE0065F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0065F',1,'N'
    ,'Relatienr','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0065F',1,'N'
    ,'Rol','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('DAT_PEIL','RLE0065F',1,'N'
    ,'Peildatum','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SRT_ADRES','RLE0065F',1,'N'
    ,'Adressoort','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'DAT_PEIL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD12');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'NAAMGEBR',
        ':RLE.CODE_AANDUIDING_NAAMGEBRUIK');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'REF_GWT',
        ':RLE.DWE_WRDDOM_GEWATITEL');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'REF_REV',
        ':RLE.DWE_WRDDOM_RVORM');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'REF_SAD',
        ':QMS$CTRL.CGNBT_CHAR_FIELD13');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'CODE_ROL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD1');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0065F',1,'SRT_ADRES',
        ':QMS$CTRL.CGNBT_CHAR_FIELD13');
