delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0350F';
delete from stm_moduleparameters
    where mde_module = 'RLE0350F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0350F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0350F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0350F';
DELETE FROM stm_modules
    WHERE module = 'RLE0350F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0350F',1,'RLE',1
    ,'Venster om een rol voor verendbestemmingen te selecteren','S'
    ,'RLE0350F',''
    ,'A',''
    ,1);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE0350F',1,'N'
    ,'rol','OUT');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0350F',1,'CODE_ROL',
        ':GLOBAL.CG$CODE_ROL');
