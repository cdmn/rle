INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00001', 'Rol is geen groepsrol.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00002', 'Er zijn onjuiste waarden mee gegeven bij de aanroep van dit scherm.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00003', 'Selecteren postcode alleen mogelijk voor Nederland'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00004', 'Landcode onbekend'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00005', 'Postcode onbekend'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00006', 'Sofinummer <p1> bestaat al'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00009', 'Specificeer zoveel mogelijk de zoekvraag (vul in ieder geval ��n zoekveld in).'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00010', 'Rechtsvorm moet bij een werkgever gevuld zijn'
            , 'I', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00042', 'Persoon kan niet verwijderd worden wegens nog bestaande gerelateerde gegevens'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00365', 'Geen relatie bij dit rekeningcourantnummer.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00367', 'Naw-gegevens volledig invullen a.u.b.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00368', 'Persoonsgevens niet toegestaan , deze worden geleegd !!!'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00369', 'Er zijn referenties bij deze relatie, verwijderen niet toegestaan !!!'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00370', 'Geen kenmerk gevonden'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00371', 'Veld mag niet leeggemaakt worden'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00372', 'Alleen  J en N toegestaan'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00373', 'Datum heeft een onjuist formaat'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00374', 'Let op;  Relatie vervalt per heden !!'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00375', 'Bij het zoeken op huisnummer is ook (een gedeelte van) postcode vullen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00376', 'Bij het zoeken op naam tenminste de eerste 2 letters ingeven'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00377', 'Relatie moet tenminste een postadres hebben'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00378', 'Er is geen adres gevuld bij de relatie'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00379', 'Niet bestaande combinatie postcode <p1> en huisnr <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00380', 'Parameter <p1> onbekend'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00381', 'Rowid kan niet toegevoegd worden'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00382', 'Array kan niet leeggemaakt worden'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00383', 'Rowid kan niet gelezen worden'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00384', 'Geen lock te zetten op <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00385', 'Begindatum niet geldig'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00386', 'Begindatum niet geldig in tabel <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00387', 'Begindatum niet geldig t.o.v. relatie in <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00388', 'Rol  bij instelling is <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00389', 'Combinatie van relatie en rol bestaat niet'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00390', 'Geen fiat voor wijziging'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00391', 'Huisnummer behoort niet bij de postcode'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00392', 'Relatie <p1> met <p2>  bestaat al'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00393', '<p1>, <p2>, <p3>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00395', 'RL-000002'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00396', 'Externe code  verwijst naar meer dan een relatienummer'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00398', 'Relatie <p1> heeft reeds het kenmerk <p2>l'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00399', 'Selecteer een relatie'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00400', 'Schoningsdatum voor einddatum  voor <p1> in <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00401', 'Invullen <p1> verplicht'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00407', 'Geboortedatum na begindatum voor <p1> in <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00412', 'Max. aant. koppelingen bereikt voor <p1> van <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00414', 'Geen inverse koppeling mogelijk voor <p1> en <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00415', 'Fout in opbouwen relatiekop bij <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00416', 'Rol <p1> is geen groepsrol'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00417', 'Indicatie persoon/instelling is niet wijzigbaar voor <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00419', 'Indicatie wijzigbaar = nee voor <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00420', 'Foutieve schoningsdatum in relatierol voor <p1> in <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00427', 'Niet verwijderen, relatie <p1>heeft nog adressen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00428', 'Rol <p1> is aanwezig in soort koppeling'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00430', 'Soort koppeling geen toegestane rol voor <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00432', 'Datum <p1> niet in toekomst'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00435', 'Straat heeft een andere woonplaats'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00436', 'Woonplaats <p1> bestaat niet'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00437', 'Land <p1> is niet gelijk aan het land bij de woonplaats <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00438', 'Relatie <p1> met rol <p2> heeft niet het betreffende adres'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00439', 'Adres met postcode <p1> en huisnummer <p2> <p3> bestaat al'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00440', 'Geen land gevonden voor <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00441', '<p1> verplicht'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00445', 'Fout in procedure'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00447', 'Rol bij persoon is <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00448', 'Rekening <p1> voldoet niet aan de 11-proef'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00449', 'Relatie <p1> heeft minimaal een belang'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00450', 'Relatie <p1> heeft de  rol tussenpersoon'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00451', 'Beroep is niet bekend'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00452', 'Het aantal voorletters <p1> kan maximaal 6 lang zijn'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00453', 'Aanschrijfnaam moet korter zijn dan 38 posities'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00454', 'Geen scheidingsteken aanwezig'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00455', 'Scheidingsteken maximaal op positie 19'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00456', 'Lengte tot scheidingsteken moet korter zijn dan 19 posities'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00457', 'Lengte vanaf scheidingsteken moet korter zijn dan 19 posities'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00458', 'Minimaal achternaam en een adres invullen'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00459', 'Deze rol <p1> is in de koppeling <p2> niet toegestaan'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00460', 'Postcode/huisnummer is niet in overeenstemming met woonplaats/straat'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00461', 'Soort koppeling heeft reeds gerelateerde koppelingen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00462', 'Rekening <p1> is geen gironummer'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00463', 'Bankrekeningnummer <p1> is geen 9 posities'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00465', 'Geen dienstverleners opvoeren, enkel selecteren'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00500', 'Relatie met externe code <p1> uit <p2> komt niet voor bij rol <p3>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00501', 'Parameter <p1> moet gevuld zijn'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00502', 'Systeemcomponent <p1> onbekend bij rol <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00503', 'Er zijn al postcodes aanwezig'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00504', 'Er zijn twee mutaties geconstateerd'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00505', 'Er zijn teveel mutaties geconstateerd (<p1>)'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00506', 'Relatie <p1> kent deze code niet'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00507', 'Intern of extern relatienummer moet gevuld zijn'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00509', 'Actie <p1> is binnen recordtype <p2> onbekend'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00510', 'Recordtype <p1> is onbekend'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00511', 'Wijzigen van indicatie instelling niet toegestaan'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00512', 'Bij een instelling geen persoonsgegevens invullen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00513', 'Bij een persoon geen instellingsgegevens invullen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00514', 'Huisnummer is een numeriek veld'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00515', 'Dubbele overeenkomst'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00516', 'Geen of te weinig  geldige zoekgegevens ingevoerd !'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00517', 'Eerst een woonplaats invullen'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00518', 'Straatnaam is niet gevuld'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00519', 'Telefoonnummer is fout: streepje op de verkeerde plaats'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00520', 'Telefoonnummer bevat alfanumerieke tekens'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00521', 'Telefoonnummer heeft niet de juiste lengte of streepje ontbreekt'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00522', 'Faxnummer is fout: streepje op de verkeerde plaats'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00523', 'Faxnummer bevat alfanumerieke tekens'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00524', 'Faxnummer is niet de juiste lengte'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00525', 'Toevoeging van huisnummer mag niet langer dan 6 tekens zijn'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00526', 'Bankrekening mag niet met een 0 of 7 beginnen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00527', 'Te wijzigen adres <p1> heeft nog relatie adressen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00528', 'Er kan slechts 1 keer een dubbele worden gekozen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00529', 'Relatie vanavond naar Wang, weet u het zeker'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00530', 'Er staan nog wijzigingen open. Maak eerst u wijziging af'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00531', 'Relatienummer niet bepaald bij referentie <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00532', 'Het RC-nummer komt al voor bij een relatie'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00533', 'Rol heeft nog rollen als groepsrol'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00534', 'Er is al een <p1> belang'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00535', 'Referentie is niet bij deze relatie aanwezig'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00536', 'Verwijderen niet toegestaan, relatie wordt nog gebruikt in ander systeem'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00537', 'Kenmerk bestaat (nog) niet in GG'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00538', 'Onbekend gegeven <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00539', 'Relatie vanavond naar Coda, weet u het zeker'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00540', 'Verkeerde combinatie van zoek gegevens'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00541', 'Selectie uitbreiden, zoekvraag levert te veel records op'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00542', 'Zoekvraag levert geen records op'
            , 'I', 'N', 'Y', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00543', 'Loonbelastingnummer moet bestaan uit een nummer gevolgd door een L en 2 cijfers'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00544', 'Fout bij overhangen externe code van <p1> naar <p2>: <p3>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00545', 'Procent-teken gevonden op eerste drie posities, deze zoekopdracht kan lang gaan duren!'
            , 'I', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00546', 'In het huisnummer mag geen wildcard voorkomen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00550', 'Referentie (= Sofinummer of A-nummer) reeds gekoppeld aan een andere persoon.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00551', 'Bij de persoon eerst een feitelijk adres opgeven.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00552', 'Bij een persoon is de geboortedatum verplicht'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00553', 'Bij een persoon is een Feitelijk- of Domicilieadres verplicht, deze eerst opvoeren.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00554', 'Naamgebruik (N, V of P) vereist een Naam-Partner.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00555', 'Fout bij het toevoegen van het adres.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00556', 'Bij deze VZR-mutatieregel is geen persoon gevonden'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00557', 'Bij deze VZR-opvoerregel kan geen match gemaakt worden met een reeds bestaande persoon'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00600', 'Postbus of antwoordnummer niet toegestaan als adres'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00601', 'Communicatie alleen in combinatie met een relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00602', 'Rekening alleen in combinatie met een relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00603', 'Externe Referenties alleen in combinatie met een relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00604', 'Adres alleen in combinatie met een relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00605', 'Een domicilie-adres mag niet worden toegevoegd of gewijzigd'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00606', 'In dit scherm mag geen SOFInummer opgevoerd worden.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00607', 'Weet U zeker dat U deze persoon wilt verwijderen?    (kan niet terug gedraaid worden)'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00608', 'G-rekening mag alleen opgevoerd worden voor premierekeningen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00609', 'Bij opvoer van een G-rekening dient er voor een lopende Bankrekening voor type UITK te zijn.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00610', 'Een adres (bij een relatie) mag niet worden verwijderd.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00611', 'Voor gebruik functie rle_ras_chk_periode is nr_relatie, srt_adres en begindatum verplicht.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00612', 'Fout, bij gebruik functie rle_ras_chk_periode, p_begindatum mag niet groter zijn aan p_einddatum.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00613', '<p1> verwerkt.'
            , 'I', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00614', '<p1> niet verwerkt.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00615', 'Geen externe code gevonden bij numrelatie <p1>, systeemcomponent <p2>, code rol <p2> op <p4>.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-00616', 'Fout in <p1>: <p2>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10000', 'Dummy melding, zodat de 1e 10000 messages voor eigen messages zijn'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10001', 'Waarde voor Postcode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10002', 'Waarde voor Woonplaats moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10003', 'Waarde voor Naam moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10004', 'Waarde voor Codkenmerk moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10005', 'Waarde voor Postcode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10006', 'Waarde voor Groepsrol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10007', 'Waarde voor Toegestane Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10008', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10009', 'Waarde voor Postcode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10010', 'Waarde voor Land moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10011', 'Waarde voor Land moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10012', 'Waarde voor Land moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10013', 'Waarde voor Controletabel moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10014', 'Waarde voor Straat Verplicht moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10015', 'Waarde voor Huisnummer Verplicht moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10016', 'Waarde voor Woonplaats Verplicht moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10017', 'Waarde voor Postcode Verplicht moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10018', 'Waarde voor Layout Adres moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10019', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10020', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10021', 'Waarde voor Kenmerk moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10022', 'Waarde voor Eigen Periode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10023', 'Waarde voor Domein moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10024', 'Waarde voor Indvervallen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10025', 'Waarde voor Indmulti moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10026', 'Waarde voor Rol_Codrol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10027', 'Waarde voor Inddomein moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10028', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10029', 'Waarde voor Mkk_Codkenmerk moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10030', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10031', 'Waarde voor Organisatiecode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10032', 'Waarde voor Voorletters moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10033', 'Waarde voor Voorvoegsel moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10034', 'Waarde voor Instelling moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10035', 'Waarde voor Naam Gewijzigd moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10036', 'Waarde voor Titel moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10037', 'Waarde voor Burgerlijke Staat moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10038', 'Waarde voor Rechtsvorm moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10039', 'Waarde voor Tussentitel moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10040', 'Waarde voor Achtervoegsel moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10041', 'Waarde voor Achtertitel moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10042', 'Waarde voor Nationaliteit moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10043', 'Waarde voor 2e Nationaliteit moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10044', 'Waarde voor Zoeknaam moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10045', 'Waarde voor Geslacht moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10046', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10047', 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10048', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10049', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10050', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10051', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10052', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10053', 'Waarde voor Representatiecode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10054', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10055', 'Waarde voor Wijzigbaar moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10056', 'Waarde voor Schonen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10057', 'Waarde voor Groepsrol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10058', 'Waarde voor P/I/B moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10059', 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10060', 'Waarde voor Inverse Soort Koppeling moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10061', 'Waarde voor Omgekeerd Toegestaan moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10062', 'Waarde voor Inverse Aanmaken moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10063', 'Waarde voor Eerste Groepsrol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10064', 'Waarde voor Tweede Groepsrol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10065', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10066', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10067', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10068', 'Waarde voor Gefiatteerd moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10069', 'Waarde voor Land moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10070', 'Waarde voor Soort Rekening moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10071', 'Waarde voor Indinhown moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10072', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10073', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10074', 'Waarde voor Indinhown moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10075', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10076', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10077', 'Waarde voor Indinhown moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10078', 'Waarde voor Soort Communicatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10079', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10080', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10081', 'Waarde voor Postcode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10082', 'Waarde voor Ind. Postcode Handmatig moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10083', 'Waarde voor Codprovincie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10084', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10085', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10086', 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10087', 'Waarde voor Organisatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10088', 'Waarde voor Code Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10089', 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10090', 'Waarde voor Mutatiecode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10091', 'Waarde voor Ind_Verwerkt moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10092', 'Waarde voor Ind_Verwerkt_Coda moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10093', 'Waarde voor Postcode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10094', 'Waarde voor Codprov moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10095', 'Waarde voor Ind_Mut_Postcode_Dl1 moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10096', 'Waarde voor Ind_Mut_Reekscod moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10097', 'Waarde voor Ind_Mut_Numscheidingvan moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10098', 'Waarde voor Ind_Mut_Numscheidingtm moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10099', 'Waarde voor Ind_Mut_Woonplaats_Nen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10100', 'Waarde voor Ind_Mut_Straatnaam_Ptt moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10101', 'Waarde voor Ind_Mut_Straatnaam_Off moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10102', 'Waarde voor Ind_Mut_Gemeentenaam moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10103', 'Waarde voor Ind_Mut_Codprov moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10104', 'Waarde voor Postcode_Oud moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10105', 'Waarde voor Reekscod_Oud moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10106', 'Waarde voor Codprov_Oud moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10107', 'Waarde voor Postcode_Nw moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10108', 'Waarde voor Codprov_Nw moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10109', 'Waarde voor Ind_Mut_Postcode_Dl2 moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10110', 'Waarde voor Ind_Mut_Woonplaats_Ptt moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10111', 'Waarde voor Ind_Mut_Straatnaam_Nen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10112', 'Waarde voor Ind_Mut_Codcebuco moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10113', 'Waarde voor Reekscod_Nw moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10114', 'Waarde voor Ind_Gekozen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10115', 'Waarde voor Codorgansatie moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10116', 'Waarde voor Tijdelijk moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10117', 'Waarde voor Codreden moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10118', 'Waarde voor Code moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10119', 'Waarde voor Positief moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10120', 'Waarde voor Indvervallen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10121', 'Waarde voor Ind_Mut_Postcode_Dl1 moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10122', 'Waarde voor Ind_Mut_Postcode_Dl2 moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10123', 'Waarde voor Ind_Mut_Numscheidingvan moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10124', 'Waarde voor Ind_Mut_Numscheidingtm moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10125', 'Waarde voor Ind_Mut_Woonplaats_Nen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10126', 'Waarde voor Ind_Mut_Straatnaam_Ptt moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10127', 'Waarde voor Ind_Mut_Straatnaam_Nen moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10128', 'Waarde voor Ind_Mut_Gemeentenaam moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10129', 'Waarde voor Ind_Mut_Codprov moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10130', 'Waarde voor Ind_Mut_Codcebuco moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10131', 'Waarde voor Reekscod_Oud moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10132', 'Waarde voor Codprov_Oud moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10133', 'Waarde voor Postcode_Nw moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10134', 'Waarde voor Reekscod_Nw moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10135', 'Waarde voor Codprov_Nw moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10136', 'Waarde voor Ind_Mut_Huisnum moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10137', 'Waarde voor Ind_Mut_Huisnumtoev moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10138', 'Waarde voor Ind_Mut_Perceelnum moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10139', 'Waarde voor Ind_Mut_Reekscod moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10140', 'Waarde voor Ind_Mut_Woonplaats_Ptt moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10141', 'Waarde voor Ind_Mut_Straatnaam_Off moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10142', 'Waarde voor Postcode_Oud moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10143', 'Waarde voor Indverwerkt moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10144', 'Waarde voor Codgegeven moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10145', 'Waarde voor Rgt_Codgegeven moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10146', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10147', 'Waarde voor Code Domein moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10148', 'Waarde voor Code Domein moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10149', 'Relatie met deze Relatienummer bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10150', 'Overlijdensdatum moet na de geboortedatum vallen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10151', 'Land met deze Nationaliteit bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10152', 'Land met deze 2e Nationaliteit bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10153', 'Vrle_Ind met deze Relatienummer bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10154', 'Relatie met deze Relatie koppeling bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10155', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10156', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10157', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10158', 'Soort Koppeling met deze Soort koppeling bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10159', 'Relatie Koppeling met deze Relatie, Rol, Soort koppeling, Relatie koppeling, Rol, Geldig van bestaat al.
'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10160', 'Relatie mag geen koppeling hebben met zichzelf'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10161', 'Einddatum moet na begindatum liggen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10162', 'Notitie met deze Kladblok bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10163', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10164', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10165', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10166', 'Relatierol met deze Relatie, Rol bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10167', 'Rol met deze Rol bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10168', 'Rol met deze Pr bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10169', 'Rol met deze Omschrijving bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10170', 'Vrol_Plus met deze P/I/B bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10171', 'Soort Koppeling met deze Soort koppeling bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10172', 'Rol met deze Eerste groepsrol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10173', 'Rol met deze Tweede groepsrol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10174', 'Inverse soort koppeling verplicht bij indicatie inverse aanmaken'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10175', 'Soort Koppeling met deze Inverse soort koppeling bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10176', 'Soort Koppeling met deze Omschrijving bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10177', 'Rol met deze Groepsrol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10178', 'Rol met deze Toegestane rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10179', 'Toegestane Rol met deze Groepsrol, Toegestane rol bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10180', 'Land met deze Land bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10181', 'Woonplaats met deze Woonplaats bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10182', 'Straat met deze Straat bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10183', 'Adres met deze Adres bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10184', 'Adres met deze Land, Straat, Woonplaats, Huisnummer, Toevoeging bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10185', 'Land met deze Land bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10186', 'Woonplaats met deze Woonplaats bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10187', 'Woonplaats met deze Land, Woonplaats bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10188', 'Woonplaats met deze Woonplaats bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10189', 'Straat met deze Straat bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10190', 'Straat met deze Woonplaats, Straat bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10191', 'Land met deze Land bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10192', 'Land met deze Naam bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10193', 'Land met deze Naam ingezetene bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10194', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10195', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10196', 'Relatie Externe Code met deze Uniek id van een relatie externe code bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10197', 'Einddatum moet na begindatum liggen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10198', 'Mogelijk Kenmerk met deze Kenmerk, Id bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10199', 'Rol met deze Rol_Codrol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10200', 'Mogelijk Kenmerk met deze Rol_Codrol, Kenmerk bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10201', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10202', 'Mogelijk Kenmerk met deze Mkk_Codkenmerk, Mkk_Id bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10203', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10204', 'Relatie Extra Kenmerk met deze Relatie, Mkk_Id, Mkk_Codkenmerk, Geldig van, Waarde kenmerk bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10205', 'Einddatum moet na begindatum liggen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10206', 'Relatierol met deze Relatie, Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10207', 'Gebruik Relatie met deze Relatie, Rol, Extern systeem bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10208', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10209', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10210', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10211', 'Land met deze Land bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10212', 'Financieel Nummer met deze Relatie, Rekening bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10213', 'Einddatum moet na begindatum vallen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10214', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10215', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10216', 'Adres met deze Adres bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10217', 'Relatie Adres met deze Uniek id van een relatie externe code bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10218', 'Vads_Stt_Vw met deze Adres bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10219', 'Relatie Adres met deze Relatie, Rol, Adres, Soort adres, Geldig van bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10220', 'Einddatum moet na begindatum liggen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10221', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10222', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10223', 'Adres met deze Nummer adres bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10224', 'Communicatie Nummer met deze Nr bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10225', 'Vads_Stt_Vw met deze Nummer adres bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10226', 'Communicatie Nummer met deze Relatie, Soort communicatie, Communicatienummer, Geldig van bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10227', 'Einddatum moet na de begindatum vallen'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10228', 'Vras_Ads_Vw met deze Relatie, Nummer adres bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10229', 'Soort Adres Per Rol met deze Rol, Volgnummer bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10230', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10231', 'Soort Adres Per Rol met deze Rol, Soort adres bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10232', 'Ned Postcode met deze Postcode, Even- onevencode, Scheidingsnummer van, Scheidingsnummer tot/met bestaat
 al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10233', 'Mutatieactie met deze Id bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10234', 'Mutatieactie met deze Mutatiecode, Extern systeem, Organisatie, Rol, Soort koppeling bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10235', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10236', 'Soort Koppeling met deze Soort koppeling bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10237', 'RLE_VAST_ADS_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10238', 'RLE_VAST_STT_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10239', 'RLE_VAST_WPS_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10240', 'RLE_VAST_LND_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10241', 'RLE_VAST_PK'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10242', 'Pcod_Batchrun met deze Pbn_Id bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10243', 'Rela Dubbel met deze Relatie, Mogelijke dubbele relatie bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10244', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10245', 'Relatie met deze Mogelijke dubbele relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10246', 'RLE_VRAS_RAS_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10247', 'RLE_VRAS_ADS_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10248', 'RLE_VRAS_STT_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10249', 'RLE_VRAS_WPS_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10250', 'RLE_VRAS_CNR_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10251', 'RLE_VSWS_WPS_FK1'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10252', 'Belang met deze Relatie, Codreden, Volgnummer bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10253', 'Relatie met deze Relatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10254', 'Reden Belang met deze Codreden bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10255', 'Reden Belang met deze Code bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10256', 'Reden Belang met deze Omschrijving bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10257', 'Pcod_Batchrun met deze Pbn_Id bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10258', 'Rle_Synch_Tabel met deze Numrelatie bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10259', 'Rle_Pcod_Batchrun met deze Id bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10260', 'Rela Gegeven Type met deze Codgegeven bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10261', 'Rdub Gegeven met deze Rle_Numrelatie, Rgt_Codgegeven bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10262', 'Rela Gegeven Type met deze Rgt_Codgegeven bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10263', 'Relatie met deze Rle_Numrelatie bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10264', 'Dubbele Faktor met deze Proces, Rubriek bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10265', 'Rle_Transport_Selektie met deze Rle_Numrelatie bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10266', 'Rol Sys Autorisatie met deze Rol, Code domein, Waarde domein bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10267', 'Rol met deze Rol bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10268', 'Gewenste Mailing met deze Relatienummer, Code domein, Waarde domein bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10269', 'Relatie met deze Relatienummer bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10270', 'Mailing met deze Id bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10271', 'Ontvanger Mailing met deze Id, Numrelatie bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10272', 'Mailing met deze Id bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10273', 'Rle_Vrol_Plus met deze Rol_Codrol bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10274', 'Rle_Adres_Chk met deze Numadres bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10275', 'Rle_Vrle_Ind met deze Numrelatie bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10276', 'Tmp_Dump met deze Volgnummer bestaat al.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10277', 'Waarde voor Rol moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10278', 'Waarde voor Mutatiecode moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10279', 'Waarde voor Indicatie verwerkt moet in hoofdletters.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10280', 'De geboortedatum moet formaat DD-MM-YYYY hebben.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10281', 'BRATT0019 De geboortedatum van een kind mag niet voor de geboortedatum van een ouder liggen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10282', 'BRATT0018 De provincie mag alleen gevuld zijn bij een buitenlands adres.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10283', 'BRDEL001 Een adres mag alleen verwijderd worden als het geen statutair of domicilie adres betreft.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10284', 'BRENT0001 Nederlandse adressen moeten een unieke combinatie van postcode, huisnummer en huisnummer toevo
eging hebben. Deze combinatie is reeds in gebruik.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10285', 'BRENT0008 Geen overlap van huwelijk, geregistreerd partnerschap of samenlevingscontract.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10286', 'BRIER0021 Voorkomen dat begindatum relatie koppeling 9 maanden na overlijdensdatum van ��n van de relati
es ligt'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10287', 'BRUPD0001 Een relatie mag niet gewijzigd worden van een persoon naar een instelling.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10288', 'BRATT0016 Een relatie koppeling mag niet in de toekomst beginnen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10289', 'BRATT0017 Een relatie koppeling mag niet in de toekomst eindigen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10290', 'BRIER0009 Postbus of antwoordnummer is alleen toegestaan bij een correspondentieadres.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10291', 'BRIER0020 Een beeindigd relatie koppeling (S of H) mag niet gewijzigd worden als ��n van de relaties geen SOFI-NUMMER en geen A-NUMMER heeft.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10292', 'Kanaalvoorkeur alleen in combinatie met een relatie'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10293', 'Verwantschappen alleen in combinatie met een relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10294', '(BRENT0010) Let op: het kind in deze verwantschap heeft reeds twee (of meer) ouders. Wilt u doorgaan met opslaan?'
            , 'Q', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10295', '(BRIER0023) Let op: de begindatum van de kanaalvoorkeur ligt na de einddatum van de relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10296', 'Contactpersonen alleen in combinatie met een adres.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10297', 'De module kan niet gestart worden met een onbekende rol.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10298', 'Het adres kan niet worden aangemaakt.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10299', 'Let op: deze wijziging impliceert een nieuw adres. Voer een nieuwe ''datum vanaf'' in als u niet wilt dat het oude adres wordt overschreven.'
            , 'I', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10300', '(BRIER0016) Let op: de begindatum van het relatieadres ligt na de einddatum van de relatie.'
            , 'I', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10301', '(BRIER0015) Let op: de begindatum van het financiele nummer ligt na de einddatum van de relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10302', '(BRIER0008) Let op: de begindatum van het communicatienummer ligt na de einddatum van de relatie.'
            , 'E', 'Y', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10303', '(BRATT0015) Het A-nummer voldoet niet aan de GBA 11-proef.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10304', '(BRENT0011) Een <p1> met een <p2> adres hebben.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10305', 'Selecteer eerst een soort relatie alvorens een relatie te kiezen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10306', 'Er kan geen <p1> worden gevonden met het nummer <p2>.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10307', 'De soort relatie ''<p1>'' kan niet gevonden worden.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10308', 'De code ''<p1>'' is niet uniek. Gebruik de lijst met waarden om een soort relatie te selecteren.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10309', 'Gemoedsbezwaardheid alleen in combinatie met een relatie'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10310', 'Op het scherm te starten moet er een <p1> als parameter worden meegegeven.'
            , 'E', 'Y', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10311', 'Dit soort adres is niet toegestaan voor deze relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10312', 'Er mag geen DA adres worden opgevoerd of gewijzigd wanneer de status van de persoon ''In afstemming'', ''Te beoordelen'', ''Overgezet'' of ''Afgemeld'' is.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10313', 'Fout in synchronisatie met GBA: <p1>'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10314', 'Deze code voor postsoort bestaat niet.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10315', 'Er is geen relatiekoppeling tussen de relatie met rol <p1> en de relatie met rol <p2>.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10316', 'Er kan geen relatie worden geselecteerd als er nog geen rol is ingevoerd.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10317', 'Er is wel een rol ingevoerd, maar nog geen relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10318', 'Deze contactpersoon hoort niet bij de relatie.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10319', 'Het sofinummer is verplicht voor personen die 13 jaar of ouder zijn.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10320', 'Er bestaat al een koppeling van dit type met zelfde relaties in deze periode.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10321', 'Een relatiekoppeling van het type ''Huwelijk'', ''Geregistreerd partnerschap'' of ''Samenlevingscontract'' mag niet eindigen na de overlijdensdatum van een van de twee personen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10322', 'Een relatiekoppeling mag niet beginnen voor de geboortedatum van ��n van de twee personen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10323', 'Een relatiekoppeling van het type ''Huwelijk'', ''Geregistreerd partnerschap'' of ''Samenlevingscontract'' mag niet beginnen na de overlijdensdatum van een van de twee personen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10324', 'Het type van een relatiekoppeling mag niet gewijzigd worden.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10325', 'De "omgekeerde" code voor de relatiekoppeling kan niet worden gevonden.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10326', 'De "omgekeerde" relatiekoppeling kan niet worden gevonden.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10327', 'Een persoon mag geen verwantschap met zichzelf hebben.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10328', 'Een relatiekoppeling mag niet eindigen voordat hij begonnen is.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10329', 'Bij het opvoeren van een persoon moet het geslacht ''M''an of ''V''rouw zijn.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10330', 'Het sofinummer met tussen 001000007 en 299999993 liggen.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00001', 'DUT'
            , 'Rol is geen groepsrol.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00002', 'DUT'
            , 'Er zijn onjuiste waarden mee gegeven bij de aanroep van dit scherm.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00003', 'DUT'
            , 'Selecteren postcode alleen mogelijk voor Nederland'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00004', 'DUT'
            , 'Landcode onbekend'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00005', 'DUT'
            , 'Postcode onbekend'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00006', 'DUT'
            , 'Sofinummer <p1> bestaat al'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00009', 'DUT'
            , 'Specificeer zoveel mogelijk de zoekvraag (vul in ieder geval ��n zoekveld in).'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00010', 'DUT'
            , 'Rechtsvorm moet bij een werkgever gevuld zijn'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00042', 'DUT'
            , 'Persoon kan niet verwijderd worden wegens nog bestaande gerelateerde gegevens'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00365', 'DUT'
            , 'Geen relatie bij dit rekeningcourantnummer.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00367', 'DUT'
            , 'Naw-gegevens volledig invullen a.u.b.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00368', 'DUT'
            , 'Persoonsgevens niet toegestaan , deze worden geleegd !!!'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00369', 'DUT'
            , 'Er zijn referenties bij deze relatie, verwijderen niet toegestaan !!!'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00370', 'DUT'
            , 'Geen kenmerk gevonden'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00371', 'DUT'
            , 'Veld mag niet leeggemaakt worden'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00372', 'DUT'
            , 'Alleen  J en N toegestaan'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00373', 'DUT'
            , 'Datum heeft een onjuist formaat'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00374', 'DUT'
            , 'Let op;  Relatie vervalt per heden !!'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00375', 'DUT'
            , 'Bij het zoeken op huisnummer is ook (een gedeelte van) postcode vullen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00376', 'DUT'
            , 'Bij het zoeken op naam tenminste de eerste 2 letters ingeven'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00377', 'DUT'
            , 'Relatie moet tenminste een postadres hebben'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00378', 'DUT'
            , 'Er is geen adres gevuld bij de relatie'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00379', 'DUT'
            , 'Niet bestaande combinatie postcode <p1> en huisnr <p2>'
            , 'Niet bestaande combinatie postcode <p1> en huisnr <p2>');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00380', 'DUT'
            , 'Parameter <p1> onbekend'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00381', 'DUT'
            , 'Rowid kan niet toegevoegd worden'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00382', 'DUT'
            , 'Array kan niet leeggemaakt worden'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00383', 'DUT'
            , 'Rowid kan niet gelezen worden'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00384', 'DUT'
            , 'Geen lock te zetten op <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00385', 'DUT'
            , 'Begindatum niet geldig'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00386', 'DUT'
            , 'Begindatum niet geldig in tabel <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00387', 'DUT'
            , 'Begindatum niet geldig t.o.v. relatie in <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00388', 'DUT'
            , 'Rol  bij instelling is <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00389', 'DUT'
            , 'Combinatie van relatie en rol bestaat niet'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00390', 'DUT'
            , 'Geen fiat voor wijziging'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00391', 'DUT'
            , 'Huisnummer behoort niet bij de postcode'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00392', 'DUT'
            , 'Relatie <p1> met <p2>  bestaat al'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00393', 'DUT'
            , '<p1>, <p2>, <p3>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00395', 'DUT'
            , 'RL-000002'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00396', 'DUT'
            , 'Externe code  verwijst naar meer dan een relatienummer'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00398', 'DUT'
            , 'Relatie <p1> heeft reeds het kenmerk <p2>l'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00399', 'DUT'
            , 'Selecteer een relatie'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00400', 'DUT'
            , 'Schoningsdatum voor einddatum  voor <p1> in <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00401', 'DUT'
            , 'Invullen <p1> verplicht'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00407', 'DUT'
            , 'Geboortedatum na begindatum voor <p1> in <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00412', 'DUT'
            , 'Max. aant. koppelingen bereikt voor <p1> van <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00414', 'DUT'
            , 'Geen inverse koppeling mogelijk voor <p1> en <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00415', 'DUT'
            , 'Fout in opbouwen relatiekop bij <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00416', 'DUT'
            , 'Rol <p1> is geen groepsrol'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00417', 'DUT'
            , 'Indicatie persoon/instelling is niet wijzigbaar voor <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00419', 'DUT'
            , 'Indicatie wijzigbaar = nee voor <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00420', 'DUT'
            , 'Foutieve schoningsdatum in relatierol voor <p1> in <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00427', 'DUT'
            , 'Niet verwijderen, relatie <p1>heeft nog adressen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00428', 'DUT'
            , 'Rol <p1> is aanwezig in soort koppeling'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00430', 'DUT'
            , 'Soort koppeling geen toegestane rol voor <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00432', 'DUT'
            , 'Datum <p1> niet in toekomst'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00435', 'DUT'
            , 'Straat heeft een andere woonplaats'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00436', 'DUT'
            , 'Woonplaats <p1> bestaat niet'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00437', 'DUT'
            , 'Land <p1> is niet gelijk aan het land bij de woonplaats <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00438', 'DUT'
            , 'Relatie <p1> met rol <p2> heeft niet het betreffende adres'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00439', 'DUT'
            , 'Adres met postcode <p1> en huisnummer <p2> <p3> bestaat al'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00440', 'DUT'
            , 'Geen land gevonden voor <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00441', 'DUT'
            , '<p1> verplicht'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00445', 'DUT'
            , 'Fout in procedure'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00447', 'DUT'
            , 'Rol bij persoon is <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00448', 'DUT'
            , 'Rekening <p1> voldoet niet aan de 11-proef'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00449', 'DUT'
            , 'Relatie <p1> heeft minimaal een belang'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00450', 'DUT'
            , 'Relatie <p1> heeft de  rol tussenpersoon'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00451', 'DUT'
            , 'Beroep is niet bekend'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00452', 'DUT'
            , 'Het aantal voorletters <p1> kan maximaal 6 lang zijn'
            , 'Het aantal voorletters <p1> kan maximaal 6 lang zijn');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00453', 'DUT'
            , 'Aanschrijfnaam moet korter zijn dan 38 posities'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00454', 'DUT'
            , 'Geen scheidingsteken aanwezig'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00455', 'DUT'
            , 'Scheidingsteken maximaal op positie 19'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00456', 'DUT'
            , 'Lengte tot scheidingsteken moet korter zijn dan 19 posities'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00457', 'DUT'
            , 'Lengte vanaf scheidingsteken moet korter zijn dan 19 posities'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00458', 'DUT'
            , 'Minimaal achternaam en een adres invullen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00459', 'DUT'
            , 'Deze rol <p1> is in de koppeling <p2> niet toegestaan'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00460', 'DUT'
            , 'Postcode/huisnummer is niet in overeenstemming met woonplaats/straat'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00461', 'DUT'
            , 'Soort koppeling heeft reeds gerelateerde koppelingen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00462', 'DUT'
            , 'Rekening <p1> is geen gironummer'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00463', 'DUT'
            , 'Bankrekeningnummer <p1> is geen 9 posities'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00465', 'DUT'
            , 'Geen dienstverleners opvoeren, enkel selecteren'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00500', 'DUT'
            , 'Relatie met externe code <p1> uit <p2> komt niet voor bij rol <p3>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00501', 'DUT'
            , 'Parameter <p1> moet gevuld zijn'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00502', 'DUT'
            , 'Systeemcomponent <p1> onbekend bij rol <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00503', 'DUT'
            , 'Er zijn al postcodes aanwezig'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00504', 'DUT'
            , 'Er zijn twee mutaties geconstateerd'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00505', 'DUT'
            , 'Er zijn teveel mutaties geconstateerd (<p1>)'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00506', 'DUT'
            , 'Relatie <p1> kent deze code niet'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00507', 'DUT'
            , 'Intern of extern relatienummer moet gevuld zijn'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00509', 'DUT'
            , 'Actie <p1> is binnen recordtype <p2> onbekend'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00510', 'DUT'
            , 'Recordtype <p1> is onbekend'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00511', 'DUT'
            , 'Wijzigen van indicatie instelling niet toegestaan'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00512', 'DUT'
            , 'Bij een instelling geen persoonsgegevens invullen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00513', 'DUT'
            , 'Bij een persoon geen instellingsgegevens invullen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00514', 'DUT'
            , 'Huisnummer is een numeriek veld'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00515', 'DUT'
            , 'Dubbele overeenkomst'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00516', 'DUT'
            , 'Geen of te weinig  geldige zoekgegevens ingevoerd !'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00517', 'DUT'
            , 'Eerst een woonplaats invullen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00518', 'DUT'
            , 'Straatnaam is niet gevuld'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00519', 'DUT'
            , 'Telefoonnummer is fout: streepje op de verkeerde plaats'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00520', 'DUT'
            , 'Telefoonnummer bevat alfanumerieke tekens'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00521', 'DUT'
            , 'Telefoonnummer heeft niet de juiste lengte of streepje ontbreekt'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00522', 'DUT'
            , 'Faxnummer is fout: streepje op de verkeerde plaats'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00523', 'DUT'
            , 'Faxnummer bevat alfanumerieke tekens'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00524', 'DUT'
            , 'Faxnummer is niet de juiste lengte'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00525', 'DUT'
            , 'Toevoeging van huisnummer mag niet langer dan 6 tekens zijn'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00526', 'DUT'
            , 'Bankrekening mag niet met een 0 of 7 beginnen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00527', 'DUT'
            , 'Te wijzigen adres <p1> heeft nog relatie adressen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00528', 'DUT'
            , 'Er kan slechts 1 keer een dubbele worden gekozen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00529', 'DUT'
            , 'Relatie vanavond naar Wang, weet u het zeker'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00530', 'DUT'
            , 'Er staan nog wijzigingen open. Maak eerst u wijziging af'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00531', 'DUT'
            , 'Relatienummer niet bepaald bij referentie <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00532', 'DUT'
            , 'Het RC-nummer komt al voor bij een relatie'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00533', 'DUT'
            , 'Rol heeft nog rollen als groepsrol'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00534', 'DUT'
            , 'Er is al een <p1> belang'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00535', 'DUT'
            , 'Referentie is niet bij deze relatie aanwezig'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00536', 'DUT'
            , 'Verwijderen niet toegestaan, relatie wordt nog gebruikt in ander systeem'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00537', 'DUT'
            , 'Kenmerk bestaat (nog) niet in GG'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00538', 'DUT'
            , 'Onbekend gegeven <p1>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00539', 'DUT'
            , 'Relatie vanavond naar Coda, weet u het zeker'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00540', 'DUT'
            , 'Verkeerde combinatie van zoek gegevens'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00541', 'DUT'
            , 'Selectie uitbreiden, zoekvraag levert te veel records op'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00542', 'DUT'
            , 'Zoekvraag levert geen records op'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00543', 'DUT'
            , 'Loonbelastingnummer moet bestaan uit een nummer gevolgd door een L en 2 cijfers'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00544', 'DUT'
            , 'Fout bij overhangen externe code van <p1> naar <p2>: <p3>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00545', 'DUT'
            , 'Procent-teken gevonden op eerste drie posities, deze zoekopdracht kan lang gaan duren!'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00546', 'DUT'
            , 'In het huisnummer mag geen wildcard voorkomen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00550', 'DUT'
            , 'Referentie (= Sofinummer of A-nummer) reeds gekoppeld aan een andere persoon.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00551', 'DUT'
            , 'Bij de persoon eerst een feitelijk adres opgeven.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00552', 'DUT'
            , 'Bij een persoon is de geboortedatum verplicht'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00553', 'DUT'
            , 'Bij een persoon is een Feitelijk- of Domicilieadres verplicht, deze eerst opvoeren.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00554', 'DUT'
            , 'Naamgebruik (N, V of P) vereist een Naam-Partner.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00555', 'DUT'
            , 'Fout bij het toevoegen van het adres.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00556', 'DUT'
            , 'Bij deze VZR-mutatieregel is geen persoon gevonden'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00557', 'DUT'
            , 'Bij deze VZR-opvoerregel kan geen match gemaakt worden met een reeds bestaande persoon'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00600', 'DUT'
            , 'Postbus of antwoordnummer niet toegestaan als adres'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00601', 'DUT'
            , 'Communicatie alleen in combinatie met een relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00602', 'DUT'
            , 'Rekening alleen in combinatie met een relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00603', 'DUT'
            , 'Externe Referenties alleen in combinatie met een relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00604', 'DUT'
            , 'Adres alleen in combinatie met een relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00605', 'DUT'
            , 'Een domicilie-adres mag niet worden toegevoegd of gewijzigd'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00606', 'DUT'
            , 'In dit scherm mag geen SOFInummer opgevoerd worden.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00607', 'DUT'
            , 'Weet U zeker dat U deze persoon wilt verwijderen?    (kan niet terug gedraaid worden)'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00608', 'DUT'
            , 'G-rekening mag alleen opgevoerd worden voor premierekeningen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00609', 'DUT'
            , 'Bij opvoer van een G-rekening dient er voor een lopende Bankrekening voor type UITK te zijn.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00610', 'DUT'
            , 'Een adres (bij een relatie) mag niet worden verwijderd.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00611', 'DUT'
            , 'Voor gebruik functie rle_ras_chk_periode is nr_relatie, srt_adres en begindatum verplicht.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00612', 'DUT'
            , 'Fout, bij gebruik functie rle_ras_chk_periode, p_begindatum mag niet groter zijn aan p_einddatum.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00613', 'DUT'
            , '<p1> verwerkt.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00614', 'DUT'
            , '<p1> niet verwerkt.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00615', 'DUT'
            , 'Geen externe code gevonden bij numrelatie <p1>, systeemcomponent <p2>, code rol <p2> op <p4>.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-00616', 'DUT'
            , 'Fout in <p1>: <p2>'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10000', 'DUT'
            , 'Dummy melding, zodat de 1e 10000 messages voor eigen messages zijn'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10001', 'DUT'
            , 'Waarde voor Postcode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10002', 'DUT'
            , 'Waarde voor Woonplaats moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10003', 'DUT'
            , 'Waarde voor Naam moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10004', 'DUT'
            , 'Waarde voor Codkenmerk moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10005', 'DUT'
            , 'Waarde voor Postcode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10006', 'DUT'
            , 'Waarde voor Groepsrol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10007', 'DUT'
            , 'Waarde voor Toegestane Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10008', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10009', 'DUT'
            , 'Waarde voor Postcode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10010', 'DUT'
            , 'Waarde voor Land moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10011', 'DUT'
            , 'Waarde voor Land moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10012', 'DUT'
            , 'Waarde voor Land moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10013', 'DUT'
            , 'Waarde voor Controletabel moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10014', 'DUT'
            , 'Waarde voor Straat Verplicht moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10015', 'DUT'
            , 'Waarde voor Huisnummer Verplicht moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10016', 'DUT'
            , 'Waarde voor Woonplaats Verplicht moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10017', 'DUT'
            , 'Waarde voor Postcode Verplicht moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10018', 'DUT'
            , 'Waarde voor Layout Adres moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10019', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10020', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10021', 'DUT'
            , 'Waarde voor Kenmerk moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10022', 'DUT'
            , 'Waarde voor Eigen Periode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10023', 'DUT'
            , 'Waarde voor Domein moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10024', 'DUT'
            , 'Waarde voor Indvervallen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10025', 'DUT'
            , 'Waarde voor Indmulti moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10026', 'DUT'
            , 'Waarde voor Rol_Codrol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10027', 'DUT'
            , 'Waarde voor Inddomein moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10028', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10029', 'DUT'
            , 'Waarde voor Mkk_Codkenmerk moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10030', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10031', 'DUT'
            , 'Waarde voor Organisatiecode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10032', 'DUT'
            , 'Waarde voor Voorletters moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10033', 'DUT'
            , 'Waarde voor Voorvoegsel moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10034', 'DUT'
            , 'Waarde voor Instelling moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10035', 'DUT'
            , 'Waarde voor Naam Gewijzigd moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10036', 'DUT'
            , 'Waarde voor Titel moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10037', 'DUT'
            , 'Waarde voor Burgerlijke Staat moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10038', 'DUT'
            , 'Waarde voor Rechtsvorm moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10039', 'DUT'
            , 'Waarde voor Tussentitel moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10040', 'DUT'
            , 'Waarde voor Achtervoegsel moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10041', 'DUT'
            , 'Waarde voor Achtertitel moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10042', 'DUT'
            , 'Waarde voor Nationaliteit moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10043', 'DUT'
            , 'Waarde voor 2e Nationaliteit moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10044', 'DUT'
            , 'Waarde voor Zoeknaam moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10045', 'DUT'
            , 'Waarde voor Geslacht moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10046', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10047', 'DUT'
            , 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10048', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10049', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10050', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10051', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10052', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10053', 'DUT'
            , 'Waarde voor Representatiecode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10054', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10055', 'DUT'
            , 'Waarde voor Wijzigbaar moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10056', 'DUT'
            , 'Waarde voor Schonen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10057', 'DUT'
            , 'Waarde voor Groepsrol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10058', 'DUT'
            , 'Waarde voor P/I/B moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10059', 'DUT'
            , 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10060', 'DUT'
            , 'Waarde voor Inverse Soort Koppeling moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10061', 'DUT'
            , 'Waarde voor Omgekeerd Toegestaan moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10062', 'DUT'
            , 'Waarde voor Inverse Aanmaken moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10063', 'DUT'
            , 'Waarde voor Eerste Groepsrol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10064', 'DUT'
            , 'Waarde voor Tweede Groepsrol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10065', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10066', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10067', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10068', 'DUT'
            , 'Waarde voor Gefiatteerd moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10069', 'DUT'
            , 'Waarde voor Land moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10070', 'DUT'
            , 'Waarde voor Soort Rekening moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10071', 'DUT'
            , 'Waarde voor Indinhown moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10072', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10073', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10074', 'DUT'
            , 'Waarde voor Indinhown moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10075', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10076', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10077', 'DUT'
            , 'Waarde voor Indinhown moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10078', 'DUT'
            , 'Waarde voor Soort Communicatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10079', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10080', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10081', 'DUT'
            , 'Waarde voor Postcode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10082', 'DUT'
            , 'Waarde voor Ind. Postcode Handmatig moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10083', 'DUT'
            , 'Waarde voor Codprovincie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10084', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10085', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10086', 'DUT'
            , 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10087', 'DUT'
            , 'Waarde voor Organisatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10088', 'DUT'
            , 'Waarde voor Code Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10089', 'DUT'
            , 'Waarde voor Soort Koppeling moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10090', 'DUT'
            , 'Waarde voor Mutatiecode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10091', 'DUT'
            , 'Waarde voor Ind_Verwerkt moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10092', 'DUT'
            , 'Waarde voor Ind_Verwerkt_Coda moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10093', 'DUT'
            , 'Waarde voor Postcode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10094', 'DUT'
            , 'Waarde voor Codprov moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10095', 'DUT'
            , 'Waarde voor Ind_Mut_Postcode_Dl1 moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10096', 'DUT'
            , 'Waarde voor Ind_Mut_Reekscod moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10097', 'DUT'
            , 'Waarde voor Ind_Mut_Numscheidingvan moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10098', 'DUT'
            , 'Waarde voor Ind_Mut_Numscheidingtm moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10099', 'DUT'
            , 'Waarde voor Ind_Mut_Woonplaats_Nen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10100', 'DUT'
            , 'Waarde voor Ind_Mut_Straatnaam_Ptt moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10101', 'DUT'
            , 'Waarde voor Ind_Mut_Straatnaam_Off moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10102', 'DUT'
            , 'Waarde voor Ind_Mut_Gemeentenaam moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10103', 'DUT'
            , 'Waarde voor Ind_Mut_Codprov moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10104', 'DUT'
            , 'Waarde voor Postcode_Oud moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10105', 'DUT'
            , 'Waarde voor Reekscod_Oud moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10106', 'DUT'
            , 'Waarde voor Codprov_Oud moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10107', 'DUT'
            , 'Waarde voor Postcode_Nw moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10108', 'DUT'
            , 'Waarde voor Codprov_Nw moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10109', 'DUT'
            , 'Waarde voor Ind_Mut_Postcode_Dl2 moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10110', 'DUT'
            , 'Waarde voor Ind_Mut_Woonplaats_Ptt moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10111', 'DUT'
            , 'Waarde voor Ind_Mut_Straatnaam_Nen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10112', 'DUT'
            , 'Waarde voor Ind_Mut_Codcebuco moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10113', 'DUT'
            , 'Waarde voor Reekscod_Nw moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10114', 'DUT'
            , 'Waarde voor Ind_Gekozen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10115', 'DUT'
            , 'Waarde voor Codorgansatie moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10116', 'DUT'
            , 'Waarde voor Tijdelijk moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10117', 'DUT'
            , 'Waarde voor Codreden moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10118', 'DUT'
            , 'Waarde voor Code moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10119', 'DUT'
            , 'Waarde voor Positief moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10120', 'DUT'
            , 'Waarde voor Indvervallen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10121', 'DUT'
            , 'Waarde voor Ind_Mut_Postcode_Dl1 moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10122', 'DUT'
            , 'Waarde voor Ind_Mut_Postcode_Dl2 moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10123', 'DUT'
            , 'Waarde voor Ind_Mut_Numscheidingvan moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10124', 'DUT'
            , 'Waarde voor Ind_Mut_Numscheidingtm moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10125', 'DUT'
            , 'Waarde voor Ind_Mut_Woonplaats_Nen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10126', 'DUT'
            , 'Waarde voor Ind_Mut_Straatnaam_Ptt moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10127', 'DUT'
            , 'Waarde voor Ind_Mut_Straatnaam_Nen moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10128', 'DUT'
            , 'Waarde voor Ind_Mut_Gemeentenaam moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10129', 'DUT'
            , 'Waarde voor Ind_Mut_Codprov moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10130', 'DUT'
            , 'Waarde voor Ind_Mut_Codcebuco moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10131', 'DUT'
            , 'Waarde voor Reekscod_Oud moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10132', 'DUT'
            , 'Waarde voor Codprov_Oud moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10133', 'DUT'
            , 'Waarde voor Postcode_Nw moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10134', 'DUT'
            , 'Waarde voor Reekscod_Nw moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10135', 'DUT'
            , 'Waarde voor Codprov_Nw moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10136', 'DUT'
            , 'Waarde voor Ind_Mut_Huisnum moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10137', 'DUT'
            , 'Waarde voor Ind_Mut_Huisnumtoev moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10138', 'DUT'
            , 'Waarde voor Ind_Mut_Perceelnum moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10139', 'DUT'
            , 'Waarde voor Ind_Mut_Reekscod moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10140', 'DUT'
            , 'Waarde voor Ind_Mut_Woonplaats_Ptt moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10141', 'DUT'
            , 'Waarde voor Ind_Mut_Straatnaam_Off moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10142', 'DUT'
            , 'Waarde voor Postcode_Oud moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10143', 'DUT'
            , 'Waarde voor Indverwerkt moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10144', 'DUT'
            , 'Waarde voor Codgegeven moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10145', 'DUT'
            , 'Waarde voor Rgt_Codgegeven moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10146', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10147', 'DUT'
            , 'Waarde voor Code Domein moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10148', 'DUT'
            , 'Waarde voor Code Domein moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10149', 'DUT'
            , 'Relatie met deze Relatienummer bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10150', 'DUT'
            , 'Overlijdensdatum moet na de geboortedatum vallen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10151', 'DUT'
            , 'Land met deze Nationaliteit bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10152', 'DUT'
            , 'Land met deze 2e Nationaliteit bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10153', 'DUT'
            , 'Vrle_Ind met deze Relatienummer bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10154', 'DUT'
            , 'Relatie met deze Relatie koppeling bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10155', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10156', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10157', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10158', 'DUT'
            , 'Soort Koppeling met deze Soort koppeling bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10159', 'DUT'
            , 'Relatie Koppeling met deze Relatie, Rol, Soort koppeling, Relatie koppeling, Rol, Geldig van bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10160', 'DUT'
            , 'Relatie mag geen koppeling hebben met zichzelf'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10161', 'DUT'
            , 'Einddatum moet na begindatum liggen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10162', 'DUT'
            , 'Notitie met deze Kladblok bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10163', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10164', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10165', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10166', 'DUT'
            , 'Relatierol met deze Relatie, Rol bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10167', 'DUT'
            , 'Rol met deze Rol bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10168', 'DUT'
            , 'Rol met deze Pr bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10169', 'DUT'
            , 'Rol met deze Omschrijving bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10170', 'DUT'
            , 'Vrol_Plus met deze P/I/B bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10171', 'DUT'
            , 'Soort Koppeling met deze Soort koppeling bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10172', 'DUT'
            , 'Rol met deze Eerste groepsrol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10173', 'DUT'
            , 'Rol met deze Tweede groepsrol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10174', 'DUT'
            , 'Inverse soort koppeling verplicht bij indicatie inverse aanmaken'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10175', 'DUT'
            , 'Soort Koppeling met deze Inverse soort koppeling bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10176', 'DUT'
            , 'Soort Koppeling met deze Omschrijving bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10177', 'DUT'
            , 'Rol met deze Groepsrol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10178', 'DUT'
            , 'Rol met deze Toegestane rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10179', 'DUT'
            , 'Toegestane Rol met deze Groepsrol, Toegestane rol bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10180', 'DUT'
            , 'Land met deze Land bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10181', 'DUT'
            , 'Woonplaats met deze Woonplaats bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10182', 'DUT'
            , 'Straat met deze Straat bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10183', 'DUT'
            , 'Adres met deze Adres bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10184', 'DUT'
            , 'Adres met deze Land, Straat, Woonplaats, Huisnummer, Toevoeging bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10185', 'DUT'
            , 'Land met deze Land bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10186', 'DUT'
            , 'Woonplaats met deze Woonplaats bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10187', 'DUT'
            , 'Woonplaats met deze Land, Woonplaats bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10188', 'DUT'
            , 'Woonplaats met deze Woonplaats bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10189', 'DUT'
            , 'Straat met deze Straat bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10190', 'DUT'
            , 'Straat met deze Woonplaats, Straat bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10191', 'DUT'
            , 'Land met deze Land bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10192', 'DUT'
            , 'Land met deze Naam bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10193', 'DUT'
            , 'Land met deze Naam ingezetene bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10194', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10195', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10196', 'DUT'
            , 'Relatie Externe Code met deze Uniek id van een relatie externe code bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10197', 'DUT'
            , 'Einddatum moet na begindatum liggen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10198', 'DUT'
            , 'Mogelijk Kenmerk met deze Kenmerk, Id bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10199', 'DUT'
            , 'Rol met deze Rol_Codrol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10200', 'DUT'
            , 'Mogelijk Kenmerk met deze Rol_Codrol, Kenmerk bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10201', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10202', 'DUT'
            , 'Mogelijk Kenmerk met deze Mkk_Codkenmerk, Mkk_Id bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10203', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10204', 'DUT'
            , 'Relatie Extra Kenmerk met deze Relatie, Mkk_Id, Mkk_Codkenmerk, Geldig van, Waarde kenmerk bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10205', 'DUT'
            , 'Einddatum moet na begindatum liggen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10206', 'DUT'
            , 'Relatierol met deze Relatie, Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10207', 'DUT'
            , 'Gebruik Relatie met deze Relatie, Rol, Extern systeem bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10208', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10209', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10210', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10211', 'DUT'
            , 'Land met deze Land bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10212', 'DUT'
            , 'Financieel Nummer met deze Relatie, Rekening bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10213', 'DUT'
            , 'Einddatum moet na begindatum vallen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10214', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10215', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10216', 'DUT'
            , 'Adres met deze Adres bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10217', 'DUT'
            , 'Relatie Adres met deze Uniek id van een relatie externe code bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10218', 'DUT'
            , 'Vads_Stt_Vw met deze Adres bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10219', 'DUT'
            , 'Relatie Adres met deze Relatie, Rol, Adres, Soort adres, Geldig van bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10220', 'DUT'
            , 'Einddatum moet na begindatum liggen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10221', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10222', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10223', 'DUT'
            , 'Adres met deze Nummer adres bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10224', 'DUT'
            , 'Communicatie Nummer met deze Nr bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10225', 'DUT'
            , 'Vads_Stt_Vw met deze Nummer adres bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10226', 'DUT'
            , 'Communicatie Nummer met deze Relatie, Soort communicatie, Communicatienummer, Geldig van bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10227', 'DUT'
            , 'Einddatum moet na de begindatum vallen'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10228', 'DUT'
            , 'Vras_Ads_Vw met deze Relatie, Nummer adres bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10229', 'DUT'
            , 'Soort Adres Per Rol met deze Rol, Volgnummer bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10230', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10231', 'DUT'
            , 'Soort Adres Per Rol met deze Rol, Soort adres bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10232', 'DUT'
            , 'Ned Postcode met deze Postcode, Even- onevencode, Scheidingsnummer van, Scheidingsnummer tot/met bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10233', 'DUT'
            , 'Mutatieactie met deze Id bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10234', 'DUT'
            , 'Mutatieactie met deze Mutatiecode, Extern systeem, Organisatie, Rol, Soort koppeling bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10235', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10236', 'DUT'
            , 'Soort Koppeling met deze Soort koppeling bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10237', 'DUT'
            , 'RLE_VAST_ADS_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10238', 'DUT'
            , 'RLE_VAST_STT_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10239', 'DUT'
            , 'RLE_VAST_WPS_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10240', 'DUT'
            , 'RLE_VAST_LND_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10241', 'DUT'
            , 'RLE_VAST_PK'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10242', 'DUT'
            , 'Pcod_Batchrun met deze Pbn_Id bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10243', 'DUT'
            , 'Rela Dubbel met deze Relatie, Mogelijke dubbele relatie bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10244', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10245', 'DUT'
            , 'Relatie met deze Mogelijke dubbele relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10246', 'DUT'
            , 'RLE_VRAS_RAS_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10247', 'DUT'
            , 'RLE_VRAS_ADS_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10248', 'DUT'
            , 'RLE_VRAS_STT_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10249', 'DUT'
            , 'RLE_VRAS_WPS_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10250', 'DUT'
            , 'RLE_VRAS_CNR_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10251', 'DUT'
            , 'RLE_VSWS_WPS_FK1'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10252', 'DUT'
            , 'Belang met deze Relatie, Codreden, Volgnummer bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10253', 'DUT'
            , 'Relatie met deze Relatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10254', 'DUT'
            , 'Reden Belang met deze Codreden bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10255', 'DUT'
            , 'Reden Belang met deze Code bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10256', 'DUT'
            , 'Reden Belang met deze Omschrijving bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10257', 'DUT'
            , 'Pcod_Batchrun met deze Pbn_Id bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10258', 'DUT'
            , 'Rle_Synch_Tabel met deze Numrelatie bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10259', 'DUT'
            , 'Rle_Pcod_Batchrun met deze Id bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10260', 'DUT'
            , 'Rela Gegeven Type met deze Codgegeven bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10261', 'DUT'
            , 'Rdub Gegeven met deze Rle_Numrelatie, Rgt_Codgegeven bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10262', 'DUT'
            , 'Rela Gegeven Type met deze Rgt_Codgegeven bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10263', 'DUT'
            , 'Relatie met deze Rle_Numrelatie bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10264', 'DUT'
            , 'Dubbele Faktor met deze Proces, Rubriek bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10265', 'DUT'
            , 'Rle_Transport_Selektie met deze Rle_Numrelatie bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10266', 'DUT'
            , 'Rol Sys Autorisatie met deze Rol, Code domein, Waarde domein bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10267', 'DUT'
            , 'Rol met deze Rol bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10268', 'DUT'
            , 'Gewenste Mailing met deze Relatienummer, Code domein, Waarde domein bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10269', 'DUT'
            , 'Relatie met deze Relatienummer bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10270', 'DUT'
            , 'Mailing met deze Id bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10271', 'DUT'
            , 'Ontvanger Mailing met deze Id, Numrelatie bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10272', 'DUT'
            , 'Mailing met deze Id bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10273', 'DUT'
            , 'Rle_Vrol_Plus met deze Rol_Codrol bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10274', 'DUT'
            , 'Rle_Adres_Chk met deze Numadres bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10275', 'DUT'
            , 'Rle_Vrle_Ind met deze Numrelatie bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10276', 'DUT'
            , 'Tmp_Dump met deze Volgnummer bestaat al.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10277', 'DUT'
            , 'Waarde voor Rol moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10278', 'DUT'
            , 'Waarde voor Mutatiecode moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10279', 'DUT'
            , 'Waarde voor Indicatie verwerkt moet in hoofdletters.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10280', 'DUT'
            , 'De geboortedatum moet formaat DD-MM-YYYY hebben.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10281', 'DUT'
            , '(BRATT0019) De geboortedatum van een kind mag niet voor de geboortedatum van een ouder liggen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10282', 'DUT'
            , '(BRATT0018) De provincie mag alleen gevuld zijn bij een buitenlands adres.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10283', 'DUT'
            , '(BRDEL001) Een adres mag alleen verwijderd worden als het geen statutair of domicilie adres betreft.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10284', 'DUT'
            , '(BRENT0001) Nederlandse adressen moeten een unieke combinatie van postcode, huisnummer en huisnummer toevoeging hebben. Deze combinatie is reeds in gebruik.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10285', 'DUT'
            , '(BRENT0008) De periode van dit huwelijk, geregistreerd partnerschap of samenlevingscontract overlapt met een andere relatiekoppeling voor (minimaal) ��n van deze twee personen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10286', 'DUT'
            , '(BRIER0021) De begindatum van een relatie koppeling van het type ''Kind van'' mag niet meer dan 9 maanden na het overlijden van de ouder liggen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10287', 'DUT'
            , '(BRUPD0001) Een relatie mag niet gewijzigd worden van een persoon naar een instelling.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10288', 'DUT'
            , '(BRATT0016) Een relatie koppeling mag niet in de toekomst beginnen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10289', 'DUT'
            , '(BRATT0017) Een relatie koppeling mag niet in de toekomst eindigen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10290', 'DUT'
            , '(BRIER0009) Postbus of antwoordnummer is alleen toegestaan bij een correspondentieadres.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10291', 'DUT'
            , '(BRIER0020) Een be�indigde relatie koppeling in zake een huwelijk of een samenlevingscontract mag niet gewijzigd of verwijderd worden als ��n van de relaties geen SOFI-nummer en geen A-nummer heeft.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10292', 'DUT'
            , 'Kanaalvoorkeur alleen in combinatie met een relatie'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10293', 'DUT'
            , 'Verwantschappen alleen in combinatie met een relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10294', 'DUT'
            , 'Let op: het kind in deze verwantschap heeft reeds twee (of meer) ouders. Wilt u doorgaan met opslaan?'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10295', 'DUT'
            , '(BRIER0023) Let op: de begindatum van de kanaalvoorkeur ligt na de einddatum van de relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10296', 'DUT'
            , 'Contactpersonen alleen in combinatie met een adres.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10297', 'DUT'
            , 'De module kan niet gestart worden met een onbekende rol.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10298', 'DUT'
            , 'Het adres kan niet worden aangemaakt.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10299', 'DUT'
            , 'Let op: deze wijziging impliceert een nieuw adres. Voer een nieuwe ''datum vanaf'' in als u niet wilt dat het oude adres wordt overschreven.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10300', 'DUT'
            , '(BRIER0016) Let op: de begindatum van het relatieadres ligt na de einddatum van de relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10301', 'DUT'
            , '(BRIER0015) Let op: de begindatum van het financiele nummer ligt na de einddatum van de relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10302', 'DUT'
            , '(BRIER0008) Let op: de begindatum van het communicatienummer ligt na de einddatum van de relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10303', 'DUT'
            , '(BRATT0015) Het A-nummer voldoet niet aan de GBA 11-proef.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10304', 'DUT'
            , '(BRENT0011) Een <p1> moet een <p2> adres hebben.'
            , 'Waarborgen dat een relatie in de rol van persoon of werkgever minimaal ��n adres heeft.'||CHR(10)||''||CHR(10)||'Een werkgever moet als de einddatum niet is gevuld altijd een geldig statutair zetel adres hebben.'||CHR(10)||'Een persoon moet als de overlijdensdatum niet is gevuld altijd een geldig domicilie adres hebben.');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10305', 'DUT'
            , 'Selecteer eerst een soort relatie alvorens een relatie te kiezen.'
            , 'Selecteer eerst een soort relatie alvorens een relatie te kiezen. Aan de hand van het soort relatie wordt bepaalt welke relatie geselecteerd kan worden.');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10306', 'DUT'
            , 'Er kan geen <p1> worden gevonden met het nummer <p2>.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10307', 'DUT'
            , 'De soort relatie ''<p1>'' kan niet gevonden worden.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10308', 'DUT'
            , 'De code ''<p1>'' is niet uniek. Gebruik de lijst met waarden om een soort relatie te selecteren.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10309', 'DUT'
            , 'Gemoedsbezwaardheid alleen in combinatie met een relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10310', 'DUT'
            , 'Op het scherm te starten moet er een <p1> als parameter worden meegegeven.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10311', 'DUT'
            , 'Dit soort adres is niet toegestaan voor deze relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10312', 'DUT'
            , 'Er mag geen DA adres worden opgevoerd of gewijzigd wanneer de status van de persoon ''In afstemming'', ''Te beoordelen'', ''Overgezet'' of ''Afgemeld'' is.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10313', 'DUT'
            , 'Fout in synchronisatie met GBA: <p1>'
            , 'Fout in synchronisatie met GBA: <p1>'||CHR(10)||''||CHR(10)||'Details:'||CHR(10)||'<p2>');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10314', 'DUT'
            , 'Deze code voor postsoort bestaat niet.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10315', 'DUT'
            , 'Er is geen relatiekoppeling tussen de relatie met rol <p1> en de relatie met rol <p2>.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10316', 'DUT'
            , 'Er kan geen relatie worden geselecteerd als er nog geen rol is ingevoerd.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10317', 'DUT'
            , 'Er is wel een rol ingevoerd, maar nog geen relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10318', 'DUT'
            , 'Deze contactpersoon hoort niet bij de relatie.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10319', 'DUT'
            , 'Het sofinummer is verplicht voor personen die 13 jaar of ouder zijn.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10320', 'DUT'
            , 'Er bestaat al een koppeling van dit type met zelfde relaties in deze periode.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10321', 'DUT'
            , 'Een relatiekoppeling van het type ''Huwelijk'', ''Geregistreerd partnerschap'' of ''Samenlevingscontract'' mag niet eindigen na de overlijdensdatum van een van de twee personen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10322', 'DUT'
            , 'Een relatiekoppeling mag niet beginnen voor de geboortedatum van ��n van de twee personen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10323', 'DUT'
            , 'Een relatiekoppeling van het type ''Huwelijk'', ''Geregistreerd partnerschap'' of ''Samenlevingscontract'' mag niet beginnen na de overlijdensdatum van een van de twee personen.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10324', 'DUT'
            , 'Het type van een relatiekoppeling mag niet gewijzigd worden.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10325', 'DUT'
            , 'De "omgekeerde" code voor de relatiekoppeling kan niet worden gevonden.'
            , 'Een relatiekoppeling wordt gedefinieerd door het type (bijvoorbeeld ''Kind van'').'||CHR(10)||'Elke relatiekoppeling wordt ook "omgekeerd" opgeslagen (bijvoorbeeld: wanneer een ''Kind van'' record wordt opgeslagen wordt een ''Ouder van'' record aangemaakt voor de andere relatie).'||CHR(10)||''||CHR(10)||'Deze foutmelding geeft aan dat de code die opgezocht moet worden voor het "omgekeerde" record (bijvoorbeeld ''Ouder van'' ) niet kan worden gevonden.'||CHR(10)||'');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10326', 'DUT'
            , 'De "omgekeerde" relatiekoppeling kan niet worden gevonden.'
            , 'Een relatiekoppeling wordt gedefinieerd door het type (bijvoorbeeld ''Kind van'').'||CHR(10)||'Elke relatiekoppeling wordt ook "omgekeerd" opgeslagen (bijvoorbeeld: wanneer een ''Kind van'' record wordt opgeslagen wordt een ''Ouder van'' record aangemaakt voor de andere relatie).'||CHR(10)||''||CHR(10)||'Deze foutmelding geeft aan dat het "omgekeerde" record (bijvoorbeeld ''Ouder van'' ) niet kan worden gevonden.');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10327', 'DUT'
            , 'Een persoon mag geen verwantschap met zichzelf hebben.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10328', 'DUT'
            , 'Een relatiekoppeling mag niet eindigen voordat hij begonnen is.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10329', 'DUT'
            , 'Bij het opvoeren van een persoon moet het geslacht ''M''an of ''V''rouw zijn.'
            , '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10330', 'DUT'
            , 'Het sofinummer met tussen 001000007 en 299999993 liggen.'
            , '');

