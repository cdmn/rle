prompt opvoeren gif voor SELECTEREN_SRTADR_PER_ROL
delete from stm_gebruik_interfaces 
where  mde_module = 'RLE0110F'
and    ifv_ife_id = (select id from stm_interfaces where codzoek = 'SELECTEREN_SRTADR_PER_ROL');
commit;



prompt gebruik van SELECTEREN_SRTADR_PER_ROL door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_SRTADR_PER_ROL')
,1
,'RLE0110F'
,1
);

commit;
