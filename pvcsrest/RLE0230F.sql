delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0230F';
delete from stm_moduleparameters
    where mde_module = 'RLE0230F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0230F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0230F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0230F';
DELETE FROM stm_modules
    WHERE module = 'RLE0230F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0230F',1,'RLE',1
    ,'Onderhouden dubbele relaties','S'
    ,'RLE0230F',''
    ,'A','Onderhouden dubbele relaties'
    ,null);
prompt moduleparameters
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0230F',1,'NR_RELATIE',
        ':DBE.RLE_NUMRELATIE_DUB');
