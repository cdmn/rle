delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I050';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I050';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I050';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I050';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I050';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_INIT_I050';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_INIT_I050',1,'RLE',1
    ,'Ophalen contactpersonen (s)','P'
    ,'RLE_PCK_LIJSTEN.PRC_INIT_I050',''
    ,'A',''
    ,6);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_PCK_LIJSTEN.PRC_INIT_I050',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_PCK_LIJSTEN.PRC_INIT_I050',1,'J'
    ,'P_RAS_ID','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_PCK_LIJSTEN.PRC_INIT_I050',1,'J'
    ,'P_POSTCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N4','RLE_PCK_LIJSTEN.PRC_INIT_I050',1,'J'
    ,'P_HUISNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_PCK_LIJSTEN.PRC_INIT_I050',1,'J'
    ,'P_TOEVOEGING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_PCK_LIJSTEN.PRC_INIT_I050',1,'J'
    ,'P_SOORT_ADRES','INP');
