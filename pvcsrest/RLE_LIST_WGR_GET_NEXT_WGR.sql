delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_LIST_WGR.GET_NEXT_WGR';
delete from stm_moduleparameters
    where mde_module = 'RLE_LIST_WGR.GET_NEXT_WGR';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_LIST_WGR.GET_NEXT_WGR';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_LIST_WGR.GET_NEXT_WGR';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_LIST_WGR.GET_NEXT_WGR';
DELETE FROM stm_modules
    WHERE module = 'RLE_LIST_WGR.GET_NEXT_WGR';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_LIST_WGR.GET_NEXT_WGR',1,'RLE',1
    ,'ophalen werkgevers ter controle bestaan','P'
    ,'RLE_LIST_WGR.GET_NEXT_WGR',''
    ,'A',''
    ,3);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_LIST_WGR.GET_NEXT_WGR',1,'J'
    ,'P_IND_AANWEZIG','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_LIST_WGR.GET_NEXT_WGR',1,'N'
    ,'P_RELATIENUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_LIST_WGR.GET_NEXT_WGR',1,'N'
    ,'P_WERKGEVERSNUMMER','OUT');
