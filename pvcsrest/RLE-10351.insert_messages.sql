INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10351', 'Een <p1> mag niet getrouwd zijn met zijn (of haar) <p2>.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10351', 'DUT'
            , 'Een <p1> mag niet getrouwd zijn met zijn (of haar) <p2>.'
            , '');

