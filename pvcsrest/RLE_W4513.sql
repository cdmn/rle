/* Naam......: RLE_W4513.sql
** Release...: W4513
** Datum.....: 28-05-2008 11:50:18
** Notes.....: Gegenereerd door ITStools Versie 3.10
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4513', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_administraties.atb
@@ RLE.rle_administraties.atb
prompt RLE.rle_rie_adm_fk.con
@@ RLE.rle_rie_adm_fk.con
prompt RLE.rle_v_administraties.vw
@@ RLE.rle_v_administraties.vw
prompt RLE.rle_v_relatierollen_in_adm.vw
@@ RLE.rle_v_relatierollen_in_adm.vw
prompt RLE.rle_insert_rie.prc
@@ RLE.rle_insert_rie.prc
prompt RLE.rle_chk_gba_afst.fnc
@@ RLE.rle_chk_gba_afst.fnc
prompt RLE.rle_rie.pks
@@ RLE.rle_rie.pks
prompt RLE.rle_cre_verwantschap.prc
@@ RLE.rle_cre_verwantschap.prc
prompt RLE.rle_cre_waardeoverdracht.prc
@@ RLE.rle_cre_waardeoverdracht.prc
prompt RLE.rle_ins_ll_overeenkomst_prs.prc
@@ RLE.rle_ins_ll_overeenkomst_prs.prc
prompt RLE.rle_ins_ll_overeenkomst_wgr.prc
@@ RLE.rle_ins_ll_overeenkomst_wgr.prc
prompt RLE.rle_ins_relatierol_in_admin.prc
@@ RLE.rle_ins_relatierol_in_admin.prc
prompt RLE.rle_mut_col_contract.prc
@@ RLE.rle_mut_col_contract.prc
prompt RLE.rle_mut_werknemer_beroepen.prc
@@ RLE.rle_mut_werknemer_beroepen.prc
prompt RLE.rle_mut_wgr_werkzaamheden.prc
@@ RLE.rle_mut_wgr_werkzaamheden.prc
prompt RLE.rle_aq_cre_verwantschap.prc
@@ RLE.rle_aq_cre_verwantschap.prc
prompt RLE.rle_aq_cre_waardeoverdracht.prc
@@ RLE.rle_aq_cre_waardeoverdracht.prc
prompt RLE.rle_aq_ins_ll_overeenkomst_prs.prc
@@ RLE.rle_aq_ins_ll_overeenkomst_prs.prc
prompt RLE.rle_aq_ins_ll_overeenkomst_wgr.prc
@@ RLE.rle_aq_ins_ll_overeenkomst_wgr.prc
prompt RLE.rle_aq_ins_relatierol_in_admin.prc
@@ RLE.rle_aq_ins_relatierol_in_admin.prc
prompt RLE.rle_aq_mut_col_contract.prc
@@ RLE.rle_aq_mut_col_contract.prc
prompt RLE.rle_aq_mut_werknemer_beroepen.prc
@@ RLE.rle_aq_mut_werknemer_beroepen.prc
prompt RLE.rle_aq_mut_wgr_werkzaamheden.prc
@@ RLE.rle_aq_mut_wgr_werkzaamheden.prc
prompt RLE.rle_ins_bsp_spaarproduct.prc
@@ RLE.rle_ins_bsp_spaarproduct.prc
prompt RLE.rle_aq_ins_bsp_spaarproduct.prc
@@ RLE.rle_aq_ins_bsp_spaarproduct.prc
prompt RLE.rle_aq_rkn_rle_rie.prc
@@ RLE.rle_aq_rkn_rle_rie.prc
set define off
set define on
set define off
prompt RLE.rle_rie.pkb
@@ RLE.rle_rie.pkb
set define on
prompt RLE.rle_cnr_bri.trg
@@ RLE.rle_cnr_bri.trg
prompt RLE.rle_fnr_bri.trg
@@ RLE.rle_fnr_bri.trg
prompt RLE.rle_ras_bri.trg
@@ RLE.rle_ras_bri.trg
prompt RLE.rle_rkn_as.trg
@@ RLE.rle_rkn_as.trg
prompt RLE.grant_references.sql
@@ RLE.grant_references.sql
prompt RLE.insert_qms.sql
@@ RLE.insert_qms.sql
prompt RLE.rle_delete_rie.sql
@@ RLE.rle_delete_rie.sql
prompt RLE.update_insert_adm.sql
@@ RLE.update_insert_adm.sql
/* Switchen naar het juiste schema
*/
set define on
connect /@&&DBN

prompt RLE0500F.rol
@@ RLE0500F.rol
prompt RLE.type_administratie.dom
@@ RLE.type_administratie.dom

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4513 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W4513 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
