delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0061F';
delete from stm_moduleparameters
    where mde_module = 'RLE0061F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0061F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0061F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0061F';
DELETE FROM stm_modules
    WHERE module = 'RLE0061F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0061F',1,'RLE',1
    ,'Onderhouden relaties','S'
    ,'RLE0061F',''
    ,'A','Onderhouden relaties'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0061F',1,'J'
    ,'Relatienr','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0061F',1,'J'
    ,'Rol','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('DAT_PEIL','RLE0061F',1,'J'
    ,'Peildatum','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('INDLEEG','RLE0061F',1,'J'
    ,'Leeg','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SRT_ADRES','RLE0061F',1,'J'
    ,'Adressoort','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SW_PRS','RLE0061F',1,'J'
    ,'Persoonsnummer','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SOFI_VERPL','RLE0061F',1,'J'
    ,'Sofinummer verplicht','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'REF_GWT',
        ':RLE.DWE_WRDDOM_GEWATITEL');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'REF_BST',
        ':RLE.DWE_WRDDOM_BRGST');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'COD_GEBDAT',
        ':RLE.CODE_GEBDAT_FICTIEF');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'REF_GES',
        ':RLE.DWE_WRDDOM_GESLACHT');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'REF_VVL',
        ':RLE.DWE_WRDDOM_VVGSL');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'CODE_ROL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD1');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'DAT_PEIL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD12');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'REF_SAD',
        ':QMS$CTRL.CGNBT_CHAR_FIELD13');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'NAAMGEBR',
        ':RLE.CODE_AANDUIDING_NAAMGEBRUIK');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'NR_PERSOON',
        ':RLE.U_PERSOONSNUMMER');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0061F',1,'EINDDATUM',
        ':global.cg$einddatum');
