declare
  t_id number;
begin
  delete
  from rle_rol_in_koppelingen
  where code_rol_in_koppeling in
  ('MTT','DTT');
  select max(id) into t_id
  from rle_rol_in_koppelingen;
  insert into RLE_ROL_IN_KOPPELINGEN
  (ID
  ,CODE_ROL_IN_KOPPELING
  ,OMSCHRIJVING
  ,ROL_CODROL_BEPERKT_TOT
  ,ROL_CODROL_MET
  ) values
  ((t_id + 1)
  ,'MTT'
  ,'Moederbedrijf tbv team'
  ,'WG'
  ,'WG'
  );
  insert into RLE_ROL_IN_KOPPELINGEN
  (ID
  ,CODE_ROL_IN_KOPPELING
  ,OMSCHRIJVING
  ,ROL_CODROL_BEPERKT_TOT
  ,ROL_CODROL_MET
  ) values
  ((t_id + 2)
  ,'DTT'
  ,'Dochterbedrijf tbv team'
  ,'WG'
  ,'WG'
  );
end;
/
  