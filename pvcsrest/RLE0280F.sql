delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0280F';
delete from stm_moduleparameters
    where mde_module = 'RLE0280F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0280F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0280F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0280F';
DELETE FROM stm_modules
    WHERE module = 'RLE0280F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0280F',1,'RLE',1
    ,'Selecteren rol in koppeling','S'
    ,'RLE0280F',''
    ,'A','Selecteren rol'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODROL_MET','RLE0280F',1,'J'
    ,'CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('RKG_ID','RLE0280F',1,'J'
    ,'RKG_ID','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODROL_BEP','RLE0280F',1,'J'
    ,'CODROL','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0280F',1,'RKG_ID',
        ':RKG.ID');
