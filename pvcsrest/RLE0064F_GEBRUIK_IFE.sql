delete from stm_gebruik_interfaces where mde_module = 'RLE0064F';
delete from stm_gebruik_interfaces
    where ifv_ife_id = (select id from stm_interfaces where codzoek = 'ONDERHOUDEN_SOFI_A_NUMMER');

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek ='ONDH_VERWANTSCHAPPEN')
,1
,'RLE0064F'
,1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'ONDERHOUDEN_SOFI_A_NUMMER')
    ,1
    ,'Q34WERKB'
    ,1);

commit;
