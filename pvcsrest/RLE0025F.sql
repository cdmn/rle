delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0025F';
delete from stm_moduleparameters
    where mde_module = 'RLE0025F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0025F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0025F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0025F';
DELETE FROM stm_modules
    WHERE module = 'RLE0025F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0025F',1,'RLE',1
    ,'Toevoegen persoon','S'
    ,'RLE0025F',''
    ,'A','Toevoegen persoon'
    ,null);
prompt moduleparameters
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0025F',1,'REF_BST',
        ':RLE.DWE_WRDDOM_BRGST');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0025F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0025F',1,'REF_GES',
        ':RLE.DWE_WRDDOM_GESLACHT');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0025F',1,'DOM_ADRES',
        ':RAS2.ADS_NUMADRES');
