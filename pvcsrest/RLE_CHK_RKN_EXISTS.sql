delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_CHK_RKN_EXISTS';
delete from stm_moduleparameters
    where mde_module = 'RLE_CHK_RKN_EXISTS';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_CHK_RKN_EXISTS';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_CHK_RKN_EXISTS';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_CHK_RKN_EXISTS';
DELETE FROM stm_modules
    WHERE module = 'RLE_CHK_RKN_EXISTS';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_CHK_RKN_EXISTS',1,'RLE',1
    ,'Controleer bestaan RKN','F'
    ,'RLE_CHK_RKN_EXISTS',''
    ,'A',''
    ,6);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_CHK_RKN_EXISTS',1,'J'
    ,'PIV_CODROL_2','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D6','RLE_CHK_RKN_EXISTS',1,'J'
    ,'PID_PEILDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_CHK_RKN_EXISTS',1,'J'
    ,'RETURNVALUE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_CHK_RKN_EXISTS',1,'J'
    ,'PIN_NUMRELATIE_1','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_CHK_RKN_EXISTS',1,'J'
    ,'PIV_CODROL_1','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N4','RLE_CHK_RKN_EXISTS',1,'J'
    ,'PIN_NUMRELATIE_2','INP');
