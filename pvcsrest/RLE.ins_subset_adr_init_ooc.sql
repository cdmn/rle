--
-- creatie  : 23-feb-2009 
-- creator : JKU
-- doel : vullen van de personen + adres gegevens
--        van de eerste subset INIT_OOC
--        voor werknemers die een rol spelen voor OOC
--  
-- delete van de set is voor test doeleinden
--
delete from rle_ext_afgestemde_adressen
where       eat_id = (select id from rle_ext_adr_subsets where subsetnaam = 'INIT_OOC')
/
delete from rle_ext_adr_subsets
where       subsetnaam = 'INIT_OOC'
/
insert into rle_ext_adr_subsets
            ( id
            , subsetnaam
            , rol_codrol
            , soort_adres
            , dat_selectie
            , dat_ontvangen
            )
values      ( rle_eat_seq1.nextval 
            , 'INIT_OOC'
            , 'PR'
            , 'OA'
            , sysdate
            , null
            )          
/
insert into rle_ext_afgestemde_adressen
       ( id 
        ,eat_id
        ,mn_numrelatie
        ,mn_persoonsnummer
        ,mn_voorletters
        ,mn_tussenvoegsel
        ,mn_achternaam
        ,mn_straat
        ,mn_huisnr
        ,mn_huisnr_toevoeging
        ,mn_postcode
        ,mn_woonplaats )
select  RLE_EAS_SEQ1_NEXT_ID -- function voor rle_eas_seq1.nextval 
      , (select id from rle_ext_adr_subsets where subsetnaam = 'INIT_OOC') -- eat_id
      , avg.rle_numrelatie_pr numrelatie
      , max(avg.werknemer) persoonsnummer
      -- max / maakt niet uit adressen zijn gelijk
      -- is alleen om dubbele vanuit avg te filteren
      , max(rle.voorletter)       voorletter
      , max(rle.dwe_wrddom_vvgsl )dwe_wrddom_vvgsl
      , max(rle.namrelatie       )namrelatie
      , max(stt.straatnaam       )straatnaam
      , max(ads.huisnummer       )huisnummer
      , max(ads.toevhuisnum      )toevhuisnum
      , max(ads.postcode         )postcode
      , max(wps.woonplaats       )woonplaats
from   rle_relaties rle
     , rle_relatie_adressen ras
     , rle_adressen ads
     , rle_straten stt
     , rle_woonplaatsen wps
     , avg_arbeidsverhoudingen avg
     , ( select distinct ( lpad ( wgr.nr_werkgever, 6, 0) ) wgr_nummer
        from            wgr_werkgevers wgr   ---- uit het systeem comp_wgr
                      , wgr_werkzaamheden wwg   ---- uit het systeem comp_wgr
                      , wgr_werkgever_statussen wst   ---- uit het systeem comp_wgr
                      , wsf wsf   ---- uit wsf (odin)
        where           wst.code_status <> 'B'   ---- mogelijke statussen zijn  ('B' ,'D','I')
        and             wwg.wgr_id = wst.wgr_id
        and             wgr.id = wst.wgr_id
        and             sysdate between nvl ( wgr.dat_begin, sysdate ) and nvl ( wgr.dat_einde, sysdate)
        and             sysdate between wst.dat_begin and nvl ( wst.dat_einde, sysdate)
        and             wwg.code_groep_werkzhd = wsf.kodegvw
        and             nvl ( wwg.code_subgroep_werkzhd, '@' ) = nvl ( wsf.kodesgw, '@')
        and             wsf.kodefon = 'OOC'   ---- Een vd OPLEIDINGSFONDSEN
        and             sysdate between wwg.dat_begin and nvl ( wwg.dat_einde , sysdate)
        and             sysdate between wsf.dataanv and nvl ( wsf.dateind, sysdate)
        union
        select cvc.wgr_werkgnr wgr_nummer
        from   cvc   ---- via de odin de vrijwillige werkgevers ophalen
        where  sysdate between dataanv and nvl ( dateind, sysdate)
        and    vzp_nummer = 13 ) wsr   -- vzp_nummer vh OOC Fonds
where   avg.werkgever = wsr.wgr_nummer
and     avg.dat_einde is null
and     avg.rle_numrelatie_pr = rle.numrelatie
and     rle.numrelatie = ras.rle_numrelatie
and     stt.straatid = ads.stt_straatid
and     wps.woonplaatsid = ads.wps_woonplaatsid
and     ras.dateinde is null
and     ras.dwe_wrddom_srtadr = 'OA'
and     ras.rol_codrol = 'PR'
and     ads.lnd_codland = 'NL'
and     ads.numadres = ras.ads_numadres
group   by avg.rle_numrelatie_pr -- ieder prs maar 1 x afstemmen
/
commit;

