merge into rle_standaard_voorwaarden sve1 using ( select 'AANTAL WNR MAX (WGR)' code ,q'#4.1) Werkgevers met maximum aantal werknemers#' naam ,q'#Definitie:
Werkgevers waarvan het aantal Werknemers kleiner of gelijk is dan het opgegeven aantal.
Parameters:
1	Datum
2	Aantal#' beschrijving ,q'#select 1
from   avg_arbeidsverhoudingen   avh
,      rle_relatie_externe_codes rec
where  rec.rle_numrelatie    = :NUMRELATIE
and    rec.dwe_wrddom_extsys = 'WGR'
and    rec.rol_codrol        = 'WG'
and    avh.werkgever         = rec.extern_relatie
and     :VAR2 between avh.dat_begin
              and nvl(avh.dat_einde, :VAR2)
and not exists (select 1
                from avg_aard_arb_verhoudingen aad
                where aad.avg_id    = avh.id
                and   aad.code_aard = 'OPKR'
                and   :VAR2 between aad.dat_begin
                            and nvl(aad.dat_einde, :VAR2)
                )
having count(*)  <= :VAR1#' sqltext ,q'#Maximum aantal werknemers#' var1_naam ,'N' var1_datatype ,q'#Peildatum (DD-MM-YYYY)#' var2_naam ,'D' var2_datatype ,q'##' var3_naam ,'' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$PAS' creatie_door ,to_date('20-11-2009 16:33:04','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'EPM' mutatie_door ,to_date('01-04-2010 13:40:40','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
