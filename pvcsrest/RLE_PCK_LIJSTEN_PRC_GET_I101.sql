delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I101';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I101';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I101';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I101';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I101';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_GET_I101';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_GET_I101',1,'RLE',1
    ,'Ophalen relatierollen (rij)','P'
    ,'RLE_PCK_LIJSTEN.PRC_GET_I101',''
    ,'A',''
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_PCK_LIJSTEN.PRC_GET_I101',1,'J'
    ,'P_CODROL','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_PCK_LIJSTEN.PRC_GET_I101',1,'J'
    ,'P_OMSROL','OUT');
