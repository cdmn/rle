merge into rle_standaard_voorwaarden sve1 using ( select 'SALARIS RANGE (PRS)' code ,q'#12.3) Personen met salaris binnen range (1.31)#' naam ,q'#Definitie:
Personen met op gegeven peildatum een Jaarsalaris groter of gelijk aan gegeven salaris-ondergrens en kleiner of gelijk aan gegeven salaris-bovengrens.
Parameters:
1	Datum
2	Salaris-ondergrens
3	Salaris-bovengrens
4	Code inkomenscomponent#' beschrijving ,q'#select 1
from   avg_arbeidsverhoudingen avg 
,      avg_werknemer_inkomens wni 
where  avg.rle_numrelatie_pr      = :NUMRELATIE
and    wni.avh_id                 = avg.id
and    wni.code_inkomenscomponent in (:VAR1)
and    :VAR4 between wni.dat_begin and nvl(wni.dat_einde, :VAR4 )
having round(sum(  case 
             when wni.code_frequentie = 'M'
               then wni.bedrag_basisvaluta * 12.96
             when wni.code_frequentie = 'J'
               then wni.bedrag_basisvaluta * 1.08
             when wni.code_frequentie = 'W'
               then wni.bedrag_basisvaluta * 56.36
             when wni.code_frequentie = '4'
               then wni.bedrag_basisvaluta * 14.09
             end ),0) between :VAR2 and :VAR3#' sqltext ,q'#Code inkomenscomponent (b.v. SALI, PROV )#' var1_naam ,'A' var1_datatype ,q'#Minimaal salaris#' var2_naam ,'N' var2_datatype ,q'#Maximaal salaris#' var3_naam ,'N' var3_datatype ,q'#Peildatum (DD-MM-YYYY)#' var4_naam ,'D' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'COMP_RLE' creatie_door ,to_date('15-12-2009 11:32:23','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('18-12-2009 12:47:41','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
