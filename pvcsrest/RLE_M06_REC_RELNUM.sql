delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_M06_REC_RELNUM';
delete from stm_moduleparameters
    where mde_module = 'RLE_M06_REC_RELNUM';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_M06_REC_RELNUM';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_M06_REC_RELNUM';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_M06_REC_RELNUM';
DELETE FROM stm_modules
    WHERE module = 'RLE_M06_REC_RELNUM';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_M06_REC_RELNUM',1,'RLE',1
    ,'Ophalen relnum bij externe ref','P'
    ,'RLE_M06_REC_RELNUM',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_M06_REC_RELNUM',1,'J'
    ,'P_RELEXT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_M06_REC_RELNUM',1,'J'
    ,'P_SYSTEEMCOMP','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_M06_REC_RELNUM',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_M06_REC_RELNUM',1,'J'
    ,'P_DATPARM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N5','RLE_M06_REC_RELNUM',1,'J'
    ,'P_O_RELNUM','MOD');
