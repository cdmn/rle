set define off
merge into rle_administraties rle
  using ( select 'ALG' code
               , 'Administratie voor algemeen beschikbare gegevens' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'A' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'BSP' code
               , 'Bedrijfssparen' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'PMT' code
               , 'St. Pensioenfonds Metaal en Techniek' omschrijving
               , 'J' ind_afstemmen_gba_tgstn
               , 'P' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'NVSCHADE' code
               , 'N.V. Schadeverz. Metaal en Techn. Bedrijfstakken' omschrijving
               , 'J' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'BOVEMIJ' code
               , 'Bovemij Inkomensverzekeringen' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'ZILVKR' code
               , 'Zilveren Kruis Achmea' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'INTPOL' code
               , 'Interpolis' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'MTLL' code
               , 'Vakraad Metaal en Techniek' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'MNLL' code
               , 'Stichting Mn Services Levensloop' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'AOV' code
               , 'N.V. Schadeverz. Metaal en Techn. Bedrijfstakken' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'MELL' code
               , 'Vakraad Metalektro' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'V' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'VFM' code
               , 'Stichting VakantieFondsMetaalnijverheid' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'SFM' code
               , 'Sociaal Fonds Metaal En Techn. Bedr. Takken ' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'MET' code
               , 'Stichting  Motor- en Tweewielerbedrijven' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OOC' code
               , 'Opleidingsfonds Carrosserie' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OFE' code
               , 'Opleidingsfonds Electrotechniek, Installatie' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OOG' code
               , 'Opleidingsfonds Galvanotechniek' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OOM' code
               , 'Opleidingsfonds Metaalbewerking' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OOGZ' code
               , 'Opleidingsfonds Goud en Zilver Nijverheid' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OLC' code
               , 'Opleidingsfonds Loodgieters-, Fitters-, En Centr.V' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OOMT' code
               , 'Opleidingsfonds Motorvoertuigen' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'SKO' code
               , 'Opleidingsfonds Koeltechniek' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OOI' code
               , 'Opleidingsfonds Isolatie' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OSZS' code
               , 'O&O scheepsben. handel, zeilmakers, scheepstuigers' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OOMT A' code
               , 'Opleidingsfonds Motorvoertuigen (A)' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OTIB_E' code
               , 'O & O voor het Technisch Installatiebedrijf (E)' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OTIB_L' code
               , 'O & O voor het Technisch Installatiebedrijf (L)' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'OTIB_K' code
               , 'O & O voor het Technisch Installatiebedrijf (K)' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'SVZ' code
               , 'Stichting Vakopleiding Zonweringbedrijven' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'SVUM' code
               , 'Stichting Vervroegd Uittreden MTB' omschrijving
               , 'J' ind_afstemmen_gba_tgstn
               , 'S' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
merge into rle_administraties rle
  using ( select 'SKM' code
               , 'Stichting Kinderopvang Metaal En Techniek' omschrijving
               , 'N' ind_afstemmen_gba_tgstn
               , 'O' type
         from   dual ) new_rec
  on ( rle.code = new_rec.code )
  when matched then
    update
       set rle.omschrijving = new_rec.omschrijving, rle.ind_afstemmen_gba_tgstn = new_rec.ind_afstemmen_gba_tgstn
         , rle.type = new_rec.type
  when not matched then
    insert ( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values ( new_rec.code, new_rec.omschrijving, new_rec.ind_afstemmen_gba_tgstn, new_rec.type );
set define on
