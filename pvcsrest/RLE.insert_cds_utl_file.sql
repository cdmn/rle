whenever sqlerror continue

prompt RLE.insert_cds_utl_file
prompt deleting CDS_UTL_FILE voor RLE postcode

delete from CDS_UTL_FILE
where APPL  = 'RLE'
and module  = 'POSTCODE';

prompt inserting CDS_UTL_FILE voor RLE postcode

INSERT INTO CDS_UTL_FILE
( APPL, MODULE, UTL_DIR, BLOCKED )
VALUES
('RLE', 'POSTCODE',
 (select f.UTL_DIR
  from cds_utl_file f
  where f.APPL='EBR' and f.MODULE='ALGEMEEN')
--, '/ot1/ClusterD/app/HERI/EBR/algemeen'
, 'N');
COMMIT;

