delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I107';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I107';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I107';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I107';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I107';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_INIT_I107';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_INIT_I107',1,'RLE',1
    ,'Ophalen woonplaatsen (start)','P'
    ,'RLE_PCK_LIJSTEN.PRC_INIT_I107',''
    ,'A',''
    ,0);
prompt moduleparameters
