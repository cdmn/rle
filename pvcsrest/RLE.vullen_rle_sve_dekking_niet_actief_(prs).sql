merge into rle_standaard_voorwaarden sve1 using ( select 'DEKKING NIET ACTIEF (PRS)' code ,q'#7.2) Personen zonder dekking (1.12)#' naam ,q'#Definitie:
Personen met op gegeven peildatum een geldige Verzekerde AO en heeft voor ��n of meer van de gegeven Dekkingsoorten: 
- geen geldige Grondslag AO 
of 
- met een geldige Grondslag AO en een geldige Uitsluiting.
Parameters:
1	Datum
2	Lijst met Dekkingsoorten (REG DEKKINGSOORT.Code)#' beschrijving ,q'#select 1
from aos_verzekerden_ao        vao2
,    rle_relatie_externe_codes rec
where vao2.prs_persoonsnummer = rec.extern_relatie
and   rec.dwe_wrddom_extsys   = 'PRS'
and   rec.rol_codrol          = 'PR'
and   rec.rle_numrelatie      = :NUMRELATIE
and exists 
   (select 1
    from  avg_arbeidsverhoudingen avg
    where avg.rle_numrelatie_pr = rec.rle_numrelatie
    and   :VAR2                 between avg.dat_begin and nvl(avg.dat_einde, :VAR2)
   )
and (not exists 
       (select 1
        from  aos_vastgestelde_dekkingen_ao vdo
        ,     aos_polissen_ao               pao
        ,     aos_verzekerden_ao            vao 
        where vao.prs_persoonsnummer  = vao2.prs_persoonsnummer
        and   vao.pao_id              = pao.id
        and   vdo.pgo_pao_id          = pao.id
        and   pao.fiatteur_polis      is not null
        and   pao.fiatteur_annulering is null
        and   vdo.pgo_dks_code        in (:VAR1)
        and   vdo.vervalmoment        is null
        and   vdo.fiatmoment          is not null
        and   :VAR2                   between vdo.ingangsdatum and nvl(vdo.einddatum, :VAR2)
        and exists 
          (select 1
           from  aos_verzekerd_perioden_ao veo
           where veo.fiatmoment   is not null
           and   veo.vervalmoment is null
           and   veo.veo_type     = 'GAO'
           and   veo.vao_id       = vao.id
           and   :VAR2            between veo.ingangsdatum and nvl(veo.einddatum, :VAR2)
          )
       ) 
     or exists
       (select 1
        from  aos_vastgestelde_dekkingen_ao vdo
        ,     aos_verzekerden_ao            vao
        ,     aos_polissen_ao               pao
        where vao.prs_persoonsnummer  =  vao2.prs_persoonsnummer
        and   vao.pao_id              = pao.id
        and   vdo.pgo_pao_id          = pao.id
        and   pao.fiatteur_polis      is not null
        and   pao.fiatteur_annulering is null
        and   vdo.pgo_dks_code        in (:VAR1)
        and   vdo.vervalmoment        is null
        and   vdo.fiatmoment          is not null
        and   :VAR2                   between vdo.ingangsdatum and nvl(vdo.einddatum, :VAR2)
        and exists
          (select 1
           from  aos_verzekerd_perioden_ao veo
           where veo.fiatmoment   is not null
           and   veo.vervalmoment is null
           and   veo.veo_type     = 'GAO'
           and   veo.vao_id       = vao.id
           and   :VAR2            between veo.ingangsdatum and nvl(veo.einddatum, :VAR2))
        and exists
          (select 1
           from  aos_verzekerd_perioden_ao veo
           where veo.vao_id        = vao.id
           and   veo.fiatmoment    is not null
           and   veo.pgo_dks_code  = vdo.pgo_dks_code
           and   veo.vervalmoment  is null
           and   veo.veo_type      = 'USG'
           and   :VAR2             between veo.ingangsdatum and nvl(veo.einddatum, :VAR2))
       )
    )#' sqltext ,q'#Dekking (b.v. WBO,WHI )#' var1_naam ,'A' var1_datatype ,q'#Peildatum (DD-MM-YYYY)#' var2_naam ,'D' var2_datatype ,q'##' var3_naam ,'' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$PAS' creatie_door ,to_date('20-11-2009 16:33:05','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('09-12-2009 11:10:36','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
