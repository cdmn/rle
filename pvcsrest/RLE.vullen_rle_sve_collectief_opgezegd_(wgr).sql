merge into rle_standaard_voorwaarden sve1 using ( select 'COLLECTIEF OPGEZEGD (WGR)' code ,q'#6.1) Werkgevers met collectieve opzegging (1.9)#' naam ,q'#Definitie:
Werkgevers met op gegeven peildatum een geldige Polis AO waarvan de Indicator Autom Verzek Nieuwe Deelnemers Nee is.
Parameters:
1	Datum#' beschrijving ,q'#select 1
from   aos_polissen_ao pao
,      rle_relatie_externe_codes rec
where  rec.rle_numrelatie              = :NUMRELATIE
and    rec.dwe_wrddom_extsys           = 'WGR'
and    rec.rol_codrol                  = 'WG'
and    pao.wgr_werkgnr                 = rec.extern_relatie
and    pao.indicatie_auto_verz_nw_dlnr = 'N'
and    pao.fiatteur_polis              is not null
and    pao.fiatteur_annulering         is null
and    :VAR1 between pao.ingangsdatum
                 and nvl(pao.einddatum_premie, :VAR1)#' sqltext ,q'#Peildatum (DD-MM-YYYY)#' var1_naam ,'D' var1_datatype ,q'##' var2_naam ,'' var2_datatype ,q'##' var3_naam ,'' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$PAS' creatie_door ,to_date('20-11-2009 16:33:04','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('01-12-2009 17:15:59','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
