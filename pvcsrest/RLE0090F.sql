delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0090F';
delete from stm_moduleparameters
    where mde_module = 'RLE0090F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0090F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0090F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0090F';
DELETE FROM stm_modules
    WHERE module = 'RLE0090F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0090F',1,'RLE',1
    ,'Onderhouden externe referentie','S'
    ,'RLE0090F',''
    ,'A','Onderhouden externe referenties'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0090F',1,'N'
    ,'Relatienr','MOD');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0090F',1,'EX_REF',
        ':REC.DWE_WRDDOM_EXTSYS');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0090F',1,'REF_CES',
        ':REC.CGNBT_RLE_NUMRELATIE');
