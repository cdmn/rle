delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_ADMIN_GEGEVENS';
delete from stm_moduleparameters
    where mde_module = 'RLE_ADMIN_GEGEVENS';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_ADMIN_GEGEVENS';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_ADMIN_GEGEVENS';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_ADMIN_GEGEVENS';
DELETE FROM stm_modules
    WHERE module = 'RLE_ADMIN_GEGEVENS';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_ADMIN_GEGEVENS',1,'RLE',1
    ,'Ophalen gegevens van een administratiekantoor','P'
    ,'RLE_ADMIN_GEGEVENS',''
    ,'A',''
    ,11);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_EXTERN_NR','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_NUMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_NAMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_STRAAT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N5','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_HUISNR','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_TOEVNUM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_POSTCODE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_WOONPLAATS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_LAND','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_NUMCOMMUNICATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_ADMIN_GEGEVENS',1,'J'
    ,'P_NAAM_CONTACTPERS','OUT');
