delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_BEZWAARD';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_BEZWAARD';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_BEZWAARD';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_BEZWAARD';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_BEZWAARD';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_BEZWAARD';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_BEZWAARD',1,'RLE',1
    ,'Bepalen gemoedsbezwaardheid','P'
    ,'RLE_GET_BEZWAARD',''
    ,'A',''
    ,7);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_GET_BEZWAARD',1,'J'
    ,'POD_BEGINDATUM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_GET_BEZWAARD',1,'N'
    ,'POD_EINDDATUM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GET_BEZWAARD',1,'J'
    ,'POV_CREATIE_DOOR','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D7','RLE_GET_BEZWAARD',1,'J'
    ,'POD_DAT_CREATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_BEZWAARD',1,'J'
    ,'PIN_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D2','RLE_GET_BEZWAARD',1,'J'
    ,'PID_PEILDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_BEZWAARD',1,'J'
    ,'POV_IND_BEZWAARD','OUT');
