-- inserten parameterwaarden voor de rapportages DWHAR004R

-- QMS_REP_MODULES
INSERT INTO QMS_REP_MODULES 
( SHORT_NAME
, PRINT_ORIENTATION
, PURPOSE ) 
VALUES 
( 'RLE0001R'
 ,'PORTRAIT'
 , NULL); 

-- QMS_MODULES
INSERT INTO QMS_MODULES 
( ID
, SHORT_NAME
, NAME
, IMPLEMENTATION_NAME
, TITLE
, MTYPE
, PURPOSE
, MSHELP_ID
, VALIDATION_CLAUSE ) 
VALUES 
((select max(id)+1 from QMS_MODULES)
, 'RLE0001R'
, 'Relaties handmatig naar GBA'
, 'RLE0001R'
, 'Relaties handmatig naar GBA'
, 'REPORT'
, 'Relaties handmatig naar GBA'
, NULL
, NULL); 




COMMIT;
