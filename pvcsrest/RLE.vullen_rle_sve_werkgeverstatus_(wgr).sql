merge into rle_standaard_voorwaarden sve1 using ( select 'WERKGEVERSTATUS (WGR)' code ,q'#1.1) Werkgevers met status (1.41)#' naam ,q'#Definitie:
Werkgevers met op gegeven peildatum een geldige Werkgever Status met minimaal ��n van de gegeven WGR Status codes.
Parameters:
1	Datum
2	lijst met WGR Status Codes (WGR WERKGEVER STATUS.Code)#' beschrijving ,q'#select 1
from     rle_relatie_externe_codes   rec
,        wgr_werkgevers              wgr
,        wgr_werkgever_statussen     wss
where    rec.rle_numrelatie    = :NUMRELATIE
and      rec.rol_codrol        = 'WG'
and      rec.dwe_wrddom_extsys = 'WGR'
and      wgr.nr_werkgever      = rec.extern_relatie
and      wss.wgr_id            = wgr.id
and      :VAR2                 between wss.dat_begin
                               and     nvl(wss.dat_einde,:VAR2)
and      wss.code_status       in ( :VAR1 )#' sqltext ,q'#Status (b.v. D,B )#' var1_naam ,'A' var1_datatype ,q'#Peildatum (DD-MM-YYYY)#' var2_naam ,'D' var2_datatype ,q'##' var3_naam ,'' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$PAS' creatie_door ,to_date('20-11-2009 16:33:04','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('09-12-2009 11:14:39','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
