INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10333', 'Dit a-nummer (<p1>) voldoet niet aan de GBA-elfproef.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10333', 'DUT'
            , 'Dit a-nummer (<p1>) voldoet niet aan de GBA-elfproef.'
            , '');

