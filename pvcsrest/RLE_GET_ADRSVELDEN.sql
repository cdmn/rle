delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_ADRSVELDEN';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_ADRSVELDEN';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_ADRSVELDEN';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_ADRSVELDEN';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_ADRSVELDEN';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_ADRSVELDEN';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_ADRSVELDEN',1,'RLE',1
    ,'ophalen adresvelden','P'
    ,'RLE_GET_ADRSVELDEN',''
    ,'A',''
    ,16);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_SRTADRES','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_DATUMP','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_POSTCODE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N6','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_HUISNR','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_TOEVNUM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_WOONPLAATS','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_STRAAT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_CODLAND','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_IND_WOONBOOT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A13','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_IND_WOONWAGEN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_LOCATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_LANDNAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D15','RLE_GET_ADRSVELDEN',1,'J'
    ,'P_DATBEGIN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D16','RLE_GET_ADRSVELDEN',1,'N'
    ,'P_DATEINDE','OUT');
