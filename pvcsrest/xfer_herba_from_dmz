#!/usr/bin/ksh
##set -x
## De volgende 2 regels moeten verwijderd worden na testfase op UNIX
#TMP="/opt/pvapp/p/var/output"
OTAP_KEY="p"
## Einde van de te verwijderen regels
#=======================================================================================#
#                                                                                       #
# Procedurenaam          : xfer_herba_from_dmz                                          #
# Procedureomschrijving  : Dit script haalt een file op vanuit de MN DMZ omgeving.      #
#                          Dit ter verdere verwerking door Cronacle (GBA).              #
# Auteur                 : Eelco Dikkeboom                                              #
# Afdeling               : IV-is/bo                                                     #
# Datum                  : 26-05-2014                                                   #
# Versie                 : 1.1                                                          #
# Sublibrarys            : Nee                                                          #
#                                                                                       #
#---------------------------------------------------------------------------------------#
# Naam             | Datum      | Wijziging                                             #
#---------------------------------------------------------------------------------------#
# EDI              | 14-01-2014 | Creatie                                               #
# EDI              | 26-05-2014 | FILE parameter verwijderd                             #
#=======================================================================================#

## Algemene variabelen definitie

if [ "${OTAP_KEY}" = "p" ] ; then
 export DMZ_SERVER=mn512
fi
if [ "${OTAP_KEY}" = "a" ] ; then
 export DMZ_SERVER=mn613
fi
if [ "${OTAP_KEY}" = "t" ] ; then
 export DMZ_SERVER=mn613
fi
if [ "${OTAP_KEY}" = "o" ] ; then
 export DMZ_SERVER=mn613
fi

export SCRIPTNAME=`basename $0`
export DOWN_REMOTE_PATH=/opt/gbatentsync/upload
export REMOTE_USER=gbatent
export DOWN_LOCAL_PATH=/opt/pvapp/${OTAP_KEY}/var/output/RLE/outbox
export LOCAL_SERVER=`hostname`
export EXT_SERVER=DMZ
export DATE=`date '+%Y%m%d'`
export FILELIST=HERBA_DOWN_FILELLIST

## Functies voor log en error meldingen
logpr () {
   export TIMESTAMP=`date '+%d-%h-%Y_%H:%M:%S'`
   echo "${TIMESTAMP} : $1"
         }

error () {
   echo
   export TIMESTAMP=`date '+%d-%h-%Y_%H:%M:%S'`
   echo "${TIMESTAMP} : ERROR : $1"
   echo "${TIMESTAMP} : Script ${SCRIPTNAME} wordt beeindigd!"
   echo
   exit 1
         }

## Start van het script
logpr "Start ${SCRIPTNAME}";echo

## Print logheader
logpr "#------------------------------------------------------------------#"
logpr "# Script       :     ${SCRIPTNAME}"
logpr "# Locatie      :     `pwd`"
logpr "#------------------------------------------------------------------#"

> ${DOWN_LOCAL_PATH}/${FILELIST}

## Is de DMZ server via ping bereikbaar
ping -c 1 ${DMZ_SERVER} 1>/dev/null
if [ $? != 0 ] ; then
 error "De DMZ server ${DMZ_SERVER} is via ping niet bereikbaar!"
fi

ssh -n ${REMOTE_USER}@${DMZ_SERVER} find ${DOWN_REMOTE_PATH} -maxdepth 1 -type f | awk -F"${DOWN_REMOTE_PATH}/" '{print $2}' | while read FILE
do

## Files overzetten van ${DMZ_SERVER} naar ${LOCAL_SERVER}.
scp ${REMOTE_USER}@${DMZ_SERVER}:${DOWN_REMOTE_PATH}/${FILE} ${DOWN_LOCAL_PATH}/. 1>/dev/null
if [ $? != 0 ] ; then
 error "Problemen met het overzetten van bestand ${BESTAND} van ${DMZ_SERVER} naar ${LOCAL_SERVER}."
else
 logpr "Bestand ${FILE} succesvol overgezet van ${DMZ_SERVER} naar ${LOCAL_SERVER}."
fi

## MD5 sum checken & bestand verwijderen op DMZ.
logpr "Bestand ${FILE} verwijderen op ${DMZ_SERVER}."
MD5_LOCAL=`md5sum ${DOWN_LOCAL_PATH}/${FILE} | awk -F" " '{print $1}'`
MD5_REMOTE=`ssh -n ${REMOTE_USER}@${DMZ_SERVER} md5sum ${DOWN_REMOTE_PATH}/${FILE} | awk -F" " '{print $1}'`
if [ "${MD5_LOCAL}" != "${MD5_REMOTE}" ] ; then
 echo "MD5 sum van ${FILE} wijkt af na de download, bestand zal niet worden verwijderd op de ${DMZ_SERVER}!"
else
 ssh -n ${REMOTE_USER}@${DMZ_SERVER} rm ${DOWN_REMOTE_PATH}/${FILE}
fi

echo ${FILE} >> ${DOWN_LOCAL_PATH}/${FILELIST}

done

echo;cat ${DOWN_LOCAL_PATH}/${FILELIST}

echo;logpr "Einde script ${SCRIPTNAME}"
## Einde van het script
