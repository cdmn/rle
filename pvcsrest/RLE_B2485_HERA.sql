/* Naam......: RLE_B2485_HERA.sql
** Release...: B2485
** Datum.....: Wed Apr 27 10:36:24 2005
** Notes.....: Aangemaakt met Des_tool_mn Versie 3.1.4.1.5.9i 


*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
                    ,'continue') P_EXIT
from dual;


WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C en RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    into dummy
    from dba_users
   where username = 'COMP_RLE';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Voor databases afgeleid van ZEUS, dus aangemaakt vanuit EROS
   moeten we het versie-nummer vullen in de table IMU_APPLICATIE_VERSIE
   Voor database afgeleid van HERA, dus aangemaakt vanuit DKRE
   moeten we het versie-nummer vullen in de tabel STM_VERSIE_WIJZIGINGEN
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'B2485', sysdate );

commit;


/* Cross grants tussen de COMP_-users onderling deel 1
*/
REM start RLE_B2485_grants1.sql

/* Switchen naar het juiste schema
*/
connect /@&&DBN
set define on
set verify off
define _password = "SU_PASSWORD"
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT


REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
REM Hier de files zetten die vanuit deze directory aangeroepen moeten worden.

@@ RLE.rle_gba_wijzig_adres.prc

REM Tot zover de inhoud van de directory
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* Cross grants tussen de COMP_-users onderling deel 2
*/
connect /@&&DBN
REM start RLE_B2485_grants2.sql

/* creatie database-rollen
*/
connect /@&&DBN
REM start RLE.????.rol

/* RLE_REF_CODES vullen en check constraints plaatsen
*/
connect /@&&DBN
REM start RLE.????.dom




insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'B2485 : Klaar', sysdate );

commit;

prompt Implementatie RLE release B2485 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on

