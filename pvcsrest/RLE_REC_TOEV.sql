delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_REC_TOEV';
delete from stm_moduleparameters
    where mde_module = 'RLE_REC_TOEV';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_REC_TOEV';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_REC_TOEV';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_REC_TOEV';
DELETE FROM stm_modules
    WHERE module = 'RLE_REC_TOEV';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_REC_TOEV',1,'RLE',1
    ,'Toevoegen externe referentie','P'
    ,'RLE_REC_TOEV',''
    ,'A',''
    ,13);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A13','RLE_REC_TOEV',1,'J'
    ,'P_IND_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_REC_TOEV',1,'J'
    ,'P_ID','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_REC_TOEV',1,'J'
    ,'P_RLE_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_REC_TOEV',1,'J'
    ,'P_WRDDOM_EXTSYS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_REC_TOEV',1,'J'
    ,'P_DATINGANG','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_REC_TOEV',1,'J'
    ,'P_CODORGANISATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_REC_TOEV',1,'J'
    ,'P_ROL_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_REC_TOEV',1,'J'
    ,'P_EXTERN_RELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D8','RLE_REC_TOEV',1,'J'
    ,'P_DATEINDE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_REC_TOEV',1,'J'
    ,'P_NEPPARAMETER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_REC_TOEV',1,'J'
    ,'P_IND_GEVERIFIEERD','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_REC_TOEV',1,'J'
    ,'P_IND_VERVALLEN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_REC_TOEV',1,'J'
    ,'P_IND_COMMIT','INP');
