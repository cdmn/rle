INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10346', 'Een kind mag niet ouder zijn dan de ouder.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10346', 'DUT'
            , 'Een kind mag niet ouder zijn dan de ouder.'
            , 'De geboortedatum van het kind mag niet voor de geboortedatum van de ouder liggen.');

