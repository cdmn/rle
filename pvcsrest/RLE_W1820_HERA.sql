/* Naam......: RLE_W1820_HERA.sql
** Release...: 1
** Datum.....: 7 aug 2002


*/

set linesize   79
set pause     off


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma beeindigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er vanuit JCS wordt gestart.
   Hier wordt er vanuit gegaan dat vanuit JCS de user OPS$xJCS wordt gebruikt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(user,6),'JCS','exit','continue') P_EXIT
from dual;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Voor databases afgeleid van ZEUS, dus aangemaakt vanuit EROS
   moeten we het versie-nummer vullen in de table IMU_APPLICATIE_VERSIE
   Voor database afgeleid van HERA, dus aangemaakt vanuit DKRE
   moeten we het versie-nummer vullen in de tabel STM_VERSIE_WIJZIGINGEN
*/

insert into STM_VERSIE_WIJZIGINGEN
       values ( 'RLE', 'W1820', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* insert scripts
*/
connect /@&&DBN

@@stmdel.sql

prompt Implementatie RLE release W1820 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from _wafnwaefn_sadf_;
set termout on

