/* Naam......: RLE_W5853.sql
** Release...: W5853
** Datum.....: 14-12-2009 17:37:47
** Notes.....: Gegenereerd door ITStools Versie 3.25
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W5853', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema voor RLE.rle_mv_swg_algemeen_nr.ind
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_mv_zoek_werkgever.vw
@@ RLE.rle_mv_zoek_werkgever.vw
prompt RLE.rle_mv_zoek_werknemer.vw
@@ RLE.rle_mv_zoek_werknemer.vw
prompt RLE.rle_v_personen_xl.vw
@@ RLE.rle_v_personen_xl.vw
prompt RLE.rle_v_personen_xl_portal.vw
@@ RLE.rle_v_personen_xl_portal.vw
prompt RLE.rle_v_werkgevers_xl.vw
@@ RLE.rle_v_werkgevers_xl.vw
prompt RLE.rle_v_werkgevers_xl_portal.vw
@@ RLE.rle_v_werkgevers_xl_portal.vw
prompt RLE.rle_mv_swg_algemeen_nr.ind
@@ RLE.rle_mv_swg_algemeen_nr.ind
prompt RLE.rle_mv_swg_kvk_nummer.ind
@@ RLE.rle_mv_swg_kvk_nummer.ind
prompt RLE.rle_mv_swg_pens_admin_id.ind
@@ RLE.rle_mv_swg_pens_admin_id.ind
prompt RLE.rle_mv_swg_pov_aansluitnummer.ind
@@ RLE.rle_mv_swg_pov_aansluitnummer.ind
prompt RLE.rle_mv_swg_relatienummer.ind
@@ RLE.rle_mv_swg_relatienummer.ind
prompt RLE.rle_mv_swg_sz_huisnummer.ind
@@ RLE.rle_mv_swg_sz_huisnummer.ind
prompt RLE.rle_mv_swg_sz_postcode.ind
@@ RLE.rle_mv_swg_sz_postcode.ind
prompt RLE.rle_mv_swg_sz_straat.ind
@@ RLE.rle_mv_swg_sz_straat.ind
prompt RLE.rle_mv_swg_sz_woonplaats.ind
@@ RLE.rle_mv_swg_sz_woonplaats.ind
prompt RLE.rle_mv_swg_werkgevernummer.ind
@@ RLE.rle_mv_swg_werkgevernummer.ind
prompt RLE.rle_mv_swg_zoeknaam.ind
@@ RLE.rle_mv_swg_zoeknaam.ind
prompt RLE.rle_mv_swn_da_postcode.ind
@@ RLE.rle_mv_swn_da_postcode.ind
prompt RLE.rle_mv_swn_da_straat.ind
@@ RLE.rle_mv_swn_da_straat.ind
prompt RLE.rle_mv_swn_da_woonplaats.ind
@@ RLE.rle_mv_swn_da_woonplaats.ind
prompt RLE.rle_mv_swn_datgeboorte.ind
@@ RLE.rle_mv_swn_datgeboorte.ind
prompt RLE.rle_mv_swn_datoverlijden.ind
@@ RLE.rle_mv_swn_datoverlijden.ind
prompt RLE.rle_mv_swn_naam_partner.ind
@@ RLE.rle_mv_swn_naam_partner.ind
prompt RLE.rle_mv_swn_persoonsnummer.ind
@@ RLE.rle_mv_swn_persoonsnummer.ind
prompt RLE.rle_mv_swn_pvachmea_nr.ind
@@ RLE.rle_mv_swn_pvachmea_nr.ind
prompt RLE.rle_mv_swn_relatienummer.ind
@@ RLE.rle_mv_swn_relatienummer.ind
prompt RLE.rle_mv_swn_sofinummer.ind
@@ RLE.rle_mv_swn_sofinummer.ind
prompt RLE.rle_mv_swn_wgr_personeelsnr.ind
@@ RLE.rle_mv_swn_wgr_personeelsnr.ind
prompt RLE.rle_mv_swn_zoeknaam.ind
@@ RLE.rle_mv_swn_zoeknaam.ind


set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W5853 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W5853 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
