whenever sqlerror continue

alter table RLE_SADRES_ROLLEN disable all triggers;

delete from RLE_SADRES_ROLLEN where codrol in ( 'VN', 'VZ' );

alter table RLE_SADRES_ROLLEN enable all triggers;

INSERT INTO RLE_SADRES_ROLLEN ( CODROL, VOLGNR, DWE_WRDDOM_SADRES) VALUES ('VN', 1, 'FA');
INSERT INTO RLE_SADRES_ROLLEN ( CODROL, VOLGNR, DWE_WRDDOM_SADRES) VALUES ('VN', 2, 'CA');
INSERT INTO RLE_SADRES_ROLLEN ( CODROL, VOLGNR, DWE_WRDDOM_SADRES) VALUES ('VZ', 1, 'FA');

whenever sqlerror exit
