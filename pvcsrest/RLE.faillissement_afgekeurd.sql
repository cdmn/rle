whenever sqlerror exit rollback

set lines         10000
set pages         10000
set pause         off
set serveroutput  on size unlimited
set trimspool     on
set verify        off
set feedback      on
set termout       on
set echo          off
set heading       on

column cur_database     new_value  cur_database

select name cur_database 
from   v$database
/

column code           format a12         heading "CODE"
column omschrijving   format a120        heading "BESCHRIJVING"
column language       format a10         heading "LANGUAGE"
column text           format a120        heading "TEXT"
column msp_code       format a12         heading "MSP_CODE"
column rv_low_value   format a40         heading "RV_LOW_VALUE"
column rv_domain      format a40         heading "RV_DOMAIN"
column rv_meaning     format a40         heading "RV_MEANING"

spool RLE.faillissement_afgekeurd.&cur_database..txt

prompt **
prompt Inserten BOF messages voor AVG
prompt **
prompt **
prompt DataBASE : &cur_database
prompt **

/*
AFG	Faillissement afgekeurd
GER	Registratie faillissement gereed
GEW	Afhandeling faillissement gewijzigd
REG	Faillisement geregisteerd in BOF
*/

prompt 
prompt
prompt   Displayen status faillissement SITUATIE VOOR 
prompt
prompt 

select c.rv_low_value, c.rv_domain, c.rv_meaning
from   cg_ref_codes c
where  rv_domain = 'STATUS AFHANDELING FAILLISSEMENT'
/

update cg_ref_codes
set    rv_meaning   = 'Faillissement afgekeurd' 
where  rv_low_value = 'AFG'
and    rv_domain    = 'STATUS AFHANDELING FAILLISSEMENT'
/

prompt 
prompt
prompt   Displayen status faillissement SITUATIE NA
prompt
prompt 

select c.rv_low_value, c.rv_domain, c.rv_meaning
from   cg_ref_codes c
where  rv_domain = 'STATUS AFHANDELING FAILLISSEMENT'
/


-- ROLLBACK;

spool off
