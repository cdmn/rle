delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PLS_GET_RELANUM';
delete from stm_moduleparameters
    where mde_module = 'RLE_PLS_GET_RELANUM';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PLS_GET_RELANUM';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PLS_GET_RELANUM';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PLS_GET_RELANUM';
DELETE FROM stm_modules
    WHERE module = 'RLE_PLS_GET_RELANUM';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PLS_GET_RELANUM',1,'RLE',1
    ,'ophalen relatienr bij sofinr','F'
    ,'RLE_PLS_GET_RELANUM',''
    ,'A',''
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_PLS_GET_RELANUM',1,'J'
    ,'P_SOFI_NR','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_PLS_GET_RELANUM',1,'J'
    ,'returnvalue','OUT');
