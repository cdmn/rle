DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10419';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10419';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10419', 'Waarde voor Indicatie SEPA moet een geldige keuze zijn uit J en N.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Waarde voor Indicatie SEPA moet een geldige keuze zijn uit J en N.', NULL, 'RLE-10419');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10420';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10420';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10420', 'Waarde voor LANDCODE_ISO3166_A2 moet in hoofdletters.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Waarde voor LANDCODE_ISO3166_A2 moet in hoofdletters.', NULL, 'RLE-10420');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10421';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10421';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10421', 'waarde voor deze unieke sleutel van financiele nummers bestaat al.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'waarde voor deze unieke sleutel van financiele nummers bestaat al.', NULL, 'RLE-10421');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10422';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10422';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10422', 'BIC en IBAN dienen �f beide gevuld, �f beide leeg te zijn.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'BIC en IBAN dienen �f beide gevuld, �f beide leeg te zijn.', NULL, 'RLE-10422');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10423';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10423';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10423', 'Indien wijze betaling AS is, dienen BIC en IBAN gevuld te zijn.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Indien wijze betaling AS is, dienen BIC en IBAN gevuld te zijn.', NULL, 'RLE-10423');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10424';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10424';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10424', 'De landcode <p1> in de IBAN behoort bij een niet-SEPA land.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'De landcode <p1> in de IBAN behoort bij een niet-SEPA land.', NULL, 'RLE-10424');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10425';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10425';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10425', 'ongeldige combinatie BIC, IBAN en BBAN.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'ongeldige combinatie BIC, IBAN en BBAN.', NULL, 'RLE-10425');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10426';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10426';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10426', 'BIC en IBAN dienen beide leeg te zijn.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'BIC en IBAN dienen beide leeg te zijn.', NULL, 'RLE-10426');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10427';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10427';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10427', 'IBAN <p1> voldoet niet aan de 97 proef.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'IBAN <p1> voldoet niet aan de 97 proef.', NULL, 'RLE-10427');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10428';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10428';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10428', 'Bij een Nederlandse IBAN mag het rekeningnummer niet worden gemuteerd.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Bij een Nederlandse IBAN mag het rekeningnummer niet worden gemuteerd.', NULL, 'RLE-10428');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10429';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10429';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10429', 'Deze waarde voor ISO-landcode bestaat al.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Deze waarde voor ISO-landcode bestaat al.', NULL, 'RLE-10429');


DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10430';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10430';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10430', 'De betaalwijze dient alleen ingevoerd te worden indien het een SEPA-land betreft.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'De betaalwijze dient ingevoerd te worden indien het een SEPA-land betreft. Bij niet SEPA-landen is deze leeg', NULL, 'RLE-10430');


