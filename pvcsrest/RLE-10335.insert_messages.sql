INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10335', 'De gegevens van werkgever ''000001'' mogen niet gewijzigd worden.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10335', 'DUT'
            , 'De gegevens van werkgever ''000001'' mogen niet gewijzigd worden.'
            , '');

