delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_KANAAL';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_KANAAL';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_KANAAL';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_KANAAL';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_KANAAL';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_KANAAL';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_KANAAL',1,'RLE',1
    ,'Ophalen van de kanaalvoorkeur van een relatie','P'
    ,'RLE_GET_KANAAL',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_KANAAL',1,'J'
    ,'p_numrelatie','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D2','RLE_GET_KANAAL',1,'N'
    ,'p_peildatum','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_KANAAL',1,'J'
    ,'p_kanaalvoorkeur','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_GET_KANAAL',1,'J'
    ,'p_datbegin','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_GET_KANAAL',1,'N'
    ,'p_dateinde','OUT');
