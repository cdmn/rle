INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10342', 'De naam van een persoon mag geen cijfers bevatten'
            , 'E', 'N', 'N', 'N', 'RLE_RLE_CK20');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10342', 'DUT'
            , 'De naam van een persoon mag geen cijfers bevatten'
            , '');

