-- Programma : RLE.bugfix_6006.sql
-- Auteur    : XSW
-- Datum     : 09-07-2012
-- Doel      : aanzetten tiggers van rle_landen
-- Marval    : 
----------------------------------------------
whenever sqlerror exit 
whenever oserror  exit 

set lines        200
set pages        100
set pause        off
set serveroutput on size 1000000
set trimspool    on
set verify       off
set feedback     on
set termout      on

spool rle.bugfix_6006.log

prompt ***********************************

set linesize 500
set heading off

set define on
col dbn new_value dbn noprint

select sysdate || '  database: ' || value
from   v$parameter
where  name = 'db_name';

set heading on
prompt
prompt **********************************
prompt ***      start bugfix 6006     ***
prompt **********************************
prompt 
prompt
prompt
prompt =================================
prompt = aanzetten triggers rle_landen =
prompt =================================
prompt
prompt

prompt beginsituatie triggers rle_landen tabel
select trigger_name   
     , table_name
     , status
from   user_triggers
where  table_name = 'RLE_LANDEN';

prompt bezig met het aanzetten van alle triggers
alter table comp_rle.rle_landen enable all triggers;


prompt eindsituatie triggers rle_landen tabel
select trigger_name   
     , table_name
     , status
from   user_triggers
where  table_name = 'RLE_LANDEN';

prompt
prompt
prompt einde rle.bugfix 6006......
spool off

