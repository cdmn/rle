set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_AUT');
alter user COMP_AUT identified by COMP_AUT;
connect COMP_AUT/COMP_AUT@&&DBN
alter user COMP_AUT identified by values '&&_password';
column password clear
undefine _password


insert into comp_aut.aut_filesysteemobjecten ( korte_naam, naam )
values ( 'RLE0330F', 'ONDERHOUDEN PENSIOENCOMMUNICATIE' );


set define on
connect /@&&DBN