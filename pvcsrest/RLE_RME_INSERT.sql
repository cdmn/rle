delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RME_INSERT';
delete from stm_moduleparameters
    where mde_module = 'RLE_RME_INSERT';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RME_INSERT';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RME_INSERT';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RME_INSERT';
DELETE FROM stm_modules
    WHERE module = 'RLE_RME_INSERT';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RME_INSERT',1,'RLE',1
    ,'opvoeren rij relatiemutatie','P'
    ,'RLE_RME_INSERT',''
    ,'A',''
    ,10);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_RME_INSERT',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_RME_INSERT',1,'J'
    ,'P_ID_TABEL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_RME_INSERT',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_RME_INSERT',1,'J'
    ,'P_CODE_MUTATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_RME_INSERT',1,'J'
    ,'P_SRT_MUTATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_RME_INSERT',1,'J'
    ,'P_VELD','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D7','RLE_RME_INSERT',1,'J'
    ,'P_DAT_BEGIN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D8','RLE_RME_INSERT',1,'J'
    ,'P_DAT_BEGIN_NW','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_RME_INSERT',1,'J'
    ,'P_WAARDE_OUD','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_RME_INSERT',1,'J'
    ,'P_WAARDE_NW','INP');
