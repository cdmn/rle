DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-00978';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-00978';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME )
VALUES ( 'RLE-00978', 'Administratie <p1> mag niet valideren op basis van het GBA', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE )
VALUES ( 'DUT', 'Administratie <p1> mag niet valideren op basis van het GBA', NULL, 'RLE-00978');


