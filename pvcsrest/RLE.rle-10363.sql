DELETE qms_message_text mst
WHERE  mst.msp_code = 'RLE-10363'
/

DELETE qms_message_properties msp
WHERE  msp.code = 'RLE-10363'
/


INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND,
SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) VALUES ( 
'RLE-10363', 'Bij het zoeken op postbusnummer is ook (een gedeelte van) postcode verplicht', 'E', 'N', 'N', 'N', NULL); 

INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) VALUES ( 
'DUT', 'Bij het zoeken op postbusnummer is ook (een gedeelte van) postcode verplicht', NULL, 'RLE-10363'); 
