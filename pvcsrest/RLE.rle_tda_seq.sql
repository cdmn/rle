create sequence comp_rle.rle_tda_seq
start with 1
maxvalue 999999999999
minvalue 1
nocycle
nocache
noorder;

create public synonym rle_tda_seq for comp_rle.rle_tda_seq;
