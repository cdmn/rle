DELETE qms_message_text mst
WHERE  mst.msp_code LIKE 'RLE-10357'
/

DELETE qms_message_properties msp
WHERE  msp.code LIKE 'RLE-10357'
/

INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10357', 'Bij rol WG mag handelsnaam niet meer dan 35 posities bevatten.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10357', 'DUT'
            , 'Bij rol WG mag handelsnaam niet meer dan 35 posities bevatten.'
            , '');

