INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10338', 'De datum emigratie mag niet voor de geboortedatum liggen.'
            , 'E', 'N', 'N', 'N', 'RLE_RLE_CK18');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10338', 'DUT'
            , 'De datum emigratie mag niet voor de geboortedatum liggen.'
            , '');

