prompt QMS_MESSAGE_TEXT
--
delete from QMS_MESSAGE_TEXT
where msp_code = 'RLE-00628'
;
--
prompt QMS_MESSAGE_PROPERTIES
--
delete from QMS_MESSAGE_PROPERTIES
where code = 'RLE-00628'
;
--
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND,
SUPPR_ALWAYS_IND ) VALUES ( 
'RLE-00628', 'Procent-teken gevonden, deze zoekopdracht kan lang gaan duren!', 'I'
, 'N', 'N', 'N'); 
-- 
prompt QMS_MESSAGE_TEXT
--
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) VALUES ( 
'DUT', 'Procent-teken gevonden, deze zoekopdracht kan lang gaan duren!', NULL, 'RLE-00628'); 

