delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I051';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I051';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I051';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I051';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I051';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_GET_I051';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_GET_I051',1,'RLE',1
    ,'Ophalen verwantschappen (r)','P'
    ,'RLE_PCK_LIJSTEN.PRC_GET_I051',''
    ,'A',''
    ,11);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_REC_PERSOONSNUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_GESLACHT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_OMS_GESLACHT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_NUMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_COD_RELATIE_KOPPELING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_OMS_RELATIE_KOPPELING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_RKN_DATBEGIN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D5','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_RKN_DATEINDE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D6','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_RLE_DATGEBOORTE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D7','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_RLE_DATOVERLIJDEN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_PCK_LIJSTEN.PRC_GET_I051',1,'J'
    ,'P_RLE_OPGEMAAKTE_NAAM','OUT');
