delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_RKG';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_RKG';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_RKG';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_RKG';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_RKG';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_RKG';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_RKG',1,'RLE',1
    ,'Ophalen rol in koppeling bij relatiekoppeling','P'
    ,'RLE_GET_RKG',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_RKG',1,'J'
    ,'P_CODE_ROL_IN_KOPPELING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_RKG',1,'J'
    ,'P_OMSCHRIJVING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_RKG',1,'J'
    ,'P_ROL_CODROL_BEPERKT_TOT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GET_RKG',1,'J'
    ,'P_ROL_CODROL_MET','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_RKG',1,'J'
    ,'P_RKN_ID','INP');
