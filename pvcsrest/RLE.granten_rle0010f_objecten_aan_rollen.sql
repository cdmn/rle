declare
cursor c_grants
is
select distinct('grant '||ggk.soort_handeling||' on '
                ||ggk.onderwerp||' to '||dbr.naam) recht
from   aut_taken_van_rol       tvl
,      AUT_FILESYSTEEMOBJ_VOOR_TAAK fvk
,      aut_gegevensgebruik ggk
,      aut_database_rollen dbr
where  dbr.rol_id = tvl.rol_id
and    tvl.ingangsdatum <= sysdate
and    nvl(tvl.einddatum,sysdate) >= sysdate
and    fvk.tak_id = tvl.tak_id
and    ggk.fot_korte_naam ='RLE0010F';
--
e_rol_not_exists exception;
pragma exception_init (e_rol_not_exists, -1917);
--
begin
for r_grants in c_grants 
loop
  --
  begin
    dbms_output.put_line (r_grants.recht);
    execute immediate r_grants.recht;
  exception
    when e_rol_not_exists then dbms_output.put_line ('NOT EXISTS');
  end;
  --
end loop;
end;
/
