delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GBA_WIJZIG_ADRES';
delete from stm_moduleparameters
    where mde_module = 'RLE_GBA_WIJZIG_ADRES';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GBA_WIJZIG_ADRES';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GBA_WIJZIG_ADRES';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GBA_WIJZIG_ADRES';
DELETE FROM stm_modules
    WHERE module = 'RLE_GBA_WIJZIG_ADRES';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GBA_WIJZIG_ADRES',1,'RLE',1
    ,'I_089: Wijzigen adres vanuit GBA (Alleen aan te roepen door GBA!!!)','P'
    ,'RLE_GBA_WIJZIG_ADRES',''
    ,'A',''
    ,10);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIN_PERSOONSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIV_STRAATNAAM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIN_HUISNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIV_WOONPLAATS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIV_HUISNUMMER_TOEVOEGING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIV_POSTCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIV_LOCATIEOMSCHRIJVING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'PIV_LANDCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'POV_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_GBA_WIJZIG_ADRES',1,'J'
    ,'POV_FOUTMELDING','OUT');
