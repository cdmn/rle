/*************************************************************************
***              Uitdelen RLE grants aan APP_kbd_mns                  ***
*************************************************************************/
grant select on comp_rle.rle_v_financieel_nummers to app_kbd_mns;

grant select on comp_rle.rle_v_financieel_nummers to app_kbd_pme;

grant select on comp_rle.rle_v_financieel_nummers to app_pwg_pme;

