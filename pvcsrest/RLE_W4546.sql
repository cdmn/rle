/* Naam......: RLE_W4546.sql
** Release...: W4546
** Datum.....: 22-04-2008 12:18:05
** Notes.....: Gegenereerd door ITStools Versie 3.10
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4546', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_v_administratiekantoren.vw
@@ RLE.rle_v_administratiekantoren.vw
prompt RLE.rle_v_ned_postcode.vw
@@ RLE.rle_v_ned_postcode.vw
prompt RLE.rle_mut_handelsnaam.prc
@@ RLE.rle_mut_handelsnaam.prc
prompt RLE.rle_chk_br_ier0037.fnc
@@ RLE.rle_chk_br_ier0037.fnc
prompt RLE.rle_chk_br_mod0001.fnc
@@ RLE.rle_chk_br_mod0001.fnc
prompt RLE.rle_chk_br_mod0003.fnc
@@ RLE.rle_chk_br_mod0003.fnc
prompt RLE.rle_chk_br_tpl0029.fnc
@@ RLE.rle_chk_br_tpl0029.fnc
prompt RLE.rle_chk_tel_tien.fnc
@@ RLE.rle_chk_tel_tien.fnc
prompt RLE.rle_ins_postsoort_adressen.sql
@@ RLE.rle_ins_postsoort_adressen.sql

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4546 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W4546 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
