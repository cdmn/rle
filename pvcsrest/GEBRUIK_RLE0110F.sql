delete from stm_gebruik_interfaces
where  mde_module = 'RLE0110F';

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'SEL_REF_ADI')
    ,1,'RLE0110F',1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'SEL_LAND')
    ,1,'RLE0110F',1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'ONDH_CONTACTPERSONEN')
    ,1,'RLE0110F',1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'SEL_POSTCODE')
    ,1,'RLE0110F',1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'SEL_REF_AVV')
    ,1,'RLE0110F',1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'SEL_REF_SAD')
    ,1,'RLE0110F',1);

insert into stm_gebruik_interfaces
    (ifv_ife_id,ifv_numversie,mde_module,mde_numversie)
    values
    ((select id from stm_interfaces where codzoek = 'SELECTEREN_SRTADR_PER_ROL')
    ,1,'RLE0110F',1);