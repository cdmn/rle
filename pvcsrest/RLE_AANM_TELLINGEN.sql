REM ****************************************************************************
REM Naam      : RLE_AANM_TELLINGEN.sql
REM Doel      : Het rapporteren van aantallen gevalideerde adressen per
REM             uitspraak- en verhuiscode.
REM -
REM Parameters: %1  script naam   ( wordt door JCS bepaald )
REM             %2  script id     ( wordt door JCS bepaald )
REM             %3  naam subset
REM             %4  relatierol
REM             %5  soort adres
REM -
REM Wijzigingshistorie:
REM Versie Naam                   Datum      Wijziging
REM ------ ---------------------- ---------- -----------------------------------
REM 1.0    E. Schillemans         17-03-2009 Creatie
REM 1.1    E. Schillemans         03-04-2009 Kolom Verwerkt kan nu ook de
REM                                          waarde X bevatten. Er kan nu
REM                                          onderscheid gemaakt worden tussen
REM                                          de waarde N (vanwege een fout
REM                                          tijdens een workflow stap) en de
REM                                          waarde X (indicatie verwerkt nog
REM                                          niet ingevuld).
REM                                          Per query totaalregel toegevoegd
REM                                          waarin het aantal adressen wordt
REM                                          weergegeven.
REM ****************************************************************************

whenever sqlerror exit failure
whenever oserror  exit failure

SET linesize 130
SET define '&'
SET define on
SET pagesize 100
SET heading off
SET feedback off
SET pause off
SET verify off
SET trimspool on
SET termout off

define SUBSET=&3
define ROL_CODROL=&4
define SOORT_ADRES=&5

column FILENAAM noprint new_value FILENAAM
column SELECTIEDATUM noprint new_value SELECTIEDATUM

define l_instance = ''

column l1 noprint new_value l_instance

SELECT   name||'_'                          L1
FROM     V$DATABASE
WHERE    name != 'PPVD'
/


REM - Bepalen bestandsnaam
REM - De bestandsnaam begint altijd met de scriptnaam, %1
REM   en eindigt (voor de extensie) met het script_id, %2
REM - Juiste bestandsnaam moet nog worden vastgesteld
REM - Nu staan alle parameters vernoemd,
REM   maar de lengte van de bestandsnaam kon wel eens gelimiteerd zijn
REM - Eventueel enkele parameterwaarden in de naam laten vervallen

define l_file_naam = ''

column l2 noprint new_value l_file_naam

SELECT                  '&l_instance'       -- INSTANCE NAAM
                     || '&1'                -- SCRIPTNAAM  , underscore zit in de selectie
         || '_'      || '&3'                -- SUBSETNAAM
         || '_'      || '&2'                -- SCRIPT_ID
         || '.TXT'
                                            L2
FROM       DUAL
/

alter session set optimizer_index_cost_adj=1000;
-- Komma voor decimale positie en duizendtallen scheider instellen
alter session set nls_numeric_characters = ',.'

-- Op unix
SPOOL $TMP/RLEPRC10.txt
-- Op windows
-- SPOOL &l_file_naam

column groep noprint
column verhuiscode       format a11
column correct           format 9G999G990
column onbekend          format 9G999G990
column mn_adres_recenter format 9G999G990
column werkbak           format 9G999G990
column gewijzigd         format 9G999G990
column geen_resultaat    format 9G999G990

select rpad('OVERZICHT TELLINGEN VAN VERWERKTE ADRESSEN T.B.V. EXTERNE VALIDATIE',92)||
       to_char(sysdate,'dd-mm-yyyy hh24:mi')
from dual;

select 'Subset      = '||'&3'||chr(10)||
       'Relatierol  = '||'&4'||chr(10)||
       'Soort adres = '||'&5'
from dual;

set heading on

-- Totalen per uitspraakcode

break on verwerkt skip 1

select   '1' groep
,        case when eas.ind_verwerkt is null then 'X' else eas.ind_verwerkt end verwerkt
,        case when eas.uitspraakcode = 'V06' then 'V06 - Voldoende match'
              when eas.uitspraakcode = 'V08' then 'V08 - Huisnr wijkt af'
              when eas.uitspraakcode = 'A05' then 'A05 - PC/huisnr wijkt af'
              when eas.uitspraakcode = 'A08' then 'A08 - Adres mag niet geleverd'
              when eas.uitspraakcode = 'A13' then 'A13 - Naam wijkt af'
              when eas.uitspraakcode = 'A99' then 'A99 - Geen match'
                                             else 'xxx - geen uitspraak' end uitspraak_code_cendris
,        sum(case when eas.resultaat = 'OC' then 1 else null end) Correct
,        sum(case when eas.resultaat = 'OO' then 1 else null end) Onbekend
,        sum(case when eas.resultaat = 'OM' then 1 else null end) MN_adres_recenter
,        sum(case when eas.resultaat = 'B' then 1 else null end) Werkbak
,        sum(case when eas.resultaat = 'G' then 1 else null end) Gewijzigd
,        sum(case when eas.resultaat is null then 1 else null end) geen_resultaat
from     rle_ext_afgestemde_adressen eas
,        rle_ext_adr_subsets eat
where    eat.id = eas.eat_id
and      eat.subsetnaam = '&SUBSET'
and      eat.rol_codrol = '&ROL_CODROL'
and      eat.soort_adres = '&SOORT_ADRES'
group by case when eas.ind_verwerkt is null then 'X' else eas.ind_verwerkt end
,        case when eas.uitspraakcode = 'V06' then 'V06 - Voldoende match'
              when eas.uitspraakcode = 'V08' then 'V08 - Huisnr wijkt af'
              when eas.uitspraakcode = 'A05' then 'A05 - PC/huisnr wijkt af'
              when eas.uitspraakcode = 'A08' then 'A08 - Adres mag niet geleverd'
              when eas.uitspraakcode = 'A13' then 'A13 - Naam wijkt af'
              when eas.uitspraakcode = 'A99' then 'A99 - Geen match'
                                             else 'xxx - geen uitspraak' end
union all
select   '2' groep
,        null verwerkt
,        'Tot - Totalen' uitspraak_code_cendris
,        sum(case when eas.resultaat = 'OC' then 1 else null end) Correct
,        sum(case when eas.resultaat = 'OO' then 1 else null end) Onbekend
,        sum(case when eas.resultaat = 'OM' then 1 else null end) MN_adres_recenter
,        sum(case when eas.resultaat = 'B' then 1 else null end) Werkbak
,        sum(case when eas.resultaat = 'G' then 1 else null end) Gewijzigd
,        sum(case when eas.resultaat is null then 1 else null end) geen_resultaat
from     rle_ext_afgestemde_adressen eas
,        rle_ext_adr_subsets eat
where    eat.id = eas.eat_id
and      eat.subsetnaam = '&SUBSET'
and      eat.rol_codrol = '&ROL_CODROL'
and      eat.soort_adres = '&SOORT_ADRES'
union all
select   '3' groep
,        null verwerkt
,        'Tot - Aantal adressen =' uitspraak_code_cendris
,        count('1') Correct
,        null Onbekend
,        null MN_adres_recenter
,        null Werkbak
,        null Gewijzigd
,        null geen_resultaat
from     rle_ext_afgestemde_adressen eas
,        rle_ext_adr_subsets eat
where    eat.id = eas.eat_id
and      eat.subsetnaam = '&SUBSET'
and      eat.rol_codrol = '&ROL_CODROL'
and      eat.soort_adres = '&SOORT_ADRES'
order by 1,2,3;

-- Totalen per uitspraakcode en verhuiscode

break on verwerkt skip 1 on uitspraak_code_cendris

select   '1' groep
,        case when eas.ind_verwerkt is null then 'X' else eas.ind_verwerkt end verwerkt
,        case when eas.uitspraakcode = 'V06' then 'V06 - Voldoende match'
              when eas.uitspraakcode = 'V08' then 'V08 - Huisnr wijkt af'
              when eas.uitspraakcode = 'A05' then 'A05 - PC/huisnr wijkt af'
              when eas.uitspraakcode = 'A08' then 'A08 - Adres mag niet geleverd'
              when eas.uitspraakcode = 'A13' then 'A13 - Naam wijkt af'
              when eas.uitspraakcode = 'A99' then 'A99 - Geen match'
                                             else 'xxx - geen uitspraak' end uitspraak_code_cendris
,        eas.verhuiscode
,        sum(case when eas.resultaat = 'OC' then 1 else null end) Correct
,        sum(case when eas.resultaat = 'OO' then 1 else null end) Onbekend
,        sum(case when eas.resultaat = 'OM' then 1 else null end) MN_adres_recenter
,        sum(case when eas.resultaat = 'B' then 1 else null end) Werkbak
,        sum(case when eas.resultaat = 'G' then 1 else null end) Gewijzigd
,        sum(case when eas.resultaat is null then 1 else null end) geen_resultaat
from     rle_ext_afgestemde_adressen eas
,        rle_ext_adr_subsets eat
where    eat.id = eas.eat_id
and      eat.subsetnaam = '&SUBSET'
and      eat.rol_codrol = '&ROL_CODROL'
and      eat.soort_adres = '&SOORT_ADRES'
group by case when eas.ind_verwerkt is null then 'X' else eas.ind_verwerkt end
,        case when eas.uitspraakcode = 'V06' then 'V06 - Voldoende match'
              when eas.uitspraakcode = 'V08' then 'V08 - Huisnr wijkt af'
              when eas.uitspraakcode = 'A05' then 'A05 - PC/huisnr wijkt af'
              when eas.uitspraakcode = 'A08' then 'A08 - Adres mag niet geleverd'
              when eas.uitspraakcode = 'A13' then 'A13 - Naam wijkt af'
              when eas.uitspraakcode = 'A99' then 'A99 - Geen match'
                                             else 'xxx - geen uitspraak' end
         , eas.verhuiscode
union all
select   '2' groep
,        null verwerkt
,        'Tot - Totalen' uitspraak_code_cendris
,        '_' verhuiscode
,        sum(case when eas.resultaat = 'OC' then 1 else null end) Correct
,        sum(case when eas.resultaat = 'OO' then 1 else null end) Onbekend
,        sum(case when eas.resultaat = 'OM' then 1 else null end) MN_adres_recenter
,        sum(case when eas.resultaat = 'B' then 1 else null end) Werkbak
,        sum(case when eas.resultaat = 'G' then 1 else null end) Gewijzigd
,        sum(case when eas.resultaat is null then 1 else null end) geen_resultaat
from     rle_ext_afgestemde_adressen eas
,        rle_ext_adr_subsets eat
where    eat.id = eas.eat_id
and      eat.subsetnaam = '&SUBSET'
and      eat.rol_codrol = '&ROL_CODROL'
and      eat.soort_adres = '&SOORT_ADRES'
union all
select   '3' groep
,        null verwerkt
,        'Tot - Aantal adressen =' uitspraak_code_cendris
,        null verhuiscode
,        count('1') Correct
,        null Onbekend
,        null MN_adres_recenter
,        null Werkbak
,        null Gewijzigd
,        null geen_resultaat
from     rle_ext_afgestemde_adressen eas
,        rle_ext_adr_subsets eat
where    eat.id = eas.eat_id
and      eat.subsetnaam = '&SUBSET'
and      eat.rol_codrol = '&ROL_CODROL'
and      eat.soort_adres = '&SOORT_ADRES'
order by 1,2,3,4;

SPOOL OFF

REM - job_statussen bijwerken

INSERT INTO stm_job_statussen
(   script_naam
,   script_id
,   programma_naam
,   volgnummer
,   uitvoer_soort
,   uitvoer_waarde
,   datum_tijd_mutatie
,   gebruiker
)
VALUES
(   UPPER('&1')
,   &2
,   'RLE_AANM_TELLINGEN'
,   1
,   'S'
,   '0'
,   SYSDATE
,   USER
);

COMMIT;
