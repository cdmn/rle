delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GBA_NIEUW_VERWANT';
delete from stm_moduleparameters
    where mde_module = 'RLE_GBA_NIEUW_VERWANT';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GBA_NIEUW_VERWANT';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GBA_NIEUW_VERWANT';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GBA_NIEUW_VERWANT';
DELETE FROM stm_modules
    WHERE module = 'RLE_GBA_NIEUW_VERWANT';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GBA_NIEUW_VERWANT',1,'RLE',1
    ,'RLE0942: Toevoegen verwant vanuit GBA (Alleen aan te roepen door GBA!!!)','P'
    ,'RLE_GBA_NIEUW_VERWANT',''
    ,'A',''
    ,27);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIN_AANMELDINGSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_STATUS_GBA','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_AFNEMERS_INDICATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N4','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIN_SOFINUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N5','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIN_ANUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_CODE_ROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_NAAM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_VOORLETTERS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_VOORVOEGSELS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D10','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PID_GEBOORTEDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_GESLACHT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_STRAATNAAM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N13','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIN_HUISNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_TOEVOEGING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_POSTCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A16','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_WOONPLAATS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A17','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_LOCATIEOMSCHRIJVING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A18','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_LANDCODE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N19','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIN_PERSOONSNUMMER_DEELNEMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D20','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PID_INGANGSDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A21','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_VERWANTSCHAPSOORT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D22','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PID_EINDDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A23','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_NAAM_PARTNER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A24','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PIV_VVGSL_PARTNER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A25','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'POV_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A26','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'POV_FOUTMELDING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N27','RLE_GBA_NIEUW_VERWANT',1,'J'
    ,'PON_PERSOONSNUMMER','OUT');
