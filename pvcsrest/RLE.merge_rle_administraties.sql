merge into comp_rle.rle_administraties a
  using ( select 'BPFK' as "CODE"
               , 'Pensioenfonds Koopvaardij' as "OMSCHRIJVING"
               , 'J' as "IND_AFSTEMMEN_GBA_TGSTN"
               , 'P' as "TYPE"
         from   dual ) b
  on ( a.code = b.code )
  when not matched then
    insert( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values( b.code, b.omschrijving, b.ind_afstemmen_gba_tgstn, b.type )
  when matched then
    update
       set a.omschrijving = b.omschrijving, a.ind_afstemmen_gba_tgstn = b.ind_afstemmen_gba_tgstn, a.type = b.type
    ;

merge into comp_rle.rle_administraties a
  using ( select 'PME' as "CODE"
               , 'Pensioenfonds Metalektro' as "OMSCHRIJVING"
               , 'J' as "IND_AFSTEMMEN_GBA_TGSTN"
               , 'P' as "TYPE"
         from   dual ) b
  on ( a.code = b.code )
  when not matched then
    insert( code, omschrijving, ind_afstemmen_gba_tgstn, type )
    values( b.code, b.omschrijving, b.ind_afstemmen_gba_tgstn, b.type )
  when matched then
    update
       set a.omschrijving = b.omschrijving, a.ind_afstemmen_gba_tgstn = b.ind_afstemmen_gba_tgstn, a.type = b.type
    ;

commit ;