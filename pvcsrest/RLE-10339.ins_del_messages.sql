prompt Verwijder message als die al bestaat
delete from qms_message_text
    where msp_code = 'RLE-10339';
delete from qms_message_properties
    where code = 'RLE-10339';
prompt Insert qms_message_properties
INSERT INTO qms_message_properties
    (code, description, severity, logging_ind, suppr_wrng_ind, suppr_always_ind, constraint_name)
    VALUES
    ('RLE-10339','Een geboortedatum mag niet liggen na de begindatum van een bijhorende relatiekoppeling.','E'
    ,'N','N'
    ,'N','');
prompt Insert qms_message_txt
INSERT into qms_message_text
    (language, text, help_text, msp_code)
    values
    ('DUT','Een geboortedatum mag niet liggen na de begindatum van een bijhorende relatiekoppeling.',''
    ,'RLE-10339');
prompt Nog wel COMMIT uitvoeren !!!
