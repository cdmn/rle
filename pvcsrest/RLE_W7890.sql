/* Naam......: RLE_W7890.sql
** Release...: W7890
** Datum.....: 01-05-2012 17:12:26
** Notes.....: Gegenereerd door ItsTools Versie 4.0.2.3
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W7890', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste schema voor RLE.rle_financieel_nummers.atb
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_financieel_nummers.atb
@@ RLE.rle_financieel_nummers.atb
prompt RLE.rle_financieel_nummers_jn.atb
@@ RLE.rle_financieel_nummers_jn.atb
prompt RLE.rle_landen.atb
@@ RLE.rle_landen.atb
prompt RLE.rle_fnr_pk.con
@@ RLE.rle_fnr_pk.con
prompt RLE.rle_fnr_uk1.con
@@ RLE.rle_fnr_uk1.con
prompt RLE.rle_fnr_ck10.con
@@ RLE.rle_fnr_ck10.con
prompt RLE.rle_fnr_ck9.con
@@ RLE.rle_fnr_ck9.con
prompt RLE.rle_lnd_ck8.con
@@ RLE.rle_lnd_ck8.con
prompt RLE.rle_lnd_ck9.con
@@ RLE.rle_lnd_ck9.con
prompt RLE.rle_lnd_uk3.con
@@ RLE.rle_lnd_uk3.con
prompt RLE.rle_chk_iban_lnd.fnc
@@ RLE.rle_chk_iban_lnd.fnc
prompt RLE.rle_fnr_ar.trg
@@ RLE.rle_fnr_ar.trg
prompt RLE.rle_fnr_as.trg
@@ RLE.rle_fnr_as.trg
prompt RLE.rle_fnr_bri.trg
@@ RLE.rle_fnr_bri.trg
prompt RLE.rle_fnr_bru.trg
@@ RLE.rle_fnr_bru.trg
prompt RLE.rle_lnd_bru.trg
@@ RLE.rle_lnd_bru.trg
prompt RLE.rle_m29_finnum_sepa.prc
@@ RLE.rle_m29_finnum_sepa.prc
prompt RLE.rle_get_finnum_sepa.prc
@@ RLE.rle_get_finnum_sepa.prc

prompt RLE.rle_get_fnr_ai_sepa.prc
@@ RLE.rle_get_fnr_ai_sepa.prc


prompt RLE.rle_finnum_sepa.typ
@@ RLE.rle_finnum_sepa.typ
prompt RLE.rle_finnum_sepa_table.typ
@@ RLE.rle_finnum_sepa_table.typ
prompt RLE.rle_get_finnum_cca.fnc
@@ RLE.rle_get_finnum_cca.fnc


prompt RLE.grants_tbv_coda.sql
@@ RLE.grants_tbv_coda.sql


/* Switchen naar het juiste schema voor RLE.wijze_van_betaling.dom
*/
set define on
connect /@&&DBN

prompt RLE.wijze_van_betaling.dom
@@ RLE.wijze_van_betaling.dom

prompt RLE.insert_qms_messages.sql
@@ RLE.insert_qms_messages.sql

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W7890 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W7890 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
