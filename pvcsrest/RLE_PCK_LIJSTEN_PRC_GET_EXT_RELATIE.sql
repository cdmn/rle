delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'RLE',1
    ,'Ophalen externe relatie (r)','P'
    ,'RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',''
    ,'A',''
    ,11);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_EXTERN_NUMMER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_OPGEMAAKTE_NAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D3','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_RLE_DAT_BEGIN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_RLE_DAT_EINDE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_SOORT_ADRES','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_ADRES_REGEL_1','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_ADRES_REGEL_2','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_ADRES_REGEL_3','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_ADRES_REGEL_4','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_ADRES_REGEL_5','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_PCK_LIJSTEN.PRC_GET_EXT_RELATIE',1,'J'
    ,'P_ADRES_REGEL_6','OUT');
