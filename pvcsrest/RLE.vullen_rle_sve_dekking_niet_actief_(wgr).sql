merge into rle_standaard_voorwaarden sve1 using ( select 'DEKKING NIET ACTIEF (WGR)' code ,q'#3.2) Werkgevers zonder dekking (1.13)#' naam ,q'#Definitie:
Werkgevers met op gegeven peildatum een geldige Polis AO en heeft voor ��n of meer van de gegeven Dekkingsoorten:
- geen geldige Vastgestelde Dekking AO (gefiateerd en niet vervallen)
of
- een geldige Opschorting AO (gefiateerd, niet vervallen, beeindigings-fiatdatum leeg en dekkingscode is leeg of indien gevuld gelijk aan de vastgestelde dekking)
Parameters:
1		Datum
2		Lijst met Dekkingsoorten (REG DEKKINGSOORT.Code)#' beschrijving ,q'#select 1
from   rle_relatie_externe_codes rec2
where  rec2.rle_numrelatie      = :NUMRELATIE
and    rec2.dwe_wrddom_extsys   = 'WGR'
and    rec2.rol_codrol          = 'WG'
and    exists ( select 'x' from aos_polissen_ao pao 
                where  pao.wgr_werkgnr         = rec2.extern_relatie
                and    pao.fiatteur_polis      is not null
                and    pao.fiatteur_annulering is null
                and    :VAR2 between pao.ingangsdatum_dekking
                                 and nvl(pao.einddatum_premie,:VAR2)
              )
and   (     
            ( not exists ( select 'x'
                           from aos_polissen_ao               pao 
                           ,    aos_vastgestelde_dekkingen_ao vdo 
                           where vdo.pgo_pao_id = pao.id
                           and   pao.wgr_werkgnr = rec2.extern_relatie
                           and    :VAR2 between vdo.ingangsdatum
                                            and nvl(vdo.einddatum,:VAR2)
                           and    vdo.pgo_dks_code        in ( :VAR1 )
                           and    pao.fiatteur_polis      is not null
                           and    pao.fiatteur_annulering is null
                           and    :VAR2 between pao.ingangsdatum_dekking
                                            and nvl(pao.einddatum_premie,:VAR2)
                           and    vdo.vervalmoment        is null
                           and    vdo.fiatteur            is not null
                          )
            )
       or                                      
            (  exists  ( select 'x'
                         from    aos_opschortingen_ao osg
                         ,       aos_polissen_ao      pao 
                         where   osg.pao_id                = pao.id
                         and     pao.wgr_werkgnr           = rec2.extern_relatie
                         and     osg.fiatdatum             is not null
                         and     osg.fiatdatum_beeindigen  is null
                         and     osg.datum_vervallen       is null
                         and     :VAR2 between osg.ingangsdatum and nvl(osg.einddatum,:VAR2)
                         and    (    osg.pgo_dks_code  is null
                                  or osg.pgo_dks_code  in ( :VAR1 )
                                 )                                          
                       ) 
            )
      )#' sqltext ,q'#Dekking (b.v. WBO,WHI )#' var1_naam ,'A' var1_datatype ,q'#Peildatum (DD-MM-YYYY)#' var2_naam ,'D' var2_datatype ,q'##' var3_naam ,'' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$PAS' creatie_door ,to_date('20-11-2009 16:33:05','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('09-12-2009 11:10:49','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
