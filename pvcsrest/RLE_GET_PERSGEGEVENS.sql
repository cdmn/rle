delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_PERSGEGEVENS';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_PERSGEGEVENS';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_PERSGEGEVENS';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_PERSGEGEVENS';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_PERSGEGEVENS';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_PERSGEGEVENS';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_PERSGEGEVENS',1,'RLE',1
    ,'Ophalen persoon','P'
    ,'RLE_GET_PERSGEGEVENS',''
    ,'A',''
    ,28);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A24','RLE_GET_PERSGEGEVENS',1,'N'
    ,'P_SOFINUMMER','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A25','RLE_GET_PERSGEGEVENS',1,'N'
    ,'P_NAAM_PARTNER','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A26','RLE_GET_PERSGEGEVENS',1,'N'
    ,'P_VOORVOEGSEL_PARTNER','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_NAMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_INDINSTELLING','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_COD_GESLACHT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_OMS_GESLACHT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GET_PERSGEGEVENS',1,'N'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_COD_VVGSL','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_OMS_VVGSL','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_VOORLETTER','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A9','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_VOORNAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D10','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_DATGEBOORTE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A11','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_COD_DATGEB_FICTIEF','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A12','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_OMS_DATGEB_FICTIEF','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A13','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_INDCONTROLE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D14','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_DATCONTROLE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_COD_NAAMGEBRUIK','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A16','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_OMS_NAAMGEBRUIK','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A17','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_COD_AANSCHRIJFTITEL','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A18','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_OMS_AANSCHRIJFTITEL','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D19','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_DATOVERLIJDEN','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D20','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_REGDATUM_OVERLBEVEST','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D21','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_DATEMIGRATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D22','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_DAT_CREATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A23','RLE_GET_PERSGEGEVENS',1,'J'
    ,'P_CREATIE_DOOR','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A27','RLE_GET_PERSGEGEVENS',1,'N'
    ,'P_PERSOONSNR','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A28','RLE_GET_PERSGEGEVENS',1,'N'
    ,'P_IND_PORTAL','INP');
