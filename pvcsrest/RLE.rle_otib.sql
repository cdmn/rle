delete      comp_rle.rle_bestandsabonnementen;

delete      comp_rle.rle_uitvoer_statussen;

delete      comp_rle.rle_bestanden;

insert into comp_rle.rle_bestanden
            (naam
            ,db_directory
            ,bsd_type
            ,formaat_naam
            ,bsd_groep)
values      ('OTIB_WGR'
            ,'RLE_OUTBOX'
            ,'WGR'
            ,'WGR_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.dat'
            ,'OTIB');
insert into comp_rle.rle_bestanden
            (naam
            ,db_directory
            ,bsd_type
            ,formaat_naam
            ,bsd_groep)
values      ('OTIB_WNR'
            ,'RLE_OUTBOX'
            ,'WNR'
            ,'WNR_<vanaf_datum>_<tot_datum>_<rowcount>_<jobid>.dat'
            ,'OTIB');
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WGR'
            ,'OTIB_L'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WGR'
            ,'OTIB_K'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WGR'
            ,'OTIB_E'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WGR'
            ,'SKO'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WGR'
            ,'OLC'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WGR'
            ,'OFE'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
--
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WNR'
            ,'OTIB_L'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WNR'
            ,'OTIB_K'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WNR'
            ,'OTIB_E'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WNR'
            ,'SKO'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WNR'
            ,'OLC'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
insert into comp_rle.rle_bestandsabonnementen
            (bsd_naam
            ,code_abonnee
            ,dat_begin
            ,dat_einde)
values      ('OTIB_WNR'
            ,'OFE'
            ,to_date ('01-01-1992', 'DD-MM-YYYY')
            ,null);
--
commit ;
