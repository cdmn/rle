/* Naam......: RLE_W9241.sql
** Release...: W9241
** Datum.....: 16-06-2014 19:59:03
** Notes.....: Gegenereerd door ItsTools Versie 4.1.0.0
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W9241', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste schema voor RLE.rle_bron_ex_partners.atb
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR continue
prompt RLE.rle_bron_ex_partners.atb
@@ RLE.rle_bron_ex_partners.atb
WHENEVER SQLERROR EXIT failure rollback

prompt RLE.rle_verwerk_ex_partners.prc
@@ RLE.rle_verwerk_ex_partners.prc
prompt RLE.rle_chk_voorletter_geldig.fnc
@@ RLE.rle_chk_voorletter_geldig.fnc
prompt RLE.rle_aanmaken_gbavbatch.pks
@@ RLE.rle_aanmaken_gbavbatch.pks
prompt RLE.rle_chk_input_ex.pks
@@ RLE.rle_chk_input_ex.pks
prompt RLE.rle_ex_partners.pks
@@ RLE.rle_ex_partners.pks
prompt RLE.rle_verw_geg_exp_gbav.pks
@@ RLE.rle_verw_geg_exp_gbav.pks
prompt RLE.rle_verw_xml_huw_exen.pks
@@ RLE.rle_verw_xml_huw_exen.pks
prompt RLE.rle_vul_bron_ex.pks
@@ RLE.rle_vul_bron_ex.pks
prompt RLE.rle_aanmaken_gbavbatch.pkb
@@ RLE.rle_aanmaken_gbavbatch.pkb
prompt RLE.rle_chk_input_ex.pkb
@@ RLE.rle_chk_input_ex.pkb
prompt RLE.rle_ex_partners.pkb
@@ RLE.rle_ex_partners.pkb
prompt RLE.rle_verw_geg_exp_gbav.pkb
@@ RLE.rle_verw_geg_exp_gbav.pkb
prompt RLE.rle_verw_xml_huw_exen.pkb
@@ RLE.rle_verw_xml_huw_exen.pkb
prompt RLE.rle_vul_bron_ex.pkb
@@ RLE.rle_vul_bron_ex.pkb
prompt RLE.rle_vul_bron_ex.pkb
@@ RLE.rle_vul_bron_ex.pkb
prompt RLE.rle_chk_input_ex.pkb
@@ RLE.rle_chk_input_ex.pkb
prompt RLE.rle_aanmaken_gbavbatch.pkb
@@ RLE.rle_aanmaken_gbavbatch.pkb
set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W9241 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W9241 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
