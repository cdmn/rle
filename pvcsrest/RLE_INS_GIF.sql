delete from stm_gebruik_interfaces where mde_module like 'RLE%';
commit;

prompt gebruik van ONDH_CONTACTPERSONEN door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_CONTACTPERSONEN')
,1
,'RLE0110F'
,1
);

prompt gebruik van SEL_SOORT_VERWANTSCHAP door RLE0270F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_SOORT_VERWANTSCHAP')
,1
,'RLE0270F'
,1
);

prompt gebruik van SEL_REF_SAD door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_SAD')
,1
,'RLE0340F'
,1
);

prompt gebruik van SEL_REF_SAD door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_SAD')
,1
,'RLE0110F'
,1
);

prompt gebruik van SEL_REF_SAD door RLE0040F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_SAD')
,1
,'RLE0040F'
,1
);

prompt gebruik van SEL_REF_SC door RLE0050F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_SC')
,1
,'RLE0050F'
,1
);

prompt gebruik van SEL_REF_SR door RLE0080F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_SR')
,1
,'RLE0080F'
,1
);

prompt gebruik van SEL_REF_FGD door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_FGD')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_FGD door RLE0060F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_FGD')
,1
,'RLE0060F'
,1
);

prompt gebruik van SEL_REF_VSB door RLE0050F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_VSB')
,1
,'RLE0050F'
,1
);

prompt gebruik van SEL_REF_CES door RLE0090F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_CES')
,1
,'RLE0090F'
,1
);

prompt gebruik van ONDH_WERKGEVER door RLE0270F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_WERKGEVER')
,1
,'RLE0270F'
,1
);

prompt gebruik van ONDH_WERKGEVER door RLE0010F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_WERKGEVER')
,1
,'RLE0010F'
,1
);

prompt gebruik van SEL_REF_GES door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_GES')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_GES door RLE0060F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_GES')
,1
,'RLE0060F'
,1
);

prompt gebruik van SEL_REF_GES door RLE0025F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_GES')
,1
,'RLE0025F'
,1
);

prompt gebruik van SEL_REF_GWT door RLE0065F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_GWT')
,1
,'RLE0065F'
,1
);

prompt gebruik van SEL_REF_GWT door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_GWT')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_GWT door RLE0060F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_GWT')
,1
,'RLE0060F'
,1
);

prompt gebruik van SEL_REF_REV door RLE0065F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_REV')
,1
,'RLE0065F'
,1
);

prompt gebruik van SEL_REF_VVL door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_VVL')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_VVL door RLE0060F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_VVL')
,1
,'RLE0060F'
,1
);

prompt gebruik van SEL_REF_BST door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_BST')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_BST door RLE0060F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_BST')
,1
,'RLE0060F'
,1
);

prompt gebruik van SEL_REF_BST door RLE0025F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_BST')
,1
,'RLE0025F'
,1
);

prompt gebruik van SELECTEREN_CURATOR door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_CURATOR')
,1
,'RLE0340F'
,1
);

prompt gebruik van SELECTEREN_ADMINKANTOOR door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_ADMINKANTOOR')
,1
,'RLE0340F'
,1
);

prompt gebruik van SEL_LAND door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_LAND')
,1
,'RLE0110F'
,1
);

prompt gebruik van SEL_LAND door RLE0080F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_LAND')
,1
,'RLE0080F'
,1
);

prompt gebruik van SEL_POSTCODE door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_POSTCODE')
,1
,'RLE0110F'
,1
);

prompt gebruik van ONDH_REKENING door RLE0300F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_REKENING')
,1
,'RLE0300F'
,1
);

prompt gebruik van ONDH_REKENING door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_REKENING')
,1
,'RLE0061F'
,1
);

prompt gebruik van ONDH_ADRES door RLE0300F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_ADRES')
,1
,'RLE0300F'
,1
);

prompt gebruik van ONDH_ADRES door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_ADRES')
,1
,'RLE0061F'
,1
);

prompt gebruik van ONDH_COMMUNICATIENUMMER door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_COMMUNICATIENUMMER')
,1
,'RLE0061F'
,1
);

prompt gebruik van ONDH_RELATIE door RLE0270F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_RELATIE')
,1
,'RLE0270F'
,1
);

prompt gebruik van ONDH_KANAALVOORKEUR door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_KANAALVOORKEUR')
,1
,'RLE0061F'
,1
);

prompt gebruik van SELECTEREN_PERSOON door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_PERSOON')
,1
,'RLE0340F'
,1
);

prompt gebruik van SELECTEREN_PERSOON door RLE0310F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_PERSOON')
,1
,'RLE0310F'
,1
);

prompt gebruik van SELECTEREN_PERSOON door RLE0290F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_PERSOON')
,1
,'RLE0290F'
,1
);

prompt gebruik van SELECTEREN_PERSOON door RLE0270F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_PERSOON')
,1
,'RLE0270F'
,1
);

prompt gebruik van SELECTEREN_PERSOON door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_PERSOON')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_KVR door RLE0260F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_KVR')
,1
,'RLE0260F'
,1
);

prompt gebruik van ONDH_VERWANTSCHAPPEN door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_VERWANTSCHAPPEN')
,1
,'RLE0061F'
,1
);

prompt gebruik van SELECTEREN_BEWINDVOERDER door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_BEWINDVOERDER')
,1
,'RLE0340F'
,1
);

prompt gebruik van SELECTEREN_BEWINDVOERDER door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_BEWINDVOERDER')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_AVV door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_AVV')
,1
,'RLE0110F'
,1
);

prompt gebruik van SEL_REF_ADI door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_ADI')
,1
,'RLE0110F'
,1
);

prompt gebruik van SELECTEREN_RELATIE door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_RELATIE')
,1
,'RLE0340F'
,1
);

prompt gebruik van SELECTEREN_RELATIE door RLE0300F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_RELATIE')
,1
,'RLE0300F'
,1
);

prompt gebruik van SELECTEREN_RELATIE door RLE0290F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_RELATIE')
,1
,'RLE0290F'
,1
);

prompt gebruik van SELECTEREN_RELATIE door RLE0270F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_RELATIE')
,1
,'RLE0270F'
,1
);

prompt gebruik van SEL_REF_DRG door RLE0080F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_DRG')
,1
,'RLE0080F'
,1
);

prompt gebruik van SEL_REF_ANG door RLE0065F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_ANG')
,1
,'RLE0065F'
,1
);

prompt gebruik van SEL_REF_ANG door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_ANG')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_REF_ANG door RLE0060F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_ANG')
,1
,'RLE0060F'
,1
);

prompt gebruik van ONDH_GEMOEDSBEZWAARDHEDEN door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_GEMOEDSBEZWAARDHEDEN')
,1
,'RLE0061F'
,1
);

prompt gebruik van SELECTEREN_SRTADR_PER_ROL door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_SRTADR_PER_ROL')
,1
,'RLE0340F'
,1
);

prompt gebruik van SELECTEREN_SRTADR_PER_ROL door RLE0110F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SELECTEREN_SRTADR_PER_ROL')
,1
,'RLE0110F'
,1
);

prompt gebruik van GBA_AANMAKEN_ADHOC door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'GBA_AANMAKEN_ADHOC')
,1
,'RLE0061F'
,1
);

prompt gebruik van GBA_AANMAKEN_PLAATSING door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'GBA_AANMAKEN_PLAATSING')
,1
,'RLE0061F'
,1
);

prompt gebruik van GBA_AANMAKEN_VERWIJDER door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'GBA_AANMAKEN_VERWIJDER')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_SOORT_EXTERNE_RELATIE door RLE0270F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_SOORT_EXTERNE_RELATIE')
,1
,'RLE0270F'
,1
);

prompt gebruik van ONDH_OVERIGE_ROLLEN door RLE0270F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_OVERIGE_ROLLEN')
,1
,'RLE0270F'
,1
);

prompt gebruik van ONDH_VERZENDBESTEMMING door RLE0061F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'ONDH_VERZENDBESTEMMING')
,1
,'RLE0061F'
,1
);

prompt gebruik van SEL_ROL_VERZEND door RLE0340F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_ROL_VERZEND')
,1
,'RLE0340F'
,1
);

prompt gebruik van SEL_REF_DLR door RLE0080F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'SEL_REF_DLR')
,1
,'RLE0080F'
,1
);

prompt gebruik van RAADPLEGEN_AVG_BIJ_PERSOON door RLE0310F
insert into stm_gebruik_interfaces
(ifv_ife_id
,ifv_numversie
,mde_module
,mde_numversie
)
values (
(select id from stm_interfaces where codzoek = 'RAADPLEGEN_AVG_BIJ_PERSOON')
,1
,'RLE0310F'
,1
);

commit;

