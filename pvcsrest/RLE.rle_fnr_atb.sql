whenever sqlerror continue

alter table rle_financieel_nummers
drop constraint rle_fnr_pk
/

DROP INDEX COMP_RLE.RLE_FNR_PK;

alter table rle_financieel_nummers
add ( id number )
/
-- id wordt de PK, van de oude PK maken we een UK
alter table rle_financieel_nummers
 add (constraint rle_fnr_uk2 unique 
  (datingang
  ,numrekening
  ,rle_numrelatie
  ,dwe_wrddom_srtrek
  ,coddoelrek))
/
-- vul het ID
alter table rle_financieel_nummers disable all triggers
/
update rle_financieel_nummers
set id = rownum
/
alter table rle_financieel_nummers enable all triggers
/
-- en maak er de PK van
alter table rle_financieel_nummers
 add (constraint rle_fnr_pk primary key 
  (id))
/
create sequence rle_fnr_seq1
 nomaxvalue
 nominvalue
 nocycle
/
create or replace public synonym rle_fnr_seq1 for rle_fnr_seq1
/
-- deze krijgt een sequence, en die zetten we gelijk even goed
declare
   i number;
   j number;
begin
select nvl(max(id),0) into i from rle_financieel_nummers;
loop
  select rle_fnr_seq1.nextval into j from dual;
  exit when j >= i;
end loop;
end;
/


