prompt stm_moduleparameters
delete from stm_moduleparameters
    where mde_module = 'RLE0300F';
--
prompt stm_modulegegevens
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0300F';
--
prompt stm_modules
BEGIN
 INSERT INTO stm_modules
      (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
     VALUES
      ('RLE0300F',1,'RLE',1
      ,'Onderhouden externe relaties','S'
      ,'RLE0300F',''
      ,'A',''
      ,null);
EXCEPTION
  WHEN dup_val_on_index THEN NULL;
END;
/
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_RKG','RLE0300F',1,'N'
    ,'Rol in koppeling','IN');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_REL_VR','RLE0300F',1,'N'
    ,'Relatienr voor','IN');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0300F',1,'J'
    ,'Relatienr','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('EXT_NUMMER','RLE0300F',1,'J'
    ,'Extern nummer','OUT');
prompt stm_modulegegevens
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0300F',1,'CODE_RKG',
        ':GLOBAL.CG$CODE_ROL_IN_KOPPELING');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0300F',1,'NR_REL_VR',
        ':GLOBAL.CG$NR_RELATIE_VOOR');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0300F',1,'EXT_NUMMER',
        ':RLE.EXTERN_NUMMER');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0300F',1,'CODE_ROL',
        ':RLE.COD_SOORT_RELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0300F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
