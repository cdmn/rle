/* Naam......: RLE_W6808.sql
** Release...: W6808
** Datum.....: Wed Dec  1 16:32:26 2010
** Notes.....: Aangemaakt met Des_tool_mn Versie 3.1.4.g.palm 


*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/

col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
                    ,'continue') P_EXIT
from dual;


WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C en RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    into dummy
    from dba_users
   where username = 'COMP_RLE';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
  select 'X'
    into dummy
    from dba_tablespaces
   where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE bestaat niet in deze database,
*          neem contact op met PD-if/bo/Oracle !');
end;
/

/* Versie-nummer van de oplevering wordt bij gehouden in de tabel STM_VERSIE_WIJZIGINGEN
   Voorlopig is er ook nog een synonym IMU_APPLICATIE_VERSIE die naar deze tabel verwijst.
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W6808', sysdate );

commit;


/* Cross grants tussen de SCHEMA-users onderling deel 1
*/
REM start RLE_6808_grants1.sql

/* Switchen naar het juiste schema
*/
connect /@&&DBN
set define on
set verify off
define _password = "SU_PASSWORD"
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT


REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
REM Hier de files zetten die vanuit deze directory aangeroepen moeten worden.

@@ RLE.rle_v_zoek_werknemer.vw
--
@@ RLE.drop_rle_mv_zoek_werknemer_1.sql
@@ RLE.rle_mv_zoek_werknemer_1.vw
@@ RLE.rle_mv_zoek_werknemer_syn.syn
--
@@ RLE.rle_zwn1_ind1.ind
@@ RLE.rle_zwn1_ind10.ind
@@ RLE.rle_zwn1_ind11.ind
@@ RLE.rle_zwn1_ind12.ind
@@ RLE.rle_zwn1_ind2.ind
@@ RLE.rle_zwn1_ind3.ind
@@ RLE.rle_zwn1_ind4.ind
@@ RLE.rle_zwn1_ind5.ind
@@ RLE.rle_zwn1_ind6.ind
@@ RLE.rle_zwn1_ind7.ind
@@ RLE.rle_zwn1_ind8.ind
@@ RLE.rle_zwn1_ind9.ind
--
@@ RLE.drop_rle_mv_zoek_werknemer_2.sql
@@ RLE.rle_mv_zoek_werknemer_2.vw
--
@@ RLE.rle_zwn2_ind1.ind
@@ RLE.rle_zwn2_ind10.ind
@@ RLE.rle_zwn2_ind11.ind
@@ RLE.rle_zwn2_ind12.ind
@@ RLE.rle_zwn2_ind2.ind
@@ RLE.rle_zwn2_ind3.ind
@@ RLE.rle_zwn2_ind4.ind
@@ RLE.rle_zwn2_ind5.ind
@@ RLE.rle_zwn2_ind6.ind
@@ RLE.rle_zwn2_ind7.ind
@@ RLE.rle_zwn2_ind8.ind
@@ RLE.rle_zwn2_ind9.ind
--
@@ RLE.rle_v_personen_xl_portal.vw
prompt RLE.rle_v_relatierollen_in_adm_zc.vw
@@ RLE.rle_v_relatierollen_in_adm_zc.vw



prompt RLE.rle_grants_app_mns.sql
@@ RLE.rle_grants_app_mns.sql
prompt RLE.rle_grants_comp_avg.sql
@@ RLE.rle_grants_comp_avg.sql





REM Tot zover de inhoud van de directory
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/* Cross grants tussen de SCHEMA-users onderling deel 2
*/
connect /@&&DBN
REM start RLE_6808_grants2.sql




/* creatie database-rollen
*/
connect /@&&DBN
REM start RLE.????.rol

/* RLE_REF_CODES vullen en check constraints plaatsen
*/
connect /@&&DBN
REM start RLE.????.dom




insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W6808 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W6808 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off
WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on

