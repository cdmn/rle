delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0040F';
delete from stm_moduleparameters
    where mde_module = 'RLE0040F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0040F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0040F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0040F';
DELETE FROM stm_modules
    WHERE module = 'RLE0040F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0040F',1,'RLE',1
    ,'Onderhouden relatieadressen','S'
    ,'RLE0040F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SRT_ADRES','RLE0040F',1,'N'
    ,'Adressoort','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0040F',1,'N'
    ,'Relatienr','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0040F',1,'N'
    ,'Rol','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('DAT_PEIL','RLE0040F',1,'N'
    ,'Peildatum','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0040F',1,'REF_SAD',
        ':QMS$CTRL.CGNBT_CHAR_FIELD44');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0040F',1,'NR_RELATIE',
        ':QMS$CTRL.CGNBT_CHAR_FIELD4');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0040F',1,'CODE_ROL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD42');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0040F',1,'DAT_PEIL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD43');
