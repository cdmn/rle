
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0270F';
--
-- nieuw
--
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0270F',1,'NR_WGR',
        ':RLE.LKP_PERS_WERKG_NUMMER');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0270F',1,'NR_WGR1',
        ':RKN.LKP_EXTERN_NUMMER');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0270F',1,'DAT_PEIL',
        ':GLOBAL.CG$DAT_PEIL');

--
-- bestaand
--
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0270F',1,'CODE_ROL',
        ':GLOBAL.CG$CODE_ROL');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0270F',1,'NR_RELATIE',
        ':RKN.RLE_NUMRELATIE_VOOR');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0270F',1,'SOFI_VERPL',
        ':GLOBAL.CG$SOFI_VERPLICHT');
