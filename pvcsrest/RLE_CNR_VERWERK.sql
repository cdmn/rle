delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_CNR_VERWERK';
delete from stm_moduleparameters
    where mde_module = 'RLE_CNR_VERWERK';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_CNR_VERWERK';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_CNR_VERWERK';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_CNR_VERWERK';
DELETE FROM stm_modules
    WHERE module = 'RLE_CNR_VERWERK';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_CNR_VERWERK',1,'RLE',1
    ,'Verwerken communicatienummer','S'
    ,'RLE_CNR_VERWERK',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_CNR_VERWERK',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_CNR_VERWERK',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_CNR_VERWERK',1,'J'
    ,'P_SRT_COM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_CNR_VERWERK',1,'J'
    ,'P_PEILDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_CNR_VERWERK',1,'J'
    ,'P_NUMCOMMUNICATIE','INP');
