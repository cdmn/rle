declare
  ln_id number(10) ;
  ln_ccn_id_pmt number(10);  

begin
  ln_ccn_id_pmt := null ;

  select id
  into   ln_ccn_id_pmt
  from   rle_communicatie_categorieen
  where  omschrijving = 'PMT E-Nieuwsbrief' 
  and    adm_code     = 'PMT'
  ;

  update rle_communicatie_cat_kanalen
  set    ind_default = 'N'
  where  ccn_id      = ln_ccn_id_pmt
  and    dwe_wrddom_srtcom = 'EMAIL'
  ;

  update rle_communicatie_cat_kanalen
  set    ind_default = 'J'
  where  ccn_id      = ln_ccn_id_pmt
  and    dwe_wrddom_srtcom = 'POST'
  ;
  
  insert into rle_communicatie_categorieen
  ( code
  , omschrijving
  , datum_begin
  , adm_code
  )
  values
  ( 'COM'
  , 'PME E-Nieuwsbrief'
  , to_date ( '01-01-2013', 'dd-mm-yyyy' )
  , 'PME'
  ) returning id into ln_id;
  
  insert into rle_communicatie_cat_kanalen
  ( ccn_id 
  , dwe_wrddom_srtcom
  , datum_begin
  , ind_default
  ) select 
   ln_id
  , 'NIETGEWENST'
  , to_date ( '01-01-2013', 'dd-mm-yyyy' )
  , 'N'
   from dual 
    where not exists 
    ( select 1
	  from rle_communicatie_cat_kanalen
	  where ccn_id = ln_id
	  and   dwe_wrddom_srtcom = 'NIETGEWENST'
	) ;  
  
  insert into rle_communicatie_cat_kanalen
  ( ccn_id 
  , dwe_wrddom_srtcom
  , datum_begin
  , ind_default
  ) select 
   ln_id
  , 'EMAIL'
  , to_date ( '01-01-2013', 'dd-mm-yyyy' )
  , 'N'
   from dual 
    where not exists 
    ( select 1
	  from rle_communicatie_cat_kanalen
	  where ccn_id = ln_id
	  and   dwe_wrddom_srtcom = 'EMAIL'
	) ;  
  
  insert into rle_communicatie_cat_kanalen
  ( ccn_id 
  , dwe_wrddom_srtcom
  , datum_begin
  , ind_default
  ) select 
    ln_id
  , 'POST'
  , to_date ( '01-01-2013', 'dd-mm-yyyy' )
  , 'J'
   from dual
     where not exists 
    ( select 1
      from rle_communicatie_cat_kanalen
      where ccn_id = ln_id
      and   dwe_wrddom_srtcom = 'POST'
    ) ; 

  commit;
exception
  when others 
    then null;
end ;
/

