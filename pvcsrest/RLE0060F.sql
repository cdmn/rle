delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0060F';
delete from stm_moduleparameters
    where mde_module = 'RLE0060F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0060F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0060F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0060F';
DELETE FROM stm_modules
    WHERE module = 'RLE0060F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0060F',1,'RLE',1
    ,'Onderhouden relaties persoon','S'
    ,'RLE0060F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('COD_ROL_R','RLE0060F',1,'N'
    ,'Code Rol Referentie','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('INDLEEG','RLE0060F',1,'N'
    ,'Leeg','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('SRT_ADRES','RLE0060F',1,'N'
    ,'Adressoort','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0060F',1,'N'
    ,'Relatienr','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0060F',1,'N'
    ,'Rol','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('DAT_PEIL','RLE0060F',1,'N'
    ,'Peildatum','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'SRT_ADRES',
        ':QMS$CTRL.CGNBT_CHAR_FIELD13');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'NAAMGEBR',
        ':RLE.CODE_AANDUIDING_NAAMGEBRUIK');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'CODE_LAND',
        ':RLE.CODNATIONALITEIT');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'COD_GEBDAT',
        ':RLE.CODE_GEBDAT_FICTIEF');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'REF_SAD',
        ':QMS$CTRL.CGNBT_CHAR_FIELD13');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'CODE_ROL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD1');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'DAT_PEIL',
        ':QMS$CTRL.CGNBT_CHAR_FIELD12');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'COD_ROL_R',
        ':QMS$CTRL.CGNBT_CHAR_FIELD14');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'REF_GWT',
        ':RLE.DWE_WRDDOM_GEWATITEL');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'REF_GES',
        ':RLE.DWE_WRDDOM_GESLACHT');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'REF_BST',
        ':RLE.DWE_WRDDOM_BRGST');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0060F',1,'REF_VVL',
        ':RLE.DWE_WRDDOM_VVGSL');
