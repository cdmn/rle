delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0080F';
delete from stm_moduleparameters
    where mde_module = 'RLE0080F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0080F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0080F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0080F';
DELETE FROM stm_modules
    WHERE module = 'RLE0080F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0080F',1,'RLE',1
    ,'Onderhouden Rekeningen','S'
    ,'RLE0080F',''
    ,'A',''
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('IND_GEWIJZ','RLE0080F',1,'N'
    ,'Is er iets gemuteerd','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0080F',1,'N'
    ,'Rol','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0080F',1,'N'
    ,'Relatienr','MOD');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0080F',1,'REF_DRG',
        ':FNR.CODDOELREK');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0080F',1,'CODE_LAND',
        ':FNR.LND_CODLAND');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0080F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0080F',1,'REF_SR',
        ':FNR.DWE_WRDDOM_SRTREK');
