delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GBD_TOEV';
delete from stm_moduleparameters
    where mde_module = 'RLE_GBD_TOEV';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GBD_TOEV';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GBD_TOEV';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GBD_TOEV';
DELETE FROM stm_modules
    WHERE module = 'RLE_GBD_TOEV';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GBD_TOEV',1,'RLE',1
    ,'I_106: Vastleggen gemoedsbezwaardheid','P'
    ,'RLE_GBD_TOEV',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GBD_TOEV',1,'J'
    ,'PIN_NR_RELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GBD_TOEV',1,'J'
    ,'PIN_NR_PERSOON','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D3','RLE_GBD_TOEV',1,'J'
    ,'PID_DAT_BEGIN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GBD_TOEV',1,'J'
    ,'POV_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GBD_TOEV',1,'J'
    ,'POV_FOUTMELDING','OUT');
