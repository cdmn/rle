INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10341', 'Een geboortedatum mag niet voor 01-01-1875 liggen.'
            , 'E', 'N', 'N', 'N', 'RLE_RLE_CK19');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10341', 'DUT'
            , 'Een geboortedatum mag niet voor 01-01-1875 liggen.'
            , '');

