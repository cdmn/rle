INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10334', 'Dit a-nummer (<p1>) is reeds gekoppeld aan een ander persoon.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10334', 'DUT'
            , 'Dit a-nummer (<p1>) is reeds gekoppeld aan een ander persoon.'
            , '');

