delete qms_message_text where msp_code between 'RLE-00618' and 'RLE-00627';
delete qms_message_properties where code between 'RLE-00618' and 'RLE-00627';

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00618', 'Relatie bestaat niet.', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Relatie bestaat niet', 'RLE-00618');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00619', 'Ongeldig achtervoegsel', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldig achtervoegsel', 'RLE-00619');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00620', 'Ongeldige burgerlijke staat', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldige burgerlijke staat', 'RLE-00620');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00621', 'Ongeldige code naamgebruik', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldige code naamgebruik', 'RLE-00621');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00622', 'Ongeldig geslacht (M/V/O)', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldig geslacht (M/V/O)', 'RLE-00622');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00623', 'Ongeldige gewenste aanschrijftitel', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldige gewenste aanschrijftitel', 'RLE-00623');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00624', 'Ongeldige titel', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldige titel', 'RLE-00624');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00625', 'Ongeldige tussentitel', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldige tussentitel', 'RLE-00625');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00626', 'Ongeldige voorvoegsels', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldige voorvoegsels', 'RLE-00626');

Insert into qms_message_properties
  (CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND)
 Values
   ('RLE-00627', 'Ongeldige voorvoegsels partner', 'E', 'N', 'N', 'N');

Insert into qms_message_text
   (LANGUAGE, TEXT, MSP_CODE)
 Values
   ('DUT', 'Ongeldige voorvoegsels partner', 'RLE-00627');


