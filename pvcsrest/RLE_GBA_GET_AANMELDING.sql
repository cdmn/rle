delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GBA_GET_AANMELDING';
delete from stm_moduleparameters
    where mde_module = 'RLE_GBA_GET_AANMELDING';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GBA_GET_AANMELDING';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GBA_GET_AANMELDING';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GBA_GET_AANMELDING';
DELETE FROM stm_modules
    WHERE module = 'RLE_GBA_GET_AANMELDING';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GBA_GET_AANMELDING',1,'RLE',1
    ,'I_095: Ophalen aanmeldingsnummer','P'
    ,'RLE_GBA_GET_AANMELDING',''
    ,'A',''
    ,3);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GBA_GET_AANMELDING',1,'J'
    ,'PIN_PERSOONSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GBA_GET_AANMELDING',1,'J'
    ,'PIN_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_GBA_GET_AANMELDING',1,'J'
    ,'PON_AANMELDINGSNUMMER','OUT');
