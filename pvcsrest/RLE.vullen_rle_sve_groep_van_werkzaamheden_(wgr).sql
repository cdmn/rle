merge into rle_standaard_voorwaarden sve1 using ( select 'GROEP VAN WERKZAAMHEDEN (WGR)' code ,q'#10.1) Werkgevers met groep van werkzaamheden binnen range (1.23)#' naam ,q'#Definitie:
Werkgevers met op gegeven peildatum als activiteit een geldige Werkzaamheid met de gegeven Code Groep van Werkzaamheden en met, indien kode subgroep is gevuld, gegeven Code Subgroep van Werkzaamheden.
Parameters:
1	Datum
2	Groep van Werkzaamheden (WSF GROEP VAN WERKZAAMHEDEN.Kode Groep)
3	Subgroep van Werkzaamheden, optioneel (WSF SUBGROEP VAN WERKZAAMHEDEN.Kode Subgroep)#' beschrijving ,q'#select 1
from   wgr_werkzaamheden         wzd
,      wgr_werkgevers            wgr
,      rle_relatie_externe_codes rec
where  wzd.code_groep_werkzhd    in ( :VAR1 )
and    nvl(wzd.code_subgroep_werkzhd,'-99') = nvl(:VAR2,nvl(wzd.code_subgroep_werkzhd,'-99'))
and    rec.rle_numrelatie        = :NUMRELATIE
and    rec.rol_codrol            = 'WG'
and    rec.dwe_wrddom_extsys     = 'WGR'
and    wgr.nr_werkgever          = rec.extern_relatie
and    wgr.id                    = wzd.wgr_id
and    wzd.dat_begin            <= :VAR3
and    nvl(wzd.dat_einde,:VAR3) >= :VAR3#' sqltext ,q'#Groep van werkzaamheden (b.v. 15,16 )#' var1_naam ,'A' var1_datatype ,q'#Subgroep van werkzaamheden (b.v. 1 )#' var2_naam ,'C' var2_datatype ,q'#Peildatum (DD-MM-YYYY)#' var3_naam ,'D' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'OPS$PAS' creatie_door ,to_date('20-11-2009 16:33:05','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('09-12-2009 11:11:27','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
