delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_ROL_GETOMS';
delete from stm_moduleparameters
    where mde_module = 'RLE_ROL_GETOMS';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_ROL_GETOMS';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_ROL_GETOMS';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_ROL_GETOMS';
DELETE FROM stm_modules
    WHERE module = 'RLE_ROL_GETOMS';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_ROL_GETOMS',1,'RLE',1
    ,'Ophalen omschrijving rol','F'
    ,'RLE_ROL_GETOMS',''
    ,'A',''
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_ROL_GETOMS',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_ROL_GETOMS',1,'J'
    ,'RETURNVALUE','OUT');
