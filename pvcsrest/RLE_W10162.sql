/* Naam......: RLE_W10162.sql
** Release...: W10162
** Datum.....: 22-07-2015 17:11:41
** Notes.....: Gegenereerd door ItsTools Versie 4.2.1.0
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W10162', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste 11g schema voor RLE.rle_rle_chk_adres.prc
*/
set define on
connect /@&&DBN
@su COMP_RLE

prompt RLE.rle_stamdata_rfc385543.sql
@@ RLE.rle_stamdata_rfc385543.sql
prompt RLE.rle_rle_chk_adres.prc
@@ RLE.rle_rle_chk_adres.prc
prompt RLE.rle_get_extsys_voor_codrol.fnc
@@ RLE.rle_get_extsys_voor_codrol.fnc
/* Switchen naar het juiste 11g schema voor RLE0111F.rol
*/
set define on
connect /@&&DBN

prompt RLE0111F.rol
@@ RLE0111F.rol
prompt RLE0301F.rol
@@ RLE0301F.rol
prompt RLE0302F.rol
@@ RLE0302F.rol
prompt RLE.communicatie_kanaal.dom
@@ RLE.communicatie_kanaal.dom
prompt RLE.extern_systeem.dom
@@ RLE.extern_systeem.dom

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W10162 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W10162 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
