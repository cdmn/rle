merge into rle_standaard_voorwaarden sve1 using ( select 'LOONSOM MIN (WGR)' code ,q'#11.2) Werkgevers met minimum loonsom#' naam ,q'#Definitie:
Werkgevers waarvan op gegeven peildatum de som van de Jaarsalarissen die worden verdiend door  de Werknemers groter of gelijk is dan de opgegeven loonsom.
Parameters:
1	Datum
2	Loonsom
3      Code inkomenscomponent#' beschrijving ,q'#select 1
from   wgr_werkgevers wgr
     , avg_werknemer_inkomens wni
     , avg_arbeidsverhoudingen avg
where  wgr.nr_relatie = :NUMRELATIE
and    wgr.nr_relatie = avg.rle_numrelatie_wg(+)
and    wni.avh_id(+) = avg.id
and    wni.code_inkomenscomponent(+) in (:VAR1)
and    :VAR3 between wni.dat_begin(+) and nvl( wni.dat_einde(+)
                                          , :VAR3
                                          )
having round( sum( case
                     when wni.code_frequentie = 'M'
                       then wni.bedrag_basisvaluta * 12.96
                     when wni.code_frequentie = 'J'
                       then wni.bedrag_basisvaluta * 1.08
                     when wni.code_frequentie = 'W'
                       then wni.bedrag_basisvaluta * 56.36
                     when wni.code_frequentie = '4'
                       then wni.bedrag_basisvaluta * 14.09
                     else 0
                   end )
            , 0
            ) >= :VAR2#' sqltext ,q'#Code inkomenscomponent (b.v. SALI, PROV )#' var1_naam ,'A' var1_datatype ,q'#Loonsom minimum#' var2_naam ,'N' var2_datatype ,q'#Peildatum (DD-MM-YYYY)#' var3_naam ,'D' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'COMP_RLE' creatie_door ,to_date('15-12-2009 11:32:23','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'OPS$ORACLE' mutatie_door ,to_date('22-12-2009 10:56:06','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
