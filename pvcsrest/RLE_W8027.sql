/* Naam......: RLE_W8027.sql
** Release...: W8027
** Datum.....: 11-07-2012 14:57:00
** Notes.....: Gegenereerd door ItsTools Versie 4.0.2.5
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W8027', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste schema voor RLE.rle_contactpersonen_pma.sql
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_contactpersonen_pma.sql
@@ RLE.rle_contactpersonen_pma.sql
prompt RLE.rle_wgr.pks
@@ RLE.rle_wgr.pks
prompt RLE.rle_wgr.pkb
@@ RLE.rle_wgr.pkb
prompt RLE.rle_chk_email.fnc
@@ RLE.rle_chk_email.fnc
prompt RLE.rle_cnr_bru.trg
@@ RLE.rle_cnr_bru.trg
prompt RLE.rle_grants_chk_email.sql
@@ RLE.rle_grants_chk_email.sql
prompt RLE.rle_cnr_verwerk.prc
@@ RLE.rle_cnr_verwerk.prc
prompt RLE.rle_v_werkgevers_xl.vw
@@ RLE.rle_v_werkgevers_xl.vw
prompt RLE.rle_v_werkgevers_xl_portal.vw
@@ RLE.rle_v_werkgevers_xl_portal.vw



set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W8027 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W8027 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
