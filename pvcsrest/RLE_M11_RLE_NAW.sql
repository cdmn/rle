delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_M11_RLE_NAW';
delete from stm_moduleparameters
    where mde_module = 'RLE_M11_RLE_NAW';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_M11_RLE_NAW';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_M11_RLE_NAW';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_M11_RLE_NAW';
DELETE FROM stm_modules
    WHERE module = 'RLE_M11_RLE_NAW';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_M11_RLE_NAW',1,'RLE',1
    ,'Ophalen naam van een persoon','P'
    ,'RLE_M11_RLE_NAW',''
    ,'A',''
    ,5);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_M11_RLE_NAW',1,'J'
    ,'p_aantpos','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_M11_RLE_NAW',1,'J'
    ,'p_numrelatie','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_M11_RLE_NAW',1,'J'
    ,'p_opmaak','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_M11_RLE_NAW',1,'J'
    ,'p_melding','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_M11_RLE_NAW',1,'J'
    ,'p_cod_opmaak','INP');
