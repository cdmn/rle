delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0020F';
delete from stm_moduleparameters
    where mde_module = 'RLE0020F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0020F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0020F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0020F';
DELETE FROM stm_modules
    WHERE module = 'RLE0020F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0020F',1,'RLE',1
    ,'Wijzigen Persoon/Werknemer','S'
    ,'RLE0020F',''
    ,'A','Wijzigen persoon'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_SOFI','RLE0020F',1,'N'
    ,'Sofinummer','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0020F',1,'NR_RELATIE',
        ':RLE.NUMRELATIE');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0020F',1,'REF_SAD',
        ':RAS.DWE_WRDDOM_SRTADR');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0020F',1,'NUM_ADR_C',
        ':RAS1.ADS_NUMADRES');
