/*************************************************************************
***              Uitdelen COMP_RLE grants aan COMP_KCT                 ***
*************************************************************************/

prompt Uitdelen COMP_RLE grants aan COMP_KCT


GRANT REFERENCES ON RLE_RELATIES TO COMP_KCT
/

GRANT EXECUTE ON RLE_LIB TO COMP_KCT WITH GRANT OPTION
/

GRANT SELECT ON RLE_V_RELATIEROLLEN_IN_ADM_ZC TO COMP_KCT WITH GRANT OPTION
/