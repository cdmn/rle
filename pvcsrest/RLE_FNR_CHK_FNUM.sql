delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_FNR_CHK_FNUM';
delete from stm_moduleparameters
    where mde_module = 'RLE_FNR_CHK_FNUM';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_FNR_CHK_FNUM';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_FNR_CHK_FNUM';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_FNR_CHK_FNUM';
DELETE FROM stm_modules
    WHERE module = 'RLE_FNR_CHK_FNUM';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_FNR_CHK_FNUM',1,'RLE',1
    ,'Bestaat financieel nummer?','F'
    ,'RLE_FNR_CHK_FNUM',''
    ,'A',''
    ,6);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_FNR_CHK_FNUM',1,'J'
    ,'P_ROL_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_FNR_CHK_FNUM',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_FNR_CHK_FNUM',1,'J'
    ,'P_SRTREKENING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_FNR_CHK_FNUM',1,'J'
    ,'P_NUMREKENING','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D6','RLE_FNR_CHK_FNUM',1,'J'
    ,'P_PEILDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_FNR_CHK_FNUM',1,'J'
    ,'RETURNVALUE','OUT');
