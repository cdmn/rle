delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_CHK_WERKGEVER';
delete from stm_moduleparameters
    where mde_module = 'RLE_CHK_WERKGEVER';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_CHK_WERKGEVER';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_CHK_WERKGEVER';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_CHK_WERKGEVER';
DELETE FROM stm_modules
    WHERE module = 'RLE_CHK_WERKGEVER';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_CHK_WERKGEVER',1,'RLE',1
    ,'Controleren werkgever (I-090)','P'
    ,'RLE_CHK_WERKGEVER',''
    ,'A',''
    ,7);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_CHK_WERKGEVER',1,'J'
    ,'p_naam_werkgever','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_CHK_WERKGEVER',1,'N'
    ,'p_handelsnaam','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_CHK_WERKGEVER',1,'J'
    ,'p_postcode','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N4','RLE_CHK_WERKGEVER',1,'J'
    ,'p_huisnummer','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_CHK_WERKGEVER',1,'J'
    ,'p_ind_aanwezig','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N6','RLE_CHK_WERKGEVER',1,'J'
    ,'p_relatienummer','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_CHK_WERKGEVER',1,'J'
    ,'p_werkgeversnummer','OUT');
