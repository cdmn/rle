delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I103';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I103';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I103';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I103';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_GET_I103';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_GET_I103';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_GET_I103',1,'RLE',1
    ,'Ophalen rij','P'
    ,'RLE_PCK_LIJSTEN.PRC_GET_I103',''
    ,'A',''
    ,4);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_PCK_LIJSTEN.PRC_GET_I103',1,'J'
    ,'P_NUMRELATIE','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_PCK_LIJSTEN.PRC_GET_I103',1,'J'
    ,'P_NAAM','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_PCK_LIJSTEN.PRC_GET_I103',1,'J'
    ,'P_EXTERN_NR','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_PCK_LIJSTEN.PRC_GET_I103',1,'J'
    ,'P_TELNR','OUT');
