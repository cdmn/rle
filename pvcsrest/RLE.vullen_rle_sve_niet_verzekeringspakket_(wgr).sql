update rle_standaard_voorwaarden
set code = 'VERZEKERINGSPAKKET NIET ACTIEF (WGR)'
where code = 'NIET VERZEKERINGSPAKKET (WGR)';
merge into rle_standaard_voorwaarden sve1 using ( select 'VERZEKERINGSPAKKET NIET ACTIEF (WGR)' code ,q'#2.2) Werkgevers zonder verzekeringspakket#' naam ,q'#Definitie:
Werkgevers met op gegeven peildatum geen geldige Polis AO (gefiatteerd en niet geannuleerd) die gebaseerd is op het VERZEKERINGSPAKKET met het opgegeven Pakketnummer.
- geen geldige Vastgesteld Verzekeringspakket AO
Parameters:
1	Datum
2	Pakketnummer (REG VERZEKERINGSPAKKET.Verzekeringspakketnummer)#' beschrijving ,q'#select 1
from   rle_relatie_externe_codes rec2
where  rec2.rle_numrelatie      = :NUMRELATIE
and    rec2.dwe_wrddom_extsys   = 'WGR'
and    rec2.rol_codrol          = 'WG'
and    not exists (select 'x' from aos_polissen_ao pao
                   where  pao.wgr_werkgnr         = rec2.extern_relatie
                   and    pao.vzp_nummer          in ( :VAR1 )
                   and    :VAR2 between pao.ingangsdatum and nvl (pao.einddatum_premie,:VAR2)
                   and    pao.fiatteur_polis      is not null
                   and    pao.fiatteur_annulering is null
                  )#' sqltext ,q'#Verzekeringspakket (b.v. 38,39,40 )#' var1_naam ,'R' var1_datatype ,q'#Peildatum (DD-MM-YYYY)#' var2_naam ,'D' var2_datatype ,q'##' var3_naam ,'' var3_datatype ,q'##' var4_naam ,'' var4_datatype ,q'##' var5_naam ,'' var5_datatype ,'COMP_RLE' creatie_door ,to_date('03-02-2010 08:57:19','dd-mm-yyyy hh24:mi:ss') dat_creatie ,'COMP_RLE' mutatie_door ,to_date('03-02-2010 08:57:19','dd-mm-yyyy hh24:mi:ss') dat_mutatie from dual ) sve2 on (sve1.code = sve2.code) when matched then update set naam          = sve2.naam ,   beschrijving  = sve2.beschrijving ,   sqltext       = sve2.sqltext ,   var1_naam     = sve2.var1_naam ,   var1_datatype = sve2.var1_datatype ,   var2_naam     = sve2.var2_naam ,   var2_datatype = sve2.var2_datatype ,   var3_naam     = sve2.var3_naam ,   var3_datatype = sve2.var3_datatype ,   var4_naam     = sve2.var4_naam ,   var4_datatype = sve2.var4_datatype ,   var5_naam     = sve2.var5_naam ,   var5_datatype = sve2.var5_datatype ,   creatie_door  = sve2.creatie_door ,   dat_creatie   = sve2.dat_creatie ,   mutatie_door  = sve2.mutatie_door ,   dat_mutatie   = sve2.dat_mutatie  where code = sve2.code when not matched then insert   (id, code, naam, beschrijving, sqltext, var1_naam, var1_datatype, var2_naam, var2_datatype, var3_naam, var3_datatype, var4_naam, var4_datatype, var5_naam, var5_datatype,creatie_door,dat_creatie,mutatie_door,dat_mutatie) values   (rle_sve_seq.nextval, sve2.code, sve2.naam, sve2.beschrijving, sve2.sqltext, sve2.var1_naam, sve2.var1_datatype, sve2.var2_naam, sve2.var2_datatype, sve2.var3_naam, sve2.var3_datatype, sve2.var4_naam, sve2.var4_datatype, sve2.var5_naam, sve2.var5_datatype,sve2.creatie_door,sve2.dat_creatie,sve2.mutatie_door,sve2.dat_mutatie);
