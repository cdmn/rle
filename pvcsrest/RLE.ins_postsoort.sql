REM Draaien onder ops$ user.

insert into comp_opu.opu_postsoorten (id, srt_post, srt_overwat, oms)
values (66, 'FACTUUR LEVENSLOOP', 'WG', 'Factuur levensloop');

Insert into comp_rle.RLE_POSTSOORT_ADRESSOORTEN
   (ID, POSTSOORT, ADRESSOORT, PRIORITEIT)
 Values
   (2, 'FACTUUR LEVENSLOOP', 'CA', 1);

Insert into comp_rle.RLE_POSTSOORT_ADRESSOORTEN
   (ID, POSTSOORT, ADRESSOORT, PRIORITEIT)
 Values
   (3, 'FACTUUR LEVENSLOOP', 'SZ', 2);

commit;

