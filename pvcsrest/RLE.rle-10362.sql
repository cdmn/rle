DELETE qms_message_text mst
WHERE  mst.msp_code = 'RLE-10362'
/

DELETE qms_message_properties msp
WHERE  msp.code = 'RLE-10362'
/


INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND,
SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) VALUES ( 
'RLE-10362', 'In het postbusnr mag geen wildcard voorkomen.', 'E', 'N', 'N', 'N', NULL); 

INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) VALUES ( 
'DUT', 'In het postbusnr mag geen wildcard voorkomen.', NULL, 'RLE-10362'); 
