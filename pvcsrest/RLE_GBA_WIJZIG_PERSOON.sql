delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GBA_WIJZIG_PERSOON';
delete from stm_moduleparameters
    where mde_module = 'RLE_GBA_WIJZIG_PERSOON';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GBA_WIJZIG_PERSOON';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GBA_WIJZIG_PERSOON';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GBA_WIJZIG_PERSOON';
DELETE FROM stm_modules
    WHERE module = 'RLE_GBA_WIJZIG_PERSOON';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GBA_WIJZIG_PERSOON',1,'RLE',1
    ,'RLE0934: Wijzigen persoon vanuit GBA (alleen aan te roepen door GBA!!!)','P'
    ,'RLE_GBA_WIJZIG_PERSOON',''
    ,'A',''
    ,17);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIN_PERSOONSNUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIN_SOFINUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N3','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIN_A_NUMMER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A4','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_GBA_STATUS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_GBA_AFNEMERS_IND','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_NAAM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_VOORLETTERS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A8','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_VOORVOEGSELS','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D9','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PID_GEBOORTEDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A10','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_GESLACHT','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D11','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PID_OVERLIJDENSDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D12','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PID_REGDAT_OVERLIJDEN','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D13','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PID_EMIGRATIEDATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A14','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_NAAM_PARTNER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A15','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'PIV_VVGSL_PARTNER','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A16','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'POV_VERWERKT','OUT');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A17','RLE_GBA_WIJZIG_PERSOON',1,'J'
    ,'POV_FOUTMELDING','OUT');
