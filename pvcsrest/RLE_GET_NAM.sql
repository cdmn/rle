delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_GET_NAM';
delete from stm_moduleparameters
    where mde_module = 'RLE_GET_NAM';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_GET_NAM';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_GET_NAM';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_GET_NAM';
DELETE FROM stm_modules
    WHERE module = 'RLE_GET_NAM';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_GET_NAM',1,'RLE',1
    ,'Ophalen NAMRELATIE','F'
    ,'RLE_GET_NAM',''
    ,'A',''
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N2','RLE_GET_NAM',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A1','RLE_GET_NAM',1,'J'
    ,'RETURNVALUE','OUT');
