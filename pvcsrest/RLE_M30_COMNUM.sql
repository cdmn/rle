delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_M30_COMNUM';
delete from stm_moduleparameters
    where mde_module = 'RLE_M30_COMNUM';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_M30_COMNUM';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_M30_COMNUM';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_M30_COMNUM';
DELETE FROM stm_modules
    WHERE module = 'RLE_M30_COMNUM';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_M30_COMNUM',1,'RLE',1
    ,'Ophalen communicnr van relatie','P'
    ,'RLE_M30_COMNUM',''
    ,'A',''
    ,7);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_M30_COMNUM',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_M30_COMNUM',1,'J'
    ,'P_CODROL','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A3','RLE_M30_COMNUM',1,'J'
    ,'P_SRTCOM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D4','RLE_M30_COMNUM',1,'J'
    ,'P_DATUM','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A5','RLE_M30_COMNUM',1,'J'
    ,'P_NUMCOMMUNICATIE','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A6','RLE_M30_COMNUM',1,'J'
    ,'P_O_SRTCOM','MOD');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A7','RLE_M30_COMNUM',1,'J'
    ,'P_MELDING','MOD');
