INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10332'
            , 'Bij het opvoeren van een aanschijfwijze van een persoon moet deze kloppen bij het geslacht.'
            , 'E', 'N', 'N', 'N', '');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10332', 'DUT'
            , 'Bij het opvoeren van een aanschijfwijze van een persoon moet deze kloppen bij het geslacht.'
            , 'Bij het opvoeren van een aanschijfwijze van een persoon moet deze kloppen bij het geslacht.'
            );

