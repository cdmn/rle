/*************************************************************************
***              Uitdelen RLE grants aan APP_KBD_PME                   ***
*************************************************************************/

grant select on COMP_RLE.RLE_V_WERKGEVERS_XL            to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_PERSONEN_XL              to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_RELATIEKOPPELINGEN       to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_PERSONEN                 to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_ADMINISTRATIEKANTOREN    to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_ADM_KANTOREN             to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_WERKGEVERS_XL_PORTAL     to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_WERKGEVERS_PORTAL_LOOKUP to APP_KBD_PME;
grant select on COMP_RLE.RLE_V_RELATIES                 to APP_KBD_PME;
GRANT SELECT ON COMP_RLE.RLE_V_PERSONEN_XL_PORTAL       TO APP_KBD_PME;
GRANT SELECT ON COMP_RLE.RLE_V_RELATIEROLLEN_IN_ADM_ZC  TO APP_KBD_PME;
grant execute on comp_rle.rle_lib                       to app_kbd_pme;


