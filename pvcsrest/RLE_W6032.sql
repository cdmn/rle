/* Naam......: RLE_W6032.sql
** Release...: W6032
** Datum.....: 13-01-2010 16:07:39
** Notes.....: Gegenereerd door ITStools Versie 3.26
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W6032', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema voor RLE.rle_zwg1_ind1.ind
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_v_zoek_werkgever.vw
@@ RLE.rle_v_zoek_werkgever.vw
prompt RLE.rle_v_zoek_werknemer.vw
@@ RLE.rle_v_zoek_werknemer.vw

prompt RLE.rle_mv_zoek_werknemer_1.vw
@@ RLE.rle_mv_zoek_werknemer_1.vw
prompt RLE.rle_mv_zoek_werknemer_2.vw
@@ RLE.rle_mv_zoek_werknemer_2.vw
prompt RLE.rle_mv_zoek_werkgever_1.vw
@@ RLE.rle_mv_zoek_werkgever_1.vw
prompt RLE.rle_mv_zoek_werkgever_2.vw
@@ RLE.rle_mv_zoek_werkgever_2.vw

prompt RLE.rle_zwg1_ind1.ind
@@ RLE.rle_zwg1_ind1.ind
prompt RLE.rle_zwg1_ind2.ind
@@ RLE.rle_zwg1_ind2.ind
prompt RLE.rle_zwg1_ind3.ind
@@ RLE.rle_zwg1_ind3.ind
prompt RLE.rle_zwg1_ind4.ind
@@ RLE.rle_zwg1_ind4.ind
prompt RLE.rle_zwg1_ind5.ind
@@ RLE.rle_zwg1_ind5.ind
prompt RLE.rle_zwg1_ind6.ind
@@ RLE.rle_zwg1_ind6.ind
prompt RLE.rle_zwg2_ind1.ind
@@ RLE.rle_zwg2_ind1.ind
prompt RLE.rle_zwg2_ind2.ind
@@ RLE.rle_zwg2_ind2.ind
prompt RLE.rle_zwg2_ind3.ind
@@ RLE.rle_zwg2_ind3.ind
prompt RLE.rle_zwg2_ind4.ind
@@ RLE.rle_zwg2_ind4.ind
prompt RLE.rle_zwg2_ind5.ind
@@ RLE.rle_zwg2_ind5.ind
prompt RLE.rle_zwg2_ind6.ind
@@ RLE.rle_zwg2_ind6.ind
prompt RLE.rle_zwn1_ind1.ind
@@ RLE.rle_zwn1_ind1.ind
prompt RLE.rle_zwn1_ind10.ind
@@ RLE.rle_zwn1_ind10.ind
prompt RLE.rle_zwn1_ind11.ind
@@ RLE.rle_zwn1_ind11.ind
prompt RLE.rle_zwn1_ind2.ind
@@ RLE.rle_zwn1_ind2.ind
prompt RLE.rle_zwn1_ind3.ind
@@ RLE.rle_zwn1_ind3.ind
prompt RLE.rle_zwn1_ind4.ind
@@ RLE.rle_zwn1_ind4.ind
prompt RLE.rle_zwn1_ind5.ind
@@ RLE.rle_zwn1_ind5.ind
prompt RLE.rle_zwn1_ind6.ind
@@ RLE.rle_zwn1_ind6.ind
prompt RLE.rle_zwn1_ind7.ind
@@ RLE.rle_zwn1_ind7.ind
prompt RLE.rle_zwn1_ind8.ind
@@ RLE.rle_zwn1_ind8.ind
prompt RLE.rle_zwn1_ind9.ind
@@ RLE.rle_zwn1_ind9.ind
prompt RLE.rle_zwn2_ind1.ind
@@ RLE.rle_zwn2_ind1.ind
prompt RLE.rle_zwn2_ind10.ind
@@ RLE.rle_zwn2_ind10.ind
prompt RLE.rle_zwn2_ind11.ind
@@ RLE.rle_zwn2_ind11.ind
prompt RLE.rle_zwn2_ind2.ind
@@ RLE.rle_zwn2_ind2.ind
prompt RLE.rle_zwn2_ind3.ind
@@ RLE.rle_zwn2_ind3.ind
prompt RLE.rle_zwn2_ind4.ind
@@ RLE.rle_zwn2_ind4.ind
prompt RLE.rle_zwn2_ind5.ind
@@ RLE.rle_zwn2_ind5.ind
prompt RLE.rle_zwn2_ind6.ind
@@ RLE.rle_zwn2_ind6.ind
prompt RLE.rle_zwn2_ind7.ind
@@ RLE.rle_zwn2_ind7.ind
prompt RLE.rle_zwn2_ind8.ind
@@ RLE.rle_zwn2_ind8.ind
prompt RLE.rle_zwn2_ind9.ind
@@ RLE.rle_zwn2_ind9.ind

set define off
prompt RLE.rle_mvr_ptl.pks
@@ RLE.rle_mvr_ptl.pks
set define on
set define off
prompt RLE.rle_mvr_ptl.pkb
@@ RLE.rle_mvr_ptl.pkb
set define on

prompt RLE.rle_mv_zoek_werkgever_syn.syn
@@ RLE.rle_mv_zoek_werkgever_syn.syn
prompt RLE.rle_mv_zoek_werknemer_syn.syn
@@ RLE.rle_mv_zoek_werknemer_syn.syn

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W6032 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W6032 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
