DELETE qms_message_text mst WHERE  mst.msp_code like 'RLE-10399';
DELETE qms_message_properties msp WHERE  msp.code like 'RLE-10399';
INSERT INTO QMS_MESSAGE_PROPERTIES ( CODE, DESCRIPTION, SEVERITY, LOGGING_IND, SUPPR_WRNG_IND, SUPPR_ALWAYS_IND, CONSTRAINT_NAME ) 
VALUES ( 'RLE-10399', 'Relatie <p1> heeft Levensloop overeenkomst.', 'E', 'N', 'N', 'N', NULL);
INSERT INTO QMS_MESSAGE_TEXT ( LANGUAGE, TEXT, HELP_TEXT, MSP_CODE ) 
VALUES ( 'DUT', 'Relatie <p1> heeft Levensloop overeenkomst.', NULL, 'RLE-10399');


