delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I093';
delete from stm_moduleparameters
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I093';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I093';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I093';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_PCK_LIJSTEN.PRC_INIT_I093';
DELETE FROM stm_modules
    WHERE module = 'RLE_PCK_LIJSTEN.PRC_INIT_I093';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_PCK_LIJSTEN.PRC_INIT_I093',1,'RLE',1
    ,'Ophalen werkgevers bij administratiekantoor (start)','P'
    ,'RLE_PCK_LIJSTEN.PRC_INIT_I093',''
    ,'A',''
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_PCK_LIJSTEN.PRC_INIT_I093',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('D2','RLE_PCK_LIJSTEN.PRC_INIT_I093',1,'J'
    ,'P_PEILDATUM','INP');
