/* Naam......: RLE_W4927.sql
** Release...: W4927
** Datum.....: 09-03-2009 16:53:04
** Notes.....: Gegenereerd door ITStools Versie 3.19
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4927', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema voor RLE.rle_ext_adr_subsets.tab
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_ext_adr_subsets.tab
@@ RLE.rle_ext_adr_subsets.tab
prompt RLE.rle_ext_adr_verwerkingsregels.tab
@@ RLE.rle_ext_adr_verwerkingsregels.tab
prompt RLE.rle_ext_afgestemde_adressen.tab
@@ RLE.rle_ext_afgestemde_adressen.tab
@@ RLE.rle_ext_adressen_bestand.tab
prompt RLE.rle_eas_pk.con
@@ RLE.rle_eas_pk.con
prompt RLE.rle_eat_pk.con
@@ RLE.rle_eat_pk.con
prompt RLE.rle_eat_uk1.con
@@ RLE.rle_eat_uk1.con
@@ RLE.rle_eas_uk1.con
@@ RLE.rle_eat_uk2.con
prompt RLE.rle_eas_eat_fk.con
@@ RLE.rle_eas_eat_fk.con
@@ RLE.rle_ext_afg_adressen.pks
@@ RLE.rle_ext_afg_adressen.pkb
@@ RLE.rle_ras_toev.prc
prompt RLE.rle_eas_seq1.sqs
@@ RLE.rle_eas_seq1.sqs
@@ RLE.rle_eat_seq1.sqs
@@ RLE.rle_eas_seq1_next_id.fnc
@@ RLE.ins_verw_regels_ads.sql
@@ RLE.ins_subset_adr_init_ooc.sql

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4927 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W4927 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
