/* Naam......: RLE_W11155.sql
** Release...: W11155
** Datum.....: 18-10-2016 16:10:55
** Notes.....: Gegenereerd door ItsTools Versie 5.1.1.1
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W11155', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste 11g schema voor RLE.rle_ads_ar.trg
*/
set define on
connect /@&&DBN
@su COMP_RLE

prompt RLE.rle_ads_ar.trg
@@ RLE.rle_ads_ar.trg
prompt RLE.rle_ads_as.trg
@@ RLE.rle_ads_as.trg
prompt RLE.rle_ras_ar.trg
@@ RLE.rle_ras_ar.trg
prompt RLE.rle_ras_as.trg
@@ RLE.rle_ras_as.trg
prompt RLE.rle_rle_ar.trg
@@ RLE.rle_rle_ar.trg
prompt RLE.rle_rle_as.trg
@@ RLE.rle_rle_as.trg
prompt RLE.rle_stt_ar.trg
@@ RLE.rle_stt_ar.trg
prompt RLE.rle_stt_as.trg
@@ RLE.rle_stt_as.trg
prompt RLE.rle_wps_ar.trg
@@ RLE.rle_wps_ar.trg
prompt RLE.rle_wps_as.trg
@@ RLE.rle_wps_as.trg
prompt RLE.rle_wps_bs.trg
@@ RLE.rle_wps_bs.trg

prompt RLE.rle_asa_ar.trg
@@ RLE.rle_asa_ar.trg
prompt RLE.rle_asa_as.trg
@@ RLE.rle_asa_as.trg
prompt RLE.rle_asa_bs.trg
@@ RLE.rle_asa_bs.trg
prompt RLE.rle_rec_as.trg
@@ RLE.rle_rec_as.trg

prompt RLE.rle_rkn_ar.trg
@@ RLE.rle_rkn_ar.trg
prompt RLE.rle_rkn_as.trg
@@ RLE.rle_rkn_as.trg


set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W11155 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W11155 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
