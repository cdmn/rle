delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0340F';
delete from stm_moduleparameters
    where mde_module = 'RLE0340F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0340F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0340F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0340F';
DELETE FROM stm_modules
    WHERE module = 'RLE0340F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0340F',1,'RLE',1
    ,'Onderhouden verzendbestemmingen','S'
    ,'RLE0340F',''
    ,'A','Onderhouden verzendbestemming'
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0340F',1,'J'
    ,'Relatienr','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('CODE_ROL','RLE0340F',1,'J'
    ,'Rol','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0340F',1,'REF_SAD',
        ':VBG.CODE_SOORT_ADRES');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0340F',1,'NR_ADMINKA',
        ':VBG.RLE_NUMRELATIE_VIA');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0340F',1,'CODE_ROL',
        ':GLOBAL.CG$CODE_ROL');
