/* verwijderen van overbodige rollen van testomgevingen */
--
alter trigger RLE_ROL_BDR disable;

delete from rle_rollen
where codrol in ('AL','AP','BM','DI','DO','DP','IM','OA','OO','OP','PM','RK','UO','VB','VG','VL','VN','VV','VZ','ZF');

alter trigger RLE_ROL_BDR enable;
--
