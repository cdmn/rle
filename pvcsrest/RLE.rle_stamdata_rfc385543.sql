insert into rle_rollen( codrol, omsrol, indwijzigbaar, indschonen, indgroepsrol, rle_vrol_plus_rol_codrol, prioriteit )
  select 'TP', 'Tussenpersoon', 'J', 'N', 'N', 'P', 123
  from dual
  where not exists ( select 1
                     from rle_rollen
                     where codrol = 'TP'
                   );

insert into rle_rollen( codrol, omsrol, indwijzigbaar, indschonen, indgroepsrol, rle_vrol_plus_rol_codrol, prioriteit )
  select 'GV', 'Gevolmachtigde', 'J', 'N', 'N', 'P', 124
  from dual
  where not exists ( select 1
                     from rle_rollen
                     where codrol = 'GV'
                   );

insert into rle_sadres_rollen( codrol, dwe_wrddom_sadres, volgnr )
  select 'TP', 'SZ', 1
  from dual
  where not exists ( select 1
                     from rle_sadres_rollen
                     where codrol = 'TP'
                     and   dwe_wrddom_sadres = 'SZ'
                   );

insert into rle_sadres_rollen( codrol, dwe_wrddom_sadres, volgnr )
  select 'TP', 'CA', 2
  from dual
  where not exists ( select 1
                     from rle_sadres_rollen
                     where codrol = 'TP'
                     and   dwe_wrddom_sadres = 'CA'
                   );


insert into rle_sadres_rollen( codrol, dwe_wrddom_sadres, volgnr )
  select 'GV', 'CA', 1
  from dual
  where not exists ( select 1
                     from rle_sadres_rollen
                     where codrol = 'GV'
                     and   dwe_wrddom_sadres = 'CA'
                   );

insert into rle_sadres_rollen( codrol, dwe_wrddom_sadres, volgnr )
  select 'UI', 'SZ', 3
  from dual
  where not exists ( select 1
                     from rle_sadres_rollen
                     where codrol = 'UI'
                     and   dwe_wrddom_sadres = 'SZ'
                   );

insert into rle_sadres_rollen( codrol, dwe_wrddom_sadres, volgnr )
  select 'AD', 'SZ', 3
  from dual
  where not exists ( select 1
                     from rle_sadres_rollen
                     where codrol = 'AD'
                     and   dwe_wrddom_sadres = 'SZ'
                   );



