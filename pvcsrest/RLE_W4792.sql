/* Naam......: RLE_W4792.sql
** Release...: W4792
** Datum.....: 24-11-2008 11:46:40
** Notes.....: Gegenereerd door ITStools Versie 3.16
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met ITS-bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4792', sysdate );

commit;


WHENEVER SQLERROR EXIT
WHENEVER OSERROR  EXIT

/* Switchen naar het juiste schema voor RLE.rle_relatie_verzetten.tab
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_relatie_verzetten_rename.tab
@@ RLE.rle_relatie_verzetten_rename.tab
prompt RLE.rle_relatie_verzetten.tab
@@ RLE.rle_relatie_verzetten.tab
prompt RLE.rle_rvt_rle_fk.ind
@@ RLE.rle_rvt_rle_fk.ind
prompt RLE.rle_rvt_pk.con
@@ RLE.rle_rvt_pk.con
prompt RLE.rle_rvt_uk.con
@@ RLE.rle_rvt_uk.con
prompt RLE.rle_rvt_adm_fk.con
@@ RLE.rle_rvt_adm_fk.con
prompt RLE.rle_rvt_rle_fk.con
@@ RLE.rle_rvt_rle_fk.con
prompt RLE.rle_v_relatie_verzetten.vw
@@ RLE.rle_v_relatie_verzetten.vw
prompt RLE.rle_rvt_seq1.sqs
@@ RLE.rle_rvt_seq1.sqs
set define off
prompt RLE.rle_vzt.pks
@@ RLE.rle_vzt.pks
set define on
set define off
prompt RLE.rle_vzt.pkb
@@ RLE.rle_vzt.pkb
set define on
prompt RLE.rle_rvt_bri.trg
@@ RLE.rle_rvt_bri.trg
prompt RLE.rle_rvt_bru.trg
@@ RLE.rle_rvt_bru.trg
/* Switchen naar het juiste schema voor RLE.communicatie_kanaal.dom
*/
set define on
connect /@&&DBN

prompt RLE.communicatie_kanaal.dom
@@ RLE.communicatie_kanaal.dom
prompt RLE.tussenpersoon.dom
@@ RLE.tussenpersoon.dom
prompt RLE.verzet_productgroep.dom
@@ RLE.verzet_productgroep.dom

set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'W4792 : Klaar', sysdate );

commit;

prompt Implementatie RLE release W4792 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
