delete from stm_wrdparameters
    where mpr_mde_module = 'RLE_RLE_GET_AANHEF';
delete from stm_moduleparameters
    where mde_module = 'RLE_RLE_GET_AANHEF';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE_RLE_GET_AANHEF';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE_RLE_GET_AANHEF';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE_RLE_GET_AANHEF';
DELETE FROM stm_modules
    WHERE module = 'RLE_RLE_GET_AANHEF';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE_RLE_GET_AANHEF',1,'RLE',1
    ,'Ophalen aanhef relatie','P'
    ,'RLE_RLE_GET_AANHEF',''
    ,'A',''
    ,2);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('N1','RLE_RLE_GET_AANHEF',1,'J'
    ,'P_NUMRELATIE','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('A2','RLE_RLE_GET_AANHEF',1,'J'
    ,'P_AANHEF','OUT');
