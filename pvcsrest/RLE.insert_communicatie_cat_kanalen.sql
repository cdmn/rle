PROMPT aanmaken Communicatie Categorie Kanalen
insert into RLE_COMMUNICATIE_CAT_KANALEN
            (  CCN_ID , DWE_WRDDOM_SRTCOM, DATUM_BEGIN )
select distinct ccn.id, rfe.code, trunc(sysdate)
from   rfe_v_domeinwaarden  rfe
,        rle_communicatie_categorieen ccn
WHERE  rfe.domeincode     = 'SC'
and    ccn.code = 'ALG'
and    not exists (select 1  
                   from   RLE_COMMUNICATIE_CAT_KANALEN ccn2
                   where  ccn2.ccn_id = ccn.id
                   and    ccn2.DWE_WRDDOM_SRTCOM = rfe.code)
/

commit;
