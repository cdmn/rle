/* Naam......: RLE_P8647.sql
** Release...: P8647
** Datum.....: 03-04-2013 10:05:19
** Notes.....: Gegenereerd door ItsTools Versie 4.0.3.1
*/

set linesize   79
set pause     off
set define    on

WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Speciaal voor sqlplus onder windows
   bij de connect gaat het naar de default database (local variabele)
   Deze hoeft niet altijd goed te staan
*/

col DBN new_value DBN noprint
select name DBN from v$database;

WHENEVER OSERROR  CONTINUE
@logging
WHENEVER OSERROR  EXIT failure rollback

/* Check op aanwezigheid COMP_RLE en de tablespaces
   RLE_C. Indien niet aanwezig dan
   SQLERROR af laten gaan.
*/

declare
  dummy char(1);
begin
  select 'X'
    Into dummy
    From dba_users
   Where username = 'COMP_RLE';
  select 'X'
    Into dummy
    From dba_tablespaces
   Where tablespace_name = 'RLE_C';
exception
  when no_data_found then
     raise_application_error(-20000,
         'COMP_RLE en/of RLE_C bestaat niet in deze database,
*          neem contact op met bo/cc !');
end;
/

/* Vullen versienummer
*/

insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'P8647', sysdate );

commit;


WHENEVER SQLERROR EXIT failure rollback
WHENEVER OSERROR  EXIT failure rollback

/* Switchen naar het juiste schema voor RLE.rle_finnr_coda.tab
*/
set define on
connect /@&&DBN
set verify off
define _password = SU_PASSWORD
column password new_value _password noprint
select password from sys.dba_users where username = upper('COMP_RLE');
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

prompt RLE.rle_finnr_coda.tab
@@ RLE.rle_finnr_coda.tab
prompt RLE.rle_fnr_sepa_ind1.ind
@@ RLE.rle_fnr_sepa_ind1.ind
prompt RLE.rle_fnc_pk.con
@@ RLE.rle_fnc_pk.con
prompt RLE.rle_fnc_uk1.con
@@ RLE.rle_fnc_uk1.con
prompt RLE.rle_fnc_adm_fk1.con
@@ RLE.rle_fnc_adm_fk1.con
prompt RLE.rle_fnc_fnr_fk1.con
@@ RLE.rle_fnc_fnr_fk1.con
prompt RLE.fnc_fnr_fk.con
@@ RLE.fnc_fnr_fk.con
prompt RLE.rle_get_fnr_tag.prc
@@ RLE.rle_get_fnr_tag.prc
prompt RLE.rle_ins_fnr_tag.prc
@@ RLE.rle_ins_fnr_tag.prc
prompt RLE.rle_insert_rie.prc
@@ RLE.rle_insert_rie.prc
prompt RLE.rle_upd_fnr_tag.prc
@@ RLE.rle_upd_fnr_tag.prc
prompt RLE.rle_grants_tbv_coda.sql
@@ RLE.rle_grants_tbv_coda.sql
prompt RLE.rle_update_codwijzebataling.sql
@@ RLE.rle_update_codwijzebataling.sql


set define on
connect /@&&DBN
insert into STM_VERSIE_WIJZIGINGEN (APPLICATIE_CODE, RELEASE_NUMMER, DATUM_TIJD)
       values ( 'RLE', 'P8647 : Klaar', sysdate );

commit;

prompt Implementatie RLE release P8647 voltooid.
WHENEVER OSERROR CONTINUE
set termout off
@logging_off

/* Bij windows willen we niet dat als het programma be�indigd, dat sqlplus
   wordt verlaten. Echter, dit willen we wel als er op unix gewerkt wordt.
   Daarom wordt hier getest of er op unix gewerkt wordt.
*/
col P_EXIT new_value P_EXIT noprint
select decode(substr(userenv('terminal'),1,3)
              , null,'exit'
              ,'pts','exit'
              ,'continue') P_EXIT
from dual;

WHENEVER SQLERROR &&P_EXIT
select * from deze_tabel_bestaat_niet;
set termout on
