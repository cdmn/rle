whenever sqlerror exit failure
whenever oserror exit failure

set define '&'
set define on
set scan on
set pagesize 0
set heading off
set feedback off
set pause off
set linesize 200
set verify off
set trimspool on
--
declare
   t_sqlerrm   varchar2(255);
begin
   delete 
     from rle_uitval_adressen
        ;
   --
   commit;
   --
   insert into stm_job_statussen
   (   script_naam
   ,   script_id
   ,   programma_naam
   ,   volgnummer
   ,   uitvoer_soort
   ,   uitvoer_waarde
   ,   datum_tijd_mutatie
   ,   gebruiker
   )
   values
   (   upper('&1')
   ,   &2
   ,   'RLECAN01'
   ,   1
   ,   'S'
   ,   '0'
   ,   sysdate
   ,   user
   );
   --	  
   commit;
   --
exception
   when others 
   then
      t_sqlerrm := substr(stm_get_batch_message(sqlerrm),1,255);
      rollback;
      stm_util.debug(t_sqlerrm);
      --
      insert into stm_job_statussen
         ( script_naam
         , script_id
         , programma_naam
         , volgnummer
         , uitvoer_soort
         , uitvoer_waarde
         , datum_tijd_mutatie
         , gebruiker
         )
      values
         ( upper('&1')
         , &2
         , 'RLECAN01'
         , 1
         , 'S'
         , '1'
         , sysdate
         , user
         );
      insert into stm_job_statussen
         ( script_naam
         , script_id
         , programma_naam
         , volgnummer
         , uitvoer_soort
         , uitvoer_waarde
         , datum_tijd_mutatie
         , gebruiker
         )
      values
         ( upper('&1')
         , &2
         , 'RLECAN01'
         , 2
         , 'I'
         , t_sqlerrm
         , sysdate
         , user
         );
      --
      commit;
      --
end;
/
exit;
