/* Formatted on 20-09-2012 15:49:46 (QP5 v5.163.1008.3004) - Mn template 1.0 */
rem **************************************************************************************
rem Naam      : RLE_PME_REGELING.sql
rem Doel      : Overzicht actieve werkgevers voor de regeling PME
rem
rem Parameters: %1  script naam   ( wordt door JCS bepaald )
rem             %2  script id     ( wordt door JCS bepaald )
rem
rem Wijzigingshistorie:
rem Versie Naam                   Datum      Wijziging
rem ------ ---------------------- ---------- ---------------------------------------------
REM 1.0    J.Hengst            20-09-2012 Creatie
rem **************************************************************************************


whenever sqlerror exit failure
whenever oserror  exit failure

set linesize 10000
set define '&'
set define on
set pagesize 0
set heading off
set feedback off
set pause off
set verify off
set trimspool on
set termout off

variable var_context_naam varchar2(10)

begin
  :var_context_naam := stm_util.t_context;
end;
/

var b_script_naam             varchar2(20);
var b_script_id               varchar2(20);

exec :b_script_naam             := '&1'
exec :b_script_id               := '&2'


--
alter session set nls_date_format='DD-MM-YYYY';
--
whenever sqlerror exit failure
whenever oserror  exit failure
--

rem - Ophalen instance naam

define l_instance = ''

column l1 noprint new_value l_instance

select name || '_' l1
from   v$database
where  name != 'PPVD'
/

rem - Bepalen bestandsnaam
rem - De bestandsnaam begint altijd met de scriptnaam, %1
rem   en eindigt (voor de extensie) met het job_id, %2

define l_file_naam = ''

column l2 new_value l_file_naam

select '&l_instance'                                                                                    -- INSTANCE NAAM
                    || '&1'                                                                                -- SCRIPTNAAM
                           || '_' || 'RLE_PME_REGELING' || '_' || '&2'                                         -- JOB_ID
                                                                      || '.csv' l2
from   dual
/

-- Op unix
spool $TMP/RLE/outbox/\&l_file_naam
-- Op windows
--spool &l_file_naam

select 'Overzicht actieve werkgevers voor de regeling PME ' || to_char ( trunc ( sysdate ) )
from   dual
/

select 'werkgevernummer' || ';' || 'handelsnaam' || ';' || 'statutairenaam' || ';' || 'wgr_sz_straatnaam' || ';' ||
       'wgr_sz_huisnummer' || ';' || 'wgr_sz_toevhuisnum' || ';' || 'wgr_sz_postcode' || ';' || 'wgr_sz_woonplaats' ||
       ';' || 'wgr_sz_landnaam' || ';' || 'wgr_ca_straatnaam' || ';' || 'wgr_ca_huisnummer' || ';' ||
       'wgr_ca_toevhuisnum' || ';' || 'wgr_ca_postcode' || ';' || 'wgr_ca_woonplaats' || ';' || 'wgr_ca_landnaam' ||
       ';' || 'rechtsvorm' || ';' || 'gvw' || ';' || 'kvk' || ';' || 'sa_nummer' || ';' || 'ingdat_rechtsvorm' || ';'
       || 'aantal_wkn'
from   dual;

select distinct
       lpad ( wgr.nr_werkgever
            ,6
            ,0 )                                                                --||';'||      to_number(wgr.nr_relatie)
                || ';' || rle.handelsnaam || ';' || nvl ( (select rvm.statutaire_naam
                                                           from   wgr_rechtsvormen rvm
                                                           where  rvm.wgr_id = wgr.id
                                                           and rvm.dat_begin = (select max ( rvm1.dat_begin )
                                                                                from   wgr_rechtsvormen rvm1
                                                                                where  rvm1.wgr_id = rvm.wgr_id) )
                                                        ,rle.namrelatie ) || ';' || (select straat || ';' ||
       huisnummer || '; ' || huisnummer_toevoeging || ';' || postcode || ';' || woonplaats || ';' || landnaam
                                                                                     from   rle_v_relatie_adressen rlv
                                                                                     where  rlv.relatienummer = wgr.nr_relatie
                                                                                     and rlv.soort_relatie = 'WGR'
                                                                                     and sysdate between rlv.ingangsdatum
                                                                                                     and nvl ( rlv.einddatum
                                                                                                             ,sysdate )
                                                                                     and rlv.soort_adres = 'SZ' ) ||
       ';' || nvl (
       (            select straat || ';' || huisnummer || '; ' || huisnummer_toevoeging || ';' || postcode || ';' ||
       woonplaats || ';' || landnaam
                    from   rle_v_relatie_adressen rlv
                    where  rlv.relatienummer =
         wgr.nr_relatie
                    and rlv.soort_relatie = 'WGR'
                    and sysdate between rlv.ingangsdatum
                                    and nvl ( rlv.einddatum, sysdate )
                    and rlv.soort_adres = 'CA' )
                  , (select straat || ';' || huisnummer || '; ' || huisnummer_toevoeging || ';' || postcode || ';' ||
       woonplaats || ';' || landnaam
                     from   rle_v_relatie_adressen rlv
                     where  rlv.relatienummer =
         wgr.nr_relatie
                     and rlv.soort_relatie = 'WGR'
                     and sysdate between rlv.ingangsdatum
                                     and nvl ( rlv.einddatum, sysdate )
                     and rlv.soort_adres = 'SZ' ) ) || ';' || (select rvm.code_rechtsvorm
                                                               from   wgr_rechtsvormen rvm
                                                               where  rvm.wgr_id = wgr.id
                                                               and rvm.dat_begin = (select max ( rvm1.dat_begin )
                                                                                    from   wgr_rechtsvormen rvm1
                                                                                    where  rvm1.wgr_id = rvm.wgr_id) )
       || ';' || wwg.code_groep_werkzhd || ';' || rle_wgr.kvk_nr ( wgr.nr_relatie ) || ';' || rle_wgr.pens_admin ( wgr.nr_relatie )
       || ';' || (select to_char ( rvm.dat_begin, 'dd-mm-yyyy' )
                  from   wgr_rechtsvormen rvm
                  where  rvm.wgr_id = wgr.id
                  and rvm.dat_begin =
         (               select max (
       rvm1.dat_begin )
                         from   wgr_rechtsvormen rvm1
                         where  rvm1.wgr_id =
         rvm.wgr_id ) ) || ';' || (select count ( avg.werknemer )
                                   from   avg_arbeidsverhoudingen avg
                                   where  avg.werkgever = to_char ( lpad ( wgr.nr_werkgever
                                                                         ,6
                                                                         ,0 ) )
                                   and sysdate between avg.dat_begin and nvl ( avg.dat_einde, sysdate )
                                   group by lpad ( wgr.nr_werkgever
                                                 ,6
                                                 ,0 ) )
from   wgr_werkgevers wgr
     , wgr_werkzaamheden wwg
     , wgr_werkgever_statussen wst
     , rle_relaties rle
where  rle.numrelatie = wgr.nr_relatie
and    wgr.id = wst.wgr_id
and    wgr.dat_einde is null
--and    lpad(wgr.nr_werkgever,6,0) in ('196275','136188') -- test jku
and    wst.code_status <> 'B'                                                -- mogelijke statussen zijn  ('b' ,'d','i')
and    wst.dat_einde is null                                                                        -- gew op verzoek fb
and    wst.wgr_id = wwg.wgr_id
and    sysdate between wwg.dat_begin and nvl ( wwg.dat_einde, sysdate + 1 )
and    wwg.code_groep_werkzhd in ('94', '100')                                                                     --pme
and    exists
         (select 1
          from   ovt_wgr_overeenkomsten wot
          where  wot.rle_numrelatie = to_number ( wgr.nr_relatie )
          and    wot.rgg_code = 'PME')
--order by 1
;
spool off
--
rem - job_statussen bijwerken

insert into stm_job_statussen ( script_naam
                              , script_id
                              , programma_naam
                              , volgnummer
                              , uitvoer_soort
                              , uitvoer_waarde
                              , datum_tijd_mutatie
                              , gebruiker
                              , context_naam )
values ( upper ( '&1' )
       , &2
       , 'RLEPME01'
       , 1
       , 'S'
       , '0'
       , sysdate
       , user
       , :var_context_naam );

commit;
