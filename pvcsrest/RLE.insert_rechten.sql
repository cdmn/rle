-- Programma : RLE.insert_rechten.sql
-- Auteur    : XSW
-- Datum     : 16-11-2011
-- Doel      : voor het correct functioneren vanqrt_mv_s090
-- Marval    : 242169
------------------------------------------------------------------------------------------------------

whenever sqlerror exit 
whenever oserror  exit 

set lines        200
set pages        100
set pause        off
set serveroutput on size 1000000
set trimspool    on
set verify       off
set feedback     on
set termout      on

spool.w7508.log

prompt ***********************************************************************************

set linesize 500
set heading off

set define on
col DBN new_value DBN noprint
select name dbn
from   v$database;

connect /@&&DBN
set define on
set verify off
define _password = "SU_PASSWORD"
column password new_value _password noprint
select password
from   sys.dba_users
where  username = upper( 'COMP_RLE' );
alter user COMP_RLE identified by COMP_RLE;
connect COMP_RLE/COMP_RLE@&&DBN
alter user COMP_RLE identified by values '&&_password';
column password clear
undefine _password

select sysdate || '  database: ' || value
from   v$parameter
where  name = 'db_name';
set heading on

prompt 
prompt *************************************************************************
prompt ***    Regelen rechten van onderliggende objecten van qrt_v_s090      ***
prompt *************************************************************************
prompt

prompt rechten rle_objecten...

grant execute on rle_get_aanhef to comp_qrt with grant option;

grant execute on rle_get_verzendnaam to comp_qrt with grant option;

prompt w7508 voltooid...

spool off
