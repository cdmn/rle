INSERT INTO qms_message_properties ( code, description, severity
                                   , logging_ind, suppr_wrng_ind, suppr_always_ind
                                   , constraint_name)
     VALUES ( 'RLE-10353', 'Als de voorvoegsels partner zijn gevuld, moet de naam partner ook gevuld zijn.'
            , 'E', 'N', 'N', 'N', 'RLE_RLE_CK21');

INSERT INTO qms_message_text ( msp_code, language, text, help_text)
     VALUES ( 'RLE-10353', 'DUT'
            , 'Als de voorvoegsels partner zijn gevuld, moet de naam partner ook gevuld zijn.'
            , '');

