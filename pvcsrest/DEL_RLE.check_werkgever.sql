prompt CHECK_WERKGEVER
set define off
set heading off
set pagesize 0
set linesize 1000
set trimspool on
set echo off

DELETE FROM  stm_gebruik_interfaces
    WHERE ifv_ife_id = (SELECT id FROM stm_interfaces WHERE codzoek = 'CHECK_WERKGEVER');
DELETE FROM aut_dienstengebruik
    WHERE ife_id = (SELECT id FROM stm_interfaces
         WHERE codzoek = 'CHECK_WERKGEVER');
DELETE FROM stm_wrdparameters
    WHERE mif_ifv_ife_id = (SELECT id FROM stm_interfaces
         WHERE codzoek = 'CHECK_WERKGEVER');
delete from stm_moduleinterfaces
    where ifv_ife_id = (select id from stm_interfaces
         where codzoek = 'CHECK_WERKGEVER');
delete from stm_abonnementen
    where ife_id = (select id from stm_interfaces
         where codzoek = 'CHECK_WERKGEVER');
delete from stm_interface_parameters
 where ifv_ife_id = (select id from stm_interfaces
         where codzoek = 'CHECK_WERKGEVER');
delete from stm_interface_versies
    where ife_id = (select id from stm_interfaces
         where codzoek = 'CHECK_WERKGEVER');
delete from stm_interfaces
    where codzoek = 'CHECK_WERKGEVER';
set define on
