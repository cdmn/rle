rem Het opvoeren van de nieuwe RLE_ROL_IN_KOPPELINGEN
rem ten behoeve van change 374612
rem Creatie: J. Chin 

set linesize   2000
set pause     off
set define off

PROMPT QUERY VOORAF

select *
from   RLE_ROL_IN_KOPPELINGEN 
where  ROL_CODROL_BEPERKT_TOT = 'WG'
/


Insert into RLE_ROL_IN_KOPPELINGEN 
(ID,CODE_ROL_IN_KOPPELING,OMSCHRIJVING,ROL_CODROL_BEPERKT_TOT,ROL_CODROL_MET) 
values ( RLE_RKG_SEQ1.nextval ,'OVGN','Overgezet naar','WG','WG')
/

Insert into RLE_ROL_IN_KOPPELINGEN 
(ID,CODE_ROL_IN_KOPPELING,OMSCHRIJVING,ROL_CODROL_BEPERKT_TOT,ROL_CODROL_MET) 
values ( RLE_RKG_SEQ1.nextval ,'AFKV','Afkomstig van','WG','WG')
/


PROMPT QUERY ACHTERAF: 

select *
from   RLE_ROL_IN_KOPPELINGEN 
where  ROL_CODROL_BEPERKT_TOT = 'WG'
/
