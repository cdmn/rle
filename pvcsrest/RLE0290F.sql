delete from stm_wrdparameters
    where mpr_mde_module = 'RLE0290F';
delete from stm_moduleparameters
    where mde_module = 'RLE0290F';
DELETE FROM stm_modulegegevens
    WHERE mde_module = 'RLE0290F';
DELETE from stm_gebruik_interfaces
    where mde_module = 'RLE0290F';
DELETE from stm_moduleinterfaces
    where mde_module = 'RLE0290F';
DELETE FROM stm_modules
    WHERE module = 'RLE0290F';
INSERT INTO stm_modules
    (module,numversie,ave_name,ave_numversie,oms,SUBTYPE,naam,locatie,codstatus,window_titel,aantal_parameters)
    VALUES
    ('RLE0290F',1,'RLE',1
    ,'Onderhouden contactpersonen','S'
    ,'RLE0290F',''
    ,'A','Onderhouden contactpersonen'
    ,null);
prompt moduleparameters
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NR_RELATIE','RLE0290F',1,'J'
    ,'Relatienr','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('RAS_ID','RLE0290F',1,'J'
    ,'Relatie adres','INP');
INSERT into stm_moduleparameters
    (prm_cod,mde_module,mde_numversie,indverplicht,prompt,typ)
    values
    ('NUMRLE_CP','RLE0290F',1,'J'
    ,'Contactpersoon','INP');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0290F',1,'CODE_ROL',
        ':GLOBAL.CG$CODE_ROL');
INSERT into stm_modulegegevens
       (mde_module,mde_numversie,ggn_cod,veld)
       values
       ('RLE0290F',1,'NR_RELATIE',
        ':CPN.RLE_NUMRELATIE');
